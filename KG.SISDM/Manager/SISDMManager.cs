﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using System.Reflection;


namespace KG.SISDM
{
    public class SISDMManager : KGRepositoryBase
    {
        public static List<ATS> GetVacancyList(string[] excludeCodeList)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ATS));
            decrit.Add(Expression.Not(Expression.In("Objid", excludeCodeList)));

            return decrit.List<ATS>() as List<ATS>;
        }

        public static ATS GetSISDMVacancy(string vacancyCode)
        {
            return session.Get<ATS>(vacancyCode);
        }

    }
}
