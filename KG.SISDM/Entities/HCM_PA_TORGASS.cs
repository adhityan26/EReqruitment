using System;
using System.Text;
using System.Collections.Generic;


namespace KG.SISDM
{

    public partial class HCM_PA_TORGASS
    {
        private int _torgassId;
        private int _tactionId;
        private string _nik;
        private DateTime _startDate;
        private string _persSubarea;
        private string _payrArea;
        private string _workContract;
        private string _persAdmin;
        private string _payrAdmin;
        private bool _isDelimitvacant;
        private DateTime? _delimitOn;
        private string _createdBy;
        private DateTime? _createdOn;
        private string _modifiedBy;
        private DateTime? _modifiedOn;
        private int _status;
        private bool _sapFlag;
        private bool _isSame;

        public virtual int TorgassId
        {
            get
            {
                return _torgassId;
            }
            set
            {
                _torgassId = value;
            }
        }

        public virtual int TactionId
        {
            get
            {
                return _tactionId;
            }
            set
            {
                _tactionId = value;
            }
        }

        public virtual string Nik
        {
            get
            {
                return _nik;
            }
            set
            {
                _nik = value;
            }
        }

        public virtual DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual string PersSubarea
        {
            get
            {
                return _persSubarea;
            }
            set
            {
                _persSubarea = value;
            }
        }

        public virtual string PayrArea
        {
            get
            {
                return _payrArea;
            }
            set
            {
                _payrArea = value;
            }
        }

        public virtual string WorkContract
        {
            get
            {
                return _workContract;
            }
            set
            {
                _workContract = value;
            }
        }

        public virtual string PersAdmin
        {
            get
            {
                return _persAdmin;
            }
            set
            {
                _persAdmin = value;
            }
        }

        public virtual string PayrAdmin
        {
            get
            {
                return _payrAdmin;
            }
            set
            {
                _payrAdmin = value;
            }
        }

        public virtual bool IsDelimitvacant
        {
            get
            {
                return _isDelimitvacant;
            }
            set
            {
                _isDelimitvacant = value;
            }
        }

        public virtual DateTime? DelimitOn
        {
            get
            {
                return _delimitOn;
            }
            set
            {
                _delimitOn = value;
            }
        }

        public virtual string CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public virtual DateTime? CreatedOn
        {
            get
            {
                return _createdOn;
            }
            set
            {
                _createdOn = value;
            }
        }

        public virtual string ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                _modifiedBy = value;
            }
        }

        public virtual DateTime? ModifiedOn
        {
            get
            {
                return _modifiedOn;
            }
            set
            {
                _modifiedOn = value;
            }
        }

        public virtual int Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual bool SapFlag
        {
            get
            {
                return _sapFlag;
            }
            set
            {
                _sapFlag = value;
            }
        }

        public virtual bool IsSame
        {
            get
            {
                return _isSame;
            }
            set
            {
                _isSame = value;
            }
        }
    }
}
