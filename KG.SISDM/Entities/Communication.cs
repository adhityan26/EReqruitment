using System;
using System.Text;
using System.Collections.Generic;


namespace KG.SISDM
{

    public partial class Communication
    {

        private int _tcommId;
        private int _tactionId;
        private string _nik;
        private DateTime _startDate;
        private string _commType;
        private string _commValue;
        private string _createdBy;
        private DateTime? _createdOn;
        private string _modifiedBy;
        private DateTime? _modifiedOn;
        private int _status;
        private bool _sapFlag;

        public virtual int TcommId
        {
            get
            {
                return _tcommId;
            }
            set
            {
                _tcommId = value;
            }
        }

        public virtual int TactionId
        {
            get
            {
                return _tactionId;
            }
            set
            {
                _tactionId = value;
            }
        }

        public virtual string Nik
        {
            get
            {
                return _nik;
            }
            set
            {
                _nik = value;
            }
        }

        public virtual DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual string CommType
        {
            get
            {
                return _commType;
            }
            set
            {
                _commType = value;
            }
        }

        public virtual string CommValue
        {
            get
            {
                return _commValue;
            }
            set
            {
                _commValue = value;
            }
        }

        public virtual string CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public virtual DateTime? CreatedOn
        {
            get
            {
                return _createdOn;
            }
            set
            {
                _createdOn = value;
            }
        }

        public virtual string ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                _modifiedBy = value;
            }
        }

        public virtual DateTime? ModifiedOn
        {
            get
            {
                return _modifiedOn;
            }
            set
            {
                _modifiedOn = value;
            }
        }

        public virtual int Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual bool SapFlag
        {
            get
            {
                return _sapFlag;
            }
            set
            {
                _sapFlag = value;
            }
        }
    }
}
