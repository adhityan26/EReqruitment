using System;
using System.Text;
using System.Collections.Generic;


namespace KG.SISDM
{

    public partial class HCM_PA_TACTION
    {
        private int _tactionId;
        private string _actionCode;
        private string _reasonCode;
        private string _nik;
        private DateTime _startDate;
        private string _positionId;
        private string _persArea;
        private string _empGroup;
        private string _esg;
        private string _createdBy;
        private DateTime? _createdOn;
        private string _modifiedBy;
        private DateTime? _modifiedOn;
        private int _status;
        private int _sapFlag;
        private int _payroll;
        private string _oldPost;

        public virtual int TactionId
        {
            get
            {
                return _tactionId;
            }
            set
            {
                _tactionId = value;
            }
        }

        public virtual string ActionCode
        {
            get
            {
                return _actionCode;
            }
            set
            {
                _actionCode = value;
            }
        }

        public virtual string ReasonCode
        {
            get
            {
                return _reasonCode;
            }
            set
            {
                _reasonCode = value;
            }
        }

        public virtual string Nik
        {
            get
            {
                return _nik;
            }
            set
            {
                _nik = value;
            }
        }

        public virtual DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual string PositionId
        {
            get
            {
                return _positionId;
            }
            set
            {
                _positionId = value;
            }
        }

        public virtual string PersArea
        {
            get
            {
                return _persArea;
            }
            set
            {
                _persArea = value;
            }
        }

        public virtual string EmpGroup
        {
            get
            {
                return _empGroup;
            }
            set
            {
                _empGroup = value;
            }
        }

        public virtual string Esg
        {
            get
            {
                return _esg;
            }
            set
            {
                _esg = value;
            }
        }

        public virtual string CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public virtual DateTime? CreatedOn
        {
            get
            {
                return _createdOn;
            }
            set
            {
                _createdOn = value;
            }
        }

        public virtual string ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                _modifiedBy = value;
            }
        }

        public virtual DateTime? ModifiedOn
        {
            get
            {
                return _modifiedOn;
            }
            set
            {
                _modifiedOn = value;
            }
        }

        public virtual int Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual int SapFlag
        {
            get
            {
                return _sapFlag;
            }
            set
            {
                _sapFlag = value;
            }
        }

        public virtual int Payroll
        {
            get
            {
                return _payroll;
            }
            set
            {
                _payroll = value;
            }
        }

        public virtual string OldPost
        {
            get
            {
                return _oldPost;
            }
            set
            {
                _oldPost = value;
            }
        }
    }
}
