using System;
using System.Text;
using System.Collections.Generic;


namespace KG.SISDM
{

    public partial class ATS
    {
        private string _objid;
        private string _stext;
        private string _plvarname;
        private string _mcStext;
        private string _buName;
        private string _persAdminId;
        private string _layerId;
        private string _layer;
        private DateTime _begda;
        private DateTime _endda;
        private string _status;

        public virtual string Objid
        {
            get
            {
                return _objid;
            }
            set
            {
                _objid = value;
            }
        }

        public virtual string Stext
        {
            get
            {
                return _stext;
            }
            set
            {
                _stext = value;
            }
        }

        public virtual string Plvarname
        {
            get
            {
                return _plvarname;
            }
            set
            {
                _plvarname = value;
            }
        }

        public virtual string McStext
        {
            get
            {
                return _mcStext;
            }
            set
            {
                _mcStext = value;
            }
        }

        public virtual string BuName
        {
            get
            {
                return _buName;
            }
            set
            {
                _buName = value;
            }
        }

        public virtual string PersAdminId
        {
            get
            {
                return _persAdminId;
            }
            set
            {
                _persAdminId = value;
            }
        }

        public virtual string LayerId
        {
            get
            {
                return _layerId;
            }
            set
            {
                _layerId = value;
            }
        }

        public virtual string Layer
        {
            get
            {
                return _layer;
            }
            set
            {
                _layer = value;
            }
        }

        public virtual DateTime Begda
        {
            get
            {
                return _begda;
            }
            set
            {
                _begda = value;
            }
        }

        public virtual DateTime Endda
        {
            get
            {
                return _endda;
            }
            set
            {
                _endda = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }
    }
}
