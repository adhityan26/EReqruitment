using System;
using System.Text;
using System.Collections.Generic;


namespace KG.SISDM
{

    public partial class ms_PersAdmin
    {
        private string _persAdminId;
        private string _persAdminText;

        public virtual string PersAdminId
        {
            get
            {
                return _persAdminId;
            }
            set
            {
                _persAdminId = value;
            }
        }

        public virtual string PersAdminText
        {
            get
            {
                return _persAdminText;
            }
            set
            {
                _persAdminText = value;
            }
        }
    }
}
