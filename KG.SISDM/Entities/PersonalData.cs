using System;
using System.Text;
using System.Collections.Generic;


namespace KG.SISDM
{

    public partial class PersonalData
    {
        private int _tpersdataId;
        private int _tactionId;
        private string _nik;
        private DateTime _startDate;
        private string _title;
        private string _name;
        private string _alias;
        private DateTime? _birthdate;
        private int _gender;
        private string _lang;
        private string _nationality;
        private string _birthplace;
        private string _birthcountry;
        private int _maritalStatus;
        private DateTime? _maritalSince;
        private string _religion;
        private string _createdBy;
        private DateTime? _createdOn;
        private string _modifiedBy;
        private DateTime? _modifiedOn;
        private int _status;
        private bool _sapFlag;

        public virtual int TpersdataId
        {
            get
            {
                return _tpersdataId;
            }
            set
            {
                _tpersdataId = value;
            }
        }

        public virtual int TactionId
        {
            get
            {
                return _tactionId;
            }
            set
            {
                _tactionId = value;
            }
        }

        public virtual string Nik
        {
            get
            {
                return _nik;
            }
            set
            {
                _nik = value;
            }
        }

        public virtual DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual string Title
        {
            get
            {
                return _title;
            }
            set
            {
                _title = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Alias
        {
            get
            {
                return _alias;
            }
            set
            {
                _alias = value;
            }
        }

        public virtual DateTime? Birthdate
        {
            get
            {
                return _birthdate;
            }
            set
            {
                _birthdate = value;
            }
        }

        public virtual int Gender
        {
            get
            {
                return _gender;
            }
            set
            {
                _gender = value;
            }
        }

        public virtual string Lang
        {
            get
            {
                return _lang;
            }
            set
            {
                _lang = value;
            }
        }

        public virtual string Nationality
        {
            get
            {
                return _nationality;
            }
            set
            {
                _nationality = value;
            }
        }

        public virtual string Birthplace
        {
            get
            {
                return _birthplace;
            }
            set
            {
                _birthplace = value;
            }
        }

        public virtual string Birthcountry
        {
            get
            {
                return _birthcountry;
            }
            set
            {
                _birthcountry = value;
            }
        }

        public virtual int MaritalStatus
        {
            get
            {
                return _maritalStatus;
            }
            set
            {
                _maritalStatus = value;
            }
        }

        public virtual DateTime? MaritalSince
        {
            get
            {
                return _maritalSince;
            }
            set
            {
                _maritalSince = value;
            }
        }

        public virtual string Religion
        {
            get
            {
                return _religion;
            }
            set
            {
                _religion = value;
            }
        }

        public virtual string CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public virtual DateTime? CreatedOn
        {
            get
            {
                return _createdOn;
            }
            set
            {
                _createdOn = value;
            }
        }

        public virtual string ModifiedBy
        {
            get
            {
                return _modifiedBy;
            }
            set
            {
                _modifiedBy = value;
            }
        }

        public virtual DateTime? ModifiedOn
        {
            get
            {
                return _modifiedOn;
            }
            set
            {
                _modifiedOn = value;
            }
        }

        public virtual int Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual bool SapFlag
        {
            get
            {
                return _sapFlag;
            }
            set
            {
                _sapFlag = value;
            }
        }
    }
}
