﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;


namespace ERecruitment.Domain
{
    public class ApplicationManager : ERecruitmentRepositoryBase
    {

        #region themes

        public static List<ApplicationThemes> GetApplicationThemeList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicationThemes));
            return decrit.List<ApplicationThemes>() as List<ApplicationThemes>;
        }

        #endregion

        #region language

        public static List<ApplicationLanguages> GetApplicationLanguageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicationLanguages));
            return decrit.List<ApplicationLanguages>() as List<ApplicationLanguages>;
        }

        #endregion

        #region pages

        public static List<string> GetPageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicationPageControls));
            decrit.SetProjection(Projections.Distinct(Projections.Property("PageName")));
            return decrit.List<string>() as List<string>;
        }

        #endregion

        #region page controls

        public static List<ApplicationPageControls> GetPageControlList(string pageName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicationPageControls));
            decrit.Add(Expression.Eq("PageName", pageName));
            return decrit.List<ApplicationPageControls>() as List<ApplicationPageControls>;
        }

        public static void SaveApplicationPageControlList(List<ApplicationPageControls> controlList)
        {
            ITransaction transaction = session.BeginTransaction();
            List<ApplicationPageControls> checkDuplicateList = new List<ApplicationPageControls>();
            ApplicationPageControls getDuplicate = new ApplicationPageControls();
            try
            {

                foreach (ApplicationPageControls item in controlList)
                {
                    if (checkDuplicateList.Contains(item))
                        getDuplicate = item;
                    session.Save(item);

                    checkDuplicateList.Add(item);

                }
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemoveApplicationPageControlList(List<ApplicationPageControls> controlList)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                foreach (ApplicationPageControls item in controlList)
                    session.Delete(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region control language

        public static List<ApplicationPageControlTexts> GetPageControlTextList(string pageName, string languageID)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicationPageControlTexts));
            decrit.Add(Expression.Eq("PageName", pageName));
            decrit.Add(Expression.Eq("LanguageCode", languageID));
            return decrit.List<ApplicationPageControlTexts>() as List<ApplicationPageControlTexts>;
        }

        public static void SavePageControlTextList(List<ApplicationPageControlTexts> controlList)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {


                foreach (ApplicationPageControlTexts item in controlList)
                    session.Save(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemovePageControlTextList(List<ApplicationPageControlTexts> controlList)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                foreach (ApplicationPageControlTexts item in controlList)
                    session.Delete(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region reports

        public static List<Reports> GetReportList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Reports));
            return decrit.List<Reports>() as List<Reports>;
        }

        public static Reports GetReport(string code)
        {
            return session.Get<Reports>(code);
        }

        public static void UpdateReport(Reports report)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<ReportSubscribers> existingList = GetReportSubscriberList(report.Code);
                foreach (ReportSubscribers item in existingList)
                    session.Delete(item);

                session.Update(report);
                foreach (ReportSubscribers subscriber in report.SubscriberList)
                {
                    subscriber.ReportCode = report.Code;
                    session.Save(subscriber);
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static List<ReportSubscribers> GetReportSubscriberList(string reportCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ReportSubscribers));
            decrit.Add(Expression.Eq("ReportCode", reportCode));
            return decrit.List<ReportSubscribers>() as List<ReportSubscribers>;
        }

        public static List<ReportAutomatic> GetReportAutoList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(ReportAutomatic));
            return decrit.List<ReportAutomatic>() as List<ReportAutomatic>;
        }

        public static List<ReportAutomatic> GetReportAutoList(string timer)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ReportAutomatic));
            decrit.Add(Expression.Eq("TimeSchedule", timer));
            return decrit.List<ReportAutomatic>() as List<ReportAutomatic>;
        }

        #endregion
    }
}
