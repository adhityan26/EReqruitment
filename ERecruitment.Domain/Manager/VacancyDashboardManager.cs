﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using NHibernate.SqlCommand;
using NHibernate.Mapping;

namespace ERecruitment.Domain
{
    public class VacancyDashboardManager : ERecruitmentRepositoryBase
    {
        public static int CountUniversityVsJobCategory(string prop1Name, object prop1Value, string prop2Name, object prop2Value, string status, String[] companiesList)
        {
            ICriteria vacancies = session.CreateCriteria(typeof(Vacancies));
            vacancies.Add(Restrictions.Eq(prop2Name, prop2Value));
            vacancies.Add(Expression.In("CompanyCode", companiesList));
            List<Vacancies> vacList = vacancies.List<Vacancies>() as List<Vacancies>;
            int count = 0;
            foreach (Vacancies vac in vacList) {
                ICriteria aplVacancies = session.CreateCriteria(typeof(ApplicantVacancies));
                aplVacancies.Add(Expression.Eq("VacancyCode", vac.Code));
                List<ApplicantVacancies> aplList = aplVacancies.List<ApplicantVacancies>() as List<ApplicantVacancies>;
                foreach (ApplicantVacancies av in aplList) {
                    ICriteria education = session.CreateCriteria(typeof(ApplicantEducations));
                    education.Add(Expression.Eq("ApplicantCode",av.ApplicantCode));
                    List<ApplicantEducations> eduList = education.List<ApplicantEducations>() as List<ApplicantEducations>;
                    foreach (ApplicantEducations edu in eduList) {
                        if (prop1Value.Equals(edu.InstitutionName)) {
                            count++;
                        }
                    }
                }
            }
            
            return count;
        }

        public static int CountPositionVsSource(string prop1Name, object prop1Value, string prop2Name, object prop2Value, string status, String[] companiesList)
        {
            int count = 0;
            String sql = "SELECT c.Name, (SELECT vc.Name FROM VacancyCategories vc WHERE vc.Code = v.CategoryCode) JobCategory, v.PositionName, av.SourceReferencesName, v.CategoryCode, v.CompanyCode, v.PositionCode  FROM ApplicantVacancies av INNER JOIN Vacancies v ON av.VacancyCode = v.Code INNER JOIN Companies c on v.CompanyCode = c.Code WHERE PositionCode ='" + prop1Value + "' AND SourceReferencesName='" + prop2Value + "'";
            String paramCompany = ConvertCompany(companiesList);
            IQuery query = session.CreateSQLQuery(sql + paramCompany);
            IList<Object[]> entities = query.List<Object[]>();
            foreach (Object edu in entities)
            {
                count++;
            }

            return count;
        }

        public static IList<Object[]> ReportPositionVsSource(String[] companiesList)
        {
//            int count = 0;
            IQuery query = session.CreateSQLQuery("SELECT Name, JobCategory, PositionName, SourceReferencesName, CategoryCode,CompanyCode, COUNT(SourceReferencesName) TotalApplicant FROM (SELECT c.Name, (SELECT vc.Name FROM VacancyCategories vc WHERE vc.Code = v.CategoryCode) JobCategory, v.PositionName, av.SourceReferencesName, v.CategoryCode, v.CompanyCode  FROM ApplicantVacancies av INNER JOIN Vacancies v ON av.VacancyCode = v.Code INNER JOIN Companies c on v.CompanyCode = c.Code) a GROUP BY Name, JobCategory, PositionName, SourceReferencesName, CategoryCode, CompanyCode");
            IList<Object[]> entities = query.List<Object[]>();
            return entities;
        }

        public static int CountUniversityVsPosition(string prop1Name, object prop1Value, string prop2Name, object prop2Value, string status, string from, string to, String[] companiesList)
        {
            int count = 0;
            IQuery query = session.CreateSQLQuery("SELECT * FROM (SELECT ap.ApplicantCode, ap.VacancyCode, c.PositionCode,  c.PositionName, ap.SourceReferencesName, edu.InstitutionName, edu.Score from ERecruitment.dbo.ApplicantVacancies ap inner join ERecruitment.dbo.Vacancies c on ap.VacancyCode = c.Code inner join ERecruitment.dbo.ApplicantEducations edu on edu.ApplicantCode = ap.ApplicantCode where ap.VacancyCode = c.Code) a WHERE a.PositionCode ='" + prop1Value + "' AND a.InstitutionName='" + prop2Value + "' AND (a.Score BETWEEN " + from+" AND "+to+")");
            IList<Object[]> entities = query.List<Object[]>();
            foreach (Object edu in entities)
            {
                count++;
            }

            return count;
        }

        public static int CountVacancy(string prop1Name, object prop1Value, string prop2Name, object prop2Value, string status , String[] companiesList)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.In("CompanyCode", companiesList));

            if (prop1Name == "Month")
            {
                DateTime date = new DateTime(DateTime.Now.Year, Convert.ToInt32(prop1Value), 1);
                DateTime startDate = date;
                DateTime endDate = date.AddMonths(1);
                decrit.Add(Expression.Ge("EffectiveDate", startDate));
                decrit.Add(Expression.Lt("EffectiveDate", endDate));
            }
            else if (prop1Name == "Year")
            {
                DateTime date = new DateTime(Convert.ToInt32(prop1Value), 1, 1);
                DateTime startDate = date;
                DateTime endDate = date.AddYears(1);
                decrit.Add(Expression.Ge("EffectiveDate", startDate));
                decrit.Add(Expression.Lt("EffectiveDate", endDate));
            }
            else
                decrit.Add(Expression.Eq(prop1Name, prop1Value));

            if (prop2Name == "Month")
            {
                DateTime date = new DateTime(DateTime.Now.Year, Convert.ToInt32(prop2Value), 1);
                DateTime startDate = date;
                DateTime endDate = date.AddMonths(1);
                decrit.Add(Expression.Ge("EffectiveDate", startDate));
                decrit.Add(Expression.Lt("EffectiveDate", endDate));
            }
            else if (prop2Name == "Year")
            {
                DateTime date = new DateTime(Convert.ToInt32(prop2Value), 1, 1);
                DateTime startDate = date;
                DateTime endDate = date.AddYears(1);
                decrit.Add(Expression.Ge("EffectiveDate", startDate));
                decrit.Add(Expression.Lt("EffectiveDate", endDate));
            }
            else
                decrit.Add(Expression.Eq(prop2Name, prop2Value));

            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static List<Jobs> GetJobsList()
        {
            return ERecruitmentManager.GetJobsList();
        }

        public static List<EducationLevels> GetEducationLevelList()
        {
            return ERecruitmentManager.GetEducationLevels();
        }

        public static List<Companies> GetCompanyList()
        {
            return ERecruitmentManager.GetCompanyList();
        }

        public static List<Companies> GetCompanyList(string[] companyList)
        {
            return ERecruitmentManager.GetCompanyPerSite(companyList);
        }

        public static Dictionary<string, string> GetRecruitmentTargetGroupList()
        {
            Dictionary<string, string> dataList = new Dictionary<string, string>();
            dataList.Add("Experienced", "Experienced");
            dataList.Add("Recent Graduate", "Recent Graduate");

            return dataList;
        }

        public static Dictionary<string, string> GetPositionReplacementList()
        {
            Dictionary<string, string> dataList = new Dictionary<string, string>();
            dataList.Add("New Position", "New Position");
            dataList.Add("Replacement", "Replacement");

            return dataList;
        }

        public static List<Country> GetCountryList()
        {
            return ERecruitmentManager.GetCountryList();
        }

        public static List<EmploymentStatus> GetEmploymentStatusType()
        {
            return ERecruitmentManager.GetEmploymentStatusList();
        }

        public static List<Industries> GetIndustryList()
        {
            return ERecruitmentManager.GetIndustryList();
        }

        public static List<Departments> GetDepartmentList()
        {
            return ERecruitmentManager.GetDepartmentList();
        }

        public static List<Divisions> GetDivisionList()
        {
            return ERecruitmentManager.GetDivisionList();
        }

        public static List<Years> GetYearList()
        {
            return ERecruitmentManager.GetLastYearList(10);
        }

        public static List<Month> GetMonthList()
        {
            return ERecruitmentManager.GetMonthList();
        }

        public static List<Universities> GetUniversityList()
        {
            return ERecruitmentManager.GetUniversityListDashboard();
        }

        public static List<VacancyCategories> GetVacanciesCategoryList()
        {
            return ERecruitmentManager.GetVacancyCategoryList();
        }

        public static List<Vacancies> GetPositionList()
        {
            return ERecruitmentManager.GetPositionsList();
        }

        public static List<SourceReferences> GetSourceList()
        {
            return ERecruitmentManager.GetSourceList();
        }

        public static String ConvertCompany(String[] companies) {
            String result = "";
            String q = "";
            if (companies != null && companies.Length > 0) {
                result = " AND (";
                for (int i=0; i< companies.Length; i++) {
                    
                    if (i == 0)
                    {
                        if (!String.IsNullOrEmpty(companies[i]))
                        {
                            q += "v.CompanyCode = '" + companies[i] + "'";
                        }
                        
                    }
                    else {
                        if (!String.IsNullOrEmpty(companies[i]))
                        {
                            q += "OR v.CompanyCode = '" + companies[i] + "'";
                        }
                    }
                }
                result += q + ")";
            }

            return result;
        }

    }
}
