using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;


namespace ERecruitment.Domain
{
    public class SurveyQuestionManager : ERecruitmentRepositoryBase
    {
        #region survey question

        public static void SaveSurveyQuestion(SurveyQuestions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                question.Code = Guid.NewGuid().ToString();
                session.Save(question);
                int number = 1;
                foreach (SurveyQuestionAnswerOptions option in question.AnswerOptionList)
                {
                    option.Number = number;
                    option.QuestionCode = question.Code;
                    session.Save(option);
                    number++;
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void UpdateSurveyQuestion(SurveyQuestions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<SurveyQuestionAnswerOptions> existingOptionList = GetSurveyAnswerOptionList(question.Code);
                foreach (SurveyQuestionAnswerOptions item in existingOptionList)
                    session.Delete(item);

                session.Update(question);
                int number = 1;
                foreach (SurveyQuestionAnswerOptions option in question.AnswerOptionList)
                {
                    option.Number = number;
                    option.QuestionCode = question.Code;
                    session.Save(option);
                    number++;
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void DeleteSurveyQuestion(SurveyQuestions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<SurveyQuestionAnswerOptions> existingOptionList = GetSurveyAnswerOptionList(question.Code);
                foreach (SurveyQuestionAnswerOptions item in existingOptionList)
                    session.Delete(item);

                session.Delete(question);

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static SurveyQuestions GetSurveyQuestion(string code)
        {
            return session.Get<SurveyQuestions>(code);
        }

        public static List<SurveyQuestionAnswerOptions> GetSurveyAnswerOptionList(string questionCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(SurveyQuestionAnswerOptions));
            decrit.Add(Expression.Eq("QuestionCode", questionCode));
            return decrit.List<SurveyQuestionAnswerOptions>() as List<SurveyQuestionAnswerOptions>;
        }
        
        public static List<SurveyQuestions> GetSurveyQuestionList(string globalSearch, int startRecord,
                                                                              int length, string sortColumn,
                                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(SurveyQuestions));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<SurveyQuestions>() as List<SurveyQuestions>;
        }

        public static int CountSurveyQuestionList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(SurveyQuestions));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static List<SurveyQuestions> GetSurveyQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(SurveyQuestions));
            decrit.Add(Expression.Eq("Active", true));

            return decrit.List<SurveyQuestions>() as List<SurveyQuestions>;
        }

        public static List<SurveyQuestions> GetSurveyQuestionList(string[] codeList)
        {
            ICriteria decrit = session.CreateCriteria(typeof(SurveyQuestions));
            decrit.Add(Expression.In("Code", codeList));
            decrit.Add(Expression.Eq("Active", true));

            return decrit.List<SurveyQuestions>() as List<SurveyQuestions>;
        }

        #endregion

        #region applicant survey

        public static void SaveApplicantSurvey(ApplicantSurveys survey)
        {
            SaveWithTransaction<ApplicantSurveys>(survey);
        }

        public static int CountApplicantSurvey(string questionCode, string[] companyCode, string answer)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSurveys));
            decrit.Add(Expression.Eq("QuestionCode", questionCode));
            decrit.Add(Expression.In("CompaniesCode", companyCode));
            decrit.Add(Expression.Eq("Answer", answer));

            decrit.SetProjection(Projections.Count("Answer"));

            return decrit.UniqueResult<int>();
        }

        public static int CountApplicantSurvey(string questionCode, string answer)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSurveys));
            decrit.Add(Expression.Eq("QuestionCode", questionCode));
            decrit.Add(Expression.Eq("Answer", answer));

            decrit.SetProjection(Projections.Count("Answer"));

            return decrit.UniqueResult<int>();
        }


        public static IList<Object[]> ReportSatisfactorySurvey(string questionCode)
        {
            List<String[]> ls = new List<String[]>();
            IQuery query = session.CreateSQLQuery("SELECT a.Name, a.Email, srv.Answer, srv.Notes, (SELECT TOP 1 v.PositionName FROM ApplicantSurveySessions ass INNER JOIN Vacancies v on ass.VacancyCode = v.Code WHERE ass.ApplicantCode = srv.ApplicantCode) PositionName FROM ApplicantSurveys srv INNER JOIN Applicants a on srv.ApplicantCode = a.Code WHERE srv.QuestionCode = '" + questionCode+"'");
            IList<Object[]> entities = query.List<Object[]>();
            return entities;
        }
        #endregion

    }
}

