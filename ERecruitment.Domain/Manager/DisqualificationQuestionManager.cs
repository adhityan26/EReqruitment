using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;


namespace ERecruitment.Domain
{
    public class DisqualificationQuestionManager : ERecruitmentRepositoryBase
    {
        #region disqualifier question
        public static void SaveDisqualificationQuestion(DisqualifierQuestions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                question.Code = Guid.NewGuid().ToString();
                session.Save(question);
                int number = 1;
                foreach (DisqualifierQuestionAnswerOptions option in question.AnswerOptionList)
                {
                    option.Number = number;
                    option.QuestionCode = question.Code;
                    if (option.IsCorrectAnswer)
                        question.CorrectAnswerLabel = option.Label;
                    session.Save(option);
                    number++;
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void UpdateDisqualificationQuestion(DisqualifierQuestions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<DisqualifierQuestionAnswerOptions> existingOptionList = GetAnswerOptionList(question.Code);
                foreach (DisqualifierQuestionAnswerOptions item in existingOptionList)
                    session.Delete(item);

                session.Update(question);
                int number = 1;
                foreach (DisqualifierQuestionAnswerOptions option in question.AnswerOptionList)
                {
                    option.Number = number;
                    option.QuestionCode = question.Code;
                    if (option.IsCorrectAnswer)
                        question.CorrectAnswerLabel = option.Label;
                    session.Save(option);
                    number++;
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void DeleteDisqualificationQuestion(DisqualifierQuestions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<VacancyDisqualifierQuestions> checkDisqualifierQuestion =ERecruitmentManager.GetVacancyDisqualifierQuestionList(question.Code);
                
                List<DisqualifierQuestionAnswerOptions> existingOptionList = GetAnswerOptionList(question.Code);
                foreach (DisqualifierQuestionAnswerOptions item in existingOptionList)
                    session.Delete(item);

                session.Delete(question);

                List<DisqualifierQuestions> childQuestionList = GetChildrenDisqualificationQuestionList(question.Code);
                foreach (DisqualifierQuestions childQuestion in childQuestionList)
                {
                    List<DisqualifierQuestionAnswerOptions> existingCHildOptionList = GetAnswerOptionList(childQuestion.Code);
                    foreach (DisqualifierQuestionAnswerOptions item in existingCHildOptionList)
                        session.Delete(item);

                    session.Delete(childQuestion);

                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static DisqualifierQuestions GetDisqualificationQuestion(string code)
        {
            return session.Get<DisqualifierQuestions>(code);
        }

        public static List<DisqualifierQuestions> GetDisqualifierQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }


        public static List<DisqualifierQuestions> GetDisqualifierQuestionList(string[] codeList)
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            decrit.Add(Expression.In("Code", codeList));
            decrit.Add(Expression.Eq("Active", true));

            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }

        public static List<DisqualifierQuestions> GetParentDisqualificationQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("QuestionType", "Disqualifier"));
            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }

        public static List<DisqualifierQuestions> GetParentSurveyQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("QuestionType", "Survey"));
            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }

        public static List<DisqualifierQuestions> GetChildrenDisqualificationQuestionList(string parentCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            decrit.Add(Expression.Eq("Type", "Child"));
            decrit.Add(Expression.Eq("ParentCode", parentCode));
            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }

        public static List<DisqualifierQuestionAnswerOptions> GetAnswerOptionList(string questionCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestionAnswerOptions));
            decrit.Add(Expression.Eq("QuestionCode", questionCode));
            return decrit.List<DisqualifierQuestionAnswerOptions>() as List<DisqualifierQuestionAnswerOptions>;
        }



        public static List<DisqualifierQuestions> GetDisqualifierQuestionList(string globalSearch, int startRecord,
                                                                              int length, string sortColumn,
                                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }

        public static int CountDisqualifierQuestionList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region applicant disqualifier answer

        public static void SaveDisqualifierAnswer(ApplicantDisqualifierAnswers answer)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Save(answer);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SaveSurveyAnswer(ApplicantSurveyAnswers answer)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Save(answer);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<ApplicantDisqualifierAnswers> GetApplicantDisqualifierAnswers(string applicantCode,string vacancyCode,string globalSearch,int startRecord,
                                                                                        int length,
                                                                                        string sortColumn,
                                                                                        string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantDisqualifierAnswers));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Answer", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            return decrit.List<ApplicantDisqualifierAnswers>() as List<ApplicantDisqualifierAnswers>;

        }

      
        public static int CountApplicantDisqualifierAnswers (string applicantCode, string vacancyCode,string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantDisqualifierAnswers));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Answer", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Id"));
            return decrit.UniqueResult<int>();

        }

        #endregion


    }
}

