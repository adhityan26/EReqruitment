﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Globalization;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using System.Reflection;
using System.IO;
using System.Linq;
using ERecruitment.Domain.Helper;


namespace ERecruitment.Domain
{
    public class BatchProcessManager : ERecruitmentRepositoryBase
    { 
        #region batch schema

        public static List<BatchProcessSchemas> GetBatchProcessSchemaList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(BatchProcessSchemas));
            decrit.AddOrder(Order.Asc("Name"));
            return decrit.List<BatchProcessSchemas>() as List<BatchProcessSchemas>;
        }

        public static BatchProcessSchemas GetBatchProcessSchemaByCode(string schemaCode)
        {
            return session.Get<BatchProcessSchemas>(schemaCode);
        }

        #endregion

        #region batch process

        public static void SaveBatchProcess(BatchProcesses process)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                process.Code = Guid.NewGuid().ToString();

                // save process
                session.Save(process);

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static BatchProcesses GetBatchProcess(string code)
        {
            return session.Get<BatchProcesses>(code);
        }

        public static List<BatchProcesses> GetBatchProcessList(string vacancyCode,
                                                               string globalSearch,
                                                               int startRecord,
                                                               int length,
                                                               string sortColumn,
                                                               string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(BatchProcesses));

            if (!string.IsNullOrEmpty(vacancyCode))
                decrit.Add(Expression.Eq("VacancyCode", vacancyCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("ProcesserName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<BatchProcesses>() as List<BatchProcesses>;
        }

        public static int CountBatchProcessList(string vacancyCode,
                                                string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(BatchProcesses));
            if (!string.IsNullOrEmpty(vacancyCode))
                decrit.Add(Expression.Eq("VacancyCode", vacancyCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("ProcesserName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));

            return decrit.UniqueResult<int>();
        }

        #endregion

        #region process action

        public static void ProcessUpdateStatusBatch(BatchProcesses batchProcess, Stream fileStream, string schemaCode, string importerCode, Dictionary<String, int> option)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];
            if (string.IsNullOrEmpty(delimiter))
                delimiter = ";";
            List<string> logger = new List<string>();
            Companies company = null;
            StreamReader excelReader = new StreamReader(fileStream);
            IDictionary<string, int> columnMap = new Dictionary<string, int>();
            bool readColumnHeader = false;
            while (!excelReader.EndOfStream)
            {
                if (!readColumnHeader)
                {
                    excelReader.ReadLine();
                    readColumnHeader = true;
                    continue;
                }

                string[] data = excelReader.ReadLine().Split(new string[] { delimiter }, StringSplitOptions.None);
                if (data.Length > 6)
                {
                    if (string.IsNullOrEmpty(data[0]))
                    {
                        throw new ApplicationException("Vacancy code should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[1]))
                    {
                        throw new ApplicationException("Email should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[6]))
                    {
                        throw new ApplicationException("New status should not be empty");   
                    }
                    if (data[0] != batchProcess.VacancyCode)
                    {
                        throw new ApplicationException("Invalid vacancy code not match with current process");
                    }
                    Vacancies jobVacancy = ERecruitmentManager.GetVacancy(data[0]);
                    Applicants applicant = ApplicantManager.GetApplicantByEmail(data[1]);
                
                    if (applicant == null)
                    {
                        throw new ApplicationException("Invalid email. Email is not registered as applicant");
                    }
                    
                    VacancyStatus status = ERecruitmentManager.GetVacancyStatusByRecruiterLabel(data[6]);
                    if (status == null)
                    {
                        throw new ApplicationException("Invalid status: " + data[6]);   
                    }

                    #region process object

                    ApplicantVacancies applicantVacancy = ERecruitmentManager.GetApplicantVacancies(applicant.Code, jobVacancy.Code);
                    if (applicantVacancy == null)
                    {
                        throw new ApplicationException("Invalid email. Applicant not registered with current job requisition");   
                    }
                    applicantVacancy.UpdateStamp = DateTime.Now;

                    VacancyManager.UpdateApplicantVacancyRecruitmentStatus(applicantVacancy, status.Code, DateTime.Now);
                    
                    Emails emailTemplate = ERecruitmentManager.GetEmail(option["emailTemplateId"]);
                    
                    if (company == null)
                    {
                        company = session.Get<Companies>(jobVacancy.CompanyCode);   
                    }
                    
                    Dictionary<String, String> dataTemplate = new Dictionary<string, string>
                    {
                        {"[Name]", applicantVacancy.ApplicantName},
                        {"[Site]", company.ATSSite + "public/joblist.aspx"},
                        {"[Status]", status.CandidateLabel},
                        {"[Title]", jobVacancy.PositionName},
                        {"[Company]", jobVacancy.CompanyName}
                    };
                    string body = Mail.EmailParser(emailTemplate.Body, dataTemplate);

                    string subject = emailTemplate
                        .Subject
                        .Replace("[Title]", jobVacancy.PositionName)
                        .Replace("[Company]", jobVacancy.CompanyName);

                    #region send email

                    Mail.SendEmail(company, applicantVacancy.ApplicantEmail, subject, body, true);

                    #endregion   
                    
                    #endregion   
                }
            }

            batchProcess.Code = Guid.NewGuid().ToString();
            batchProcess.ProcessedBy = importerCode;
            batchProcess.SchemaCode = schemaCode;
            batchProcess.Status = "Processed";

        }

        public static void ProcessSchedulingBatch(BatchProcesses batchProcess, Stream fileStream, string schemaCode, string importerCode, Dictionary<String, int> option)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];
            List<string> logger = new List<string>();
            StreamReader excelReader = new StreamReader(fileStream);
            IDictionary<string, int> columnMap = new Dictionary<string, int>();
            bool readColumnHeader = false;
            Companies company = null;
            Vacancies jobVacancy = null;
//            List<PipelineSteps> pipelineSteps = null;
//            PipelineSteps step = null;
            while (!excelReader.EndOfStream)
            {
                if (!readColumnHeader)
                {
                    excelReader.ReadLine();
                    readColumnHeader = true;
                    continue;
                }
                string readLine = excelReader.ReadLine();
                if (readLine != null)
                {
                    string[] data = readLine.Split(new string[] { delimiter }, StringSplitOptions.None);
                    if (string.IsNullOrEmpty(data[0]))
                    {
                        throw new ApplicationException("Vacancy code should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[1]))
                    {
                        throw new ApplicationException("Email should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[6]))
                    {
                        throw new ApplicationException("Invitation Type should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[7]))
                    {
                        throw new ApplicationException("Date should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[8]))
                    {
                        throw new ApplicationException("Start Time should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[9]))
                    {
                        throw new ApplicationException("End Time should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[10]))
                    {
                        throw new ApplicationException("Place should not be empty");   
                    }
                    
                    if (data[0] != batchProcess.VacancyCode)
                    {
                        throw new ApplicationException("Invalid vacancy code not match with current process");   
                    }
                
                    if (jobVacancy == null)
                    {
                        jobVacancy = ERecruitmentManager.GetVacancy(data[0]);
                    }
                    Applicants applicant = ApplicantManager.GetApplicantByEmail(data[1]);
                    
                    string pic = data[11];
                    string vacStatus = data[6];
//                    UserAccounts pic = AuthenticationManager.GetApplicantUserAccountByUserName(data[11]);
//                    VacancyStatus vacStatus = ERecruitmentManager.GetVacancyStatusByRecruiterLabel(data[2]);
                    ApplicantVacancies application = ERecruitmentManager.GetApplicantVacancies(applicant.Code, jobVacancy.Code);
//                    if (pipelineSteps == null)
//                    {
//                        pipelineSteps =
//                            HiringManager.GetPipelineStepList(jobVacancy.PipelineCode, false);
//                        step = pipelineSteps.First(a => a.StatusObj.Name.IndexOf("interview", StringComparison.OrdinalIgnoreCase) > -1);   
//                    }
//                    PipelineSteps currentStep = HiringManager.GetPipelineStep(jobVacancy.PipelineCode, vacStatus);

//                    if (pic == null)
//                        throw new ApplicationException("Invalid pic");
//
//                    if (vacStatus == null)
//                        throw new ApplicationException("Invalid invite to");
                    
                    if (application == null)
                    {
                        throw new ApplicationException("Invalid email. Applicant not registered with current job requisition");   
                    }

                    #region process object

                    ApplicantVacancySchedules applicantInvitation = new ApplicantVacancySchedules();
                    applicantInvitation.Date = Utils.ConvertFormatDateNumericToDateTime(data[7]);
                    applicantInvitation.Place = data[10];
                    applicantInvitation.InviteTo = vacStatus;
                    applicantInvitation.Notes = data[12];
                    applicantInvitation.Pic = pic;
                    string[] startTime = data[8].Split(':');
                    applicantInvitation.TimeStart = data[8];
                    applicantInvitation.StartHour = startTime[0];
                    applicantInvitation.StartMinute = startTime[1];

                    string[] endTime = data[9].Split(':');
                    applicantInvitation.TimeEnd = data[9];
                    applicantInvitation.EndHour = endTime[0];
                    applicantInvitation.EndMinute = endTime[1];

                    applicantInvitation.ApplicantCode = applicant.Code;
                    applicantInvitation.VacancyCode = jobVacancy.Code;
                    applicantInvitation.CreatedDate = DateTime.Now;
                    applicantInvitation.CreatedBy = importerCode;
                    applicantInvitation.FeedbackInvitationEmail = option["feedbackEmailTemplateId"];
                    if (applicantInvitation.Date < DateTime.Now.Date)
                    {
                        throw new Exception("Date must be greater or equal than today");
                    }

                    HiringManager.SaveSchedule(applicantInvitation);
                    #region send email

                    // get email template
                    Emails emailTemplate = ERecruitmentManager.GetEmail(option["emailTemplateId"]);
                    if (emailTemplate != null)
                    {
                        if (company == null)
                        {
                            company = session.Get<Companies>(jobVacancy.CompanyCode);   
                        }
                        string approveParam = "id=" + applicantInvitation.Id.ToString() +
                                              "&response=approve";
                        string reviseParam = "id=" + applicantInvitation.Id.ToString() +
                                              "&response=revise";
                        string rejectParam = "id=" + applicantInvitation.Id.ToString() +
                                              "&response=reject";
                        
                        string approveLink = company.ATSBaseUrl + "/applicant/confirmation.aspx?_x=" + Encriptor.Encrypt(approveParam) + "&type=schedulling";
                        string reviseLink = company.ATSBaseUrl + "/applicant/confirmation.aspx?_x=" + Encriptor.Encrypt(reviseParam) + "&type=schedulling";
                        string rejectLink = company.ATSBaseUrl + "/applicant/confirmation.aspx?_x=" + Encriptor.Encrypt(rejectParam) + "&type=schedulling";
                        string approve = @"<a href='" + approveLink + "'>Approve</a>";
                        string revise = @"<a href='" + reviseLink + "'>Reschedule</a>";
                        string reject = @"<a href='" + rejectLink + "'>Reject</a>";

                        Dictionary<String, String> dataBody = new Dictionary<string, string>
                        {
                            {"[Name]", application.ApplicantName},
                            {"[Place]", applicantInvitation.Place},
                            {"[Company]", application.CompanyName},
                            {"[Time Start]", applicantInvitation.TimeStart},
                            {"[Time End]", applicantInvitation.TimeEnd},
                            {"[PIC]", applicantInvitation.Pic},
                            {"[Notes]", applicantInvitation.Notes},
                            {"[Title]", application.VacancyPositionName},
                            {"[Date]", Utils.DisplayDateTime(applicantInvitation.Date)},
                            {"[Approve]", approve},
                            {"[Revise]", revise},
                            {"[Reject]", reject}
                        };

                        string body = Mail.EmailParser(emailTemplate.Body, dataBody);
                    
                        Mail.SendEmail(company, application.ApplicantEmail, emailTemplate.Subject, body, true);
                    
                        applicantInvitation.ApplicantStatusNotification = "EmailSent";
                        HiringManager.UpdateSchedule(applicantInvitation);
                    }
                }

                #endregion
                #endregion

            }

            batchProcess.Code = Guid.NewGuid().ToString();
            batchProcess.ProcessedBy = importerCode;
            batchProcess.SchemaCode = schemaCode;
            batchProcess.Status = "Processed";

        }

        public static void ProcessOfferingBatch(BatchProcesses batchProcess, Stream fileStream, string schemaCode, string importerCode, Dictionary<String, int> option)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];
            List<string> logger = new List<string>();
            StreamReader excelReader = new StreamReader(fileStream);
            IDictionary<string, int> columnMap = new Dictionary<string, int>();
            bool readColumnHeader = false;
            Vacancies jobVacancy = null;
            Companies company = null;
//            PipelineSteps offeringStep = null;
            String offeringCode = null;
            
            while (!excelReader.EndOfStream)
            {
                if (!readColumnHeader)
                {
                    excelReader.ReadLine();
                    readColumnHeader = true;
                    continue;
                }
                string readLine = excelReader.ReadLine();
                if (readLine != null)
                {
                    string[] data = readLine.Split(new string[] { delimiter }, StringSplitOptions.None);
                
                    if (string.IsNullOrEmpty(data[0]))
                    {
                        throw new ApplicationException("Vacancy code should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[1]))
                    {
                        throw new ApplicationException("Email should not be empty");   
                    }
                    if (string.IsNullOrEmpty(data[6]))
                    {
                        throw new ApplicationException("Monthly salary should not be empty");   
                    }
                
                    if (data[0] != batchProcess.VacancyCode)
                    {
                        throw new ApplicationException("Invalid vacancy code not match with current process");    
                    }
                
                    if (jobVacancy == null)
                    {
                        jobVacancy = ERecruitmentManager.GetVacancy(data[0]);
                        if (jobVacancy == null)
                        {
                            throw new ApplicationException("Job Vacancy not found");
                        }
                        
                        company = ERecruitmentManager.GetCompany(jobVacancy.CompanyCode);
                        if (company == null)
                        {
                            throw new ApplicationException("Company not found");
                        }
                        List<PipelineSteps> stepList = HiringManager.GetPipelineStepList(jobVacancy.PipelineCode);
                        offeringCode = stepList.Where(a => a.StatusName.IndexOf("Offering", StringComparison.OrdinalIgnoreCase) > -1).Select(a => a.StatusCode).FirstOrDefault();
                        offeringCode = String.IsNullOrEmpty(offeringCode) ? "" : offeringCode;
                        
                        if (String.IsNullOrEmpty(offeringCode))
                        {
                            throw new ApplicationException("Offering pipeline step is not found on current job vacancy");
                        }
//                        offeringStep = HiringManager.GetPipelineStep(jobVacancy.PipelineCode, offeringCode);
//                        if (offeringStep == null)
//                        {
//                            throw new ApplicationException("Offering email template not found");
//                        }
                    }
                
                    Applicants applicant = ApplicantManager.GetApplicantByEmail(data[1]);
                
                    if (applicant == null)
                    {
                        throw new ApplicationException("Invalid email. Email is not registered as applicant");
                    }
                
                    ApplicantVacancies applicantVacancy = ERecruitmentManager.GetApplicantVacancies(applicant.Code, jobVacancy.Code);

                    if (applicantVacancy == null)
                    {
                        throw new ApplicationException("Invalid email. Applicant not registered with current job requisition");
                    }

                    #region process object

                    ApplicantVacancyOffering applicantOffering = new ApplicantVacancyOffering();

                    applicantOffering.EffectiveDate = DateTime.Now;
                    applicantOffering.MonthlySalary = Utils.ConvertMoneyAmount(data[6]);
                    applicantOffering.Notes = data[10];
                    applicantOffering.ApplicantCode = applicant.Code;
                    applicantOffering.VacancyCode = jobVacancy.Code;
                    applicantOffering.CreatedDate = DateTime.Now;
                    applicantOffering.CreatedBy = importerCode;
                    string[] okAnswer =
                    {
                        "yes", "true", "y", "t", "ok", "ya"
                    };
                    applicantOffering.EligibleForTHR = !string.IsNullOrEmpty(data[7]) && Array.Exists(okAnswer, o => o.Equals(data[7], StringComparison.OrdinalIgnoreCase));
                    applicantOffering.EligibleForIncentive = !string.IsNullOrEmpty(data[8]) && Array.Exists(okAnswer, o => o.Equals(data[8], StringComparison.OrdinalIgnoreCase));
                    applicantOffering.EligibleForBonus = !string.IsNullOrEmpty(data[9]) && Array.Exists(okAnswer, o => o.Equals(data[9], StringComparison.OrdinalIgnoreCase));
                
                    Dictionary<string, string> dataMail = new Dictionary<string, string>();
                    dataMail.Add("[Name]", applicantVacancy.ApplicantName);
                    dataMail.Add("[Company]", company.Name);
                    dataMail.Add("[MonthlySalary]", Utils.DisplayMoneyAmount(applicantOffering.MonthlySalary));
                    dataMail.Add("[THR]", applicantOffering.EligibleForTHR.ToString(CultureInfo.CurrentCulture));
                    dataMail.Add("[Incentive]", applicantOffering.EligibleForIncentive.ToString(CultureInfo.CurrentCulture));
                    dataMail.Add("[Bonus]", applicantOffering.EligibleForBonus.ToString(CultureInfo.CurrentCulture));
                    dataMail.Add("[Title]", applicantVacancy.VacancyPositionName);
                    dataMail.Add("[Notes]", applicantOffering.Notes);
                    Emails emailTemplate = ERecruitmentManager.GetEmail(option["emailTemplateId"]);

                    String emailBody = Mail.EmailParser(emailTemplate.Body, dataMail);

                    #region send email

                    Mail.SendEmail(company, applicantVacancy.ApplicantEmail, emailTemplate.Subject, emailBody, true);

                    #endregion

                    HiringManager.SaveOffering(applicantOffering);
                    applicantVacancy.Status = offeringCode;
                    VacancyManager.UpdateApplicantVacancyRecruitmentStatus(applicantVacancy, offeringCode, DateTime.Now);
                }

                #endregion
            }

            batchProcess.Code = Guid.NewGuid().ToString();
            batchProcess.ProcessedBy = importerCode;
            batchProcess.SchemaCode = schemaCode;
            batchProcess.Status = "Processed";

        }

        public static void ProcessTestResultBatch(BatchProcesses batchProcess, Stream fileStream, string schemaCode, string importerCode, Dictionary<String, int> option)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];
            List<string> logger = new List<string>();
            StreamReader excelReader = new StreamReader(fileStream);
            IDictionary<string, int> columnMap = new Dictionary<string, int>();
            List<ApplicantVacancyTestResults> testResultList = new List<ApplicantVacancyTestResults>();
            bool readColumnHeader = false;
            Vacancies job = ERecruitmentManager.GetVacancy(batchProcess.VacancyCode);
            Pipelines pipeline = HiringManager.GetPipeline(job.PipelineCode);
//            List<PipelineTests> pipelineTestList = HiringManager.GetPipelineTestList(job.PipelineCode);
            List<Tests> testList = new List<Tests>();
            String[] testCode = pipeline.TestCodes.Split(',');
            int startTestColumn = 6;

            while (!excelReader.EndOfStream)
            {
                string[] data = excelReader.ReadLine().Split(new string[] { delimiter }, StringSplitOptions.None);

                if (!readColumnHeader)
                {
                    for (int i = startTestColumn; i < data.Length; i++)
                    {
                        Tests test = GetTestByName(data[i]);

                        if (Array.Exists(testCode, a => a == test.TestName))
                        {
                            throw new ApplicationException("Invalid test configuration template");
                        }

//                        // validation
//                        if (pipelineTestList[i - 6].TestCode != test.TestCode)
//                            throw new ApplicationException("Invalid test configuration template");

                        testList.Add(test);
                    }
                    readColumnHeader = true;
                    continue;
                }
                
                if (String.IsNullOrEmpty(data[0]))
                {
                    throw new ApplicationException("Vacancy code should not be empty");   
                }
                
                if (String.IsNullOrEmpty(data[1]))
                {
                    throw new ApplicationException("Email should not be empty");   
                }

                if (data[0] != batchProcess.VacancyCode)
                {
                    throw new ApplicationException("Invalid vacancy code not match with current process");   
                }
                
                Applicants applicant = ApplicantManager.GetApplicantByEmail(data[1]);

                if (applicant == null)
                {
                    throw new ApplicationException("Invalid email. Email is not registered as applicant");
                }

                ApplicantVacancies applicantVacancy =
                    ERecruitmentManager.GetApplicantVacancyByCode(job.CompanyCode, job.Code,
                        applicant.Code);

                if (applicantVacancy == null)
                {
                    throw new ApplicationException("Invalid email. Applicant not registered with current job requisition");
                }

                #region process object

                for (int i = startTestColumn; i < data.Length; i++)
                {
                    Tests test = testList[i - startTestColumn];

                    ApplicantVacancyTestResults testResult = new ApplicantVacancyTestResults();
                    testResult.TestCode = test.TestCode;
                    testResult.VacancyCode = job.Code;
                    testResult.ApplicantCode = applicant.Code;
                    testResult.Result = data[i];

                    testResultList.Add(testResult);
                }
                #endregion
            }

            foreach (ApplicantVacancyTestResults item in testResultList)
            {
                ApplicantVacancyTestResults existingItem = ERecruitmentManager.GetVacancyApplicantTest(item.ApplicantCode, item.VacancyCode, item.TestCode);
                if (existingItem != null)
                    ERecruitmentManager.DeleteVacancyApplicantTest(existingItem);
                ERecruitmentManager.SaveVacancyApplicantTest(item);
            }

            batchProcess.Code = Guid.NewGuid().ToString();
            batchProcess.ProcessedBy = importerCode;
            batchProcess.SchemaCode = schemaCode;
            batchProcess.Status = "Processed";
        }


        #region support

        public static Tests GetTestByName(string testName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Tests));
            decrit.Add(Expression.Eq("TestName", testName));
            return decrit.UniqueResult<Tests>();
        }

        #endregion

        #endregion

        #region generate schema

        public static List<string> GenerateUpdateStatusBatchTemplate(string vacancyCode, string status)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];
            if (string.IsNullOrEmpty(delimiter))
                delimiter = ";";

            List<string> columnList = new List<string>();
            columnList.Add("Vacancy Code");
            columnList.Add("Email");
            columnList.Add("Name");
            columnList.Add("Gender");
            columnList.Add("Marital Status");
            columnList.Add("Current Status");
            columnList.Add("New Status");

            List<string> dataList = new List<string>();
            dataList.Add(string.Join(delimiter, columnList.ToArray()));

            List<ApplicantVacancies> applicantVacancyList = ERecruitmentManager.GetActiveApplicantVacancyListByVacancyCode(vacancyCode, status);
            foreach (ApplicantVacancies applicant in applicantVacancyList)
            {
                VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(applicant.Status);
                List<string> cellList = new List<string>();
                cellList.Add(applicant.VacancyCode);
                cellList.Add(applicant.ApplicantEmail);
                cellList.Add(applicant.ApplicantName);
                cellList.Add(applicant.ApplicantGender);
                cellList.Add(applicant.ApplicantMaritalStatus);
                cellList.Add(getStatus.RecruiterLabel);
                cellList.Add("");
                dataList.Add(string.Join(delimiter, cellList.ToArray()));
            }

            return dataList;
        }

        public static List<string> GenerateSchedulingBatchTemplate(string vacancyCode, string status)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];

            List<string> columnList = new List<string>();
            columnList.Add("Vacancy Code");
            columnList.Add("Email");
            columnList.Add("Name");
            columnList.Add("Gender");
            columnList.Add("Marital Status");
            columnList.Add("Current Status");
            columnList.Add("Invitation Type");
            columnList.Add("Date (dd/mm/yyyy)");
            columnList.Add("Start Time (hh:mm)");
            columnList.Add("End Time (hh:mm)");
            columnList.Add("Place");
            columnList.Add("PIC");
            columnList.Add("Notes");

            List<string> dataList = new List<string>();
            dataList.Add(string.Join(delimiter, columnList.ToArray()));
            List<ApplicantVacancies> applicantVacancyList = ERecruitmentManager.GetActiveApplicantVacancyListByVacancyCode(vacancyCode, status);
            foreach (ApplicantVacancies applicant in applicantVacancyList)
            {
                VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(applicant.Status);
                List<string> cellList = new List<string>();
                cellList.Add(applicant.VacancyCode);
                cellList.Add(applicant.ApplicantEmail);
                cellList.Add(applicant.ApplicantName);
                cellList.Add(applicant.ApplicantGender);
                cellList.Add(applicant.ApplicantMaritalStatus);
                cellList.Add(getStatus.RecruiterLabel);
                cellList.Add("");
                cellList.Add("");
                cellList.Add("");
                cellList.Add("");
                cellList.Add("");
                cellList.Add("");
                cellList.Add("");
                dataList.Add(string.Join(delimiter, cellList.ToArray()));
            }

            return dataList;
        }

        public static List<string> GenerateOfferingBatchTemplate(string vacancyCode, string status)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];

            List<string> columnList = new List<string>
            {
                "Vacancy Code",
                "Email",
                "Name",
                "Gender",
                "Marital Status",
                "Current Status",
                "Monthly Salary",
                "THR",
                "Incentive",
                "Bonus",
                "Notes",
            };

            List<string> dataList = new List<string> {string.Join(delimiter, columnList.ToArray())};
            List<ApplicantVacancies> applicantVacancyList = ERecruitmentManager.GetActiveApplicantVacancyListByVacancyCode(vacancyCode, status);
            foreach (ApplicantVacancies applicant in applicantVacancyList)
            {
                VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(applicant.Status);
                List<string> cellList = new List<string>
                {
                    applicant.VacancyCode,
                    applicant.ApplicantEmail,
                    applicant.ApplicantName,
                    applicant.ApplicantGender,
                    applicant.ApplicantMaritalStatus,
                    getStatus.RecruiterLabel,
                    "",
                    "",
                    "",
                    "",
                    "",
                };
                dataList.Add(string.Join(delimiter, cellList.ToArray()));
            }
            return dataList;
        }

        public static List<string> GenerateTestResultBatchTemplate(string vacancyCode, string status)
        {
            string delimiter = ConfigurationManager.AppSettings["CsvDelimiter"];
            Vacancies job = ERecruitmentManager.GetVacancy(vacancyCode);
//            List<PipelineTests> testList = HiringManager.GetPipelineTestList(job.PipelineCode);
            Pipelines pipeline = HiringManager.GetPipeline(job.PipelineCode);

            List<string> columnList = new List<string>
            {
                "Vacancy Code",
                "Email",
                "Name",
                "Gender",
                "Marital Status",
                "Current Status"
            };
            
//            foreach (PipelineTests test in testList)
//            {
//                columnList.Add(test.TestName);
//            }
            
            String[] testCode = String.IsNullOrEmpty(pipeline.TestCodes) ? new string[]{} : pipeline.TestCodes.Split(',');
            
            foreach (String test in testCode)
            {
                Tests tests =HiringManager.GetTest(test);
                columnList.Add(tests.TestName);
            }

            List<string> dataList = new List<string>();
            dataList.Add(string.Join(delimiter, columnList.ToArray()));
            List<ApplicantVacancies> applicantVacancyList = ERecruitmentManager.GetActiveApplicantVacancyListByVacancyCode(vacancyCode, status);

            foreach (ApplicantVacancies applicant in applicantVacancyList)
            {
                VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(applicant.Status);
                List<string> cellList = new List<string>
                {
                    applicant.VacancyCode,
                    applicant.ApplicantEmail,
                    applicant.ApplicantName,
                    applicant.ApplicantGender,
                    applicant.ApplicantMaritalStatus,
                    getStatus.RecruiterLabel
                };
                
                foreach (String test in testCode)
                {
                    ApplicantVacancyTestResults applicantVacancyTestResult = ERecruitmentManager.GetVacancyApplicantTest(applicant.ApplicantCode, vacancyCode, test);
                    cellList.Add(applicantVacancyTestResult != null ? applicantVacancyTestResult.Result : "");
                }

                dataList.Add(string.Join(delimiter, cellList.ToArray()));
            }
            return dataList;
        }

        #endregion

        #region synchronization with external tables

        public static void SynchronizeData(TableSynchronizationSessions sychronizationSession)
        {
            if (sychronizationSession.SynchronizationFlow == "Incoming")
            {
                // initiate peer connection string

                // process table map
                string[] tableMapCodeList = sychronizationSession.MapCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < tableMapCodeList.Length; i++)
                {
                    // get data from peer database

                    // insert/update to internal database
                }
            }

            if (sychronizationSession.SynchronizationFlow == "Outgoing")
            {

            }
        }

        #endregion

    }
}
