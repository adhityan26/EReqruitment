﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using System.Linq;
using NHibernate.Transform;

namespace ERecruitment.Domain
{
    public class HiringManager : ERecruitmentRepositoryBase
    {
        #region update status

        public static void NotifyApplicantStatusChange(ApplicantVacancies applicantVacancy)
        {
            Vacancies vacancy = session.Get<Vacancies>(applicantVacancy.VacancyCode);
            Applicants applicant = session.Get<Applicants>(applicantVacancy.ApplicantCode);
            PipelineSteps currentStep = GetPipelineStep(vacancy.PipelineCode, applicantVacancy.Status);
            Emails emailTemplate = session.Get<Emails>(currentStep.UpdateStatusTemplateId);
            VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(applicantVacancy.Status);
            ITransaction transaction = session.BeginTransaction();
            try
            {
                // get email template
                if (emailTemplate != null)
                {
                    if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
                    {
                        SmtpClient client = new SmtpClient();
                        client.Port = Utils.ConvertString<int>(ConfigurationManager.AppSettings["MailSMTPPortNumber"]);
                        client.Host = ConfigurationManager.AppSettings["MailSMTPHostAddress"];
                        client.EnableSsl = true;
                        client.Timeout = 5000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential("no-reply@setiawan-software.net", "Bebek123");

                        MailMessage mm = new MailMessage(new MailAddress(ConfigurationManager.AppSettings["MailMailerAddress"],
                                                                         ConfigurationManager.AppSettings["MailMailerName"]),
                                                                         new MailAddress(applicant.Email, ""));
                        mm.Subject = emailTemplate.Subject;
                        mm.Body = emailTemplate.Body;
                        Companies company = session.Get<Companies>(vacancy.CompanyCode);
                        mm.Body = mm.Body.Replace("[Name]", applicant.Name).Replace("[Status]",
                                                    getStatus.CandidateLabel).Replace("[Company]", company.Name);

                        mm.BodyEncoding = UTF8Encoding.UTF8;
                        mm.IsBodyHtml = true;
                        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        client.Send(mm);
                    }
                }
            }
            catch (Exception ex)
            {
                emailTemplate.LogError = ex.Message;
                session.Update(emailTemplate);
                transaction.Commit();
            }
        }

        public static void SaveSMSMessageStatusChange(ApplicantVacancies applicantVacancy)
        {
            Vacancies vacancy = session.Get<Vacancies>(applicantVacancy.VacancyCode);
            Applicants applicant = session.Get<Applicants>(applicantVacancy.ApplicantCode);
            PipelineSteps currentStep = GetPipelineStep(vacancy.PipelineCode, applicantVacancy.Status);
            SMSTemplates templateSMS = ERecruitmentManager.GetSMSTemplate(currentStep.UpdateStatusSMSTemplateId);
            if (templateSMS != null)
            {
                SMSMessages smsMessage = new SMSMessages();
                smsMessage.Message = templateSMS.Body;
                smsMessage.MessageType = "UpdateStatus";
                smsMessage.Verified = false;
                smsMessage.Flow = "Out";
                smsMessage.LogTime = DateTime.Now;
                smsMessage.RecipientNumber = applicantVacancy.ApplicantPhone;
                MessageManager.SaveSMSMessage(smsMessage);
            }
        }

        #endregion

        #region pipeline

        public static List<Pipelines> GetPipelineList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Pipelines));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Pipelines>() as List<Pipelines>;
        }

        public static List<Pipelines> GetPipelineList(string globalSearch,
                                                                          int startRecord,
                                                                          int length,
                                                                          string sortColumn,
                                                                          string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pipelines));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<Pipelines>() as List<Pipelines>;
        }
        public static int CountPipelineList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pipelines));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static Pipelines GetPipeline(string code)
        {
            return session.Get<Pipelines>(code);
        }

        public static Pipelines SavePipeline(Pipelines obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (obj.StepList.Count == 0)
                    throw new ApplicationException("Invalid operation, pipeline must have at least one step");


                obj.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                session.Save(obj);

                List<PipelineSteps> stepList = obj.StepList.OrderBy(o => o.SortOrder).ToList();
                int getSortOrder = stepList.Max(s => s.SortOrder);

                List<string> stepTracker = new List<string>();
                foreach (PipelineSteps step in stepList)
                {
                    if (!stepTracker.Contains(step.StatusCode))
                        stepTracker.Add(step.StatusCode);
                    else
                        throw new ApplicationException("Invalid operation, duplicate step found");

                    step.PipelineCode = obj.Code;
                    session.Save(step);
                    VacancyStatus vs = ERecruitmentManager.GetVacancyStatus(step.StatusCode);
                    vs.Removable = false;
                    session.Save(vs);
                }

                if (obj.AttachmentList != null)
                {
                    foreach (PipelineAttachments attachment in obj.AttachmentList)
                    {
                        attachment.PipelineCode = obj.Code;
                        session.Save(attachment);
                    }
                }

                if (obj.TestList != null)
                {
                    foreach (PipelineTests test in obj.TestList)
                    {
                        test.PipelineCode = obj.Code;
                        session.Save(test);
                    }
                }

                transaction.Commit();

                ERecruitmentManager.UpdateRemoveableVacancyStatus();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Pipelines UpdatePipeline(Pipelines obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                #region validation

                // get all vacancies related to the pipeline, if exist then can not update
                List<Vacancies> vacancyList = ERecruitmentManager.GetOpenVacancyListByPipelineCode(obj.Code);
                
                int totalDraft = vacancyList.Count(a => a.AdvertisementStatus == "Draft");
                int totalAll = vacancyList.Count();

                if (totalDraft != totalAll)
                {
                    throw new ApplicationException("Failed to save, pipeline is already posted on job requsition");
                }
                
                if (obj.StepList.Count == 0)
                    throw new ApplicationException("Invalid operation, pipeline must have at least one step");

                #endregion

                session.Update(obj);

                List<PipelineSteps> existingSteps = GetPipelineStepList(obj.Code);
                List<PipelineTests> existingTests = GetPipelineTestList(obj.Code);
                List<PipelineAttachments> existingAttachment = GetPipelineAttachmentList(obj.Code);

                foreach (PipelineSteps step in existingSteps)
                    session.Delete(step);
                foreach (PipelineTests test in existingTests)
                    session.Delete(test);
                foreach (PipelineAttachments attachment in existingAttachment)
                    session.Delete(attachment);
                List<string> stepTracker = new List<string>();

                foreach (PipelineSteps step in obj.StepList)
                {
                    if (!stepTracker.Contains(step.StatusCode))
                        stepTracker.Add(step.StatusCode);
                    else
                        throw new ApplicationException("Invalid operation, duplicate step found");
                    step.PipelineCode = obj.Code;
                    session.Save(step);

                    VacancyStatus vs = ERecruitmentManager.GetVacancyStatus(step.StatusCode);
                    vs.Removable = false;
                    session.Save(vs);
                }

                if (obj.AttachmentList != null)
                {
                    foreach (PipelineAttachments attachment in obj.AttachmentList)
                    {
                        attachment.PipelineCode = obj.Code;
                        session.Save(attachment);
                    }
                }
                if (obj.TestList != null)
                {
                    foreach (PipelineTests test in obj.TestList)
                    {
                        test.PipelineCode = obj.Code;
                        session.Save(test);
                    }
                }

                transaction.Commit();

                ERecruitmentManager.UpdateRemoveableVacancyStatus();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Pipelines RemovePipeline(Pipelines obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Pipelines>(obj);

            ERecruitmentManager.UpdateRemoveableVacancyStatus();
            return obj;
        }

        public static PipelineSteps GetDefaultPipelineStep(string pipelineCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(PipelineSteps));
            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));
            deCrit.AddOrder(Order.Asc("SortOrder"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<PipelineSteps>();
        }

        public static List<PipelineSteps> GetPipelineStepList(string pipelineCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(PipelineSteps));
            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));
            deCrit.AddOrder(Order.Asc("SortOrder"));
            return deCrit.List<PipelineSteps>() as List<PipelineSteps>;
        }

        public static List<PipelineSteps> GetPipelineStepList(string pipelineCode, bool isShowHidden)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(PipelineSteps));
            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));
            deCrit.Add(Expression.Eq("IsHidden", isShowHidden));
            deCrit.AddOrder(Order.Asc("SortOrder"));
            return deCrit.List<PipelineSteps>() as List<PipelineSteps>;
        }

        public static List<PipelineTests> GetPipelineTestList(string pipelineCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(PipelineTests));
            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));
            return deCrit.List<PipelineTests>() as List<PipelineTests>;
        }

        public static List<Tests> GetPipelineTestListByCodes(string[] testCodes)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Tests));
            deCrit.Add(Expression.In("TestCode", testCodes));
            return deCrit.List<Tests>() as List<Tests>;
        }

        public static PipelineSteps GetPipelineStep(string pipelineCode, string statusCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(PipelineSteps));
            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));
            deCrit.Add(Expression.Eq("StatusCode", statusCode));
            return deCrit.UniqueResult<PipelineSteps>();
        }

        public static List<PipelineAttachments> GetPipelineAttachmentList(string pipelineCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(PipelineAttachments));
            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));
            return deCrit.List<PipelineAttachments>() as List<PipelineAttachments>;
        }

        #endregion

        #region schedule

        public static ApplicantVacancySchedules GetApplicantSchedule(int id)
        {
            return session.Get<ApplicantVacancySchedules>(id);
        }

        public static void NotifyApplicantSchedule(ApplicantVacancySchedules applicantSchedule)
        {
            //ApplicantVacancies applicantVacancy = ERecruitmentManager.GetApplicantVacancies(applicantSchedule.ApplicantCode, applicantSchedule.VacancyCode);
            //PipelineSteps currentStep = new PipelineSteps();//GetPipelineStep(applicantVacancy.Vacancy.PipelineCode, applicantVacancy.Status);

            //Emails emailTemplate = session.Get<Emails>(currentStep.SchedullingApplicantTemplateId);
            //ITransaction transaction = session.BeginTransaction();
            //string approveLink = ConfigurationManager.AppSettings["BaseUrl"] + "applicant/confirmation.aspx?id=" + applicantSchedule.Id.ToString() + "&response=approve&type=schedulling";
            //string reviseLink = ConfigurationManager.AppSettings["BaseUrl"] + "applicant/confirmation.aspx?id=" + applicantSchedule.Id.ToString() + "&response=revise&type=schedulling";
            //string rejectLink = ConfigurationManager.AppSettings["BaseUrl"] + "applicant/confirmation.aspx?id=" + applicantSchedule.Id.ToString() + "&response=reject&type=schedulling";
            //try
            //{
            //    // get email template
            //    string approve = @"<a href='" + approveLink + "'>approve</a>";
            //    string revise = @"<a href='" + reviseLink + "'>revise</a>";
            //    string reject = @"<a href='" + rejectLink + "'>revise</a>";
            //    if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
            //    {
            //        SmtpClient client = new SmtpClient();
            //        client.Port = Utils.ConvertString<int>(ConfigurationManager.AppSettings["MailSMTPPortNumber"]);
            //        client.Host = ConfigurationManager.AppSettings["MailSMTPHostAddress"];
            //        client.EnableSsl = true;
            //        client.Timeout = 20000;
            //        client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //        client.UseDefaultCredentials = false;
            //        client.Credentials = new NetworkCredential("no-reply@setiawan-software.net", "Bebek123");

            //        MailMessage mm = new MailMessage(new MailAddress(ConfigurationManager.AppSettings["MailMailerAddress"],
            //                                                         ConfigurationManager.AppSettings["MailMailerName"]),
            //                                                         new MailAddress(applicantVacancy.ApplicantEmail, ""));
            //        mm.Subject = emailTemplate.Subject;
            //        mm.Body = emailTemplate.Body;
            //        mm.Body = mm.Body.Replace("[name]", applicantVacancy.ApplicantName).
            //                    Replace("[Place]", applicantSchedule.Place).
            //                    Replace("[approve]", approve).Replace("[revise]", revise)
            //                    .Replace("[company name]", applicantVacancy.CompanyCode)
            //                    .Replace("[Day, Time 1]", string.Concat(Utils.DisplayDateTime(applicantSchedule.Date.Value), " ",
            //                    applicantSchedule.StartHour, ":", applicantSchedule.StartMinute));

            //        mm.BodyEncoding = UTF8Encoding.UTF8;
            //        mm.IsBodyHtml = true;
            //        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            //        client.Send(mm);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    emailTemplate.LogError = ex.Message;
            //    session.Update(emailTemplate);
            //    transaction.Commit();
            //}
        }

        public static void NotifyRecruiterSchedule(ApplicantVacancySchedules applicantSchedule)
        {
            //ApplicantVacancies applicantVacancy = ERecruitmentManager.GetApplicantVacancies(applicantSchedule.ApplicantCode, applicantSchedule.VacancyCode);
            //PipelineSteps currentStep = new PipelineSteps();//GetPipelineStep(applicantVacancy.Vacancy.PipelineCode, applicantVacancy.Status);
            //UserAccounts getUserAccount = AuthenticationManager.GetUserAccount(applicantSchedule.Pic);
            //Emails emailTemplate = session.Get<Emails>(currentStep.SchedullingRecruiterTemplateId);
            //ITransaction transaction = session.BeginTransaction();
            //try
            //{
            //    // get email template

            //    if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
            //    {
            //        SmtpClient client = new SmtpClient();
            //        client.Port = Utils.ConvertString<int>(ConfigurationManager.AppSettings["MailSMTPPortNumber"]);
            //        client.Host = ConfigurationManager.AppSettings["MailSMTPHostAddress"];
            //        client.EnableSsl = true;
            //        client.Timeout = 20000;
            //        client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //        client.UseDefaultCredentials = false;
            //        client.Credentials = new NetworkCredential("no-reply@setiawan-software.net", "Bebek123");

            //        MailMessage mm = new MailMessage(new MailAddress(ConfigurationManager.AppSettings["MailMailerAddress"],
            //                                                         ConfigurationManager.AppSettings["MailMailerName"]),
            //                                                         new MailAddress(getUserAccount.Email, ""));
            //        mm.Subject = emailTemplate.Subject;
            //        mm.Body = emailTemplate.Body;
            //        mm.Body = mm.Body.Replace("[applicantName]", applicantVacancy.ApplicantName).
            //                    Replace("[place]", applicantSchedule.Place).Replace("[date]", Utils.DisplayDateTime(applicantSchedule.Date.Value))
            //                    .Replace("[timestart]", string.Concat(applicantSchedule.StartHour, ":", applicantSchedule.StartMinute)).
            //                    Replace("[timeend]", string.Concat(applicantSchedule.EndHour, ":",
            //                    applicantSchedule.EndMinute));

            //        mm.BodyEncoding = UTF8Encoding.UTF8;
            //        mm.IsBodyHtml = true;
            //        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            //        client.Send(mm);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    emailTemplate.LogError = ex.Message;
            //    session.Update(emailTemplate);
            //    transaction.Commit();
            //}

        }

        public static int CountAllSchedulesByCompany(string companyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));
            decrit.Add(Expression.Eq("CompanyCode", companyCode));
            decrit.Add(Expression.IsNotNull("ApplicantStatusNotification"));
            decrit.SetProjection(Projections.Count("Id"));
            return decrit.UniqueResult<int>();
        }

        public static List<ApplicantVacancySchedules> GetAllSchedulesByCompany(string companyCode, int startRow, int length)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));
            deCrit.Add(Expression.Eq("CompanyCode", companyCode));
            deCrit.AddOrder(Order.Desc("Date"));
            deCrit.Add(Expression.IsNotNull("ApplicantStatusNotification"));
            deCrit.SetFirstResult(startRow);
            deCrit.SetMaxResults(length);
            return deCrit.List<ApplicantVacancySchedules>() as List<ApplicantVacancySchedules>;
        }
        
        public static int CountSchedulesSummaryByCompany(string companyCode, string filter, Dictionary<String, DateTime> dateParam)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));
            Companies company = ERecruitmentManager.GetCompany(companyCode);
            object[] listSiteCoverage = { };
            if (!String.IsNullOrEmpty(company.CompanyCodes))
            {
                listSiteCoverage = company.CompanyCodes.Split(',');
            }
            
            deCrit
                .Add(Expression.IsNotNull("ApplicantStatusNotification"))
                .Add(
                    Expression.Disjunction()
                        .Add(Expression.Eq("CompanyCode", companyCode))
                        .Add(Expression.In("CompanyCode", listSiteCoverage))
                )
                .Add(
                    Expression.Disjunction()
                        .Add(Expression.Like("InviteTo", filter))
                        .Add(Expression.Like("JobName", filter))
                        .Add(Expression.Like("Place", filter))
                );

            if (dateParam.ContainsKey("DateFrom"))
            {
                deCrit.Add(Expression.Ge("Date", dateParam["DateFrom"]));
            }

            if (dateParam.ContainsKey("DateTo"))
            {
                deCrit.Add(Expression.Le("Date", dateParam["DateTo"]));
            }
            
            deCrit
                .SetProjection(
                    Projections.ProjectionList()
                        .Add(
                            Projections.SqlProjection(
                                "count(distinct InviteTo + VacancyCode + CONVERT(varchar, Date, 101) + Place) TotalRow",
                                new[] { "TotalRow" }, 
                                new[] { NHibernateUtil.Int32 }
                            ),
                            "TotalRow"
                        )
                );
            return deCrit.UniqueResult<int>();
        }

        public static List<ApplicantVacancySchedulesSummary> GetSchedulesSummaryByCompany(string companyCode, int start, int length, string filter, Dictionary<String, String> orders, Dictionary<String, DateTime> dateParam)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));
            Companies company = ERecruitmentManager.GetCompany(companyCode);
            object[] listSiteCoverage = { };
            if (!String.IsNullOrEmpty(company.CompanyCodes))
            {
                listSiteCoverage = company.CompanyCodes.Split(',');
            }

            deCrit
                .Add(Expression.IsNotNull("ApplicantStatusNotification"))
                .Add(
                    Expression.Disjunction()
                        .Add(Expression.Eq("CompanyCode", companyCode))
                        .Add(Expression.In("CompanyCode", listSiteCoverage))
                )
                .Add(
                    Expression.Disjunction()
                        .Add(Expression.Like("InviteTo", filter))
                        .Add(Expression.Like("JobName", filter))
                        .Add(Expression.Like("Place", filter))
                );

            if (dateParam.ContainsKey("DateFrom"))
            {
                deCrit.Add(Expression.Ge("Date", dateParam["DateFrom"]));
            }

            if (dateParam.ContainsKey("DateTo"))
            {
                deCrit.Add(Expression.Le("Date", dateParam["DateTo"]));
            }
            
            deCrit
                .SetProjection(
                    Projections.ProjectionList()
                        .Add(
                            Projections.SqlProjection(
                                "(select count(1) Accepted from ApplicantVacancySchedules avs2 where this_.InviteTo = avs2.InviteTo and this_.VacancyCode = avs2.VacancyCode and this_.Date = avs2.Date and this_.Place = avs2.Place and avs2.ApplicantStatusNotification = 'approve') as Accepted",
                                new[] { "Accepted" }, 
                                new[] { NHibernateUtil.Int32 }
                            ),
                            "Accepted"
                        )
                        .Add(
                            Projections.SqlProjection(
                                "(select count(1) from ApplicantVacancySchedules avs2 where this_.InviteTo = avs2.InviteTo and this_.VacancyCode = avs2.VacancyCode and this_.Date = avs2.Date and this_.Place = avs2.Place and avs2.ApplicantStatusNotification = 'reject') as Rejected",
                                new[] { "Rejected" }, 
                                new[] { NHibernateUtil.Int32 }
                            ), "Rejected"
                        )
                        .Add(
                            Projections.SqlProjection(
                                "(select count(1) from ApplicantVacancySchedules avs2 where this_.InviteTo = avs2.InviteTo and this_.VacancyCode = avs2.VacancyCode and this_.Date = avs2.Date and this_.Place = avs2.Place and avs2.ApplicantStatusNotification = 'revise') as Reschedule",
                                new[] { "Reschedule" }, 
                                new[] { NHibernateUtil.Int32 }
                            ), "Reschedule"
                        )
                        .Add(Projections.Property("JobName"), "JobName")
                        .Add(Projections.GroupProperty("InviteTo"), "InviteTo")
                        .Add(Projections.GroupProperty("VacancyCode"), "VacancyCode")
                        .Add(Projections.GroupProperty("Date"), "Date")
                        .Add(Projections.GroupProperty("Place"), "Place")
                )
                .SetResultTransformer(Transformers.AliasToBean(typeof(ApplicantVacancySchedulesSummary)));

            foreach (var order in orders)
            {
                deCrit
                    .AddOrder(order.Value == "desc" ? Order.Desc(order.Key) : Order.Asc(order.Key));
            }
                
            deCrit
                .SetMaxResults(length)
                .SetFirstResult(start);
//            var a = deCrit.List<Object[]>();
            return deCrit.List<ApplicantVacancySchedulesSummary>() as List<ApplicantVacancySchedulesSummary>;
        }

        public static ApplicantVacancySchedules GetLatestSchedule(string inviteTo, string applicantCode, string vacancyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));
            deCrit.Add(Expression.Eq("InviteTo", inviteTo));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.AddOrder(Order.Desc("Date"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<ApplicantVacancySchedules>();
        }

        public static List<ApplicantVacancySchedules> GetScheduleList(string vacancyCode, string applicantCode, string pipeline,
                                                                      string globalSearch,
                                                                      int startRecord,
                                                                      int length,
                                                                      string sortColumn,
                                                                      string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("InviteTo", pipeline));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Place", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.AddOrder(Order.Desc("CreatedDate"));
            return decrit.List<ApplicantVacancySchedules>() as List<ApplicantVacancySchedules>;
        }


        public static int CountScheduleList(string vacancyCode, string applicantCode, string pipeline, string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("InviteTo", pipeline));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Place", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Id"));
            return decrit.UniqueResult<int>();
        }

        public static List<ApplicantVacancySchedules> GetOpenScheduleList(string picCode,
                                                                          string globalSearch,
                                                                          int startRecord,
                                                                          int length,
                                                                          string sortColumn,
                                                                          string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Place", globalSearch, MatchMode.Anywhere));
            }
            decrit.Add(Expression.Eq("Pic", picCode));
            decrit.Add(Expression.Eq("Status", "Open"));

            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            decrit.AddOrder(Order.Desc("CreatedDate"));
            return decrit.List<ApplicantVacancySchedules>() as List<ApplicantVacancySchedules>;
        }

        public static int CountOpenScheduleList(string picCode, string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancySchedules));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Place", globalSearch, MatchMode.Anywhere));
            }

            decrit.Add(Expression.Eq("Pic", picCode));
            decrit.Add(Expression.Eq("Status", "Open"));
            decrit.SetProjection(Projections.Count("Id"));
            return decrit.UniqueResult<int>();
        }

        public static ApplicantVacancySchedules GetSchedule(int id)
        {
            return session.Get<ApplicantVacancySchedules>(id);
        }

        public static ApplicantVacancySchedules SaveSchedule(ApplicantVacancySchedules obj)
        {
            obj.Status = "Open";
            obj.ApplicantStatusNotification = "NotSent";
            SaveWithTransaction<ApplicantVacancySchedules>(obj);

            return obj;
        }

        public static ApplicantVacancySchedules UpdateSchedule(ApplicantVacancySchedules obj)
        {
            UpdateWithTransaction<ApplicantVacancySchedules>(obj);
            return obj;
        }

        public static ApplicantVacancySchedules DeleteSchedule(int id)
        {
            ApplicantVacancySchedules obj = new ApplicantVacancySchedules();
            obj.Id = id;
            DeleteWithTransaction<ApplicantVacancySchedules>(obj);
            return obj;
        }

        #endregion

        #region offering

        public static ApplicantVacancyOffering GetLatestOffering(string vacancyCode, string applicantCode, string status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancyOffering));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.Add(Expression.Eq("Status", status));
            deCrit.AddOrder(Order.Desc("CreatedDate"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<ApplicantVacancyOffering>();
        }

        public static void NotifyApplicantOffering(ApplicantVacancyOffering applicantOffering)
        {
            //ApplicantVacancies applicantVacancy = ERecruitmentManager.GetApplicantVacancies(applicantOffering.ApplicantCode, applicantOffering.VacancyCode);
            //PipelineSteps currentStep = new PipelineSteps();//GetPipelineStep(applicantVacancy.Vacancy.PipelineCode, applicantVacancy.Status);

            //Emails emailTemplate = session.Get<Emails>(currentStep.OfferingApplicantTemplateId);
            //string approveLink = ConfigurationManager.AppSettings["BaseUrl"] + "applicant/confirmation.aspx?id=" + applicantOffering.Id.ToString() + "&response=approve&type=offering";
            //string reviseLink = ConfigurationManager.AppSettings["BaseUrl"] + "applicant/confirmation.aspx?id=" + applicantOffering.Id.ToString() + "&response=revise&type=offering";
            //string rejectLink = ConfigurationManager.AppSettings["BaseUrl"] + "applicant/confirmation.aspx?id=" + applicantOffering.Id.ToString() + "&response=reject&type=offering";
            //ITransaction transaction = session.BeginTransaction();
            //try
            //{
            //    // get email template
            //    string approve = @"<a href='" + approveLink + "'>approve</a>";
            //    string revise = @"<a href='" + reviseLink + "'>revise</a>";
            //    string reject = @"<a href='" + rejectLink + "'>revise</a>";
            //    if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
            //    {
            //        SmtpClient client = new SmtpClient();
            //        client.Port = Utils.ConvertString<int>(ConfigurationManager.AppSettings["MailSMTPPortNumber"]);
            //        client.Host = ConfigurationManager.AppSettings["MailSMTPHostAddress"];
            //        client.EnableSsl = true;
            //        client.Timeout = 20000;
            //        client.DeliveryMethod = SmtpDeliveryMethod.Network;
            //        client.UseDefaultCredentials = false;
            //        client.Credentials = new NetworkCredential("no-reply@setiawan-software.net", "Bebek123");

            //        MailMessage mm = new MailMessage(new MailAddress(ConfigurationManager.AppSettings["MailMailerAddress"],
            //                                                         ConfigurationManager.AppSettings["MailMailerName"]),
            //                                                         new MailAddress(applicantVacancy.ApplicantEmail, ""));
            //        mm.Subject = emailTemplate.Subject;
            //        mm.Body = emailTemplate.Body;
            //        mm.Body = mm.Body.Replace("[name]", applicantVacancy.ApplicantName).
            //            Replace("[job title]", applicantOffering.Vacancy.Subject).
            //            Replace("[$X]", Utils.DisplayMoneyAmount(applicantOffering.AnnualSalary.Value))
            //            .Replace("[number of days]", applicantOffering.AnnualLeave.Value.ToString()).Replace("[approve]", approve)
            //            .Replace("[reject]", reject).Replace("[revise]", revise);


            //        mm.BodyEncoding = UTF8Encoding.UTF8;
            //        mm.IsBodyHtml = true;
            //        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            //        client.Send(mm);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    emailTemplate.LogError = ex.Message;
            //    session.Update(emailTemplate);
            //    transaction.Commit();
            //}
        }

        public static ApplicantVacancyOffering GetOffering(int id)
        {
            return session.Get<ApplicantVacancyOffering>(id);

        }
        public static ApplicantVacancyOffering SaveOffering(ApplicantVacancyOffering obj)
        {
            if (string.IsNullOrEmpty(obj.ApplicantStatusNotification))
                obj.ApplicantStatusNotification = "NotSent";
            SaveWithTransaction<ApplicantVacancyOffering>(obj);

            return obj;
        }
        public static ApplicantVacancyOffering UpdateOffering(ApplicantVacancyOffering obj)
        {
            UpdateWithTransaction<ApplicantVacancyOffering>(obj);
            return obj;
        }
        public static List<ApplicantVacancyOffering> GetOfferingList(string vacancyCode, string applicantCode, string pipeline,
                                                                      string globalSearch, int startRecord, int length,
                                                                       string sortColumn, string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyOffering));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            //decrit.Add(Expression.Eq("Status", pipeline));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("CreatedBy", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.AddOrder(Order.Desc("CreatedDate"));
            return decrit.List<ApplicantVacancyOffering>() as List<ApplicantVacancyOffering>;
        }

        public static int CountOfferingList(string vacancyCode, string applicantCode, string pipeline,
                                                                      string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyOffering));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            //decrit.Add(Expression.Eq("Status", pipeline));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("CreatedBy", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Id"));
            return decrit.UniqueResult<int>();
        }


        #endregion

        #region comment
        public static ApplicantVacancyComments GetComment(string appCode, string vacancyCode, string status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancyComments));
            deCrit.Add(Expression.Eq("ApplicantCode", appCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.Add(Expression.Eq("Status", status));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<ApplicantVacancyComments>();
        }
        public static List<ApplicantVacancyComments> GetComment(string appCode, string vacancyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancyComments));
            deCrit.Add(Expression.Eq("ApplicantCode", appCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.AddOrder(Order.Desc("Date"));
            return deCrit.List<ApplicantVacancyComments>() as List<ApplicantVacancyComments>;
        }

        public static ApplicantVacancyComments SaveComment(ApplicantVacancyComments obj)
        {
            SaveWithTransaction<ApplicantVacancyComments>(obj);
            return obj;
        }

        public static ApplicantVacancyComments UpdateComment(ApplicantVacancyComments obj)
        {
            UpdateWithTransaction<ApplicantVacancyComments>(obj);
            return obj;
        }
        #endregion

        #region statistics

        public static int CountVacancyApplicantStatus(string vacancyCode, string statusCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(statusCode))
                decrit.Add(Expression.Eq("Status", statusCode));
            decrit.SetProjection(Projections.Count("ApplicantCode"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyApplicant(string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.SetProjection(Projections.Count("ApplicantCode"));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region test

        public static List<ApplicantVacancyTestResults> GetTestResult(string applicantCode, string vacancyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancyTestResults));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            return deCrit.List<ApplicantVacancyTestResults>() as List<ApplicantVacancyTestResults>;
        }
        public static ApplicantVacancyTestResults GetTestResult(string applicantCode, string vacancyCode, string testCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancyTestResults));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.Add(Expression.Eq("TestCode", testCode));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<ApplicantVacancyTestResults>();
        }
        public static int CountTestResult(string applicantCode, string vacancyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancyTestResults));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.SetProjection(Projections.Count("ApplicantCode"));
            return deCrit.UniqueResult<int>();
        }

        public static Tests SaveTest(Tests obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (string.IsNullOrEmpty(obj.TestCode))
                    obj.TestCode = Guid.NewGuid().ToString();
                session.Save(obj);
                foreach (TestQuestions question in obj.QuestionList)
                {
                    question.TestCode = obj.TestCode;
                    session.Save(question);
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
            }
            return obj;
        }

        public static Tests UpdateTest(Tests obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Update(obj);
                List<TestQuestions> existingQuestionList = AssestmentManager.GetTestQuestionList(obj.TestCode);
                foreach (TestQuestions question in existingQuestionList)
                    session.Delete(question);
                foreach (TestQuestions question in obj.QuestionList)
                {
                    question.TestCode = obj.TestCode;
                    session.Save(question);
                }
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
            }
            return obj;
        }

        public static Tests GetTest(string code)
        {
            return session.Get<Tests>(code);
        }
        public static Tests RemoveTest(Tests obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Tests>(obj);
            return obj;
        }
        public static List<Tests> GetTestList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Tests));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Tests>() as List<Tests>;
        }

        public static List<Tests> GetTestList(string globalSearch, int startRecord,
                                                int length, string sortColumn,
                                                 string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Tests));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("TestName", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            return decrit.List<Tests>() as List<Tests>;
        }
        public static int CountTestList(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Tests));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.SetProjection(Projections.Count("TestCode"));
            return deCrit.UniqueResult<int>();
        }

        public static TestAttachments GetTestAttachment(string code)
        {
            return session.Get<TestAttachments>(code);
        }
        public static TestAttachments SaveTestAttachment(TestAttachments obj)
        {
            SaveWithTransaction<TestAttachments>(obj);
            return obj;
        }
        public static TestAttachments UpdateTestAttachment(TestAttachments obj)
        {
            UpdateWithTransaction<TestAttachments>(obj);
            return obj;
        }
        public static TestAttachments RemoveTestAttachment(TestAttachments obj)
        {
            obj.Active = false;
            UpdateWithTransaction<TestAttachments>(obj);
            return obj;
        }
        public static List<TestAttachments> GetTestAttachment()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(TestAttachments));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<TestAttachments>() as List<TestAttachments>;
        }
        public static List<TestAttachments> GetTestAttachment(string globalSearch, int startRecord,
                                                            int length, string sortColumn,
                                                                string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(TestAttachments));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            return decrit.List<TestAttachments>() as List<TestAttachments>;
        }

        public static int CountTestAttachment(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(TestAttachments));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.SetProjection(Projections.Count("AttachmentCode"));
            return deCrit.UniqueResult<int>();
        }
        #endregion
    }
}
