﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using System.Reflection;


namespace ERecruitment.Domain
{
    public class VacancyManager : ERecruitmentRepositoryBase
    {
        public static List<string> GetAllVacancyCodeList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.SetProjection(Projections.Property("Code"));
            return decrit.List<string>() as List<string>;
        }

        public static VacanciesSAPJobPosition GetVacanciesJobSAPPositionId(string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacanciesSAPJobPosition));
            decrit.Add(Expression.Eq("VacanciesCode", vacancyCode));
            decrit.Add(Expression.Eq("Status", "Vacant"));
            decrit.AddOrder(Order.Asc("SAPJobPositionID"));
            decrit.SetMaxResults(1);

            return decrit.UniqueResult<VacanciesSAPJobPosition>();
        }

        public static List<VacanciesSAPJobPosition> GetSAPPositionId(string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacanciesSAPJobPosition));
            decrit.Add(Expression.Eq("VacanciesCode", vacancyCode));
            decrit.Add(Expression.Eq("Status", "Vacant"));
            decrit.Add(Expression.IsNull("ApplicantCode"));
            decrit.AddOrder(Order.Asc("SAPJobPositionID"));

            return decrit.List<VacanciesSAPJobPosition>() as List<VacanciesSAPJobPosition>;
        }


        public static void SaveUpdateVacanciesJobSAPPositionId(VacanciesSAPJobPosition sapJob)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.SaveOrUpdate(sapJob);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }


        #region vacancy applicants

        public static void SaveApplicantVacancy(ApplicantVacancies application)
        {
            SaveApplicantVacancy(application, null);
        }
        
        public static void SaveApplicantVacancy(ApplicantVacancies application, ITransaction tr)
        {
            ITransaction transaction;

            if (tr == null)
            {
                transaction = session.BeginTransaction();
            }
            else
            {
                transaction = tr;
            }
            
            try
            {
                // set initial pipeline process

                bool disqualify = false;
                foreach (ApplicantVacancyDisqualificationAnswers disqualifierAnswer in application.DisqualifierAnswerList)
                {
                    session.Save(disqualifierAnswer);
                    DisqualifierQuestions disqQuestion = session.Get<DisqualifierQuestions>(disqualifierAnswer.QuestionCode);
                    if (disqQuestion.AnswerFormat == "MultipleChoice")
                    {
                        if (disqQuestion.CorrectAnswerLabel != disqualifierAnswer.Answer)
                            disqualify = true;
                    }
                    else
                    {
                        int numericAnswer = Convert.ToInt32(disqualifierAnswer.Answer);
                        int correctAnswer = Convert.ToInt32(disqQuestion.CorrectAnswerLabel);
                        switch (disqQuestion.CorrectAnswerOperator)
                        {
                            case ">":
                                if (!(numericAnswer > correctAnswer))
                                    disqualify = true;
                                break;
                            case "<":
                                if (!(numericAnswer < correctAnswer))
                                    disqualify = true;
                                break;
                            case ">=":
                                if (!(numericAnswer >= correctAnswer))
                                    disqualify = true;
                                break;
                            case "<=":
                                if (!(numericAnswer <= correctAnswer))
                                    disqualify = true;
                                break;
                            case "==":
                                if (!(numericAnswer == correctAnswer))
                                    disqualify = true;
                                break;
                            case "!=":
                                if (!(numericAnswer != correctAnswer))
                                    disqualify = true;
                                break;
                        }
                    }
                }

                ApplicationSettings applicationSetting = ERecruitmentManager.GetApplicationSetting();
                if (disqualify)
                {
                    Vacancies vacancy = ERecruitmentManager.GetVacancy(application.VacancyCode);
                    Pipelines pipeline = HiringManager.GetPipeline(vacancy.PipelineCode);
                    // disqualify application
                    application.Status = pipeline.AutoDisqualifiedRecruitmentStatusCode;
                }
                else
                {
                    Vacancies vacancy = session.Get<Vacancies>(application.VacancyCode);
                    List<PipelineSteps> pipelineList = VacancyManager.GetPipelineStepList(vacancy.PipelineCode);
                    application.Status = pipelineList[0].StatusCode;
                }

                session.Save(application);

                //add log
                ApplicantsVacancyMilestoneLogs addLogs = new ApplicantsVacancyMilestoneLogs();
                addLogs.ApplicantCode = application.ApplicantCode;
                addLogs.VacancyCode = application.VacancyCode;
                addLogs.Status = application.Status;
                addLogs.Date = DateTime.Now;
                session.Save(addLogs);

                if (tr == null)
                {
                    transaction.Commit();   
                }

                session.Evict(application);
                session.Flush();
            }
            catch (Exception e)
            {
                if (tr == null)
                {
                    transaction.Rollback();
                }
                throw e;
            }
        }

        public static void UpdateApplicantVacancyByFlagApplicant(ApplicantVacancies applicantVacancy, string statusCode, DateTime date)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                applicantVacancy.Status = statusCode;
                applicantVacancy.UpdateStamp = DateTime.Now;
                session.Update(applicantVacancy);

                //add log
                ApplicantsVacancyMilestoneLogs addLogs = new ApplicantsVacancyMilestoneLogs();
                addLogs.ApplicantCode = applicantVacancy.ApplicantCode;
                addLogs.VacancyCode = applicantVacancy.VacancyCode;
                addLogs.Status = statusCode;
                addLogs.Date = DateTime.Now;
                session.Save(addLogs);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void UpdateApplicantVacancyRecruitmentStatus(ApplicantVacancies applicantVacancy, string statusCode, DateTime date)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                #region validation

                Vacancies vacancy = session.Get<Vacancies>(applicantVacancy.VacancyCode);
                Pipelines pipeline = session.Get<Pipelines>(vacancy.PipelineCode);

                if (statusCode == pipeline.AutoDisqualifiedRecruitmentStatusCode || statusCode == pipeline.ReferedDisqualifiedRecruitmentStatusCode
                    || statusCode == pipeline.HiredRecruitmentStatusCode || statusCode == pipeline.DisqualifiedRecruitmentStatusCode)
                {
                    applicantVacancy.Status = statusCode;
                    applicantVacancy.UpdateStamp = DateTime.Now;
                    session.Update(applicantVacancy);
                }
                else
                { 
                    List<PipelineSteps> pipelineStepList = HiringManager.GetPipelineStepList(vacancy.PipelineCode);
                    pipelineStepList.Add(new PipelineSteps(pipeline.AutoDisqualifiedRecruitmentStatusCode));
                    pipelineStepList.Add(new PipelineSteps(pipeline.ReferedDisqualifiedRecruitmentStatusCode));
                    pipelineStepList.Add(new PipelineSteps(pipeline.HiredRecruitmentStatusCode));
                    pipelineStepList.Add(new PipelineSteps(pipeline.DisqualifiedRecruitmentStatusCode));


                    PipelineSteps targetStep = pipelineStepList.Find(delegate (PipelineSteps temp) { return temp.StatusCode == statusCode; });
                    if (targetStep == null)
                    {
                        throw new ApplicationException("Invalid status. Status is not registered with current pipeline");   
                    }

                    PipelineSteps currentStep = pipelineStepList.Find(delegate (PipelineSteps temp) { return temp.StatusCode == applicantVacancy.Status; });

                    if (currentStep.SortOrder > targetStep.SortOrder)
                    {
                        throw new ApplicationException("Invalid status. status cannot be backward");   
                    }

                    applicantVacancy.Status = statusCode;
                    applicantVacancy.UpdateStamp = DateTime.Now;
                    session.Update(applicantVacancy);
                }
                #endregion

                //add log
                ApplicantsVacancyMilestoneLogs addLogs = new ApplicantsVacancyMilestoneLogs();
                addLogs.ApplicantCode = applicantVacancy.ApplicantCode;
                addLogs.VacancyCode = applicantVacancy.VacancyCode;
                addLogs.Status = statusCode;
                addLogs.Date = DateTime.Now;
                session.Save(addLogs);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public static List<ApplicantVacancies> GetApplicantVacancyListByVacancyCode(string vacancyCode,
                                                                                    string genderSpecification,
                                                                                    string maritalStatus,
                                                                                    string[] status,
                                                                                    bool rankedOnly)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(genderSpecification))
                decrit.Add(Expression.Eq("ApplicantGender", genderSpecification));
            if (!string.IsNullOrEmpty(maritalStatus))
                decrit.Add(Expression.Eq("ApplicantMaritalStatus", maritalStatus));

            decrit.Add(Expression.In("Status", status));


            if (rankedOnly)
                decrit.Add(Expression.Eq("Ranked", true));

            decrit.AddOrder(Order.Desc("Ranked"));
            decrit.AddOrder(Order.Desc("RankWeight"));

            List<ApplicantVacancies> list = decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
            List<ApplicantVacancies> tmp = new List<ApplicantVacancies>();
            int rank = 1;
            double weight = 0;
            string display = "style='display:none'";
            foreach (ApplicantVacancies av in list) {
                if (av.RankWeight > weight) {
                    weight = av.RankWeight;
                    av.RankNumber = rank;
                } else if (av.RankWeight == weight) {
                    av.RankNumber = rank;
                } else if (av.RankWeight < weight) {
                    rank = rank + 1;
                    weight = av.RankWeight;
                    av.RankNumber = rank;
                }

                av.IsGreenFlag = "";
                av.IsBlackFlag = "";
                av.IsAppointment = "";
                av.IsComments = "";
                av.IsTestResult = "";
                av.IsReject = "";
                av.IsOffer = "";
                av.IsRefer = "";
                av.IsHire = "";
                av.IsProcessed = "";

                if ("Rejected".Equals(av.Status))
                {
                    List<ApplicantVacancyReferences> referenceList = ERecruitmentManager.GetApplicantsVacancyReferences(av.ApplicantCode);
                    if (referenceList != null && referenceList.Count > 0) {
                        av.IsAppointment = display;
                        av.IsComments = display;
                        av.IsTestResult = display;
                        av.IsReject = display;
                        av.IsOffer = display;
                        av.IsRefer = display;
                        av.IsHire = display;
                        av.IsProcessed = display;
                    }

                    List<ApplicantFlagStatuses> flagList = ERecruitmentManager.GetApplicantFlagStatuses(av.ApplicantCode, "GreenList");
                    if (flagList != null && flagList.Count > 0)
                    {
                        av.IsGreenFlag = display;
                        av.IsBlackFlag = display;
                        av.IsAppointment = display;
                        av.IsComments = display;
                        av.IsTestResult = display;
                        av.IsReject = display;
                        av.IsOffer = display;
                        av.IsHire = display;
                        av.IsProcessed = display;
                    }

                    if ((flagList != null && flagList.Count > 0) || (referenceList != null && referenceList.Count > 0))
                    {

                    }
                    else {
                        av.IsGreenFlag = display;
                        av.IsBlackFlag = display;
                        av.IsAppointment = display;
                        av.IsComments = display;
                        av.IsTestResult = display;
                        av.IsReject = display;
                        av.IsOffer = display;
                        av.IsRefer = display;
                        av.IsHire = display;
                        av.IsProcessed = display;
                    }
                    
                }
                
                if ("Hired".Equals(av.Status) || "1e48c7f0".Equals(av.Status))
                {
                    av.IsGreenFlag = display;
                    av.IsBlackFlag = display;
                    av.IsAppointment = display;
                    av.IsComments = display;
                    av.IsTestResult = display;
                    av.IsReject = display;
                    av.IsOffer = display;
                    av.IsRefer = display;
                    av.IsHire = display;
                    av.IsProcessed = display;
                }
            }

            return list;
        }

        public static List<ApplicantVacancies> GetApplicantVacancyListByVacancyCode(string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            return decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static ApplicantVacancies GetApplicantVacancy(string vacancyCode, string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            return decrit.UniqueResult<ApplicantVacancies>();
        }

        public static void ReferApplicantVacancy(ApplicantVacancies applicantVacancy, string[] listCompany, string commentNotes)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (listCompany.Length == 0)
                {
                    throw new ApplicationException("Refer Company could not be empty");
                }
                
                if (Array.Exists(listCompany, c => c.Equals(applicantVacancy.CompanyCode)))
                {
                    throw new ApplicationException("Invalid operation, could not refer to the same company");   
                }
                Vacancies vacancy = ERecruitmentManager.GetVacancy(applicantVacancy.VacancyCode);
                Pipelines pipeline = HiringManager.GetPipeline(vacancy.PipelineCode);

                applicantVacancy.Active = false;
                applicantVacancy.Status = pipeline.DisqualifiedRecruitmentStatusCode;
                session.Update(applicantVacancy);

                ApplicantVacancyReferences referencial = new ApplicantVacancyReferences();
                referencial.AppliedDate = applicantVacancy.AppliedDate;
                referencial.OriginCompany = applicantVacancy.CompanyCode;
                referencial.VacancyCode = applicantVacancy.VacancyCode;
                referencial.ApplicantCode = applicantVacancy.ApplicantCode;
                referencial.IssuedDate = DateTime.Now;
                referencial.IssuedByCode = applicantVacancy.UpdatedBy;
                referencial.CompanyReferenceCode = String.Join(", ", listCompany);
                referencial.Notes = commentNotes;

                session.Save(referencial);

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void SaveSurvay(ApplicantVacancies application)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                IQuery query = session.CreateSQLQuery("UPDATE ApplicantVacancies set SourceReferencesName = :reference  WHERE ApplicantCode = :apl AND VacancyCode = :vac");
                query.SetParameter("reference", application.SourceReferencesName);
                query.SetParameter("apl", application.ApplicantCode);
                query.SetParameter("vac", application.VacancyCode);
                int result = query.ExecuteUpdate();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region vacancy comments

        public static List<ApplicantVacancyComments> GetVacancyApplicantCommentList(string vacancyCode, string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyComments));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            decrit.AddOrder(Order.Desc("Date"));

            return decrit.List<ApplicantVacancyComments>() as List<ApplicantVacancyComments>;
        }

        public static void SaveUpdateApplicantComment(ApplicantVacancyComments comment)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.SaveOrUpdate(comment);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region vacancy attachments


        public static void InitiateApplicantAttachmentFiles(string vacancyCode, string applicantCode)
        {
            Vacancies vacancy = session.Get<Vacancies>(vacancyCode);
            Pipelines pipeline = session.Get<Pipelines>(vacancy.PipelineCode);
            if (!string.IsNullOrEmpty(pipeline.AttachmentCodes))
            {
                string[] attachmentCodeList = pipeline.AttachmentCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < attachmentCodeList.Length; i++)
                {
                    ApplicantVacancyAttachmentFiles applicantAttachment = GetApplicantAttachment(vacancyCode, applicantCode, attachmentCodeList[i]);
                    if (applicantAttachment == null)
                    {
                        applicantAttachment = new ApplicantVacancyAttachmentFiles();
                        applicantAttachment.AttachmentCode = attachmentCodeList[i];
                        applicantAttachment.VacancyCode = vacancyCode;
                        applicantAttachment.ApplicantCode = applicantCode;
                        SaveUpdateApplicantAttachmentFile(applicantAttachment);
                    }
                }
            }
        }
        public static ApplicantVacancyAttachmentFiles GetApplicantAttachment(string vacancyCode, string applicantCode, string attachmentCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyAttachmentFiles));

            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("AttachmentCode", attachmentCode));

            return decrit.UniqueResult<ApplicantVacancyAttachmentFiles>();
        }


        public static List<ApplicantVacancyAttachmentFiles> GetVacancyApplicantAttachmentFileList(string vacancyCode, string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyAttachmentFiles));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            decrit.AddOrder(Order.Desc("Date"));

            return decrit.List<ApplicantVacancyAttachmentFiles>() as List<ApplicantVacancyAttachmentFiles>;
        }

        public static void SaveUpdateApplicantAttachmentFile(ApplicantVacancyAttachmentFiles attachmentFile)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.SaveOrUpdate(attachmentFile);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region applicant test

        public static void InitiateApplicantTest(string vacancyCode, string applicantCode)
        {
            Vacancies vacancy = session.Get<Vacancies>(vacancyCode);
            Pipelines pipeline = session.Get<Pipelines>(vacancy.PipelineCode);
            if (!string.IsNullOrEmpty(pipeline.TestCodes))
            {
                string[] testCodeList = pipeline.TestCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < testCodeList.Length; i++)
                {
                    ApplicantVacancyTestResults applicantTest = GetApplicantTestResult(vacancyCode, applicantCode, testCodeList[i]);
                    if (applicantTest == null)
                    {
                        applicantTest = new ApplicantVacancyTestResults();
                        applicantTest.TestCode = testCodeList[i];
                        applicantTest.VacancyCode = vacancyCode;
                        applicantTest.ApplicantCode = applicantCode;
                        applicantTest.IsNew = true;
                        SaveUpdateApplicantTestResult(applicantTest);
                    }
                }
            }
        }

        public static void SaveUpdateApplicantTestResult(ApplicantVacancyTestResults testResult)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (testResult.IsNew)
                    session.Save(testResult);
                else
                    session.Update(testResult);

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static ApplicantVacancyTestResults GetApplicantTestResult(string vacancyCode, string applicantCode, string testCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyTestResults));

            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("TestCode", testCode));

            return decrit.UniqueResult<ApplicantVacancyTestResults>();
        }

        public static List<ApplicantVacancyTestResults> GetApplicantTestResultList(string vacancyCode, string applicantCode)
        {

            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyTestResults));

            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            return decrit.List<ApplicantVacancyTestResults>() as List<ApplicantVacancyTestResults>;
        }

        #endregion

        #region vacancy offering

        public static List<ApplicantVacancyOffering> GetVacancyApplicantOfferingList(string vacancyCode, string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyOffering));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            decrit.AddOrder(Order.Desc("Date"));

            return decrit.List<ApplicantVacancyOffering>() as List<ApplicantVacancyOffering>;
        }

        public static void SaveUpdateApplicantOffering(ApplicantVacancyOffering attachmentFile)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.SaveOrUpdate(attachmentFile);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region invitation

        public static void SaveUpdateApplicantInvitation(ApplicantVacancySchedules invitation)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.SaveOrUpdate(invitation);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region ranking

        public static void UpdateVacancyRank(Vacancies vacancy)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Update(vacancy);

                List<ApplicantVacancies> applicationList = VacancyManager.GetApplicantVacancyListByVacancyCode(vacancy.Code);

                foreach (ApplicantVacancies application in applicationList)
                {
                    if (!application.Active)
                    {
                        application.Ranked = false;
                        application.RankNumber = 0;
                        application.RankWeight = 0;
                        session.Update(application);
                    }
                    else
                    {
                        Applicants applicant = session.Get<Applicants>(application.ApplicantCode);

                        #region evaluation experience years

                        if (vacancy.RankQualificationExperienceMinimumYearWeight > 0)
                        {

                            List<ApplicantExperiences> experienceList = ERecruitmentManager.GetApplicantExperienceList(application.ApplicantCode);
                            int totalExperienceYear = 0;
                            foreach (ApplicantExperiences experience in experienceList)
                            {
                                if (experience.StillUntillNow)
                                    totalExperienceYear += (DateTime.Now.Year - experience.StartYear);
                                else
                                    totalExperienceYear += (experience.EndYear - experience.StartYear);
                            }

                            application.RankQualificationExperienceMinimumYear = totalExperienceYear;
                            if (totalExperienceYear >= vacancy.RankQualificationExperienceMinimumYear)
                            {
                                application.RankQualificationExperienceMinimumYearWeight = vacancy.RankQualificationExperienceMinimumYearWeight;
                            }
                            else {
                                application.RankQualificationExperienceMinimumYearWeight = 0;
                            }

                        }
                        else {
                            application.RankQualificationExperienceMinimumYearWeight = 0;
                        }

                        #endregion

                        #region evaluate education level

                        List<ApplicantEducations> educationList = ERecruitmentManager.GetApplicantEducationList(application.ApplicantCode);

                        if (vacancy.RankQualificationMinimumEducationLevelCodeWeight > 0)
                        {


                            foreach (ApplicantEducations education in educationList) {
                                if (education.LevelCode == vacancy.RankQualificationMinimumEducationLevelCode)
                                {
                                    application.RankQualificationMinimumEducationLevelCodeWeight = vacancy.RankQualificationMinimumEducationLevelCodeWeight;
                                    break;
                                }
                                else {
                                    application.RankQualificationMinimumEducationLevelCodeWeight = 0;
                                }
                            }

                        }
                        else {
                            application.RankQualificationMinimumEducationLevelCodeWeight = 0;
                        }

                        #endregion

                        #region evaluate education field

                        if (vacancy.RankQualificationEducationFieldCodeWeight > 0)
                        {

                            foreach (ApplicantEducations education in educationList)
                            {
                                if (education.FieldOfStudyCode == vacancy.RankQualificationEducationFieldCode)
                                {
                                    application.RankQualificationEducationFieldCodeWeight = vacancy.RankQualificationEducationFieldCodeWeight;
                                    break;
                                }
                                else {
                                    application.RankQualificationEducationFieldCodeWeight = 0;
                                }
                            }

                        }
                        else {
                            application.RankQualificationEducationFieldCodeWeight = 0;
                        }

                        #endregion

                        #region evaluate gpa

                        if (vacancy.RankQualificationEducationGpaRangeWeight > 0)
                        {

                            foreach (ApplicantEducations education in educationList)
                            {
                                double gpaScore = education.Score;

                                if (gpaScore >= vacancy.RankQualificationEducationGpaRangeBottom && gpaScore <= vacancy.RankQualificationEducationGpaRangeTop)
                                {
                                    application.RankQualificationEducationGpaRangeWeight = vacancy.RankQualificationEducationGpaRangeWeight;
                                    break;
                                }
                                else {
                                    application.RankQualificationEducationGpaRangeWeight = 0;
                                }
                                   
                            }
                        }
                        else {
                            application.RankQualificationEducationGpaRangeWeight = 0;
                        }

                        #endregion

                        #region evaluate education location

                        if (vacancy.RankQualificationEducationLocationCodeWeight > 0)
                        {

                            foreach (ApplicantEducations education in educationList) {
                                if (education.EducationCityCode == vacancy.RankQualificationEducationLocationCode)
                                {
                                    application.RankQualificationEducationLocationCodeWeight = vacancy.RankQualificationEducationLocationCodeWeight;
                                    break;
                                }
                                else {
                                    application.RankQualificationEducationLocationCodeWeight = 0;
                                }
                            }
                                   

                        }
                        else {
                            application.RankQualificationEducationLocationCodeWeight = 0;
                        }

                        #endregion

                        #region residence

                        if (vacancy.RankQualificationResidenceLocationCodeWeight > 0)
                        {

                            if (applicant.CurrentCardCity == vacancy.RankQualificationResidenceLocationCode)
                            {
                                application.RankQualificationResidenceLocationCodeWeight = vacancy.RankQualificationResidenceLocationCodeWeight;
                            }
                            else {
                                application.RankQualificationResidenceLocationCodeWeight = 0;
                            }
                                
                        }
                        else {
                            application.RankQualificationResidenceLocationCodeWeight = 0;
                        }

                        #endregion

                        #region salary

                        if (vacancy.RankQualificationSalaryRangeWeight > 0)
                        {

                            decimal expectedSalary = application.ApplicantExpectedSalary;

                            if (expectedSalary >= vacancy.RankQualificationSalaryRangeBottom && expectedSalary <= vacancy.RankQualificationSalaryRangeTop)
                            {
                                application.RankQualificationSalaryRangeWeight = vacancy.RankQualificationSalaryRangeWeight;
                            }
                            else {
                                application.RankQualificationSalaryRangeWeight = 0;
                            }
                               
                        }
                        else {
                            application.RankQualificationSalaryRangeWeight = 0;
                        }

                        #endregion

                        #region age

                        if (vacancy.RankQualificationAgeRangeWeight > 0)
                        {

                            if (application.ApplicantAge >= vacancy.RankQualificationAgeRangeBottom && application.ApplicantAge <= vacancy.RankQualificationAgeRangeTop)
                            {
                                application.RankQualificationAgeRangeWeight = vacancy.RankQualificationAgeRangeWeight;
                            }
                            else {
                                application.RankQualificationAgeRangeWeight = 0;
                            }
                        }
                        else {
                            application.RankQualificationAgeRangeWeight = 0;
                        }

                        #endregion

                        application.RankWeight = application.RankQualificationExperienceMinimumYearWeight +
                                                 application.RankQualificationMinimumEducationLevelCodeWeight +
                                                 application.RankQualificationEducationFieldCodeWeight +
                                                 application.RankQualificationEducationLocationCodeWeight +
                                                 application.RankQualificationResidenceLocationCodeWeight +
                                                 application.RankQualificationSalaryRangeWeight +
                                                 application.RankQualificationAgeRangeWeight +
                                                 application.RankQualificationEducationGpaRangeWeight;

                        if (application.RankWeight > 0)
                            application.Ranked = true;
                        else
                            application.Ranked = false;
                    }
                }

                // sorting by rank
                applicationList.Sort(
                    delegate (ApplicantVacancies p1, ApplicantVacancies p2)
                    {
                        return p2.RankWeight.CompareTo(p2.RankWeight);
                    }
                );

                int rank = 1;
                foreach (ApplicantVacancies item in applicationList)
                {
                    if (item.Ranked)
                    {
                        item.RankNumber = rank;
                        session.Update(item);
                        rank++;
                    }
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        #endregion

        #region vacancy pipeline

        public static List<PipelineSteps> GetPipelineStepList(string pipelineCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(PipelineSteps));
            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));

            deCrit.AddOrder(Order.Asc("SortOrder"));
            return deCrit.List<PipelineSteps>() as List<PipelineSteps>;
        }

        #endregion

        #region recruitment process

        public static List<VacancyStatus> GetNonArchiveRecruitmentProcessList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));
            deCrit.Add(Expression.Eq("IsArchive", false));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<VacancyStatus>() as List<VacancyStatus>;
        }

        public static List<string> GetNonArchiveRecruitmentProcessCodeList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));
            deCrit.Add(Expression.Eq("IsArchive", false));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.SetProjection(Projections.Property("Code"));
            return deCrit.List<string>() as List<string>;
        }

        public static List<string> GetNonArchiveAndHiredRecruitmentProcessCodeList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));

            var conjunction1 = Restrictions.Conjunction();
            conjunction1.Add(Restrictions.Eq("IsArchive", false));
            conjunction1.Add(Restrictions.Eq("Active", true));

            var conjunction2 = Restrictions.Conjunction();
            conjunction2.Add(Restrictions.Eq("Name", "Hired"));
            conjunction2.Add(Restrictions.Eq("Active", true));

            var disjunction = Restrictions.Disjunction();
            disjunction.Add(conjunction1);
            disjunction.Add(conjunction2);

            deCrit.Add(disjunction);
            deCrit.SetProjection(Projections.Property("Code"));
            return deCrit.List<string>() as List<string>;
        }

        public static List<Vacancies> GetVacanciesByPipeline(string pipilneCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("PipelineCode", pipilneCode));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<VacancyStatus> GetArchiveRecruitmentProcessList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));
            deCrit.Add(Expression.Eq("IsArchive", true));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<VacancyStatus>() as List<VacancyStatus>;
        }

        public static int CountVacancyApplicantStatus(string vacancyCode, string[] statusCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.In("Status", statusCode));

            decrit.SetProjection(Projections.Count("ApplicantCode"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyApplicantStatus(string vacancyCode, string statusCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(statusCode))
                decrit.Add(Expression.Eq("Status", statusCode));
            decrit.SetProjection(Projections.Count("ApplicantCode"));
            return decrit.UniqueResult<int>();
        }

        #endregion

    }
}
