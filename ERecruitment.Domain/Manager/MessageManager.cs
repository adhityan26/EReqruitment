using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;


namespace ERecruitment.Domain
{
    public class MessageManager : ERecruitmentRepositoryBase
    {       

        #region crude

        public static void SaveSMSMessage(SMSMessages smsMessage)
        {
            SaveWithTransaction<SMSMessages>(smsMessage);
        }

        public static void Update(SMSMessages smsMessage)
        {
            UpdateWithTransaction<SMSMessages>(smsMessage);
        }

        public static SMSMessages GetSMSMessage(string smsMessageId)
        {
            return session.Get<SMSMessages>(smsMessageId);
        }

        #endregion

        #region get

        public static List<SMSMessages> GetUnVerifiedOutgoingMessageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(SMSMessages));
            decrit.Add(Expression.Eq("Verified", false));
            decrit.Add(Expression.Eq("Flow", "Out"));
            decrit.AddOrder(Order.Asc("LogTime"));
            return decrit.List<SMSMessages>() as List<SMSMessages>;
        }

        #endregion
        


    }
}
