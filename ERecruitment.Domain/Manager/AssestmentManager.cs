using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;


namespace ERecruitment.Domain
{
    public class AssestmentManager : ERecruitmentRepositoryBase
    {
        #region assestment question

        public static void SaveAssestmentQuestion(Questions question)
        {

            ITransaction transaction = session.BeginTransaction();
            try
            {
                question.Code = Guid.NewGuid().ToString();
                session.Save(question);
                int number = 1;
                foreach (QuestionAnswers option in question.AnswerOptionList)
                {
                    option.Number = number;
                    option.QuestionCode = question.Code;
                    if (option.IsCorrectAnswer)
                        question.CorrectAnswerLabel = option.Label;
                    session.Save(option);
                    number++;
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void UpdateAssestmentQuestion(Questions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<QuestionAnswers> existingOptionList = GetAnswerOptionList(question.Code);
                foreach (QuestionAnswers item in existingOptionList)
                    session.Delete(item);

                session.Update(question);
                int number = 1;
                foreach (QuestionAnswers option in question.AnswerOptionList)
                {
                    option.Number = number;
                    option.QuestionCode = question.Code;
                    if (option.IsCorrectAnswer)
                        question.CorrectAnswerLabel = option.Label;
                    session.Save(option);
                    number++;
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void DeleteAssestmentQuestion(Questions question)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<QuestionAnswers> existingOptionList = GetAnswerOptionList(question.Code);
                foreach (QuestionAnswers item in existingOptionList)
                    session.Delete(item);

                session.Delete(question);

                List<Questions> childQuestionList = GetChildrenAssestmentQuestionList(question.Code);
                foreach (Questions childQuestion in childQuestionList)
                {
                    List<QuestionAnswers> existingCHildOptionList = GetAnswerOptionList(childQuestion.Code);
                    foreach (QuestionAnswers item in existingCHildOptionList)
                        session.Delete(item);

                    session.Delete(childQuestion);

                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static Questions GetAssestmentQuestion(string code)
        {
            return session.Get<Questions>(code);
        }

        public static List<Questions> GetAssestmentQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Questions));

            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Questions>() as List<Questions>;
        }
        
        public static List<Questions> GetAssestmentQuestionList(string[] codeList)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Questions));
            decrit.Add(Expression.In("Code", codeList));
            decrit.Add(Expression.Eq("Active", true));

            return decrit.List<Questions>() as List<Questions>;
        }

        public static List<Questions> GetParentAssestmentQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Questions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("QuestionType", "Assestment"));
            return decrit.List<Questions>() as List<Questions>;
        }

        public static List<Questions> GetParentSurveyQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Questions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("QuestionType", "Survey"));
            return decrit.List<Questions>() as List<Questions>;
        }

        public static List<Questions> GetChildrenAssestmentQuestionList(string parentCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Questions));
            decrit.Add(Expression.Eq("Type", "Child"));
            decrit.Add(Expression.Eq("ParentCode", parentCode));
            return decrit.List<Questions>() as List<Questions>;
        }

        public static List<QuestionAnswers> GetAnswerOptionList(string questionCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(QuestionAnswers));
            decrit.Add(Expression.Eq("QuestionCode", questionCode));
            return decrit.List<QuestionAnswers>() as List<QuestionAnswers>;
        }
        
        public static List<Questions> GetAssestmentQuestionList(string globalSearch, int startRecord,
                                                                              int length, string sortColumn,
                                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Questions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<Questions>() as List<Questions>;
        }

        public static int CountAssestmentQuestionList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Questions));
            decrit.Add(Expression.Eq("Type", "Parent"));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region test questions

        public static List<TestQuestions> GetTestQuestionList(string testCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(TestQuestions));
            decrit.Add(Expression.Eq("TestCode", testCode));
            decrit.AddOrder(Order.Asc("OrderId"));
            return decrit.List<TestQuestions>() as List<TestQuestions>;
        }

        public static List<string> GetAssestmentQuestionCodeList(string testCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(TestQuestions));
            decrit.Add(Expression.Eq("TestCode", testCode));
            decrit.AddOrder(Order.Asc("OrderId"));
            decrit.SetProjection(Projections.Property("QuestionCode"));
            return decrit.List<string>() as List<string>;
        }

        #endregion
    }
}

