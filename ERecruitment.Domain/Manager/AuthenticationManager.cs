using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;


namespace ERecruitment.Domain
{
    public class AuthenticationManager : ERecruitmentRepositoryBase
    {
        #region user accounts

        public static UserAccounts GetUserByNIK(string nik)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("NIK", nik));
            return decrit.UniqueResult<UserAccounts>();
        }

        public static UserExternalAccounts GetExternalAccount(string appName, string userId)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserExternalAccounts));
            decrit.Add(Expression.Eq("ApplicationName", appName));
            decrit.Add(Expression.Eq("ExternalID", userId));

            return decrit.UniqueResult<UserExternalAccounts>();
        }

        public static void RemoveUserAccount(UserAccounts userAccount)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                userAccount.Active = false;
                session.Update(userAccount);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SaveUserAccount(UserAccounts userAccount, Applicants applicantProfile,
                            UserExternalAccounts externalAccountId)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                #region validation

                Applicants conflictApplicant = ApplicantManager.GetVerifiedActiveApplicantByEmailAddress(applicantProfile.Email);
                if (conflictApplicant != null)
                    throw new ApplicationException("Email address already used");
                else
                {
                    conflictApplicant = ApplicantManager.GetVerifiedActiveApplicantByEmailAddress(applicantProfile.Email);
                    if (conflictApplicant != null)
                        throw new ApplicationException("Email address already used");
                }

                #endregion

                userAccount.ApplicantCode = applicantProfile.Code;
                userAccount.IsApplicant = true;
                session.Save(userAccount);
                session.Save(applicantProfile);
                session.Save(externalAccountId);

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void SaveUserAccount(UserAccounts userAccount)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (string.IsNullOrEmpty(userAccount.Code))
                    userAccount.Code = Guid.NewGuid().ToString();

                if (!string.IsNullOrEmpty(userAccount.NIK))
                {
                    UserAccounts checkUserNik = GetUserByNIK(userAccount.NIK);
                    if (checkUserNik != null)
                        throw new ApplicationException("NIK already used");
                    session.Evict(checkUserNik);
                }

                UserAccounts checkUserName = GetUserAccountByEmail(userAccount.UserName);
                if (checkUserName != null)
                    throw new ApplicationException("Email address already used");
                session.Evict(checkUserName);


                if (!string.IsNullOrEmpty(userAccount.RoleCodes))
                {
                    string[] roleCodes = userAccount.RoleCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < roleCodes.Length; i++)
                    {
                        UserRoles userRole = new UserRoles();
                        userRole.UserCode = userAccount.Code;
                        userRole.RoleCode = roleCodes[i];
                        session.Save(userRole);
                    }
                }

                if (!string.IsNullOrEmpty(userAccount.CompanyCodes))
                {
                    string[] companyCodes = userAccount.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < companyCodes.Length; i++)
                    {
                        UserCompanies userCompany = new UserCompanies();
                        userCompany.UserCode = userAccount.Code;
                        userCompany.CompanyCode = companyCodes[i];
                        session.Save(userCompany);
                    }
                }

                session.Save(userAccount);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SaveUserAccount(UserAccounts userAccount, Applicants applicantProfile)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                #region validation

                Applicants conflictApplicant = ApplicantManager.GetVerifiedActiveApplicantByEmailAddress(applicantProfile.Email);
                if (conflictApplicant != null)
                    throw new ApplicationException("Your already registered in workwithkg");
                
                UserAccounts checkUserName = GetUserAccountByEmail(userAccount.UserName);
                if (checkUserName != null)
                    throw new ApplicationException("Email address already used");
                session.Evict(checkUserName);

                #endregion

                userAccount.LastLogin = DateTime.Now;
                session.Save(userAccount);
                session.Save(applicantProfile);

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void SaveUserAccountSosmedRegistration(UserAccounts userAccount, Applicants applicantProfile)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                userAccount.LastLogin = DateTime.Now;
                session.Save(userAccount);
                session.Save(applicantProfile);

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void UpdateUserAccount(UserAccounts userAccount)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                if (!string.IsNullOrEmpty(userAccount.NIK))
                {
                    UserAccounts checkUser = GetUserByNIK(userAccount.NIK);
                    if (checkUser != null)
                        if (checkUser.Code != userAccount.Code)
                            throw new ApplicationException("NIK already used");
                    session.Evict(checkUser);
                }

                UserAccounts checkUserName = GetUserAccountByEmail(userAccount.UserName);
                if (checkUserName != null)
                    if (checkUserName.Code != userAccount.Code)
                        throw new ApplicationException("Email address already used");
                session.Evict(checkUserName);

                session.Update(userAccount);
                List<UserRoles> existingRoles = GetRoleList(userAccount.Code);
                foreach (UserRoles item in existingRoles)
                    session.Delete(item);

                List<UserCompanies> existingCompany = GetUserCompanyList(userAccount.Code);
                foreach (UserCompanies item in existingCompany)
                    session.Delete(item);

                if (!string.IsNullOrEmpty(userAccount.RoleCodes))
                {
                    string[] roleCodes = userAccount.RoleCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < roleCodes.Length; i++)
                    {
                        UserRoles userRole = new UserRoles();
                        userRole.UserCode = userAccount.Code;
                        userRole.RoleCode = roleCodes[i];
                        session.Save(userRole);
                    }
                }

                if (!string.IsNullOrEmpty(userAccount.CompanyCodes))
                {
                    string[] companyCodes = userAccount.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < companyCodes.Length; i++)
                    {
                        UserCompanies userCompany = new UserCompanies();
                        userCompany.UserCode = userAccount.Code;
                        userCompany.CompanyCode = companyCodes[i];
                        session.Save(userCompany);
                    }
                }

                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateUserAccountStamp(UserAccounts userAccount)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Update(userAccount);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void UpdateApplicant(Applicants applicant)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Update(applicant);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static UserAccounts GetUserAccount(string accountId)
        {

            return session.Get<UserAccounts>(accountId);
        }

        public static List<UserAccounts> GetUserAccount()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(UserAccounts));
            deCrit.Add(Restrictions.IsNotNull("CompanyCode"));
            return deCrit.List<UserAccounts>() as List<UserAccounts>;
        }

        public static UserAccounts GetUserAccountByEmail(string userName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("UserName", userName));

            return decrit.UniqueResult<UserAccounts>();
        }

        public static UserAccounts GetUserAccountByUserNameOrEmail(string userName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("UserName", userName));
            decrit.Add(Restrictions.IsNotNull("CompanyCode"));

            return decrit.UniqueResult<UserAccounts>();
        }

        public static UserAccounts GetApplicantUserAccount(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.UniqueResult<UserAccounts>();
        }

        public static UserAccounts GetApplicantUserAccountByUserName(string userName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("UserName", userName));
            return decrit.UniqueResult<UserAccounts>();
        }

        public static UserAccounts AuthenticateUser(string userName, string password)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("UserName", userName));
            decrit.Add(Expression.Eq("Password", password));

            return decrit.UniqueResult<UserAccounts>();
        }

        public static UserAccounts CheckUserNameUser(string userName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("UserName", userName));

            return decrit.UniqueResult<UserAccounts>();
        }

        public static UserAccounts CheckUserNameUserisActive(string userName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UserAccounts));
            decrit.Add(Expression.Eq("UserName", userName));
            decrit.Add(Expression.Eq("Active", true));

            return decrit.UniqueResult<UserAccounts>();
        }

        public static List<UserAccounts> GetUserAccountList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(UserAccounts));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.Add(Expression.Eq("IsAdmin", false));
            deCrit.Add(Expression.Eq("IsApplicant", false));

            return deCrit.List<UserAccounts>() as List<UserAccounts>;
        }

        public static List<UserAccounts> GetUserAccountList(string globalSearch, int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(UserAccounts));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.Add(Expression.Eq("IsApplicant", false));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return deCrit.List<UserAccounts>() as List<UserAccounts>;
        }
        public static int CountUserAccountList(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(UserAccounts));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.Add(Expression.Eq("IsApplicant", false));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetProjection(Projections.Count("Code"));
            return deCrit.UniqueResult<int>();
        }

        public static List<UserRoles> GetRoleList(string code)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(UserRoles));
            deCrit.Add(Expression.Eq("UserCode", code));
            return deCrit.List<UserRoles>() as List<UserRoles>;
        }

        public static List<UserCompanies> GetUserCompanyList(string code)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(UserCompanies));
            deCrit.Add(Expression.Eq("UserCode", code));
            return deCrit.List<UserCompanies>() as List<UserCompanies>;
        }

        #endregion

        #region recruiter

        public static List<UserAccounts> GetRecruiter()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(UserAccounts));
            deCrit.Add(Expression.Eq("IsRecruiter", true));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<UserAccounts>() as List<UserAccounts>;
        }

        #endregion

    }
}
