using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Linq;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using KG.SISDM;
using NHibernate.Transform;

namespace ERecruitment.Domain
{
    public class ERecruitmentManager : ERecruitmentRepositoryBase
    {
        #region lookup values

        #region city

        public static List<City> GetCityDataTableList(string globalSearch, int startRecord,
                                               int length, string sortColumn, string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(City));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("CityName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<City>() as List<City>;
        }

        public static int CountCityList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(City));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("CityName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("CityCode"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }


        public static List<City> GetCity(string provinceCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(City));
            deCrit.Add(Expression.Eq("ProvinceCode", provinceCode));
            return deCrit.List<City>() as List<City>;
        }

        public static List<City> GetCityList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(City));
            deCrit.AddOrder(Order.Asc("CityName"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<City>() as List<City>;
        }

        public static City GetCityByCode(string code)
        {
            return session.Get<City>(code);
        }

        public static City SaveCity(City obj)
        {
            SaveWithTransaction<City>(obj);
            return obj;
        }

        public static City UpdateCity(City obj)
        {
            UpdateWithTransaction<City>(obj);
            return obj;
        }

        public static City RemoveCity(City obj)
        {
            obj.Active = false;
            UpdateWithTransaction<City>(obj);
            return obj;
        }

        public static City GetCity(string countryCode, string provinceCode, string cityCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(City));
            deCrit.Add(Expression.Eq("CountryCode", countryCode));
            deCrit.Add(Expression.Eq("ProvinceCode", provinceCode));
            deCrit.Add(Expression.Eq("CityCode", cityCode));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<City>();
        }

        #endregion

        #region province
        public static List<Province> GetProvinceList(string globalSearch, int startRecord,
                                            int length, string sortColumn, string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Province));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("ProvinceName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Province>() as List<Province>;
        }

        public static int CountProvinceList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Province));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("ProvinceName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ProvinceCode"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        public static List<Province> GetProvinceListByCountry(string countryCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Province));
            deCrit.AddOrder(Order.Asc("ProvinceName"));
            deCrit.Add(Expression.Eq("CountryCode", countryCode));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Province>() as List<Province>;
        }

        public static List<Province> GetProvinceList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Province));
            deCrit.AddOrder(Order.Asc("ProvinceName"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Province>() as List<Province>;
        }
        public static Province SaveProvince(Province obj)
        {
            SaveWithTransaction<Province>(obj);
            return obj;
        }
        public static Province UpdateProvince(Province obj)
        {
            UpdateWithTransaction<Province>(obj);
            return obj;
        }
        public static Province RemoveProvince(Province obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Province>(obj);
            return obj;
        }
        public static Province GetProvince(string code)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Province));
            deCrit.Add(Expression.Eq("ProvinceCode", code));
            return deCrit.UniqueResult<Province>();
        }
        #endregion

        #region country
        public static Country GetIndonesia()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Country));
            deCrit.Add(Expression.Eq("Code", "Indonesia"));
            return deCrit.UniqueResult<Country>();
        }
        public static List<Country> GetCountryList(string globalSearch, int startRecord,
                                            int length, string sortColumn, string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Country));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Country>() as List<Country>;
        }

        public static int CountCountryList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Country));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }
        public static Country GetCountry(string code)
        {
            return session.Get<Country>(code);
        }
        public static List<Country> GetCountryList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Country));
            deCrit.AddOrder(Order.Asc("Name"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Country>() as List<Country>;
        }
        public static Country SaveCountry(Country obj)
        {
            SaveWithTransaction<Country>(obj);
            return obj;
        }
        public static Country UpdateCountry(Country obj)
        {
            UpdateWithTransaction<Country>(obj);
            return obj;
        }
        public static Country RemoveCountry(Country obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Country>(obj);
            return obj;
        }
        public static List<Country> GetNationalityList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Country));
            deCrit.Add(Expression.IsNotNull(Projections.Property("Nationality")));
            deCrit.AddOrder(Order.Asc("Nationality"));
            return deCrit.List<Country>() as List<Country>;
        }
        #endregion

        public static List<ReasonForLeaving> GetReasonForLeaving()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ReasonForLeaving));
            deCrit.AddOrder(Order.Asc("Name"));
            return deCrit.List<ReasonForLeaving>() as List<ReasonForLeaving>;
        }

        #region year
        public static List<Years> GetYearList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Years));
            deCrit.AddOrder(Order.Desc("Year"));
            return deCrit.List<Years>() as List<Years>;
        }

        public static List<Years> GetLastYearList(int limit)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Years));
            deCrit.Add(Expression.Le("Year", DateTime.Now.Year));
            deCrit.AddOrder(Order.Desc("Year"));
            deCrit.SetMaxResults(limit);
            return deCrit.List<Years>() as List<Years>;
        }
        #endregion

        public static List<Month> GetMonthList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Month));
            return deCrit.List<Month>() as List<Month>;
        }

        #region vacancy category

        public static List<VacancyCategories> GetVacancyCategoryList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacancyCategories));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("Name"));

            return decrit.List<VacancyCategories>() as List<VacancyCategories>;
        }

        public static List<Vacancies> GetPositionsList()
        {
            List<Vacancies> vlist = new List<Vacancies>();
            IQuery decrit = session.CreateSQLQuery("SELECT DISTINCT(v.PositionCode), v.PositionName FROM ApplicantVacancies av INNER JOIN Vacancies v ON av.VacancyCode= v.Code");
            IList<Object[]> list  = decrit.List<Object[]>();
            for(int i=0;i<list.Count;i++) {
                Object[] obj = list[i];
                Vacancies v = new Vacancies();
                v.PositionCode = toString(obj[0]);
                v.PositionName = toString(obj[1]);
                vlist.Add(v);
            }

            return vlist;
        }

        public static String toString(Object o) {
            if (o != null)
            {
                return o.ToString();
            }
            else {
                return "";
            }
        }

        public static List<SourceReferences> GetSourceList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(SourceReferences));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("Name"));

            return decrit.List<SourceReferences>() as List<SourceReferences>;
        }

        public static VacancyCategories GetVacancyCategory(string code)
        {
            return session.Get<VacancyCategories>(code);
        }


        public static List<VacancyCategories> GetCategoriesList(string globalSearch,
                                                                          int startRecord,
                                                                          int length,
                                                                          string sortColumn,
                                                                          string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacancyCategories));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<VacancyCategories>() as List<VacancyCategories>;
        }

        public static int CountCategoryList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacancyCategories));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        public static VacancyCategories GetJobCategory(string code)
        {
            return session.Get<VacancyCategories>(code);
        }

        public static VacancyCategories SaveJobCategory(VacancyCategories obj)
        {
            SaveWithTransaction<VacancyCategories>(obj);
            return obj;
        }

        public static VacancyCategories UpdateJobCategory(VacancyCategories obj)
        {
            UpdateWithTransaction<VacancyCategories>(obj);
            return obj;
        }

        #endregion

        #region location
        public static List<Locations> GetLocations()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Locations));
            return deCrit.List<Locations>() as List<Locations>;

        }

        public static List<Locations> GetLocationList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Locations));
            return deCrit.List<Locations>() as List<Locations>;
        }
        #endregion

        #region organization position
        public static List<OrganizationPosition> GetOrganizationPosition()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(OrganizationPosition));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<OrganizationPosition>() as List<OrganizationPosition>;
        }
        public static OrganizationPosition SaveOrganizationPosition(OrganizationPosition obj)
        {
            SaveWithTransaction<OrganizationPosition>(obj);
            return obj;
        }
        public static OrganizationPosition UpdateOrganizationPosition(OrganizationPosition obj)
        {
            UpdateWithTransaction<OrganizationPosition>(obj);
            return obj;
        }

        public static OrganizationPosition GetOrganizationPosition(string code)
        {
            return session.Get<OrganizationPosition>(code);
        }
        public static List<OrganizationPosition> GetOrganizationPositionList(string globalSearch,
                                                                  int startRecord,
                                                                  int length,
                                                                  string sortColumn,
                                                                  string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(OrganizationPosition));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Position", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<OrganizationPosition>() as List<OrganizationPosition>;
        }

        public static int CountOrganizationPositionList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(OrganizationPosition));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Position", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region industry
        public static Industries GetIndustry(string code)
        {
            return session.Get<Industries>(code);
        }
        public static List<Industries> GetIndustryList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Industries));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("Name"));

            return decrit.List<Industries>() as List<Industries>;
        }
        public static List<Industries> GetIndustriesList(string globalSearch,
                                                                          int startRecord,
                                                                          int length,
                                                                          string sortColumn,
                                                                          string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Industries));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Industries>() as List<Industries>;
        }

        public static int CountIndustryList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Industries));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        public static Industries SaveIndustry(Industries obj)
        {
            SaveWithTransaction<Industries>(obj);
            return obj;
        }

        public static Industries UpdateIndustry(Industries obj)
        {
            UpdateWithTransaction<Industries>(obj);
            return obj;
        }

        #endregion

        #region positions

        public static Positions GetPositionByName(string companyCode, string positionName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Positions));
            decrit.Add(Expression.Eq("CompanyCode", companyCode));
            decrit.Add(Expression.Eq("Name", positionName));
            return decrit.UniqueResult<Positions>();
        }

        public static List<Positions> GetPositionList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Positions));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Positions>() as List<Positions>;
        }

        public static List<Positions> GetPositionListByCompanyCode(string companyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Positions));
            deCrit.Add(Expression.Eq("CompanyCode", companyCode));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.AddOrder(Order.Asc("Name"));
            return deCrit.List<Positions>() as List<Positions>;
        }

        public static List<Positions> GetPositionList(string globalSearch,
                                                                  int startRecord,
                                                                  int length,
                                                                  string sortColumn,
                                                                  string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Positions));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Positions>() as List<Positions>;
        }

        public static int CountPositionList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Positions));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("PositionCode"));

            return decrit.UniqueResult<int>();
        }

        public static Positions GetPosition(string code)
        {
            return session.Get<Positions>(code);
        }

        public static Positions SavePosition(Positions obj)
        {
            SaveWithTransaction<Positions>(obj);
            return obj;
        }

        public static Positions UpdatePosition(Positions obj)
        {
            UpdateWithTransaction<Positions>(obj);
            return obj;
        }

        #endregion

        #region employment status
        public static List<EmploymentStatus> GetEmploymentStatusList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EmploymentStatus));
            deCrit.AddOrder(Order.Asc("Name"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<EmploymentStatus>() as List<EmploymentStatus>;
        }
        public static List<EmploymentStatus> GetEmploymentStatusList(string globalSearch,
                                                                  int startRecord,
                                                                  int length,
                                                                  string sortColumn,
                                                                  string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EmploymentStatus));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<EmploymentStatus>() as List<EmploymentStatus>;
        }

        public static int CountEmploymentStatusList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EmploymentStatus));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));

            return decrit.UniqueResult<int>();
        }


        public static EmploymentStatus UpdateEmploymentStatus(EmploymentStatus obj)
        {
            UpdateWithTransaction<EmploymentStatus>(obj);
            return obj;
        }
        public static EmploymentStatus SaveEmploymentStatus(EmploymentStatus obj)
        {
            SaveWithTransaction<EmploymentStatus>(obj);
            return obj;
        }
        public static EmploymentStatus GetEmploymentStatus(string code)
        {
            return session.Get<EmploymentStatus>(code);
        }

        #endregion

        #region education level
        public static List<EducationLevels> GetEducationLevels()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EducationLevels));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<EducationLevels>() as List<EducationLevels>;
        }

        public static List<EducationLevels> GetEducationLevelsDropdown()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EducationLevels));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.SetProjection(Projections.Distinct(Projections.ProjectionList()
                .Add(Projections.Alias(Projections.Property("Name"), "Name"))));
            deCrit.SetResultTransformer(
                new NHibernate.Transform.AliasToBeanResultTransformer(typeof(EducationLevels)));

            //deCrit.SetProjection(Projections.Distinct(Projections.Property("Name")));
            return deCrit.List<EducationLevels>() as List<EducationLevels>;
        }


        public static List<EducationLevels> GetEducationLevelList(string globalSearch,
                                                                int startRecord,
                                                                int length,
                                                                string sortColumn,
                                                                string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationLevels));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<EducationLevels>() as List<EducationLevels>;
        }

       

        public static int CountEducationLevelList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationLevels));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("EducationLevel"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }



        public static EducationLevels SaveEducationLevel(EducationLevels obj)
        {
            SaveWithTransaction<EducationLevels>(obj);
            return obj;
        }

        public static EducationLevels UpdateEducationLevel(EducationLevels obj)
        {
            UpdateWithTransaction<EducationLevels>(obj);
            return obj;
        }

        public static EducationLevels GetEducationLevel(string code)
        {
            return session.Get<EducationLevels>(code);
        }

        #endregion

        #region field of study
        public static List<EducationFieldOfStudy> GetFieldOfStudyList(string globalSearch,
                                                              int startRecord,
                                                              int length,
                                                              string sortColumn,
                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationFieldOfStudy));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<EducationFieldOfStudy>() as List<EducationFieldOfStudy>;
        }

        public static int CountFieldOfStudyList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationFieldOfStudy));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }


        public static List<EducationFieldOfStudy> GetEducationFieldOfStudy()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EducationFieldOfStudy));
            deCrit.AddOrder(Order.Asc("Name"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<EducationFieldOfStudy>() as List<EducationFieldOfStudy>;
        }

        public static EducationFieldOfStudy GetFieldOfStudy(string code)
        {
            return session.Get<EducationFieldOfStudy>(code);
        }

        public static List<EducationFieldOfStudy> GetFieldOfStudyList()
        {

            ICriteria decrit = session.CreateCriteria(typeof(EducationFieldOfStudy));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("Name"));
            return decrit.List<EducationFieldOfStudy>() as List<EducationFieldOfStudy>;
        }

        public static List<EducationFieldOfStudy> GetFieldOfStudyListByKategori(int kategori)
        {

            ICriteria decrit = session.CreateCriteria(typeof(EducationFieldOfStudy));
            decrit.Add(Expression.Eq("Active", true));
            decrit.Add(Expression.Eq("Kategori", kategori));
            decrit.AddOrder(Order.Asc("Code"));
            return decrit.List<EducationFieldOfStudy>() as List<EducationFieldOfStudy>;
        }

        public static EducationFieldOfStudy SaveFieldOfStudy(EducationFieldOfStudy obj)
        {
            SaveWithTransaction<EducationFieldOfStudy>(obj);
            return obj;
        }

        public static EducationFieldOfStudy UpdateFieldOfStudy(EducationFieldOfStudy obj)
        {
            UpdateWithTransaction<EducationFieldOfStudy>(obj);
            return obj;
        }

        #endregion

        #region education major
        public static List<EducationMajor> GetEducationMajorList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EducationMajor));
            return deCrit.List<EducationMajor>() as List<EducationMajor>;
        }

        public static EducationMajor GetEducationMajor(string code)
        {
            return session.Get<EducationMajor>(code);
        }

        public static EducationMajor SaveEducationMajor(EducationMajor obj)
        {
            SaveWithTransaction<EducationMajor>(obj);
            return obj;
        }
        public static EducationMajor UpdateEducationMajor(EducationMajor obj)
        {
            UpdateWithTransaction<EducationMajor>(obj);
            return obj;
        }

        public static List<EducationMajor> GetEducationMajorList(string globalSearch,
                                                              int startRecord,
                                                              int length,
                                                              string sortColumn,
                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMajor));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Major", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<EducationMajor>() as List<EducationMajor>;
        }

        public static int CountEducationMajorList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMajor));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Major", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }



        #endregion

        #region education mapping header
        public static List<EducationMappingHeader> GetEducationMappingHeader()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EducationMappingHeader));
            return deCrit.List<EducationMappingHeader>() as List<EducationMappingHeader>;
        }

        public static EducationMappingHeader GetEducationMappingHeader(string code)
        {
            return session.Get<EducationMappingHeader>(code);
        }

        public static EducationMappingHeader SaveEducationMappingHeader(EducationMappingHeader obj)
        {
            SaveWithTransaction<EducationMappingHeader>(obj);
            return obj;
        }
        public static EducationMappingHeader UpdateEducationMappingHeader(EducationMappingHeader obj)
        {
            UpdateWithTransaction<EducationMappingHeader>(obj);
            return obj;
        }

        public static List<EducationMappingHeader> GetEducationMappingHeader(string globalSearch,
                                                               int startRecord,
                                                               int length,
                                                               string sortColumn,
                                                               string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingHeader));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("UniversitiesName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<EducationMappingHeader>() as List<EducationMappingHeader>;
        }

        public static int CountEducationMappingHeader(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingHeader));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("UniversitiesName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("EducationMappingHeaderCode"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        public static bool CheckIfExistEducationMappingHeader(string EducationMappingHeaderCode, string UniversitiesCode, string EducationLevel, string FieldOfStudyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingHeader));
            bool exist;
            EducationMappingHeader educationMappingHeader = new EducationMappingHeader();
            decrit.Add(Expression.Eq("UniversitiesCode", UniversitiesCode));
            decrit.Add(Expression.Eq("EducationLevel", EducationLevel));
            decrit.Add(Expression.Eq("FieldOfStudyCode", FieldOfStudyCode));
            decrit.Add(Expression.Eq("Active", true));
            educationMappingHeader = decrit.UniqueResult<EducationMappingHeader>();
            if (educationMappingHeader != null && (educationMappingHeader.EducationMappingHeaderCode != EducationMappingHeaderCode ))
            {
                exist = true;
            }
            else
            {
                exist = false;
            }
            return exist;
        }



        #endregion

        #region education mapping detail
        public static List<EducationMappingDetail> GetEducationMappingDetail()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EducationMappingDetail));
            return deCrit.List<EducationMappingDetail>() as List<EducationMappingDetail>;
        }

        public static List<EducationMappingDetail> GetEducationMappingDetailByMajorCode(string MajorCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(EducationMappingDetail));
            deCrit.Add(Expression.Eq("MajorCode", MajorCode));
            return deCrit.List<EducationMappingDetail>() as List<EducationMappingDetail>;
        }

        public static EducationMappingDetail GetEducationMappingDetail(string code)
        {
            return session.Get<EducationMappingDetail>(code);
        }

        public static EducationMappingDetail SaveEducationMappingDetail(EducationMappingDetail obj)
        {
            SaveWithTransaction<EducationMappingDetail>(obj);
            return obj;
        }
        public static EducationMappingDetail UpdateEducationMappingDetail(EducationMappingDetail obj)
        {
            UpdateWithTransaction<EducationMappingDetail>(obj);
            return obj;
        }

        public static List<EducationMappingDetail> GetEducationMappingDetail(string globalSearch,
                                                               int startRecord,
                                                               int length,
                                                               string sortColumn,
                                                               string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingDetail));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("MajorName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<EducationMappingDetail>() as List<EducationMappingDetail>;
        }

        public static List<EducationMappingDetail> GetEducationMappingDetailByHeaderCode(string EducationMappingHeaderCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingDetail));
            decrit.Add(Expression.Eq("EducationMappingHeaderCode", EducationMappingHeaderCode));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<EducationMappingDetail>() as List<EducationMappingDetail>;
        }

        public static int CountEducationMappingDetail(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingDetail));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("MajorName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("EducationMappingDetailCode"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }



        #endregion

        #region languages
        public static List<Languages> GetLanguages()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Languages));
            return deCrit.List<Languages>() as List<Languages>;
        }

        public static Languages GetLanguage(string code)
        {
            return session.Get<Languages>(code);
        }
        public static Languages SaveLanguage(Languages obj)
        {
            SaveWithTransaction<Languages>(obj);
            return obj;
        }
        public static Languages UpdateLanguage(Languages obj)
        {
            UpdateWithTransaction<Languages>(obj);
            return obj;
        }


        public static List<Languages> GetLanguageList(string globalSearch,
                                                              int startRecord,
                                                              int length,
                                                              string sortColumn,
                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Languages));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Language", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Languages>() as List<Languages>;
        }

        public static int CountLanguageList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Languages));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Language", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region awards
        public static List<Awards> GetAwards()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Awards));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Awards>() as List<Awards>;
        }
        public static Awards GetAward(string code)
        {
            return session.Get<Awards>(code);
        }
        public static Awards SaveAward(Awards obj)
        {
            SaveWithTransaction<Awards>(obj);
            return obj;
        }
        public static List<Awards> GetAwardList(string globalSearch,
                                                              int startRecord,
                                                              int length,
                                                              string sortColumn,
                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Awards));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Award", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Awards>() as List<Awards>;
        }

        public static int CountAwardList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Awards));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Award", globalSearch, MatchMode.Anywhere));
            }
            decrit.Add(Expression.Eq("Active", true));
            decrit.SetProjection(Projections.Count("Code"));

            return decrit.UniqueResult<int>();
        }

        public static Awards UpdateAward(Awards obj)
        {
            UpdateWithTransaction<Awards>(obj);
            return obj;
        }

        #endregion

        #region organization type
        public static List<OrganizationType> GetOrganizationType()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(OrganizationType));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<OrganizationType>() as List<OrganizationType>;
        }
        public static OrganizationType GetOrganizationType(string code)
        {
            return session.Get<OrganizationType>(code);
        }
        public static OrganizationType SaveOrganizationType(OrganizationType obj)
        {
            SaveWithTransaction<OrganizationType>(obj);
            return obj;
        }
        public static OrganizationType UpdateOrganizationType(OrganizationType obj)
        {
            UpdateWithTransaction<OrganizationType>(obj);
            return obj;
        }

        public static List<OrganizationType> GetOrganizationTypeList(string globalSearch,
                                                              int startRecord,
                                                              int length,
                                                              string sortColumn,
                                                              string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(OrganizationType));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Type", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<OrganizationType>() as List<OrganizationType>;
        }

        public static int CountOrganizationTypeList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(OrganizationType));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Type", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }



        #endregion

        #region company

        public static List<Companies> GetCompanyList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Companies));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.AddOrder(Order.Asc("Name"));
            return deCrit.List<Companies>() as List<Companies>;
        }

        public static List<Companies> GetCompanyPerSite(string[] companyList)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Companies));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.Add(Expression.In("Code", companyList));
            deCrit.AddOrder(Order.Asc("Name"));
            return deCrit.List<Companies>() as List<Companies>;
        }

        public static List<Companies> GetCompanyList(string excludeCompanyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Companies));
            deCrit.Add(Expression.Eq
                ("Active", true));
            deCrit.Add(Expression.Not(Expression.Eq("Code", excludeCompanyCode)));
            deCrit.AddOrder(Order.Asc("Name"));
            return deCrit.List<Companies>() as List<Companies>;
        }

        public static Companies GetCompany(string code)
        {
            return session.Get<Companies>(code);
        }

        public static Companies GetMainCompany(string siteID)

        {
            ICriteria deCrit = session.CreateCriteria(typeof(Companies));

            deCrit.Add(Expression.Eq("ATSSite", siteID));
            deCrit.SetMaxResults(1);

            return deCrit.UniqueResult<Companies>();
        }

        public static Companies SaveCompany(Companies obj)
        {
            SaveWithTransaction<Companies>(obj);
            return obj;
        }

        public static Companies RemoveCompany(Companies obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Companies>(obj);
            return obj;
        }

        public static Companies UpdateCompany(Companies obj)
        {
            UpdateWithTransaction<Companies>(obj);
            return obj;
        }

        public static List<Companies> GetCompanyList(string globalSearch,
                                                     int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Companies));
            deCrit.Add(Expression.Eq
                ("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            else
                deCrit.AddOrder(Order.Desc("UpdateStamp"));

            return deCrit.List<Companies>() as List<Companies>;
        }

        public static int CountCompanyList(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Companies));
            deCrit.Add(Expression.Eq
                ("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            deCrit.SetProjection(Projections.Count("Code"));

            return deCrit.UniqueResult<int>();
        }


        #endregion

        #region divisions

        public static List<Divisions> GetDivisionList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Divisions));
            deCrit.Add(Expression.Eq
                ("Active", true));
            deCrit.AddOrder(Order.Asc("Name"));
            return deCrit.List<Divisions>() as List<Divisions>;
        }
        public static Divisions SaveDivision(Divisions obj)
        {
            SaveWithTransaction<Divisions>(obj);
            return obj;
        }
        public static Divisions RemoveDivision(Divisions obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Divisions>(obj);
            return obj;
        }
        public static Divisions UpdateDivision(Divisions obj)
        {
            UpdateWithTransaction<Divisions>(obj);
            return obj;
        }
        public static List<Divisions> GetDivisionList(string globalSearch,
                                                     int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Divisions));
            deCrit.Add(Expression.Eq
                ("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return deCrit.List<Divisions>() as List<Divisions>;
        }

        public static int CountDivisionList(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Divisions));
            deCrit.Add(Expression.Eq
                ("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            deCrit.SetProjection(Projections.Count("Code"));

            return deCrit.UniqueResult<int>();
        }

        public static Divisions GetDivision(string code)
        {

            return session.Get<Divisions>(code);
        }
        #endregion

        #region departments

        public static List<Departments> GetDepartmentList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Departments));
            deCrit.Add(Expression.Eq
                ("Active", true));
            deCrit.AddOrder(Order.Asc("Name"));
            return deCrit.List<Departments>() as List<Departments>;
        }
        public static Departments GetDepartment(string code)
        {
            return session.Get<Departments>(code);
        }
        public static Departments SaveDepartment(Departments obj)
        {
            SaveWithTransaction<Departments>(obj);
            return obj;
        }
        public static Departments UpdateDepartment(Departments obj)
        {
            UpdateWithTransaction<Departments>(obj);
            return obj;
        }
        public static Departments RemoveDepartment(Departments obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Departments>(obj);
            return obj;
        }
        public static List<Departments> GetDepartmentList(string globalSearch,
                                                     int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Departments));
            deCrit.Add(Expression.Eq
                ("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return deCrit.List<Departments>() as List<Departments>;
        }
        public static int CountDepartmentList(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Departments));
            deCrit.Add(Expression.Eq
                ("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            deCrit.SetProjection(Projections.Count("Code"));

            return deCrit.UniqueResult<int>();
        }


        #endregion

        #endregion

        #region counter

        public static int CountActiveJobPosition(DateTime activeDate)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("Active", true));
            decrit.Add(Expression.IsNotNull("PostDate"));
            decrit.Add(Expression.IsNotNull("ExpiredPostDate"));

            decrit.Add(Expression.Le("PostDate", activeDate.Date.AddDays(1).AddSeconds(-1)));
            decrit.Add(Expression.Ge("ExpiredPostDate", activeDate.Date.AddDays(-1)));

            decrit.SetProjection(Projections.Count("Code"));

            return decrit.UniqueResult<int>();
        }

        public static int CountActiveCompanies()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Companies));
            decrit.Add(Expression.Eq("Active", true));
            decrit.SetProjection(Projections.Count("Code"));

            return decrit.UniqueResult<int>();
        }

        #endregion

        #region vacancies

        public static List<Vacancies> GetPostedVacancyList(string companyCode,
                                                           string jobCategoryCode,
                                                           string locationCode)
        {
            DateTime activeDate = DateTime.Now;
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("AdvertisementStatus", "Posted"));

            if (!string.IsNullOrEmpty(companyCode))
                deCrit.Add(Expression.Eq("CompanyCode", companyCode));
            if (!string.IsNullOrEmpty(jobCategoryCode))
                deCrit.Add(Expression.Eq("CategoryCode", jobCategoryCode));
            if (!string.IsNullOrEmpty(locationCode))
                deCrit.Add(Expression.Eq("City", locationCode));

            deCrit.Add(Expression.IsNotNull("PostDate"));

            deCrit.Add(Expression.Le("PostDate", activeDate.Date.AddDays(1).AddSeconds(-1)));
            deCrit.Add(Expression.Or(Expression.IsNull("ExpirationDate"), Expression.Ge("ExpirationDate", activeDate.Date.AddDays(-1))));

            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;

        }

        public static List<Vacancies> GetVacancyList(string[] companyCode,
                                                     string jobCategoryCode,
                                                     string locationCode,
                                                     string[] status,
                                                     string advStatus)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));

            deCrit.Add(Expression.In("CompanyCode", companyCode));

            if (!string.IsNullOrEmpty(jobCategoryCode))
                deCrit.Add(Expression.Eq("CategoryCode", jobCategoryCode));
            if (!string.IsNullOrEmpty(locationCode))
                deCrit.Add(Expression.Eq("City", locationCode));
            deCrit.Add(Expression.In("Status", status));
            if (!string.IsNullOrEmpty(advStatus))
                deCrit.Add(Expression.Eq("AdvertisementStatus", advStatus));

            return deCrit.List<Vacancies>() as List<Vacancies>;

        }

        public static List<Vacancies> GetVacancyList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetOpenVacancyListByPipelineCode(string pipelineCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));

            deCrit.Add(Expression.Eq("PipelineCode", pipelineCode));
            deCrit.AddOrder(Order.Asc("PositionName"));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetOpenVacancyList(string positionName)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.Add(Expression.Eq("Status", "Open"));
            if (!string.IsNullOrEmpty(positionName))
                deCrit.Add(Expression.InsensitiveLike("PositionName", positionName, MatchMode.Anywhere));
            deCrit.AddOrder(Order.Asc("PositionName"));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetVacancyList(string typeOfEmployment, string status, string jobCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            if (!string.IsNullOrEmpty(typeOfEmployment) && typeOfEmployment != "0")
                deCrit.Add(Expression.Eq("TypeOfEmployment", typeOfEmployment));
            if (!string.IsNullOrEmpty(status) && status != "0")
                deCrit.Add(Expression.Eq("Status", status));
            if (!string.IsNullOrEmpty(jobCode) && jobCode != "0")
                deCrit.Add(Expression.Eq("JobCode", jobCode));

            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> SearchOpenVacancyList(List<string> statusList, DateTime date)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.In("Status", statusList.ToArray()));
            deCrit.Add(Expression.Lt("ExpiredPostDate", date.AddDays(1).AddSeconds(-1)));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetVacancyListByExpiredPostDate(string statusJob, List<string> advStatus, DateTime date)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Status", statusJob));
            deCrit.Add(Expression.In("AdvertisementStatus", advStatus));
            deCrit.Add(Expression.Lt("ExpiredPostDate", date.Date.AddDays(1).AddSeconds(-1)));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetExpiredVacancyList(string statusJob, List<string> advStatus, DateTime date)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Status", statusJob));
            deCrit.Add(Expression.In("AdvertisementStatus", advStatus));
            deCrit.Add(Expression.Ge("ExpiredPostDate", date.Date.AddDays(-1)));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetVacancyListByPostDate(string statusJob, List<string> advStatus, DateTime date)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Status", statusJob));
            deCrit.Add(Expression.In("AdvertisementStatus", advStatus));
            deCrit.Add(Expression.Le("PostDate", date.Date.AddDays(1).AddSeconds(-1)));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetOpenVacancyList(string statusJob, List<string> advStatus, DateTime date)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Status", statusJob));
            deCrit.Add(Expression.In("AdvertisementStatus", advStatus));
            deCrit.Add(Expression.Le("PostDate", date.Date.AddDays(1).AddSeconds(-1)));
            deCrit.Add(Expression.Ge("ExpiredPostDate", date.Date.AddDays(1).AddSeconds(-1)));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> SearchVacancyList(List<string> statusList, DateTime date)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.In("Status", statusList.ToArray()));
            deCrit.Add(Expression.IsNotNull("PostDate"));
            deCrit.Add(Expression.IsNotNull("ExpiredPostDate"));
            deCrit.Add(Expression.Le("PostDate", date.AddDays(1).AddSeconds(-1)));

            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> GetOpenVacancyList(string typeOfEmployment, string status, string jobCode, List<string> openStatus)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            if (!string.IsNullOrEmpty(typeOfEmployment) && typeOfEmployment != "0")
                deCrit.Add(Expression.Eq("TypeOfEmployment", typeOfEmployment));
            if (!string.IsNullOrEmpty(status) && status != "0")
                deCrit.Add(Expression.Eq("Status", status));
            else
                deCrit.Add(Expression.In("Status", openStatus.ToArray()));
            if (!string.IsNullOrEmpty(jobCode) && jobCode != "0")
                deCrit.Add(Expression.Eq("JobCode", jobCode));
            deCrit.AddOrder(Order.Desc("InsertStamp"));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> SearchVacancyList(string categoryCode, string locationCode, decimal minSalary, decimal maxSalary)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            if (!string.IsNullOrEmpty(categoryCode) && categoryCode != "0")
                deCrit.Add(Expression.Eq("CategoryCode", categoryCode));
            if (!string.IsNullOrEmpty(locationCode) && locationCode != "0")
                deCrit.Add(Expression.Eq("City", locationCode));
            if (minSalary > 0 && maxSalary == 0)
                deCrit.Add(Expression.Ge(Projections.Property("SalaryRangeTop"), minSalary));
            if (minSalary > 0 && maxSalary > 0)
            {
                deCrit.Add(Expression.Between(Projections.Property("SalaryRangeTop"), minSalary, maxSalary));

            }
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;

        }

        public static List<Vacancies> GetPendingVacancyList(string globalSearch,
                                                            int startRecord,
                                                            int length,
                                                            string sortColumn,
                                                            string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Status", "Pending"));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.Or(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere),
                    Expression.InsensitiveLike("CompanyName", globalSearch, MatchMode.Anywhere)));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            else
                deCrit.AddOrder(Order.Desc("InsertStamp"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static int CountPendingVacancyList(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("Status", "Pending"));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.Or(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere),
                    Expression.InsensitiveLike("CompanyName", globalSearch, MatchMode.Anywhere)));
            }

            deCrit.Add(Expression.Eq("Active", true));
            deCrit.SetProjection(Projections.Count("Code"));
            return deCrit.UniqueResult<int>();
        }

        public static List<Vacancies> GetVacancyList(string[] companyCodes,
                                                     string[] divisionCode,
                                                     string[] departmentCode,
                                                     string[] advertisementStatusCode,
                                                     string[] recruiterCodes, string statusJob,
                                                     string globalSearch,
                                                     int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            if (companyCodes.Length > 0 && companyCodes != null)
                deCrit.Add(Expression.In("CompanyCode", companyCodes));
            if (divisionCode.Length > 0 && divisionCode != null)
                deCrit.Add(Expression.In("Division", divisionCode));
            if (departmentCode.Length > 0 && departmentCode != null)
                deCrit.Add(Expression.In("Department", departmentCode));
            if (advertisementStatusCode.Length > 0 && advertisementStatusCode != null)
                deCrit.Add(Expression.In("AdvertisementStatus", advertisementStatusCode));
            if (!string.IsNullOrEmpty(statusJob) && statusJob != "0")
                deCrit.Add(Expression.Eq("Status", statusJob));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.Or(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere),
                    Expression.InsensitiveLike("CompanyName", globalSearch, MatchMode.Anywhere)));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            else
                deCrit.AddOrder(Order.Desc("InsertStamp"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;

        }

        public static int CountVacancyList(string[] companyCodes,
                                                     string[] divisionCode,
                                                     string[] departmentCode,
                                                     string[] advertisementStatusCode,
                                                     string[] recruiterCodes, string statusJob,
                                                     string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            if (companyCodes.Length > 0)
                deCrit.Add(Expression.In("CompanyCode", companyCodes));
            if (divisionCode.Length > 0)
                deCrit.Add(Expression.In("Division", divisionCode));
            if (departmentCode.Length > 0)
                deCrit.Add(Expression.In("Department", departmentCode));
            if (advertisementStatusCode.Length > 0)
                deCrit.Add(Expression.In("AdvertisementStatus", advertisementStatusCode));
            if (!string.IsNullOrEmpty(statusJob) && statusJob != "0")
                deCrit.Add(Expression.Eq("Status", statusJob));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.Or(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere),
                    Expression.InsensitiveLike("CompanyName", globalSearch, MatchMode.Anywhere)));
            }

            deCrit.Add(Expression.Eq("Active", true));
            deCrit.SetProjection(Projections.Count("Code"));

            return deCrit.UniqueResult<int>();
        }

        public static List<Vacancies> SearchPostedVacancyList(string categoryCode,
                                                        string locationCode,
                                                        decimal minSalary,
                                                        decimal maxSalary,
                                                        DateTime activeDate,
                                                        string keyWords,
                                                        string globalSearch,
                                                        int startRecord,
                                                        int length,
                                                        string sortColumn,
                                                        string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("AdvertisementStatus", "Posted"));
            if (!string.IsNullOrEmpty(categoryCode) && categoryCode != "0")
                deCrit.Add(Expression.Eq("CategoryCode", categoryCode));
            if (!string.IsNullOrEmpty(locationCode) && locationCode != "0")
                deCrit.Add(Expression.Eq("City", locationCode));
            if (minSalary > 0 && maxSalary == 0)
                deCrit.Add(Expression.Ge(Projections.Property("SalaryRangeTop"), minSalary));
            if (minSalary > 0 && maxSalary > 0)
            {
                deCrit.Add(Expression.Between(Projections.Property("SalaryRangeTop"), minSalary, maxSalary));

            }

            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere));
            }
            else if (!string.IsNullOrEmpty(keyWords))
            {
                deCrit.Add(Expression.InsensitiveLike("PositionName", keyWords, MatchMode.Anywhere));
            }

            deCrit.Add(Expression.IsNotNull("PostDate"));
            deCrit.Add(Expression.IsNotNull("ExpiredPostDate"));

            deCrit.Add(Expression.Le("PostDate", activeDate.Date.AddDays(1).AddSeconds(-1)));
            deCrit.Add(Expression.Ge("ExpiredPostDate", activeDate.Date.AddDays(-1)));

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;

        }

        public static int CountSearchPostedVacancyList(string categoryCode,
                                           string locationCode,
                                           decimal minSalary,
                                           decimal maxSalary,
                                           DateTime activeDate,
                                           string keyWords,
                                           string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            deCrit.Add(Expression.Eq("AdvertisementStatus", "Posted"));
            if (!string.IsNullOrEmpty(categoryCode) && categoryCode != "0")
                deCrit.Add(Expression.Eq("CategoryCode", categoryCode));
            if (!string.IsNullOrEmpty(locationCode) && locationCode != "0")
                deCrit.Add(Expression.Eq("City", locationCode));
            if (minSalary > 0 && maxSalary == 0)
                deCrit.Add(Expression.Ge(Projections.Property("SalaryRangeTop"), minSalary));
            if (minSalary > 0 && maxSalary > 0)
            {
                deCrit.Add(Expression.Between(Projections.Property("SalaryRangeTop"), minSalary, maxSalary));

            }

            deCrit.Add(Expression.IsNotNull("PostDate"));
            deCrit.Add(Expression.IsNotNull("ExpiredPostDate"));

            deCrit.Add(Expression.Le("PostDate", activeDate.Date.AddDays(1).AddSeconds(-1)));
            deCrit.Add(Expression.Ge("ExpiredPostDate", activeDate.Date.AddDays(-1)));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere));
            }
            else if (!string.IsNullOrEmpty(keyWords))
            {
                deCrit.Add(Expression.InsensitiveLike("PositionName", keyWords, MatchMode.Anywhere));
            }

            deCrit.Add(Expression.Eq("Active", true));
            deCrit.SetProjection(Projections.Count("Code"));

            return deCrit.UniqueResult<int>();
        }

        public static List<Vacancies> SearchVacancyList(string keyWord, string catCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            if (!string.IsNullOrEmpty(keyWord))
                deCrit.Add(Expression.InsensitiveLike("PositionName", keyWord, MatchMode.Anywhere));
            if (!string.IsNullOrEmpty(catCode))
                deCrit.Add(Expression.Eq("CategoryCode", catCode));
            deCrit.Add(Expression.Eq("Status", "Open"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static List<Vacancies> SearchVacancyList(string keyWord, string catCode, DateTime activeDate)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Vacancies));
            if (!string.IsNullOrEmpty(keyWord))
                deCrit.Add(Expression.InsensitiveLike("PositionName", keyWord, MatchMode.Anywhere));
            if (!string.IsNullOrEmpty(catCode))
                deCrit.Add(Expression.Eq("CategoryCode", catCode));
            deCrit.Add(Expression.Eq("Status", "Open"));
            deCrit.Add(Expression.Eq("Active", true));
            deCrit.Add(Expression.IsNotNull("PostDate"));
            deCrit.Add(Expression.IsNotNull("ExpiredPostDate"));

            deCrit.Add(Expression.Le("PostDate", activeDate.Date.AddDays(1).AddSeconds(-1)));
            deCrit.Add(Expression.Ge("ExpiredPostDate", activeDate.Date.AddDays(-1)));

            return deCrit.List<Vacancies>() as List<Vacancies>;
        }

        public static Vacancies GetVacancy(string code)
        {
            return session.Get<Vacancies>(code);
        }
        
        public static ApplicantVacancies GetApplicantVacancyByCode(string companyCode, string vacancyCode, string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("CompanyCode", companyCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            return decrit.UniqueResult<ApplicantVacancies>();
        }

        public static int countHiredApplicantByVacanciesCode(string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("Status", "Hired"));
            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();

        }

        public static Vacancies GetVacancyByPositionCode(string companyCode, string positionCode, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("CompanyCode", companyCode));
            decrit.Add(Expression.Eq("PositionCode", positionCode));
            decrit.Add(Expression.Eq("Status", status));

            return decrit.UniqueResult<Vacancies>();
        }

        public static Vacancies RemoveVacancy(Vacancies obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                obj.Active = false;
                obj.Status = "Deleted";
                obj.AdvertisementStatus = "Deleted";
                session.Update(obj);

                VacancyMilestoneLogs addLog = new VacancyMilestoneLogs();
                addLog.AdvertisementStatus = obj.AdvertisementStatus;
                addLog.Date = DateTime.Now;
                addLog.VacancyStatus = obj.Status;
                addLog.VacancyCode = obj.Code;
                addLog.ExecutorCode = obj.UpdatedBy;
                session.Save(addLog);

                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Vacancies CloseVacancy(Vacancies obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                obj.Active = true;
                obj.Status = "Closed";
                obj.AdvertisementStatus = "Closed";
                session.Update(obj);

                //update outstanding applicant to requisition closed
                List<ApplicantVacancies> applicantList = ERecruitmentManager.GetApplicantVacancyListByVacancy(obj.Code, "Hired");
                foreach (ApplicantVacancies outstandingApplicant in applicantList)
                {
                    outstandingApplicant.Status = "RequisitionClosed";
                    session.Update(outstandingApplicant);

                    ApplicantsVacancyMilestoneLogs applicantLog = new ApplicantsVacancyMilestoneLogs();
                    applicantLog.ApplicantCode = outstandingApplicant.ApplicantCode;
                    applicantLog.VacancyCode = outstandingApplicant.VacancyCode;
                    applicantLog.Status = "RequisitionClosed";
                    applicantLog.Date = DateTime.Now;
                    session.Save(applicantLog);
                }

                VacancyMilestoneLogs addLog = new VacancyMilestoneLogs();
                addLog.AdvertisementStatus = obj.AdvertisementStatus;
                addLog.Date = DateTime.Now;
                addLog.VacancyStatus = obj.Status;
                addLog.VacancyCode = obj.Code;
                addLog.ExecutorCode = obj.UpdatedBy;
                session.Save(addLog);

                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Vacancies UpdateVacancy(Vacancies obj)
        {
            return UpdateVacancy(obj, false);
        }

        public static Vacancies UpdateVacancy(Vacancies obj, bool resetApplicant)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<VacancyDisqualifierQuestions> disqList = GetVacancyDisqualifierQuestionList(obj.Code);
                foreach (VacancyDisqualifierQuestions item in disqList)
                    session.Delete(item);

                session.Update(obj);

                VacancyMilestoneLogs addLog = new VacancyMilestoneLogs();
                addLog.AdvertisementStatus = obj.AdvertisementStatus;
                addLog.VacancyCode = obj.Code;
                addLog.Date = DateTime.Now;
                addLog.VacancyStatus = obj.Status;
                addLog.ExecutorCode = obj.UpdatedBy;
                session.Save(addLog);

                if (obj.AdvertisementStatus == "Posted")
                {
                    if (string.IsNullOrEmpty(obj.PipelineCode))
                        throw new ApplicationException("Posted vacancy requires pipeline");
                }

                if (resetApplicant)
                {
                    List<ApplicantVacancies> listApplicantVacancieses = VacancyManager.GetApplicantVacancyListByVacancyCode(obj.Code);
                    foreach (ApplicantVacancies applicantVacancy in listApplicantVacancieses)
                    {
                        if (applicantVacancy.Active)
                        {
                            session.Delete("FROM ApplicantVacancies where ApplicantCode = '" + applicantVacancy.ApplicantCode + "' AND VacancyCode = '" + applicantVacancy.VacancyCode + "'");
                            session.Delete("FROM ApplicantsVacancyMilestoneLogs where ApplicantCode = '" + applicantVacancy.ApplicantCode + "' AND VacancyCode = '" + applicantVacancy.VacancyCode + "'");
                            
                            ApplicantVacancies newApplication = new ApplicantVacancies();
                            newApplication.AppliedDate = applicantVacancy.AppliedDate;
                            newApplication.CompanyCode = obj.CompanyCode;
                            newApplication.Active = true;
                            newApplication.UpdateStamp = DateTime.Now;
                            newApplication.ApplicantCode = applicantVacancy.ApplicantCode;
                            newApplication.VacancyCode = applicantVacancy.VacancyCode;
                    
                            VacancyManager.SaveApplicantVacancy(newApplication, transaction);   
                        }
                    }
                }

                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }
        }

        public static Vacancies UnPostAdvertisement(Vacancies obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                obj.PostDate = null;
                obj.ExpiredPostDate = null;
                obj.Status = "Open";
                obj.AdvertisementStatus = "Unposted";
                session.Update(obj);

                VacancyMilestoneLogs addLog = new VacancyMilestoneLogs();
                addLog.AdvertisementStatus = obj.AdvertisementStatus;
                addLog.Date = DateTime.Now;
                addLog.VacancyStatus = obj.Status;
                addLog.VacancyCode = obj.Code;
                addLog.ExecutorCode = obj.UpdatedBy;
                session.Save(addLog);

                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Vacancies PostAdvertisement(Vacancies obj, DateTime postDate, DateTime? expiredDate)
        {
            if (obj.NumberOfOpening == 0 || String.IsNullOrEmpty(obj.RecruitmentTargetGroup) || String.IsNullOrEmpty(obj.HiringManager) ||
                obj.ApprovedDate == null || String.IsNullOrEmpty(obj.NewPosition) || String.IsNullOrEmpty(obj.BusinessJustification) ||
                obj.PreferredStartDate == null || obj.PostDate == null || obj.ExpiredPostDate == null || String.IsNullOrEmpty(obj.JobRole) ||
                String.IsNullOrEmpty(obj.JobRequirement) || obj.SalaryRangeBottom == 0 || obj.SalaryRangeTop == 0 || String.IsNullOrEmpty(obj.JobDescription))
            {
                throw new ApplicationException("Job Vacancy is not yet complete, please complete vacancy data first.");
            }
            
            ITransaction transaction = session.BeginTransaction();
            try
            {
                obj.PostDate = postDate;
                obj.Status = "Open";
                obj.AdvertisementStatus = "Posted";
                obj.ExpiredPostDate = expiredDate;

                if (obj.ExpiredPostDate <= obj.PostDate)
                {
                    throw new ApplicationException("Invalid operation, expired posting date must be later than posting date");   
                }

                session.Update(obj);

                VacancyMilestoneLogs addLog = new VacancyMilestoneLogs();
                addLog.AdvertisementStatus = obj.AdvertisementStatus;
                addLog.Date = DateTime.Now;
                addLog.VacancyStatus = obj.Status;
                addLog.VacancyCode = obj.Code;
                addLog.ExecutorCode = obj.UpdatedBy;
                session.Save(addLog);
                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Vacancies SaveVacancy(Vacancies obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
                if (getSetting != null)
                {
                    if (getSetting.ApprovalFunction)
                        obj.Status = "Pending";
                    else
                        obj.Status = "Open";
                }
                else
                    obj.Status = "Open";
                obj.AdvertisementStatus = "Draft";
                session.Save(obj);

                Companies company = session.Get<Companies>(obj.CompanyCode);
                if (company == null)
                    throw new ApplicationException("Invalid operation, can not find company code: " + obj.CompanyCode);

                VacancyMilestoneLogs addLog = new VacancyMilestoneLogs();
                addLog.AdvertisementStatus = obj.AdvertisementStatus;
                addLog.Date = DateTime.Now;
                addLog.VacancyStatus = obj.Status;
                addLog.VacancyCode = obj.Code;
                addLog.ExecutorCode = obj.UpdatedBy;
                session.Save(addLog);

                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region vacancy disqualifier questions

        public static List<VacancyDisqualifierQuestions> GetVacancyDisqualifierQuestionList(string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacancyDisqualifierQuestions));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            return decrit.List<VacancyDisqualifierQuestions>() as List<VacancyDisqualifierQuestions>;
        }

        public static List<DisqualifierQuestions> GetVacancyQuestionList(string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacancyDisqualifierQuestions));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.SetProjection(Projections.Property("Question"));
            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }

        public static List<DisqualifierQuestions> GetSurveyQuestionList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(DisqualifierQuestions));
            decrit.Add(Expression.Eq("QuestionType", "Survey"));
            return decrit.List<DisqualifierQuestions>() as List<DisqualifierQuestions>;
        }

        public static ApplicantSurveySessions GetActiveApplicantSurveySession(string applicantCode, string companyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSurveySessions));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("CompanyCode", companyCode));
            decrit.Add(Expression.IsNull("CompletedDate"));
            decrit.AddOrder(Order.Asc("IssuedDate"));

            List<ApplicantSurveySessions> sessionList = decrit.List<ApplicantSurveySessions>() as List<ApplicantSurveySessions>;
            sessionList = sessionList.Where(a => (DateTime.Now - a.IssuedDate).TotalDays >= 30).ToList();

            return sessionList.FirstOrDefault();
        }

        public static List<ApplicantSurveySessions> ListActiveApplicantSurveySession(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSurveySessions));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.IsNull("CompletedDate"));

            return decrit.List<ApplicantSurveySessions>() as List<ApplicantSurveySessions>;
        }

        public static void SaveApplicantSurveySession(ApplicantSurveySessions surveySession)
        {
            SaveWithTransaction<ApplicantSurveySessions>(surveySession);
        }

        public static void UpdateApplicantSurveySession(ApplicantSurveySessions surveySession)
        {
            //UpdateWithTransaction<ApplicantSurveySessions>(surveySession);
            IQuery q = session.CreateSQLQuery("UPDATE ApplicantSurveySessions SET CompletedDate =:completedDate WHERE ApplicantCode=:applicantCode AND  CompletedDate IS NULL");
            q.SetParameter("completedDate", surveySession.CompletedDate);
            q.SetParameter("applicantCode", surveySession.ApplicantCode);
            //q.SetParameter("vacancyCode", surveySession.VacancyCode);
            q.ExecuteUpdate();
        }

        #endregion

        #region applicant vacancies

        public static int CountApplicant(string vacancyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));
            deCrit.CreateCriteria("Vacancy", "Vacancy");
            deCrit.Add(Expression.Eq("Vacancy.Active", true));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.SetProjection(Projections.Count("ApplicantCode"));
            return deCrit.UniqueResult<int>();
        }

        public static void Save(string applicantCode, string vacancyCode)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                Vacancies updateVacancy = GetVacancy(vacancyCode);
                updateVacancy.Applied += 1;
                session.Update(updateVacancy);

                ApplicantVacancies addApplicantVacancies = new ApplicantVacancies();
                addApplicantVacancies.ApplicantCode = applicantCode;
                addApplicantVacancies.VacancyCode = vacancyCode;
                addApplicantVacancies.AppliedDate = DateTime.Now;
                addApplicantVacancies.Status = "Process";
                addApplicantVacancies.CompanyCode = updateVacancy.CompanyCode;

                session.Save(addApplicantVacancies);


                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SaveApplicantVacancy(string applicantCode, string vacancyCode, bool correctAnswer)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                Vacancies updateVacancy = GetVacancy(vacancyCode);
                updateVacancy.Applied += 1;
                session.Update(updateVacancy);

                ApplicantVacancies addApplicantVacancies = new ApplicantVacancies();
                addApplicantVacancies.ApplicantCode = applicantCode;
                addApplicantVacancies.VacancyCode = vacancyCode;
                addApplicantVacancies.AppliedDate = DateTime.Now;
                addApplicantVacancies.CompanyCode = updateVacancy.CompanyCode;
                addApplicantVacancies.Active = true;
                if (correctAnswer == false)
                    addApplicantVacancies.Status = "AutoDisqualified";
                else
                {
                    PipelineSteps defaultStep = HiringManager.GetDefaultPipelineStep(updateVacancy.PipelineCode);
                    addApplicantVacancies.Status = defaultStep.StatusCode;
                }

                addApplicantVacancies.UpdateStamp = DateTime.Now;
                session.Save(addApplicantVacancies);


                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void SaveApplicantVacancy(string applicantCode, string vacancyCode,
                                            bool correctAnswer, string informationSource, string informationSourceNotes)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                Vacancies updateVacancy = GetVacancy(vacancyCode);
                updateVacancy.Applied += 1;
                session.Update(updateVacancy);

                ApplicantVacancies addApplicantVacancies = new ApplicantVacancies();
                addApplicantVacancies.ApplicantCode = applicantCode;
                addApplicantVacancies.VacancyCode = vacancyCode;
                addApplicantVacancies.AppliedDate = DateTime.Now;
                addApplicantVacancies.InformationSource = informationSource;
                addApplicantVacancies.InformationSourceNotes = informationSourceNotes;
                addApplicantVacancies.CompanyCode = updateVacancy.CompanyCode;

                if (correctAnswer == false)
                    addApplicantVacancies.Status = "AutoDisqualified";


                session.Save(addApplicantVacancies);


                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void ForwardApplicantVacany(ApplicantVacancies oldVacancy, ApplicantVacancies newVacancy)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                Vacancies getDestinationVacancy = ERecruitmentManager.GetVacancy(newVacancy.VacancyCode);
                getDestinationVacancy.Applied += 1;
                session.Update(getDestinationVacancy);

                newVacancy.Status = "Forwarded";
                session.Save(newVacancy);

                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Save(string applicantCode, string vacancyCode, string pipeLineStatus)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                Vacancies updateVacancy = GetVacancy(vacancyCode);
                updateVacancy.Applied += 1;
                session.Update(updateVacancy);

                ApplicantVacancies addApplicantVacancies = new ApplicantVacancies();
                addApplicantVacancies.ApplicantCode = applicantCode;
                addApplicantVacancies.VacancyCode = vacancyCode;
                addApplicantVacancies.AppliedDate = DateTime.Now;
                addApplicantVacancies.Status = pipeLineStatus;
                addApplicantVacancies.CompanyCode = updateVacancy.CompanyCode;

                session.Save(addApplicantVacancies);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Update(ApplicantVacancies applicantVacancy)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (applicantVacancy.Status == "Forwarded")
                {
                    Vacancies updateVacancy = GetVacancy(applicantVacancy.VacancyCode);
                    updateVacancy.Applied -= 1;
                    session.Update(updateVacancy);
                }

                session.Update(applicantVacancy);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ApplicantVacancies GetApplicantVacancies(string applicantCode, string vacancyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            return deCrit.UniqueResult<ApplicantVacancies>();
        }

        public static ApplicantVacancies GetApplicantVacancy(string applicantCode, string vacancyCode, string status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            deCrit.Add(Expression.Eq("Status", status));
            return deCrit.UniqueResult<ApplicantVacancies>();
        }

        public static List<ApplicantVacancies> GetApplicantVacancyListByVacancyCode(string vacancyCode, string status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));

            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(status))
                deCrit.Add(Expression.Eq("Status", status));
            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static List<ApplicantVacancies> GetActiveApplicantVacancyListByVacancyCode(string vacancyCode, string status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));

            deCrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(status))
                deCrit.Add(Expression.Eq("Status", status));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static List<ApplicantVacancies> GetApplicantVacancyListByApplicantCode(string applicantCode, string status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));

            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            if (!string.IsNullOrEmpty(status))
                deCrit.Add(Expression.Eq("Status", status));
            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static List<ApplicantVacancies> GetApplicantVacancyListByApplicantCode(string applicantCode, string[] status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));

            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.In("Status", status));

            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static List<ApplicantVacancies> GetApplicantVacancyForCheckActiveApply(string applicantCode, string[] status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));

            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Restrictions.Not(Restrictions.Eq("VacancyStatus", "Closed")));
            deCrit.Add(Expression.In("Status", status));

            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }


        public static List<ApplicantVacancies> GetApplicantVacancies(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static List<ApplicantVacancies> GetApplicantVacancyList(string applicantCode,
                                                                        string globalSearch,
                                                                        int startRecord,
                                                                        int length,
                                                                        string sortColumn,
                                                                        string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.CreateAlias("Vacancy", "Vacancy");
            if (!string.IsNullOrEmpty(globalSearch))
            {

                decrit.Add(Expression.InsensitiveLike("Vacancy.PositionName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    if (mappingProperty[0] == "Vacancy")
                    {
                        if (sortDir == "asc")
                            decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                        else
                            decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    }
                    else
                    {
                        decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                        if (sortDir == "asc")
                            decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                        else
                            decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    }
                }

                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            return decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static int CountApplicantVacancyList(string applicantCode,
                                                      string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.CreateAlias("Vacancy", "Vacancy");
            if (!string.IsNullOrEmpty(globalSearch))
            {

                decrit.Add(Expression.InsensitiveLike("Vacancy.PositionName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }


        public static List<ApplicantVacancies> GetApplicantVacancies()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));
            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static List<ApplicantVacancies> GetReceivedApplicantList(string category, string status)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantVacancies));
            deCrit.CreateCriteria("Vacancy", "Vacancy");
            if (!string.IsNullOrEmpty(category) && category != "0")
                deCrit.Add(Expression.Eq("Vacancy.CategoryCode", category));
            if (!string.IsNullOrEmpty(status) && status != "0")
                deCrit.Add(Expression.Eq("Status", status));
            return deCrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static List<ApplicantVacancies> GetApplicantVacancyListByVacancy(string vacancyCode,
                                                                        string globalSearch,
                                                                        int startRecord,
                                                                        int length,
                                                                        string sortColumn,
                                                                        string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.CreateAlias("Vacancy", "Vacancy");
            decrit.CreateAlias("Vacancy.Company", "Company");
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("CompanyName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static int CountApplicantVacancyListByVacancy(string vacancyCode,
                                                              string globalSearch
                                                                        )
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.CreateAlias("Vacancy", "Vacancy");
            decrit.CreateAlias("Vacancy.Company", "Company");
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("CompanyName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }


        public static List<ApplicantVacancies> GetApplicantVacancyListByVacancy(string vacancyCode,
                                                                                string status,
                                                                                string genderCode,
                                                                                string maritalStatusCode,
                                                                                string applicantFlag,
                                                                                string globalSearch,
                                                                                int startRecord,
                                                                                int length,
                                                                                string sortColumn,
                                                                                string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.CreateAlias("Vacancy", "Vacancy");
            decrit.CreateAlias("Applicant", "Applicant");

            if (!string.IsNullOrEmpty(status))
                decrit.Add(Expression.Eq("Status", status));

            if (!string.IsNullOrEmpty(genderCode))
                decrit.Add(Expression.Eq("Applicant.GenderSpecification", genderCode));

            if (!string.IsNullOrEmpty(maritalStatusCode))
                decrit.Add(Expression.Eq("Applicant.MaritalStatusSpecification", maritalStatusCode));
            if (string.IsNullOrEmpty(applicantFlag))
                decrit.Add(Expression.IsNull("Applicant.FlagStatus"));
            else
                decrit.Add(Expression.Eq("Applicant.FlagStatus", applicantFlag));
            if (!string.IsNullOrEmpty(globalSearch))
                decrit.Add(Expression.InsensitiveLike("Vacancy.CompanyName", globalSearch, MatchMode.Anywhere));

            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            decrit.AddOrder(Order.Desc("RankWeight"));
            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static int CountApplicantVacancyListByVacancy(string vacancyCode,
                                                             string status,
                                                             string genderCode,
                                                             string maritalStatusCode,
                                                             string applicantFlag,
                                                             string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.CreateAlias("Vacancy", "Vacancy");
            decrit.CreateAlias("Applicant", "Applicant");

            if (!string.IsNullOrEmpty(status))
                decrit.Add(Expression.Eq("Status", status));

            if (!string.IsNullOrEmpty(genderCode))
                decrit.Add(Expression.Eq("Applicant.GenderSpecification", genderCode));

            if (!string.IsNullOrEmpty(maritalStatusCode))
                decrit.Add(Expression.Eq("Applicant.MaritalStatusSpecification", maritalStatusCode));
            if (string.IsNullOrEmpty(applicantFlag))
                decrit.Add(Expression.IsNull("Applicant.FlagStatus"));
            else
                decrit.Add(Expression.Eq("Applicant.FlagStatus", applicantFlag));

            if (!string.IsNullOrEmpty(globalSearch))
                decrit.Add(Expression.InsensitiveLike("Vacancy.CompanyName", globalSearch, MatchMode.Anywhere));

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }
        public static List<ApplicantVacancies> GetApplicantNameListByVacancy(string vacancyCode, string status,
                                                                            string globalSearch,
                                                                            int startRecord,
                                                                            int length,
                                                                            string sortColumn,
                                                                            string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.CreateAlias("Applicant", "Applicant");
            if (!string.IsNullOrEmpty(status))
                decrit.Add(Expression.Eq("Status", status));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.Or(Expression.InsensitiveLike("Applicant.FirstName", globalSearch, MatchMode.Anywhere),
                    Expression.InsensitiveLike("Applicant.LastName", globalSearch, MatchMode.Anywhere)));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        public static int CountApplicantNameListByVacancy(string vacancyCode, string status,
                                                              string globalSearch
                                                                        )
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            if (!string.IsNullOrEmpty(status))
                decrit.Add(Expression.Eq("Status", status));
            decrit.CreateAlias("Applicant", "Applicant");
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Applicant.Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }

        public static List<ApplicantVacancies> GetApplicantVacancyListByVacancy(string vacancyCode, string excludeStatus)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Not(Expression.Eq("Status", excludeStatus)));

            return decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }


        public static void SaveVacancyApplicantTest(ApplicantVacancyTestResults testResult)
        {
            SaveWithTransaction<ApplicantVacancyTestResults>(testResult);
        }

        public static void SaveVacancyApplicantAttachment(ApplicantVacancyAttachmentFiles fileName)
        {
            SaveWithTransaction<ApplicantVacancyAttachmentFiles>(fileName);
        }

        public static void DeleteVacancyApplicantTest(ApplicantVacancyTestResults testResult)
        {
            DeleteWithTransaction<ApplicantVacancyTestResults>(testResult);
        }

        public static void DeleteVacancyApplicantAttachment(ApplicantVacancyAttachmentFiles fileName)
        {
            DeleteWithTransaction<ApplicantVacancyAttachmentFiles>(fileName);
        }

        public static List<ApplicantVacancyTestResults> GetVacancyApplicantTestList(string applicantCode, string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyTestResults));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));

            return decrit.List<ApplicantVacancyTestResults>() as List<ApplicantVacancyTestResults>;
        }

        public static ApplicantVacancyTestResults GetVacancyApplicantTest(string applicantCode, string vacancyCode, string testCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyTestResults));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("TestCode", testCode));
            return decrit.UniqueResult<ApplicantVacancyTestResults>();
        }

        public static List<ApplicantVacancyAttachmentFiles> GetVacancyApplicantAttachmentList(string applicantCode, string vacancyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyAttachmentFiles));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));

            return decrit.List<ApplicantVacancyAttachmentFiles>() as List<ApplicantVacancyAttachmentFiles>;
        }

        public static ApplicantVacancyAttachmentFiles GetVacancyApplicantAttachment(string applicantCode, string vacancyCode, string attachmentCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyAttachmentFiles));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("VacancyCode", vacancyCode));
            decrit.Add(Expression.Eq("AttachmentCode", attachmentCode));
            return decrit.UniqueResult<ApplicantVacancyAttachmentFiles>();
        }

        #endregion

        #region company industries
        public static CompanyIndustries GetCompanyIndustry(string companyCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(CompanyIndustries));
            deCrit.Add(Expression.Eq
                ("CompanyCode", companyCode));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<CompanyIndustries>();
        }
        #endregion

        #region applicant educations

        public static List<ApplicantEducations> GetApplicantEducations(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantEducations));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return deCrit.List<ApplicantEducations>() as List<ApplicantEducations>;
        }


        public static List<ApplicantEducations> GetApplicantEducationList(string applicantCode,
                                                                          string globalSearch,
                                                                          int startRecord,
                                                                          int length,
                                                                          string sortColumn,
                                                                          string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantEducations));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("InstitutionName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<ApplicantEducations>() as List<ApplicantEducations>;
        }


        public static List<ApplicantEducations> GetApplicantEducationList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantEducations));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            return decrit.List<ApplicantEducations>() as List<ApplicantEducations>;
        }


        public static int CountApplicantEducationList(string applicantCode,
                                                      string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantEducations));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("InstitutionName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }

        public static ApplicantEducations GetApplicantEducation(string applicantCode, int number)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantEducations));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("Number", number));
            return deCrit.UniqueResult<ApplicantEducations>();
        }

        public static int GetLatestNumberApplicantEducations(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantEducations));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.AddOrder(Order.Desc("Number"));
            deCrit.SetProjection(Projections.Property("Number"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<int>();
        }

        public static int CountApplicantEducations(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantEducations));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.SetProjection(Projections.Count("ApplicantCode"));
            return deCrit.UniqueResult<int>();
        }
        public static void Remove(ApplicantEducations obj)
        {
            DeleteWithTransaction<ApplicantEducations>(obj);
        }

        public static void Save(ApplicantEducations obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Save(obj);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public static void Update(ApplicantEducations obj)
        {
            UpdateWithTransaction<ApplicantEducations>(obj);
        }
        #endregion

        #region applicant experiences


        public static int CountApplicantExperience(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantExperiences));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.SetProjection(Projections.Count("ApplicantCode"));
            return deCrit.UniqueResult<int>();
        }

        public static int GetLatestNumberApplicantExperiences(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantExperiences));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.AddOrder(Order.Desc("Number"));
            deCrit.SetProjection(Projections.Property("Number"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<int>();
        }

        public static List<ApplicantExperiences> GetApplicantExperiences(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantExperiences));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return deCrit.List<ApplicantExperiences>() as List<ApplicantExperiences>;
        }
        public static ApplicantExperiences GetApplicantExperience(string applicantCode, int number)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantExperiences));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("Number", number));
            return deCrit.UniqueResult<ApplicantExperiences>();
        }
        public static void Remove(ApplicantExperiences obj)
        {
            DeleteWithTransaction<ApplicantExperiences>(obj);
        }

        public static void Save(ApplicantExperiences obj)
        {
            SaveWithTransaction<ApplicantExperiences>(obj);
        }

        public static void Update(ApplicantExperiences obj)
        {
            UpdateWithTransaction<ApplicantExperiences>(obj);
        }

        public static List<ApplicantExperiences> GetApplicantExperienceList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantExperiences));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            return decrit.List<ApplicantExperiences>() as List<ApplicantExperiences>;
        }

        public static List<ApplicantExperiences> GetApplicantExperienceList(string applicantCode,
                                                                        string globalSearch,
                                                                        int startRecord,
                                                                        int length,
                                                                        string sortColumn,
                                                                        string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantExperiences));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                if (sortDir == "asc")
                    decrit.AddOrder(Order.Asc(sortColumn));
                else
                    decrit.AddOrder(Order.Desc(sortColumn));
            }

            return decrit.List<ApplicantExperiences>() as List<ApplicantExperiences>;
        }


        public static int CountApplicantExperienceList(string applicantCode,
                                                      string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantExperiences));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("PositionName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }

        #endregion

        #region applicant skills

        public static int CountApplicantSkills(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantSkills));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.SetProjection(Projections.Count("ApplicantCode"));
            return deCrit.UniqueResult<int>();
        }

        public static int GetLatestNumberApplicantSkills(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantSkills));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.AddOrder(Order.Desc("Number"));
            deCrit.SetProjection(Projections.Property("Number"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<int>();
        }

        public static List<ApplicantSkills> GetApplicantSkills(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantSkills));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return deCrit.List<ApplicantSkills>() as List<ApplicantSkills>;
        }
        public static ApplicantSkills GetApplicantSkill(string applicantCode, int number)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantSkills));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("Number", number));
            return deCrit.UniqueResult<ApplicantSkills>();
        }
        public static void Remove(ApplicantSkills obj)
        {
            DeleteWithTransaction<ApplicantSkills>(obj);
        }

        public static void Save(ApplicantSkills obj)
        {
            SaveWithTransaction<ApplicantSkills>(obj);
        }

        public static void Update(ApplicantSkills obj)
        {
            UpdateWithTransaction<ApplicantSkills>(obj);
        }

        public static List<ApplicantSkills> GetApplicantSkillList(string applicantCode,
                                                                       string globalSearch,
                                                                       int startRecord,
                                                                       int length,
                                                                       string sortColumn,
                                                                       string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSkills));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("SkillName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                if (sortDir == "asc")
                    decrit.AddOrder(Order.Asc(sortColumn));
                else
                    decrit.AddOrder(Order.Desc(sortColumn));
            }

            return decrit.List<ApplicantSkills>() as List<ApplicantSkills>;
        }


        public static int CountApplicantSkillList(string applicantCode,
                                                      string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSkills));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("SkillName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }



        #endregion

        #region applicant trainings

        public static int GetLatestNumberApplicantTraining(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantTrainings));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.AddOrder(Order.Desc("Number"));
            deCrit.SetProjection(Projections.Property("Number"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<int>();
        }


        public static List<ApplicantTrainings> GetApplicantTrainings(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantTrainings));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return deCrit.List<ApplicantTrainings>() as List<ApplicantTrainings>;
        }
        public static ApplicantTrainings GetApplicantTraining(string applicantCode, int number)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantTrainings));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("Number", number));
            return deCrit.UniqueResult<ApplicantTrainings>();
        }
        public static void Remove(ApplicantTrainings obj)
        {
            DeleteWithTransaction<ApplicantTrainings>(obj);
        }

        public static void Save(ApplicantTrainings obj)
        {
            SaveWithTransaction<ApplicantTrainings>(obj);
        }

        public static void Update(ApplicantTrainings obj)
        {
            UpdateWithTransaction<ApplicantTrainings>(obj);
        }
        public static List<ApplicantTrainings> GetApplicantTrainingList(string applicantCode,
                                                                        string globalSearch,
                                                                        int startRecord,
                                                                        int length,
                                                                        string sortColumn,
                                                                        string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantTrainings));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("TrainingName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                if (sortDir == "asc")
                    decrit.AddOrder(Order.Asc(sortColumn));
                else
                    decrit.AddOrder(Order.Desc(sortColumn));
            }

            return decrit.List<ApplicantTrainings>() as List<ApplicantTrainings>;
        }

        public static int CountApplicantTrainingList(string applicantCode,
                                                      string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantTrainings));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("TrainingName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }
        #endregion

        #region attachment

        public static ApplicantAttachments SaveApplicantAttachment(ApplicantAttachments obj)
        {
            SaveWithTransaction<ApplicantAttachments>(obj);
            return obj;
        }
        public static ApplicantAttachments GetApplicantAttachment(int id)
        {
            return session.Get<ApplicantAttachments>(id);
        }
        public static ApplicantAttachments RemoveApplicantAttachment(ApplicantAttachments obj)
        {
            DeleteWithTransaction<ApplicantAttachments>(obj);
            return obj;
        }
        public static void RemoveCurriculumVitaes(CurriculumVitaes obj)
        {
            DeleteWithTransaction<CurriculumVitaes>(obj);
        }
        public static CurriculumVitaes GetCurriculumVitaes(int id)
        {
            return session.Get<CurriculumVitaes>(id);
        }

        public static List<ApplicantAttachments> GetApplicantAttachmentList(string applicantCode, string globalSearch,
                                                                        int startRecord,
                                                                        int length,
                                                                        string sortColumn,
                                                                        string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAttachments));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("FileName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<ApplicantAttachments>() as List<ApplicantAttachments>;
        }

        public static int CountApplicantAttachment(string applicantCode,
                                                string globalSearch
                                                  )
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAttachments));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("FileName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Id"));

            return decrit.UniqueResult<int>();
        }

        public static CurriculumVitaes SaveCurriculumVitaes(CurriculumVitaes obj)
        {
            SaveWithTransaction<CurriculumVitaes>(obj);
            return obj;
        }
        public static List<CurriculumVitaes> GetApplicantCurriculumVitaes(string applicantCode, string globalSearch,
                                                                        int startRecord,
                                                                        int length,
                                                                        string sortColumn,
                                                                        string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(CurriculumVitaes));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("FileName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<CurriculumVitaes>() as List<CurriculumVitaes>;
        }
        public static int CountCurriculumVitaes(string applicantCode,
                                                 string globalSearch
                                                   )
        {
            ICriteria decrit = session.CreateCriteria(typeof(CurriculumVitaes));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("FileName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Id"));

            return decrit.UniqueResult<int>();
        }

        #endregion

        #region applicant award

        public static List<ApplicantAwards> GetApplicantAwards(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantAwards));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return deCrit.List<ApplicantAwards>() as List<ApplicantAwards>;
        }
        public static ApplicantAwards GetApplicantAward(string applicantCode, int number)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantAwards));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.Add(Expression.Eq("Number", number));
            return deCrit.UniqueResult<ApplicantAwards>();
        }
        public static int GetLatestNumberApplicantAward(string applicantCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicantAwards));
            deCrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            deCrit.AddOrder(Order.Desc("Number"));
            deCrit.SetProjection(Projections.Property("Number"));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<int>();
        }

        public static void Remove(ApplicantAwards obj)
        {
            DeleteWithTransaction<ApplicantAwards>(obj);
        }

        public static void Save(ApplicantAwards obj)
        {
            SaveWithTransaction<ApplicantAwards>(obj);
        }

        public static void Update(ApplicantAwards obj)
        {
            UpdateWithTransaction<ApplicantAwards>(obj);
        }

        public static List<ApplicantAwards> GetApplicantAwardList(string applicantCode,
                                                                          string globalSearch,
                                                                          int startRecord,
                                                                          int length,
                                                                          string sortColumn,
                                                                          string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAwards));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("AwardName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                if (sortDir == "asc")
                    decrit.AddOrder(Order.Asc(sortColumn));
                else
                    decrit.AddOrder(Order.Desc(sortColumn));
            }

            return decrit.List<ApplicantAwards>() as List<ApplicantAwards>;
        }

        public static int CountApplicantAwardList(string applicantCode,
                                                      string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAwards));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("AwardName", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }


        #endregion        

        #region vacancy status

        public static List<VacancyStatus> GetPipelineStatusList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));
            deCrit.Add(Expression.Eq("IsPipelineStatus", true));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<VacancyStatus>() as List<VacancyStatus>;
        }

        public static void UpdateRemoveableVacancyStatus()
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                var query = session.CreateSQLQuery("update vs set Removable = 1 from VacancyStatus vs left join PipelineSteps ps on ps.StatusCode = vs.Code left join Pipelines p on p.Code = ps.PipelineCode where (p.Active = 0 or p.Code is null) and Removable = 0");
                object result = query.UniqueResult();
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static VacancyStatus GetVacancyStatus(string code)
        {
            return session.Get<VacancyStatus>(code);
        }

        public static VacancyStatus GetVacancyStatusByRecruiterLabel(string internalLabel)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));
            deCrit.Add(Expression.Eq("RecruiterLabel", internalLabel));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.UniqueResult<VacancyStatus>();
        }

        public static VacancyStatus SaveVacancyStatus(VacancyStatus obj)
        {
            //obj.Removable = true;
            SaveWithTransaction<VacancyStatus>(obj);
            return obj;
        }

        public static VacancyStatus RemoveVacancyStatus(VacancyStatus obj)
        {
            if (!obj.Removable)
                throw new ApplicationException("Can not remove default recruitment process");

            obj.Active = false;
            UpdateWithTransaction<VacancyStatus>(obj);
            return obj;
        }

        public static VacancyStatus UpdateVacancyStatus(VacancyStatus obj)
        {
            UpdateWithTransaction<VacancyStatus>(obj);
            return obj;
        }

        public static List<VacancyStatus> GetActiveVacancyStatusList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));
            deCrit.Add(Expression.Eq("Active", true));

            return deCrit.List<VacancyStatus>() as List<VacancyStatus>;
        }

        public static List<VacancyStatus> GetVacancyStatusList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancyStatus));

            return deCrit.List<VacancyStatus>() as List<VacancyStatus>;
        }

        public static List<VacancyStatus> GetVacancyStatusList(string globalSearch,
                                                               int startRecord,
                                                               int length,
                                                               string sortColumn,
                                                               string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacancyStatus));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<VacancyStatus>() as List<VacancyStatus>;
        }

        public static int CountVacancyStatusList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(VacancyStatus));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region vacancy steps

        public static List<VacancySteps> GetVacancySteps()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(VacancySteps));
            return deCrit.List<VacancySteps>() as List<VacancySteps>;
        }
        #endregion

        #region jobs

        public static List<Jobs> GetJobsList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Jobs));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Jobs>() as List<Jobs>;
        }
        public static Jobs SaveJob(Jobs obj)
        {
            SaveWithTransaction<Jobs>(obj);
            return obj;
        }
        public static Jobs GetJob(string code)
        {
            return session.Get<Jobs>(code);
        }
        public static Jobs UpdateJob(Jobs obj)
        {
            UpdateWithTransaction<Jobs>(obj);
            return obj;
        }
        public static Jobs RemoveJob(Jobs obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Jobs>(obj);
            return obj;
        }
        public static List<Jobs> GetJobList(string globalSearch,
                                                            int startRecord,
                                                            int length,
                                                            string sortColumn,
                                                            string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Jobs));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Jobs>() as List<Jobs>;
        }

        public static int CountJobList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Jobs));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }
        #endregion

        #region universities

        public static List<Universities> GetUniversityByCountryCode(string countryCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Universities));
            decrit.Add(Expression.Eq("CountryCode", countryCode));
            decrit.AddOrder(Order.Asc("Name"));
            return decrit.List<Universities>() as List<Universities>;
        }

        public static List<Universities> GetAffiliatedUniversityList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Universities));
            decrit.Add(Expression.Eq("IsAffiliated", true));
            decrit.AddOrder(Order.Asc("Name"));
            return decrit.List<Universities>() as List<Universities>;
        }

        public static Universities SaveUniversity(Universities obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.Save(obj);

                foreach (UniversityMajors major in obj.MajorList)
                {
                    major.UniversityCode = obj.Code;
                    session.Save(major);
                }

                transaction.Commit();

                return obj;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static Universities UpdateUniversity(Universities obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                List<UniversityMajors> existingMajorList = GetUniversityMajorList(obj.Code);
                foreach (UniversityMajors major in existingMajorList)
                    session.Delete(major);

                session.Update(obj);

                foreach (UniversityMajors major in obj.MajorList)
                {
                    major.UniversityCode = obj.Code;
                    session.Update(major);
                }

                transaction.Commit();

                return obj;
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static List<Universities> GetUniversityList()
        {

            ICriteria decrit = session.CreateCriteria(typeof(Universities));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("Name"));
            return decrit.List<Universities>() as List<Universities>;
        }

        public static List<Universities> GetUniversityListDashboard()
        {
            ProjectionList projectionList2 = Projections.ProjectionList();
            projectionList2
              .Add(Projections.Alias(Projections.Property("InstitutionCode"), "Code"))
               .Add(Projections.Alias(Projections.Property("InstitutionName"), "Name")); // const projection

            ICriteria decrit = session.CreateCriteria(typeof(ApplicantEducations));

            decrit.SetProjection(Projections.Distinct(projectionList2));
            decrit.SetResultTransformer(Transformers.AliasToBean(typeof(Universities)));
            decrit.AddOrder(Order.Asc("Code"));
            return decrit.List<Universities>() as List<Universities>;
        }

        public static List<Universities> GetUniversityListByKategori(int kategori)
        {

            ICriteria decrit = session.CreateCriteria(typeof(Universities));
            decrit.Add(Expression.Eq("Active", true));
            decrit.Add(Expression.Eq("Kategori", kategori));
            decrit.AddOrder(Order.Asc("Code"));
            return decrit.List<Universities>() as List<Universities>;
        }


        public static List<Universities> GetUniversityList(string globalSearch,
                                                            int startRecord,
                                                            int length,
                                                            string sortColumn,
                                                            string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Universities));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Universities>() as List<Universities>;
        }

        public static List<Universities> GetUniversityListByKategori(string globalSearch,
                                                    int startRecord,
                                                    int length,
                                                    int kategori,
                                                    string sortColumn,
                                                    string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Universities));
            if (kategori != 0)
                decrit.Add(Expression.Eq("Kategori", kategori));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetFirstResult(startRecord).SetMaxResults(length);


            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Universities>() as List<Universities>;
        }

        public static int CountUniversityList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Universities));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        public static int CountUniversityListByKategori(string globalSearch, int kategori)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Universities));
            if (kategori != 0)
                decrit.Add(Expression.Eq("Kategori", kategori));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            decrit.SetProjection(Projections.Count("Code"));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.UniqueResult<int>();
        }

        public static Universities GetUniversity(string code)
        {
            return session.Get<Universities>(code);
        }

        public static List<UniversityMajors> GetUniversityMajorList(string universityCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(UniversityMajors));
            decrit.Add(Expression.Eq("UniversityCode", universityCode));
            decrit.AddOrder(Order.Asc("MajorName"));
            return decrit.List<UniversityMajors>() as List<UniversityMajors>;
        }

        #endregion

        #region statistic dashboard

        public static int CountApplicantVacanyByInformationSource(string informationSource)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("InformationSource", informationSource));

            decrit.SetProjection(Projections.Count("ApplicantCode"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByCompany(string companyCode, string status, string[] companiesList)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("CompanyCode", companyCode));
            decrit.Add(Expression.Eq("Active", true));
            decrit.Add(Expression.In("CompanyCode", companiesList));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }

            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByJobCode(string jobCode, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("JobCode", jobCode));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }

            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByRequiredEducationLevel(string educationLevel, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("RequiredEducationLevel", educationLevel));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }

            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByDepartment(string department, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("Department", department));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }

            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByDivision(string division, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("Division", division));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }

            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByPositionType(string positionType, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("NewPosition", positionType));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }

            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByCountryCode(string countryCode, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("Country", countryCode));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByCategory(string categoryCode, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("CategoryCode", categoryCode));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByLocation(string locationCode, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("Location", locationCode));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByEmploymentType(string employmentType, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("TypeOfEmployment", employmentType));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByTargetGroup(string targetGroup, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Eq("RecruitmentTargetGroup", targetGroup));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByPostDate(DateTime startPostDate, DateTime endPostDate, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Ge("EffectiveDate", startPostDate));
            decrit.Add(Expression.Lt("EffectiveDate", endPostDate));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountVacancyByRegisteredDateRange(DateTime startDate, DateTime endDate, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Vacancies));
            decrit.Add(Expression.Ge("EffectiveDate", startDate));
            decrit.Add(Expression.Lt("EffectiveDate", endDate));
            if (!string.IsNullOrEmpty(status))
            {
                decrit.Add(Expression.Eq("Status", status));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        public static int CountApplicantByRegisteredDateRange(DateTime startDate, DateTime endDate, string status)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Ge("RegisteredDate", startDate));
            decrit.Add(Expression.Lt("RegisteredDate", endDate));
            if (!string.IsNullOrEmpty(status))
            {
                if (status == "True")
                    decrit.Add(Expression.Eq("Verified", true));
                else
                    decrit.Add(Expression.Eq("Verified", false));
            }
            decrit.SetProjection(Projections.Count("Code"));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region license setting

        public static ApplicationSettings GetApplicationSetting()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicationSettings));
            deCrit.SetMaxResults(1);
            return deCrit.UniqueResult<ApplicationSettings>();
        }
        public static ApplicationSettings SaveApplicationSetting(ApplicationSettings obj)
        {
            SaveWithTransaction<ApplicationSettings>(obj);
            return obj;
        }
        public static ApplicationSettings UpdateApplicationSetting(ApplicationSettings obj)
        {
            UpdateWithTransaction<ApplicationSettings>(obj);
            return obj;
        }
        #endregion

        #region email

        public static Emails SaveEmail(Emails obj)
        {
            SaveWithTransaction<Emails>(obj);
            return obj;
        }
        public static Emails RemoveEmail(Emails obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Emails>(obj);
            return obj;
        }
        public static Emails GetEmail(int id)
        {
            return session.Get<Emails>(id);
        }
        public static Emails UpdateEmail(Emails obj)
        {
            UpdateWithTransaction<Emails>(obj);
            return obj;
        }
        public static List<Emails> GetEmailList(string globalSearch, int startRecord,
                                                int length, string sortColumn,
                                                 string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Emails));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("TemplateName", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<Emails>() as List<Emails>;
        }

        public static int CountEmailList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Emails));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("TemplateName", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Id"));
            return decrit.UniqueResult<int>();
        }

        public static List<Emails> GetEmailList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Emails));
            decrit.Add(Expression.Eq("Active", true));

            return decrit.List<Emails>() as List<Emails>;
        }

        public static List<Emails> GetEmailListEmpty()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Emails));
            // decrit.Add(Expression.Eq("Category", category));
            decrit.Add(Expression.Eq("Active", true));
            IList<Emails> emails = decrit.List<Emails>();
            emails.Insert(0, new Emails()
            {
                Id = 0,
                TemplateName = "-- Select --"
            });
            return (List<Emails>) emails;
        }

        public static List<Emails> GetEmailListByCategory(string category)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Emails));
            // decrit.Add(Expression.Eq("Category", category));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<Emails>() as List<Emails>;
        }

        public static List<Emails> GetEmailListByCategoryEmpty(string category)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Emails));
            // decrit.Add(Expression.Eq("Category", category));
            decrit.Add(Expression.Eq("Active", true));
            IList<Emails> emails = decrit.List<Emails>();
            emails.Insert(0, new Emails()
            {
                Id = 0,
                TemplateName = "-- Select --"
            });
            return (List<Emails>) emails;
        }

        #endregion

        #region roles

        public static List<RoleCompanyAccessList> GetCompanyAccessList(string roleCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(RoleCompanyAccessList));
            deCrit.Add(Expression.Eq("RoleCode", roleCode));
            return deCrit.List<RoleCompanyAccessList>() as List<RoleCompanyAccessList>;
        }

        public static List<Companies> GetGrantedCompanyListByRoleCode(string roleCode)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(RoleCompanyAccessList));
            deCrit.Add(Expression.Eq("RoleCode", roleCode));
            deCrit.SetProjection(Projections.Property("Company"));
            return deCrit.List<Companies>() as List<Companies>;
        }

        public static List<Companies> GetCompanyListByCodes(string[] companyCodes)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Companies));
            deCrit.Add(Expression.In("Code", companyCodes));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Companies>() as List<Companies>;
        }

        public static Roles SaveRole(Roles obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                obj.RoleCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                session.Save(obj);
                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static Roles RemoveRole(Roles obj)
        {
            obj.Active = false;
            UpdateWithTransaction<Roles>(obj);
            return obj;
        }

        public static Roles UpdateRole(Roles obj)
        {
            ITransaction transaction = session.BeginTransaction();
            {
                session.Update(obj);
                transaction.Commit();
            }
            return obj;
        }

        public static Roles GetRole(string code)
        {
            return session.Get<Roles>(code);
        }

        public static List<Roles> GetRoleList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Roles));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Roles>() as List<Roles>;

        }

        public static List<Roles> GetRoleList(string[] codeList)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Roles));
            deCrit.Add(Expression.In("RoleCode", codeList));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<Roles>() as List<Roles>;

        }

        public static List<Roles> GetRoleList(string globalSearch, int startRecord,
                                                int length, string sortColumn,
                                                 string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Roles));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<Roles>() as List<Roles>;
        }

        public static int CountRoleList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Roles));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("RoleCode"));
            return decrit.UniqueResult<int>();
        }

        #endregion

        #region messages

        public static SMSTemplates SaveSMSTemplate(SMSTemplates obj)
        {
            SaveWithTransaction<SMSTemplates>(obj);
            return obj;
        }
        public static SMSTemplates GetSMSTemplate(int id)
        {
            return session.Get<SMSTemplates>(id);
        }
        public static SMSTemplates UpdateSMSTemplate(SMSTemplates obj)
        {
            UpdateWithTransaction<SMSTemplates>(obj);
            return obj;
        }
        public static SMSTemplates RemoveSMSTemplate(SMSTemplates obj)
        {
            obj.Active = false;
            UpdateWithTransaction<SMSTemplates>(obj);
            return obj;
        }
        public static List<SMSTemplates> GetSMSTemplateList(string globalSearch, int startRecord,
                                               int length, string sortColumn,
                                                string sortDir)
        {
            ICriteria decrit = session.CreateCriteria(typeof(SMSTemplates));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("TemplateName", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    decrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        decrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        decrit.AddOrder(Order.Asc(sortColumn));
                    else
                        decrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return decrit.List<SMSTemplates>() as List<SMSTemplates>;
        }

        public static int CountSMSTemplateList(string globalSearch)
        {
            ICriteria decrit = session.CreateCriteria(typeof(SMSTemplates));
            decrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                decrit.Add(Expression.InsensitiveLike("TemplateName", globalSearch, MatchMode.Anywhere));
            }
            decrit.SetProjection(Projections.Count("Id"));
            return decrit.UniqueResult<int>();
        }

        public static List<SMSTemplates> GetSMSTemplateList()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(SMSTemplates));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<SMSTemplates>() as List<SMSTemplates>;

        }

        #endregion

        #region ReportAutomatic

        public static List<ReportAutomatic> GetReportAutoByCode(string code)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ReportAutomatic));
            decrit.Add(Expression.Eq("Code", code));
            decrit.AddOrder(Order.Asc("Report"));
            return decrit.List<ReportAutomatic>() as List<ReportAutomatic>;
        }

        public static Reports GetReportByCode(string code)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Reports));
            decrit.Add(Expression.Eq("Code", code));
            return decrit.UniqueResult<Reports>();
        }

        public static ReportAutomatic SaveReportAuto(ReportAutomatic obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                obj.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                session.Save(obj);
                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static ReportAutomatic RemoveReportAuto(ReportAutomatic obj)
        {
            DeleteWithTransaction<ReportAutomatic>(obj);
            return obj;
        }

        public static ReportAutomatic UpdateReportAuto(ReportAutomatic obj)
        {
            ITransaction transaction = session.BeginTransaction();
            {
                session.Update(obj);
                transaction.Commit();
            }
            return obj;
        }

        public static List<ReportAutomatic> GetReportAutomatic()
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ReportAutomatic));
            return deCrit.List<ReportAutomatic>() as List<ReportAutomatic>;

        }

        public static String GetReports(string code)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Reports));
            deCrit.Add(Expression.Eq("Code", code));
            List<Reports> list = deCrit.List<Reports>() as List<Reports>;
            if (list != null && list.Count > 0) {
                return list[0].Name;
            }
            return code;

        }

        public static List<ReportAutomatic> GetReportAutomaticList(string globalSearch, int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir, string startDate, string endDate)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ReportAutomatic));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("ReportName", globalSearch, MatchMode.Anywhere));
                
            }

            if (!string.IsNullOrEmpty(startDate)) {
                deCrit.Add(Restrictions.Ge("StartDate", startDate));
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                deCrit.Add(Restrictions.Le("StartDate", endDate));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return deCrit.List<ReportAutomatic>() as List<ReportAutomatic>;
        }

        public static int CountReportAutomaticList(string globalSearch, string startDate, string endDate)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ReportAutomatic));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("ReportName", globalSearch, MatchMode.Anywhere));
            }

            if (!string.IsNullOrEmpty(startDate))
            {
                deCrit.Add(Restrictions.Ge("StartDate", startDate));
            }

            if (!string.IsNullOrEmpty(endDate))
            {
                deCrit.Add(Restrictions.Le("StartDate", endDate));
            }

            deCrit.SetProjection(Projections.Count("Code"));
            return deCrit.UniqueResult<int>();
        }

        #endregion

        #region SourceReference

        public static List<SourceReferences> GetSourceReferencesList()
        {

            ICriteria decrit = session.CreateCriteria(typeof(SourceReferences));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("Code"));
            return decrit.List<SourceReferences>() as List<SourceReferences>;
        }

        public static List<SourceReferences> GetSourceReferences(string code)
        {
            ICriteria decrit = session.CreateCriteria(typeof(SourceReferences));
            decrit.Add(Expression.Eq("Code", code));
            decrit.Add(Expression.Eq("Active", true));
            return decrit.List<SourceReferences>() as List<SourceReferences>;
        }

   
        public static SourceReferences SaveSourceReferences(SourceReferences obj)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                obj.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                obj.Active = true;
                session.Save(obj);
                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static SourceReferences RemoveSourceReferences(SourceReferences obj)
        {
            DeleteWithTransaction<SourceReferences>(obj);
            return obj;
        }

        public static SourceReferences UpdateSourceReferences(SourceReferences obj)
        {
            ITransaction transaction = session.BeginTransaction();
            {
                obj.Active = true;
                session.Update(obj);
                transaction.Commit();
            }
            return obj;
        }
        
        public static List<SourceReferences> GetSourceReferencesList(string globalSearch, int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(SourceReferences));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));

            }
            
            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.List<SourceReferences>() as List<SourceReferences>;
        }

        public static int CountSourceReferencesList(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(SourceReferences));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetProjection(Projections.Count("Code"));
            deCrit.Add(Expression.Eq("Active", true));
            return deCrit.UniqueResult<int>();
        }

        #endregion

        #region ApplicantsReferences
        public static List<ApplicantVacancyReferences> GetApplicantsVacancyReferences(string code)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyReferences));
            decrit.Add(Expression.Eq("ApplicantCode", code));
            return decrit.List<ApplicantVacancyReferences>() as List<ApplicantVacancyReferences>;
        }

        public static void DeleteApplicantsVacancyReferences(string code)
        {
            var sqlQuery = session.CreateSQLQuery("DELETE FROM ApplicantVacancyReferences WHERE ApplicantCode = '" + code + "'");
            sqlQuery.ExecuteUpdate();
        }

        #endregion

        #region ApplicantsFlag
        public static List<ApplicantFlagStatuses> GetApplicantFlagStatuses(string code, string flagStatus)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantFlagStatuses));
            decrit.Add(Expression.Eq("ApplicantCode", code));
            decrit.Add(Expression.Eq("FlagStatus",flagStatus));
            return decrit.List<ApplicantFlagStatuses>() as List<ApplicantFlagStatuses>;
        }

        public static void DeleteApplicantFlagStatuses(string code)
        {
            var sqlQuery =  session.CreateSQLQuery("DELETE FROM ApplicantFlagStatuses WHERE ApplicantCode = '"+code+"'");
            sqlQuery.ExecuteUpdate();
        }

        #endregion



        #region applicant vacancy sources

        public static List<ApplicantVacancieInformationSources> GetApplicantVacancySources()
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancieInformationSources));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("OrderID"));
            return decrit.List<ApplicantVacancieInformationSources>() as List<ApplicantVacancieInformationSources>;
        }

        public static ApplicantVacancieInformationSources GetApplicationVacancySourceByCode(string code)
        {
            return session.Get<ApplicantVacancieInformationSources>(code);
        }

        #endregion

        #region ApplicationConfiguration

        public static List<ApplicationConfiguration> GetApplicationConfigurationList()
        {

            ICriteria decrit = session.CreateCriteria(typeof(ApplicationConfiguration));
            decrit.AddOrder(Order.Asc("Code"));
            return decrit.List<ApplicationConfiguration>() as List<ApplicationConfiguration>;
        }

        public static ApplicationConfiguration GetApplicationConfiguration(int code)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicationConfiguration));
            decrit.Add(Expression.Eq("Code", code));
            decrit.SetMaxResults(1);
            return decrit.UniqueResult<ApplicationConfiguration>();
        }


        public static ApplicationConfiguration UpdateApplicationConfiguration(ApplicationConfiguration obj)
        {
            ITransaction transaction = session.BeginTransaction();
            {
                session.Update(obj);
                transaction.Commit();
            }
            return obj;
        }

        public static List<ApplicationConfiguration> GetApplicationConfigurationList(string globalSearch, int startRecord,
                                                     int length,
                                                     string sortColumn,
                                                     string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicationConfiguration));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("ConfigName", globalSearch, MatchMode.Anywhere));

            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return deCrit.List<ApplicationConfiguration>() as List<ApplicationConfiguration>;
        }

        public static int CountApplicationConfiguration(string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(ApplicationConfiguration));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("ConfigName", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetProjection(Projections.Count("Code"));
            return deCrit.UniqueResult<int>();
        }

        #endregion
        //public static void SynchronizeOpenVacanciesFromSISDM()
        //{
        //    ITransaction transaction = session.BeginTransaction();
        //    try
        //    {
        //        List<string> vacancyCodeList = new List<string>();
        //        List<ATS> vacantPositionList = new List<ATS>();
        //        Pipelines defaultPipeline = HiringManager.GetPipeline("dfa665ff");

        //        if (defaultPipeline == null)
        //            throw new ApplicationException("Invalid operation, missing default pipeline");

        //        vacancyCodeList = VacancyManager.GetAllVacancyCodeList();
        //        vacantPositionList = SISDMManager.GetVacancyList(vacancyCodeList.ToArray());

        //        List<Vacancies> affectedVacancyList = new List<Vacancies>();

        //        // iterating throuh vacant positions
        //        foreach (ATS vacantPosition in vacantPositionList)
        //        {
        //            Companies company = ERecruitmentManager.GetCompany(vacantPosition.PersAdminId);
        //            if (company == null)
        //                continue;

        //            // get existing job position
        //            Positions existingPosition = ERecruitmentManager.GetPositionByName(vacantPosition.PersAdminId, vacantPosition.Stext);
        //            if (existingPosition == null)
        //            {
        //                existingPosition = new Positions();
        //                existingPosition.Active = true;
        //                existingPosition.CompanyCode = vacantPosition.PersAdminId;
        //                existingPosition.Name = vacantPosition.Stext;
        //                existingPosition.PositionCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
        //                session.Save(existingPosition);
        //            }

        //            // transfer to new job
        //            Vacancies atsJob = affectedVacancyList.Find(delegate (Vacancies temp) { return temp.PositionCode == existingPosition.PositionCode; });
        //            if (atsJob == null)
        //            {
        //                // trying to find vacancy
        //                atsJob = ERecruitmentManager.GetVacancyByPositionCode(existingPosition.CompanyCode, existingPosition.PositionCode, "Pending");
        //                if (atsJob == null)
        //                {
        //                    atsJob = new Vacancies();
        //                    atsJob.Active = true;
        //                    atsJob.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
        //                    atsJob.PositionCode = existingPosition.PositionCode;
        //                    atsJob.PositionName = vacantPosition.Stext;
        //                    atsJob.CompanyCode = vacantPosition.PersAdminId;
        //                    atsJob.EffectiveDate = vacantPosition.Begda;
        //                    atsJob.ExpirationDate = vacantPosition.Endda;

        //                    atsJob.PostDate = vacantPosition.Begda;
        //                    atsJob.ExpiredPostDate = vacantPosition.Endda;

        //                    atsJob.Status = "Pending";
        //                    atsJob.AdvertisementStatus = "Unposted";
        //                    atsJob.ApprovedDate = DateTime.Now;
        //                    atsJob.PreferredStartDate = vacantPosition.Begda;
        //                    atsJob.InsertedBy = "SAP";
        //                    atsJob.InsertStamp = DateTime.Now;
        //                    atsJob.UpdatedBy = "SAP";
        //                    atsJob.UpdateStamp = DateTime.Now;
        //                    atsJob.Active = true;
        //                    atsJob.IsNew = true;

        //                }
        //                else
        //                    atsJob.IsNew = false;

        //                affectedVacancyList.Add(atsJob);
        //            }

        //            atsJob.PipelineCode = defaultPipeline.Code;
        //            atsJob.SAPJobPositionID += vacantPosition.Objid + ",";
        //            atsJob.SAPVacantJobPositionID += vacantPosition.Objid + ",";

        //            atsJob.NumberOfOpening++;
        //        }

        //        foreach (Vacancies atsJob in affectedVacancyList)
        //            if (atsJob.IsNew)
        //                session.Save(atsJob);
        //            else
        //                session.Update(atsJob);

        //        transaction.Commit();
        //    }
        //    catch (Exception e)
        //    {
        //        transaction.Rollback();
        //        throw e;
        //    }
        //}


        //author : dtb
        //this function replace older SynchronizeOpenVacanciesFromSISDM function
        public static string SynchronizeOpenVacanciesFromSISDM(string CompanyCode)
        {
            string message = "";
            ITransaction transaction = session.BeginTransaction();
            try
            {
                var query = session.CreateSQLQuery("EXEC cusp_DownloadVacancySISDM @CompanyCode=:CompanyCode");
                query.SetParameter("CompanyCode", CompanyCode);
                object result = query.UniqueResult();
                transaction.Commit();
                message = Convert.ToString(result);
            }
            catch (Exception e)
            {
                transaction.Rollback();
                message = e.ToString();
            }

            return message;
        }

        public static void SynchronizeHiredApplicants(List<ApplicantVacancies> applicantVacancyList, string sapJob)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                foreach (ApplicantVacancies applicantVacancy in applicantVacancyList)
                {
                    var query = session.CreateSQLQuery("EXEC cusp_SynchronizeHiredApplicants @VacanciesCode=:VacanciesCode, @ApplicantCode=:ApplicantCode, @sapJob=:sapJob");
                    query.SetParameter("VacanciesCode", applicantVacancy.VacancyCode);
                    query.SetParameter("ApplicantCode", applicantVacancy.ApplicantCode);
                    query.SetParameter("sapJob", sapJob);
                    object result = query.UniqueResult();
                }
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}

