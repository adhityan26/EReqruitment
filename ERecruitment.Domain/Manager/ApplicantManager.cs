using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Transform;


namespace ERecruitment.Domain
{
    public class ApplicantManager : ERecruitmentRepositoryBase
    {
        #region applicant

        public static Applicants GetApplicant(string code)
        {
            return session.Get<Applicants>(code);
        }

        public static Applicants SaveApplicant(Applicants obj)
        {
            ITransaction transaction = session.BeginTransaction();

            try
            {
                #region validation

                Applicants conflictApplicant = GetApplicantByEmail(obj.Email);
                if (conflictApplicant != null)
                    throw new ApplicationException("The email has been used. Please use another email");

                #endregion

                if (string.IsNullOrEmpty(obj.Code))
                    obj.Code = Guid.NewGuid().ToString();

                if (string.IsNullOrEmpty(obj.SocMedID))
                {
                    obj.Verified = false;
                    obj.ExpiredDate = DateTime.Now.AddDays(1);
                }

                obj.Name = obj.FirstName + (string.IsNullOrEmpty(obj.LastName) ? "" : " " + obj.LastName);
                session.Save(obj);

                if (obj.Account != null)
                {
                    UserAccounts userAccount = obj.Account;
                    userAccount.Code = obj.Code;
                    userAccount.ApplicantCode = obj.Code;
                    session.Save(userAccount);
                }

                if (obj.Attachment != null)
                {
                    CurriculumVitaes cv = new CurriculumVitaes();
                    cv.ApplicantCode = obj.Code;
                    cv.Attachment = obj.Attachment;
                    cv.FileName = obj.FileName;
                    cv.FileType = obj.FileType;
                    cv.InsertStamp = DateTime.Now;
                    cv.InsertedBy = obj.Code;
                    session.Save(cv);
                }
                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                transaction.Rollback();
                throw ex;
            }

        }

        public static Applicants UpdateApplicant(Applicants obj)
        {
            try
            {
                ITransaction transaction = session.BeginTransaction();

                obj.Name = obj.FirstName + (string.IsNullOrEmpty(obj.LastName) ? "" : " " + obj.LastName);

                session.Update(obj);

                transaction.Commit();
                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static Applicants GetVerifiedActiveApplicantByEmailAddress(string emailAddress)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Eq("Email", emailAddress));
            decrit.Add(Expression.Eq("Verified", true));
            decrit.Add(Expression.IsNull("ExpiredDate"));

            IList<Applicants> result = decrit.List<Applicants>();

            if (result.Count > 0)
            {
                return decrit.List<Applicants>()[0];
            }
            else
            {
                return null;
            }
        }

        public static Applicants GetApplicantByEmail(string emailAddress)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Applicants));
            deCrit.Add(Expression.Eq("Email", emailAddress));

            IList<Applicants> result = deCrit.List<Applicants>();

            if (result.Count > 0)
            {
                return deCrit.List<Applicants>()[0];
            }
            else
            {
                return null;
            }
        }

        public static Applicants GetUnVerifiedActiveApplicantByEmailAddress(string emailAddress)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Eq("Email", emailAddress));
            decrit.Add(Expression.Eq("Verified", false));
            decrit.Add(Expression.Lt("ExpiredDate", DateTime.Now));

            IList<Applicants> result = decrit.List<Applicants>();

            if (result.Count > 0)
            {
                return decrit.List<Applicants>()[0];
            }
            else
            {
                return null;
            }
        }

        public static Applicants GetApplicantBySocmedID(string socmedType, string socmedId)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Eq("SocMedName", socmedType));
            decrit.Add(Expression.Eq("SocMedID", socmedId));
            return decrit.UniqueResult<Applicants>();
        }

        public static Applicants GetApplicantByLinkedInID(string socmedId)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Eq("LinkedInID", socmedId));
            return decrit.UniqueResult<Applicants>();
        }

        public static Applicants GetApplicantByFacebookID(string socmedId)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Eq("FacebookID", socmedId));
            return decrit.UniqueResult<Applicants>();
        }

        public static Applicants GetApplicantByIDCardNumber(string IDCardNumber)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Eq("IDCardNumber", IDCardNumber));
            decrit.SetMaxResults(1);
            return decrit.UniqueResult<Applicants>();
        }

        public static List<Applicants> GetApplicantWithNoSurveyInvitationList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Applicants));
            decrit.Add(Expression.Eq("Active", true));
            decrit.Add(Expression.Eq("Verified", true));

            return decrit.List<Applicants>() as List<Applicants>;
        }

        public static List<Applicants> GetApplicantList(string[] companyCode,
                                                        string accountStatusCode,
                                                        string gender,
                                                        string maritalStatus,
                                                        string flagStatus)
        {

            ICriteria deCrit = session.CreateCriteria(typeof(Applicants));
            deCrit.Add(Expression.In("CompanyCode", companyCode));
            if (!string.IsNullOrEmpty(accountStatusCode))
            {
                if (accountStatusCode == "Verified")
                    deCrit.Add(Expression.Eq("Verified", true));
                else
                    deCrit.Add(Expression.Eq("Verified", false));
            }

            if (!string.IsNullOrEmpty(gender))
                deCrit.Add(Expression.Eq("GenderSpecification", gender));

            if (!string.IsNullOrEmpty(maritalStatus))
                deCrit.Add(Expression.Eq("MaritalStatusSpecification", maritalStatus));

            if (!string.IsNullOrEmpty(flagStatus))
                deCrit.Add(Expression.Eq("FlagStatus", flagStatus));

            deCrit.Add(Expression.Eq("Active", true));

            return deCrit.List<Applicants>() as List<Applicants>;
        }


        public static List<Applicants> GetApplicantList(string[] companyCode,
                                                        string accountStatusCode,
                                                        string gender,
                                                        string maritalStatus,
                                                        string flagStatus,
                                                        string globalSearch,
                                                        int startRecord,
                                                        int length,
                                                        string sortColumn,
                                                        string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Applicants));

            deCrit.Add(Expression.In("CompanyCode", companyCode));
            if (!string.IsNullOrEmpty(accountStatusCode))
            {
                if (accountStatusCode == "Verified")
                    deCrit.Add(Expression.Eq("Verified", true));
                else
                    deCrit.Add(Expression.Eq("Verified", false));
            }

            if (!string.IsNullOrEmpty(gender))
                deCrit.Add(Expression.Eq("GenderSpecification", gender));

            if (!string.IsNullOrEmpty(maritalStatus))
                deCrit.Add(Expression.Eq("MaritalStatusSpecification", maritalStatus));

            if (!string.IsNullOrEmpty(flagStatus))
                deCrit.Add(Expression.Eq("FlagStatus", flagStatus));
            else
                deCrit.Add(Expression.IsNull("FlagStatus"));

            deCrit.Add(Expression.Eq("Active", true));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return deCrit.List<Applicants>() as List<Applicants>;
        }

        public static int CountApplicantList(string[] companyCode,
                                             string accountStatusCode,
                                             string gender,
                                             string maritalStatus,
                                             string flagStatus,
                                             string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Applicants));

            deCrit.Add(Expression.In("CompanyCode", companyCode));
            if (!string.IsNullOrEmpty(accountStatusCode))
            {
                if (accountStatusCode == "Verified")
                    deCrit.Add(Expression.Eq("Verified", true));
                else
                    deCrit.Add(Expression.Eq("Verified", false));
            }

            if (!string.IsNullOrEmpty(gender))
                deCrit.Add(Expression.Eq("GenderSpecification", gender));

            if (!string.IsNullOrEmpty(maritalStatus))
                deCrit.Add(Expression.Eq("MaritalStatusSpecification", maritalStatus));

            if (!string.IsNullOrEmpty(flagStatus))
                deCrit.Add(Expression.Eq("FlagStatus", flagStatus));
            else
                deCrit.Add(Expression.IsNull("FlagStatus"));

            deCrit.Add(Expression.Eq("Active", true));
            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetProjection(Projections.Count("Code"));
            return deCrit.UniqueResult<int>();
        }

        public static List<Applicants> GetFlaggedApplicantList(string flagStatus,
                                                        string globalSearch,
                                                        int startRecord,
                                                        int length,
                                                        string sortColumn,
                                                        string sortDir)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Applicants));

            if (!string.IsNullOrEmpty(flagStatus))
                deCrit.Add(Expression.Eq("FlagStatus", flagStatus));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetFirstResult(startRecord).SetMaxResults(length);

            if (!string.IsNullOrEmpty(sortColumn))
            {
                string[] mappingProperty = sortColumn.Split('.');
                if (mappingProperty.Length > 1)
                {
                    deCrit.CreateAlias(mappingProperty[0], mappingProperty[0]);
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                    else
                        deCrit.AddOrder(Order.Desc(string.Concat(mappingProperty[0], ".", mappingProperty[1])));
                }
                else
                {
                    if (sortDir == "asc")
                        deCrit.AddOrder(Order.Asc(sortColumn));
                    else
                        deCrit.AddOrder(Order.Desc(sortColumn));
                }
            }

            return deCrit.List<Applicants>() as List<Applicants>;
        }

        public static int CountFlaggedApplicantList(string flagStatus, string globalSearch)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(Applicants));

            if (!string.IsNullOrEmpty(flagStatus))
                deCrit.Add(Expression.Eq("FlagStatus", flagStatus));

            if (!string.IsNullOrEmpty(globalSearch))
            {
                deCrit.Add(Expression.InsensitiveLike("Name", globalSearch, MatchMode.Anywhere));
            }

            deCrit.SetProjection(Projections.Count("Code"));
            return deCrit.UniqueResult<int>();
        }

        public static List<Applicants> GetReferencedApplicantList(string companycode,
                                                                  string accountStatusCode,
                                                                  string gender,
                                                                  string maritalStatus,
                                                                  string flagStatus)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyReferences));
            decrit.Add(Expression.Eq("CompanyReferenceCode", companycode));
            decrit.CreateAlias("Applicant", "Applicant");

            if (!string.IsNullOrEmpty(accountStatusCode))
            {
                if (accountStatusCode == "Verified")
                    decrit.Add(Expression.Eq("Applicant.Verified", true));
                else
                    decrit.Add(Expression.Eq("Applicant.Verified", false));
            }

            if (!string.IsNullOrEmpty(gender))
                decrit.Add(Expression.Eq("Applicant.GenderSpecification", gender));

            if (!string.IsNullOrEmpty(maritalStatus))
                decrit.Add(Expression.Eq("Applicant.MaritalStatusSpecification", maritalStatus));

            if (!string.IsNullOrEmpty(flagStatus))
                decrit.Add(Expression.Eq("Applicant.FlagStatus", flagStatus));

            decrit.Add(Expression.Eq("Applicant.Active", true));

            decrit.SetProjection(Projections.Distinct(Projections.Property("Applicant")));
            return decrit.List<Applicants>() as List<Applicants>;
        }

        public static List<ApplicantVacancyReferences> GetReferencedApplicantList(string companyCode,
                                                                                  string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyReferences));
            decrit.Add(Expression.Eq("CompanyReferenceCode", companyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.List<ApplicantVacancyReferences>() as List<ApplicantVacancyReferences>;
        }

        public static int CountReferencedApplicantList(string companyCode, string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyReferences));
            decrit.Add(Expression.Eq("CompanyReferenceCode", companyCode));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }

        public static int CountReferencedApplicantListBasedAppCode(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancyReferences));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));

            decrit.SetProjection(Projections.Count("ApplicantCode"));

            return decrit.UniqueResult<int>();
        }


        #endregion

        #region applicant education

        public static ApplicantEducations GetApplicantEducationInfo(int id)
        {
            return session.Get<ApplicantEducations>(id);
        }

        public static void SaveUpdateApplicantEducation(ApplicantEducations applicantEducation)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (applicantEducation.StartYear > applicantEducation.EndYear)
                    throw new ApplicationException("Invalid data, start year must not be later than end year");

                session.SaveOrUpdate(applicantEducation);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static List<ApplicantEducations> GetApplicantEducationList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantEducations));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.AddOrder(Order.Desc("StartYear"));
            return decrit.List<ApplicantEducations>() as List<ApplicantEducations>;
        }

        public static List<EducationLevels> GetEducationMajor(string EducationLevelName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(EducationLevels));
            decrit.Add(Expression.Eq("Name", EducationLevelName));
            return decrit.List<EducationLevels>() as List<EducationLevels>;
        }

        public static List<EducationLevels> GetEducationLevelCascading(string InstitutionCode)
        {
            ProjectionList projectionList = Projections.ProjectionList();
            ProjectionList projectionList2 = Projections.ProjectionList();
            projectionList2
              .Add(Projections.Distinct(projectionList.Add(Projections.Property("EducationLevel"), "EducationLevel")))
              .Add(Projections.Property("EducationLevel"), "EducationLevel")
               .Add(Projections.Property("EducationLevelName"), "Name"); // const projection

            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingHeader));
            decrit.Add(Expression.Eq("UniversitiesCode", InstitutionCode));
            decrit.Add(Expression.Eq("Active", true));
            decrit.SetProjection(projectionList2);
            decrit.SetResultTransformer(Transformers.AliasToBean(typeof(EducationLevels)));


            return decrit.List<EducationLevels>() as List<EducationLevels>;
        }

        public static List<EducationFieldOfStudy> GetFieldOfStudyCascading(string InstitutionCode, string EducationLevel)
        {
            ProjectionList projectionList2 = Projections.ProjectionList();
            projectionList2
              .Add(Projections.Property("FieldOfStudyCode"), "Code")
               .Add(Projections.Property("FieldOfStudyName"), "Name"); // const projection

            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingHeader));
            decrit.Add(Expression.Eq("UniversitiesCode", InstitutionCode));
            decrit.Add(Expression.Eq("EducationLevel", EducationLevel));
            decrit.Add(Expression.Eq("Active", true));
            decrit.SetProjection(projectionList2);
            decrit.SetResultTransformer(Transformers.AliasToBean(typeof(EducationFieldOfStudy)));


            return decrit.List<EducationFieldOfStudy>() as List<EducationFieldOfStudy>;
        }

        public static List<EducationMajor> GetEducationMajorCascading(string InstitutionCode, string EducationLevel, string FieldOfStudyCode)
        {
            ProjectionList projectionList2 = Projections.ProjectionList();
            projectionList2
              .Add(Projections.Property("MajorCode"), "Code")
               .Add(Projections.Property("MajorName"), "Major"); // const projection

            ICriteria decrit = session.CreateCriteria(typeof(EducationMappingDetail));
            decrit.Add(Expression.Eq("UniversitiesCode", InstitutionCode));
            decrit.Add(Expression.Eq("EducationLevel", EducationLevel));
            decrit.Add(Expression.Eq("FieldOfStudyCode", FieldOfStudyCode));
            decrit.Add(Expression.Eq("Active", true));
            decrit.SetProjection(projectionList2);
            decrit.SetResultTransformer(Transformers.AliasToBean(typeof(EducationMajor)));


            return decrit.List<EducationMajor>() as List<EducationMajor>;
        }

        public static void RemoveApplicantEducationInfo(ApplicantEducations applicantEducation)
        {
            DeleteWithTransaction<ApplicantEducations>(applicantEducation);
        }

        #endregion

        #region applicant experiences

        public static List<ApplicantExperiences> GetApplicantExperienceList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantExperiences));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.AddOrder(Order.Desc("StartYear"));
            return decrit.List<ApplicantExperiences>() as List<ApplicantExperiences>;
        }

        public static List<ApplicantExperiences> GetApplicantExperienceList(string applicantCode, string experienceType)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantExperiences));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("ExperienceType", experienceType));
            decrit.AddOrder(Order.Desc("StartYear"));
            return decrit.List<ApplicantExperiences>() as List<ApplicantExperiences>;
        }

        public static ApplicantExperiences GetApplicantExperienceInfo(int id)
        {
            return session.Get<ApplicantExperiences>(id);
        }

        public static void SaveUpdateApplicantExperience(ApplicantExperiences applicantExperience)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                if (applicantExperience.StartYear == 0)
                    throw new ApplicationException("start year required");

                if (!applicantExperience.StillUntillNow)
                {
                    if (applicantExperience.StartYear > applicantExperience.EndYear)
                        throw new ApplicationException("Invalid data, start year must not be later than end year");
                }

                session.SaveOrUpdate(applicantExperience);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemoveApplicantExperienceInfo(ApplicantExperiences applicantExperience)
        {
            DeleteWithTransaction<ApplicantExperiences>(applicantExperience);
        }

        #endregion

        #region applicant skills

        public static List<ApplicantSkills> GetApplicantSkillList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSkills));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.List<ApplicantSkills>() as List<ApplicantSkills>;
        }

        public static List<ApplicantSkills> GetApplicantSkillList(string applicantCode, string skillType)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantSkills));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("SkillType", skillType));

            return decrit.List<ApplicantSkills>() as List<ApplicantSkills>;
        }

        public static ApplicantSkills GetApplicantSkillInfo(int id)
        {
            return session.Get<ApplicantSkills>(id);
        }

        public static void SaveUpdateApplicantSkill(ApplicantSkills applicantEducation)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.SaveOrUpdate(applicantEducation);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemoveApplicantSkillInfo(ApplicantSkills applicantSkill)
        {
            DeleteWithTransaction<ApplicantSkills>(applicantSkill);
        }

        #endregion

        #region applicant hobbies and interest

        public static List<ApplicantHobbyInterests> GetApplicantHobbyInterestList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantHobbyInterests));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.List<ApplicantHobbyInterests>() as List<ApplicantHobbyInterests>;
        }

        public static ApplicantHobbyInterests GetApplicantHobbyInterestInfo(int id)
        {
            return session.Get<ApplicantHobbyInterests>(id);
        }

        public static void SaveUpdateApplicantHobbyInterest(ApplicantHobbyInterests applicantEducation)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.SaveOrUpdate(applicantEducation);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemoveApplicantHobbyInterestInfo(ApplicantHobbyInterests applicantHobbyInterest)
        {
            DeleteWithTransaction<ApplicantHobbyInterests>(applicantHobbyInterest);
        }

        #endregion

        #region applicant awards

        public static List<ApplicantAwards> GetApplicantAwardList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAwards));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.List<ApplicantAwards>() as List<ApplicantAwards>;
        }

        public static ApplicantAwards GetApplicantAwardInfo(int id)
        {
            return session.Get<ApplicantAwards>(id);
        }

        public static void SaveUpdateApplicantAward(ApplicantAwards applicantEducation)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.SaveOrUpdate(applicantEducation);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemoveApplicantAwardInfo(ApplicantAwards applicantAward)
        {
            DeleteWithTransaction<ApplicantAwards>(applicantAward);
        }

        #endregion

        #region applicant trainings

        public static List<ApplicantTrainings> GetApplicantTrainingList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantTrainings));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.AddOrder(Order.Desc("StartDate"));
            return decrit.List<ApplicantTrainings>() as List<ApplicantTrainings>;
        }

        public static ApplicantTrainings GetApplicantTrainingInfo(int id)
        {
            return session.Get<ApplicantTrainings>(id);
        }

        public static void SaveUpdateApplicantTraining(ApplicantTrainings applicantEducation)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.SaveOrUpdate(applicantEducation);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemoveApplicantTrainingInfo(ApplicantTrainings applicantTraining)
        {
            DeleteWithTransaction<ApplicantTrainings>(applicantTraining);
        }

        #endregion

        #region applicant attachment

        public static List<ApplicantAttachments> GetApplicantAttachmentList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAttachments));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.List<ApplicantAttachments>() as List<ApplicantAttachments>;
        }

        public static List<ApplicantAttachmentInfos> GetApplicantAttachmentInfoList(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAttachmentInfos));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.List<ApplicantAttachmentInfos>() as List<ApplicantAttachmentInfos>;
        }

        public static List<ApplicantAttachments> GetApplicantAttachmentList(string applicantCode, string skillType)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantAttachments));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            decrit.Add(Expression.Eq("AttachmentType", skillType));
            return decrit.List<ApplicantAttachments>() as List<ApplicantAttachments>;
        }

        public static ApplicantAttachments GetApplicantAttachmentInfo(int id)
        {
            return session.Get<ApplicantAttachments>(id);
        }

        public static void SaveUpdateApplicantAttachment(ApplicantAttachments applicantAttachment)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                session.SaveOrUpdate(applicantAttachment);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        public static void RemoveApplicantAttachmentInfo(ApplicantAttachments applicantAttachment)
        {
            DeleteWithTransaction<ApplicantAttachments>(applicantAttachment);
        }

        #endregion

        #region applicant vacancies

        public static List<ApplicantVacancies> GetApplicantActiveVacancies(string applicantCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(ApplicantVacancies));
            decrit.Add(Expression.Eq("ApplicantCode", applicantCode));
            return decrit.List<ApplicantVacancies>() as List<ApplicantVacancies>;
        }

        #endregion

        #region applicant flag statuses

        public static void AddApplicantFlag(ApplicantFlagStatuses applicantFlag)
        {
            ApplicantFlagStatuses checkRecord = GetApplicantFlag(applicantFlag.ApplicantCode);
            if (checkRecord == null)
                SaveWithTransaction<ApplicantFlagStatuses>(applicantFlag);
        }

        public static ApplicantFlagStatuses GetApplicantFlag(string applicantCode)
        {
            return session.Get<ApplicantFlagStatuses>(applicantCode);
        }

        #endregion
    }
}
