﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using System.Linq;
namespace ERecruitment.Domain
{
    public class PageManager : ERecruitmentRepositoryBase
    {
        public static PageHighlightContexts GetPageHighlightContext(string pageName)
        {
            return session.Get<PageHighlightContexts>(pageName);
        }

        public static Pages GetPageByName(string pageName)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pages));
            decrit.Add(Expression.Eq("PageName", pageName));

            decrit.SetMaxResults(1);
            return decrit.UniqueResult<Pages>();
        }

        public static List<Pages> GetTranslatablePageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pages));

            decrit.Add(Expression.Eq("IsTranslatable", true));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<Pages>() as List<Pages>;
        }

        public static List<Pages> GetPublicPageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pages));

            decrit.Add(Expression.IsNull("ParentID"));
            decrit.Add(Expression.Eq("IsPublicPage", true));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<Pages>() as List<Pages>;
        }

        public static List<Pages> GetApplicantPageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pages));

            decrit.Add(Expression.IsNull("ParentID"));
            decrit.Add(Expression.Eq("IsApplicantPage", true));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<Pages>() as List<Pages>;
        }

        public static List<Pages> GetRecruiterPageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pages));


            decrit.Add(Expression.IsNull("ParentID"));
            decrit.Add(Expression.Eq("IsRecruiterPage", true));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<Pages>() as List<Pages>;
        }

        public static List<Pages> GetRecruiterAdminPageList()
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pages));


            decrit.Add(Expression.IsNull("ParentID"));
            decrit.Add(
            Expression.Or(
            Expression.Eq("IsRecruiterPage", true),
            Expression.Eq("IsAdminPage", true)));

            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<Pages>() as List<Pages>;
        }

        public static List<Pages> GetChildrenPageList(string parentCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(Pages));

            decrit.Add(Expression.Eq("ParentID", parentCode));
            decrit.Add(Expression.Eq("Active", true));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<Pages>() as List<Pages>;
        }

        public static List<PageTranslatableLabels> GetPageTranslatableLabelList(string pageCode, string companyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(PageTranslatableLabels));

            session.EnableFilter("PageFilter")
                   .SetParameter("CompanyCode", companyCode);

            decrit.Add(Expression.Eq("PageCode", pageCode));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<PageTranslatableLabels>() as List<PageTranslatableLabels>;
        }

        public static List<PageTranslatableLabels> GetPageTranslatableLabelList(string[] pageCode, string companyCode)
        {
            ICriteria decrit = session.CreateCriteria(typeof(PageTranslatableLabels));

            session.EnableFilter("PageFilter")
                   .SetParameter("CompanyCode", companyCode);

            decrit.Add(Expression.In("PageCode", pageCode));
            decrit.AddOrder(Order.Asc("OrderID"));

            return decrit.List<PageTranslatableLabels>() as List<PageTranslatableLabels>;
        }

        public static PageTranslatableLabelContents GetPageTranslatableContent(string pageCode, string companyCode, string label)
        {
            ICriteria decrit = session.CreateCriteria(typeof(PageTranslatableLabelContents));

            decrit.Add(Expression.Eq("PageCode", pageCode));
            decrit.Add(Expression.Eq("CompanyCode", companyCode));
            decrit.Add(Expression.Eq("Label", label));

            return decrit.UniqueResult<PageTranslatableLabelContents>();
        }

        public static void UpdateTranslatableContentList(List<PageTranslatableLabelContents> translatableContentList)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                foreach (PageTranslatableLabelContents translatableContent in translatableContentList)
                {
                    if (translatableContent.IsNew)
                        session.Save(translatableContent);
                    else
                        session.Update(translatableContent);
                }

                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }
    }
}
