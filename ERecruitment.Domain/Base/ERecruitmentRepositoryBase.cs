using System;
using System.Configuration;
using System.Collections.Generic;
using System.Diagnostics;
using NHibernate;
using NHibernate.Criterion;
using NHibernate.Type;
using SS.DataAccess;
using log4net;
using System.Web;

namespace ERecruitment.Domain
{
    public static class GenericUtilities
    {
        public static string ComposeSequenceNumber(int number, int length)
        {
            string sequence = number.ToString();
            while (sequence.Length < length)
                sequence = "0" + sequence;
            return sequence;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public class ERecruitmentRepositoryBase : RepositoryBase
    {
        //protected static ILog _logger = LogManager.GetLogger("Domain");

        #region keys

        private static readonly string sessionkey = "erecruitmentsessionkey";//accountingsessionkey";
        private static readonly string sessionkey2 = "sisdmsessionkey";//accountingsessionkey";
        private static readonly string connectionstringkey = "ERecruitmentConnectionString";//AccountingConnectionString";
        private static readonly string connectionstringkey2 = "SISDMConnectionString";//AccountingConnectionString";

        #endregion

        /// <summary>
        /// session for BasicMasterHRMRepositoryBase
        /// </summary>
        protected static ISession session
        {
            get
            {
                ISession tempSession = NHibernateSessionManager.Instance.GetSession(sessionkey);
                if (tempSession == null)
                {
                    // check mode
                    string applicationMode = ConfigurationManager.AppSettings["ApplicationMode"];
                    string connectionString = string.Empty;
                    if (applicationMode == "Cloud")
                    {
                        string cloudConnectionString = ConfigurationManager.AppSettings["CloudConnectionString"];
                        string hostName = HttpContext.Current.Request.Url.Host;
                        string[] splitHostName = hostName.Split('.');
                        connectionString = cloudConnectionString.Replace("[dbname]",
                            string.Concat(splitHostName[0], "_", splitHostName[1], "_", splitHostName[2]));
                    }
                    else
                    {
                        connectionString = ConfigurationManager.ConnectionStrings[connectionstringkey].ConnectionString;
                    }

                    NHibernateSessionManager.Instance.InitiateSession(sessionkey, connectionString);
                    tempSession = NHibernateSessionManager.Instance.GetSession(sessionkey);
                }
                return tempSession;
            }
        }

        protected static ISession session2
        {
            get
            {
                ISession tempSession = NHibernateSessionManager.Instance.GetSession(sessionkey2);
                if (tempSession == null)
                {
                    // check mode
                    string applicationMode = ConfigurationManager.AppSettings["ApplicationMode"];
                    string connectionString = string.Empty;
                    if (applicationMode == "Cloud")
                    {
                        string cloudConnectionString = ConfigurationManager.AppSettings["CloudConnectionString"];
                        string hostName = HttpContext.Current.Request.Url.Host;
                        string[] splitHostName = hostName.Split('.');
                        connectionString = cloudConnectionString.Replace("[dbname]",
                            string.Concat(splitHostName[0], "_", splitHostName[1], "_", splitHostName[2]));
                    }
                    else
                    {
                        connectionString = ConfigurationManager.ConnectionStrings[connectionstringkey2].ConnectionString;
                    }

                    NHibernateSessionManager.Instance.InitiateSession(sessionkey2, connectionString);
                    tempSession = NHibernateSessionManager.Instance.GetSession(sessionkey2);
                }
                return tempSession;
            }
        }

        #region save, update, delete

        /// <summary>
        /// Without a call to BeginTransaction / EndTransaction NHibernate will just
        /// ignore any bag construction. I.E: children of your entity will not be persisted.
        /// You don't need this if you are trying to save a class without any children that are collections
        /// </summary>
        protected static void SaveWithTransaction<T>(T item)
        {
            //session.Flush();
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.Save(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        protected static void UpdateWithTransaction<T>(T item)
        {
            //session.Flush();
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.Update(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="item"></param>
        protected static void DeleteWithTransaction<T>(T item)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {

                session.Delete(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();

                throw e;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hql"></param>
        /// <param name="idVal"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        protected static int DeleteWithTransaction(string hql, object idVal, IType type)
        {
            int returnVal = 0;
            ITransaction transaction = session.BeginTransaction();
            try
            {

                returnVal = session.Delete(hql, idVal, type);
                transaction.Commit();
            }
            catch (Exception e)
            {
                transaction.Rollback();
                NHibernateSessionManager.Instance.RollbackTransaction();
                NHibernateSessionManager.Instance.CloseSession();
                throw e;
            }
            return returnVal;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="hql"></param>
        /// <param name="idVal"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        protected static int DeleteWithTransaction(string hql, object[] idVal, IType[] type)
        {
            int returnVal = 0;
            try
            {
                ITransaction transaction = session.BeginTransaction();
                returnVal = session.Delete(hql, idVal, type);
                transaction.Commit();
            }
            catch (Exception e)
            {
                NHibernateSessionManager.Instance.RollbackTransaction();
                NHibernateSessionManager.Instance.CloseSession();
                throw e;
            }
            return returnVal;
        }

        /// <summary>
        /// Saving collection for objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemList"></param>
        protected static void SaveCollectionWithTransaction<T>(List<T> itemList)
        {
            try
            {
                ITransaction transaction = session.BeginTransaction();
                foreach (T item in itemList)
                    session.Save(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                NHibernateSessionManager.Instance.RollbackTransaction();
                NHibernateSessionManager.Instance.CloseSession();
                throw e;
            }

        }

        /// <summary>
        /// Updating collection of objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemList"></param>
        protected static void UpdateCollectionWithTransaction<T>(List<T> itemList)
        {
            try
            {
                ITransaction transaction = session.BeginTransaction();
                foreach (T item in itemList)
                    session.Update(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                NHibernateSessionManager.Instance.RollbackTransaction();
                NHibernateSessionManager.Instance.CloseSession();
                throw e;
            }
        }

        /// <summary>
        /// delete collection of objects
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="itemList"></param>
        protected static void RemoveCollection<T>(List<T> itemList)
        {
            try
            {
                ITransaction transaction = session.BeginTransaction();
                foreach (T item in itemList)
                    session.Delete(item);
                transaction.Commit();
            }
            catch (Exception e)
            {
                NHibernateSessionManager.Instance.RollbackTransaction();
                NHibernateSessionManager.Instance.CloseSession();
                throw e;
            }
        }

        #endregion

        #region Protected support methods

        /// <summary>
        /// Format sort expression
        /// </summary>
        /// <param name="SortExpression"></param>
        /// <returns></returns>
        protected static Order formatSortExpression(string SortExpression)
        {
            if (string.IsNullOrEmpty(SortExpression))
            {
                SortExpression = "UpdateStamp";
            }
            if (SortExpression.EndsWith("DESC"))
            {
                return Order.Desc(SortExpression.Replace(" DESC", ""));
            }
            else if (SortExpression.Contains("ASC"))
            {
                return Order.Asc(SortExpression.Replace(" ASC", ""));
            }
            else
            {
                return Order.Asc(SortExpression);
            }
        }

        /// <summary>
        /// setting up filter expression using "like" operator. 
        /// Filters collection of key-value property and property value
        /// </summary>
        /// <param name="Filters"></param>
        /// <param name="deCrit"></param>
        protected static void ComposeFilter(IDictionary<string, string> Filters, ICriteria deCrit)
        {
            foreach (KeyValuePair<string, string> keyval in Filters)
            {
                deCrit.Add(Expression.InsensitiveLike(keyval.Key, keyval.Value, MatchMode.Anywhere));
            }
        }

        /// <summary>
        /// Extract sql "where" filter expression to a collection of Dictionary entry
        /// Column-Value. filter expression expected with the "like" operator
        /// </summary>
        /// <param name="FilterExpression"></param>
        /// <returns></returns>
        protected static IDictionary<string, string> ConvertFilterExpression(string FilterExpression)
        {

            string filterexpression = FilterExpression;
            IDictionary<string, string> filterExps = new Dictionary<string, string>();
            if (filterexpression.Contains("and"))
            {
                // there are more than one filter expression, to ease splitting, replace " and " with ","
                filterexpression = filterexpression.Replace(" and ", ";");
                string[] filters = filterexpression.Split(',');

                for (int i = 0; i < filters.Length; i++)
                {
                    string temp = filters[i].Replace(" like ", ",");
                    string[] filterComponent = temp.Split(',');
                    filterExps.Add(filterComponent[0], filterComponent[1]);
                }
            }
            else
            {
                string temp = filterexpression.Replace(" like ", ",");
                string[] filterComponent = temp.Split(',');
                filterExps.Add(filterComponent[0], filterComponent[1]);
            }

            return filterExps;
        }

        #endregion

        #region Protected static Methods Get methods

        /// <summary>
        /// Generic query executor for unparametrized queries
        /// </summary>
        /// <param name="queryString">NHibernate QueryString</param>
        /// <param name="CacheRegion">NHibernate CacheRegion - please use pre-defined constants from baseclass</param>
        /// <returns></returns>
        protected static IList<T> GetList<T>(string queryString, string CacheRegion)
        {
            IQuery query = session.CreateQuery(queryString);
            query.SetCacheable(true);
            query.SetCacheRegion(CacheRegion);
            return query.List<T>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="queryString"></param>
        /// <returns></returns>
        protected static IList<T> GetList<T>(string queryString)
        {
            IQuery query = session.CreateQuery(queryString);
            query.SetCacheable(true);
            query.SetCacheRegion(CR_DEFAULT);
            return query.List<T>();
        }

        /// <summary>
        /// Generic method, get list of object with the specified SortExpression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="SortExpression"></param>
        /// <returns></returns>
        protected static List<T> GetListSort<T>(string SortExpression)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(T));
            if (!string.IsNullOrEmpty(SortExpression))
                deCrit.AddOrder(formatSortExpression(SortExpression));

            return (List<T>)deCrit.List<T>();
        }

        /// <summary>
        /// Get list based on the specified criteria
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName">Property to be search</param>
        /// <param name="propertyValue">Property's value to be search</param>
        /// <param name="SortExpression">Sort expression</param>
        /// <returns></returns>
        protected static List<T> GetListFiltered<T>(string propertyName, object propertyValue, string SortExpression)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(T));
            deCrit.Add(Expression.Eq(propertyName, propertyValue));
            if (!string.IsNullOrEmpty(SortExpression))
                deCrit.AddOrder(formatSortExpression(SortExpression));

            return (List<T>)deCrit.List<T>();
        }

        /// <summary>
        /// Get list based on the specified criteria using "Like" expression
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="propertyName"></param>
        /// <param name="propertyValue"></param>
        /// <param name="SortExpression"></param>
        /// <param name="likeSearch"></param>
        /// <returns></returns>
        protected static List<T> GetListFilteredLike<T>(string propertyName, object propertyValue, string SortExpression)
        {
            ICriteria deCrit = session.CreateCriteria(typeof(T));
            deCrit.Add(Restrictions.InsensitiveLike(propertyName, propertyValue.ToString(), MatchMode.Anywhere));
            if (!string.IsNullOrEmpty(SortExpression))
                deCrit.AddOrder(formatSortExpression(SortExpression));

            return (List<T>)deCrit.List<T>();
        }

        #endregion

    }
}
