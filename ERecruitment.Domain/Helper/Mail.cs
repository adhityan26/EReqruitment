﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using SS.Web.UI;

namespace ERecruitment.Domain.Helper
{
    public class Mail
    {
        public static void SendEmail(Companies emailSetting, string emailDestination, string subject, string body, bool htmlFormat)
        {
            if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
            {
                if (!htmlFormat)
                {
                    body = body.Replace(Environment.NewLine, "<br/>");
                }
                SmtpClient client = new SmtpClient();
                client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                client.Host = emailSetting.EmailSmtpHostAddress;
                if (emailSetting.EmailSmtpUseSSL)
                    client.EnableSsl = true;
                client.Timeout = 20000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                string address = emailSetting.EmailSmtpEmailAddress;
                string displayName = emailSetting.MailSender;

                if (!address.Contains("@"))
                {
                    address = displayName;
                }
            
                MailMessage mm = new MailMessage(new MailAddress(address,
                        displayName),
                    new MailAddress(emailDestination, ""));
                mm.Subject = subject;
                mm.Body = body;
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.IsBodyHtml = true;
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);
            }
        }
        
        public static string EmailParser(string emailBody, Dictionary<String, String> data) {
            bool conditionValue = false;

            foreach (Match match in Regex.Matches(emailBody, @"{.*}"))
            {
                conditionValue = false;
                String[] con = match.Value.Replace("{", "").Replace("}", "").Split('?');
                String[] logicalCondition = con[0].Split(new[] {"||"}, StringSplitOptions.None);
                foreach (string logical in logicalCondition)
                {
                    String[] conditionEquals = logical.Split(new[] {"=="}, StringSplitOptions.None);
                    String[] conditionNotEquals = logical.Split(new[] {"!="}, StringSplitOptions.None);
                    if (conditionEquals.Length > 1 || conditionNotEquals.Length > 1)
                    {
                        if (data.ContainsKey(conditionEquals[0]))
                        {
                            string leftValue = conditionEquals[1];
                            switch (leftValue)
                            {
                                case "true":
                                    leftValue = "True";
                                    break;
                                case "false":
                                    leftValue = "False";
                                    break;
                            }
                            if (conditionEquals.Length > 1)
                            {
                                conditionValue = data[conditionEquals[0]].Equals(leftValue) || conditionValue;
                            }
                            else
                            {
                                conditionValue = !(data[conditionEquals[0]].Equals(leftValue)) || conditionValue;
                            }
                            
                        }
                    }
                    else
                    {
                        if (data.ContainsKey(conditionEquals[0]))
                        {
                            conditionValue = !String.IsNullOrEmpty(data[conditionEquals[0]]) || conditionValue;
                        }
                    }
                }

                if (conditionValue)
                {
                    emailBody = emailBody.Replace(match.Value, HttpUtility.HtmlDecode(con[1]).Replace("\"", ""));
                }
                else
                {
                    emailBody = emailBody.Replace(match.Value, "");
                }
            }

            foreach (Match match in Regex.Matches(emailBody, @"<([a-zA-Z][a-zA-Z0-9]*)[^>]*>\s*<\/\1>"))
            {
                emailBody = emailBody.Replace(match.Value, "");
            }

            foreach (var d in data)
            {
                emailBody = emailBody.Replace(d.Key, d.Value);
            }
            
            return emailBody;
        }
    }
}