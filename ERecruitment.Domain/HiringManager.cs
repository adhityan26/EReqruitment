﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;

namespace ERecruitment.Domain
{
    public class HiringManager : ERecruitmentRepositoryBase
    {
        #region update status

        public static void UpdatePipeline(ApplicantVacancies applicantVacancy, string statusCode, DateTime date)
        {
            ITransaction transaction = session.BeginTransaction();
            try
            {
                applicantVacancy.Status = statusCode;
                applicantVacancy.UpdateStamp = DateTime.Now;
                session.Update(applicantVacancy);

                //add log
                ApplicantsVacancyMilestoneLogs addLogs = new ApplicantsVacancyMilestoneLogs();
                addLogs.ApplicantCode = applicantVacancy.ApplicantCode;
                addLogs.VacancyCode = applicantVacancy.VacancyCode;
                addLogs.Status = statusCode;
                addLogs.Date = DateTime.Now;
                session.Save(addLogs);
                transaction.Commit();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
