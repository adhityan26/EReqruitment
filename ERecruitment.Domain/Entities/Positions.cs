using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class Positions
    {
        public Positions() { }

        private string _positionCode;
        private string _companycode;
        private string _name;
        private bool _active;
        private string _companyname;

        public virtual string PositionCode
        {
            get
            {
                return _positionCode;
            }
            set
            {
                _positionCode = value;
            }
        }

        public virtual string CompanyCode
        {
            get { return _companycode; }
            set { _companycode = value; }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string CompanyName
        {
            get { return _companyname; }
            set { _companyname = value; }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
