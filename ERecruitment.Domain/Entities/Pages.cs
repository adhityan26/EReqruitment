using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class Pages
    {
        private string _id;
        private string _name;
        private string _parentId;
        private string _parentname;
        private string _baseUrl;
        private string _subUrl;
        private string _pagename;
        private bool _isPublicPage;
        private bool _isApplicantPage;
        private bool _isRecruiterPage;
        private bool _isadminpage;
        private bool _istranslatable;
        private string _datalangattributes;
        private int _orderId;
        private bool _active;

        public virtual string ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string ParentID
        {
            get
            {
                return _parentId;
            }
            set
            {
                _parentId = value;
            }
        }

        public virtual string ParentName
        {
            get { return _parentname; }
            set { _parentname = value; }
        }

        public virtual string BaseUrl
        {
            get
            {
                return _baseUrl;
            }
            set
            {
                _baseUrl = value;
            }
        }

        public virtual string SubUrl
        {
            get
            {
                return _subUrl;
            }
            set
            {
                _subUrl = value;
            }
        }

        public virtual string PageName
        {
            get { return _pagename; }
            set { _pagename = value; }
        }

        public virtual string FullUrl
        {
            get { return BaseUrl + SubUrl; }
        }

        public virtual bool IsPublicPage
        {
            get
            {
                return _isPublicPage;
            }
            set
            {
                _isPublicPage = value;
            }
        }

        public virtual bool IsApplicantPage
        {
            get
            {
                return _isApplicantPage;
            }
            set
            {
                _isApplicantPage = value;
            }
        }

        public virtual bool IsRecruiterPage
        {
            get
            {
                return _isRecruiterPage;
            }
            set
            {
                _isRecruiterPage = value;
            }
        }

        public virtual bool IsAdminPage
        {
            get { return _isadminpage; }
            set { _isadminpage = value; }
        }

        public virtual bool IsTranslatable
        {
            get { return _istranslatable; }
            set { _istranslatable = value; }
        }

        public virtual string DataLangAttribute
        {
            get { return _datalangattributes; }
            set { _datalangattributes = value; }
        }

        public virtual int OrderID
        {
            get
            {
                return _orderId;
            }
            set
            {
                _orderId = value;
            }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }
    }
}
