using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class Month {
        private int? _id;
        private string _name;
        public virtual int? Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public virtual string Name {
            get {
                return _name;
            }
            set {
                _name = value;
            }
        }
    }
}
