using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class EducationLevels
    {
        public EducationLevels() { }
        private string _educationLevel;
        private string _name;
        private bool _majorexist;
        private bool _active;


        public virtual string EducationLevel
        {
            get
            {
                return _educationLevel;
            }
            set
            {
                _educationLevel = value;
            }
        }

        public virtual string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public virtual bool MajorExist
        {
            get { return _majorexist; }
            set { _majorexist = value; }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
