using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantSkills
    {
        private int _id;
        private string _applicantCode;
        private string _skillType;
        private string _skillName;
        private string _proficiencySpeaking;
        private string _proficiencyWriting;
        private string _proficiencyListening;
        private string _proficiency;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string SkillType
        {
            get
            {
                return _skillType;
            }
            set
            {
                _skillType = value;
            }
        }

        public virtual string SkillName
        {
            get
            {
                return _skillName;
            }
            set
            {
                _skillName = value;
            }
        }

        public virtual string ProficiencySpeaking
        {
            get
            {
                return _proficiencySpeaking;
            }
            set
            {
                _proficiencySpeaking = value;
            }
        }

        public virtual string ProficiencyWriting
        {
            get
            {
                return _proficiencyWriting;
            }
            set
            {
                _proficiencyWriting = value;
            }
        }

        public virtual string ProficiencyListening
        {
            get
            {
                return _proficiencyListening;
            }
            set
            {
                _proficiencyListening = value;
            }
        }

        public virtual string Proficiency
        {
            get
            {
                return _proficiency;
            }
            set
            {
                _proficiency = value;
            }
        }
    }
}
