using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class VacancyMilestoneLogs
    {
        public VacancyMilestoneLogs() { }
        private int _id;
        private string _vacancyCode;
        private string _vacancyStatus;
        private string _advertisementStatus;
        private DateTime? _date;
        private string _executorcode;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }
        public virtual string VacancyStatus
        {
            get
            {
                return _vacancyStatus;
            }
            set
            {
                _vacancyStatus = value;
            }
        }
        public virtual string AdvertisementStatus
        {
            get
            {
                return _advertisementStatus;
            }
            set
            {
                _advertisementStatus = value;
            }
        }
        public virtual DateTime? Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }

        public virtual string ExecutorCode
        {
            get { return _executorcode; }
            set { _executorcode = value; }
        }
    }
}
