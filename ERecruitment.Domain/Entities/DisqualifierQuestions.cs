using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class DisqualifierQuestions
    {
        private string _code;
        private string _type;
        private string _name;
        private string _parentCode;
        private string _question;
        private string _categoryCode;
        private string _answerFormat;
        private int _orderId;
        private string _questiontype;
        private bool _active;
        private string _correctansweroperator;
        private string _correctanswerlabel;
        private List<DisqualifierQuestionAnswerOptions> _answeroptionlist = new List<DisqualifierQuestionAnswerOptions>();

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string ParentCode
        {
            get
            {
                return _parentCode;
            }
            set
            {
                _parentCode = value;
            }
        }

        public virtual string Question
        {
            get
            {
                return _question;
            }
            set
            {
                _question = value;
            }
        }

        public virtual string CategoryCode
        {
            get
            {
                return _categoryCode;
            }
            set
            {
                _categoryCode = value;
            }
        }

        public virtual string AnswerFormat
        {
            get
            {
                return _answerFormat;
            }
            set
            {
                _answerFormat = value;
            }
        }

        public virtual int OrderId
        {
            get
            {
                return _orderId;
            }
            set
            {
                _orderId = value;
            }
        }

        public virtual string QuestionType
        {
            get { return _questiontype; }
            set { _questiontype = value; }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }

        public virtual string CorrectAnswerOperator
        {
            get { return _correctansweroperator; }
            set { _correctansweroperator = value; }
        }

        public virtual string CorrectAnswerLabel
        {
            get { return _correctanswerlabel; }
            set { _correctanswerlabel = value; }
        }

        public virtual List<DisqualifierQuestionAnswerOptions> AnswerOptionList
        {
            get { return _answeroptionlist; }
            set { _answeroptionlist = value; }
        }
    }
}
