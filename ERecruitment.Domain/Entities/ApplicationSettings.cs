using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class ApplicationSettings
    {
        public ApplicationSettings() { }
        private int _id;
        private bool _applicantPortal;
        private bool _applicantTrackingSystem;
        private bool _approvalFunction;
        private string _vacancyapplicationmode;
        private string _jobagecalculationmethod;
        private bool _enablehotbutton;
        private string _hotbuttontext;
        private bool _enablesurveyquestion;
        private string _autodisqualifiedrecruitmentstatuscode;
        private string _refereddisqualifiedrecruitmentstatuscode;
        private string _hiredrecruitmentstatuscode;
        private string _disqualifiedrecruitmentstatuscode;
        private string _peerconnectionstring;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual bool ApplicantPortal
        {
            get
            {
                return _applicantPortal;
            }
            set
            {
                _applicantPortal = value;
            }
        }

        public virtual bool ApplicantTrackingSystem
        {
            get
            {
                return _applicantTrackingSystem;
            }
            set
            {
                _applicantTrackingSystem = value;
            }
        }

        public virtual bool ApprovalFunction
        {
            get
            {
                return _approvalFunction;
            }
            set
            {
                _approvalFunction = value;
            }
        }

        public virtual string VacancyApplicationMode
        {
            get { return _vacancyapplicationmode; }
            set { _vacancyapplicationmode = value; }
        }

        public virtual string JobAgeCalculationMethod
        {
            get { return _jobagecalculationmethod; }
            set { _jobagecalculationmethod = value; }
        }

        public virtual bool EnableHotButton
        {
            get { return _enablehotbutton; }
            set { _enablehotbutton = value; }
        }

        public virtual string HotButtonText
        {
            get { return _hotbuttontext; }
            set { _hotbuttontext = value; }
        }

        public virtual bool EnableSurveyQuestion
        {
            get { return _enablesurveyquestion; }
            set { _enablesurveyquestion = value; }
        }

        public virtual string PeerConnectionString
        {
            get { return _peerconnectionstring; }
            set { _peerconnectionstring = value; }
        }
    }
}
