using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantVacancyDisqualificationAnswers
    {
        private string _applicantCode;
        private string _vacancyCode;
        private string _questionCode;
        private string _answer;

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string QuestionCode
        {
            get
            {
                return _questionCode;
            }
            set
            {
                _questionCode = value;
            }
        }

        public virtual string Answer
        {
            get
            {
                return _answer;
            }
            set
            {
                _answer = value;
            }
        }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicantVacancyDisqualificationAnswers;
            if (t == null) return false;
            if (ApplicantCode == t.ApplicantCode
             && VacancyCode == t.VacancyCode
             && QuestionCode == t.QuestionCode)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ApplicantCode.GetHashCode();
            hash = (hash * 397) ^ VacancyCode.GetHashCode();
            hash = (hash * 397) ^ QuestionCode.GetHashCode();

            return hash;
        }
        
        #endregion
    }
}
