using System;
using System.Text;
using System.Collections.Generic;
using SS.Web.UI;


namespace ERecruitment.Domain
{

    public class ApplicantVacancyComments
    {
        public ApplicantVacancyComments() { }
        private int _id;
        private string _vacancyCode;
        private string _applicantCode;
        private string _comment;
        private string _status;
        private string _commentby;
        private DateTime _date;
        private string _issuedby;
        private string _commentatorname;
        private byte[] _commentatorphoto;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string Comment
        {
            get
            {
                return _comment;
            }
            set
            {
                _comment = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual string CommentBy

        {
            get { return _commentby; }
            set { _commentby = value; }
        }

        public virtual DateTime Date
        {
            get { return _date; }
            set { _date = value; }
        }

        public virtual string DateString
        {
            get { return Utils.DisplayDateAndHour(Date); }
        }

        public virtual string CommentatorName
        {
            get { return _commentatorname; }
            set { _commentatorname = value; }
        }

        public virtual byte[] CommentatorPhoto
        {
            get { return _commentatorphoto; }
            set { _commentatorphoto = value; }
        }
    }
}
