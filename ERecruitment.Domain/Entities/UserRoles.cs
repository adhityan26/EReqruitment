using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public  class UserRoles {
        public UserRoles() { }
        private string _userCode;
        private string _roleCode;

        public virtual string UserCode {
            get {
                return _userCode;
            }
            set {
                _userCode = value;
            }
        }
        public virtual string RoleCode {
            get {
                return _roleCode;
            }
            set {
                _roleCode = value;
            }
        }
        #region NHibernate Composite Key Requirements
        public  override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as UserRoles;
			if (t == null) return false;
			if (UserCode == t.UserCode
			 && RoleCode == t.RoleCode)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ UserCode.GetHashCode();
			hash = (hash * 397) ^ RoleCode.GetHashCode();

			return hash;
        }
        #endregion
    }
}
