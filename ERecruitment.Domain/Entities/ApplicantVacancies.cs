﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
namespace ERecruitment.Domain
{
    public class ApplicantVacancies
    {
        public ApplicantVacancies() { }

        private string _applicantcode;
        private string _vacancycode;
        private string _companycode;
        private DateTime _applieddate;
        private DateTime? _offeringeffectivedate;
        private string _offeringposition;
        private string _offeringaddress;
        private decimal _offeringannualsalary;
        private decimal _offeringmonthlysalary;
        private decimal _offeringthr;
        private int _offeringleavedays;
        private string _offeringtransportation;
        private string _offeringtelecommunication;
        private decimal _offeringbonus;
        private decimal _offeringincentive;
        private string _informationsource;
        private string _informationsourcenotes;
        private double _rankweight;
        private string _status;
        private string _companyname;
        private string _applicantname;
        private string _applicantphone;
        private string _applicantemail;
        private string _applicantgender;
        private string _applicantmaritalstatus;

        private int _applicantage;
        private string _applicantlasteducationinfo;

        private string _applicantlasteducationlevel;
        private string _applicantlasteducationfield;
        private double _applicantlasteducationgpa;
        private string _applicantlasteducationlocation;

        private string _applicantlastexperienceinfo;
        private decimal _applicantexpectedsalary;
        private string _applicantcurrentresidence;

        private int _applicantyearsofexperience;

        private byte[] _applicantphoto;
        private string _vacancypositionname;
        private string _vacancystatus;
        private string _vacancycompanyname;
        private string _vacancycityname;
        private string _pipelinecode;
        private string _candidatestatuslabel;
        private string _internalstatuslabel;
        private bool _ranked;
        private int _rankNumber;
        private int _rankQualificationExperienceMinimumYear;
        private string _rankQualificationMinimumEducationLevelCode;
        private string _rankQualificationEducationFieldCode;
        private float _rankQualificationEducationGpaRangeBottom;
        private float _rankQualificationEducationGpaRangeTop;
        private string _rankQualificationEducationLocationCode;
        private string _rankQualificationResidenceLocationCode;
        private decimal _rankQualificationSalaryRangeBottom;
        private decimal _rankQualificationSalaryRangeTop;
        private int _rankQualificationAgeRangeBottom;
        private int _rankQualificationAgeRangeTop;

        private int _rankQualificationExperienceMinimumYearWeight;
        private int _rankQualificationMinimumEducationLevelCodeWeight;
        private int _rankQualificationEducationFieldCodeWeight;
        private int _rankQualificationEducationGpaRangeWeight;
        private int _rankQualificationEducationLocationCodeWeight;
        private int _rankQualificationResidenceLocationCodeWeight;
        private int _rankQualificationSalaryRangeWeight;
        private int _rankQualificationAgeRangeWeight;

        private string _updatedby;
        private DateTime _updatestamp;
        private int _applicantjobappliedcount;

        private string _flagnotes;
        private string _flagStatus;
        private string _flagissuedByCode;
        private string _flagissuedbyname;
        private DateTime _flagissuedDate;
        private bool _active;
        private String _sourceReferencesName;
        private String _isGreenFlag;
        private String _isBlackFlag;
        private String _isTestResult;
        private String _isOffer;
        private String _isComments;
        private String _isAppointment;
        private String _isRefer;
        private String _isReject;
        private String _isHire;
        private String _isProcessed;


        private List<ApplicantVacancyDisqualificationAnswers> _disqualifieranswerlist = new List<ApplicantVacancyDisqualificationAnswers>();

        public virtual string SourceReferencesName
        {
            get { return _sourceReferencesName; }
            set { _sourceReferencesName = value; }
        }

        public virtual string InformationSource
        {
            get { return _informationsource; }
            set { _informationsource = value; }
        }

        public virtual string CompanyCode
        {
            get { return _companycode; }
            set { _companycode = value; }
        }

        public virtual string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public virtual DateTime AppliedDate
        {
            get { return _applieddate; }
            set { _applieddate = value; }
        }

        public virtual string ApplicantCode
        {
            get { return _applicantcode; }
            set { _applicantcode = value; }
        }

        public virtual string VacancyCode
        {
            get { return _vacancycode; }
            set { _vacancycode = value; }
        }

        public virtual DateTime? OfferingEffectiveDate
        {
            get { return _offeringeffectivedate; }
            set { _offeringeffectivedate = value; }
        }

        public virtual string OfferingPosition
        {
            get { return _offeringposition; }
            set { _offeringposition = value; }
        }

        public virtual string OfferingAddress
        {
            get { return _offeringaddress; }
            set { _offeringaddress = value; }
        }

        public virtual decimal OfferingAnnualSalary
        {
            get { return _offeringannualsalary; }
            set { _offeringannualsalary = value; }
        }

        public virtual decimal OfferingMonthlySalary
        {
            get { return _offeringmonthlysalary; }
            set { _offeringmonthlysalary = value; }
        }

        public virtual decimal OfferingTHR
        {
            get { return _offeringthr; }
            set { _offeringthr = value; }
        }

        public virtual int OfferingLeaveDays
        {
            get { return _offeringleavedays; }
            set { _offeringleavedays = value; }
        }

        public virtual string OfferingTransportation
        {
            get { return _offeringtransportation; }
            set { _offeringtransportation = value; }
        }

        public virtual string OfferingTelecommunication
        {
            get { return _offeringtelecommunication; }
            set { _offeringtelecommunication = value; }
        }

        public virtual decimal OfferingBonus
        {
            get { return _offeringbonus; }
            set { _offeringbonus = value; }
        }

        public virtual decimal OfferingIncentive
        {
            get { return _offeringincentive; }
            set { _offeringincentive = value; }
        }

        public virtual string InformationSourceNotes
        {
            get { return _informationsourcenotes; }
            set { _informationsourcenotes = value; }
        }

        public virtual double RankWeight
        {
            get { return _rankweight; }
            set { _rankweight = value; }
        }

        public virtual string CompanyName
        {
            get { return _companyname; }
            set { _companyname = value; }
        }

        public virtual string ApplicantName
        {
            get { return _applicantname; }
            set { _applicantname = value; }
        }

        public virtual string ApplicantPhone
        {
            get { return _applicantphone; }
            set { _applicantphone = value; }
        }

        public virtual string ApplicantEmail
        {
            get { return _applicantemail; }
            set { _applicantemail = value; }
        }

        public virtual string ApplicantGender
        {
            get { return _applicantgender; }
            set { _applicantgender = value; }
        }

        public virtual string ApplicantMaritalStatus
        {
            get { return _applicantmaritalstatus; }
            set { _applicantmaritalstatus = value; }
        }

        public virtual string ApplicantCurrentResidence
        {
            get { return _applicantcurrentresidence; }
            set { _applicantcurrentresidence = value; }
        }

        public virtual byte[] ApplicantPhoto
        {
            get { return _applicantphoto; }
            set { _applicantphoto = value; }
        }

        public virtual int ApplicantAge
        {
            get { return _applicantage; }
            set { _applicantage = value; }
        }

        public virtual string ApplicantLastEducationInfo
        {
            get { return _applicantlasteducationinfo; }
            set { _applicantlasteducationinfo = value; }
        }

        public virtual string ApplicantLastEducationLevel
        {
            get { return _applicantlasteducationlevel; }
            set { _applicantlasteducationlevel = value; }
        }

        public virtual string ApplicantLastEducationField
        {
            get { return _applicantlasteducationfield; }
            set { _applicantlasteducationfield = value; }
        }

        public virtual double ApplicantLastEducationGPA
        {
            get { return _applicantlasteducationgpa; }
            set { _applicantlasteducationgpa = value; }
        }

        public virtual string ApplicantLastEducationLocation
        {
            get { return _applicantlasteducationlocation; }
            set { _applicantlasteducationlocation = value; }
        }

        public virtual int ApplicantYearsOfExperience
        {
            get { return _applicantyearsofexperience; }
            set { _applicantyearsofexperience = value; }
        }

        public virtual string ApplicantLastExperienceInfo
        {
            get { return _applicantlastexperienceinfo; }
            set { _applicantlastexperienceinfo = value; }
        }

        public virtual decimal ApplicantExpectedSalary
        {
            get { return _applicantexpectedsalary; }
            set { _applicantexpectedsalary = value; }
        }

        public virtual string PipelineCode
        {
            get { return _pipelinecode; }
            set { _pipelinecode = value; }
        }

        public virtual string UpdatedBy
        {
            get { return _updatedby; }
            set { _updatedby = value; }
        }

        public virtual DateTime UpdateStamp
        {
            get { return _updatestamp; }
            set { _updatestamp = value; }
        }

        public virtual string VacancyPositionName
        {
            get { return _vacancypositionname; }
            set { _vacancypositionname = value; }
        }

        public virtual string VacancyStatus
        {
            get { return _vacancystatus; }
            set { _vacancystatus = value; }
        }

        public virtual string VacancyCompanyName
        {
            get { return _vacancycompanyname; }
            set { _vacancycompanyname = value; }
        }

        public virtual string VacancyCityName
        {
            get { return _vacancycityname; }
            set { _vacancycityname = value; }
        }

        public virtual string CandidateStatusLabel
        {
            get { return _candidatestatuslabel; }
            set { _candidatestatuslabel = value; }
        }

        public virtual string InternalStatusLabel
        {
            get { return _internalstatuslabel; }
            set { _internalstatuslabel = value; }
        }

        public virtual int ApplicantJobAppliedCount
        {
            get { return _applicantjobappliedcount; }
            set { _applicantjobappliedcount = value; }
        }

        public virtual string FlagNotes
        {
            get
            {
                return _flagnotes;
            }
            set
            {
                _flagnotes = value;
            }
        }

        public virtual string FlagStatus
        {
            get
            {
                return _flagStatus;
            }
            set
            {
                _flagStatus = value;
            }
        }

        public virtual string FlagIssuedByCode
        {
            get
            {
                return _flagissuedByCode;
            }
            set
            {
                _flagissuedByCode = value;
            }
        }

        public virtual string FlagIssuedByName
        {
            get { return _flagissuedbyname; }
            set { _flagissuedbyname = value; }
        }

        public virtual DateTime FlagIssuedDate
        {
            get
            {
                return _flagissuedDate;
            }
            set
            {
                _flagissuedDate = value;
            }
        }


        public virtual bool Ranked
        {
            get
            {
                return _ranked;
            }
            set
            {
                _ranked = value;
            }
        }

        public virtual int RankNumber
        {
            get
            {
                return _rankNumber;
            }
            set
            {
                _rankNumber = value;
            }
        }

        public virtual int RankQualificationExperienceMinimumYear
        {
            get
            {
                return _rankQualificationExperienceMinimumYear;
            }
            set
            {
                _rankQualificationExperienceMinimumYear = value;
            }
        }

        public virtual string RankQualificationMinimumEducationLevelCode
        {
            get
            {
                return _rankQualificationMinimumEducationLevelCode;
            }
            set
            {
                _rankQualificationMinimumEducationLevelCode = value;
            }
        }

        public virtual string RankQualificationEducationFieldCode
        {
            get
            {
                return _rankQualificationEducationFieldCode;
            }
            set
            {
                _rankQualificationEducationFieldCode = value;
            }
        }

        public virtual float RankQualificationEducationGpaRangeBottom
        {
            get
            {
                return _rankQualificationEducationGpaRangeBottom;
            }
            set
            {
                _rankQualificationEducationGpaRangeBottom = value;
            }
        }

        public virtual float RankQualificationEducationGpaRangeTop
        {
            get
            {
                return _rankQualificationEducationGpaRangeTop;
            }
            set
            {
                _rankQualificationEducationGpaRangeTop = value;
            }
        }

        public virtual string RankQualificationEducationLocationCode
        {
            get
            {
                return _rankQualificationEducationLocationCode;
            }
            set
            {
                _rankQualificationEducationLocationCode = value;
            }
        }

        public virtual string RankQualificationResidenceLocationCode
        {
            get
            {
                return _rankQualificationResidenceLocationCode;
            }
            set
            {
                _rankQualificationResidenceLocationCode = value;
            }
        }

        public virtual decimal RankQualificationSalaryRangeBottom
        {
            get
            {
                return _rankQualificationSalaryRangeBottom;
            }
            set
            {
                _rankQualificationSalaryRangeBottom = value;
            }
        }

        public virtual decimal RankQualificationSalaryRangeTop
        {
            get
            {
                return _rankQualificationSalaryRangeTop;
            }
            set
            {
                _rankQualificationSalaryRangeTop = value;
            }
        }

        public virtual int RankQualificationAgeRangeBottom
        {
            get
            {
                return _rankQualificationAgeRangeBottom;
            }
            set
            {
                _rankQualificationAgeRangeBottom = value;
            }
        }

        public virtual int RankQualificationAgeRangeTop
        {
            get
            {
                return _rankQualificationAgeRangeTop;
            }
            set
            {
                _rankQualificationAgeRangeTop = value;
            }
        }

        public virtual int RankQualificationExperienceMinimumYearWeight
        {
            get
            {
                return _rankQualificationExperienceMinimumYearWeight;
            }
            set
            {
                _rankQualificationExperienceMinimumYearWeight = value;
            }
        }

        public virtual int RankQualificationMinimumEducationLevelCodeWeight
        {
            get
            {
                return _rankQualificationMinimumEducationLevelCodeWeight;
            }
            set
            {
                _rankQualificationMinimumEducationLevelCodeWeight = value;
            }
        }

        public virtual int RankQualificationEducationFieldCodeWeight
        {
            get
            {
                return _rankQualificationEducationFieldCodeWeight;
            }
            set
            {
                _rankQualificationEducationFieldCodeWeight = value;
            }
        }

        public virtual int RankQualificationEducationGpaRangeWeight
        {
            get
            {
                return _rankQualificationEducationGpaRangeWeight;
            }
            set
            {
                _rankQualificationEducationGpaRangeWeight = value;
            }
        }

        public virtual int RankQualificationEducationLocationCodeWeight
        {
            get
            {
                return _rankQualificationEducationLocationCodeWeight;
            }
            set
            {
                _rankQualificationEducationLocationCodeWeight = value;
            }
        }

        public virtual int RankQualificationResidenceLocationCodeWeight
        {
            get
            {
                return _rankQualificationResidenceLocationCodeWeight;
            }
            set
            {
                _rankQualificationResidenceLocationCodeWeight = value;
            }
        }

        public virtual int RankQualificationSalaryRangeWeight
        {
            get
            {
                return _rankQualificationSalaryRangeWeight;
            }
            set
            {
                _rankQualificationSalaryRangeWeight = value;
            }
        }

        public virtual int RankQualificationAgeRangeWeight
        {
            get
            {
                return _rankQualificationAgeRangeWeight;
            }
            set
            {
                _rankQualificationAgeRangeWeight = value;
            }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public virtual List<ApplicantVacancyDisqualificationAnswers> DisqualifierAnswerList
        {
            get { return _disqualifieranswerlist; }
            set { _disqualifieranswerlist = value; }
        }

        public virtual string IsGreenFlag
        {
            get { return _isGreenFlag; }
            set { _isGreenFlag = value; }
        }

        public virtual string IsBlackFlag
        {
            get { return _isBlackFlag; }
            set { _isBlackFlag = value; }
        }

        public virtual string IsTestResult
        {
            get { return _isTestResult; }
            set { _isTestResult = value; }
        }

        public virtual string IsAppointment
        {
            get { return _isAppointment; }
            set { _isAppointment = value; }
        }

        public virtual string IsComments
        {
            get { return _isComments; }
            set { _isComments = value; }
        }

        public virtual string IsOffer
        {
            get { return _isOffer; }
            set { _isOffer = value; }
        }

        public virtual string IsRefer
        {
            get { return _isRefer; }
            set { _isRefer = value; }
        }

        public virtual string IsHire
        {
            get { return _isHire; }
            set { _isHire = value; }
        }

        public virtual string IsReject
        {
            get { return _isReject; }
            set { _isReject = value; }
        }

        public virtual string IsProcessed
        {
            get { return _isProcessed; }
            set { _isProcessed = value; }
        }

        [DebuggerStepThrough]
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        [DebuggerStepThrough]
        public override bool Equals(object obj)
        {
            ApplicantVacancies temp = obj as ApplicantVacancies;
            if (temp == null)
                return false;
            else
            {
                if (temp._applicantcode == this._applicantcode && temp._vacancycode == this._vacancycode)
                    return true;
                else
                    return false;
            }
        }
    }
}
