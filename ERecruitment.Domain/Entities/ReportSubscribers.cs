using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ReportSubscribers
    {
        private string _reportCode;
        private string _userCode;
        private string _username;

        public virtual string ReportCode
        {
            get
            {
                return _reportCode;
            }
            set
            {
                _reportCode = value;
            }
        }

        public virtual string UserCode
        {
            get
            {
                return _userCode;
            }
            set
            {
                _userCode = value;
            }
        }

        public virtual string UserName
        {
            get { return _username; }
            set { _username = value; }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ReportSubscribers;
            if (t == null) return false;
            if (ReportCode == t.ReportCode
             && UserCode == t.UserCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ReportCode.GetHashCode();
            hash = (hash * 397) ^ UserCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
