using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantVacancieInformationSources
    {
        private string _code;
        private string _name;
        private bool _additionalInfoRequired;
        private int _orderId;
        private bool _active;
        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }
        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public virtual bool AdditionalInfoRequired
        {
            get
            {
                return _additionalInfoRequired;
            }
            set
            {
                _additionalInfoRequired = value;
            }
        }
        public virtual int OrderID
        {
            get
            {
                return _orderId;
            }
            set
            {
                _orderId = value;
            }
        }
        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }
    }
}
