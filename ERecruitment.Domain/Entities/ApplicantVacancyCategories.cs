using System;
using System.Data;
using System.Collections;
using System.Diagnostics;


namespace ERecruitment.Domain
{

	/// <summary>
	///
	/// </summary>
	[Serializable]
	public class ApplicantVacancyCategories
	{


		public ApplicantVacancyCategories(){}
		private string _applicantcode;
		private string _vacancycategorycode;


		public virtual  string ApplicantCode
		{
			[DebuggerStepThrough]
			get { return _applicantcode; }
			[DebuggerStepThrough]
			set { _applicantcode = value; }
		}

		public virtual  string VacancyCategoryCode
		{
			[DebuggerStepThrough]
			get { return _vacancycategorycode; }
			[DebuggerStepThrough]
			set { _vacancycategorycode = value; }
		}

		[DebuggerStepThrough]
		public override int GetHashCode()
		{
			return base.GetHashCode();
		}
		[DebuggerStepThrough]
		public override bool Equals(object obj)
		{
			ApplicantVacancyCategories temp = obj as ApplicantVacancyCategories;
            if (temp == null)
                return false;
            else
            {
                if ( temp._applicantcode == this._applicantcode&& temp._vacancycategorycode == this._vacancycategorycode)
                    return true;
                else
                    return false;
            }
		}

	}
}
