using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class PipelineSteps
    {
        public PipelineSteps() { }
        public PipelineSteps(string statusCode) { _statusCode = statusCode; }
        public PipelineSteps(string statusCode, int emailTemplateId) { _statusCode = statusCode; _applicantEmailTemplateId = emailTemplateId; }

        private string _pipelineCode;
        private string _statusCode;
        private bool _required;
        private bool _compensation;
        private bool _scheduling;
        private bool _comment;
        private string _testcode;
        private int _recruiterEmailTemplateId;
        private int _applicantEmailTemplateId;
        private int _sortorder;
        private VacancyStatus _statusobj;
        private int _updatestatustemplateid;
        private int _schedullingrecruitertemplateid;
        private int _schedullingapplicanttemplateid;
        private int _offeringapplicanttemplateid;
        private bool _ishidden;
        private int _updatestatussmstemplateid;
        private int _schedullingsmstemplateid;

        public virtual int UpdateStatusSMSTemplateId
        {
            get { return _updatestatussmstemplateid; }
            set { _updatestatussmstemplateid = value; }
        }

        public virtual int SchedullingSMSTemplateId
        {
            get { return _schedullingsmstemplateid; }
            set { _schedullingsmstemplateid = value; }
        }

        public virtual bool IsHidden
        {
            get { return _ishidden; }
            set { _ishidden = value; }
        }

        public virtual int UpdateStatusTemplateId
        {
            get { return _updatestatustemplateid; }
            set { _updatestatustemplateid = value; }
        }

        public virtual int SchedullingRecruiterTemplateId
        {
            get { return _schedullingrecruitertemplateid; }
            set { _schedullingrecruitertemplateid = value; }
        }

        public virtual int SchedullingApplicantTemplateId
        {
            get { return _schedullingapplicanttemplateid; }
            set { _schedullingapplicanttemplateid = value; }
        }

        public virtual int OfferingApplicantTemplateId
        {
            get { return _offeringapplicanttemplateid; }
            set { _offeringapplicanttemplateid = value; }
        }

        public virtual VacancyStatus StatusObj
        {
            get { return _statusobj; }
            set { _statusobj = value; }
        }

        public virtual string StatusName
        {
            get { return _statusobj.Name; }
        }

        public virtual int SortOrder
        {
            get { return _sortorder; }
            set { _sortorder = value; }
        }

        public virtual string PipelineCode
        {
            get
            {
                return _pipelineCode;
            }
            set
            {
                _pipelineCode = value;
            }
        }

        public virtual string StatusCode
        {
            get
            {
                return _statusCode;
            }
            set
            {
                _statusCode = value;
            }
        }

        public virtual bool Required
        {
            get
            {
                return _required;
            }
            set
            {
                _required = value;
            }
        }

        public virtual bool Compensation
        {
            get
            {
                return _compensation;
            }
            set
            {
                _compensation = value;
            }
        }

        public virtual bool Scheduling
        {
            get
            {
                return _scheduling;
            }
            set
            {
                _scheduling = value;
            }
        }

        public virtual bool Comment
        {
            get
            {
                return _comment;
            }
            set
            {
                _comment = value;
            }
        }

        public virtual string TestCode
        {
            get { return _testcode; }
            set { _testcode = value; }
        }

        public virtual int RecruiterEmailTemplateId
        {
            get
            {
                return _recruiterEmailTemplateId;
            }
            set
            {
                _recruiterEmailTemplateId = value;
            }
        }

        public virtual int ApplicantEmailTemplateId
        {
            get
            {
                return _applicantEmailTemplateId;
            }
            set
            {
                _applicantEmailTemplateId = value;
            }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as PipelineSteps;
            if (t == null) return false;
            if (PipelineCode == t.PipelineCode
             && StatusCode == t.StatusCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ PipelineCode.GetHashCode();
            hash = (hash * 397) ^ StatusCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
