using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicationPageControls
    {
        private string _pageName;
        private string _controlId;
        private string _controlType;
        private int _orderId;

        public virtual string PageName
        {
            get
            {
                return _pageName;
            }
            set
            {
                _pageName = value;
            }
        }

        public virtual string ControlId
        {
            get
            {
                return _controlId;
            }
            set
            {
                _controlId = value;
            }
        }

        public virtual string ControlType
        {
            get
            {
                return _controlType;
            }
            set
            {
                _controlType = value;
            }
        }

        public virtual int OrderId
        {
            get
            {
                return _orderId;
            }
            set
            {
                _orderId = value;
            }
        }
        
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicationPageControls;
            if (t == null) return false;
            if (PageName == t.PageName
             && ControlId == t.ControlId)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ PageName.GetHashCode();
            hash = (hash * 397) ^ ControlId.GetHashCode();

            return hash;
        }
        #endregion
    }
}
