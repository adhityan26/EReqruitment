using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public partial class TableMaps {

        private string _code;
        private string _name;
        private string _internalTableName;
        private string _externalTableName;
        private bool _active;

        public virtual string Code {
            get {
                return _code;
            }
            set {
                _code = value;
            }
        }

        public virtual string Name {
            get {
                return _name;
            }
            set {
                _name = value;
            }
        }

        public virtual string InternalTableName {
            get {
                return _internalTableName;
            }
            set {
                _internalTableName = value;
            }
        }

        public virtual string ExternalTableName {
            get {
                return _externalTableName;
            }
            set {
                _externalTableName = value;
            }
        }

        public virtual bool Active {
            get {
                return _active;
            }
            set {
                _active = value;
            }
        }
    }
}
