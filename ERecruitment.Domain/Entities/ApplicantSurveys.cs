using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantSurveys
    {
        private string _applicantCode;
        private string _questionCode;
        private string _companycode;
        private string _companiesCode;
        private string _answer;
        private string _notes;
        private string _positionCode;

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string QuestionCode
        {
            get
            {
                return _questionCode;
            }
            set
            {
                _questionCode = value;
            }
        }

        public virtual string CompanyCode
        {
            get { return _companycode; }
            set { _companycode = value; }
        }

        public virtual string Answer
        {
            get
            {
                return _answer;
            }
            set
            {
                _answer = value;
            }
        }

        public virtual string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public virtual string CompaniesCode
        {
            get
            {
                return _companiesCode;
            }
            set
            {
                _companiesCode = value;
            }
        }

        public virtual string PositionCode
        {
            get
            {
                return _positionCode;
            }
            set
            {
                _positionCode = value;
            }
        }



        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicantSurveys;
            if (t == null) return false;
            if (ApplicantCode == t.ApplicantCode
             && QuestionCode == t.QuestionCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ApplicantCode.GetHashCode();
            hash = (hash * 397) ^ QuestionCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
