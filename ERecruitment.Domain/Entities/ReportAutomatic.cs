﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERecruitment.Domain
{
    public partial class ReportAutomatic
    {
        private string _code;
        private string _report;
        private string _schedule;
        private string _timeSchedule;
        private string _email;
        private string _reportName;
        private string _startDate;
        private string _endDate;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Report
        {
            get
            {
                return _report;
            }
            set
            {
                _report = value;
            }
        }
        
        public virtual string Schedule
        {
            get
            {
                return _schedule;
            }
            set
            {
                _schedule = value;
            }
        }

        public virtual string TimeSchedule
        {
            get
            {
                return _timeSchedule;
            }
            set
            {
                _timeSchedule = value;
            }
        }

        public virtual string Email
        {
            get
            {
                return _email;
            }
            set
            {
                _email = value;
            }
        }

        public virtual string ReportName
        {
            get
            {
                return _reportName;
            }
            set
            {
                _reportName = value;
            }
        }

        public virtual string StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual string EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }
    }
}
