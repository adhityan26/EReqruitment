using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class SurveyQuestionAnswerOptions
    {
        private string _questionCode;
        private int _number;
        private string _label;

        public virtual string QuestionCode
        {
            get
            {
                return _questionCode;
            }
            set
            {
                _questionCode = value;
            }
        }

        public virtual int Number
        {
            get
            {
                return _number;
            }
            set
            {
                _number = value;
            }
        }

        public virtual string Label
        {
            get
            {
                return _label;
            }
            set
            {
                _label = value;
            }
        }
        
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as SurveyQuestionAnswerOptions;
            if (t == null) return false;
            if (QuestionCode == t.QuestionCode
             && Number == t.Number)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ QuestionCode.GetHashCode();
            hash = (hash * 397) ^ Number.GetHashCode();

            return hash;
        }
        #endregion
    }
}
