using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class VacancyStatus
    {
        private string _code;
        private string _name;
        private string _recruiterLabel;
        private string _candidateLabel;
        private string _description;
        private bool _removable;
        private bool _isArchive;
        private bool _active;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string RecruiterLabel
        {
            get
            {
                return _recruiterLabel;
            }
            set
            {
                _recruiterLabel = value;
            }
        }

        public virtual string CandidateLabel
        {
            get
            {
                return _candidateLabel;
            }
            set
            {
                _candidateLabel = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public virtual bool Removable
        {
            get
            {
                return _removable;
            }
            set
            {
                _removable = value;
            }
        }

        public virtual bool IsArchive
        {
            get
            {
                return _isArchive;
            }
            set
            {
                _isArchive = value;
            }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }
    }
}
