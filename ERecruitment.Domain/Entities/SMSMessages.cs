using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class SMSMessages
    {
        private int _id;
        private string _flow;
        private string _senderNumber;
        private string _recipientNumber;
        private string _messageType;
        private string _message;
        private DateTime _logTime;
        private bool _verified;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }
        public virtual string Flow
        {
            get
            {
                return _flow;
            }
            set
            {
                _flow = value;
            }
        }
        public virtual string SenderNumber
        {
            get
            {
                return _senderNumber;
            }
            set
            {
                _senderNumber = value;
            }
        }
        public virtual string RecipientNumber
        {
            get
            {
                return _recipientNumber;
            }
            set
            {
                _recipientNumber = value;
            }
        }
        public virtual string MessageType
        {
            get
            {
                return _messageType;
            }
            set
            {
                _messageType = value;
            }
        }
        public virtual string Message
        {
            get
            {
                return _message;
            }
            set
            {
                _message = value;
            }
        }
        public virtual DateTime LogTime
        {
            get
            {
                return _logTime;
            }
            set
            {
                _logTime = value;
            }
        }
        public virtual bool Verified
        {
            get { return _verified; }
            set { _verified = value; }
        }
    }
}
