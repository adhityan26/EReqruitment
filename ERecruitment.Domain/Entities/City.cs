using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class City {
        public City() { }
        private string _countryCode;
        private string _countryName;
        private string _provinceCode;
        private string _provinceName;
        private string _cityCode;
        private string _cityname;
        private bool _active;
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public virtual string CountryCode {
            get {
                return _countryCode;
            }
            set {
                _countryCode = value;
            }
        }
        public virtual string CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                _countryName = value;
            }
        }
        public virtual string ProvinceCode {
            get {
                return _provinceCode;
            }
            set {
                _provinceCode = value;
            }
        }
        public virtual string ProvinceName
        {
            get
            {
                return _provinceName;
            }
            set
            {
                _provinceName = value;
            }
        }
        public virtual string CityCode {
            get {
                return _cityCode;
            }
            set {
                _cityCode = value;
            }
        }
        public virtual string CityName {
            get {
                return _cityname;
            }
            set {
                _cityname = value;
            }
        }
       
    }
}
