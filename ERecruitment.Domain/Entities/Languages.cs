using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class Languages {
        public Languages() { }
        private string _code;
        private string _language;
        private bool _active;
        public virtual string Code {
            get {
                return _code;
            }
            set {
                _code = value;
            }
        }
        public virtual  string Language {
            get {
                return _language;
            }
            set {
                _language = value;
            }
        }
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
