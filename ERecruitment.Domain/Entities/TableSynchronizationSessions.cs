using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class TableSynchronizationSessions
    {
        private string _code;
        private DateTime _date;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _processedByCode;
        private string _connectionString;
        private string _mapCodes;
        private string _status;
        private string _synchronizationLogs;
        private string _synchronizationflow;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }

        public virtual DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }

        public virtual string ProcessedByCode
        {
            get
            {
                return _processedByCode;
            }
            set
            {
                _processedByCode = value;
            }
        }

        public virtual string ConnectionString
        {
            get
            {
                return _connectionString;
            }
            set
            {
                _connectionString = value;
            }
        }

        public virtual string MapCodes
        {
            get
            {
                return _mapCodes;
            }
            set
            {
                _mapCodes = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual string SynchronizationLogs
        {
            get
            {
                return _synchronizationLogs;
            }
            set
            {
                _synchronizationLogs = value;
            }
        }

        public virtual string SynchronizationFlow
        {
            get { return _synchronizationflow; }
            set { _synchronizationflow = value; }
        }
    }
}
