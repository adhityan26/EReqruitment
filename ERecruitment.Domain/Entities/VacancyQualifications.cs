using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class VacancyQualifications
    {
        private string _vacancyCode;
        private string _componentCode;
        private string _valueRangeStart;
        private string _valueRangeEnd;
        private float _weight;

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string ComponentCode
        {
            get
            {
                return _componentCode;
            }
            set
            {
                _componentCode = value;
            }
        }

        public virtual string ValueRangeStart
        {
            get
            {
                return _valueRangeStart;
            }
            set
            {
                _valueRangeStart = value;
            }
        }

        public virtual string ValueRangeEnd
        {
            get
            {
                return _valueRangeEnd;
            }
            set
            {
                _valueRangeEnd = value;
            }
        }

        public virtual float Weight
        {
            get
            {
                return _weight;
            }
            set
            {
                _weight = value;
            }
        }
        
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as VacancyQualifications;
            if (t == null) return false;
            if (VacancyCode == t.VacancyCode
             && ComponentCode == t.ComponentCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ VacancyCode.GetHashCode();
            hash = (hash * 397) ^ ComponentCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
