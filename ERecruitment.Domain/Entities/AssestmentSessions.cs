using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class AssestmentSessions
    {
        private string _code;
        private string _applicantCode;
        private string _testCode;
        private DateTime _startDate;
        private DateTime _endDate;
        private string _status;
        private DateTime _actualStartDate;
        private DateTime _actualEndDate;
        private string _assedBy;
        private DateTime _insertStamp;
        private string _insertedBy;
        private DateTime _updateStamp;
        private string _updatedBy;
        private bool _active;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string TestCode
        {
            get
            {
                return _testCode;
            }
            set
            {
                _testCode = value;
            }
        }

        public virtual DateTime StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual DateTime EndDate
        {
            get
            {
                return _endDate;
            }
            set
            {
                _endDate = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual DateTime ActualStartDate
        {
            get
            {
                return _actualStartDate;
            }
            set
            {
                _actualStartDate = value;
            }
        }

        public virtual DateTime ActualEndDate
        {
            get
            {
                return _actualEndDate;
            }
            set
            {
                _actualEndDate = value;
            }
        }

        public virtual string AssedBy
        {
            get
            {
                return _assedBy;
            }
            set
            {
                _assedBy = value;
            }
        }

        public virtual DateTime InsertStamp
        {
            get
            {
                return _insertStamp;
            }
            set
            {
                _insertStamp = value;
            }
        }

        public virtual string InsertedBy
        {
            get
            {
                return _insertedBy;
            }
            set
            {
                _insertedBy = value;
            }
        }

        public virtual DateTime UpdateStamp
        {
            get
            {
                return _updateStamp;
            }
            set
            {
                _updateStamp = value;
            }
        }

        public virtual string UpdatedBy
        {
            get
            {
                return _updatedBy;
            }
            set
            {
                _updatedBy = value;
            }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }
    }
}
