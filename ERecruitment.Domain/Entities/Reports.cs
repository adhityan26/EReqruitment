using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class Reports
    {
        private string _code;
        private string _name;
        private string _reportFile;
        private string _mode;
        private string _schedule;
        private DateTime? _startDate;
        private string _status;
        private List<ReportSubscribers> _subscriberlist;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string ReportFile
        {
            get
            {
                return _reportFile;
            }
            set
            {
                _reportFile = value;
            }
        }

        public virtual string Mode
        {
            get
            {
                return _mode;
            }
            set
            {
                _mode = value;
            }
        }

        public virtual string Schedule
        {
            get
            {
                return _schedule;
            }
            set
            {
                _schedule = value;
            }
        }

        public virtual DateTime? StartDate
        {
            get
            {
                return _startDate;
            }
            set
            {
                _startDate = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual List<ReportSubscribers> SubscriberList
        {
            get { return _subscriberlist; }
            set { _subscriberlist = value; }
        }
    }
}
