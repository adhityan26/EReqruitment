using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class SurveyQuestions
    {
        private string _code;
        private string _type;
        private string _name;
        private string _question;
        private bool _active;
        private List<SurveyQuestionAnswerOptions> _answeroptionlist = new List<SurveyQuestionAnswerOptions>();


        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Question
        {
            get
            {
                return _question;
            }
            set
            {
                _question = value;
            }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }

        public virtual List<SurveyQuestionAnswerOptions> AnswerOptionList
        {
            get { return _answeroptionlist; }
            set { _answeroptionlist = value; }
        }
    }
}
