using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class PipelineAttachments {
        public PipelineAttachments() { }
        private string _pipelineCode;
        private string _attachmentCode;
        private TestAttachments _attachmentobj;
       
        public virtual TestAttachments AttachmentObj
        {
            get { return _attachmentobj; }
            set { _attachmentobj = value; }
        }
        public virtual string AttachmentName
        {
            get { return _attachmentobj.Name; }
        }
        public  virtual string PipelineCode {
            get {
                return _pipelineCode;
            }
            set {
                _pipelineCode = value;
            }
        }
        public  virtual string AttachmentCode {
            get {
                return _attachmentCode;
            }
            set {
                _attachmentCode = value;
            }
        }
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as PipelineAttachments;
			if (t == null) return false;
			if (PipelineCode == t.PipelineCode
			 && AttachmentCode == t.AttachmentCode)
				return true;

			return false;
        }
        public  override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ PipelineCode.GetHashCode();
			hash = (hash * 397) ^ AttachmentCode.GetHashCode();

			return hash;
        }
        #endregion
    }
}
