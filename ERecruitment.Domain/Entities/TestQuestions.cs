using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class TestQuestions
    {
        private string _testCode;
        private string _questionCode;
        private int _durationInSeconds;
        private int _point;
        private bool _canSkip;
        private bool _showtimer;
        private int _orderId;

        public virtual string TestCode
        {
            get
            {
                return _testCode;
            }
            set
            {
                _testCode = value;
            }
        }

        public virtual string QuestionCode
        {
            get
            {
                return _questionCode;
            }
            set
            {
                _questionCode = value;
            }
        }

        public virtual int DurationInSeconds
        {
            get
            {
                return _durationInSeconds;
            }
            set
            {
                _durationInSeconds = value;
            }
        }

        public virtual int Point
        {
            get
            {
                return _point;
            }
            set
            {
                _point = value;
            }
        }

        public virtual bool CanSkip
        {
            get
            {
                return _canSkip;
            }
            set
            {
                _canSkip = value;
            }
        }

        public virtual bool ShowTimer
        {
            get { return _showtimer; }
            set { _showtimer = value; }
        }

        public virtual int OrderId
        {
            get
            {
                return _orderId;
            }
            set
            {
                _orderId = value;
            }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as TestQuestions;
            if (t == null) return false;
            if (TestCode == t.TestCode
             && QuestionCode == t.QuestionCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ TestCode.GetHashCode();
            hash = (hash * 397) ^ QuestionCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
