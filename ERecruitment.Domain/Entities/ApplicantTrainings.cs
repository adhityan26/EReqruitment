using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public partial class ApplicantTrainings {
        private int _id;
        private string _applicantCode;
        private string _trainingName;
        private string _country;
        private string _stateRegion;
        private string _providerName;
        private int _year;
        private string _certified;
        private string _description;
        private DateTime? _startDate;
        private DateTime? _endDate;
        private bool _newCertified;

        public virtual int Id {
            get {
                return _id;
            }
            set {
                _id = value;
            }
        }

        public virtual string ApplicantCode {
            get {
                return _applicantCode;
            }
            set {
                _applicantCode = value;
            }
        }

        public virtual string TrainingName {
            get {
                return _trainingName;
            }
            set {
                _trainingName = value;
            }
        }

        public virtual string Country {
            get {
                return _country;
            }
            set {
                _country = value;
            }
        }

        public virtual string StateRegion {
            get {
                return _stateRegion;
            }
            set {
                _stateRegion = value;
            }
        }

        public virtual string ProviderName {
            get {
                return _providerName;
            }
            set {
                _providerName = value;
            }
        }

        public virtual int Year {
            get {
                return _year;
            }
            set {
                _year = value;
            }
        }

        public virtual string Certified {
            get {
                return _certified;
            }
            set {
                _certified = value;
            }
        }

        public virtual string Description {
            get {
                return _description;
            }
            set {
                _description = value;
            }
        }

        public virtual DateTime? StartDate
        {
            get
            { return _startDate; }
            set
            { _startDate = value; }
        }

        public virtual DateTime? EndDate
        {
            get
            { return _endDate; }
            set
            { _endDate = value; }
        }

        public virtual bool NewCertified
        {
            get
            {
                return _newCertified;
            }
            set
            {
                _newCertified = value;
            }
        }
    }
}
