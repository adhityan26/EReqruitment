using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class EducationMajor
    {
        public EducationMajor() { }
        private string _code;
        private string _major;
        private bool _active;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Major
        {
            get
            {
                return _major;
            }
            set
            {
                _major = value;
            }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
