using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class Roles
    {
        private string _roleCode;
        private string _name;
        private string _companyCodes;
        private bool _accessSetting;
        private bool _accessSetup;
        private bool _accessReport;
        private bool _accessApplicant;
        private bool _importBulkData;
        private bool _createJob;
        private bool _postUnPostJob;
        private bool _deleteJob;
        private bool _archiveJob;
        private bool _schedulingFunctionality;
        private bool _offeringFunctionality;
        private bool _rankFunctionality;
        private bool _referFunctionality;
        private bool _flagFunctionality;
        private bool _disqualifyFunctionality;
        private bool _accessCompensationData;
        private bool _changeApplicantStatus;
        private bool _massScheduling;
        private bool _updateApplicantApplicationData;
        private bool _searchApplicant;
        private bool _exportBulkData;
        private bool _approval;
        private bool _downloaddashboarddata;
        private bool _accesscsstheme;
        private bool _active;

        public virtual string RoleCode
        {
            get
            {
                return _roleCode;
            }
            set
            {
                _roleCode = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string CompanyCodes
        {
            get
            {
                return _companyCodes;
            }
            set
            {
                _companyCodes = value;
            }
        }

        public virtual bool AccessSetting
        {
            get
            {
                return _accessSetting;
            }
            set
            {
                _accessSetting = value;
            }
        }

        public virtual bool AccessSetup
        {
            get
            {
                return _accessSetup;
            }
            set
            {
                _accessSetup = value;
            }
        }

        public virtual bool AccessReport
        {
            get
            {
                return _accessReport;
            }
            set
            {
                _accessReport = value;
            }
        }

        public virtual bool AccessApplicant
        {
            get
            {
                return _accessApplicant;
            }
            set
            {
                _accessApplicant = value;
            }
        }

        public virtual bool ImportBulkData
        {
            get
            {
                return _importBulkData;
            }
            set
            {
                _importBulkData = value;
            }
        }

        public virtual bool CreateJob
        {
            get
            {
                return _createJob;
            }
            set
            {
                _createJob = value;
            }
        }

        public virtual bool PostUnPostJob
        {
            get
            {
                return _postUnPostJob;
            }
            set
            {
                _postUnPostJob = value;
            }
        }

        public virtual bool DeleteJob
        {
            get
            {
                return _deleteJob;
            }
            set
            {
                _deleteJob = value;
            }
        }

        public virtual bool ArchiveJob
        {
            get
            {
                return _archiveJob;
            }
            set
            {
                _archiveJob = value;
            }
        }

        public virtual bool SchedulingFunctionality
        {
            get
            {
                return _schedulingFunctionality;
            }
            set
            {
                _schedulingFunctionality = value;
            }
        }

        public virtual bool OfferingFunctionality
        {
            get
            {
                return _offeringFunctionality;
            }
            set
            {
                _offeringFunctionality = value;
            }
        }

        public virtual bool RankFunctionality
        {
            get
            {
                return _rankFunctionality;
            }
            set
            {
                _rankFunctionality = value;
            }
        }

        public virtual bool ReferFunctionality
        {
            get
            {
                return _referFunctionality;
            }
            set
            {
                _referFunctionality = value;
            }
        }

        public virtual bool FlagFunctionality
        {
            get
            {
                return _flagFunctionality;
            }
            set
            {
                _flagFunctionality = value;
            }
        }

        public virtual bool DisqualifyFunctionality
        {
            get
            {
                return _disqualifyFunctionality;
            }
            set
            {
                _disqualifyFunctionality = value;
            }
        }

        public virtual bool AccessCompensationData
        {
            get
            {
                return _accessCompensationData;
            }
            set
            {
                _accessCompensationData = value;
            }
        }

        public virtual bool ChangeApplicantStatus
        {
            get
            {
                return _changeApplicantStatus;
            }
            set
            {
                _changeApplicantStatus = value;
            }
        }

        public virtual bool MassScheduling
        {
            get
            {
                return _massScheduling;
            }
            set
            {
                _massScheduling = value;
            }
        }

        public virtual bool UpdateApplicantApplicationData
        {
            get
            {
                return _updateApplicantApplicationData;
            }
            set
            {
                _updateApplicantApplicationData = value;
            }
        }

        public virtual bool SearchApplicant
        {
            get
            {
                return _searchApplicant;
            }
            set
            {
                _searchApplicant = value;
            }
        }

        public virtual bool ExportBulkData
        {
            get
            {
                return _exportBulkData;
            }
            set
            {
                _exportBulkData = value;
            }
        }

        public virtual bool Approval
        {
            get
            {
                return _approval;
            }
            set
            {
                _approval = value;
            }
        }

        public virtual bool DownloadDashboardData
        {
            get { return _downloaddashboarddata; }
            set { _downloaddashboarddata = value; }
        }

        public virtual bool AccessCSSTheme
        {
            get { return _accesscsstheme; }
            set { _accesscsstheme = value; }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }
    }
}
