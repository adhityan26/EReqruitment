using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class Companies
    {
        private string _code;
        private string _name;
        private string _addressline1;
        private string _addressline2;
        private string _addressline3;
        private string _status;
        private byte[] _logo;
        private byte[] _coverImage;
        private string _covervideourl;
        private byte[] _aboutUsImage;
        private byte[] _faviconLogo;
        private string _cssTheme;
        private bool _enableMedsosAuthentication;
        private string _applicationTitle;
        private string _industry;
        private string _contactPerson;
        private string _contactNumber;
        private string _contactextensionNumber;
        private string _contactmobileNumber;
        private string _contactmobilePerson;
        private string _website;
        private string _atsSite;
        private string _atsbaseurl;
        private string _mailSender;
        private string _fileName;
        private string _fileType;
        private string _landingOpeningTitle;
        private string _landingOpeningText;
        private string _landingAboutUsTitle;
        private string _landingAboutUsSubTitle;
        private string _landingAboutUsText;
        private string _facebookApiKey;
        private string _linkedInapiKey;
        private string _facebookurl;
        private string _linkedinurl;
        private string _copyrightText;
        private string _surveyquestioncodes;
        private string _surveydeliveryterms;
        private int _surveyemailtemplateid;
        private string _emailSmtpHostAddress;
        private string _emailSmtpPortNumber;
        private string _emailSmtpEmailAddress;
        private string _emailSmtpEmailPassword;
        private bool _emailSmtpUseSsl;
        private string _csscontent;
        private byte[] _coverImage1;
        private byte[] _coverImage2;
        private byte[] _coverImage3;
        private byte[] _coverImage4;
        private string _covervideourl1;
        private string _covervideourl2;
        private string _covervideourl3;
        private string _covervideourl4;
        private string _companyCodes;
        private DateTime _insertStamp;
        private DateTime _updateStamp;
        private string _updatedBy;
        private string _insertedBy;
        private bool _active;
        private bool _isnew;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string AddressLine1
        {
            get
            {
                return _addressline1;
            }
            set
            {
                _addressline1 = value;
            }
        }

        public virtual string AddressLine2
        {
            get
            {
                return _addressline2;
            }
            set
            {
                _addressline2 = value;
            }
        }

        public virtual string AddressLine3
        {
            get
            {
                return _addressline3;
            }
            set
            {
                _addressline3 = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual byte[] Logo
        {
            get
            {
                return _logo;
            }
            set
            {
                _logo = value;
            }
        }

        public virtual byte[] CoverImage
        {
            get
            {
                return _coverImage;
            }
            set
            {
                _coverImage = value;
            }
        }

        public virtual string CoverVideoUrl
        {
            get { return _covervideourl; }
            set { _covervideourl = value; }
        }

        public virtual byte[] AboutUsImage
        {
            get
            {
                return _aboutUsImage;
            }
            set
            {
                _aboutUsImage = value;
            }
        }

        public virtual byte[] FaviconLogo
        {
            get
            {
                return _faviconLogo;
            }
            set
            {
                _faviconLogo = value;
            }
        }

        public virtual string CssTheme
        {
            get
            {
                return _cssTheme;
            }
            set
            {
                _cssTheme = value;
            }
        }

        public virtual bool EnableMedsosAuthentication
        {
            get
            {
                return _enableMedsosAuthentication;
            }
            set
            {
                _enableMedsosAuthentication = value;
            }
        }

        public virtual string ApplicationTitle
        {
            get
            {
                return _applicationTitle;
            }
            set
            {
                _applicationTitle = value;
            }
        }

        public virtual string Industry
        {
            get
            {
                return _industry;
            }
            set
            {
                _industry = value;
            }
        }

        public virtual string ContactPerson
        {
            get
            {
                return _contactPerson;
            }
            set
            {
                _contactPerson = value;
            }
        }

        public virtual string ContactNumber
        {
            get
            {
                return _contactNumber;
            }
            set
            {
                _contactNumber = value;
            }
        }

        public virtual string ContactExtensionNumber
        {
            get { return _contactextensionNumber; }
            set { _contactextensionNumber = value; }
        }

        public virtual string ContactMobileNumber
        {
            get { return _contactmobileNumber; }
            set { _contactmobileNumber = value; }
        }

        public virtual string ContactMobilePerson
        {
            get { return _contactmobilePerson; }
            set { _contactmobilePerson = value; }
        }

        public virtual string Website
        {
            get
            {
                return _website;
            }
            set
            {
                _website = value;
            }
        }

        public virtual string ATSSite
        {
            get
            {
                return _atsSite;
            }
            set
            {
                _atsSite = value;
            }
        }

        public virtual string ATSBaseUrl
        {
            get { return _atsbaseurl; }
            set { _atsbaseurl = value; }
        }

        public virtual string MailSender
        {
            get
            {
                return _mailSender;
            }
            set
            {
                _mailSender = value;
            }
        }

        public virtual string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        public virtual string FileType
        {
            get
            {
                return _fileType;
            }
            set
            {
                _fileType = value;
            }
        }

        public virtual string LandingOpeningTitle
        {
            get
            {
                return _landingOpeningTitle;
            }
            set
            {
                _landingOpeningTitle = value;
            }
        }

        public virtual string LandingOpeningText
        {
            get
            {
                return _landingOpeningText;
            }
            set
            {
                _landingOpeningText = value;
            }
        }

        public virtual string LandingAboutUsTitle
        {
            get
            {
                return _landingAboutUsTitle;
            }
            set
            {
                _landingAboutUsTitle = value;
            }
        }

        public virtual string LandingAboutUsSubTitle
        {
            get
            {
                return _landingAboutUsSubTitle;
            }
            set
            {
                _landingAboutUsSubTitle = value;
            }
        }

        public virtual string LandingAboutUsText
        {
            get
            {
                return _landingAboutUsText;
            }
            set
            {
                _landingAboutUsText = value;
            }
        }

        public virtual string FacebookAPIKey
        {
            get
            {
                return _facebookApiKey;
            }
            set
            {
                _facebookApiKey = value;
            }
        }

        public virtual string LinkedINAPIKey
        {
            get
            {
                return _linkedInapiKey;
            }
            set
            {
                _linkedInapiKey = value;
            }
        }

        public virtual string FacebookUrl
        {
            get { return _facebookurl; }
            set { _facebookurl = value; }
        }

        public virtual string LinkedINUrl
        {
            get { return _linkedinurl; }
            set { _linkedinurl = value; }
        }

        public virtual string CopyrightText
        {
            get
            {
                return _copyrightText;
            }
            set
            {
                _copyrightText = value;
            }
        }

        public virtual string SurveyQuestionCodes
        {
            get { return _surveyquestioncodes; }
            set { _surveyquestioncodes = value; }
        }

        public virtual string SurveyDeliveryTerms
        {
            get { return _surveydeliveryterms; }
            set { _surveydeliveryterms = value; }
        }

        public virtual int SurveyEmailTemplateId
        {
            get { return _surveyemailtemplateid; }
            set { _surveyemailtemplateid = value; }
        }

        public virtual string EmailSmtpHostAddress
        {
            get
            {
                return _emailSmtpHostAddress;
            }
            set
            {
                _emailSmtpHostAddress = value;
            }
        }

        public virtual string EmailSmtpPortNumber
        {
            get
            {
                return _emailSmtpPortNumber;
            }
            set
            {
                _emailSmtpPortNumber = value;
            }
        }

        public virtual string EmailSmtpEmailAddress
        {
            get
            {
                return _emailSmtpEmailAddress;
            }
            set
            {
                _emailSmtpEmailAddress = value;
            }
        }

        public virtual string EmailSmtpEmailPassword
        {
            get
            {
                return _emailSmtpEmailPassword;
            }
            set
            {
                _emailSmtpEmailPassword = value;
            }
        }

        public virtual bool EmailSmtpUseSSL
        {
            get
            {
                return _emailSmtpUseSsl;
            }
            set
            {
                _emailSmtpUseSsl = value;
            }
        }

        public virtual string CssContent
        {
            get { return _csscontent; }
            set { _csscontent = value; }
        }

        public virtual byte[] CoverImage1
        {
            get
            {
                return _coverImage1;
            }
            set
            {
                _coverImage1 = value;
            }
        }

        public virtual byte[] CoverImage2
        {
            get
            {
                return _coverImage2;
            }
            set
            {
                _coverImage2 = value;
            }
        }

        public virtual byte[] CoverImage3
        {
            get
            {
                return _coverImage3;
            }
            set
            {
                _coverImage3 = value;
            }
        }

        public virtual byte[] CoverImage4
        {
            get
            {
                return _coverImage4;
            }
            set
            {
                _coverImage4 = value;
            }
        }

        public virtual string CoverVideoUrl1
        {
            get { return _covervideourl1; }
            set { _covervideourl1 = value; }
        }

        public virtual string CoverVideoUrl2
        {
            get { return _covervideourl2; }
            set { _covervideourl2 = value; }
        }

        public virtual string CoverVideoUrl3
        {
            get { return _covervideourl3; }
            set { _covervideourl3 = value; }
        }

        public virtual string CoverVideoUrl4
        {
            get { return _covervideourl4; }
            set { _covervideourl4 = value; }
        }

        public virtual string CompanyCodes
        {
            get
            {
                return _companyCodes;
            }
            set
            {
                _companyCodes = value;
            }
        }

        public virtual DateTime InsertStamp
        {
            get
            {
                return _insertStamp;
            }
            set
            {
                _insertStamp = value;
            }
        }

        public virtual DateTime UpdateStamp
        {
            get
            {
                return _updateStamp;
            }
            set
            {
                _updateStamp = value;
            }
        }

        public virtual string UpdatedBy
        {
            get
            {
                return _updatedBy;
            }
            set
            {
                _updatedBy = value;
            }
        }

        public virtual string InsertedBy
        {
            get
            {
                return _insertedBy;
            }
            set
            {
                _insertedBy = value;
            }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }

        public virtual bool IsNew
        {
            get { return _isnew; }
            set { _isnew = value; }
        }
    }
}
