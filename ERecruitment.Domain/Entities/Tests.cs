using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class Tests
    {
        public Tests() { }
        private string _testCode;
        private string _testName;
        private int _durationInSeconds;
        private string _description;
        private int _pagesize;
        private bool _showtimer;
        private bool _active;
        private List<TestQuestions> _questionlist = new List<TestQuestions>();

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public virtual string TestCode
        {
            get
            {
                return _testCode;
            }
            set
            {
                _testCode = value;
            }
        }

        public virtual string TestName
        {
            get
            {
                return _testName;
            }
            set
            {
                _testName = value;
            }
        }

        public virtual int DurationInSeconds
        {
            get
            {
                return _durationInSeconds;
            }
            set
            {
                _durationInSeconds = value;
            }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual int PageSize
        {
            get { return _pagesize; }
            set { _pagesize = value; }
        }

        public virtual bool ShowTimer
        {
            get { return _showtimer; }
            set { _showtimer = value; }
        }

        public virtual List<TestQuestions> QuestionList
        {
            get { return _questionlist; }
            set { _questionlist = value; }
        }
    }
}
