using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using SS.DataAccess;
using NHibernate;
using NHibernate.Criterion;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using System.Reflection;


namespace ERecruitment.Domain
{

    public partial class BatchProcessSchemas
    {
        private string _code;
        private string _name;
        private string _assemblyName;
        private string _processerAssemblyName;
        private string _processMethodName;
        private string _schemaGeneratorMethodName;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string AssemblyName
        {
            get
            {
                return _assemblyName;
            }
            set
            {
                _assemblyName = value;
            }
        }

        public virtual string ProcesserAssemblyName
        {
            get
            {
                return _processerAssemblyName;
            }
            set
            {
                _processerAssemblyName = value;
            }
        }

        public virtual string ProcessMethodName
        {
            get
            {
                return _processMethodName;
            }
            set
            {
                _processMethodName = value;
            }
        }

        public virtual string SchemaGeneratorMethodName
        {
            get
            {
                return _schemaGeneratorMethodName;
            }
            set
            {
                _schemaGeneratorMethodName = value;
            }
        }

        public virtual List<string> GenerateImportSchema(string vacancyCode, string applicantStatus)
        {
            List<string> importSchemaList = new List<string>();

            object batchProcessor = null;
            MethodInfo batchProcessCommand = null;

            batchProcessor = Activator.CreateInstance(Type.GetType(this.ProcesserAssemblyName));
            batchProcessCommand = batchProcessor.GetType().GetMethod(this.SchemaGeneratorMethodName);
            importSchemaList = batchProcessCommand.Invoke(batchProcessor, new object[] { vacancyCode, applicantStatus }) as List<string>;

            return importSchemaList;
        }
    }
}
