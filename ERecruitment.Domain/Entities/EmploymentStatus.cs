using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class EmploymentStatus
    {
        public EmploymentStatus() { }
        private string code;
        private string name;
        private bool active;
        public virtual string Code
        {
            get { return code; }
            set { code = value; }
        }
        public virtual string Name
        {
            get { return name; }
            set { name = value; }
        }
        public virtual bool Active
        {
            get { return active; }
            set { active = value; }
        }
    }
}
