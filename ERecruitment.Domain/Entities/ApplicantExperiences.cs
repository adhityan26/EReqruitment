using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantExperiences
    {
        private int _id;
        private string _applicantCode;
        private string _experienceType;
        private string _organizationName;
        private string _positionName;
        private int _startYear;
        private int _startMonth;
        private int _endYear;
        private int _endMonth;
        private bool _stillUntillNow;
        private string _positionLevel;
        private string _employmentStatus;
        private string _salaryCurrency;
        private decimal _salaryAmount;
        private string _salaryMethod;
        private string _reasonForLeaving;
        private string _jobDescription;
        private string _startmonthyear;
        private string _endmonthyear;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string ExperienceType
        {
            get
            {
                return _experienceType;
            }
            set
            {
                _experienceType = value;
            }
        }

        public virtual string OrganizationName
        {
            get
            {
                return _organizationName;
            }
            set
            {
                _organizationName = value;
            }
        }

        public virtual string PositionName
        {
            get
            {
                return _positionName;
            }
            set
            {
                _positionName = value;
            }
        }

        public virtual int StartYear
        {
            get
            {
                return _startYear;
            }
            set
            {
                _startYear = value;
            }
        }

        public virtual int StartMonth
        {
            get
            {
                return _startMonth;
            }
            set
            {
                _startMonth = value;
            }
        }

        public virtual int EndYear
        {
            get
            {
                return _endYear;
            }
            set
            {
                _endYear = value;
            }
        }

        public virtual int EndMonth
        {
            get
            {
                return _endMonth;
            }
            set
            {
                _endMonth = value;
            }
        }

        public virtual bool StillUntillNow
        {
            get
            {
                return _stillUntillNow;
            }
            set
            {
                _stillUntillNow = value;
            }
        }

        public virtual string PositionLevel
        {
            get
            {
                return _positionLevel;
            }
            set
            {
                _positionLevel = value;
            }
        }

        public virtual string EmploymentStatus
        {
            get
            {
                return _employmentStatus;
            }
            set
            {
                _employmentStatus = value;
            }
        }

        public virtual string SalaryCurrency
        {
            get
            {
                return _salaryCurrency;
            }
            set
            {
                _salaryCurrency = value;
            }
        }

        public virtual decimal SalaryAmount
        {
            get
            {
                return _salaryAmount;
            }
            set
            {
                _salaryAmount = value;
            }
        }

        public virtual string SalaryMethod
        {
            get
            {
                return _salaryMethod;
            }
            set
            {
                _salaryMethod = value;
            }
        }

        public virtual string ReasonForLeaving
        {
            get
            {
                return _reasonForLeaving;
            }
            set
            {
                _reasonForLeaving = value;
            }
        }

        public virtual string JobDescription
        {
            get
            {
                return _jobDescription;
            }
            set
            {
                _jobDescription = value;
            }
        }

        public virtual string StartMonthYear
        {
            get
            {
                return _startmonthyear;
            }
            set
            {
                _startmonthyear = value;
            }
        }

        public virtual string EndMonthYear
        {
            get
            {
                return _endmonthyear;
            }
            set
            {
                _endmonthyear = value;
            }
        }
    }
}
