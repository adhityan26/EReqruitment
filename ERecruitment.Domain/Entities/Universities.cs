using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class Universities
    {
        private string _code;
        private string _name;
        private string _countryCode;
        private bool _isaffiliated;
        private int _kategori;
        private bool _active;
        private List<UniversityMajors> _majorlist = new List<UniversityMajors>();

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                _countryCode = value;
            }
        }

        public virtual bool IsAffiliated
        {
            get { return _isaffiliated; }
            set { _isaffiliated = value; }
        }


        public virtual int Kategori
        {
            get { return _kategori; }
            set { _kategori = value; }
        }

        public virtual List<UniversityMajors> MajorList
        {
            get { return _majorlist; }
            set { _majorlist = value; }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }
    }
}
