using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class ApplicantsVacancyMilestoneLogs {
        public ApplicantsVacancyMilestoneLogs() { }
        private int _id;
        private string _applicantCode;
        private string _vacancyCode;
        private string _status;
        private DateTime _date;
        public virtual int Id {
            get {
                return _id;
            }
            set {
                _id = value;
            }
        }
        public virtual string ApplicantCode {
            get {
                return _applicantCode;
            }
            set {
                _applicantCode = value;
            }
        }
        public virtual string VacancyCode {
            get {
                return _vacancyCode;
            }
            set {
                _vacancyCode = value;
            }
        }
        public virtual string Status {
            get {
                return _status;
            }
            set {
                _status = value;
            }
        }
        public virtual DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }
    }
}
