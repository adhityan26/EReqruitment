using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class TestAttachments {
        public TestAttachments() { }
        private string _attachmentCode;
        private string _name;
        private bool _active;
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public virtual string AttachmentCode {
            get {
                return _attachmentCode;
            }
            set {
                _attachmentCode = value;
            }
        }
        public virtual string Name {
            get {
                return _name;
            }
            set {
                _name = value;
            }
        }
    }
}
