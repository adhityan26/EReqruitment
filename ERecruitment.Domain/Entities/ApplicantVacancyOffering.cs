using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class ApplicantVacancyOffering
    {
        public ApplicantVacancyOffering() { }
        private int _id;
        private string _applicantCode;
        private string _vacancyCode;
        private string _status;
        private DateTime _createddate;
        private string _createdBy;
        private decimal _annualSalary;
        private decimal _monthlySalary;
        private decimal _thr;
        private int _annualLeave;
        private decimal _transportation;
        private decimal _telecommunication;
        private decimal _bonus;
        private decimal _incentive;
        private decimal _otherAllowance1;
        private decimal _otherAllowance2;
        private decimal _otherAllowance3;
        private UserAccounts _issuedbyobj;
        private DateTime _effectivedate;
        private string _notes;
        private Vacancies _vacancy;
        private Applicants _applicant;
        private string _applicantstatusnotification;
        private bool _eligibleforthr;
        private bool _eligibleforincentive;
        private bool _eligibleforbonus;

        public virtual string ApplicantStatusNotification
        {
            get { return _applicantstatusnotification; }
            set { _applicantstatusnotification = value; }
        }

        public virtual Vacancies Vacancy
        {
            get { return _vacancy; }
            set { _vacancy = value; }
        }

        public virtual Applicants Applicant
        {
            get { return _applicant; }
            set { _applicant = value; }
        }

        public virtual string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        public virtual DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }

        public virtual DateTime EffectiveDate
        {
            get { return _effectivedate; }
            set { _effectivedate = value; }
        }

        public virtual UserAccounts IssuedObj
        {
            get { return _issuedbyobj; }
            set { _issuedbyobj = value; }
        }

        public virtual string IssuedBy
        {
            get { return _issuedbyobj.Name; }
        }

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual string CreatedBy
        {
            get
            {
                return _createdBy;
            }
            set
            {
                _createdBy = value;
            }
        }

        public virtual decimal AnnualSalary
        {
            get
            {
                return _annualSalary;
            }
            set
            {
                _annualSalary = value;
            }
        }

        public virtual decimal MonthlySalary
        {
            get
            {
                return _monthlySalary;
            }
            set
            {
                _monthlySalary = value;
            }
        }

        public virtual decimal Thr
        {
            get
            {
                return _thr;
            }
            set
            {
                _thr = value;
            }
        }

        public virtual int AnnualLeave
        {
            get
            {
                return _annualLeave;
            }
            set
            {
                _annualLeave = value;
            }
        }

        public virtual decimal Transportation
        {
            get
            {
                return _transportation;
            }
            set
            {
                _transportation = value;
            }
        }

        public virtual decimal Telecommunication
        {
            get
            {
                return _telecommunication;
            }
            set
            {
                _telecommunication = value;
            }
        }

        public virtual decimal Bonus
        {
            get
            {
                return _bonus;
            }
            set
            {
                _bonus = value;
            }
        }

        public virtual decimal Incentive
        {
            get
            {
                return _incentive;
            }
            set
            {
                _incentive = value;
            }
        }

        public virtual decimal OtherAllowance1
        {
            get
            {
                return _otherAllowance1;
            }
            set
            {
                _otherAllowance1 = value;
            }
        }

        public virtual decimal OtherAllowance2
        {
            get
            {
                return _otherAllowance2;
            }
            set
            {
                _otherAllowance2 = value;
            }
        }

        public virtual decimal OtherAllowance3
        {
            get
            {
                return _otherAllowance3;
            }
            set
            {
                _otherAllowance3 = value;
            }
        }
        
        public virtual bool EligibleForTHR
        {
            get { return _eligibleforthr; }
            set { _eligibleforthr = value; }
        }

        public virtual bool EligibleForIncentive
        {
            get { return _eligibleforincentive; }
            set { _eligibleforincentive = value; }
        }

        public virtual bool EligibleForBonus
        {
            get { return _eligibleforbonus; }
            set { _eligibleforbonus = value; }
        }
    }
}
