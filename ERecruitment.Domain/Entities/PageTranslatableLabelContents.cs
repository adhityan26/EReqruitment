using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class PageTranslatableLabelContents
    {
        private string _companyCode;
        private string _pageCode;
        private string _label;
        private string _idText;
        private string _enText;
        private int _orderId;
        private bool _isnew;

        public virtual string CompanyCode
        {
            get
            {
                return _companyCode;
            }
            set
            {
                _companyCode = value;
            }
        }

        public virtual string PageCode
        {
            get
            {
                return _pageCode;
            }
            set
            {
                _pageCode = value;
            }
        }

        public virtual string Label
        {
            get
            {
                return _label;
            }
            set
            {
                _label = value;
            }
        }

        public virtual string IDText
        {
            get
            {
                return _idText;
            }
            set
            {
                _idText = value;
            }
        }

        public virtual string ENText
        {
            get
            {
                return _enText;
            }
            set
            {
                _enText = value;
            }
        }

        public virtual int OrderID
        {
            get
            {
                return _orderId;
            }
            set
            {
                _orderId = value;
            }
        }

        public virtual bool IsNew
        {
            get { return _isnew; }
            set { _isnew = value; }
        }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as PageTranslatableLabelContents;
            if (t == null) return false;
            if (CompanyCode == t.CompanyCode
             && PageCode == t.PageCode
             && Label == t.Label)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ CompanyCode.GetHashCode();
            hash = (hash * 397) ^ PageCode.GetHashCode();
            hash = (hash * 397) ^ Label.GetHashCode();

            return hash;
        }

        #endregion
    }
}
