using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public  class OrganizationPosition {
        public OrganizationPosition() { }
        private string _code;
        private string _position;
        private bool _active;
        public virtual string Code {
            get {
                return _code;
            }
            set {
                _code = value;
            }
        }
        public virtual string Position {
            get {
                return _position;
            }
            set {
                _position = value;
            }
        }
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
