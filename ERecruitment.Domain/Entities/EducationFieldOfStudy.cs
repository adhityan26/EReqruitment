using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class EducationFieldOfStudy
    {
        public EducationFieldOfStudy() { }
        private string _code;
        private string _name;
        private bool _active;
        private int _kategori;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }

        public virtual int Kategori
        {
            get { return _kategori; }
            set { _kategori = value; }
        }
    }
}
