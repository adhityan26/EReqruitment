using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantSurveyAnswers
    {
        private string _applicantCode;
        private string _disqualifierCode;
        private string _answer;

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string DisqualifierCode
        {
            get
            {
                return _disqualifierCode;
            }
            set
            {
                _disqualifierCode = value;
            }
        }

        public virtual string Answer
        {
            get
            {
                return _answer;
            }
            set
            {
                _answer = value;
            }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicantSurveyAnswers;
            if (t == null) return false;
            if (ApplicantCode == t.ApplicantCode
             && DisqualifierCode == t.DisqualifierCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ApplicantCode.GetHashCode();
            hash = (hash * 397) ^ DisqualifierCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
