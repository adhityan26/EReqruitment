using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public partial class TableMapProperties {

        private string _mapCode;
        private string _propertyName;
        private string _externalPropertyName;
        private string _dataType;
        private bool _isKey;
        private bool _allowNull;
        private string _defaultValue;

        public virtual string MapCode {
            get {
                return _mapCode;
            }
            set {
                _mapCode = value;
            }
        }

        public virtual string PropertyName {
            get {
                return _propertyName;
            }
            set {
                _propertyName = value;
            }
        }

        public virtual string ExternalPropertyName {
            get {
                return _externalPropertyName;
            }
            set {
                _externalPropertyName = value;
            }
        }

        public virtual string DataType {
            get {
                return _dataType;
            }
            set {
                _dataType = value;
            }
        }

        public virtual bool IsKey {
            get {
                return _isKey;
            }
            set {
                _isKey = value;
            }
        }

        public virtual bool AllowNull {
            get {
                return _allowNull;
            }
            set {
                _allowNull = value;
            }
        }

        public virtual string DefaultValue {
            get {
                return _defaultValue;
            }
            set {
                _defaultValue = value;
            }
        }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as TableMapProperties;
			if (t == null) return false;
			if (MapCode == t.MapCode
			 && PropertyName == t.PropertyName)
				return true;

			return false;
        }

        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ MapCode.GetHashCode();
			hash = (hash * 397) ^ PropertyName.GetHashCode();

			return hash;
        }
        
        #endregion
    }
}
