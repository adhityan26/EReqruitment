using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantAttachments
    {
        private int _id;
        private string _attachmentType;
        private string _applicantCode;
        private byte[] _attachment;
        private string _fileName;
        private string _fileType;
        private string _notes;
        private string _insertedBy;
        private DateTime? _insertStamp;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string AttachmentType
        {
            get
            {
                return _attachmentType;
            }
            set
            {
                _attachmentType = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual byte[] Attachment
        {
            get
            {
                return _attachment;
            }
            set
            {
                _attachment = value;
            }
        }

        public virtual string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        public virtual string FileType
        {
            get
            {
                return _fileType;
            }
            set
            {
                _fileType = value;
            }
        }

        public virtual string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public virtual string InsertedBy
        {
            get
            {
                return _insertedBy;
            }
            set
            {
                _insertedBy = value;
            }
        }

        public virtual DateTime? InsertStamp
        {
            get
            {
                return _insertStamp;
            }
            set
            {
                _insertStamp = value;
            }
        }
    }
}
