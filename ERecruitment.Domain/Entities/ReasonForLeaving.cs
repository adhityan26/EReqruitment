using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class ReasonForLeaving {
        private string _name;
        public virtual string Name {
            get {
                return _name;
            }
            set {
                _name = value;
            }
        }
    }
}
