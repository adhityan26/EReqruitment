using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class Pipelines
    {
        public Pipelines() { }
        private string _code;
        private string _name;
        private string _testcodes;
        private string _attachmentcodes;
        private string _autodisqualifiedrecruitmentstatuscode;
        private string _refereddisqualifiedrecruitmentstatuscode;
        private string _hiredrecruitmentstatuscode;
        private string _disqualifiedrecruitmentstatuscode;
        private int _autodisqualifiedrecruitmentstatusemailtemplateid;
        private int _refereddisqualifiedrecruitmentstatusemailtemplateid;
        private int _hiredrecruitmentstatusemailtemplateid;
        private int _disqualifiedrecruitmentstatusemailtemplateid;
        private bool _active;
        private List<PipelineSteps> _steplist;
        private List<PipelineTests> _teslist;
        private List<PipelineAttachments> _attachmentlist;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string TestCodes
        {
            get { return _testcodes; }
            set { _testcodes = value; }
        }

        public virtual string AttachmentCodes
        {
            get { return _attachmentcodes; }
            set { _attachmentcodes = value; }
        }

        public virtual string AutoDisqualifiedRecruitmentStatusCode
        {
            get { return _autodisqualifiedrecruitmentstatuscode; }
            set { _autodisqualifiedrecruitmentstatuscode = value; }
        }

        public virtual string ReferedDisqualifiedRecruitmentStatusCode
        {
            get { return _refereddisqualifiedrecruitmentstatuscode; }
            set { _refereddisqualifiedrecruitmentstatuscode = value; }
        }

        public virtual string HiredRecruitmentStatusCode
        {
            get { return _hiredrecruitmentstatuscode; }
            set { _hiredrecruitmentstatuscode = value; }
        }

        public virtual string DisqualifiedRecruitmentStatusCode
        {
            get { return _disqualifiedrecruitmentstatuscode; }
            set { _disqualifiedrecruitmentstatuscode = value; }
        }

        public virtual int AutoDisqualifiedRecruitmentStatusEmailTemplateId
        {
            get { return _autodisqualifiedrecruitmentstatusemailtemplateid; }
            set { _autodisqualifiedrecruitmentstatusemailtemplateid = value; }
        }

        public virtual int ReferedDisqualifiedRecruitmentStatusEmailTemplateId
        {
            get { return _refereddisqualifiedrecruitmentstatusemailtemplateid; }
            set { _refereddisqualifiedrecruitmentstatusemailtemplateid = value; }
        }

        public virtual int HiredRecruitmentStatusEmailTemplateId
        {
            get { return _hiredrecruitmentstatusemailtemplateid; }
            set { _hiredrecruitmentstatusemailtemplateid = value; }
        }

        public virtual int DisqualifiedRecruitmentStatusEmailTemplateId
        {
            get { return _disqualifiedrecruitmentstatusemailtemplateid; }
            set { _disqualifiedrecruitmentstatusemailtemplateid = value; }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }

        public virtual List<PipelineSteps> StepList
        {
            get { return _steplist; }
            set { _steplist = value; }
        }

        public virtual List<PipelineTests> TestList
        {
            get { return _teslist; }
            set { _teslist = value; }
        }

        public virtual List<PipelineAttachments> AttachmentList
        {
            get { return _attachmentlist; }
            set { _attachmentlist = value; }
        }

    }
}
