﻿using System;
using System.Text;
using System.Collections.Generic;
namespace ERecruitment.Domain
{
    public class EducationMappingDetail
    {
        public EducationMappingDetail() { }
        private string _educationMappingDetailCode;
        private string _educationMappingHeaderCode;
        private string _universitiesCode;
        //private string _universitiesName;
        private string _educationLevel;
        //private string _educationLevelName;
        private string _fieldOfStudyCode;
        //private string _fieldOfStudyName;
        private string _majorCode;
        private string _majorName;
        private bool _active;

        public virtual string EducationMappingDetailCode
        {
            get { return _educationMappingDetailCode; }
            set { _educationMappingDetailCode = value; }
        }

        public virtual string EducationMappingHeaderCode
        {
            get { return _educationMappingHeaderCode; }
            set { _educationMappingHeaderCode = value; }
        }

        public virtual string UniversitiesCode
        {
            get { return _universitiesCode; }
            set { _universitiesCode = value; }
        }

        //public virtual string UniversitiesName
        //{
        //    get { return _universitiesName; }
        //    set { _universitiesName = value; }
        //}

        public virtual string EducationLevel
        {
            get { return _educationLevel; }
            set { _educationLevel = value; }
        }

        //public virtual string EducationLevelName
        //{
        //    get { return _educationLevelName; }
        //    set { _educationLevelName = value; }
        //}

        public virtual string FieldOfStudyCode
        {
            get { return _fieldOfStudyCode; }
            set { _fieldOfStudyCode = value; }
        }

        //public virtual string FieldOfStudyName
        //{
        //    get { return _fieldOfStudyName; }
        //    set { _fieldOfStudyName = value; }
        //}

        public virtual string MajorCode
        {
            get { return _majorCode; }
            set { _majorCode = value; }
        }

        public virtual string MajorName
        {
            get { return _majorName; }
            set { _majorName = value; }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
