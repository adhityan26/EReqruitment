using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class RankQualificationComponents
    {
        private string _code;
        private string _name;
        private string _evaluatorMethodName;
        private string _evaluatorAssemblyName;

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string EvaluatorMethodName
        {
            get
            {
                return _evaluatorMethodName;
            }
            set
            {
                _evaluatorMethodName = value;
            }
        }

        public virtual string EvaluatorAssemblyName
        {
            get
            {
                return _evaluatorAssemblyName;
            }
            set
            {
                _evaluatorAssemblyName = value;
            }
        }
    }
}
