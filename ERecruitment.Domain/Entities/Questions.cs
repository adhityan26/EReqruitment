using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class Questions
    {
        private string _code;
        private string _type;
        private string _name;
        private string _description;
        private byte[] _image;
        private string _question;
        private string _answerFormat;
        private string _correctAnswerOperator;
        private string _correctAnswerLabel;
        private int _orderId;
        private bool _active;
        private List<QuestionAnswers> _answeroptionlist = new List<QuestionAnswers>();

        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string Type
        {
            get
            {
                return _type;
            }
            set
            {
                _type = value;
            }
        }

        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public virtual string Description
        {
            get
            {
                return _description;
            }
            set
            {
                _description = value;
            }
        }

        public virtual byte[] Image
        {
            get
            {
                return _image;
            }
            set
            {
                _image = value;
            }
        }

        public virtual string Question
        {
            get
            {
                return _question;
            }
            set
            {
                _question = value;
            }
        }

        public virtual string AnswerFormat
        {
            get
            {
                return _answerFormat;
            }
            set
            {
                _answerFormat = value;
            }
        }

        public virtual string CorrectAnswerOperator
        {
            get
            {
                return _correctAnswerOperator;
            }
            set
            {
                _correctAnswerOperator = value;
            }
        }

        public virtual string CorrectAnswerLabel
        {
            get
            {
                return _correctAnswerLabel;
            }
            set
            {
                _correctAnswerLabel = value;
            }
        }

        public virtual int OrderId
        {
            get
            {
                return _orderId;
            }
            set
            {
                _orderId = value;
            }
        }

        public virtual bool Active
        {
            get
            {
                return _active;
            }
            set
            {
                _active = value;
            }
        }

        public virtual List<QuestionAnswers> AnswerOptionList
        {
            get { return _answeroptionlist; }
            set { _answeroptionlist = value; }
        }
    }
}
