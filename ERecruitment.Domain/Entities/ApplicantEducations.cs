using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantEducations
    {
        private int _id;
        private string _applicantCode;
        private string _levelcode;
        private int _startYear;
        private int _endYear;
        private string _educationMappingDetailCode;
        private string _institutionCode;
        private string _institutionName;
        private string _fieldOfStudyCode;
        private string _majorCode;
        private float _score;
        private float _scoretotal;
        private string _educationcitycode;
        private string _levelname;
        private string _fieldofstudyname;
        private string _majorname;
        private string _educationcountrycode;
        private string _educationcityname;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string LevelCode
        {
            get { return _levelcode; }
            set { _levelcode = value; }
        }

        public virtual int StartYear
        {
            get
            {
                return _startYear;
            }
            set
            {
                _startYear = value;
            }
        }

        public virtual int EndYear
        {
            get
            {
                return _endYear;
            }
            set
            {
                _endYear = value;
            }
        }

        public virtual string EducationMappingDetailCode
        {
            get
            {
                return _educationMappingDetailCode;
            }
            set
            {
                _educationMappingDetailCode = value;
            }
        }

        public virtual string InstitutionCode
        {
            get
            {
                return _institutionCode;
            }
            set
            {
                _institutionCode = value;
            }
        }

        public virtual string InstitutionName
        {
            get
            {
                return _institutionName;
            }
            set
            {
                _institutionName = value;
            }
        }

        public virtual string FieldOfStudyCode
        {
            get
            {
                return _fieldOfStudyCode;
            }
            set
            {
                _fieldOfStudyCode = value;
            }
        }

        public virtual string EducationCityCode
        {
            get { return _educationcitycode; }
            set { _educationcitycode = value; }
        }

        public virtual string MajorCode
        {
            get
            {
                return _majorCode;
            }
            set
            {
                _majorCode = value;
            }
        }

        public virtual float Score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
            }
        }

        public virtual float ScoreTotal
        {
            get
            {
                return _scoretotal;
            }
            set
            {
                _scoretotal = value;
            }
        }

        public virtual string LevelName
        {
            get { return _levelname; }
            set { _levelname = value; }
        }

        public virtual string FieldOfStudyName
        {
            get { return _fieldofstudyname; }
            set { _fieldofstudyname = value; }
        }

        public virtual string MajorName
        {
            get { return _majorname; }
            set { _majorname = value; }
        }

        public virtual string EducationCountryCode
        {
            get { return _educationcountrycode; }
            set { _educationcountrycode = value; }
        }

        public virtual string EducationCityName
        {
            get { return _educationcityname; }
            set { _educationcityname = value; }
        }
    }
}
