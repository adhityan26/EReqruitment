using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public  class OrganizationType {
        private string _code;
        private string _type;
        private bool _active;
        public virtual string Code {
            get {
                return _code;
            }
            set {
                _code = value;
            }
        }
        public virtual string Type {
            get {
                return _type;
            }
            set {
                _type = value;
            }
        }
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
