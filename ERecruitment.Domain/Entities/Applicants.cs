using System;
using System.Data;
using System.Collections;
using System.Diagnostics;


namespace ERecruitment.Domain
{

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class Applicants
    {


        public Applicants() { }
        private string _code;
        private byte[] _photo;
        private byte[] _cv;
        private string _name;
        private string _firstname;
        private string _lastname;
        private string _genderspecification;
        private string _maritalstatusspecification;
        private int _yearsofexperience;
        private int _age;
        private DateTime? _birthday;
        private string _birthplace;
        private string _address;
        private string _email;
        private string _phone;
        private string _alternativephonenumber;
        private string _mobile;
        private string _religion;
        private string _nationality;
        private string _numberofchildren;
        private DateTime _registereddate;
        private byte[] _attachment;
        private string _filename;
        private string _filetype;
        private byte[] _otherattachment;
        private string _otherfilename;
        private string _otherfiletype;
        private bool _previouslyemployed;
        private string _idcardnumber;
        private string _socmedname;
        private string _socmedid;
        private string _idcardaddress;
        private string _idcardcountry;
        private string _idcardprovince;
        private string _idcardcity;
        private string _idcardpostalcode;
        private string _currentcardaddress;
        private string _currentcardcountry;
        private string _currentcardprovince;
        private string _currentcardcity;
        private string _currentcardpostalcode;
        private bool _verified;
        private DateTime? _expireddate;
        private bool _surveyinvitationsent;
        private bool _surveytaken;
        private bool _active;
        private decimal _expectedsalary;
        private string _expectedpositionorfunction;
        private int _noticeperiod;
        private string _expectedsalarycurrency;
        private string _expectedsalarymethod;
        private string _eligibleforanycities;
        private string _cvfilename;
        private string _cvfiletype;
        private string _facebookurl;
        private string _twitterurl;
        private string _linkedinurl;
        private UserAccounts _account;
        private UserExternalAccounts _externalaccount;
        private string _latesteducationlevelcode;
        private string _latestorganizationexperience;
        private string _flagnotes;
        private string _flagStatus;
        private string _flagissuedByCode;
        private string _flagissuedbyname;
        private DateTime _flagissuedDate;
        private string _affiliatedinstitutioncode;
        private string _affiliatedmajorcode;
        private string _companycode;
        private string _companyname;
        private int _jobappliedcount;
        private string _currentcardcityname;
        private int _applicantage;
        private string _applicantlasteducationinfo;
        private string _applicantlastexperienceinfo;

        private string _linkedinimageurl;
        private string _refvacancycode;
        private string _linkedinid;
        private string _facebookid;
        private string _isregis;

        public virtual byte[] CV
        {
            get { return _cv; }
            set { _cv = value; }
        }

        public virtual string CVFileName
        {
            get { return _cvfilename; }
            set { _cvfilename = value; }
        }

        public virtual string CVFileType
        {
            get { return _cvfiletype; }
            set { _cvfiletype = value; }
        }

        public virtual string Code
        {
            [DebuggerStepThrough]
            get
            { return _code; }
            [DebuggerStepThrough]
            set
            { _code = value; }
        }

        public virtual byte[] Photo
        {
            get { return _photo; }
            set { _photo = value; }
        }

        public virtual string Name
        {
            [DebuggerStepThrough]
            get
            { return _name; }
            [DebuggerStepThrough]
            set
            { _name = value; }
        }

        public virtual string FirstName
        {
            get { return _firstname; }
            set { _firstname = value; }
        }

        public virtual string LastName
        {
            get { return _lastname; }
            set { _lastname = value; }
        }

        public virtual string GenderSpecification
        {
            [DebuggerStepThrough]
            get
            { return _genderspecification; }
            [DebuggerStepThrough]
            set
            { _genderspecification = value; }
        }

        public virtual string MaritalStatusSpecification
        {
            [DebuggerStepThrough]
            get
            { return _maritalstatusspecification; }
            [DebuggerStepThrough]
            set
            { _maritalstatusspecification = value; }
        }

        public virtual int YearsOfExperience
        {
            [DebuggerStepThrough]
            get
            { return _yearsofexperience; }
            [DebuggerStepThrough]
            set
            { _yearsofexperience = value; }
        }

        public virtual int Age
        {
            [DebuggerStepThrough]
            get
            { return _age; }
            [DebuggerStepThrough]
            set
            { _age = value; }
        }

        public virtual DateTime? Birthday
        {
            [DebuggerStepThrough]
            get
            { return _birthday; }
            [DebuggerStepThrough]
            set
            { _birthday = value; }
        }

        public virtual string BirthPlace
        {
            [DebuggerStepThrough]
            get
            { return _birthplace; }
            [DebuggerStepThrough]
            set
            { _birthplace = value; }
        }

        public virtual string Address
        {
            [DebuggerStepThrough]
            get
            { return _address; }
            [DebuggerStepThrough]
            set
            { _address = value; }
        }

        public virtual string Email
        {
            [DebuggerStepThrough]
            get
            { return _email; }
            [DebuggerStepThrough]
            set
            { _email = value; }
        }

        public virtual string Phone
        {
            [DebuggerStepThrough]
            get
            { return _phone; }
            [DebuggerStepThrough]
            set
            { _phone = value; }
        }

        public virtual string AlternativePhoneNumber
        {
            get { return _alternativephonenumber; }
            set { _alternativephonenumber = value; }
        }

        public virtual string Mobile
        {
            [DebuggerStepThrough]
            get
            { return _mobile; }
            [DebuggerStepThrough]
            set
            { _mobile = value; }
        }

        public virtual string Religion
        {
            get { return _religion; }
            set { _religion = value; }
        }

        public virtual string Nationality
        {
            get { return _nationality; }
            set { _nationality = value; }
        }

        public virtual string NumberOfChildren
        {
            get { return _numberofchildren; }
            set { _numberofchildren = value; }
        }

        public virtual DateTime RegisteredDate
        {
            [DebuggerStepThrough]
            get
            { return _registereddate; }
            [DebuggerStepThrough]
            set
            { _registereddate = value; }
        }

        public virtual byte[] Attachment
        {
            get { return _attachment; }
            set { _attachment = value; }
        }

        public virtual string FileName
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public virtual string FileType
        {
            get { return _filetype; }
            set { _filetype = value; }
        }

        public virtual byte[] OtherAttachment
        {
            get { return _otherattachment; }
            set { _otherattachment = value; }
        }

        public virtual string OtherFileName
        {
            get { return _otherfilename; }
            set { _otherfilename = value; }
        }

        public virtual string OtherFileType
        {
            get { return _otherfiletype; }
            set { _otherfiletype = value; }
        }

        public virtual bool PreviouslyEmployed
        {
            get { return _previouslyemployed; }
            set { _previouslyemployed = value; }
        }

        public virtual string IDCardNumber
        {
            get { return _idcardnumber; }
            set { _idcardnumber = value; }
        }

        public virtual string SocMedName
        {
            get { return _socmedname; }
            set { _socmedname = value; }
        }

        public virtual string SocMedID
        {
            get { return _socmedid; }
            set { _socmedid = value; }
        }

        public virtual string IDCardAddress
        {
            get { return _idcardaddress; }
            set { _idcardaddress = value; }
        }

        public virtual string IDCardCountry
        {
            get { return _idcardcountry; }
            set { _idcardcountry = value; }
        }

        public virtual string IDCardProvince
        {
            get { return _idcardprovince; }
            set { _idcardprovince = value; }
        }

        public virtual string IDCardCity
        {
            get { return _idcardcity; }
            set { _idcardcity = value; }
        }

        public virtual string IDCardPostalCode
        {
            get { return _idcardpostalcode; }
            set { _idcardpostalcode = value; }
        }

        public virtual string CurrentCardAddress
        {
            get { return _currentcardaddress; }
            set { _currentcardaddress = value; }
        }

        public virtual string CurrentCardCountry
        {
            get { return _currentcardcountry; }
            set { _currentcardcountry = value; }
        }

        public virtual string CurrentCardProvince
        {
            get { return _currentcardprovince; }
            set { _currentcardprovince = value; }
        }

        public virtual string CurrentCardCity
        {
            get { return _currentcardcity; }
            set { _currentcardcity = value; }
        }

        public virtual string CurrentCardPostalCode
        {
            get { return _currentcardpostalcode; }
            set { _currentcardpostalcode = value; }
        }

        public virtual string ExpectedSalaryCurrency
        {
            get { return _expectedsalarycurrency; }
            set { _expectedsalarycurrency = value; }
        }

        public virtual string ExpectedSalaryMethod
        {
            get { return _expectedsalarymethod; }
            set { _expectedsalarymethod = value; }
        }

        public virtual decimal ExpectedSalary
        {
            get { return _expectedsalary; }
            set { _expectedsalary = value; }
        }

        public virtual string EligibleForAnyCities
        {
            get { return _eligibleforanycities; }
            set { _eligibleforanycities = value; }

        }

        public virtual string ExpectedPositionOrFunction
        {

            get { return _expectedpositionorfunction; }
            set { _expectedpositionorfunction = value; }
        }

        public virtual int NoticePeriod
        {
            get { return _noticeperiod; }
            set { _noticeperiod = value; }

        }

        public virtual bool Verified
        {
            get { return _verified; }
            set { _verified = value; }
        }

        public virtual DateTime? ExpiredDate
        {
            get { return _expireddate; }
            set { _expireddate = value; }
        }

        public virtual bool SurveyInvitationSent
        {
            get { return _surveyinvitationsent; }
            set { _surveyinvitationsent = value; }
        }

        public virtual bool SurveyTaken
        {
            get { return _surveytaken; }
            set { _surveytaken = value; }
        }

        public virtual bool Active
        {
            [DebuggerStepThrough]
            get
            { return _active; }
            [DebuggerStepThrough]
            set
            { _active = value; }
        }

        public virtual string AffiliatedInstitutionCode
        {
            get { return _affiliatedinstitutioncode; }
            set { _affiliatedinstitutioncode = value; }
        }

        public virtual string AffiliatedMajorCode
        {
            get { return _affiliatedmajorcode; }
            set { _affiliatedmajorcode = value; }
        }

        public virtual string FacebookUrl
        {
            get { return _facebookurl; }
            set { _facebookurl = value; }
        }

        public virtual string TwitterUrl
        {
            get { return _twitterurl; }
            set { _twitterurl = value; }
        }

        public virtual string LinkedInUrl
        {
            get { return _linkedinurl; }
            set { _linkedinurl = value; }
        }

        public virtual UserAccounts Account
        {
            get { return _account; }
            set { _account = value; }
        }

        public virtual UserExternalAccounts ExternalAccount
        {
            get { return _externalaccount; }
            set { _externalaccount = value; }
        }

        public virtual string LatestEducationLevelCode
        {
            get { return _latesteducationlevelcode; }
            set { _latesteducationlevelcode = value; }
        }

        public virtual string LatestOrganizationExperience
        {
            get { return _latestorganizationexperience; }
            set { _latestorganizationexperience = value; }
        }

        public virtual string FlagNotes
        {
            get
            {
                return _flagnotes;
            }
            set
            {
                _flagnotes = value;
            }
        }

        public virtual string FlagStatus
        {
            get
            {
                return _flagStatus;
            }
            set
            {
                _flagStatus = value;
            }
        }

        public virtual string FlagIssuedByCode
        {
            get
            {
                return _flagissuedByCode;
            }
            set
            {
                _flagissuedByCode = value;
            }
        }

        public virtual string FlagIssuedByName
        {
            get { return _flagissuedbyname; }
            set { _flagissuedbyname = value; }
        }

        public virtual DateTime FlagIssuedDate
        {
            get
            {
                return _flagissuedDate;
            }
            set
            {
                _flagissuedDate = value;
            }
        }

        public virtual string CompanyCode
        {
            get { return _companycode; }
            set { _companycode = value; }
        }

        public virtual string CompanyName
        {
            get { return _companyname; }
            set { _companyname = value; }
        }

        public virtual int JobAppliedCount
        {
            get { return _jobappliedcount; }
            set { _jobappliedcount = value; }
        }

        public virtual string CurrentCardCityName
        {
            get { return _currentcardcityname; }
            set { _currentcardcityname = value; }
        }

        public virtual int ApplicantAge
        {
            get { return _applicantage; }
            set { _applicantage = value; }
        }

        public virtual string ApplicantLastEducationInfo
        {
            get { return _applicantlasteducationinfo; }
            set { _applicantlasteducationinfo = value; }
        }

        public virtual string ApplicantLastExperienceInfo
        {
            get { return _applicantlastexperienceinfo; }
            set { _applicantlastexperienceinfo = value; }
        }

        public virtual string LinkedInImageUrl
        {
            get { return _linkedinimageurl; }
            set { _linkedinimageurl = value; }
        }

        public virtual string RefVacancyCode
        {
            get { return _refvacancycode; }
            set { _refvacancycode = value; }
        }
        public virtual string LinkedInID
        {
            get { return _linkedinid; }
            set { _linkedinid = value; }
        }
        public virtual string FacebookID
        {
            get { return _facebookid; }
            set { _facebookid = value; }
        }
        public virtual string IsRegis
        {
            get { return _isregis; }
            set { _isregis = value; }
        }
    }
}
