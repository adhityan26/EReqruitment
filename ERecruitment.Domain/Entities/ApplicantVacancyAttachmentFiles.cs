using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantVacancyAttachmentFiles
    {
        private string _applicantCode;
        private string _vacancyCode;
        private string _attachmentCode;
        private byte[] _attachment;
        private string _filename;
        private string _filetype;

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string AttachmentCode
        {
            get
            {
                return _attachmentCode;
            }
            set
            {
                _attachmentCode = value;
            }
        }

        public virtual byte[] Attachment
        {
            
            get
            { return _attachment; }
            
            set
            { _attachment = value; }
        }

        public virtual string FileName
        {
            
            get
            { return _filename; }
            
            set
            { _filename = value; }
        }

        public virtual string FileType
        {
            
            get
            { return _filetype; }
            
            set
            { _filetype = value; }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicantVacancyAttachmentFiles;
            if (t == null) return false;
            if (ApplicantCode == t.ApplicantCode
             && VacancyCode == t.VacancyCode
             && AttachmentCode == t.AttachmentCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ApplicantCode.GetHashCode();
            hash = (hash * 397) ^ VacancyCode.GetHashCode();
            hash = (hash * 397) ^ AttachmentCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
