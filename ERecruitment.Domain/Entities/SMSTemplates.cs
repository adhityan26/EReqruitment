using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public  class SMSTemplates {
        public SMSTemplates() { }
        private int _id;
        private string _templateName;
        private string _category;
        private string _subject;
        private string _body;
        private bool _active;

        public virtual string Subject
        {
            get { return _subject; }
            set { _subject = value; }
        }

        public virtual int Id {
            get {
                return _id;
            }
            set {
                _id = value;
            }
        }
        public virtual string TemplateName {
            get {
                return _templateName;
            }
            set {
                _templateName = value;
            }
        }
        public virtual string Category {
            get {
                return _category;
            }
            set {
                _category = value;
            }
        }
        public virtual string Body {
            get {
                return _body;
            }
            set {
                _body = value;
            }
        }
        public virtual bool Active {
            get {
                return _active;
            }
            set {
                _active = value;
            }
        }
    }
}
