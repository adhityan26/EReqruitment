using System;
using System.Text;
using System.Collections.Generic;
using System.Web.Script.Serialization;


namespace ERecruitment.Domain
{

    public partial class ApplicantVacancyReferences
    {
        private string _applicantCode;
        private string _vacancyCode;
        private string _companyReferenceCode;
        private DateTime _appliedDate;
        private string _originCompany;
        private string _issuedByCode;
        private DateTime _issuedDate;
        private string _notes;
        private Applicants _applicant;
        private string _vacancyName;
        private string _originCompanyName;
        private string _referenceCompanyName;
        private string _commentatorname;
        private byte[] _commentatorphoto;

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string CompanyReferenceCode
        {
            get
            {
                return _companyReferenceCode;
            }
            set
            {
                _companyReferenceCode = value;
            }
        }

        public virtual  DateTime AppliedDate
        {
            get
            {
                return _appliedDate;
            }
            set
            {
                _appliedDate = value;
            }
        }

        public virtual string OriginCompany
        {
            get
            {
                return _originCompany;
            }
            set
            {
                _originCompany = value;
            }
        }

        public virtual string IssuedByCode
        {
            get
            {
                return _issuedByCode;
            }
            set
            {
                _issuedByCode = value;
            }
        }

        public virtual DateTime IssuedDate
        {
            get
            {
                return _issuedDate;
            }
            set
            {
                _issuedDate = value;
            }
        }

        public virtual string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        [ScriptIgnore]
        public virtual Applicants Applicant
        {
            get { return _applicant; }
            set { _applicant = value; }
        }

        public virtual string CommentatorName
        {
            get { return _commentatorname; }
            set { _commentatorname = value; }
        }

        public virtual string VacancyName
        {
            get { return _vacancyName; }
            set { _vacancyName = value; }
        }

        public virtual string OriginCompanyName
        {
            get { return _originCompanyName; }
            set { _originCompanyName = value; }
        }

        public virtual string ReferenceCompanyName
        {
            get { return _referenceCompanyName; }
            set { _referenceCompanyName = value; }
        }

        public virtual byte[] CommentatorPhoto
        {
            get { return _commentatorphoto; }
            set { _commentatorphoto = value; }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicantVacancyReferences;
            if (t == null) return false;
            if (ApplicantCode == t.ApplicantCode
             && VacancyCode == t.VacancyCode
             && CompanyReferenceCode == t.CompanyReferenceCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ApplicantCode.GetHashCode();
            hash = (hash * 397) ^ VacancyCode.GetHashCode();
            hash = (hash * 397) ^ CompanyReferenceCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
