using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class Province {
        public Province() { }
        private string _countryCode;
        private string _provinceCode;
        private string _provincename;
        private bool _active;
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public virtual string CountryCode {
            get {
                return _countryCode;
            }
            set {
                _countryCode = value;
            }
        }
        public virtual string ProvinceCode {
            get {
                return _provinceCode;
            }
            set {
                _provinceCode = value;
            }
        }
        public virtual string ProvinceName {
            get {
                return _provincename;
            }
            set {
                _provincename = value;
            }
        }
        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            Province temp = obj as Province;
            if (temp == null)
                return false;
            else
            {
                if (temp._countryCode == this._countryCode &&
                    temp._provinceCode == this._provinceCode)
                    return true;
                else
                    return false;
            }
        }
    


        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        #endregion
    }
}
