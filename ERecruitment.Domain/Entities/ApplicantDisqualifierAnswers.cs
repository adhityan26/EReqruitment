using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class ApplicantDisqualifierAnswers {
        public ApplicantDisqualifierAnswers() { }
        private int _id;
        private string _applicantCode;
        private string _vacancyCode;
        private string _disqualifierCode;
        private string _answer;
        private DisqualifierQuestions _disqualifierQuestion;

        public virtual DisqualifierQuestions DisqualifierQuestion
        {
            get { return _disqualifierQuestion; }
            set { _disqualifierQuestion = value; }
        }

        public virtual int Id {
            get {
                return _id;
            }
            set {
                _id = value;
            }
        }
        public virtual string ApplicantCode {
            get {
                return _applicantCode;
            }
            set {
                _applicantCode = value;
            }
        }
        public virtual string VacancyCode {
            get {
                return _vacancyCode;
            }
            set {
                _vacancyCode = value;
            }
        }
        public virtual string DisqualifierCode {
            get {
                return _disqualifierCode;
            }
            set {
                _disqualifierCode = value;
            }
        }
        public virtual  string Answer {
            get {
                return _answer;
            }
            set {
                _answer = value;
            }
        }
    }
}
