using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantAwards
    {
        private int _id;
        private string _applicantCode;
        private string _awardType;
        private string _awardName;
        private int _year;
        private string _description;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string AwardType
        {
            get
            {
                return _awardType;
            }
            set
            {
                _awardType = value;
            }
        }

        public virtual string AwardName
        {
            get
            {
                return _awardName;
            }
            set
            {
                _awardName = value;
            }
        }

        public virtual int Year
        {
            get
            {
                return _year;
            }
            set
            {
                _year = value;
            }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }
    }
}
