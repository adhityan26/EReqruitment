using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class Jobs
    {
        public Jobs() { }
        private string _code;
        private string _jobCode;
        private string _name;
        private string _requirements;
        private string _jobrole;
        private string _jobstage;
        private string _requirededucationlevel;
        private string _jobdescription;
        private bool _active;

        public virtual string Code
        {
            get { return _code; }
            set { _code = value; }
        }
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
        public virtual string JobRole
        {
            get { return _jobrole; }
            set { _jobrole = value; }
        }
        public virtual string JobStage
        {
            get { return _jobstage; }
            set { _jobstage = value; }
        }
        public virtual string RequiredEducationLevel
        {
            get { return _requirededucationlevel; }
            set { _requirededucationlevel = value; }
        }
        public virtual string JobCode
        {
            get
            {
                return _jobCode;
            }
            set
            {
                _jobCode = value;
            }
        }
        public virtual string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        public virtual string JobDescription
        {
            get { return _jobdescription; }
            set { _jobdescription = value; }
        }
        public virtual string Requirements
        {
            get
            {
                return _requirements;
            }
            set
            {
                _requirements = value;
            }
        }
    }
}
