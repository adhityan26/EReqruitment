using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class PageHighlightContexts
    {
        private string _pageFileName;
        private string _contextFileName;

        public virtual string PageFileName
        {
            get
            {
                return _pageFileName;
            }
            set
            {
                _pageFileName = value;
            }
        }

        public virtual string ContextFileName
        {
            get
            {
                return _contextFileName;
            }
            set
            {
                _contextFileName = value;
            }
        }
    }
}
