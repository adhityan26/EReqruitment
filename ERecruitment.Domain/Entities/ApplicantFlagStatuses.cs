using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantFlagStatuses
    {
        private string _applicantCode;
        private string _notes;
        private string _flagStatus;
        private string _issuedByCode;
        private DateTime _issuedDate;
        private string _vacancyCode;

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public virtual string FlagStatus
        {
            get
            {
                return _flagStatus;
            }
            set
            {
                _flagStatus = value;
            }
        }

        public virtual string IssuedByCode
        {
            get
            {
                return _issuedByCode;
            }
            set
            {
                _issuedByCode = value;
            }
        }

        public virtual DateTime IssuedDate
        {
            get
            {
                return _issuedDate;
            }
            set
            {
                _issuedDate = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }
    }
}
