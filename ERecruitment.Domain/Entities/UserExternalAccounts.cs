using System;
using System.Data;
using System.Collections;
using System.Diagnostics;


namespace ERecruitment.Domain
{

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class UserExternalAccounts
    {


        public UserExternalAccounts() { }
        private string _applicationname;
        private string _externalid;
        private string _internalid;


        public virtual string ApplicationName
        {
            [DebuggerStepThrough]
            get
            { return _applicationname; }
            [DebuggerStepThrough]
            set
            { _applicationname = value; }
        }

        public virtual string ExternalID
        {
            [DebuggerStepThrough]
            get
            { return _externalid; }
            [DebuggerStepThrough]
            set
            { _externalid = value; }
        }

        public virtual string InternalID
        {
            [DebuggerStepThrough]
            get
            { return _internalid; }
            [DebuggerStepThrough]
            set
            { _internalid = value; }
        }

        [DebuggerStepThrough]
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        [DebuggerStepThrough]
        public override bool Equals(object obj)
        {
            UserExternalAccounts temp = obj as UserExternalAccounts;
            if (temp == null)
                return false;
            else
            {
                if (temp._applicationname == this._applicationname && temp._externalid == this._externalid)
                    return true;
                else
                    return false;
            }
        }

    }
}
