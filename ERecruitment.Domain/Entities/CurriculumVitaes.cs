using System;
using System.Data;
using System.Collections;
using System.Diagnostics;


namespace ERecruitment.Domain
{

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class CurriculumVitaes
    {


        public CurriculumVitaes() { }
        private int _id;
        private string _applicantcode;
        private string _filename;
        private string _filetype;
        private byte[] _attachment;
        private string _notes;
        private string _insertedby;
        private DateTime _insertstamp;


        public virtual int Id
        {
            [DebuggerStepThrough]
            get
            { return _id; }
            [DebuggerStepThrough]
            set
            { _id = value; }
        }

        public virtual string ApplicantCode
        {
            [DebuggerStepThrough]
            get
            { return _applicantcode; }
            [DebuggerStepThrough]
            set
            { _applicantcode = value; }
        }

        public virtual string FileName
        {
            [DebuggerStepThrough]
            get
            { return _filename; }
            [DebuggerStepThrough]
            set
            { _filename = value; }
        }

        public virtual string FileType
        {
            [DebuggerStepThrough]
            get
            { return _filetype; }
            [DebuggerStepThrough]
            set
            { _filetype = value; }
        }

        public virtual byte[] Attachment
        {
            get { return _attachment; }
            set { _attachment = value; }
        }

        public virtual string Notes
        {
            [DebuggerStepThrough]
            get
            { return _notes; }
            [DebuggerStepThrough]
            set
            { _notes = value; }
        }

        public virtual string InsertedBy
        {
            [DebuggerStepThrough]
            get
            { return _insertedby; }
            [DebuggerStepThrough]
            set
            { _insertedby = value; }
        }

        public virtual DateTime InsertStamp
        {
            [DebuggerStepThrough]
            get
            { return _insertstamp; }
            [DebuggerStepThrough]
            set
            { _insertstamp = value; }
        }


    }
}
