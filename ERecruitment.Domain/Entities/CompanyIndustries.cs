using System;
using System.Data;
using System.Collections;
using System.Diagnostics;


namespace ERecruitment.Domain
{

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class CompanyIndustries
    {


        public CompanyIndustries() { }
        private string _companycode;
        private string _industrycode;
        private Industries _industries;

        public virtual Industries Industries
        {
            get { return _industries; }
            set { _industries = value; }
        }
        public virtual string CompanyCode
        {
            [DebuggerStepThrough]
            get
            { return _companycode; }
            [DebuggerStepThrough]
            set
            { _companycode = value; }
        }

        public virtual string IndustryCode
        {
            [DebuggerStepThrough]
            get
            { return _industrycode; }
            [DebuggerStepThrough]
            set
            { _industrycode = value; }
        }

        [DebuggerStepThrough]
        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
        [DebuggerStepThrough]
        public override bool Equals(object obj)
        {
            CompanyIndustries temp = obj as CompanyIndustries;
            if (temp == null)
                return false;
            else
            {
                if (temp._companycode == this._companycode && temp._industrycode == this._industrycode)
                    return true;
                else
                    return false;
            }
        }

    }
}
