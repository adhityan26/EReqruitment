using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class Awards {
        public Awards() { }
        private string _code;
        private string _award;
        private bool _active;
        public virtual string Code {
            get {
                return _code;
            }
            set {
                _code = value;
            }
        }
        public virtual string Award {
            get {
                return _award;
            }
            set {
                _award = value;
            }
        }
        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }
    }
}
