using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class UniversityMajors
    {
        private string _universityCode;
        private string _majorCode;
        private string _majorname;

        public virtual string UniversityCode
        {
            get
            {
                return _universityCode;
            }
            set
            {
                _universityCode = value;
            }
        }

        public virtual string MajorCode
        {
            get
            {
                return _majorCode;
            }
            set
            {
                _majorCode = value;
            }
        }

        public virtual string MajorName
        {
            get { return _majorname; }
            set { _majorname = value; }
        }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as UniversityMajors;
            if (t == null) return false;
            if (UniversityCode == t.UniversityCode
             && MajorCode == t.MajorCode)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ UniversityCode.GetHashCode();
            hash = (hash * 397) ^ MajorCode.GetHashCode();

            return hash;
        }

        #endregion
    }
}
