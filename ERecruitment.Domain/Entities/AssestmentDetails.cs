using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class AssestmentDetails
    {
        private string _assestmentCode;
        private string _questionCode;
        private int _point;

        public virtual string AssestmentCode
        {
            get
            {
                return _assestmentCode;
            }
            set
            {
                _assestmentCode = value;
            }
        }

        public virtual string QuestionCode
        {
            get
            {
                return _questionCode;
            }
            set
            {
                _questionCode = value;
            }
        }

        public virtual int Point
        {
            get
            {
                return _point;
            }
            set
            {
                _point = value;
            }
        }
        
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as AssestmentDetails;
            if (t == null) return false;
            if (AssestmentCode == t.AssestmentCode
             && QuestionCode == t.QuestionCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ AssestmentCode.GetHashCode();
            hash = (hash * 397) ^ QuestionCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
