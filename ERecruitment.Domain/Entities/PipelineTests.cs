using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class PipelineTests
    {
        public PipelineTests() { }
        private string _pipelineCode;
        private string _testCode;
        private string _testname;

        public virtual string PipelineCode
        {
            get
            {
                return _pipelineCode;
            }
            set
            {
                _pipelineCode = value;
            }
        }

        public virtual string TestCode
        {
            get
            {
                return _testCode;
            }
            set
            {
                _testCode = value;
            }
        }

        public virtual string TestName
        {
            get { return _testname; }
            set { _testname = value; }
        }



        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as PipelineTests;
            if (t == null) return false;
            if (PipelineCode == t.PipelineCode
             && TestCode == t.TestCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ PipelineCode.GetHashCode();
            hash = (hash * 397) ^ TestCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
