﻿using System;
using System.Text;
using System.Collections.Generic;

namespace ERecruitment.Domain
{
    public partial class ApplicationConfiguration
    {
        private int _code;
        private string _configName;
        private string _configValue;

        public virtual int Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string ConfigName
        {
            get
            {
                return _configName;
            }
            set
            {
                _configName = value;
            }
        }

        public virtual string ConfigValue
        {
            get
            {
                return _configValue;
            }
            set
            {
                _configValue = value;
            }
        }
    }
}
