﻿using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class VacanciesSAPJobPosition
    {
        private string _code;
        private string _vacanciesCode;
        private string _sapJobPositionId;
        private string _status;
        private string _applicantCode;


        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string VacanciesCode
        {
            get
            {
                return _vacanciesCode;
            }
            set
            {
                _vacanciesCode = value;
            }
        }

        public virtual string SAPJobPositionID
        {
            get
            {
                return _sapJobPositionId;
            }
            set
            {
                _sapJobPositionId = value;
            }
        }

        public virtual string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

    }
}
