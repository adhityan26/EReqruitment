using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.Serialization;
using SS.Web.UI;

namespace ERecruitment.Domain
{

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class Vacancies
    {


        public Vacancies() { }
        private string _code;
        private string _companycode;
        private string _categorycode;
        private string _subject;
        private string _positioncode;
        private string _positionname;
        private DateTime? _effectivedate;
        private DateTime? _expirationdate;
        private string _salarycurrencycode;
        private decimal _salaryrangebottom;
        private decimal _salaryrangetop;
        private string _genderspecification;
        private string _maritalstatusspecification;
        private int _agerangebottom;
        private int _agerangetop;
        private int _yearsofexperience;
        private string _status;
        private string _description;
        private string _location;
        private string _keywords;
        private string _typeofemployment;
        private string _hiringmanager;
        private string _recruiter;
        private string _businessjustification;
        private string _jobrole;
        private string _jobstage;
        private string _requirededucationlevel;
        private string _department;
        private string _reportingto;
        private string _teamname;
        private string _country;
        private string _organizationunit;
        private string _province;
        private string _city;
        private string _jobcode;
        private int _numberofopening;
        private DateTime _approveddate;
        private string _jobqrequirement;
        private string _jobdescription;
        private string _competencies;
        private string _disqualifierquestions;
        private int _applied;
        private DateTime? _postdate;
        private DateTime? _expiredpostdate;
        private DateTime _preferredstartdate;
        private string _recruitmenttargetgroup;
        private string _jobpostingoption;
        private string _newposition;
        private bool _isshowcompany;
        private bool _isshowsalary;
        private string _division;
        private decimal _costcentre;
        private string _advertisementstatus;
        private string _pipelinecode;
        private string _disqualifierquestion;
        private int _agefromcreationdate;
        private int _agefrompostdate;
        private int _age;
        private double _maximumpipelineorder;
        private double _latestpipelineactive;
        private string _categoryname;
        private string _cityname;
        private string _companyname;
        private List<VacancyDisqualifierQuestions> _disqualifierquestionlist = new List<VacancyDisqualifierQuestions>();
        private DateTime _insertstamp;
        private DateTime _updatestamp;
        private string _updatedby;
        private string _insertedby;
        private bool _active;
        private int _applicantcount;
        private string _creatorname;
        private bool _isnew;


        private int _rankQualificationExperienceMinimumYear;
        private string _rankQualificationMinimumEducationLevelCode;
        private string _rankQualificationEducationFieldCode;
        private float _rankQualificationEducationGpaRangeBottom;
        private float _rankQualificationEducationGpaRangeTop;
        private string _rankQualificationEducationLocationCode;
        private string _rankQualificationResidenceLocationCode;
        private decimal _rankQualificationSalaryRangeBottom;
        private decimal _rankQualificationSalaryRangeTop;
        private int _rankQualificationAgeRangeBottom;
        private int _rankQualificationAgeRangeTop;
        private int _rankQualificationExperienceMinimumYearWeight;
        private int _rankQualificationMinimumEducationLevelCodeWeight;
        private int _rankQualificationEducationFieldCodeWeight;
        private int _rankQualificationEducationGpaRangeWeight;
        private int _rankQualificationEducationLocationCodeWeight;
        private int _rankQualificationResidenceLocationCodeWeight;
        private int _rankQualificationSalaryRangeWeight;
        private int _rankQualificationAgeRangeWeight;
        private string _sapjobpositionid;
        private string _sapvacantjobpositionid;

        public virtual string DisplayName
        {
            get
            {
                if (!string.IsNullOrEmpty(_companycode))
                    return string.Concat(_positionname, " [ ", _companycode, " ] ");
                else
                    return _positionname;
            }
        }

        public virtual string PipelineCode
        {
            get { return _pipelinecode; }
            set { _pipelinecode = value; }
        }

        public virtual string DisqualifierQuestion
        {
            get { return _disqualifierquestion; }
            set { _disqualifierquestion = value; }
        }

        public virtual string AdvertisementStatus
        {
            get { return _advertisementstatus; }
            set { _advertisementstatus = value; }
        }

        public virtual decimal CostCentre
        {
            get { return _costcentre; }
            set { _costcentre = value; }
        }

        public virtual string Division
        {
            get { return _division; }
            set { _division = value; }
        }

        public virtual bool IsShowCompany
        {
            get { return _isshowcompany; }
            set { _isshowcompany = value; }
        }

        public virtual bool IsShowSalary
        {
            get { return _isshowsalary; }
            set { _isshowsalary = value; }
        }

        public virtual string RecruitmentTargetGroup
        {
            get { return _recruitmenttargetgroup; }
            set { _recruitmenttargetgroup = value; }
        }

        public virtual string JobPostingOption
        {
            get { return _jobpostingoption; }
            set { _jobpostingoption = value; }
        }

        public virtual string NewPosition
        {
            get { return _newposition; }
            set { _newposition = value; }
        }

        public virtual DateTime PreferredStartDate
        {
            get { return _preferredstartdate; }
            set { _preferredstartdate = value; }
        }

        public virtual int Applied
        {
            get { return _applied; }
            set { _applied = value; }
        }

        public virtual int NumberOfOpening
        {
            get { return _numberofopening; }
            set { _numberofopening = value; }
        }

        public virtual string JobDescription
        {
            get { return _jobdescription; }
            set { _jobdescription = value; }
        }

        public virtual DateTime ApprovedDate
        {
            get { return _approveddate; }
            set { _approveddate = value; }
        }

        public virtual string JobRequirement
        {
            get { return _jobqrequirement; }
            set { _jobqrequirement = value; }
        }

        public virtual string Competencies
        {
            get { return _competencies; }
            set { _competencies = value; }
        }

        public virtual string DisqualifierQuestions
        {
            get { return _disqualifierquestions; }
            set { _disqualifierquestions = value; }
        }

        public virtual string HiringManager
        {
            get { return _hiringmanager; }
            set { _hiringmanager = value; }
        }

        public virtual string Recruiter
        {
            get { return _recruiter; }
            set { _recruiter = value; }
        }

        public virtual string JobCode
        {
            get { return _jobcode; }
            set { _jobcode = value; }
        }

        public virtual string City
        {
            get { return _city; }
            set { _city = value; }

        }

        public virtual string Province
        {
            get { return _province; }
            set { _province = value; }
        }

        public virtual string OrganizationUnit
        {
            get { return _organizationunit; }
            set { _organizationunit = value; }
        }

        public virtual string Country
        {
            get { return _country; }
            set { _country = value; }
        }

        public virtual string TeamName
        {
            get { return _teamname; }
            set { _teamname = value; }
        }

        public virtual string ReportingTo
        {
            get { return _reportingto; }
            set { _reportingto = value; }
        }

        public virtual string Department
        {
            get { return _department; }
            set { _department = value; }
        }

        public virtual string RequiredEducationLevel
        {
            get { return _requirededucationlevel; }
            set { _requirededucationlevel = value; }
        }

        public virtual string JobStage
        {
            get { return _jobstage; }
            set { _jobstage = value; }
        }

        public virtual string JobRole
        {
            get { return _jobrole; }
            set { _jobrole = value; }
        }

        public virtual string BusinessJustification
        {
            get { return _businessjustification; }
            set { _businessjustification = value; }
        }

        public virtual string TypeOfEmployment
        {
            get { return _typeofemployment; }
            set { _typeofemployment = value; }
        }

        public virtual string Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual string Code
        {
            [DebuggerStepThrough]
            get
            { return _code; }
            [DebuggerStepThrough]
            set
            { _code = value; }
        }

        public virtual string CompanyCode
        {
            [DebuggerStepThrough]
            get
            { return _companycode; }
            [DebuggerStepThrough]
            set
            { _companycode = value; }
        }

        public virtual string PositionCode
        {
            get { return _positioncode; }
            set { _positioncode = value; }
        }

        public virtual string CategoryCode
        {
            [DebuggerStepThrough]
            get
            { return _categorycode; }
            [DebuggerStepThrough]
            set
            { _categorycode = value; }
        }

        public virtual string Subject
        {
            [DebuggerStepThrough]
            get
            { return _subject; }
            [DebuggerStepThrough]
            set
            { _subject = value; }
        }

        public virtual string PositionName
        {
            [DebuggerStepThrough]
            get
            { return _positionname; }
            [DebuggerStepThrough]
            set
            { _positionname = value; }
        }

        public virtual DateTime? EffectiveDate
        {
            [DebuggerStepThrough]
            get
            { return _effectivedate; }
            [DebuggerStepThrough]
            set
            { _effectivedate = value; }
        }

        public virtual DateTime? ExpirationDate
        {
            [DebuggerStepThrough]
            get
            { return _expirationdate; }
            [DebuggerStepThrough]
            set
            { _expirationdate = value; }
        }

        public virtual string SalaryCurrencyCode
        {
            [DebuggerStepThrough]
            get
            { return _salarycurrencycode; }
            [DebuggerStepThrough]
            set
            { _salarycurrencycode = value; }
        }

        public virtual decimal SalaryRangeBottom
        {
            [DebuggerStepThrough]
            get
            { return _salaryrangebottom; }
            [DebuggerStepThrough]
            set
            { _salaryrangebottom = value; }
        }

        public virtual decimal SalaryRangeTop
        {
            [DebuggerStepThrough]
            get
            { return _salaryrangetop; }
            [DebuggerStepThrough]
            set
            { _salaryrangetop = value; }
        }

        public virtual string GenderSpecification
        {
            [DebuggerStepThrough]
            get
            { return _genderspecification; }
            [DebuggerStepThrough]
            set
            { _genderspecification = value; }
        }

        public virtual string MaritalStatusSpecification
        {
            [DebuggerStepThrough]
            get
            { return _maritalstatusspecification; }
            [DebuggerStepThrough]
            set
            { _maritalstatusspecification = value; }
        }

        public virtual int AgeRangeBottom
        {
            [DebuggerStepThrough]
            get
            { return _agerangebottom; }
            [DebuggerStepThrough]
            set
            { _agerangebottom = value; }
        }

        public virtual int AgeRangeTop
        {
            [DebuggerStepThrough]
            get
            { return _agerangetop; }
            [DebuggerStepThrough]
            set
            { _agerangetop = value; }
        }

        public virtual int YearsOfExperience
        {
            [DebuggerStepThrough]
            get
            { return _yearsofexperience; }
            [DebuggerStepThrough]
            set
            { _yearsofexperience = value; }
        }

        public virtual string Status
        {
            [DebuggerStepThrough]
            get
            { return _status; }
            [DebuggerStepThrough]
            set
            { _status = value; }
        }

        public virtual string Keywords
        {
            get { return _keywords; }
            set { _keywords = value; }
        }

        public virtual DateTime? PostDate
        {
            get { return _postdate; }
            set { _postdate = value; }
        }

        public virtual DateTime? ExpiredPostDate
        {
            get { return _expiredpostdate; }
            set { _expiredpostdate = value; }
        }

        public virtual int AgeFromCreationDate
        {
            get { return _agefromcreationdate; }
            set { _agefromcreationdate = value; }
        }

        public virtual int AgeFromPostDate
        {
            get { return _agefrompostdate; }
            set { _agefrompostdate = value; }
        }

        public virtual int Age
        {
            get { return _age; }
            set { _age = value; }
        }

        public virtual double MaximumPipelineOrder
        {
            get { return _maximumpipelineorder; }
            set { _maximumpipelineorder = value; }
        }

        public virtual double LatestPipelineActive
        {
            get { return _latestpipelineactive; }
            set { _latestpipelineactive = value; }
        }

        public virtual double ProgressPercentage
        {
            get { return _maximumpipelineorder > 0 ? (_latestpipelineactive / _maximumpipelineorder) * (double)100 : 0; }
        }

        public virtual int RankQualificationExperienceMinimumYear
        {
            get
            {
                return _rankQualificationExperienceMinimumYear;
            }
            set
            {
                _rankQualificationExperienceMinimumYear = value;
            }
        }

        public virtual string RankQualificationMinimumEducationLevelCode
        {
            get
            {
                return _rankQualificationMinimumEducationLevelCode;
            }
            set
            {
                _rankQualificationMinimumEducationLevelCode = value;
            }
        }

        public virtual string RankQualificationEducationFieldCode
        {
            get
            {
                return _rankQualificationEducationFieldCode;
            }
            set
            {
                _rankQualificationEducationFieldCode = value;
            }
        }

        public virtual float RankQualificationEducationGpaRangeBottom
        {
            get
            {
                return _rankQualificationEducationGpaRangeBottom;
            }
            set
            {
                _rankQualificationEducationGpaRangeBottom = value;
            }
        }

        public virtual float RankQualificationEducationGpaRangeTop
        {
            get
            {
                return _rankQualificationEducationGpaRangeTop;
            }
            set
            {
                _rankQualificationEducationGpaRangeTop = value;
            }
        }

        public virtual string RankQualificationEducationLocationCode
        {
            get
            {
                return _rankQualificationEducationLocationCode;
            }
            set
            {
                _rankQualificationEducationLocationCode = value;
            }
        }

        public virtual string RankQualificationResidenceLocationCode
        {
            get
            {
                return _rankQualificationResidenceLocationCode;
            }
            set
            {
                _rankQualificationResidenceLocationCode = value;
            }
        }

        public virtual decimal RankQualificationSalaryRangeBottom
        {
            get
            {
                return _rankQualificationSalaryRangeBottom;
            }
            set
            {
                _rankQualificationSalaryRangeBottom = value;
            }
        }

        public virtual decimal RankQualificationSalaryRangeTop
        {
            get
            {
                return _rankQualificationSalaryRangeTop;
            }
            set
            {
                _rankQualificationSalaryRangeTop = value;
            }
        }

        public virtual int RankQualificationAgeRangeBottom
        {
            get
            {
                return _rankQualificationAgeRangeBottom;
            }
            set
            {
                _rankQualificationAgeRangeBottom = value;
            }
        }

        public virtual int RankQualificationAgeRangeTop
        {
            get
            {
                return _rankQualificationAgeRangeTop;
            }
            set
            {
                _rankQualificationAgeRangeTop = value;
            }
        }

        public virtual int RankQualificationExperienceMinimumYearWeight
        {
            get
            {
                return _rankQualificationExperienceMinimumYearWeight;
            }
            set
            {
                _rankQualificationExperienceMinimumYearWeight = value;
            }
        }

        public virtual int RankQualificationMinimumEducationLevelCodeWeight
        {
            get
            {
                return _rankQualificationMinimumEducationLevelCodeWeight;
            }
            set
            {
                _rankQualificationMinimumEducationLevelCodeWeight = value;
            }
        }

        public virtual int RankQualificationEducationFieldCodeWeight
        {
            get
            {
                return _rankQualificationEducationFieldCodeWeight;
            }
            set
            {
                _rankQualificationEducationFieldCodeWeight = value;
            }
        }

        public virtual int RankQualificationEducationGpaRangeWeight
        {
            get
            {
                return _rankQualificationEducationGpaRangeWeight;
            }
            set
            {
                _rankQualificationEducationGpaRangeWeight = value;
            }
        }

        public virtual int RankQualificationEducationLocationCodeWeight
        {
            get
            {
                return _rankQualificationEducationLocationCodeWeight;
            }
            set
            {
                _rankQualificationEducationLocationCodeWeight = value;
            }
        }

        public virtual int RankQualificationResidenceLocationCodeWeight
        {
            get
            {
                return _rankQualificationResidenceLocationCodeWeight;
            }
            set
            {
                _rankQualificationResidenceLocationCodeWeight = value;
            }
        }

        public virtual int RankQualificationSalaryRangeWeight
        {
            get
            {
                return _rankQualificationSalaryRangeWeight;
            }
            set
            {
                _rankQualificationSalaryRangeWeight = value;
            }
        }

        public virtual int RankQualificationAgeRangeWeight
        {
            get
            {
                return _rankQualificationAgeRangeWeight;
            }
            set
            {
                _rankQualificationAgeRangeWeight = value;
            }
        }

        public virtual DateTime InsertStamp
        {
            [DebuggerStepThrough]
            get
            { return _insertstamp; }
            [DebuggerStepThrough]
            set
            { _insertstamp = value; }
        }

        public virtual DateTime UpdateStamp
        {
            [DebuggerStepThrough]
            get
            { return _updatestamp; }
            [DebuggerStepThrough]
            set
            { _updatestamp = value; }
        }

        public virtual string UpdatedBy
        {
            [DebuggerStepThrough]
            get
            { return _updatedby; }
            [DebuggerStepThrough]
            set
            { _updatedby = value; }
        }

        public virtual string InsertedBy
        {
            [DebuggerStepThrough]
            get
            { return _insertedby; }
            [DebuggerStepThrough]
            set
            { _insertedby = value; }
        }

        public virtual bool Active
        {
            [DebuggerStepThrough]
            get
            { return _active; }
            [DebuggerStepThrough]
            set
            { _active = value; }
        }

        public virtual List<VacancyDisqualifierQuestions> DisqualifierQuestionList
        {
            get { return _disqualifierquestionlist; }
            set { _disqualifierquestionlist = value; }
        }

        public virtual string CategoryName
        {
            get { return _categoryname; }
            set { _categoryname = value; }
        }

        public virtual string CityName
        {
            get { return _cityname; }
            set { _cityname = value; }
        }

        public virtual string CompanyName
        {
            get { return _companyname; }
            set { _companyname = value; }
        }

        public virtual string PostDatePart
        {
            get { return GenericUtilities.ComposeSequenceNumber(_postdate.Value.Day, 2); }
        }

        public virtual string PostDateMonthPart
        {
            get { return _postdate.Value.ToString("MMM"); }
        }

        public virtual string SalaryInformation
        {
            get { return IsShowSalary ? Utils.DisplayMoneyAmount(this.SalaryRangeBottom) + " - " + Utils.DisplayMoneyAmount(this.SalaryRangeTop) : ""; }
        }

        public virtual int ApplicantCount
        {
            get { return _applicantcount; }
            set { _applicantcount = value; }
        }

        public virtual string CreatorName
        {
            get { return _creatorname; }
            set { _creatorname = value; }
        }

        public virtual string SAPJobPositionID
        {
            get { return _sapjobpositionid; }
            set { _sapjobpositionid = value; }
        }

        public virtual string SAPVacantJobPositionID
        {
            get { return _sapvacantjobpositionid; }
            set { _sapvacantjobpositionid = value; }
        }

        public virtual bool IsNew
        {
            get { return _isnew; }
            set { _isnew = value; }
        }
    }
}
