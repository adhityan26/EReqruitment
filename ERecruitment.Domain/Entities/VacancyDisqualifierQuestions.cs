using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class VacancyDisqualifierQuestions
    {
        private string _vacancyCode;
        private string _questionCode;
        private DisqualifierQuestions _question;

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string QuestionCode
        {
            get
            {
                return _questionCode;
            }
            set
            {
                _questionCode = value;
            }
        }

        public virtual DisqualifierQuestions Question
        {
            get { return _question; }
            set { _question = value; }
        }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as VacancyDisqualifierQuestions;
            if (t == null) return false;
            if (VacancyCode == t.VacancyCode
             && QuestionCode == t.QuestionCode)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ VacancyCode.GetHashCode();
            hash = (hash * 397) ^ QuestionCode.GetHashCode();

            return hash;
        }

        #endregion
    }
}
