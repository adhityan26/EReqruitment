using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class RoleCompanyAccessList
    {
        public RoleCompanyAccessList() { }
        private string _roleCode;
        private string _companyCode;
        private Companies _company;

        public virtual string RoleCode
        {
            get
            {
                return _roleCode;
            }
            set
            {
                _roleCode = value;
            }
        }

        public virtual string CompanyCode
        {
            get
            {
                return _companyCode;
            }
            set
            {
                _companyCode = value;
            }
        }

        public virtual Companies Company
        {
            get { return _company; }
            set { _company = value; }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as RoleCompanyAccessList;
            if (t == null) return false;
            if (RoleCode == t.RoleCode
             && CompanyCode == t.CompanyCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ RoleCode.GetHashCode();
            hash = (hash * 397) ^ CompanyCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
