using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class ApplicantVacancySchedulesSummary
    {
        public ApplicantVacancySchedulesSummary() { }

        private string _inviteTo;
        private string _vacancyCode;
        private string _jobName;
        private DateTime _date;
        private string _place;
        private string _companyCode;
        private string _applicantStatusNotification;
        private Int32 _accepted;
        private Int32 _rejected;
        private Int32 _reschedule;

        public virtual string InviteTo
        {
            get
            {
                return _inviteTo;
            }
            set
            {
                _inviteTo = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string JobName
        {
            get
            {
                return _jobName;
            }
            set
            {
                _jobName = value;
            }
        }

        public virtual DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }

        public virtual string Place
        {
            get
            {
                return _place;
            }
            set
            {
                _place = value;
            }
        }

        public virtual string CompanyCode
        {
            get
            {
                return _companyCode;
            }
            set
            {
                _companyCode = value;
            }
        }

        public virtual string ApplicantStatusNotification
        {
            get
            {
                return _applicantStatusNotification;
            }
            set
            {
                _applicantStatusNotification = value;
            }
        }

        public virtual Int32 Accepted
        {
            get
            {
                return _accepted;
            }
            set
            {
                _accepted = value;
            }
        }

        public virtual Int32 Rejected
        {
            get
            {
                return _rejected;
            }
            set
            {
                _rejected = value;
            }
        }

        public virtual Int32 Reschedule
        {
            get
            {
                return _reschedule;
            }
            set
            {
                _reschedule = value;
            }
        }
        
        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicantVacancySchedulesSummary;
            if (t == null) return false;
            if (Place == t.Place
                && InviteTo == t.InviteTo
                && Date == t.Date
                && VacancyCode == t.VacancyCode)
                return true;

            return false;
        }
        
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ InviteTo.GetHashCode();
            hash = (hash * 397) ^ Date.GetHashCode();
            hash = (hash * 397) ^ Place.GetHashCode();
            hash = (hash * 397) ^ VacancyCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
