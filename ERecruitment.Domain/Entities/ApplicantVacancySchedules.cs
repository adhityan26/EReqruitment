using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class ApplicantVacancySchedules
    {
        public ApplicantVacancySchedules() { }

        private int _id;
        private string _vacancyCode;
        private string _applicantCode;
        private string _inviteTo;
        private DateTime _date;
        private string _timestart;
        private string _timeend;
        private string _startHour;
        private string _startMinute;
        private string _endHour;
        private string _endMinute;
        private string _place;
        private string _pic;
        private string _notes;
        private string _createdby;
        private string _status;
        private DateTime _createddate;
        private string _jobname;
        private string _invitetoname;
        private string _recruiterstatusnotification;
        private string _applicantstatusnotification;
        private int _feedbackInvitationEmail;
        private string _companycode;

        public virtual int Id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string InviteTo
        {
            get
            {
                return _inviteTo;
            }
            set
            {
                _inviteTo = value;
            }
        }

        public virtual DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }

        public virtual string TimeStart
        {
            get { return _timestart; }
            set { _timestart = value; }
        }

        public virtual string TimeEnd
        {
            get { return _timeend; }
            set { _timeend = value; }
        }

        public virtual string StartHour
        {
            get
            {
                return _startHour;
            }
            set
            {
                _startHour = value;
            }
        }

        public virtual string StartMinute
        {
            get
            {
                return _startMinute;
            }
            set
            {
                _startMinute = value;
            }
        }

        public virtual string EndHour
        {
            get
            {
                return _endHour;
            }
            set
            {
                _endHour = value;
            }
        }

        public virtual string EndMinute
        {
            get
            {
                return _endMinute;
            }
            set
            {
                _endMinute = value;
            }
        }

        public virtual string Place
        {
            get
            {
                return _place;
            }
            set
            {
                _place = value;
            }
        }

        public virtual string Pic
        {
            get
            {
                return _pic;
            }
            set
            {
                _pic = value;
            }
        }

        public virtual string Notes
        {
            get
            {
                return _notes;
            }
            set
            {
                _notes = value;
            }
        }

        public virtual string Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public virtual string JobName
        {
            get { return _jobname; }
            set { _jobname = value; }
        }

        public virtual string InviteToName
        {
            get { return _invitetoname; }
            set { _invitetoname = value; }
        }

        public virtual string ApplicantStatusNotification
        {
            get { return _applicantstatusnotification; }
            set { _applicantstatusnotification = value; }
        }

        public virtual string RecruiterStatusNotification
        {
            get { return _recruiterstatusnotification; }
            set { _recruiterstatusnotification = value; }
        }

        public virtual string CreatedBy
        {
            get { return _createdby; }
            set { _createdby = value; }
        }
        public virtual DateTime CreatedDate
        {
            get { return _createddate; }
            set { _createddate = value; }
        }
        public virtual String CompanyCode
        {
            get { return _companycode; }
            set { _companycode = value; }
        }
        public virtual int FeedbackInvitationEmail
        {
            get { return _feedbackInvitationEmail; }
            set { _feedbackInvitationEmail = value; }
        }

    }
}
