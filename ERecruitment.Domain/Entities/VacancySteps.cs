using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public class VacancySteps {
        public VacancySteps() { }
        private string _code;
        private string _name;
        private string _description;
        public virtual string Code {
            get {
                return _code;
            }
            set {
                _code = value;
            }
        }
        public virtual string Name {
            get {
                return _name;
            }
            set {
                _name = value;
            }
        }
        public virtual string Description {
            get {
                return _description;
            }
            set {
                _description = value;
            }
        }
    }
}
