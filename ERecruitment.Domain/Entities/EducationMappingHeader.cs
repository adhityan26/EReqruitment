﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Web.Script.Serialization;
namespace ERecruitment.Domain
{
    public class EducationMappingHeader
    {
        public EducationMappingHeader() { }
        private string _educationMappingHeaderCode;
        private string _universitiesCode;
        private string _universitiesName;
        private string _educationLevel;
        private string _educationLevelName;
        private string _fieldOfStudyCode;
        private string _fieldOfStudyName;
        private string _majorConcateCode;
        private string _majorConcate;
        private bool _active;


        public virtual string EducationMappingHeaderCode
        {
            get { return _educationMappingHeaderCode; }
            set { _educationMappingHeaderCode = value; }
        }

        public virtual string UniversitiesCode
        {
            get { return _universitiesCode; }
            set { _universitiesCode = value; }
        }

        public virtual string UniversitiesName
        {
            get { return _universitiesName; }
            set { _universitiesName = value; }
        }

        public virtual string EducationLevel
        {
            get { return _educationLevel; }
            set { _educationLevel = value; }
        }

        public virtual string EducationLevelName
        {
            get { return _educationLevelName; }
            set { _educationLevelName = value; }
        }

        public virtual string FieldOfStudyCode
        {
            get { return _fieldOfStudyCode; }
            set { _fieldOfStudyCode = value; }
        }

        public virtual string FieldOfStudyName
        {
            get { return _fieldOfStudyName; }
            set { _fieldOfStudyName = value; }
        }

        public virtual string MajorConcateCode
        {
            get { return _majorConcateCode; }
            set { _majorConcateCode = value; }
        }

        public virtual string MajorConcate
        {
            get { return _majorConcate; }
            set { _majorConcate = value; }
        }

        public virtual bool Active
        {
            get { return _active; }
            set { _active = value; }
        }



    }
}
