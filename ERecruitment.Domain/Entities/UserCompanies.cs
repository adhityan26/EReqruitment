using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain {
    
    public  class UserCompanies
    {
        public UserCompanies() { }
        private string _userCode;
        private string _companycode;

        public virtual string UserCode {
            get {
                return _userCode;
            }
            set {
                _userCode = value;
            }
        }
        public virtual string CompanyCode {
            get {
                return _companycode;
            }
            set {
                _companycode = value;
            }
        }
        #region NHibernate Composite Key Requirements
        public  override bool Equals(object obj) {
			if (obj == null) return false;
			var t = obj as UserRoles;
			if (t == null) return false;
			if (UserCode == t.UserCode
			 && CompanyCode == t.RoleCode)
				return true;

			return false;
        }
        public override int GetHashCode() {
			int hash = GetType().GetHashCode();
			hash = (hash * 397) ^ UserCode.GetHashCode();
			hash = (hash * 397) ^ CompanyCode.GetHashCode();

			return hash;
        }
        #endregion
    }
}
