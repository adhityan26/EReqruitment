using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantVacancyTestResults
    {
        private string _applicantCode;
        private string _vacancyCode;
        private string _testCode;
        private string _result;
        private string _testname;
        private bool _isnew;


        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }

        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }

        public virtual string TestCode
        {
            get
            {
                return _testCode;
            }
            set
            {
                _testCode = value;
            }
        }

        public virtual string Result
        {
            get
            {
                return _result;
            }
            set
            {
                _result = value;
            }
        }

        public virtual bool IsNew
        {
            get { return _isnew; }
            set { _isnew = value; }
        }

        public virtual string TestName
        {
            get { return _testname; }
            set { _testname = value; }
        }

        #region NHibernate Composite Key Requirements
        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicantVacancyTestResults;
            if (t == null) return false;
            if (ApplicantCode == t.ApplicantCode
             && VacancyCode == t.VacancyCode
             && TestCode == t.TestCode)
                return true;

            return false;
        }
        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ ApplicantCode.GetHashCode();
            hash = (hash * 397) ^ VacancyCode.GetHashCode();
            hash = (hash * 397) ^ TestCode.GetHashCode();

            return hash;
        }
        #endregion
    }
}
