using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class Locations
    {
        public Locations() { }
        private string _location;
        public virtual string Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
            }
        }
    }
}
