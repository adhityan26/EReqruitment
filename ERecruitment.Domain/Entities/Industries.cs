using System;
using System.Data;
using System.Collections;
using System.Diagnostics;


namespace ERecruitment.Domain
{

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class Industries
    {


        public Industries() { }
        private string _code;
        private string _name;
        private string _description;
        private int _countcompany;
        private bool _active;


        public virtual string Code
        {
            [DebuggerStepThrough]
            get
            { return _code; }
            [DebuggerStepThrough]
            set
            { _code = value; }
        }

        public virtual string Name
        {
            [DebuggerStepThrough]
            get
            { return _name; }
            [DebuggerStepThrough]
            set
            { _name = value; }
        }

        public virtual string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public virtual int CountCompany
        {
            get { return _countcompany; }
            set { _countcompany = value; }
        }

        public virtual bool Active
        {
            [DebuggerStepThrough]
            get
            { return _active; }
            [DebuggerStepThrough]
            set
            { _active = value; }
        }


    }
}
