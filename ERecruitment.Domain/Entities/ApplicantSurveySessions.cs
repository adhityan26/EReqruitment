using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicantSurveySessions
    {
        private string _applicantCode;
        private string _vacancyCode;
        private string _companyCode;
        private DateTime _issuedDate;
        private DateTime? _completedDate;

        public virtual string ApplicantCode
        {
            get
            {
                return _applicantCode;
            }
            set
            {
                _applicantCode = value;
            }
        }
        
        public virtual string VacancyCode
        {
            get
            {
                return _vacancyCode;
            }
            set
            {
                _vacancyCode = value;
            }
        }
        public virtual string CompanyCode
        {
            get
            {
                return _companyCode;
            }
            set
            {
                _companyCode = value;
            }
        }

        public virtual DateTime IssuedDate
        {
            get
            {
                return _issuedDate;
            }
            set
            {
                _issuedDate = value;
            }
        }

        public virtual DateTime? CompletedDate
        {
            get
            {
                return _completedDate;
            }
            set
            {
                _completedDate = value;
            }
        }
    }
}
