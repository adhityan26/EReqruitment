using System;
using System.Data;
using System.Collections;
using System.Diagnostics;
using System.Collections.Generic;

namespace ERecruitment.Domain
{

    /// <summary>
    ///
    /// </summary>
    [Serializable]
    public class UserAccounts
    {


        public UserAccounts() { }
        private string _code;
        private string _name;
        private string _username;
        private string _password;
        private bool _isadmin;
        private bool _isapplicant;
        private bool _iscompany;
        private string _applicantcode;
        private string _companycode;
        private string _status;
        private bool _active;
        private bool _isrecruiter;
        private DateTime? _lastlogin;
        private string _email;
        private string _filetype;
        private string _filename;
        private byte[] _photo;
        private string _rolecode;
        private string _position;
        private List<UserRoles> _userroles;
        private string _rolecodes;
        private string _companycodes;
        private string _nik;

        public virtual string Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public virtual List<UserRoles> UserRoles
        {
            get { return _userroles; }
            set { _userroles = value; }
        }
        public virtual string FileType
        {
            get { return _filetype; }
            set { _filetype = value; }
        }
        public virtual byte[] Photo
        {
            get { return _photo; }
            set { _photo = value; }
        }
        public virtual string FileName
        {
            get { return _filename; }
            set { _filename = value; }
        }
        public virtual string Email
        {
            get { return _email; }
            set { _email = value; }
        }

        public virtual DateTime? LastLogin
        {
            get { return _lastlogin; }
            set { _lastlogin = value; }
        }

        public virtual bool IsRecruiter
        {
            get { return _isrecruiter; }
            set { _isrecruiter = value; }
        }
        public virtual string Code
        {
            [DebuggerStepThrough]
            get
            { return _code; }
            [DebuggerStepThrough]
            set
            { _code = value; }
        }

        public virtual string Name
        {
            [DebuggerStepThrough]
            get
            { return _name; }
            [DebuggerStepThrough]
            set
            { _name = value; }
        }

        public virtual string UserName
        {
            [DebuggerStepThrough]
            get
            { return _username; }
            [DebuggerStepThrough]
            set
            { _username = value; }
        }

        public virtual string Password
        {
            [DebuggerStepThrough]
            get
            { return _password; }
            [DebuggerStepThrough]
            set
            { _password = value; }
        }

        public virtual bool IsAdmin
        {
            [DebuggerStepThrough]
            get
            { return _isadmin; }
            [DebuggerStepThrough]
            set
            { _isadmin = value; }
        }

        public virtual bool IsApplicant
        {
            [DebuggerStepThrough]
            get
            { return _isapplicant; }
            [DebuggerStepThrough]
            set
            { _isapplicant = value; }
        }

        public virtual bool IsCompany
        {
            [DebuggerStepThrough]
            get
            { return _iscompany; }
            [DebuggerStepThrough]
            set
            { _iscompany = value; }
        }

        public virtual string ApplicantCode
        {
            [DebuggerStepThrough]
            get
            { return _applicantcode; }
            [DebuggerStepThrough]
            set
            { _applicantcode = value; }
        }

        public virtual string CompanyCode
        {
            [DebuggerStepThrough]
            get
            { return _companycode; }
            [DebuggerStepThrough]
            set
            { _companycode = value; }
        }

        public virtual string Status
        {
            [DebuggerStepThrough]
            get
            { return _status; }
            [DebuggerStepThrough]
            set
            { _status = value; }
        }

        public virtual string RoleCode
        {
            get { return _rolecode; }
            set { _rolecode = value; }
        }

        public virtual bool Active
        {
            [DebuggerStepThrough]
            get
            { return _active; }
            [DebuggerStepThrough]
            set
            { _active = value; }
        }

        public virtual string RoleCodes
        {
            get { return _rolecodes; }
            set { _rolecodes = value; }
        }

        public virtual string CompanyCodes
        {
            get { return _companycodes; }
            set { _companycodes = value; }
        }

        public virtual string NIK
        {
            get { return _nik; }
            set { _nik = value; }
        }

    }
}
