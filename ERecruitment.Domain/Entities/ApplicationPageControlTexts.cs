using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class ApplicationPageControlTexts
    {
        private string _pageName;
        private string _languageCode;
        private string _controlId;
        private string _text;

        public virtual string PageName
        {
            get
            {
                return _pageName;
            }
            set
            {
                _pageName = value;
            }
        }

        public virtual string LanguageCode
        {
            get
            {
                return _languageCode;
            }
            set
            {
                _languageCode = value;
            }
        }

        public virtual string ControlId
        {
            get
            {
                return _controlId;
            }
            set
            {
                _controlId = value;
            }
        }

        public virtual string Text
        {
            get
            {
                return _text;
            }
            set
            {
                _text = value;
            }
        }

        #region NHibernate Composite Key Requirements

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            var t = obj as ApplicationPageControlTexts;
            if (t == null) return false;
            if (PageName == t.PageName
             && LanguageCode == t.LanguageCode
             && ControlId == t.ControlId)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            int hash = GetType().GetHashCode();
            hash = (hash * 397) ^ PageName.GetHashCode();
            hash = (hash * 397) ^ LanguageCode.GetHashCode();
            hash = (hash * 397) ^ ControlId.GetHashCode();

            return hash;
        }

        #endregion
    }
}
