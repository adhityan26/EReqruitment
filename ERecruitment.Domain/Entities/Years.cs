using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public class Years
    {
        public Years() { }
        private int _year;
        public virtual int Year
        {
            get
            {
                return _year;
            }
            set
            {
                _year = value;
            }
        }

        public virtual string YearString
        {
            get { return Year.ToString(); }
        }
    }
}
