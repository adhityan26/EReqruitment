using System;
using System.Text;
using System.Collections.Generic;


namespace ERecruitment.Domain
{

    public partial class BatchProcesses
    {
        private string _code;
        private string _schemaCode;
        private DateTime _date;
        private string _processedBy;
        private string _status;
        private byte[] _attachment;
        private string _filename;
        private string _filetype;
        private string _schemaname;
        private string _processername;
        private string _vacancycode;


        public virtual string Code
        {
            get
            {
                return _code;
            }
            set
            {
                _code = value;
            }
        }

        public virtual string SchemaCode
        {
            get
            {
                return _schemaCode;
            }
            set
            {
                _schemaCode = value;
            }
        }

        public virtual DateTime Date
        {
            get
            {
                return _date;
            }
            set
            {
                _date = value;
            }
        }

        public virtual string ProcessedBy
        {
            get
            {
                return _processedBy;
            }
            set
            {
                _processedBy = value;
            }
        }

        public virtual string Status
        {
            get
            {
                return _status;
            }
            set
            {
                _status = value;
            }
        }

        public virtual byte[] Attachment
        {
            get { return _attachment; }
            set { _attachment = value; }
        }

        public virtual string FileName
        {
            get { return _filename; }
            set { _filename = value; }
        }

        public virtual string FileType
        {
            get { return _filetype; }
            set { _filetype = value; }
        }

        public virtual string SchemaName
        {
            get { return _schemaname; }
            set { _schemaname = value; }
        }

        public virtual string ProcesserName
        {
            get { return _processername; }
            set { _processername = value; }
        }

        public virtual string VacancyCode
        {
            get { return _vacancycode; }
            set { _vacancycode = value; }
        }
    }
}
