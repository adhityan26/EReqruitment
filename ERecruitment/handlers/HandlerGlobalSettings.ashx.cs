﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Security;
using System.Web.SessionState;
using System.Reflection;
using System.Web.Script.Serialization;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerGlobalSettings
    /// </summary>
    public class HandlerGlobalSettings : IHttpHandler, IRequiresSessionState
    {
        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (HttpContext.Current.Session["maincompany"] != null)
                    mainCompany = HttpContext.Current.Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(HttpContext.Current.Request.Url.Authority);
                    HttpContext.Current.Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            Divisions division = new Divisions();
            Country country = new Country();
            Province province = new Province();
            List<Province> provinceList = new List<Province>();
            City city = new City();
            Departments department = new Departments();
            EmploymentStatus employmentStatus = new EmploymentStatus();
            DisqualifierQuestions disqualifierQuestion = new DisqualifierQuestions();
            SurveyQuestions surveyQuestion = new SurveyQuestions();
            Industries industry = new Industries();
            VacancyCategories jobCategory = new VacancyCategories();
            Positions position = new Positions();
            Jobs jobCode = new Jobs();
            EducationLevels educationLevel = new EducationLevels();
            EducationFieldOfStudy fieldOfStudy = new EducationFieldOfStudy();
            List<EducationFieldOfStudy> fieldOfStudyList = new List<EducationFieldOfStudy>();
            EducationMajor educationMajor = new EducationMajor();
            EducationMappingHeader educationMappingHeader = new EducationMappingHeader();
            EducationMappingDetail educationMappingDetail = new EducationMappingDetail();
            List<EducationMappingDetail> educationMappingDetailList = new List<EducationMappingDetail>();
            ERecruitment.Domain.Roles role = new ERecruitment.Domain.Roles();
            UserAccounts user = new UserAccounts();
            List<SurveyQuestions> surveyQuestionList = new List<SurveyQuestions>();
            List<DisqualifierQuestions> disqualifierQuestionList = new List<DisqualifierQuestions>();
            Vacancies jobReq = new Vacancies();
            Universities university = new Universities();
            List<Universities> universityList = new List<Universities>();
            List<Positions> jobPositionList = new List<Positions>();
            List<string> dataList = new List<string>();
            ReportAutomatic autoReport = new ReportAutomatic();
            SourceReferences sourcereferences = new SourceReferences();
            string output;
            string[] keyCodes;
            string[] major = new string[] { };
            bool initated = false;
            try
            {
                switch (commandName)
                {
                    case "GetPositionByCompanyList":
                        jobPositionList = ERecruitmentManager.GetPositionListByCompanyCode(Utils.GetQueryString<string>("companyCode"));
                        foreach (Positions item in jobPositionList)
                        {
                            string data = string.Concat(item.PositionCode, ",", item.Name);
                            dataList.Add(data);
                        }
                        output = string.Join("|", dataList.ToArray());
                        context.Response.Write(output);
                        break;
                    case "UpdateUniversity":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = university.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(university, propertyValue, null);
                            if (!string.IsNullOrEmpty(university.Code) && !initated)
                            {
                                university = ERecruitmentManager.GetUniversity(university.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(university.Code))
                        {
                            university.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            university.Active = true;
                            ERecruitmentManager.SaveUniversity(university);
                        }
                        else
                            ERecruitmentManager.UpdateUniversity(university);

                        #endregion

                        break;
                    case "GetUniversity":

                        university = ERecruitmentManager.GetUniversity(Utils.GetQueryString<string>("id"));
                        if (university == null)
                            university = new Universities();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(university));
                        break;
                    case "GetUniversityByFilter":

                        universityList = ERecruitmentManager.GetUniversityListByKategori(Utils.GetQueryString<int>("id"));
                        if (universityList == null)
                            universityList = new List<Universities>();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(universityList));
                        break;
                    case "RemoveUniversity":
                        university = ERecruitmentManager.GetUniversity(Utils.GetQueryString<string>("id"));
                        university.Active = false;
                        ERecruitmentManager.UpdateUniversity(university);
                        break;
                    case "GetJobDisqualifierQuestionList":
                        jobReq = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("id"));
                        if (!string.IsNullOrEmpty(jobReq.DisqualifierQuestions))
                        {
                            disqualifierQuestionList = DisqualificationQuestionManager.GetDisqualifierQuestionList(jobReq.DisqualifierQuestions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                            foreach (DisqualifierQuestions item in disqualifierQuestionList)
                                item.AnswerOptionList = DisqualificationQuestionManager.GetAnswerOptionList(item.Code);
                            if (disqualifierQuestionList.Count > 0)
                            {
                                context.Response.ContentType = "text/plain";
                                context.Response.Write(new JavaScriptSerializer().Serialize(disqualifierQuestionList));
                            }
                            else
                            {
                                context.Response.ContentType = "text/plain";
                                context.Response.Write("");
                            }
                        }
                        else
                        {
                            context.Response.ContentType = "text/plain";
                            context.Response.Write("");
                        }
                        break;
                    case "GetSurveyQuestionList":
                        surveyQuestionList = SurveyQuestionManager.GetSurveyQuestionList(Utils.GetQueryString<string>("id").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                        foreach (SurveyQuestions item in surveyQuestionList)
                            item.AnswerOptionList = SurveyQuestionManager.GetSurveyAnswerOptionList(item.Code);

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(surveyQuestionList));
                        break;
                    case "UpdateEmploymentStatus":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = employmentStatus.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(employmentStatus, propertyValue, null);
                            if (!string.IsNullOrEmpty(employmentStatus.Code) && !initated)
                            {
                                employmentStatus = ERecruitmentManager.GetEmploymentStatus(employmentStatus.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(employmentStatus.Code))
                        {
                            employmentStatus.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            employmentStatus.Active = true;
                            ERecruitmentManager.SaveEmploymentStatus(employmentStatus);
                        }
                        else
                            ERecruitmentManager.UpdateEmploymentStatus(employmentStatus);

                        #endregion

                        break;
                    case "GetEmploymentStatus":

                        employmentStatus = ERecruitmentManager.GetEmploymentStatus(Utils.GetQueryString<string>("id"));
                        if (employmentStatus == null)
                            employmentStatus = new EmploymentStatus();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(employmentStatus));
                        break;
                    case "RemoveEmploymentStatus":
                        employmentStatus = ERecruitmentManager.GetEmploymentStatus(Utils.GetQueryString<string>("id"));
                        employmentStatus.Active = false;
                        ERecruitmentManager.UpdateEmploymentStatus(employmentStatus);
                        break;


                    case "UpdateDisqualifierQuestion":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;

                            if (name.Contains("."))
                            {
                                // nested object
                                string[] objectKeys = name.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                                string nestedPropertyName = objectKeys[1];
                                string nestedObjectPropertyName = objectKeys[2];
                                int indexNumber = Utils.ConvertString<int>(objectKeys[3]);

                                DisqualifierQuestionAnswerOptions answerOption = disqualifierQuestion.AnswerOptionList.Find(delegate (DisqualifierQuestionAnswerOptions temp) { return temp.Number == indexNumber; });
                                if (answerOption == null)
                                {
                                    answerOption = new DisqualifierQuestionAnswerOptions();
                                    answerOption.Number = indexNumber;
                                    disqualifierQuestion.AnswerOptionList.Add(answerOption);
                                }
                                // assigning value to nested object instance
                                // get property definition
                                PropertyInfo nestedPropertyInfo = answerOption.GetType().GetProperty(nestedObjectPropertyName);
                                // assign property
                                Type nestedPropertyType = nestedPropertyInfo.PropertyType;
                                if (nestedPropertyType == typeof(DateTime) || nestedPropertyType == typeof(DateTime?))
                                    propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                                else
                                    propertyValue = Convert.ChangeType(propertyValue, nestedPropertyType);

                                nestedPropertyInfo.SetValue(answerOption, propertyValue, null);
                            }
                            else
                            {
                                // get property definition
                                PropertyInfo propertyInfo = disqualifierQuestion.GetType().GetProperty(name);
                                // assign property
                                Type propertyType = propertyInfo.PropertyType;
                                if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                    propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                                else
                                    propertyValue = Convert.ChangeType(propertyValue, propertyType);

                                propertyInfo.SetValue(disqualifierQuestion, propertyValue, null);
                                if (!string.IsNullOrEmpty(disqualifierQuestion.Code) && !initated)
                                {
                                    disqualifierQuestion = DisqualificationQuestionManager.GetDisqualificationQuestion(disqualifierQuestion.Code);
                                    initated = true;
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(disqualifierQuestion.Code))
                        {
                            disqualifierQuestion.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            disqualifierQuestion.Active = true;
                            disqualifierQuestion.Type = "Parent";
                            DisqualificationQuestionManager.SaveDisqualificationQuestion(disqualifierQuestion);
                        }
                        else
                            DisqualificationQuestionManager.UpdateDisqualificationQuestion(disqualifierQuestion);

                        #endregion

                        break;
                    case "GetDisqualifierQuestion":
                        disqualifierQuestion = DisqualificationQuestionManager.GetDisqualificationQuestion(Utils.GetQueryString<string>("id"));
                        if (disqualifierQuestion == null)
                            disqualifierQuestion = new DisqualifierQuestions();
                        else
                        {
                            disqualifierQuestion.AnswerOptionList = DisqualificationQuestionManager.GetAnswerOptionList(disqualifierQuestion.Code);
                        }
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(disqualifierQuestion));
                        break;
                    case "RemoveDisqualifierQuestion":
                        keyCodes = Utils.GetQueryString<string>("id").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < keyCodes.Length; i++)
                        {

                            disqualifierQuestion = DisqualificationQuestionManager.GetDisqualificationQuestion(keyCodes[i]);
                            disqualifierQuestion.Active = false;
                            DisqualificationQuestionManager.UpdateDisqualificationQuestion(disqualifierQuestion);
                        }
                        break;

                    case "UpdateSurveyQuestion":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;

                            if (name.Contains("."))
                            {
                                // nested object
                                string[] objectKeys = name.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                                string nestedPropertyName = objectKeys[1];
                                string nestedObjectPropertyName = objectKeys[2];
                                int indexNumber = Utils.ConvertString<int>(objectKeys[3]);

                                SurveyQuestionAnswerOptions answerOption = surveyQuestion.AnswerOptionList.Find(delegate (SurveyQuestionAnswerOptions temp) { return temp.Number == indexNumber; });
                                if (answerOption == null)
                                {
                                    answerOption = new SurveyQuestionAnswerOptions();
                                    answerOption.Number = indexNumber;
                                    surveyQuestion.AnswerOptionList.Add(answerOption);
                                }
                                // assigning value to nested object instance
                                // get property definition
                                PropertyInfo nestedPropertyInfo = answerOption.GetType().GetProperty(nestedObjectPropertyName);
                                // assign property
                                Type nestedPropertyType = nestedPropertyInfo.PropertyType;
                                if (nestedPropertyType == typeof(DateTime) || nestedPropertyType == typeof(DateTime?))
                                    propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                                else
                                    propertyValue = Convert.ChangeType(propertyValue, nestedPropertyType);

                                nestedPropertyInfo.SetValue(answerOption, propertyValue, null);
                            }
                            else
                            {
                                // get property definition
                                PropertyInfo propertyInfo = surveyQuestion.GetType().GetProperty(name);
                                // assign property
                                Type propertyType = propertyInfo.PropertyType;
                                if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                    propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                                else
                                    propertyValue = Convert.ChangeType(propertyValue, propertyType);

                                propertyInfo.SetValue(surveyQuestion, propertyValue, null);
                                if (!string.IsNullOrEmpty(surveyQuestion.Code) && !initated)
                                {
                                    surveyQuestion = SurveyQuestionManager.GetSurveyQuestion(surveyQuestion.Code);
                                    initated = true;
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(surveyQuestion.Code))
                        {
                            surveyQuestion.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            surveyQuestion.Active = true;
                            SurveyQuestionManager.SaveSurveyQuestion(surveyQuestion);
                        }
                        else
                            SurveyQuestionManager.UpdateSurveyQuestion(surveyQuestion);

                        #endregion

                        break;
                    case "GetSurveyQuestion":
                        surveyQuestion = SurveyQuestionManager.GetSurveyQuestion(Utils.GetQueryString<string>("id"));
                        if (surveyQuestion == null)
                            surveyQuestion = new SurveyQuestions();
                        else
                        {
                            surveyQuestion.AnswerOptionList = SurveyQuestionManager.GetSurveyAnswerOptionList(surveyQuestion.Code);
                        }
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(surveyQuestion));
                        break;
                    case "RemoveSurveyQuestion":
                        keyCodes = Utils.GetQueryString<string>("id").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < keyCodes.Length; i++)
                        {

                            surveyQuestion = SurveyQuestionManager.GetSurveyQuestion(keyCodes[i]);
                            surveyQuestion.Active = false;
                            SurveyQuestionManager.UpdateSurveyQuestion(surveyQuestion);
                        }
                        break;

                    case "UpdateDivision":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = division.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(division, propertyValue, null);
                            if (!string.IsNullOrEmpty(division.Code) && !initated)
                            {
                                division = ERecruitmentManager.GetDivision(division.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(division.Code))
                        {
                            division.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            division.Active = true;
                            ERecruitmentManager.SaveDivision(division);
                        }
                        else
                            ERecruitmentManager.UpdateDivision(division);

                        #endregion

                        break;
                    case "GetDivision":

                        division = ERecruitmentManager.GetDivision(Utils.GetQueryString<string>("id"));
                        if (division == null)
                            division = new Divisions();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(division));
                        break;
                    case "RemoveDivision":
                        division = ERecruitmentManager.GetDivision(Utils.GetQueryString<string>("id"));
                        division.Active = false;
                        ERecruitmentManager.UpdateDivision(division);
                        break;

                    case "UpdateDepartment":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = department.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(department, propertyValue, null);
                            if (!string.IsNullOrEmpty(department.Code) && !initated)
                            {
                                department = ERecruitmentManager.GetDepartment(department.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(department.Code))
                        {
                            department.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            department.Active = true;
                            ERecruitmentManager.SaveDepartment(department);
                        }
                        else
                            ERecruitmentManager.UpdateDepartment(department);

                        #endregion

                        break;
                    case "GetDepartment":
                        department = ERecruitmentManager.GetDepartment(Utils.GetQueryString<string>("id"));
                        if (department == null)
                            department = new Departments();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(department));
                        break;
                    case "RemoveDepartment":
                        department = ERecruitmentManager.GetDepartment(Utils.GetQueryString<string>("id"));
                        department.Active = false;
                        ERecruitmentManager.UpdateDepartment(department);
                        break;

                    case "UpdateCountry":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = country.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(country, propertyValue, null);
                            if (!string.IsNullOrEmpty(country.Code) && !initated)
                            {
                                country = ERecruitmentManager.GetCountry(country.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(country.Code))
                        {
                            country.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            country.Active = true;
                            ERecruitmentManager.SaveCountry(country);
                        }
                        else
                            ERecruitmentManager.UpdateCountry(country);

                        #endregion

                        break;
                    case "GetCountry":

                        country = ERecruitmentManager.GetCountry(Utils.GetQueryString<string>("id"));
                        if (country == null)
                            country = new Country();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(country));
                        break;
                    case "RemoveCountry":
                        country = ERecruitmentManager.GetCountry(Utils.GetQueryString<string>("id"));
                        country.Active = false;
                        ERecruitmentManager.UpdateCountry(country);
                        break;

                    case "UpdateProvince":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = province.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(province, propertyValue, null);
                            if (!string.IsNullOrEmpty(province.ProvinceCode) && !initated)
                            {
                                province = ERecruitmentManager.GetProvince(province.ProvinceCode);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(province.ProvinceCode))
                        {
                            province.ProvinceCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            province.Active = true;
                            ERecruitmentManager.SaveProvince(province);
                        }
                        else
                            ERecruitmentManager.UpdateProvince(province);

                        #endregion

                        break;
                    case "GetProvince":

                        province = ERecruitmentManager.GetProvince(Utils.GetQueryString<string>("id"));
                        if (province == null)
                            province = new Province();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(province));
                        break;
                    case "GetProvinceList":

                        provinceList = ERecruitmentManager.GetProvinceListByCountry(Utils.GetQueryString<string>("countryCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(provinceList));
                        break;
                    case "RemoveProvince":
                        province = ERecruitmentManager.GetProvince(Utils.GetQueryString<string>("id"));
                        province.Active = false;
                        ERecruitmentManager.UpdateProvince(province);
                        break;
                    case "UpdateCity":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = city.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(city, propertyValue, null);
                            if (!string.IsNullOrEmpty(city.CityCode) && !initated)
                            {
                                city = ERecruitmentManager.GetCityByCode(city.CityCode);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(city.CityCode))
                        {
                            city.CityCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            city.Active = true;
                            ERecruitmentManager.SaveCity(city);
                        }
                        else
                            ERecruitmentManager.UpdateCity(city);

                        #endregion

                        break;
                    case "GetCity":

                        city = ERecruitmentManager.GetCityByCode(Utils.GetQueryString<string>("id"));
                        if (city == null)
                            city = new City();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(city));
                        break;
                    case "RemoveCity":
                        city = ERecruitmentManager.GetCityByCode(Utils.GetQueryString<string>("id"));
                        city.Active = false;
                        ERecruitmentManager.UpdateCity(city);
                        break;

                    case "UpdateJobCategory":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = jobCategory.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(jobCategory, propertyValue, null);
                            if (!string.IsNullOrEmpty(jobCategory.Code) && !initated)
                            {
                                jobCategory = ERecruitmentManager.GetJobCategory(jobCategory.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(jobCategory.Code))
                        {
                            jobCategory.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            jobCategory.Active = true;
                            ERecruitmentManager.SaveJobCategory(jobCategory);
                        }
                        else
                            ERecruitmentManager.UpdateJobCategory(jobCategory);

                        #endregion

                        break;
                    case "GetJobCategory":

                        jobCategory = ERecruitmentManager.GetJobCategory(Utils.GetQueryString<string>("id"));
                        if (jobCategory == null)
                            jobCategory = new VacancyCategories();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(jobCategory));
                        break;
                    case "RemoveJobCategory":
                        jobCategory = ERecruitmentManager.GetJobCategory(Utils.GetQueryString<string>("id"));
                        jobCategory.Active = false;
                        ERecruitmentManager.UpdateJobCategory(jobCategory);
                        break;
                    case "UpdateIndustry":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = industry.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(industry, propertyValue, null);
                            if (!string.IsNullOrEmpty(industry.Code) && !initated)
                            {
                                industry = ERecruitmentManager.GetIndustry(industry.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(industry.Code))
                        {
                            industry.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            industry.Active = true;
                            ERecruitmentManager.SaveIndustry(industry);
                        }
                        else
                            ERecruitmentManager.UpdateIndustry(industry);

                        #endregion

                        break;
                    case "GetIndustry":

                        industry = ERecruitmentManager.GetIndustry(Utils.GetQueryString<string>("id"));
                        if (industry == null)
                            industry = new Industries();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(industry));
                        break;
                    case "RemoveIndustry":
                        industry = ERecruitmentManager.GetIndustry(Utils.GetQueryString<string>("id"));
                        industry.Active = false;
                        ERecruitmentManager.UpdateIndustry(industry);
                        break;
                    case "UpdatePosition":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = position.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(position, propertyValue, null);
                            if (!string.IsNullOrEmpty(position.PositionCode) && !initated)
                            {
                                position = ERecruitmentManager.GetPosition(position.PositionCode);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(position.PositionCode))
                        {
                            position.PositionCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            position.Active = true;
                            ERecruitmentManager.SavePosition(position);
                        }
                        else
                            ERecruitmentManager.UpdatePosition(position);

                        #endregion

                        break;
                    case "GetPosition":

                        position = ERecruitmentManager.GetPosition(Utils.GetQueryString<string>("id"));
                        if (position == null)
                            position = new Positions();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(position));
                        break;
                    case "RemovePosition":
                        position = ERecruitmentManager.GetPosition(Utils.GetQueryString<string>("id"));
                        position.Active = false;
                        ERecruitmentManager.UpdatePosition(position);
                        break;
                    case "UpdateJobCode":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = jobCode.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(jobCode, propertyValue, null);
                            if (!string.IsNullOrEmpty(jobCode.Code) && !initated)
                            {
                                jobCode = ERecruitmentManager.GetJob(jobCode.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(jobCode.Code))
                        {
                            jobCode.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            jobCode.Active = true;
                            ERecruitmentManager.SaveJob(jobCode);
                        }
                        else
                            ERecruitmentManager.UpdateJob(jobCode);

                        #endregion

                        break;
                    case "GetJobCode":

                        jobCode = ERecruitmentManager.GetJob(Utils.GetQueryString<string>("id"));
                        if (jobCode == null)
                            jobCode = new Jobs();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(jobCode));
                        break;
                    case "RemoveJobCode":
                        jobCode = ERecruitmentManager.GetJob(Utils.GetQueryString<string>("id"));
                        jobCode.Active = false;
                        ERecruitmentManager.UpdateJob(jobCode);
                        break;
                    case "SaveNewEducationLevel":
                        #region filling model with data
                        //string nameLevel = "";
                        //foreach (string name in context.Request.Form.Keys)
                        //{
                        //    object propertyValue = context.Request.Form[name];
                        //    if (string.IsNullOrEmpty(propertyValue.ToString()))
                        //        continue;
                        //    // get property 

                        //    if (name != "EducationMajorCode[]")
                        //    {
                        //        PropertyInfo propertyInfo = educationLevel.GetType().GetProperty(name);
                        //        // assign property
                        //        Type propertyType = propertyInfo.PropertyType;
                        //        if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                        //            propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                        //        else
                        //            propertyValue = Convert.ChangeType(propertyValue, propertyType);
                                
                        //        propertyInfo.SetValue(educationLevel, propertyValue, null);
                        //        nameLevel = educationLevel.Name;
                        //    }
                        //    else
                        //    {
                        //        major = propertyValue.ToString().Split(',');
                        //    }
                        //}

    
                        //for (int a = 0; a < major.Length; a = a + 1)
                        //{
                        //    if (string.IsNullOrEmpty(educationLevel.EducationLevel))
                        //    {
                        //        educationLevel.Name = nameLevel;
                        //        educationLevel.EducationMajorCode = major[a];
                        //        educationLevel.EducationLevel = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                        //        educationLevel.Active = true;
                        //        ERecruitmentManager.SaveEducationLevel(educationLevel);
                        //        educationLevel = new EducationLevels();
                        //    }
                        //}

                        #endregion
                        break;
                    case "UpdateEducationLevel":
                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = educationLevel.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(educationLevel, propertyValue, null);
                            if (!string.IsNullOrEmpty(educationLevel.EducationLevel) && !initated)
                            {
                                educationLevel = ERecruitmentManager.GetEducationLevel(educationLevel.EducationLevel);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(educationLevel.EducationLevel))
                        {
                            educationLevel.EducationLevel = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            educationLevel.Active = true;
                            ERecruitmentManager.SaveEducationLevel(educationLevel);
                        }
                        else
                            ERecruitmentManager.UpdateEducationLevel(educationLevel);

                        #endregion
                        break;
                    case "GetEducationLevel":

                        educationLevel = ERecruitmentManager.GetEducationLevel(Utils.GetQueryString<string>("id"));
                        if (educationLevel == null)
                            educationLevel = new EducationLevels();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(educationLevel));
                        break;
                    case "RemoveEducationLevel":
                        educationLevel = ERecruitmentManager.GetEducationLevel(Utils.GetQueryString<string>("id"));
                        educationLevel.Active = false;
                        ERecruitmentManager.UpdateEducationLevel(educationLevel);
                        break;
                    case "UpdateFieldOfStudy":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = fieldOfStudy.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(fieldOfStudy, propertyValue, null);
                            if (!string.IsNullOrEmpty(fieldOfStudy.Code) && !initated)
                            {
                                fieldOfStudy = ERecruitmentManager.GetFieldOfStudy(fieldOfStudy.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(fieldOfStudy.Code))
                        {
                            fieldOfStudy.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            fieldOfStudy.Active = true;
                            ERecruitmentManager.SaveFieldOfStudy(fieldOfStudy);
                        }
                        else
                            ERecruitmentManager.UpdateFieldOfStudy(fieldOfStudy);

                        #endregion

                        break;
                    case "GetFieldOfStudy":

                        fieldOfStudy = ERecruitmentManager.GetFieldOfStudy(Utils.GetQueryString<string>("id"));
                        if (fieldOfStudy == null)
                            fieldOfStudy = new EducationFieldOfStudy();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(fieldOfStudy));
                        break;
                    case "GetFieldOfStudyByFilter":

                        fieldOfStudyList = ERecruitmentManager.GetFieldOfStudyListByKategori(Utils.GetQueryString<int>("id"));
                        if (fieldOfStudyList == null)
                            fieldOfStudyList = new List<EducationFieldOfStudy>();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(fieldOfStudyList));
                        break;
                    case "RemoveFieldOfStudy":
                        fieldOfStudy = ERecruitmentManager.GetFieldOfStudy(Utils.GetQueryString<string>("id"));
                        fieldOfStudy.Active = false;
                        ERecruitmentManager.UpdateFieldOfStudy(fieldOfStudy);
                        break;
                    case "UpdateEducationMajor":

                        #region filling model with data
                        string eduTextOld = "";
                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = educationMajor.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(educationMajor, propertyValue, null);
                            if (!string.IsNullOrEmpty(educationMajor.Code) && !initated)
                            {
                                educationMajor = ERecruitmentManager.GetEducationMajor(educationMajor.Code);
                                eduTextOld = educationMajor.Major;
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(educationMajor.Code))
                        {
                            educationMajor.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            educationMajor.Active = true;
                            ERecruitmentManager.SaveEducationMajor(educationMajor);
                        }
                        else
                        { 
                            ERecruitmentManager.UpdateEducationMajor(educationMajor);
                            educationMappingDetailList = ERecruitmentManager.GetEducationMappingDetailByMajorCode(educationMajor.Code);
                            for (int x = 0; x < educationMappingDetailList.Count; x += 1)
                            {
                                educationMappingHeader = ERecruitmentManager.GetEducationMappingHeader(educationMappingDetailList[x].EducationMappingHeaderCode);
                                educationMappingHeader.MajorConcate = educationMappingHeader.MajorConcate.Replace(eduTextOld, educationMajor.Major);
                                ERecruitmentManager.UpdateEducationMappingHeader(educationMappingHeader);
                            }
                        }

                        #endregion

                        break;
                    case "GetEducationMajor":

                        educationMajor = ERecruitmentManager.GetEducationMajor(Utils.GetQueryString<string>("id"));
                        if (educationMajor == null)
                            educationMajor = new EducationMajor();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(educationMajor));
                        break;
                    case "RemoveEducationMajor":
                        educationMajor = ERecruitmentManager.GetEducationMajor(Utils.GetQueryString<string>("id"));
                        educationMajor.Active = false;
                        ERecruitmentManager.UpdateEducationMajor(educationMajor);
                        break;
                    case "GetEducationMappingHeader":

                        educationMappingHeader = ERecruitmentManager.GetEducationMappingHeader(Utils.GetQueryString<string>("id"));
                        if (educationMappingHeader == null)
                            educationMappingHeader = new EducationMappingHeader();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(educationMappingHeader));
                        break;
                    case "UpdateEducationMappingHeader":
                        #region filling model with data
                       
                        foreach (string name in context.Request.Form.Keys)
                        {
                          
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()) && name != "MajorConcateCode" && name != "MajorConcate")
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = educationMappingHeader.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            if (name != "MajorConcateCode[]") { 
                            propertyInfo.SetValue(educationMappingHeader, propertyValue, null);
                            }
                            if (!string.IsNullOrEmpty(educationMappingHeader.EducationMappingHeaderCode) && !initated)
                            {
                                educationMappingHeader = ERecruitmentManager.GetEducationMappingHeader(educationMappingHeader.EducationMappingHeaderCode);
                                initated = true;
                            }
                        }

                        bool existsHeader = false;
                        existsHeader = ERecruitmentManager.CheckIfExistEducationMappingHeader(educationMappingHeader.EducationMappingHeaderCode, educationMappingHeader.UniversitiesCode, educationMappingHeader.EducationLevel, educationMappingHeader.FieldOfStudyCode);

                        if (!existsHeader)
                        {
                            if (string.IsNullOrEmpty(educationMappingHeader.EducationMappingHeaderCode))
                            {

                                educationMappingHeader.EducationMappingHeaderCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                                educationMappingHeader.Active = true;
                                ERecruitmentManager.SaveEducationMappingHeader(educationMappingHeader);

                                if (!string.IsNullOrEmpty(educationMappingHeader.MajorConcateCode.ToString()))
                                {
                                    major = educationMappingHeader.MajorConcateCode.ToString().Split(',');

                                    for (int a = 0; a < major.Length; a = a + 1)
                                    {
                                        educationMappingDetail.EducationMappingDetailCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                                        educationMappingDetail.EducationMappingHeaderCode = educationMappingHeader.EducationMappingHeaderCode;
                                        educationMappingDetail.MajorCode = major[a];
                                        educationMappingDetail.Active = true;
                                        ERecruitmentManager.SaveEducationMappingDetail(educationMappingDetail);
                                        educationMappingDetail = new EducationMappingDetail();
                                    }
                                }

                            }
                            else
                            {
                                bool exist = false;
                                educationMappingDetailList = ERecruitmentManager.GetEducationMappingDetailByHeaderCode(educationMappingHeader.EducationMappingHeaderCode);
                                if (!string.IsNullOrEmpty(educationMappingHeader.MajorConcateCode.ToString()))
                                {
                                    major = educationMappingHeader.MajorConcateCode.ToString().Split(',');

                                    //Insert New if not exist
                                    for (int a = 0; a < major.Length; a = a + 1)
                                    {
                                        for (int b = 0; b < educationMappingDetailList.Count; b = b + 1)
                                        {
                                            if (major[a] == educationMappingDetailList[b].MajorCode)
                                            {
                                                exist = true;
                                                b = educationMappingDetailList.Count;
                                            }
                                        }

                                        if (!exist)
                                        {
                                            educationMappingDetail.EducationMappingDetailCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                                            educationMappingDetail.EducationMappingHeaderCode = educationMappingHeader.EducationMappingHeaderCode;
                                            educationMappingDetail.MajorCode = major[a];
                                            educationMappingDetail.Active = true;
                                            ERecruitmentManager.SaveEducationMappingDetail(educationMappingDetail);
                                            educationMappingDetail = new EducationMappingDetail();
                                        }
                                        exist = false;
                                    }

                                    //Deactive If not exist
                                    for (int a = 0; a < educationMappingDetailList.Count; a = a + 1)
                                    {
                                        for (int b = 0; b < major.Length; b = b + 1)
                                        {
                                            if (major[b] == educationMappingDetailList[a].MajorCode)
                                            {
                                                exist = true;
                                                b = major.Length;
                                            }
                                        }

                                        if (!exist)
                                        {
                                            educationMappingDetailList[a].Active = false;
                                            ERecruitmentManager.UpdateEducationMappingDetail(educationMappingDetailList[a]);
                                            educationMappingDetail = new EducationMappingDetail();
                                        }
                                        exist = false;
                                    }

                                }
                                else
                                {
                                    for (int a = 0; a < educationMappingDetailList.Count; a = a + 1)
                                    {

                                        educationMappingDetailList[a].Active = false;
                                        ERecruitmentManager.UpdateEducationMappingDetail(educationMappingDetailList[a]);
                                        educationMappingDetail = new EducationMappingDetail();
                                    }
                                }
                                ERecruitmentManager.UpdateEducationMappingHeader(educationMappingHeader);
                            }
                        }
                        else
                        {
                            throw new ApplicationException("This Mapping Institution, Education Level and Field Of Study Is Exists");
                        }
                        #endregion
                        break;
                    case "RemoveEducationMapping":
                        educationMappingHeader = ERecruitmentManager.GetEducationMappingHeader(Utils.GetQueryString<string>("id"));
                        educationMappingHeader.Active = false;
                        ERecruitmentManager.UpdateEducationMappingHeader(educationMappingHeader);

                        educationMappingDetailList = ERecruitmentManager.GetEducationMappingDetailByHeaderCode(educationMappingHeader.EducationMappingHeaderCode);
                        for (int i = 0; i < educationMappingDetailList.Count; i = i + 1)
                        {
                            educationMappingDetailList[i].Active = false;
                            ERecruitmentManager.UpdateEducationMappingDetail(educationMappingDetailList[i]);
                        }
                        break;
                    case "UpdateRole":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = role.GetType().GetProperty(name.Replace("[]", ""));
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(role, propertyValue, null);
                            if (!string.IsNullOrEmpty(role.RoleCode) && !initated)
                            {
                                role = ERecruitmentManager.GetRole(role.RoleCode);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(role.RoleCode))
                        {
                            role.RoleCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            role.Active = true;
                            ERecruitmentManager.SaveRole(role);
                        }
                        else
                            ERecruitmentManager.UpdateRole(role);

                        #endregion

                        break;
                    case "GetRole":

                        role = ERecruitmentManager.GetRole(Utils.GetQueryString<string>("id"));
                        if (role == null)
                            role = new ERecruitment.Domain.Roles();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(role));
                        break;
                    case "RemoveRole":
                        role = ERecruitmentManager.GetRole(Utils.GetQueryString<string>("id"));
                        role.Active = false;
                        ERecruitmentManager.UpdateRole(role);
                        break;
                    case "UpdateUser":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {

                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = user.GetType().GetProperty(name.Replace("[]", string.Empty));
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(user, propertyValue, null);
                            if (!string.IsNullOrEmpty(user.Code) && !initated)
                            {
                                user = AuthenticationManager.GetUserAccount(user.Code);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(user.Code))
                        {
                            user.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            user.Active = true;
                            user.CompanyCode = MainCompany.Code;
                            AuthenticationManager.SaveUserAccount(user);
                        }
                        else
                        {
                            user.CompanyCode = MainCompany.Code;
                            AuthenticationManager.UpdateUserAccount(user);
                        }


                        #endregion

                        break;
                    case "GetUser":

                        user = AuthenticationManager.GetUserAccount(Utils.GetQueryString<string>("id"));
                        if (user == null)
                            user = new UserAccounts();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(user));
                        break;
                    case "RemoveUser":
                        user = AuthenticationManager.GetUserAccount(Utils.GetQueryString<string>("id"));
                        user.Active = false;
                        AuthenticationManager.UpdateUserAccount(user);
                        break;
                    case "GetAutoReport":
                        List<ReportAutomatic> autoReportList = ERecruitmentManager.GetReportAutoByCode(Utils.GetQueryString<string>("id"));
                        if (autoReportList != null && autoReportList.Count > 0) {
                            autoReport = autoReportList[0];
                            if (autoReport.StartDate != null && !string.IsNullOrEmpty(autoReport.StartDate)) {
                                DateTime dt = Convert.ToDateTime(autoReport.StartDate);
                                autoReport.StartDate = dt.ToString("MM/dd/yyyy");
                            }

                            if (autoReport.EndDate != null && !string.IsNullOrEmpty(autoReport.EndDate))
                            {
                                DateTime dt = Convert.ToDateTime(autoReport.EndDate);
                                autoReport.EndDate = dt.ToString("MM/dd/yyyy");
                            }
                        }

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(autoReport));
                        break;
                    case "RemoveAutoReport":
                        String id = Utils.GetQueryString<string>("id");
                        ReportAutomatic rm = new ReportAutomatic();
                        rm.Code = id;
                        ERecruitmentManager.RemoveReportAuto(rm);
                        break;
                    case "UpdateAutoReport":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = autoReport.GetType().GetProperty(name.Replace("[]", ""));
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(autoReport, propertyValue, null);
                            if (!string.IsNullOrEmpty(autoReport.Code) && !initated)
                            {
                                //role = ERecruitmentManager.GetRole(role.RoleCode);
                                initated = true;
                            }
                        }

                        if (!string.IsNullOrEmpty(autoReport.Email)) {
                            /*string[] email = autoReport.Email.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            if (email != null && email.Length > 0){
                                foreach (String s in email) {
                                    if (!string.IsNullOrEmpty(s)) {
                                        if (!IsValidEmail(s)) {
                                            throw new ApplicationException("Invalid Email Address");
                                        }
                                    }
                                }
                            }*/
                            
                            if (!string.IsNullOrEmpty(autoReport.StartDate) || !string.IsNullOrEmpty(autoReport.EndDate))
                            {
                                if (string.IsNullOrEmpty(autoReport.StartDate))
                                {
                                    throw new ApplicationException("Start Date should not empty");
                                }
                                else if (string.IsNullOrEmpty(autoReport.EndDate))
                                {
                                    throw new ApplicationException("End Date should not empty");
                                }
                                else {
                                    DateTime sdt = Convert.ToDateTime(autoReport.StartDate);
                                    DateTime edt = Convert.ToDateTime(autoReport.EndDate);

                                    if (edt < sdt)
                                    {
                                        throw new ApplicationException("Invalid Date Range");
                                    }
                                }
                            }
                        }

                        if (string.IsNullOrEmpty(autoReport.Code))
                        {
                            ERecruitmentManager.SaveReportAuto(autoReport);
                        }
                        else
                        {
                            ERecruitmentManager.UpdateReportAuto(autoReport);
                        }
                            

                        #endregion

                        break;
                    case "GetSourceReferences":
                        List<SourceReferences> sourceReferenceList = ERecruitmentManager.GetSourceReferences(Utils.GetQueryString<string>("Code"));
                        if (sourceReferenceList != null && sourceReferenceList.Count > 0)
                        {
                            sourcereferences = sourceReferenceList[0];
                        }
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(sourcereferences));
                        break;
                    case "RemoveSourceReferences":
                        String cd = Utils.GetQueryString<string>("Code");
                        SourceReferences sr = new SourceReferences();
                        sr.Code = cd;
                        ERecruitmentManager.RemoveSourceReferences(sr);
                        break;
                    case "UpdateSourceReference":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = sourcereferences.GetType().GetProperty(name.Replace("[]", ""));
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(sourcereferences, propertyValue, null);
                            if (!string.IsNullOrEmpty(sourcereferences.Code) && !initated)
                            {
                                //role = ERecruitmentManager.GetRole(role.RoleCode);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(sourcereferences.Code))
                        {
                            ERecruitmentManager.SaveSourceReferences(sourcereferences);
                        }
                        else
                        {
                            ERecruitmentManager.UpdateSourceReferences(sourcereferences);
                        }


                        #endregion

                        break;
                }
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 666;
                context.Response.ContentType = "text/plain";
                context.Response.Write(Utils.ExtractInnerExceptionMessage(e));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}