﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Security;
using System.Web.SessionState;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerDashboard
    /// </summary>
    public class HandlerDashboard : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            List<string> data = new List<string>();
            switch (commandName)
            {
                case "GetSurveyDemographicAnswer":

                    SurveyQuestions survey = SurveyQuestionManager.GetSurveyQuestion(Utils.GetQueryString<string>("surveyCode"));
                    String surveyCmpyCode = Utils.GetQueryString<string>("companyCode");
                    string[] surveyCmpyCodeList = null;
                    if (!string.IsNullOrEmpty(surveyCmpyCode))
                    {

                        surveyCmpyCodeList = surveyCmpyCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    }
                    if (survey != null)
                    {
                        if (survey.Type == "FiveStarPolls")
                        {
                            for (int i = 0; i < 5; i++)
                            {
                                string label = "";
                                for (var j = 0; j < i + 1; j++)
                                {
                                    label += "<i class='fa fa-star-o'></i>";
                                }
                                int counter = SurveyQuestionManager.CountApplicantSurvey(survey.Code, surveyCmpyCodeList, i.ToString());
                                var random = new Random();
                                string color = String.Format("#{0:X6}", random.Next(0x1000000));
                                data.Add(label + "^" + counter + "^" + color);
                            }

                            context.Response.ContentType = "text/plain";
                            context.Response.Write(string.Join("|", data.ToArray()));
                        }
                    }
                    break;
                case "GetSurveySatisfactoryAnswer":
                    String surveyCompanyCode = Utils.GetQueryString<string>("companyCode");
                    string[] surveyCompanyCodeList = null;
                    if (!string.IsNullOrEmpty(surveyCompanyCode))
                    {

                        surveyCompanyCodeList = surveyCompanyCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    }
                    SurveyQuestions satisfactorySurvey = SurveyQuestionManager.GetSurveyQuestion(Utils.GetQueryString<string>("surveyCode"));
                    if (satisfactorySurvey != null)
                    {
                        if (satisfactorySurvey.Type == "FiveStarPolls")
                        {
                            int total = 0;
                            for (int i = 1; i <= 5; i++)
                            {

                                int counter = SurveyQuestionManager.CountApplicantSurvey(satisfactorySurvey.Code, surveyCompanyCodeList, i.ToString());
                                total = total + counter;
                                data.Add(i + "^" + counter + "^" + total);
                            }

                            context.Response.ContentType = "text/plain";
                            context.Response.Write(string.Join("|", data.ToArray()));
                        }
                    }
                    break;
                case "GetReportAnswer":
                    String surveyCompaniesCode = Utils.GetQueryString<string>("companyCode");
                    string[] surveyCompaniesCodeList = null;
                    if (!string.IsNullOrEmpty(surveyCompaniesCode))
                    {

                        surveyCompaniesCodeList = surveyCompaniesCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    }
                    SurveyQuestions reportSatisfactorySurvey = SurveyQuestionManager.GetSurveyQuestion(Utils.GetQueryString<string>("surveyCode"));
                    if (reportSatisfactorySurvey != null)
                    {
                        string summary = "";
                        for (int i = 1; i <= 5; i++)
                        {
                            int counter = SurveyQuestionManager.CountApplicantSurvey(reportSatisfactorySurvey.Code, surveyCompaniesCodeList, i.ToString());
                            summary += "#" + i + "," + counter;
                        }

                        IList<Object[]> list = SurveyQuestionManager.ReportSatisfactorySurvey(reportSatisfactorySurvey.Code);
                        if (list != null && list.Count > 0)
                        {
                            for (int i = 0; i < list.Count; i++)
                            {
                                Object[] obj = list[i];
                                if (obj != null && obj.Length > 0)
                                {
                                    String label = "";
                                    int star = Int32.Parse(obj[2].ToString());
                                    for (var j = 0; j < star; j++)
                                    {
                                        label += "*";
                                    }
                                    data.Add(isBlank(obj[0]) + "^" + isBlank(obj[1]) + "^" + label + "^" + isBlank(obj[3] + "^" + summary) + "^" + isBlank(obj[4]));
                                }
                            }

                        }
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(string.Join("|", data.ToArray()));
                    }
                    break;
                case "GetActiveJobDemographicsByDepartment":
                    List<Departments> departmentList = ERecruitmentManager.GetDepartmentList();
                    foreach (Departments item in departmentList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByDepartment(item.Code, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByDivision":
                    List<Divisions> divisionList = ERecruitmentManager.GetDivisionList();
                    foreach (Divisions item in divisionList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByDivision(item.Code, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByCandidateSource":
                    // ini ngitung applicant
                    List<string> informationSourceList = new List<string>();
                    informationSourceList.Add("LinkedIn");
                    informationSourceList.Add("Facebook");
                    informationSourceList.Add("Twitter");
                    informationSourceList.Add("Newsletter");
                    informationSourceList.Add("Website");
                    informationSourceList.Add("Others");
                    foreach (string item in informationSourceList)
                    {
                        string label = item;
                        int counter = ERecruitmentManager.CountApplicantVacanyByInformationSource(item);
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByEducationLevel":
                    List<EducationLevels> educationLevelList = ERecruitmentManager.GetEducationLevels();
                    foreach (EducationLevels item in educationLevelList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByRequiredEducationLevel(item.EducationLevel, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByPositionType":
                    List<string> positionTypeList = new List<string>();
                    positionTypeList.Add("New Position");
                    positionTypeList.Add("Replacement");

                    foreach (string item in positionTypeList)
                    {
                        string label = item;
                        int counter = ERecruitmentManager.CountVacancyByPositionType(item, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));

                    break;
                case "GetActiveJobDemographicsByJobCode":
                    List<Jobs> jobCodeList = ERecruitmentManager.GetJobsList();
                    foreach (Jobs item in jobCodeList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByJobCode(item.JobCode, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByCompany":
                    List<Companies> companyList = ERecruitmentManager.GetCompanyList();
                    string[] companiesCodeList = null;
                    string cmpyCode = Utils.GetQueryString<string>("companyCode");
                    if (!string.IsNullOrEmpty(cmpyCode))
                    {

                        companiesCodeList = cmpyCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    }
                    foreach (Companies item in companyList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByCompany(item.Code, Utils.GetQueryString<string>("status"), companiesCodeList);
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByCountry":
                    List<Country> countryList = ERecruitmentManager.GetCountryList();
                    foreach (Country item in countryList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByCountryCode(item.Code, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByCategory":

                    List<VacancyCategories> categoryList = ERecruitmentManager.GetVacancyCategoryList();
                    foreach (VacancyCategories item in categoryList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByCategory(item.Code, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByLocations":

                    List<Locations> locationList = ERecruitmentManager.GetLocationList();
                    foreach (Locations item in locationList)
                    {
                        string label = item.Location;
                        int counter = ERecruitmentManager.CountVacancyByLocation(item.Location, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByEmploymentType":

                    List<EmploymentStatus> employmentStatusList = ERecruitmentManager.GetEmploymentStatusList();
                    foreach (EmploymentStatus item in employmentStatusList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByEmploymentType(item.Code, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByIndustry":

                    List<Industries> industryList = ERecruitmentManager.GetIndustryList();
                    foreach (Industries item in industryList)
                    {
                        string label = item.Name;
                        int counter = ERecruitmentManager.CountVacancyByEmploymentType(item.Code, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetActiveJobDemographicsByRecruitmentTargetGroup":

                    List<string> targetGroupList = new List<string>();
                    targetGroupList.Add("Experienced");
                    targetGroupList.Add("Recent Graduate");
                    foreach (string item in targetGroupList)
                    {
                        string label = item;
                        int counter = ERecruitmentManager.CountVacancyByTargetGroup(item, Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetJobPerMonth":
                    List<Month> jobMonthList = ERecruitmentManager.GetMonthList();
                    foreach (Month item in jobMonthList)
                    {
                        string label = item.Name;
                        DateTime date = new DateTime(DateTime.Now.Year, item.Id.Value, 1);
                        int counter = ERecruitmentManager.CountVacancyByRegisteredDateRange(date, date.AddMonths(1), Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetJobPerYear":
                    List<Years> jobYearList = ERecruitmentManager.GetLastYearList(10);
                    foreach (Years item in jobYearList)
                    {
                        string label = item.Year.ToString();
                        DateTime date = new DateTime(item.Year, 1, 1);
                        int counter = ERecruitmentManager.CountVacancyByRegisteredDateRange(date, date.AddYears(1), Utils.GetQueryString<string>("status"));
                        var random = new Random();
                        string color = String.Format("#{0:X6}", random.Next(0x1000000));
                        data.Add(label + "^" + counter + "^" + color);
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
                case "GetVacancyBarChart":

                    string[] xParameterList = Utils.GetQueryString<string>("xParams").Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                    string[] vParameterList = Utils.GetQueryString<string>("vParams").Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
                    string from = Utils.GetQueryString<string>("fromGPA");
                    string to = Utils.GetQueryString<string>("toGPA");
                    string companyCode = Utils.GetQueryString<string>("companyCode");
                    string[] companiesList = null;
                    if (!string.IsNullOrEmpty(companyCode))
                    {

                        companiesList = companyCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        
                    }

                    string getXObjectListMethodName = xParameterList[0];
                    string getVObjectListMethodName = vParameterList[0];

                    string xPropName = xParameterList[3];
                    string valuePropName = vParameterList[3];

                    string xObjectPropKey = xParameterList[1];
                    string xObjectPropLabel = xParameterList[2];

                    string vObjectPropKey = vParameterList[1];
                    string vObjectPropLabel = vParameterList[2];
                    List<object> xValueAxisList = new List<object>();
                    List<object> valueAxisKeyList = new List<object>();
                    if ("GetCompanyList".Equals(getXObjectListMethodName)) {
                        object tempXObject =  VacancyDashboardManager.GetCompanyList(companiesList);
                        if (tempXObject is System.Collections.IEnumerable)
                        {
                            var enumerator = ((System.Collections.IEnumerable)tempXObject).GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                xValueAxisList.Add(enumerator.Current);
                            }
                        }
                    }
                    else {
                        object tempXObject = typeof(VacancyDashboardManager).GetMethod(getXObjectListMethodName).Invoke(null, null);
                        if (tempXObject is System.Collections.IEnumerable)
                        {
                            var enumerator = ((System.Collections.IEnumerable)tempXObject).GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                xValueAxisList.Add(enumerator.Current);
                            }
                        }
                    }

                    if ("GetCompanyList".Equals(getVObjectListMethodName))
                    {
                        object tempYObject = VacancyDashboardManager.GetCompanyList(companiesList);
                        if (tempYObject is System.Collections.IEnumerable)
                        {
                            var enumerator = ((System.Collections.IEnumerable)tempYObject).GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                valueAxisKeyList.Add(enumerator.Current);
                            }
                        }
                    }
                    else
                    {
                        object tempYObject = typeof(VacancyDashboardManager).GetMethod(getVObjectListMethodName).Invoke(null, null);
                        if (tempYObject is System.Collections.IEnumerable)
                        {
                            var enumerator = ((System.Collections.IEnumerable)tempYObject).GetEnumerator();
                            while (enumerator.MoveNext())
                            {
                                valueAxisKeyList.Add(enumerator.Current);
                            }
                        }
                    }

                    foreach (object valueKey in valueAxisKeyList)
                    {
                        List<string> seriesList = new List<string>();
                        string vLabel = valueKey.GetType().GetProperty(vObjectPropLabel).GetValue(valueKey) as string;
                        string vKey = valueKey.GetType().GetProperty(vObjectPropKey).GetValue(valueKey) as string;
                        int i = 0;
                        foreach (object xData in xValueAxisList)
                        {
                            object xCode = xData.GetType().GetProperty(xObjectPropKey).GetValue(xData);
                            string xLabel = xData.GetType().GetProperty(xObjectPropLabel).GetValue(xData) as string;
                            int counter = 0;
                            if ("GetUniversityList".Equals(getXObjectListMethodName) && "GetVacanciesCategoryList".Equals(getVObjectListMethodName))
                            {
                                counter = VacancyDashboardManager.CountUniversityVsJobCategory(xPropName, xCode, valuePropName, vKey, Utils.GetQueryString<string>("status"), companiesList);
                            }
                            else if ("GetPositionList".Equals(getXObjectListMethodName) && "GetSourceList".Equals(getVObjectListMethodName))
                            {
                                counter = VacancyDashboardManager.CountPositionVsSource(xPropName, xCode, valuePropName, vKey, Utils.GetQueryString<string>("status"), companiesList);
                            }
                            else if ("GetPositionList".Equals(getXObjectListMethodName) && "GetUniversityList".Equals(getVObjectListMethodName))
                            {
                                counter = VacancyDashboardManager.CountUniversityVsPosition(xPropName, xCode, valuePropName, vKey, Utils.GetQueryString<string>("status"), from, to, companiesList);
                            }
                            else
                            {
                                if ("GetUniversityList".Equals(getXObjectListMethodName) || "GetUniversityList".Equals(getVObjectListMethodName) || "GetSourceList".Equals(getVObjectListMethodName))
                                {

                                }
                                else
                                {
                                    counter = VacancyDashboardManager.CountVacancy(xPropName, xCode, valuePropName, vKey, Utils.GetQueryString<string>("status"), companiesList);
                                }
                            }

                            //seriesList.Add("[\"" + xLabel + "\"," + counter + "]");
                            seriesList.Add("[" + i + "," + counter + "]");
                            i++;
                        }

                        data.Add(vLabel + "_" + string.Join(",", seriesList.ToArray()));
                    }

                    List<string> xLabelList = new List<string>();
                    foreach (object xData in xValueAxisList)
                    {
                        object xCode = xData.GetType().GetProperty(xObjectPropKey).GetValue(xData);
                        string xLabel = xData.GetType().GetProperty(xObjectPropLabel).GetValue(xData) as string;
                        xLabelList.Add(xLabel);
                    }


                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join(",", xLabelList) + "+" + string.Join("~", data.ToArray()));
                    break;
                case "GetPositionVsSource":
                    string[] cmpList = null;
                    string compCode = Utils.GetQueryString<string>("companyCode");
                    if (!string.IsNullOrEmpty(compCode))
                    {

                        cmpList = compCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                    }
                    IList<Object[]> iList = VacancyDashboardManager.ReportPositionVsSource(cmpList);
                        if (iList != null && iList.Count > 0)
                        {
                            for (int i = 0; i < iList.Count; i++)
                            {
                                Object[] obj = iList[i];
                                if (obj != null && obj.Length > 0)
                                {
                                   data.Add(isBlank(obj[0]) + "^" + isBlank(obj[1]) + "^" + isBlank(obj[2]) + "^" + isBlank(obj[3]) + "^" + isBlank(obj[6]));
                                }
                            }
                            
                        }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(string.Join("|", data.ToArray()));
                    break;
            }
}

public bool IsReusable
{
    get
    {
        return false;
    }
}

public string isBlank(Object o)
{
    try
    {
        if (o != null && !"".Equals(o.ToString()))
        {
            return o.ToString();
        }
        else
        {
            return "-";
        }
    }
    catch
    {
        return "-";
    }
}
    }
}