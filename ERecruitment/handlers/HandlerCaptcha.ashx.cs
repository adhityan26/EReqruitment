﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using SS.Web.UI;

namespace ERecruitment.handlers
{
    /// <summary>
    /// Summary description for HandlerCaptcha
    /// </summary>
    public class HandlerCaptcha : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            if (commandName != null)
            {
                if (commandName.Contains("setCaptcha"))
                {
                    context.Session["captcha"] = commandName.Substring(10, 5);
                }
            }
            int height = 30;

            int width = 100;

            Bitmap bmp = new Bitmap(width, height);



            RectangleF rectf = new RectangleF(10, 5, 0, 0);



            Graphics g = Graphics.FromImage(bmp);

            g.Clear(Color.White);

            g.SmoothingMode = SmoothingMode.AntiAlias;

            g.InterpolationMode = InterpolationMode.HighQualityBicubic;

            g.PixelOffsetMode = PixelOffsetMode.HighQuality;

            g.DrawString(context.Session["captcha"].ToString(), new Font("Thaoma", 12, FontStyle.Italic), Brushes.Green, rectf);

            g.DrawRectangle(new Pen(Color.Red), 1, 1, width - 2, height - 2);

            g.Flush();

            context.Response.ContentType = "image/jpeg";

            bmp.Save(context.Response.OutputStream, ImageFormat.Jpeg);

            g.Dispose();

            bmp.Dispose();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}