﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Reflection;
using System.IO;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerLocations
    /// </summary>
    public class HandlerLocations : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            List<string> data = new List<string>();
//            byte[] attachmentContent = null;
            Applicants getApplicant = new Applicants();
            UserAccounts getUserAccount = new UserAccounts();
            string output = string.Empty;
            List<string> dataList = new List<string>();

            UserAccounts CurrentUser = context.Session["currentuser"] as UserAccounts;
            if (CurrentUser == null)
                CurrentUser = new UserAccounts();


            switch (commandName)
            {
                case "GetCountry":
                    List<Country> getCountryList = ERecruitmentManager.GetCountryList();
                    foreach (Country item in getCountryList)
                    {
                        string dataCountry = string.Concat(item.Code, ",", item.Name);
                        dataList.Add(dataCountry);
                    }
                    output = string.Join("|", dataList.ToArray());
                    context.Response.Write(output);
                    break;
                case "GetCity":
                    List<City> getCityList = ERecruitmentManager.GetCity(Utils.GetQueryString<string>("provinceCode"));
                    foreach (City item in getCityList)
                    {
                        string dataCity = string.Concat(item.CityCode, ",", item.CityName);
                        dataList.Add(dataCity);
                    }
                    output = string.Join("|", dataList.ToArray());
                    context.Response.Write(output);
                    break;

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}