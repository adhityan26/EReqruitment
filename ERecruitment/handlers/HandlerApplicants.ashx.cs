﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Reflection;
using System.IO;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerApplicants
    /// </summary>
    public class HandlerApplicants : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            List<string> data = new List<string>();
            byte[] attachmentContent = null;
            Applicants getApplicant = new Applicants();
            UserAccounts getUserAccount = new UserAccounts();
            string output = string.Empty;
            List<string> dataList = new List<string>();

            UserAccounts User = context.Session["currentuser"] as UserAccounts;
            if (User == null)
                User = new UserAccounts();
            Applicants applicant = null;

            ApplicantEducations applicantEducation = new ApplicantEducations();
            List<ApplicantEducations> applicantEducationInfoList = new List<ApplicantEducations>();
            EducationLevels educationLevels = new EducationLevels();
            List<EducationLevels> educationLevelsList = new List<EducationLevels>();

            EducationFieldOfStudy educationFieldOfStudy = new EducationFieldOfStudy();
            List<EducationFieldOfStudy> educationFieldOfStudyList = new List<EducationFieldOfStudy>();

            EducationMajor educationMajor = new EducationMajor();
            List<EducationMajor> educationMajorList = new List<EducationMajor>();

            ApplicantExperiences applicantExperience = new ApplicantExperiences();
            List<ApplicantExperiences> applicantExperienceList = new List<ApplicantExperiences>();

            ApplicantSkills applicantSkill = new ApplicantSkills();
            List<ApplicantSkills> applicantSkillList = new List<ApplicantSkills>();

            ApplicantHobbyInterests applicantHobbyInterest = new ApplicantHobbyInterests();
            List<ApplicantHobbyInterests> applicantHobbyInterestList = new List<ApplicantHobbyInterests>();

            ApplicantAwards applicantAward = new ApplicantAwards();
            List<ApplicantAwards> applicantAwardList = new List<ApplicantAwards>();

            ApplicantTrainings applicantTraining = new ApplicantTrainings();
            List<ApplicantTrainings> applicantTrainingList = new List<ApplicantTrainings>();

            ApplicantAttachments applicantAttachment = new ApplicantAttachments();
            List<ApplicantAttachmentInfos> applicantAttachmentList = new List<ApplicantAttachmentInfos>();

            List<ApplicantVacancies> applicantVacancyList = new List<ApplicantVacancies>();

            List<ApplicantVacancyReferences> applicantReferenceList = new List<ApplicantVacancyReferences>();

            try
            {
                switch (commandName)
                {
                    case "UpdateSurveyQuestion":
                        
                            String companyCode = Utils.GetQueryString<string>("companyCode");
                            applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("applicantCode"));

                            ApplicantSurveys applicantSurvey = new ApplicantSurveys();
                            ApplicantSurveySessions applicantSurveySession = new ApplicantSurveySessions();
                            applicantSurvey.ApplicantCode = applicant.Code;
                            #region filling model with data

                            foreach (string name in context.Request.Form.Keys)
                            {
                                object propertyValue = context.Request.Form[name];

                                if (name == "Notes")
                                {
                                    // get property definition
                                    PropertyInfo propertyInfo = applicantSurvey.GetType().GetProperty(name);
                                    Type propertyType = propertyInfo.PropertyType;
                                    propertyValue = Convert.ChangeType(propertyValue, propertyType);
                                    propertyInfo.SetValue(applicantSurvey, propertyValue, null);
                                }
                                else
                                {
                                    applicantSurvey.QuestionCode = name;
                                    applicantSurvey.Answer = propertyValue.ToString();
                                }

                            }
                            applicantSurvey.CompaniesCode = companyCode;

                            List<ApplicantVacancies> appVacanciesList = ApplicantManager.GetApplicantActiveVacancies(Utils.GetQueryString<string>("applicantCode"));
                            if (appVacanciesList != null && appVacanciesList.Count > 0)
                            {
                                Vacancies v = ERecruitmentManager.GetVacancy(appVacanciesList[0].VacancyCode);
                                if (v != null)
                                {
                                    applicantSurvey.PositionCode = v.PositionCode;
                                }
                            }

                    
                            SurveyQuestionManager.SaveApplicantSurvey(applicantSurvey);
                            applicantSurveySession = ERecruitmentManager.GetActiveApplicantSurveySession(applicant.Code, companyCode);
                            applicantSurveySession.CompletedDate = DateTime.Now;
                            ERecruitmentManager.UpdateApplicantSurveySession(applicantSurveySession);

                        
                            
                        
                        #endregion

                        break;
                    case "GetApplicantReferenceList":
                        applicantReferenceList = ApplicantManager.GetReferencedApplicantList(Utils.GetQueryString<string>("companyCode"), Utils.GetQueryString<string>("applicantCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantReferenceList));
                        break;

                    case "GetApplicantApplicationList":
                        applicantVacancyList = ApplicantManager.GetApplicantActiveVacancies(Utils.GetQueryString<string>("applicantCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantVacancyList));
                        break;
                    case "ChangePassword":
                        if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("applicantCode")))
                            applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("applicantCode"));
                        else if (!string.IsNullOrEmpty(User.ApplicantCode))
                            applicant = ApplicantManager.GetApplicant(User.ApplicantCode);
                        if (applicant != null)
                        {
                            UserAccounts userAccountGet = Domain.AuthenticationManager.GetApplicantUserAccount(applicant.Code);

                            if (userAccountGet.Password != Utils.GetQueryString<string>("oldPassword"))
                                throw new ApplicationException("Old password not match");
                            userAccountGet.Password = Utils.GetQueryString<string>("newPassword");
                            Domain.AuthenticationManager.UpdateUserAccount(userAccountGet);
                            context.Session["currentuser"] = userAccountGet;

                        }
                        else
                        {
                            UserAccounts userAccountSession = Domain.AuthenticationManager.GetUserAccount(User.Code);

                            if (userAccountSession.Password != Utils.GetQueryString<string>("oldPassword"))
                                throw new ApplicationException("Old password not match");
                            userAccountSession.Password = Utils.GetQueryString<string>("newPassword");
                            Domain.AuthenticationManager.UpdateUserAccount(userAccountSession);

                            context.Session["currentuser"] = userAccountSession;
                        }
                        break;
                    case "UpdatePassword":
                        UserAccounts userAccount = Domain.AuthenticationManager.GetUserAccount(User.Code);
                        applicant = ApplicantManager.GetApplicant(User.Code);
                        applicant.IsRegis = "false";

                        userAccount.Password = Utils.GetQueryString<string>("updatePassword");
                        Domain.AuthenticationManager.UpdateUserAccount(userAccount);
                        Domain.AuthenticationManager.UpdateApplicant(applicant);

                        context.Session["currentuser"] = userAccount;
                        
                        break;
                    case "UploadApplicantAttachment":

                        if (context.Request.Files.Count > 0)
                        {
                            HttpFileCollection files = context.Request.Files;
                            for (int i = 0; i < files.Count; i++)
                            {
                                HttpPostedFile file = files[i];
                                byte[] fileData = null;
                                using (var binaryReader = new BinaryReader(file.InputStream))
                                {
                                    fileData = binaryReader.ReadBytes(file.ContentLength);
                                }

                                applicantAttachment = new ApplicantAttachments();
                                applicantAttachment.Attachment = fileData;
                                applicantAttachment.FileName = file.FileName;
                                applicantAttachment.FileType = file.ContentType;
                                applicantAttachment.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                                applicantAttachment.Notes = Utils.GetQueryString<string>("notes");
                                ApplicantManager.SaveUpdateApplicantAttachment(applicantAttachment);
                            }
                        }
                        break;
                    case "GetApplicantInfo":
                        applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("applicantCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicant));
                        break;
                    case "UploadApplicantPhoto":

                        applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("applicantCode"));
                        if (context.Request.Files.Count > 0)
                        {
                            HttpFileCollection files = context.Request.Files;
                            for (int i = 0; i < files.Count; i++)
                            {
                                HttpPostedFile file = files[i];
                                byte[] fileData = null;
                                using (var binaryReader = new BinaryReader(file.InputStream))
                                {
                                    fileData = binaryReader.ReadBytes(file.ContentLength);
                                }

                                applicant.Photo = Utils.resizeImageStream(fileData, 160, 180);
                                ApplicantManager.UpdateApplicant(applicant);
                            }
                        }
                        break;
                    case "UpdateApplicantInfo":
                        applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("applicantCode"));

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            //Commented by Ophie - when data is not required
                            //if (string.IsNullOrEmpty(propertyValue.ToString()))
                            //    continue;

                            // get property definition
                            PropertyInfo propertyInfo = applicant.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(applicant, propertyValue, null);
                        }

                        #endregion

                        ApplicantManager.UpdateApplicant(applicant);
                        break;
                    case "GetApplicantEducationLevel":
                        educationLevelsList = ApplicantManager.GetEducationLevelCascading(Utils.GetQueryString<string>("UniversitiesCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(educationLevelsList));
                        break;
                    case "GetApplicantEducationFieldOfStudy":
                        educationFieldOfStudyList = ApplicantManager.GetFieldOfStudyCascading(Utils.GetQueryString<string>("UniversitiesCode"), Utils.GetQueryString<string>("EducationLevel"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(educationFieldOfStudyList));
                        break;
                    case "GetApplicantEducationMajor":
                        educationMajorList = ApplicantManager.GetEducationMajorCascading(Utils.GetQueryString<string>("UniversitiesCode"), Utils.GetQueryString<string>("EducationLevel"), Utils.GetQueryString<string>("FieldOfStudyCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(educationMajorList));
                        break;

                    case "GetApplicantEducationInfo":
                        applicantEducation = ApplicantManager.GetApplicantEducationInfo(Utils.GetQueryString<int>("id"));
                        if (applicantEducation == null)
                        {
                            applicantEducation = new ApplicantEducations();
                            applicantEducation.EducationCountryCode = "Indonesia"; // default value
                        }
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantEducation));

                        break;
                    case "UpdateApplicantEducationInfo":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = applicantEducation.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(applicantEducation, propertyValue, null);
                        }

                        applicantEducation.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        ApplicantManager.SaveUpdateApplicantEducation(applicantEducation);

                        #endregion

                        break;
                    case "GetApplicantEducationInfoList":
                        applicantEducationInfoList = ApplicantManager.GetApplicantEducationList(Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantEducationInfoList));

                        break;
                    case "GetApplicantExperienceInfo":
                        applicantExperience = ApplicantManager.GetApplicantExperienceInfo(Utils.GetQueryString<int>("id"));
                        if (applicantExperience == null)
                        {
                            applicantExperience = new ApplicantExperiences();
                        }
                        else
                        {
                            applicantExperience.StartMonthYear = applicantExperience.StartMonth + "/" + applicantExperience.StartYear;
                            applicantExperience.EndMonthYear = applicantExperience.EndMonth + "/" + applicantExperience.EndYear;   
                        }
                        
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantExperience));

                        break;
                    case "UpdateApplicantExperienceInfo":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = applicantExperience.GetType().GetProperty(name);
                            // assign property

                            if (name == "StartMonthYear")
                            {
                                string[] monthData = propertyValue.ToString().Split('/');
                                applicantExperience.StartYear = Int32.Parse(monthData[1]);
                                applicantExperience.StartMonth = Int32.Parse(monthData[0]);
                            } 
                            else if (name == "EndMonthYear")
                            {
                                string[] monthData = propertyValue.ToString().Split('/');
                                applicantExperience.EndYear = Int32.Parse(monthData[1]);
                                applicantExperience.EndMonth = Int32.Parse(monthData[0]);
                            }
                            else
                            {
                                Type propertyType = propertyInfo.PropertyType;
                                if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                    propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                                else
                                    propertyValue = Convert.ChangeType(propertyValue, propertyType);

                                propertyInfo.SetValue(applicantExperience, propertyValue, null);
                            }
                        }

                        applicantExperience.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        ApplicantManager.SaveUpdateApplicantExperience(applicantExperience);

                        #endregion

                        break;
                    case "GetApplicantExperienceInfoList":
                        applicantExperienceList = ApplicantManager.GetApplicantExperienceList(Utils.GetQueryString<string>("applicantCode"));

                        applicantExperienceList.Select(a =>
                        {
                            a.StartMonthYear = a.StartMonth + "/" + a.StartYear;
                            a.EndMonthYear = a.EndMonth + "/" + a.EndYear;
                            return a;
                        });

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantExperienceList));

                        break;
                    case "GetApplicantSkillInfo":
                        applicantSkill = ApplicantManager.GetApplicantSkillInfo(Utils.GetQueryString<int>("id"));
                        if (applicantSkill == null)
                            applicantSkill = new ApplicantSkills();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantSkill));

                        break;
                    case "UpdateApplicantSkillInfo":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = applicantSkill.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(applicantSkill, propertyValue, null);
                        }

                        applicantSkill.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        ApplicantManager.SaveUpdateApplicantSkill(applicantSkill);

                        #endregion

                        break;
                    case "GetApplicantSkillInfoList":
                        applicantSkillList = ApplicantManager.GetApplicantSkillList(Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantSkillList));

                        break;

                    case "GetApplicantHobbyInterestInfo":
                        applicantHobbyInterest = ApplicantManager.GetApplicantHobbyInterestInfo(Utils.GetQueryString<int>("id"));
                        if (applicantHobbyInterest == null)
                            applicantHobbyInterest = new ApplicantHobbyInterests();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantHobbyInterest));

                        break;
                    case "UpdateApplicantHobbyInterestInfo":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = applicantHobbyInterest.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(applicantHobbyInterest, propertyValue, null);
                        }

                        applicantHobbyInterest.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        ApplicantManager.SaveUpdateApplicantHobbyInterest(applicantHobbyInterest);

                        #endregion

                        break;
                    case "GetApplicantHobbyInterestInfoList":
                        applicantHobbyInterestList = ApplicantManager.GetApplicantHobbyInterestList(Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantHobbyInterestList));
                        break;

                    case "GetApplicantAwardInfo":
                        applicantAward = ApplicantManager.GetApplicantAwardInfo(Utils.GetQueryString<int>("id"));
                        if (applicantAward == null)
                            applicantAward = new ApplicantAwards();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantAward));

                        break;
                    case "UpdateApplicantAwardInfo":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = applicantAward.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(applicantAward, propertyValue, null);
                        }

                        applicantAward.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        ApplicantManager.SaveUpdateApplicantAward(applicantAward);

                        #endregion

                        break;
                    case "GetApplicantAwardInfoList":
                        applicantAwardList = ApplicantManager.GetApplicantAwardList(Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantAwardList));

                        break;


                    case "GetApplicantTrainingInfo":
                        applicantTraining = ApplicantManager.GetApplicantTrainingInfo(Utils.GetQueryString<int>("id"));
                        if (applicantTraining == null)
                            applicantTraining = new ApplicantTrainings();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantTraining));

                        break;
                    case "UpdateApplicantTrainingInfo":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = applicantTraining.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "MM/dd/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(applicantTraining, propertyValue, null);
                        }

                        applicantTraining.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        ApplicantManager.SaveUpdateApplicantTraining(applicantTraining);

                        #endregion

                        break;
                    case "GetApplicantTrainingInfoList":
                        applicantTrainingList = ApplicantManager.GetApplicantTrainingList(Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantTrainingList));

                        break;

                    case "GetApplicantAttachmentInfoList":
                        applicantAttachmentList = ApplicantManager.GetApplicantAttachmentInfoList(Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(applicantAttachmentList));
                        break;
                    case "DownloadAttachment":
                        applicantAttachment = ApplicantManager.GetApplicantAttachmentInfo(Utils.GetQueryString<int>("id"));
                        if (applicantAttachment != null)
                        {

                            attachmentContent = applicantAttachment.Attachment;
                            context.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + applicantAttachment.FileName + "\"");
                            context.Response.ContentType = "application/octet-stream";
                            context.Response.Flush();
                        }
                        break;
                    case "GetApplicantPhoto":
                        getApplicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("code"));
                        if (getApplicant != null)
                        {
                            if (getApplicant.Photo != null)
                            {
                                byte[] streams = getApplicant.Photo;
                                if (streams != null)
                                    context.Response.OutputStream.Write(streams, 0, streams.Length);
                            }
                            else
                            {
                                context.Response.ContentType = "text/plain";
                                if (getApplicant.SocMedName == "facebook")
                                    context.Response.Write("http://graph.facebook.com/" + getApplicant.SocMedID + "/picture?type=large");
                                else if (getApplicant.SocMedName == "linkedin")
                                {
                                    context.Response.Write(getApplicant.LinkedInImageUrl);
                                }
                                else
                                    context.Response.Write("../assets/images/avatar.png");
                            }
                        }
                        break;
                    case "GetUserAccountPhoto":
                        getUserAccount = Domain.AuthenticationManager.GetUserAccount(Utils.GetQueryString<string>("code"));
                        if (getUserAccount != null)
                        {
                            if (getUserAccount.Photo != null)
                            {
                                byte[] streams = getUserAccount.Photo;
                                if (streams != null)
                                    context.Response.OutputStream.Write(streams, 0, streams.Length);
                            }
                        }
                        break;
                    case "GetCompanyPhoto":
                        Companies company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                        if (company != null)
                        {
                            if (company.Logo != null)
                            {
                                byte[] streams = company.Logo;
                                if (streams != null)
                                    context.Response.OutputStream.Write(streams, 0, streams.Length);
                            }
                        }
                        break;
                    case "RemoveApplicantAttachmentInfo":
                        applicantAttachment = ApplicantManager.GetApplicantAttachmentInfo(Utils.GetQueryString<int>("id"));
                        if (applicantAttachment != null)
                            ApplicantManager.RemoveApplicantAttachmentInfo(applicantAttachment);
                        break;
                    case "RemoveApplicantEducationInfo":
                        applicantEducation = ApplicantManager.GetApplicantEducationInfo(Utils.GetQueryString<int>("id"));
                        if (applicantEducation != null)
                            ApplicantManager.RemoveApplicantEducationInfo(applicantEducation);
                        break;
                    case "RemoveApplicantExperienceInfo":
                        applicantExperience = ApplicantManager.GetApplicantExperienceInfo(Utils.GetQueryString<int>("id"));
                        if (applicantExperience != null)
                            ApplicantManager.RemoveApplicantExperienceInfo(applicantExperience);
                        break;
                    case "RemoveApplicantSkillInfo":
                        applicantSkill = ApplicantManager.GetApplicantSkillInfo(Utils.GetQueryString<int>("id"));
                        if (applicantSkill != null)
                            ApplicantManager.RemoveApplicantSkillInfo(applicantSkill);
                        break;
                    case "RemoveApplicantHobbyInterestInfo":
                        applicantHobbyInterest = ApplicantManager.GetApplicantHobbyInterestInfo(Utils.GetQueryString<int>("id"));
                        if (applicantSkill != null)
                            ApplicantManager.RemoveApplicantHobbyInterestInfo(applicantHobbyInterest);
                        break;
                    case "RemoveApplicantAwardInfo":
                        applicantAward = ApplicantManager.GetApplicantAwardInfo(Utils.GetQueryString<int>("id"));
                        if (applicantSkill != null)
                            ApplicantManager.RemoveApplicantAwardInfo(applicantAward);
                        break;
                    case "RemoveApplicantTrainingInfo":
                        applicantTraining = ApplicantManager.GetApplicantTrainingInfo(Utils.GetQueryString<int>("id"));
                        if (applicantSkill != null)
                            ApplicantManager.RemoveApplicantTrainingInfo(applicantTraining);
                        break;
                }
                if (attachmentContent != null)
                {
                    context.Response.OutputStream.Write(attachmentContent, 0, attachmentContent.Length);
                }
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 666;
                context.Response.ContentType = "text/plain";
                context.Response.Write(Utils.ExtractInnerExceptionMessage(e));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}