﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Security;
using System.Web.SessionState;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerUserAccounts
    /// </summary>
    public class HandlerUserAccounts : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            List<string> data = new List<string>();
         
            UserAccounts getUserAccount = new UserAccounts();
            string output = string.Empty;
            List<string> dataList = new List<string>();
            switch (commandName)
            {
                case "RemoveUser":
                    string[] getUserCode= Utils.GetQueryString<string>("code").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < getUserCode.Length; i++)
                    {
                        getUserAccount = AuthenticationManager.GetUserAccount(getUserCode[i]);
                        if (getUserAccount != null)
                        {
                            AuthenticationManager.RemoveUserAccount(getUserAccount);
                        }
                    }
                   
                    break;
                case "GetPhoto":
                    getUserAccount = AuthenticationManager.GetUserAccount(Utils.GetQueryString<string>("code"));
                    if (getUserAccount != null)
                    {
                        if (getUserAccount.Photo != null)
                        {
                            byte[] streams = getUserAccount.Photo;
                            if (streams != null)
                                context.Response.OutputStream.Write(streams, 0, streams.Length);
                        }
                    }
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}