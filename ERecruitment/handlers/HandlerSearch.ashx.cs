﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Script.Serialization;

namespace ERecruitment
{
    public class JqueryAutoCompleteItemContainer
    {
        public JqueryAutoCompleteItemContainer(string value, string display, string caption)
        {
            Value = value;
            Display = display;
            Caption = caption;
        }

        public string Caption;
        public string Display;
        public string Value;
    }

    /// <summary>
    /// Summary description for HandlerSearch
    /// </summary>
    public class HandlerSearch : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            List<Vacancies> jobList = new List<Vacancies>();
            List<JqueryAutoCompleteItemContainer> dataContainerList = new List<JqueryAutoCompleteItemContainer>();
            switch (commandName)
            {
                case "SearchJob":
                    jobList = ERecruitmentManager.SearchVacancyList(Utils.GetQueryString<string>("q"), "",DateTime.Now);
                    foreach (Vacancies job in jobList)
                    {
                        List<string> properties = new List<string>();
                        properties.Add(job.PositionName);
                        List<string> displayTextList = new List<string>();
                        displayTextList.Add(job.PositionName);

                        dataContainerList.Add(new JqueryAutoCompleteItemContainer(string.Join("|", properties.ToArray()), string.Join("|", displayTextList.ToArray()), string.Join("|", displayTextList.ToArray())));
                    }

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(new JavaScriptSerializer().Serialize(dataContainerList));
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}