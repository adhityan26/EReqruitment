﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Script.Serialization;

using KG.SISDM;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerProcessHiring
    /// </summary>
    public class HandlerProcessHiring : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            string vacancyCode = Utils.GetQueryString<string>("vacancyCode");
            string statusCode = Utils.GetQueryString<string>("statusCode");
            string comment = Utils.GetQueryString<string>("comment");
            int numberHired;
            string[] applicantCodeList;
            List<VacanciesSAPJobPosition> sapJobPositionID = new List<VacanciesSAPJobPosition>();
            Vacancies vacancy;
            ApplicationSettings appSetting = ERecruitmentManager.GetApplicationSetting();

            try
            {
                switch (commandName)
                {
                    case "ApproveJob":
                        vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("code"));
                        vacancy.Status = "Open";
                        ERecruitmentManager.UpdateVacancy(vacancy);
                        break;
                    case "RejectJob":
                        vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("code"));
                        vacancy.Status = "Rejected";
                        ERecruitmentManager.UpdateVacancy(vacancy);
                        break;
                    case "CloseAppointment":
                        ApplicantVacancySchedules appointment = HiringManager.GetApplicantSchedule(Utils.GetQueryString<int>("id"));
                        appointment.Status = "Close";
                        HiringManager.UpdateSchedule(appointment);
                        break;
                    case "UpdateComment":
                        string[] getAppcode = Utils.GetQueryString<string>("appCodes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        ApplicantVacancyComments addComment = new ApplicantVacancyComments();
                        addComment.ApplicantCode = getAppcode[0].Replace(" ", "");
                        addComment.Status = statusCode;
                        addComment.Date = DateTime.Now;
                        addComment.VacancyCode = vacancyCode;
                        addComment.Comment = comment;
                        addComment.CommentBy = Utils.GetQueryString<string>("issuedBy");
                        HiringManager.SaveComment(addComment);

                        break;
                    case "UpdateApplicantPipeline":
                        applicantCodeList = Utils.GetQueryString<string>("appCodes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

                        for (int i = 0; i < applicantCodeList.Length; i++)
                        {
                            ApplicantVacancies appVacancy = ERecruitmentManager.GetApplicantVacancies(applicantCodeList[i], vacancyCode);
                            VacancyManager.UpdateApplicantVacancyRecruitmentStatus(appVacancy, statusCode, DateTime.Now);
                            ERecruitmentAppSettings.SendApplicantUpdateStatus(appVacancy);
                        }
                        break;
                    case "DisqualifyApplicant":
                        applicantCodeList = Utils.GetQueryString<string>("appCodes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < applicantCodeList.Length; i++)
                        {
                            ApplicantVacancies appVacancy = ERecruitmentManager.GetApplicantVacancies(applicantCodeList[i], vacancyCode);
                            vacancy = ERecruitmentManager.GetVacancy(appVacancy.VacancyCode);
                            Pipelines pipeline = HiringManager.GetPipeline(vacancy.PipelineCode);

                            VacancyManager.UpdateApplicantVacancyRecruitmentStatus(appVacancy, pipeline.DisqualifiedRecruitmentStatusCode, DateTime.Now);
                            ERecruitmentManager.DeleteApplicantsVacancyReferences(appVacancy.ApplicantCode);
                            ERecruitmentManager.DeleteApplicantFlagStatuses(appVacancy.ApplicantCode);
                            ERecruitmentAppSettings.SendApplicantUpdateStatus(appVacancy);
                        }
                        break;
                    case "HireApplicant":
                        List<ApplicantVacancies> applicantVacancyList = new List<ApplicantVacancies>();
                        applicantCodeList = Utils.GetQueryString<string>("appCodes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        string sapJob = Utils.GetQueryString<string>("sapJob");
                        for (int i = 0; i < applicantCodeList.Length; i++)
                        {
                            ApplicantVacancies appVacancy = ERecruitmentManager.GetApplicantVacancies(applicantCodeList[i], vacancyCode);
                            vacancy = ERecruitmentManager.GetVacancy(appVacancy.VacancyCode);

                            Pipelines pipeline = HiringManager.GetPipeline(vacancy.PipelineCode);
                            VacancyManager.UpdateApplicantVacancyRecruitmentStatus(appVacancy, pipeline.HiredRecruitmentStatusCode, DateTime.Now);

                            //Disqualify all active application after hired one application
                            ApplicationSettings applicationSetting = ERecruitmentManager.GetApplicationSetting();
                            if (applicationSetting.VacancyApplicationMode == "Multi")
                            {
                                List<string> activeApplicationStatusList = VacancyManager.GetNonArchiveRecruitmentProcessCodeList();

                                List<ApplicantVacancies> activeApplicantVacancyList = ERecruitmentManager.GetApplicantVacancyListByApplicantCode(applicantCodeList[i], activeApplicationStatusList.ToArray());
                                if (activeApplicantVacancyList.Count > 0)
                                {
                                    for (int x = 0; x < activeApplicantVacancyList.Count; x += 1)
                                    {
                                        vacancy = ERecruitmentManager.GetVacancy(activeApplicantVacancyList[x].VacancyCode);
                                        Pipelines pipeline2 = HiringManager.GetPipeline(vacancy.PipelineCode);
                                        VacancyManager.UpdateApplicantVacancyRecruitmentStatus(activeApplicantVacancyList[x], pipeline2.AutoDisqualifiedRecruitmentStatusCode, DateTime.Now);
                                    }
                                }
                            }

                            ERecruitmentAppSettings.SendApplicantUpdateStatus(appVacancy);
                            applicantVacancyList.Add(appVacancy);
                        }
                        //Synchronize to SAP if Job from SAP Database
                        if (sapJob != "nonesap")
                        {
                            ERecruitmentManager.SynchronizeHiredApplicants(applicantVacancyList, sapJob);
                        }

                        break;
                    case "GetSAPJobPositionID":
                        sapJobPositionID = VacancyManager.GetSAPPositionId(Utils.GetQueryString<string>("vacancyCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(sapJobPositionID));
                        break;
                    case "countHiredApplicant":
                        string vacanCode = Utils.GetQueryString<string>("vacancyCode");
                        int status;
                        vacancy = ERecruitmentManager.GetVacancy(vacanCode);
                        numberHired = ERecruitmentManager.countHiredApplicantByVacanciesCode(vacanCode);
                        if (numberHired == vacancy.NumberOfOpening)
                        {
                            status = 1;
                        }
                        else status = 0;
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(status);
                        break;
                }
          
            }
            catch(Exception e)
            {
                context.Response.StatusCode = 666;
                context.Response.ContentType = "text/plain";
                context.Response.Write(Utils.ExtractInnerExceptionMessage(e));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}