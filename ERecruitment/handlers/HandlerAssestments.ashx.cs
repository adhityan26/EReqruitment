﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Security;
using System.Web.SessionState;
using System.Reflection;
using System.Web.Script.Serialization;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerAssestments
    /// </summary>
    public class HandlerAssestments : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
//            string output;
            bool initated = false;

            Questions assestmentQuestion = new Questions();
            List<Questions> assestmentQuestionList = new List<Questions>();

            switch (commandName)
            {

                case "GetAssestmentQuestionList":

                    assestmentQuestionList = AssestmentManager.GetAssestmentQuestionList(AssestmentManager.GetAssestmentQuestionCodeList(Utils.GetQueryString<string>("assestmentCode")).ToArray());
                    foreach (Questions item in assestmentQuestionList)
                        item.AnswerOptionList = AssestmentManager.GetAnswerOptionList(item.Code);

                    context.Response.ContentType = "text/plain";
                    context.Response.Write(new JavaScriptSerializer().Serialize(assestmentQuestionList));
                    break;
                case "UpdateAssestmentQuestion":

                    #region filling model with data

                    foreach (string name in context.Request.Form.Keys)
                    {
                        object propertyValue = context.Request.Form[name];
                        if (string.IsNullOrEmpty(propertyValue.ToString()))
                            continue;

                        if (name.Contains("."))
                        {
                            // nested object
                            string[] objectKeys = name.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
                            string nestedPropertyName = objectKeys[1];
                            string nestedObjectPropertyName = objectKeys[2];
                            int indexNumber = Utils.ConvertString<int>(objectKeys[3]);

                            QuestionAnswers answerOption = assestmentQuestion.AnswerOptionList.Find(delegate (QuestionAnswers temp) { return temp.Number == indexNumber; });
                            if (answerOption == null)
                            {
                                answerOption = new QuestionAnswers();
                                answerOption.Number = indexNumber;
                                assestmentQuestion.AnswerOptionList.Add(answerOption);
                            }
                            // assigning value to nested object instance
                            // get property definition
                            PropertyInfo nestedPropertyInfo = answerOption.GetType().GetProperty(nestedObjectPropertyName);
                            // assign property
                            Type nestedPropertyType = nestedPropertyInfo.PropertyType;
                            if (nestedPropertyType == typeof(DateTime) || nestedPropertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, nestedPropertyType);

                            nestedPropertyInfo.SetValue(answerOption, propertyValue, null);
                        }
                        else
                        {
                            // get property definition
                            PropertyInfo propertyInfo = assestmentQuestion.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(assestmentQuestion, propertyValue, null);
                            if (!string.IsNullOrEmpty(assestmentQuestion.Code) && !initated)
                            {
                                assestmentQuestion = AssestmentManager.GetAssestmentQuestion(assestmentQuestion.Code);
                                initated = true;
                            }
                        }
                    }

                    if (string.IsNullOrEmpty(assestmentQuestion.Code))
                    {
                        assestmentQuestion.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                        assestmentQuestion.Active = true;
                        assestmentQuestion.Type = "Parent";
                        AssestmentManager.SaveAssestmentQuestion(assestmentQuestion);
                    }
                    else
                        AssestmentManager.UpdateAssestmentQuestion(assestmentQuestion);

                    #endregion

                    break;
                case "GetAssestmentQuestion":
                    assestmentQuestion = AssestmentManager.GetAssestmentQuestion(Utils.GetQueryString<string>("id"));
                    if (assestmentQuestion == null)
                        assestmentQuestion = new Questions();
                    else
                    {
                        assestmentQuestion.AnswerOptionList = AssestmentManager.GetAnswerOptionList(assestmentQuestion.Code);
                    }
                    context.Response.ContentType = "text/plain";
                    context.Response.Write(new JavaScriptSerializer().Serialize(assestmentQuestion));
                    break;
                case "RemoveAssestmentQuestion":
                    var keyCodes = Utils.GetQueryString<string>("id").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < keyCodes.Length; i++)
                    {

                        assestmentQuestion = AssestmentManager.GetAssestmentQuestion(keyCodes[i]);
                        assestmentQuestion.Active = false;
                        AssestmentManager.UpdateAssestmentQuestion(assestmentQuestion);
                    }
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}