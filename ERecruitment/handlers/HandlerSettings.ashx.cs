﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Security;
using System.Web.SessionState;
using System.Reflection;
using System.Web.Script.Serialization;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerSettings
    /// </summary>
    public class HandlerSettings : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            List<string> data = new List<string>();
            Applicants getApplicant = new Applicants();
            string output = string.Empty;
            List<string> dataList = new List<string>();
            List<Jobs> listJobCode = new List<Jobs>();
            VacancyStatus recruitmentProcess = new VacancyStatus();
            Tests test = new Tests();
            ApplicationConfiguration appConfig = new ApplicationConfiguration();
            TestAttachments attachment = new TestAttachments();
            List<PageTranslatableLabels> pageTranslatableList = new List<PageTranslatableLabels>();
            List<PageTranslatableLabelContents> pageTranslatableContentList = new List<PageTranslatableLabelContents>();
            bool initated = false;

            try
            {
                switch (commandName)
                {
                    case "GetParentPageContext":
                        string pageName = Utils.GetQueryString<string>("pageName");
                        PageHighlightContexts highlightContext = PageManager.GetPageHighlightContext(pageName);
                        if (highlightContext != null)
                        {
                            context.Response.ContentType = "text/plain";
                            context.Response.Write(highlightContext.ContextFileName);
                        }
                        break;
                    case "GetPageLanguageSettings":
                        string mainSite = Utils.GetQueryString<string>("atsSite");
                        string pageSite = Utils.GetQueryString<string>("pageSite");
                        string lang = Utils.GetQueryString<string>("lang");

                        Companies mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(mainSite);
                        Pages targetPage = PageManager.GetPageByName(pageSite);
                        if (targetPage != null)
                        {
                            pageTranslatableList = PageManager.GetPageTranslatableLabelList(new string[] { targetPage.ID, "P.02", "P.03", "R.05" }, mainCompany.Code);

                            context.Response.ContentType = "text/plain";
                            context.Response.Write(new JavaScriptSerializer().Serialize(pageTranslatableList));
                        }

                        break;
                    case "UpdatePageLanguageSettings":

                        foreach (string name in context.Request.Form.Keys)
                        {
                            string[] dataKeys = name.Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);

                            PageTranslatableLabelContents translatableContent = pageTranslatableContentList.Find(delegate (PageTranslatableLabelContents temp) { return temp.Label == dataKeys[0]; });

                            if (translatableContent == null)
                            {
                                translatableContent = PageManager.GetPageTranslatableContent(Utils.GetQueryString<string>("pageCode"),
                                                                                             Utils.GetQueryString<string>("companyCode"),
                                                                                             dataKeys[0]);
                                if (translatableContent == null)
                                {
                                    translatableContent = new PageTranslatableLabelContents();
                                    translatableContent.IsNew = true;
                                    translatableContent.CompanyCode = Utils.GetQueryString<string>("companyCode");
                                    translatableContent.PageCode = Utils.GetQueryString<string>("pageCode");
                                    translatableContent.Label = dataKeys[0];
                                }

                                pageTranslatableContentList.Add(translatableContent);
                            }

                            if (dataKeys[1] == "IDText")
                                translatableContent.IDText = context.Request.Form[name].ToString();
                            else
                                translatableContent.ENText = context.Request.Form[name].ToString();
                        }

                        PageManager.UpdateTranslatableContentList(pageTranslatableContentList);
                        break;
                    case "LoadPageLabels":
                        pageTranslatableList = PageManager.GetPageTranslatableLabelList(Utils.GetQueryString<string>("pageCode"),
                                                                                        Utils.GetQueryString<string>("companyCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(pageTranslatableList));
                        break;
                    case "SendTestEmail":
                        Companies company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("companyCode"));
                        Emails emailTemplate = ERecruitmentManager.GetEmail(1);
                        ERecruitmentAppSettings.SendEmail(company, Utils.GetQueryString<string>("emailAddress"), emailTemplate.Subject, emailTemplate.Body);
                        break;
                    case "UpdateRecruitmentProcess":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = recruitmentProcess.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(recruitmentProcess, propertyValue, null);
                            if (!string.IsNullOrEmpty(recruitmentProcess.Code) && !initated)
                            {
                                recruitmentProcess = ERecruitmentManager.GetVacancyStatus(recruitmentProcess.Code);
                                initated = true;
                            }
                        }

                        recruitmentProcess.RecruiterLabel = recruitmentProcess.Name;
                      
                        if (string.IsNullOrEmpty(recruitmentProcess.Code))
                        {
                            recruitmentProcess.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            recruitmentProcess.Removable = true;
                            recruitmentProcess.IsArchive = false;
                            recruitmentProcess.Active = true;
                            ERecruitmentManager.SaveVacancyStatus(recruitmentProcess);
                        }
                        else
                            ERecruitmentManager.UpdateVacancyStatus(recruitmentProcess);

                        #endregion

                        break;
                    case "GetRecruitmentProcess":
                        recruitmentProcess = ERecruitmentManager.GetVacancyStatus(Utils.GetQueryString<string>("id"));
                        if (recruitmentProcess == null)
                            recruitmentProcess = new VacancyStatus();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(recruitmentProcess));
                        break;
                    case "UpdateTest":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = test.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(test, propertyValue, null);
                            if (!string.IsNullOrEmpty(test.TestCode) && !initated)
                            {
                                test = HiringManager.GetTest(test.TestCode);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(test.TestCode))
                        {
                            test.TestCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            test.Active = true;
                            HiringManager.SaveTest(test);
                        }
                        else
                            HiringManager.UpdateTest(test);

                        #endregion

                        break;
                    case "GetTest":
                        test = HiringManager.GetTest(Utils.GetQueryString<string>("id"));
                        if (test == null)
                            test = new Tests();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(test));
                        break;
                    case "GetAppConfig":
                        appConfig = ERecruitmentManager.GetApplicationConfiguration(Utils.GetQueryString<int>("id"));
                        if (appConfig == null)
                            appConfig = new ApplicationConfiguration();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(appConfig));
                        break;

                    case "UpdateAttachment":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = attachment.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(attachment, propertyValue, null);
                            if (!string.IsNullOrEmpty(attachment.AttachmentCode) && !initated)
                            {
                                attachment = HiringManager.GetTestAttachment(attachment.AttachmentCode);
                                initated = true;
                            }
                        }

                        if (string.IsNullOrEmpty(attachment.AttachmentCode))
                        {
                            attachment.AttachmentCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                            attachment.Active = true;
                            HiringManager.SaveTestAttachment(attachment);
                        }
                        else
                            HiringManager.UpdateTestAttachment(attachment);

                        #endregion

                        break;
                    case "GetAttachment":
                        attachment = HiringManager.GetTestAttachment(Utils.GetQueryString<string>("id"));
                        if (attachment == null)
                            attachment = new TestAttachments();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(attachment));
                        break;
                    case "CheckToggleVacancyInformationSourceAdditionalInfo":
                        ApplicantVacancieInformationSources informationSource = ERecruitmentManager.GetApplicationVacancySourceByCode(Utils.GetQueryString<string>("typeCode"));
                        if (informationSource == null)
                            informationSource = new ApplicantVacancieInformationSources();
                        if (informationSource.AdditionalInfoRequired)
                        {
                            context.Response.ContentType = "text/plain";
                            context.Response.Write("True");
                        }
                        else
                        {
                            context.Response.ContentType = "text/plain";
                            context.Response.Write("False");
                        }
                        break;
                    case "RemoveTestAttachment":
                        string[] attachmentIdList = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < attachmentIdList.Length; i++)
                        {
                            TestAttachments getTest = HiringManager.GetTestAttachment(attachmentIdList[i]);
                            HiringManager.RemoveTestAttachment(getTest);
                        }
                        break;
                    case "RemoveTests":
                        string[] testIdList = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < testIdList.Length; i++)
                        {
                            Tests getTest = HiringManager.GetTest(testIdList[i]);
                            HiringManager.RemoveTest(getTest);
                        }
                        break;
                    case "RemovePipeline":
                        string[] pipelineIdList = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < pipelineIdList.Length; i++)
                        {
                            Pipelines getPipeline = HiringManager.GetPipeline(pipelineIdList[i]);
                            HiringManager.RemovePipeline(getPipeline);
                        }
                        break;
                    case "RemoveRoles":
                        string[] roleIdList = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < roleIdList.Length; i++)
                        {
                            ERecruitment.Domain.Roles getRole = ERecruitmentManager.GetRole(roleIdList[i]);
                            ERecruitmentManager.RemoveRole(getRole);
                        }
                        break;
                    case "RemoveCompany":
                        string[] companyIdList = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < companyIdList.Length; i++)
                        {
                            Companies getCompany = ERecruitmentManager.GetCompany(companyIdList[i]);
                            ERecruitmentManager.RemoveCompany(getCompany);
                        }
                        break;
                    case "RemoveStatus":
                        string[] statusIdList = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < statusIdList.Length; i++)
                        {
                            VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(statusIdList[i]);
                            ERecruitmentManager.RemoveVacancyStatus(getStatus);
                        }
                        break;
                    case "RemoveSMSTemplate":
                        string[] templateIdList = Utils.GetQueryString<string>("ids").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        List<int> convertSMSTemplateIdList = new List<int>();
                        for (int i = 0; i < templateIdList.Length; i++)
                        {
                            convertSMSTemplateIdList.Add(Utils.ConvertString<int>(templateIdList[0]));
                        }
                        if (convertSMSTemplateIdList.Count > 0)
                        {
                            foreach (int item in convertSMSTemplateIdList)
                            {
                                SMSTemplates getSMSTemplates = ERecruitmentManager.GetSMSTemplate(item);
                                ERecruitmentManager.RemoveSMSTemplate(getSMSTemplates);
                            }

                        }
                        break;
                    case "RemoveEmails":
                        string[] emailIdList = Utils.GetQueryString<string>("ids").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        List<int> convertEmailId = new List<int>();
                        for (int i = 0; i < emailIdList.Length; i++)
                        {
                            convertEmailId.Add(Utils.ConvertString<int>(emailIdList[0]));
                        }
                        if (convertEmailId.Count > 0)
                        {
                            foreach (int item in convertEmailId)
                            {
                                Emails getEmail = ERecruitmentManager.GetEmail(item);
                                ERecruitmentManager.RemoveEmail(getEmail);
                            }
                        }
                        break;

                    case "RemoveIndustry":
                        Industries getIndustry = ERecruitmentManager.GetIndustry(Utils.GetQueryString<string>("code"));
                        getIndustry.Active = false;
                        ERecruitmentManager.UpdateIndustry(getIndustry);
                        break;
                    case "RemovePosition":
                        Positions getPosition = ERecruitmentManager.GetPosition(Utils.GetQueryString<string>("code"));
                        getPosition.Active = false;
                        ERecruitmentManager.UpdatePosition(getPosition);
                        break;
                    case "RemoveUniversity":
                        Universities getUniversity = ERecruitmentManager.GetUniversity(Utils.GetQueryString<string>("code"));
                        getUniversity.Active = false;
                        ERecruitmentManager.UpdateUniversity(getUniversity);
                        break;
                    case "RemoveDepartment":
                        Departments getDepartment = ERecruitmentManager.GetDepartment(Utils.GetQueryString<string>("code"));
                        ERecruitmentManager.RemoveDepartment(getDepartment);
                        break;
                    case "RemoveCountry":
                        Country getCountry = ERecruitmentManager.GetCountry(Utils.GetQueryString<string>("code"));
                        ERecruitmentManager.RemoveCountry(getCountry);
                        break;
                    case "RemoveProvince":
                        Province getProvince = ERecruitmentManager.GetProvince(Utils.GetQueryString<string>("code"));
                        ERecruitmentManager.RemoveProvince(getProvince);
                        break;
                    case "RemoveCity":
                        City getCity = ERecruitmentManager.GetCityByCode(Utils.GetQueryString<string>("code"));
                        ERecruitmentManager.RemoveCity(getCity);
                        break;
                    case "RemoveDivision":
                        Divisions getDivision = ERecruitmentManager.GetDivision(Utils.GetQueryString<string>("code"));
                        ERecruitmentManager.RemoveDivision(getDivision);
                        break;
                    case "RemoveEmploymentStatus":
                        EmploymentStatus getEmpStatus = ERecruitmentManager.GetEmploymentStatus(Utils.GetQueryString<string>("code"));
                        getEmpStatus.Active = false;
                        ERecruitmentManager.UpdateEmploymentStatus(getEmpStatus);
                        break;
                    case "RemoveJobCode":
                        Jobs getJobCode = ERecruitmentManager.GetJob(Utils.GetQueryString<string>("code"));
                        ERecruitmentManager.RemoveJob(getJobCode);
                        break;
                    case "GetJobCode":
                        Jobs getJob = ERecruitmentManager.GetJob(Utils.GetQueryString<string>("code"));
                        if (getJob != null)
                        {
                            dataList.Add(getJob.JobRole);
                            dataList.Add(getJob.JobStage);
                            dataList.Add(getJob.Requirements);
                            dataList.Add(getJob.RequiredEducationLevel);
                            dataList.Add(getJob.Name);
                            dataList.Add(getJob.JobDescription);

                        }
                        output = string.Join(",", dataList.ToArray());
                        context.Response.Write(output);
                        break;
                    case "GetListJobCode":
                        listJobCode = ERecruitmentManager.GetJobsList();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(listJobCode));
                        break;
                    case "GetCompanyIndustry":
                        string industryCode = string.Empty;
                        Companies getCompanyIndustry = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                        if (getCompanyIndustry != null)
                        {
                            industryCode = getCompanyIndustry.Industry;
                        }
                        output = industryCode;
                        context.Response.Write(output);
                        break;
                    case "RemoveEducationLevel":
                        EducationLevels getEducationLevel = ERecruitmentManager.GetEducationLevel(Utils.GetQueryString<string>("code"));
                        getEducationLevel.Active = false;
                        ERecruitmentManager.UpdateEducationLevel(getEducationLevel);
                        break;
                    case "RemoveFieldOfStudy":
                        EducationFieldOfStudy getFieldOfStudy = ERecruitmentManager.GetFieldOfStudy(Utils.GetQueryString<string>("code"));
                        getFieldOfStudy.Active = false;
                        ERecruitmentManager.UpdateFieldOfStudy(getFieldOfStudy);
                        break;
                    case "RemoveEducationMajor":
                        EducationMajor getEducationMajor = ERecruitmentManager.GetEducationMajor(Utils.GetQueryString<string>("code"));
                        getEducationMajor.Active = false;
                        ERecruitmentManager.UpdateEducationMajor(getEducationMajor);
                        break;
                    case "RemoveLanguage":
                        Languages getLanguage = ERecruitmentManager.GetLanguage(Utils.GetQueryString<string>("code"));
                        getLanguage.Active = false;
                        ERecruitmentManager.UpdateLanguage(getLanguage);
                        break;
                    case "RemoveAward":
                        Awards getAward = ERecruitmentManager.GetAward(Utils.GetQueryString<string>("code"));
                        getAward.Active = false;
                        ERecruitmentManager.UpdateAward(getAward);
                        break;
                    case "RemoveOrganizationType":
                        OrganizationType getOrganizationType = ERecruitmentManager.GetOrganizationType(Utils.GetQueryString<string>("code"));
                        getOrganizationType.Active = false;
                        ERecruitmentManager.UpdateOrganizationType(getOrganizationType);
                        break;
                    case "RemoveOrganizationPosition":
                        OrganizationPosition getOrgPosition = ERecruitmentManager.GetOrganizationPosition(Utils.GetQueryString<string>("code"));
                        getOrgPosition.Active = false;
                        ERecruitmentManager.UpdateOrganizationPosition(getOrgPosition);
                        break;
                }
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 666;
                context.Response.ContentType = "text/plain";
                context.Response.Write(Utils.ExtractInnerExceptionMessage(e));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}