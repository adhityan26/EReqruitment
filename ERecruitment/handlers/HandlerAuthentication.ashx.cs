﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Security;
using System.Web.SessionState;
using System.Globalization;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Reflection;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerAuthentication
    /// </summary>
    public class HandlerAuthentication : System.Web.IHttpHandler, IRequiresSessionState
    {
        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (HttpContext.Current.Session["maincompany"] != null)
                    mainCompany = HttpContext.Current.Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.ToString());
                    string host = myUri.Host;
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(host);
                    HttpContext.Current.Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            Applicants applicant;
            UserAccounts userAccount;

            try
            {
                switch (commandName)
                {
                    case "loginsosmed":
                        //applicant = ApplicantManager.GetApplicantBySocmedID(Utils.GetQueryString<string>("appName"), Utils.GetQueryString<string>("id"));
                        string loginSosmed = Utils.GetQueryString<string>("appName");
                        string email = Utils.GetQueryString<string>("email");
                        string sosmedId = Utils.GetQueryString<string>("id");
//                        bool verifiedU = true;
                        string isRegis = "true";

                        applicant = ApplicantManager.GetVerifiedActiveApplicantByEmailAddress(email);

                        if (applicant == null)
                        {
                            context.Response.Write("notregistered");
                        }
                        else
                        {
                            // login user
                            isRegis = "false";
                            userAccount = Domain.AuthenticationManager.GetApplicantUserAccount(applicant.Code);
                            if (userAccount == null)
                                throw new ApplicationException("invalid user");
                            else
                            {
                                string homePage = string.Empty;
                                if (userAccount.IsApplicant)
                                    if (Utils.GetQueryString<bool>("applyJob"))
                                        homePage = "";
                                    else
                                        homePage = "applicant/applicant-profile.aspx?isRegis=" + isRegis;
                                if (userAccount.IsAdmin)
                                    homePage = "recruiter/dashboard.aspx";
                                context.Session["currentuser"] = userAccount;
                                context.Session["roleCode"] = userAccount.RoleCode;
                                context.Response.Write(homePage);
                                userAccount.LastLogin = DateTime.Now;

                                if(applicant.SocMedName == "linkedin")
                                {
                                    applicant.LinkedInImageUrl = Utils.GetQueryString<string>("pictureUrl");
                                    applicant.LinkedInUrl = "http://www.linkedin.com/profile/view?id" + sosmedId;
                                    applicant.LinkedInID = sosmedId;
                                }
                                else
                                {
                                    applicant.FacebookID = sosmedId;
                                    applicant.FacebookUrl = "https://www.facebook.com/profile.php?id=" + sosmedId;

                                    var webclient = new WebClient();
                                    byte[] imagefb = webclient.DownloadData("http://graph.facebook.com/" + applicant.FacebookID + "/picture?type=large");
                                    applicant.Photo = imagefb;
                                    
                                }

                                applicant.IsRegis = isRegis;

                                Domain.AuthenticationManager.UpdateApplicant(applicant);
                                Domain.AuthenticationManager.UpdateUserAccountStamp(userAccount);
                            }
                        }
                        break;
                    case "regsosmed":
                        string loginSosmed2 = Utils.GetQueryString<string>("appName");

                        UserAccounts registerSosmedUserAccount = new UserAccounts();
                        Applicants registerSosmedApplicant = new Applicants();
                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            PropertyInfo propertyInfo;
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            if (name == "Password" || name == "Email")
                            {
                                propertyInfo = registerSosmedUserAccount.GetType().GetProperty(name);
                            }
                            else
                            {
                                propertyInfo = registerSosmedApplicant.GetType().GetProperty(name);
                            }
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);
                            //pwd-email-username
                            if (name == "Password" || name == "Email")
                            {
                                propertyInfo.SetValue(registerSosmedUserAccount, propertyValue, null);
                            }
                            else
                            {
                                propertyInfo.SetValue(registerSosmedApplicant, propertyValue, null);
                            }
                        }

                        if (loginSosmed2 == "facebook")
                        {
                            var webclient = new WebClient();
                            byte[] imageBytes = webclient.DownloadData("http://graph.facebook.com/" + registerSosmedApplicant.FacebookID + "/picture?type=large");
                            registerSosmedApplicant.Photo = imageBytes;
                        }

                        registerSosmedUserAccount.Code = Guid.NewGuid().ToString();
                        registerSosmedApplicant.Active = true;
                        registerSosmedApplicant.Address = "";
                        registerSosmedApplicant.Verified = true;
                        registerSosmedApplicant.Code = registerSosmedUserAccount.Code;
                        registerSosmedApplicant.Name = registerSosmedApplicant.FirstName.ToUpper() + " " + registerSosmedApplicant.LastName.ToUpper();
                        registerSosmedApplicant.RegisteredDate = DateTime.Now;

                        registerSosmedApplicant.SocMedID = registerSosmedApplicant.FacebookID;
                        registerSosmedApplicant.CompanyCode = MainCompany.Code;
                        registerSosmedApplicant.IsRegis = "true";
                        registerSosmedApplicant.Email = registerSosmedUserAccount.Email;
                        registerSosmedUserAccount.Active = true;
                        registerSosmedUserAccount.ApplicantCode = registerSosmedUserAccount.Code;
                        registerSosmedUserAccount.IsApplicant = true;
                        registerSosmedUserAccount.UserName = registerSosmedUserAccount.Email;
                        registerSosmedUserAccount.Name = registerSosmedApplicant.Name;
                        Domain.AuthenticationManager.SaveUserAccountSosmedRegistration(registerSosmedUserAccount, registerSosmedApplicant);
                        //ApplicantManager.SaveUpdateApplicantEducation(applicantEducation);

                        #endregion
                        break;
                    case "registersosmed":
                        isRegis = "false";
                        applicant = new Applicants();
                        applicant.Active = true;
                        applicant.Address = "";
                        applicant.Email = Utils.GetQueryString<string>("email");
                        applicant.Name = Utils.GetQueryString<string>("name");
                        applicant.RegisteredDate = DateTime.Now;
                        applicant.GenderSpecification = Utils.GetQueryString<string>("gender");
                        applicant.SocMedName = Utils.GetQueryString<string>("appName");
                        applicant.SocMedID = Utils.GetQueryString<string>("id");
                        applicant.IsRegis = isRegis;
                        context.Session.Add("newApplicant", applicant);

                        break;
                    case "validateCaptcha":
                          string captcha2 = Utils.GetQueryString<string>("captcha");
                        string keyValidator2 = context.Session["captcha"] as string;
                        string isValidateCaptcha = "0";
                        if (captcha2 != keyValidator2)
                        {
                            isValidateCaptcha = "0";
                        }
                        else
                            isValidateCaptcha = "1";
                        context.Response.Write(isValidateCaptcha);
                        break;
                    case "validateEmail":
                        string emailUser = Utils.GetQueryString<string>("email");
                        string isValidateEmail = "0";
                        UserAccounts user2 = new UserAccounts();
                        user2 = Domain.AuthenticationManager.GetUserAccountByEmail(emailUser);
                        if (user2 != null)
                        {
                            isValidateEmail = "0";
                        }
                        else
                        {
                            isValidateEmail = "1";
                        }
                        context.Response.Write(isValidateEmail);
                        break;
                    case "register":
                      
                        UserAccounts registerUserAccount = new UserAccounts();

                        Applicants registerApplicant = new Applicants();
                        foreach (string name in context.Request.Form.Keys)
                        {
                            PropertyInfo propertyInfo;
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            if (name == "Password" || name == "Email")
                            {
                                propertyInfo = registerUserAccount.GetType().GetProperty(name);
                            }
                            else
                            {
                                propertyInfo = registerApplicant.GetType().GetProperty(name);
                            }
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);
                            //pwd-email-username
                            if (name == "Password" || name == "Email")
                            {
                                propertyInfo.SetValue(registerUserAccount, propertyValue, null);
                            }
                            else
                            {
                                propertyInfo.SetValue(registerApplicant, propertyValue, null);
                            }
                        }

                        //UserAccounts user = new UserAccounts();
                        //user = Domain.AuthenticationManager.GetUserAccountByEmail(registerUserAccount.Email);
                        //if (user != null)
                        //{
                        //    throw new ApplicationException("This email already registered!");
                        //}
                        //throw an exception if user already registered
                        // create user and external account
                        registerUserAccount.Code = Guid.NewGuid().ToString();
                        registerApplicant.Active = true;
                        registerApplicant.Address = "";
                        registerApplicant.Verified = false;
                        registerApplicant.Code = registerUserAccount.Code;
                        registerApplicant.Name = registerApplicant.FirstName.ToUpper() + " " + registerApplicant.LastName.ToUpper();
                        registerApplicant.RegisteredDate = DateTime.Now;

                        registerApplicant.CompanyCode = MainCompany.Code;
                        registerApplicant.IsRegis = "false";
                        registerApplicant.Email = registerUserAccount.Email;
                        registerUserAccount.Active = true;
                        registerUserAccount.ApplicantCode = registerUserAccount.Code;
                        registerUserAccount.IsApplicant = true;
                        registerUserAccount.UserName = registerUserAccount.Email;
                        registerUserAccount.Name = registerApplicant.Name;
                        Domain.AuthenticationManager.SaveUserAccountSosmedRegistration(registerUserAccount, registerApplicant);
                 
                            //send email registration
                            if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
                            {
                                Companies emailSetting = ERecruitmentManager.GetCompany(MainCompany.Code);
                                SmtpClient client = new SmtpClient();
                                client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                                client.Host = emailSetting.EmailSmtpHostAddress;
                                if (emailSetting.EmailSmtpUseSSL)
                                    client.EnableSsl = true;
                                client.Timeout = 20000;
                                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                                client.UseDefaultCredentials = false;
                                client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                                string address = emailSetting.EmailSmtpEmailAddress;
                                string displayName = emailSetting.MailSender;

                                if (!address.Contains("@"))
                                {
                                    address = displayName;
                                }
            
                                MailMessage mm = new MailMessage(new MailAddress(address,
                                        displayName),
                                                                                    new MailAddress(registerApplicant.Email, ""));
                                mm.Subject = "Email verification from " + emailSetting.ApplicationTitle;
                                string body = @"<div style=padding:10px>
                                <div style='width:100%;border:solid 1px #A6C9E2;background-color:#ffffff'>
                             
                                    <table>
                                        <tr><td>Dear [candidate],</td></tr>
                                    </table> 
                                    <table>
                                        <tr><td>Welcome to E-Recruitment Application</td></tr>
                                        <tr>
                                                <td>Your registration is completed, please activate your account within 1x24 hours by clicking the link below:</td>
                                        </tr>
                                        <tr>
                                            <td><a href=[link]>Activate my account</a></td>
                                        </tr>      
                                        <tr>
                                            <td>If you found this email in your Trash/Spam Folder, please click “NOT SPAM/JUNK” in order to ensure that our email not delivered to your Trash/Spam Folder.</td>
                                        </tr>
                                        <tr><td></td></tr>
                                        <tr>
                                            <td>Regards,</td>
                                        </tr>
                                            <tr><td></td></tr>
                                            <tr><td></td></tr>
                                            <tr><td>Recruitment Team</td></tr>
                                    </table> 
                                </div>
                                </div>
                                ";
                                mm.Body = body;
                                string link = MainCompany.ATSBaseUrl + "/applicant/emailverificationcompleted.aspx?code=" + registerApplicant.Code;
                                mm.Body = mm.Body.Replace("[link]", link).Replace("[candidate]", registerApplicant.FirstName);
                                mm.BodyEncoding = UTF8Encoding.UTF8;
                                mm.IsBodyHtml = true;
                                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                                client.Send(mm);
                            }

                            context.Response.Write(MainCompany.ATSBaseUrl + "default.aspx?registerd=true");
                        
                        break;
                    case "logout":
                        context.Session.Remove("currentuser");
                        break;
                    case "login":
                        isRegis = "false";
                        bool verified = true;
                        string emailLogin = Utils.GetQueryString<string>("email");
                        string password = Utils.GetQueryString<string>("password");
                        UserAccounts loginUser = Domain.AuthenticationManager.AuthenticateUser(emailLogin, password);
                        if (loginUser == null) {
                            UserAccounts chkUsername = Domain.AuthenticationManager.CheckUserNameUser(emailLogin);
                            if (chkUsername != null)
                            {
                                throw new ApplicationException("wrong password");
                            }
                            else
                            {
                                throw new ApplicationException("invalid username or password");
                            }
                        }
                        else
                        {
                            UserAccounts chkUsernameisActive = Domain.AuthenticationManager.CheckUserNameUserisActive(emailLogin);
                            if (chkUsernameisActive == null)
                            {
                                throw new ApplicationException("Login was unsuccessful due to inactive user");
                            }
                            context.Session["roleCode"] = loginUser.RoleCode;
                            string homePage = "recruiter/dashboard.aspx";
                            if (loginUser.IsApplicant)
                            {
                                applicant = ApplicantManager.GetApplicant(loginUser.ApplicantCode);
                                if (applicant.Verified)
                                {
                                    homePage = "applicant/applicant-profile.aspx?isRegis=" + isRegis;
                                    loginUser.LastLogin = DateTime.Now;
                                    applicant.IsRegis = isRegis;
                                    Domain.AuthenticationManager.UpdateApplicant(applicant);
                                }
                                else
                                {
                                    throw new ApplicationException("Login was unsuccessful due to un-verified user");
//                                    homePage = "applicant/registrationcompleted.aspx";
                                }
                                verified = applicant.Verified;   
                            }
                            if (loginUser.IsRecruiter)
                            {
                                homePage = "recruiter/dashboard.aspx";
                                loginUser.LastLogin = DateTime.Now;
                            }
                            if (loginUser.IsAdmin)
                            {
                                homePage = "recruiter/dashboard.aspx";
                                loginUser.LastLogin = DateTime.Now;
                            }

                            if (context.Session["nextPage"] != null)
                            {
                                homePage = context.Session["nextPage"].ToString();
                                context.Session.Remove("nextPage");
                            }

                            #region consolidate roles

                            if (!string.IsNullOrEmpty(loginUser.RoleCode))
                            {
                                string[] userCompanyCodes = loginUser.RoleCode.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                List<ERecruitment.Domain.Roles> roleList = ERecruitmentManager.GetRoleList(userCompanyCodes);

                                ERecruitment.Domain.Roles mainRole = new ERecruitment.Domain.Roles();

                                foreach (ERecruitment.Domain.Roles role in roleList)
                                {
                                    if (role.AccessCSSTheme)
                                        mainRole.AccessCSSTheme = role.AccessCSSTheme;

                                    if (role.AccessSetting)
                                        mainRole.AccessSetting = role.AccessSetting;

                                    if (role.AccessSetup)
                                        mainRole.AccessSetup = role.AccessSetup;

                                    if (role.AccessReport)
                                        mainRole.AccessReport = role.AccessReport;

                                    if (role.AccessApplicant)
                                        mainRole.AccessApplicant = role.AccessApplicant;

                                    if (role.CreateJob)
                                        mainRole.CreateJob = role.CreateJob;
                                    
                                    if (role.ArchiveJob)
                                        mainRole.ArchiveJob = role.ArchiveJob;

                                    if (role.PostUnPostJob)
                                        mainRole.PostUnPostJob = role.PostUnPostJob;

                                    if (role.DeleteJob)
                                        mainRole.DeleteJob = role.DeleteJob;

                                    if (role.SchedulingFunctionality)
                                        mainRole.SchedulingFunctionality = role.SchedulingFunctionality;

                                    if (role.OfferingFunctionality)
                                        mainRole.OfferingFunctionality = role.OfferingFunctionality;

                                    if (role.RankFunctionality)
                                        mainRole.RankFunctionality = role.RankFunctionality;

                                    if (role.ReferFunctionality)
                                        mainRole.ReferFunctionality = role.ReferFunctionality;

                                    if (role.FlagFunctionality)
                                        mainRole.FlagFunctionality = role.FlagFunctionality;

                                    if (role.DisqualifyFunctionality)
                                        mainRole.DisqualifyFunctionality = role.DisqualifyFunctionality;

                                    if (role.DownloadDashboardData)
                                        mainRole.DownloadDashboardData = role.DownloadDashboardData;

                                    if (role.ChangeApplicantStatus)
                                        mainRole.ChangeApplicantStatus = role.ChangeApplicantStatus;

                                    if (role.SearchApplicant)
                                        mainRole.SearchApplicant = role.SearchApplicant;

                                    if (role.AccessCompensationData)
                                        mainRole.AccessCompensationData = role.AccessCompensationData;

                                    if (role.Active)
                                        mainRole.Active = role.Active;

                                    if (role.Approval)
                                        mainRole.Approval = role.Approval;

                                    if (role.ExportBulkData)
                                        mainRole.ExportBulkData = role.ExportBulkData;

                                    if (role.ImportBulkData)
                                        mainRole.ImportBulkData = role.ImportBulkData;

                                    if (role.MassScheduling)
                                        mainRole.MassScheduling = role.MassScheduling;

                                    if (role.UpdateApplicantApplicationData)
                                        mainRole.UpdateApplicantApplicationData = role.UpdateApplicantApplicationData;

                                    mainRole.CompanyCodes += "," + role.CompanyCodes;
                                }

                                string[] companyAccessCodes = mainRole.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                                bool accessCompanyAllowed = false;
                                for (int i = 0; i < companyAccessCodes.Length; i++)
                                    if (MainCompany.Code == companyAccessCodes[i])
                                    {
                                        accessCompanyAllowed = true;
                                        break;
                                    }

                                if (!accessCompanyAllowed)
                                {
                                    throw new ApplicationException("access company not allowed");
                                }

                                context.Session["mainrole"] = mainRole;
                            }

                            #endregion

                            Domain.AuthenticationManager.UpdateUserAccountStamp(loginUser);
                            if (verified)
                                context.Session["currentuser"] = loginUser;
                            else
                                context.Session["unverifieduser"] = loginUser;

                            if (loginUser.IsAdmin)
                                context.Response.Write(MainCompany.ATSBaseUrl + homePage);
                            else
                            {
                                if (Utils.GetQueryString<bool>("applyJob"))
                                    context.Response.Write(string.Empty);
                                else
                                    context.Response.Write(MainCompany.ATSBaseUrl + homePage);
                            }
                        }
                        break;
                }
            }
            catch (ApplicationException aex)
            {
                context.Response.StatusCode = 422;
                context.Response.Write(Utils.ExtractInnerExceptionMessage(aex));
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 500;
                context.Response.Write(Utils.ExtractInnerExceptionMessage(e));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}