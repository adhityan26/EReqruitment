﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Script.Serialization;
using System.Web.SessionState;
using System.Reflection;
using System.IO;
using System.Text.RegularExpressions;
using KG.SISDM;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerVacancies
    /// </summary>
    public class HandlerVacancies : IHttpHandler, IRequiresSessionState
    {

        public UserAccounts CurrentUser
        {
            get
            {
                return HttpContext.Current.Session["currentuser"] as UserAccounts;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            List<ApplicantVacancies> vacancyApplicantList = new List<ApplicantVacancies>();
            List<ApplicantVacancyComments> vacancyCommentList = new List<ApplicantVacancyComments>();

            ApplicantVacancyComments vacancyComment = new ApplicantVacancyComments();
            List<ApplicantVacancyTestResults> testResultList = new List<ApplicantVacancyTestResults>();


            ApplicantVacancyOffering vacancyOffering = new ApplicantVacancyOffering();
            List<ApplicantVacancyOffering> vacancyOfferingList = new List<ApplicantVacancyOffering>();

            ApplicantVacancySchedules invitation = new ApplicantVacancySchedules();
            ApplicantVacancyReferences applicantReference = new ApplicantVacancyReferences();

            ApplicantVacancies application = new ApplicantVacancies();
            try
            {
                switch (commandName)
                {
                    case "SynchronizePeerJobs":
                        string result;
                        result = ERecruitmentManager.SynchronizeOpenVacanciesFromSISDM(Utils.GetQueryString<string>("CompanyCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(result);
                        break;
                    case "GetVacancyApplicant":
                        application = VacancyManager.GetApplicantVacancy(Utils.GetQueryString<string>("vacancyCode"),
                                                                         Utils.GetQueryString<string>("applicantCode"));
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(application));
                        break;
                    case "PostAdvertisement":
                        string[] postJobCodes = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < postJobCodes.Length; i++)
                        {
                            Vacancies job = ERecruitmentManager.GetVacancy(postJobCodes[i]);
                            job.UpdatedBy = CurrentUser.Code;
                            job.UpdateStamp = DateTime.Now;
                            ERecruitmentManager.PostAdvertisement(job, DateTime.Now, job.ExpiredPostDate);
                        }
                        break;
                    case "DeleteJob":
                        string[] deleteJobCodes = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < deleteJobCodes.Length; i++)
                        {
                            Vacancies job = ERecruitmentManager.GetVacancy(deleteJobCodes[i]);
                            job.UpdatedBy = CurrentUser.Code;
                            job.UpdateStamp = DateTime.Now;
                            ERecruitmentManager.RemoveVacancy(job);
                        }
                        break;
                    case "UnpostAdvertisement":
                        string[] unpostJobCodes = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < unpostJobCodes.Length; i++)
                        {
                            Vacancies job = ERecruitmentManager.GetVacancy(unpostJobCodes[i]);
                            job.UpdatedBy = CurrentUser.Code;
                            job.UpdateStamp = DateTime.Now;
                            ERecruitmentManager.UnPostAdvertisement(job);
                        }
                        break;
                    case "CloseJob":
                        string[] closeJobCodes = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < closeJobCodes.Length; i++)
                        {
                            Vacancies job = ERecruitmentManager.GetVacancy(closeJobCodes[i]);
                            job.UpdatedBy = CurrentUser.Code;
                            job.UpdateStamp = DateTime.Now;
                            ERecruitmentManager.CloseVacancy(job);
                        }
                        break;
                    case "ApplySurvay":
                        ApplicantVacancies apl = new ApplicantVacancies();
                        String applicantCode = Utils.GetQueryString<string>("applicantCode");
                        String vacancyCode = Utils.GetQueryString<string>("vacancyCode");
                        String sourceResource = Utils.GetQueryString<string>("sourceResource");
                        apl.ApplicantCode = applicantCode;
                        apl.VacancyCode = vacancyCode;
                        apl.SourceReferencesName = sourceResource;
                        VacancyManager.SaveSurvay(apl);
                        break;
                    case "ApplyJob":

                        Vacancies vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));
                        ApplicantVacancies newApplication = new ApplicantVacancies();
                        newApplication.AppliedDate = DateTime.Now;
                        newApplication.CompanyCode = vacancy.CompanyCode;
                        newApplication.Active = true;
                        newApplication.UpdateStamp = DateTime.Now;
                        newApplication.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        newApplication.VacancyCode = Utils.GetQueryString<string>("vacancyCode");

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];

                            ApplicantVacancyDisqualificationAnswers disqualifierAnswer = new ApplicantVacancyDisqualificationAnswers();
                            disqualifierAnswer.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                            disqualifierAnswer.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                            disqualifierAnswer.QuestionCode = name;
                            disqualifierAnswer.Answer = propertyValue.ToString();

                            newApplication.DisqualifierAnswerList.Add(disqualifierAnswer);
                        }

                        VacancyManager.SaveApplicantVacancy(newApplication);
                        // refresh session
                        newApplication = VacancyManager.GetApplicantVacancy(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("applicantCode"));
                        ERecruitmentAppSettings.SendApplicantUpdateStatus(newApplication);
                        Companies company = ERecruitmentManager.GetCompany(vacancy.CompanyCode);

                        if (company.SurveyQuestionCodes != null)
                        {
                            ApplicantSurveySessions surveySessions = new ApplicantSurveySessions();
                            surveySessions.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                            surveySessions.IssuedDate = newApplication.AppliedDate;
                            surveySessions.VacancyCode = newApplication.VacancyCode;
                            surveySessions.CompanyCode = company.Code;
                        
                            ERecruitmentManager.SaveApplicantSurveySession(surveySessions);   
                        }

                        break;
                    case "GetAppMode":
                        ApplicationSettings appSetting = ERecruitmentManager.GetApplicationSetting();
                        context.Response.ContentType = "text/plain";
                        context.Response.Write(appSetting.VacancyApplicationMode);
                        break;
                    case "CheckJobApplicant":
                        //Cek If Single Or Multi

                        ApplicationSettings applicationSetting = ERecruitmentManager.GetApplicationSetting();
                        if (applicationSetting.VacancyApplicationMode == "Single")
                        {
                            List<string> activeApplicationStatusList = VacancyManager.GetNonArchiveAndHiredRecruitmentProcessCodeList();

                            List<ApplicantVacancies> activeApplicantVacancyList = ERecruitmentManager.GetApplicantVacancyForCheckActiveApply(Utils.GetQueryString<string>("applicantCode"), activeApplicationStatusList.ToArray());
                            if (activeApplicantVacancyList.Count > 0)
                            {
                                throw new ApplicationException("You can't apply again, until active application you already applied on Kompas Gramedia Group is done.");
                            }
                        }

                        ApplicantVacancies applicantVacancy = VacancyManager.GetApplicantVacancy(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("applicantCode"));

                        if (applicantVacancy != null)
                            throw new ApplicationException("Vacancy already applied");
                        break;

                    case "GetApplicantVacancyCommentList":
                        vacancyCommentList = VacancyManager.GetVacancyApplicantCommentList(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(vacancyCommentList));
                        break;
                    case "GetApplicantTestResultList":

                        VacancyManager.InitiateApplicantTest(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("applicantCode"));
                        testResultList = VacancyManager.GetApplicantTestResultList(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("applicantCode"));

                        context.Response.ContentType = "text/plain";
                        context.Response.Write(new JavaScriptSerializer().Serialize(testResultList));
                        break;
                    case "AddComments":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = vacancyComment.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(vacancyComment, propertyValue, null);
                        }

                        vacancyComment.CommentBy = Utils.GetQueryString<string>("commentByCode");
                        vacancyComment.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                        vacancyComment.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        vacancyComment.Date = DateTime.Now;
                        VacancyManager.SaveUpdateApplicantComment(vacancyComment);

                        #endregion

                        break;
                    case "SubmitOffering":

                        #region filling model with data

                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = vacancyOffering.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(vacancyOffering, propertyValue, null);
                        }

                        vacancyOffering.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                        vacancyOffering.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        vacancyOffering.CreatedBy = Utils.GetQueryString<string>("issuedByCode");
                        vacancyOffering.CreatedDate = DateTime.Now;
                        vacancyOffering.EffectiveDate = DateTime.Now;
                        VacancyManager.SaveUpdateApplicantOffering(vacancyOffering);
                        ERecruitmentAppSettings.SendApplicantOfferingEmail(vacancyOffering, Utils.GetQueryString<int>("emailTemplateId"));

                        #endregion

                        break;
                    case "SubmitInvitation":
                        #region filling model with data
                        foreach (string name in context.Request.Form.Keys)
                        {
                            object propertyValue = context.Request.Form[name];
                            if (string.IsNullOrEmpty(propertyValue.ToString()))
                                continue;
                            // get property definition
                            PropertyInfo propertyInfo = invitation.GetType().GetProperty(name);
                            // assign property
                            Type propertyType = propertyInfo.PropertyType;
                            if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
                                propertyValue = DateTime.ParseExact(propertyValue.ToString(), "dd/MM/yyyy", null);
                            else
                                propertyValue = Convert.ChangeType(propertyValue, propertyType);

                            propertyInfo.SetValue(invitation, propertyValue, null);
                        }

                        invitation.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                        invitation.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                        invitation.CreatedBy = Utils.GetQueryString<string>("issuedByCode");
                        invitation.CreatedDate = DateTime.Now;
                        VacancyManager.SaveUpdateApplicantInvitation(invitation);

                        ERecruitmentAppSettings.SendApplicantInvitationEmail(invitation, Utils.GetQueryString<int>("emailTemplateId"));

                        #endregion

                        break;
                    case "ReferApplicant":
                        int totalRefer = ApplicantManager.CountReferencedApplicantListBasedAppCode(Utils.GetQueryString<string>("applicantCode"));
                        if (totalRefer < 2) {
                            string[] listCompany = Utils.GetQueryString<string>("companyCodes").Split(',');
                            int totalReferAll = totalRefer + listCompany.Length;
                            if (totalReferAll <= 2)
                            {
                                applicantVacancy = VacancyManager.GetApplicantVacancy(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("applicantCode"));
                                applicantVacancy.UpdatedBy = Utils.GetQueryString<string>("issuedByCode");
                                applicantVacancy.UpdateStamp = DateTime.Now;
                                VacancyManager.ReferApplicantVacancy(applicantVacancy, listCompany, Utils.GetQueryString<string>("comments"));
                                ERecruitmentAppSettings.SendApplicantReferer(applicantVacancy, listCompany, Utils.GetQueryString<int>("emailTemplateId"));
                            }
                            else
                            {
                                throw new ApplicationException("Can't refer applicant for more than 2 times.");
                            }
                        
                       }
                        else
                        {
                            throw new ApplicationException("This Applicant has been refered for " + totalRefer + " times. You can't refer this applicant again.");
                        }
                        break;
                    case "SubmitApplicantTestResult":

                        #region filling model data

                        foreach (string testCode in context.Request.Form.Keys)
                        {
                            object testValue = context.Request.Form[testCode];
                            ApplicantVacancyTestResults testResult = VacancyManager.GetApplicantTestResult(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("applicantCode"), testCode);
                            if (testResult == null)
                            {
                                testResult = new ApplicantVacancyTestResults();
                                testResult.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                                testResult.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                                testResult.TestCode = testCode;
                                testResult.IsNew = true;
                            }

                            testResult.Result = testValue.ToString();

                            VacancyManager.SaveUpdateApplicantTestResult(testResult);
                        }

                        #endregion

                        break;
                    case "ListAppointment":
                        String companyCode = Utils.GetQueryString<string>("companyCode");
//                        Dictionary<int, Dictionary<string, object>> column = new Dictionary<int, Dictionary<string, object>>();
//                        Dictionary<int, Dictionary<string, object>> orderParams = new Dictionary<int, Dictionary<string, object>>();
                        Dictionary<String, object> queryParam = new Dictionary<String, object>();

                        foreach (String key in HttpContext.Current.Request.Form)
                        {
                            MatchCollection matches = Regex.Matches(key, @"\[.+?\]");
                            MatchCollection matchKey = Regex.Matches(key, @"^.+?\[");
                            string indexParam = "";
                            if (matchKey.Count > 0)
                            {
                                indexParam = matchKey[0].Value.Replace("[", "");
                            }
                            else
                            {
                                indexParam = key;
                            }

                            queryParam = prosesQueryForm(queryParam, matches, HttpContext.Current.Request.Form, indexParam, key, 0);
                        }
                        
                        String draw = queryParam["draw"].ToString();
                        String filter = "%" + queryParam["filter"] + "%";
                        String dateFrom = queryParam["dateFrom"].ToString();
                        String dateTo = queryParam["dateTo"].ToString();
                        Dictionary<String, DateTime> dateParam = new Dictionary<String, DateTime>();

                        if (!String.IsNullOrEmpty(dateFrom))
                        {
                            dateParam.Add("DateFrom", DateTime.ParseExact(dateFrom + " 00:00:00", "dd/MM/yyyy HH:mm:ss",
                                System.Globalization.CultureInfo.InvariantCulture));
                        }
                        
                        if (!String.IsNullOrEmpty(dateTo))
                        {
                            dateParam.Add("DateTo", DateTime.ParseExact(dateTo + " 23:59:59", "dd/MM/yyyy HH:mm:ss",
                                System.Globalization.CultureInfo.InvariantCulture));
                        }
                        
                        decimal length = Decimal.Parse(queryParam["length"].ToString());
                        if (length == 0)
                        {
                            length = 10;
                        }
                        decimal start = Decimal.Parse(queryParam["start"].ToString());
                        Dictionary<String, String> orders = new Dictionary<string, string>();

                        foreach (var order in (Dictionary<String, object>) queryParam["order"])
                        {
                            Dictionary<String, object> o = (Dictionary<String, object>) order.Value;
                            Dictionary<String, object> c = (Dictionary<String, object>) queryParam["columns"];
                            Dictionary<String, object> cData = (Dictionary<String, object>) c[o["column"].ToString()];
                            orders.Add(cData["data"].ToString(), o["dir"].ToString());
                        }
                        
                        int page = Decimal.ToInt32((Math.Floor(start/length))) + 1;
                        int totalRecord = HiringManager.CountSchedulesSummaryByCompany(companyCode, filter, dateParam);
                        var listData = HiringManager.GetSchedulesSummaryByCompany(companyCode, Decimal.ToInt32(start), Decimal.ToInt32(length), filter, orders, dateParam);
                        
                        Dictionary<String, object> returnValue = new Dictionary<String, object>();
                        
                        returnValue.Add("draw", draw);
                        returnValue.Add("recordsTotal", totalRecord);
                        returnValue.Add("recordsFiltered", totalRecord);
                        returnValue.Add("data", listData.ToArray());
                        
                        context.Response.ContentType = "text/json";
                        context.Response.Write(new JavaScriptSerializer().Serialize(returnValue));
                        break;
                    case "DeleteAppointment":
                        int id = Utils.GetQueryString<int>("id");
                        HiringManager.DeleteSchedule(id);
                        
                        context.Response.ContentType = "text/json";
                        context.Response.Write(1);
                        break;
                }
            }
            catch (Exception e)
            {
                context.Response.StatusCode = 666;
                context.Response.ContentType = "text/plain";
                context.Response.Write(Utils.ExtractInnerExceptionMessage(e));
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        private Dictionary<String, object> prosesQueryForm(Dictionary<String, object> queryParam, MatchCollection matches, NameValueCollection form, string indexParam, string key, int iteration)
        {
            if (matches.Count > iteration)
            {
                Dictionary<String, object> param = new Dictionary<string, object>();
                if (queryParam.ContainsKey(indexParam))
                {
                    param = (Dictionary<String, object>) queryParam[indexParam];
                }
                else
                {
                    queryParam.Add(indexParam, param);
                }

                param = prosesQueryForm(param, matches, form, matches[iteration].Value.Replace("[", "").Replace("]", ""), key, iteration + 1);
            }
            else
            {
                queryParam.Add(indexParam, form[key]);
            }
            
            return queryParam;
        }
    }
}