﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.Security;
using System.Web.SessionState;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerUI
    /// </summary>
    public class HandlerUI : IHttpHandler, IRequiresSessionState
    {

        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (HttpContext.Current.Session["maincompany"] != null)
                    mainCompany = HttpContext.Current.Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(HttpContext.Current.Request.Url.Authority);
                    HttpContext.Current.Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            Companies company = null;
            Applicants applicant = null;
            switch(commandName)
            {
                case "GetApplicantPicture":
                    applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("code"));
                    if (applicant.Photo != null)
                    {
                        byte[] streams = applicant.Photo;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;

                case "GetCompanyLogo":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.Logo != null)
                    {
                        byte[] streams = company.Logo;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                    
                case "GetCompanyFavicon":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.FaviconLogo != null)
                    {
                        byte[] streams = company.FaviconLogo;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;

                case "GetMainSiteLogo":
                    if(MainCompany.Logo != null)
                    {
                        byte[] streams = MainCompany.Logo;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetMainSiteFavicon":
                    if (MainCompany.FaviconLogo != null)
                    {
                        byte[] streams = MainCompany.FaviconLogo;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetMainSiteCoverImage":
                    if (MainCompany.CoverImage != null)
                    {
                        byte[] streams = MainCompany.CoverImage;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetMainSiteCoverImage1":
                    if (MainCompany.CoverImage1 != null)
                    {
                        byte[] streams = MainCompany.CoverImage1;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetMainSiteCoverImage2":
                    if (MainCompany.CoverImage2 != null)
                    {
                        byte[] streams = MainCompany.CoverImage2;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetMainSiteCoverImage3":
                    if (MainCompany.CoverImage3 != null)
                    {
                        byte[] streams = MainCompany.CoverImage3;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetMainSiteCoverImage4":
                    if (MainCompany.CoverImage4 != null)
                    {
                        byte[] streams = MainCompany.CoverImage4;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetMainSiteAboutUsImage":
                    if (MainCompany.AboutUsImage != null)
                    {
                        byte[] streams = MainCompany.AboutUsImage;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;


                case "GetCompanySiteLogo":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.Logo != null)
                    {
                        byte[] streams = company.Logo;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetCompanySiteFavicon":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.FaviconLogo != null)
                    {
                        byte[] streams = company.FaviconLogo;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetCompanySiteCoverImage":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.CoverImage != null)
                    {
                        byte[] streams = company.CoverImage;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetCompanySiteCoverImage1":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.CoverImage1 != null)
                    {
                        byte[] streams = company.CoverImage1;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetCompanySiteCoverImage2":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.CoverImage2 != null)
                    {
                        byte[] streams = company.CoverImage2;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetCompanySiteCoverImage3":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.CoverImage3 != null)
                    {
                        byte[] streams = company.CoverImage3;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetCompanySiteCoverImage4":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.CoverImage4 != null)
                    {
                        byte[] streams = company.CoverImage4;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
                case "GetCompanySiteAboutUsImage":
                    company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
                    if (company.AboutUsImage != null)
                    {
                        byte[] streams = company.AboutUsImage;
                        if (streams != null)
                            context.Response.OutputStream.Write(streams, 0, streams.Length);
                    }
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}