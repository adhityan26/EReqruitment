﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using System.IO;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class downloadimportschema : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Clear();

            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
            {
                BatchProcessSchemas procesSchema = BatchProcessManager.GetBatchProcessSchemaByCode(Utils.GetQueryString<string>("code"));
                if (procesSchema != null)
                {

                    TextWriter fileWriter = new StreamWriter(Response.OutputStream);
                    string vacancyCode = Utils.GetQueryString<string>("vacancyCode");
                    string status = Utils.GetQueryString<string>("status");
                    List<string> dataList = procesSchema.GenerateImportSchema(vacancyCode, status);
                    string fileName = procesSchema.Code + ". " + procesSchema.Name + " (" + vacancyCode + ")_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";

                    foreach (string line in dataList)
                        fileWriter.WriteLine(line);

                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                    Response.ContentType = "application/csv"; ;
                    fileWriter.Close();
                    fileWriter.Dispose();

                    Response.End();
                }
            }
        }
    }
}