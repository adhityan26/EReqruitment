﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerDataTableVacancies
    /// </summary>
    public class HandlerDataTableVacancies : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            JqueryDataTable dt;
            int count;
            int sortCol = Utils.GetQueryString<int>(JqueryDataTable.iSortingCols);
            string sortDir = Utils.GetQueryString<string>(JqueryDataTable.sSortDir);
            string sortingProperty = string.Empty;
            List<string> dataList = new List<string>();
            string categoryCode = string.Empty;

            List<string> companyStringList = new List<string>();
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("companyCode")))
                companyStringList = new List<string>(Utils.GetQueryString<string>("companyCode").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            List<string> divisionStringList = new List<string>();
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("divisionCode")) && Utils.GetQueryString<string>("divisionCode") != "null")
                divisionStringList = new List<string>(Utils.GetQueryString<string>("divisionCode").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            List<string> departmentStringList = new List<string>();
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("departmentCode")) && Utils.GetQueryString<string>("departmentCode") != "null")
                departmentStringList = new List<string>(Utils.GetQueryString<string>("departmentCode").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            List<string> statusStringList = new List<string>();
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("advStatus")))
                statusStringList = new List<string>(Utils.GetQueryString<string>("advStatus").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
            List<string> recruiterStringList = new List<string>();
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("recruiterCode")))
                recruiterStringList = new List<string>(Utils.GetQueryString<string>("recruiterCode").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));

            List<ApplicantVacancies> applicantVacancyList = new List<ApplicantVacancies>();
            List<ApplicantVacancySchedules> scheduleList = new List<ApplicantVacancySchedules>();
            List<Vacancies> vacancyList = new List<Vacancies>();
            switch (commandName)
            {
                case "GetPendingVacancyList":
                    vacancyList = ERecruitmentManager.GetPendingVacancyList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                    Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                    Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                    sortingProperty, sortDir);
                    count = ERecruitmentManager.CountPendingVacancyList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Vacancies item in vacancyList)
                    {
                        List<string> dataComponentList = new List<string>();
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.PositionName));
                        dataComponentList.Add(JqueryDataTable.StripData(Utils.FormatDateNumeric(item.InsertStamp)));
                        string linkApprove = "<i title='Approve Job' class=\"fa fa-check-square-o text-primary\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(linkApprove));
                        string linkReject = "<i title='Reject Job' class=\"fa fa-times text-primary\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(linkReject));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetApplicantName":
                    applicantVacancyList = ERecruitmentManager.GetApplicantNameListByVacancy(Utils.GetQueryString<string>("vacancyCode"),
                                                                                        Utils.GetQueryString<string>("status"),
                                                                        Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                        sortingProperty, sortDir);
                    count = ERecruitmentManager.CountApplicantNameListByVacancy(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>("status"),
                                                                             Utils.GetQueryString<string>(JqueryDataTable.sSearch));


                    foreach (ApplicantVacancies item in applicantVacancyList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string url = "../recruiter/detail-information-applicant.aspx?vacancyCode=" + item.VacancyCode + "&appCode=" + item.ApplicantCode + "&status=" + item.Status;
                        // string link = "<div style='cursor:pointer;' class='applicantName' id='"+item.ApplicantCode+"' onclick='window.location.href = '" + url + "''> '" + item.Applicant.Name + "</div>";
                        string link = @"<a href= '" + url + "' class='applicantName'  id='" + item.ApplicantCode + "' >" + item.ApplicantName + "</a>";
                        string check = "<input name=\"" + "U_cb\" type=\"radio\" />";
                        dataComponentList.Add(JqueryDataTable.StripData(check));
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ApplicantCode));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetApplicantDetailList":
                    applicantVacancyList = ERecruitmentManager.GetApplicantVacancyListByVacancy(Utils.GetQueryString<string>("vacancyCode"),
                                                                        Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                        sortingProperty, sortDir);
                    count = ERecruitmentManager.CountApplicantVacancyListByVacancy(Utils.GetQueryString<string>("vacancyCode"),
                                                                             Utils.GetQueryString<string>(JqueryDataTable.sSearch));


                    foreach (ApplicantVacancies item in applicantVacancyList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../recruiter/detail-information-applicant.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' class='applicantName' id='" + item.ApplicantCode + "' onclick='showDetail(\"" + item.ApplicantCode + "\");'> '" + item.ApplicantName + "</div>";
                        string check = "<input name=\"" + "U_" + string.Concat(item.ApplicantCode, "_", item.VacancyCode) + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ApplicantCode));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetJobRequisitionList":
                    sortingProperty = string.Empty;

                    ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
                    if (getSetting == null)
                    {
                        getSetting = new ApplicationSettings();
                        getSetting.JobAgeCalculationMethod = "PostDate";
                    }
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "PositionName";
                            break;
                        case 2:
                            sortingProperty = "Company.Name";
                            break;
                        case 3:
                            sortingProperty = "EmploymentType.Name";
                            break;
                        case 4:
                            sortingProperty = "JobCode";
                            break;
                        case 5:
                            sortingProperty = "HiringManager";
                            break;
                        case 6:
                            sortingProperty = "PostDate";
                            break;
                        case 7:
                            if (getSetting.JobAgeCalculationMethod == "PostDate")
                                sortingProperty = "AgeFromPostDate";
                            else
                                sortingProperty = "AgeFromCreationDate";
                            break;
                        case 8:
                            sortingProperty = "Status";
                            break;
                        case 9:
                            sortingProperty = "Applied";
                            break;
                    }

                    vacancyList = ERecruitmentManager.GetVacancyList(companyStringList.ToArray(), divisionStringList.ToArray(),
                                                                    departmentStringList.ToArray(), statusStringList.ToArray(),
                                                                     recruiterStringList.ToArray(), Utils.GetQueryString<string>("statusJob"),
                                                                     Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                     Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                     Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                     sortingProperty, sortDir);
                    count = ERecruitmentManager.CountVacancyList(companyStringList.ToArray(), divisionStringList.ToArray(),
                                                                    departmentStringList.ToArray(), statusStringList.ToArray(),
                                                                     recruiterStringList.ToArray(), Utils.GetQueryString<string>("statusJob"),
                                                                     Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Vacancies item in vacancyList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../recruiter/job-requisition-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' class='applicantName' id='" + item.Code + "' onclick='showDetail(\"" + item.Code + "\");'> '" + item.PositionName + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        if (!string.IsNullOrEmpty(item.CompanyCode))
                            dataComponentList.Add(JqueryDataTable.StripData(item.CompanyName));

                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        if (!string.IsNullOrEmpty(item.InsertedBy))
                        {
                            UserAccounts getUser = AuthenticationManager.GetUserAccount(item.InsertedBy);
                            if (getUser != null)
                                dataComponentList.Add(JqueryDataTable.StripData(getUser.Name));
                            else
                                dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        }
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));

                        dataComponentList.Add(JqueryDataTable.StripData(item.HiringManager));
                        string urlApplicant = "../recruiter/detail-application.aspx";
                        string linkApplicant = @"<a href= '" + urlApplicant + "?vacancyCode=" + item.Code + "'>" + item.Applied.ToString() + "</a>";

                        dataComponentList.Add(JqueryDataTable.StripData(linkApplicant));

                        dataComponentList.Add(JqueryDataTable.StripData(Utils.DisplayDoubleAmount(item.ProgressPercentage)));

                        if (getSetting.JobAgeCalculationMethod == "PostDate")
                            dataComponentList.Add(JqueryDataTable.StripData(item.AgeFromPostDate.ToString()));
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(item.AgeFromCreationDate.ToString()));

                        if (item.PostDate.HasValue)
                            dataComponentList.Add(JqueryDataTable.StripData(Utils.FormatDateNumeric(item.PostDate.Value)));
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        if (item.ExpiredPostDate.HasValue)
                            dataComponentList.Add(JqueryDataTable.StripData(Utils.FormatDateNumeric(item.ExpiredPostDate.Value)));
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Status));
                        dataComponentList.Add(JqueryDataTable.StripData(item.AdvertisementStatus));

                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetVacancyList":
                    categoryCode = Utils.GetQueryString<string>("categoryCode");
                    switch (sortCol)
                    {
                        case 0:
                            sortingProperty = "PositionName";
                            break;
                        case 1:
                            sortingProperty = "PostDate";
                            break;
                        case 2:
                            sortingProperty = "CategoryCode";
                            break;
                        case 3:
                            sortingProperty = "Location";
                            break;
                        case 4:
                            sortingProperty = "SalaryRangeTop";
                            break;

                    }
                    categoryCode = Utils.GetQueryString<string>("categoryCode");
                    vacancyList = ERecruitmentManager.SearchPostedVacancyList(categoryCode,
                                                                        Utils.GetQueryString<string>("locationCode"),
                                                                        0,
                                                                        0,
                                                                        DateTime.Now,
                                                                        Utils.GetQueryString<string>("q"),
                                                                        Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                        sortingProperty, sortDir);
                    count = ERecruitmentManager.CountSearchPostedVacancyList(Utils.GetQueryString<string>("categoryCode"),
                                                                 Utils.GetQueryString<string>("locationCode"),
                                                                 Utils.GetQueryString<decimal>("MinimalSalary"),
                                                                 Utils.GetQueryString<decimal>("MaximalSalary"),
                                                                 DateTime.Now,
                                                                 Utils.GetQueryString<string>("q"),
                                                                 Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Vacancies item in vacancyList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string url = "../public/job-detail.aspx";
                        string link = @"<a href= '" + url + "?vacancyCode=" + item.Code + "'>" + item.PositionName + "</a>";

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        if (item.PostDate.HasValue)
                            dataComponentList.Add(JqueryDataTable.StripData(Utils.FormatDateNumeric(item.PostDate.Value)));
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        if (!string.IsNullOrEmpty(item.CategoryCode))
                        {
                            Industries getIndustry = ERecruitmentManager.GetIndustry(item.CategoryCode);
                            if (getIndustry != null)
                                dataComponentList.Add(JqueryDataTable.StripData(getIndustry.Name));
                            else
                                dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        }
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        if (!string.IsNullOrEmpty(item.City))
                        {
                            City getCity = ERecruitmentManager.GetCityByCode(item.City);
                            if (getCity != null)
                                dataComponentList.Add(JqueryDataTable.StripData(getCity.CityName));
                            else
                                dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        }
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        if (item.IsShowSalary)
                            dataComponentList.Add(JqueryDataTable.StripData(Utils.DisplayMoneyAmount(item.SalaryRangeBottom) + " - " + Utils.DisplayMoneyAmount(item.SalaryRangeTop)));
                        else
                            dataComponentList.Add(JqueryDataTable.StripData("Salary Confidential"));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());


                    break;

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}