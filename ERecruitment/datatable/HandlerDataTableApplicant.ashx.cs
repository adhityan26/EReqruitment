﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerDataTableApplicant
    /// </summary>
    public class HandlerDataTableApplicant : IHttpHandler
    {
        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (HttpContext.Current.Session["maincompany"] != null)
                    mainCompany = HttpContext.Current.Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.ToString());
                    string host = myUri.Host;
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(host);
                    HttpContext.Current.Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            JqueryDataTable dt;
            int count;
            int sortCol = Utils.GetQueryString<int>(JqueryDataTable.iSortingCols);
            string sortDir = Utils.GetQueryString<string>(JqueryDataTable.sSortDir);
            string sortingProperty = string.Empty;
            List<string> dataList = new List<string>();
            byte[] attachmentContent = null;
            List<ApplicantEducations> applicantEducationList = new List<ApplicantEducations>();
            List<ApplicantExperiences> applicantExperienceList = new List<ApplicantExperiences>();
            List<ApplicantSkills> applicantSkillList = new List<ApplicantSkills>();
            List<ApplicantTrainings> applicantTrainingList = new List<ApplicantTrainings>();
            List<ApplicantAwards> applicantAwardList = new List<ApplicantAwards>();
            List<ApplicantVacancies> applicantVacancyList = new List<ApplicantVacancies>();
            switch (commandName)
            {
                case "GetDisqualifierAnswer":
                    List<ApplicantDisqualifierAnswers> applicantAnswerList = DisqualificationQuestionManager.GetApplicantDisqualifierAnswers(Utils.GetQueryString<string>("applicantCode"),
                                                                                                            Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                                                            Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                                                        sortingProperty, sortDir);
                    count = DisqualificationQuestionManager.CountApplicantDisqualifierAnswers(Utils.GetQueryString<string>("applicantCode"),
                                                                                              Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (ApplicantDisqualifierAnswers answer in applicantAnswerList)
                    {
                        List<string> dataAnswerList = new List<string>();
                        dataAnswerList.Add(JqueryDataTable.StripData(answer.DisqualifierQuestion.Question));
                        dataAnswerList.Add(JqueryDataTable.StripData(answer.Answer));
                        dataList.Add(string.Concat("[", string.Join(",", dataAnswerList.ToArray()), "]"));
                    }

                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetOtherAttachments":
                    List<ApplicantAttachments> otherAttachmentList = ERecruitmentManager.GetApplicantAttachmentList(Utils.GetQueryString<string>("applicantCode"),
                                                                                                      Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                                                    sortingProperty, sortDir);
                    count = ERecruitmentManager.CountApplicantAttachment(Utils.GetQueryString<string>("applicantCode"),
                                                                            Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (ApplicantAttachments attachment in otherAttachmentList)
                    {
                        List<string> dataAttachmentList = new List<string>();
                        string actionDeleteAttachment = "<i  name=\"" + attachment.Id.ToString() + "\" class=\"fa fa-trash-o attachment_delete\" title=\"Remove\"></i>";
                        dataAttachmentList.Add(JqueryDataTable.StripData(actionDeleteAttachment));

                        dataAttachmentList.Add(JqueryDataTable.StripData(attachment.FileName));


                        string handlerOtherAttachmentUrl =MainCompany.ATSBaseUrl + "/system/httphandlers/HandlerApplicants.ashx?commandName=DownloadOtherAttachment&id=" + attachment.Id.ToString();
                        string otherAttachmentUrl = "<a href='" + handlerOtherAttachmentUrl + "' > Download </a>";
                        dataAttachmentList.Add(JqueryDataTable.StripData(otherAttachmentUrl));

                        dataList.Add(string.Concat("[", string.Join(",", dataAttachmentList.ToArray()), "]"));
                    }

                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());


                    break;
                case "GetCurriculumVitaes":
                    Applicants applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("applicantCode"));
                    count = 1;

                    List<string> dataCVList = new List<string>();
                    string actionDeleteCV = "<i  name=\"" + applicant.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                    dataCVList.Add(JqueryDataTable.StripData(actionDeleteCV));

                    dataCVList.Add(JqueryDataTable.StripData(applicant.CVFileName));
                    if (applicant.CV != null)
                    {
                        string handlerUrlCV =MainCompany.ATSBaseUrl + "system/httphandlers/HandlerApplicants.ashx?commandName=DownloadCV&applicantCode=" + applicant.Code;
                        string cvUrl = "<a href='" + handlerUrlCV + "' > Download </a>";
                        dataCVList.Add(JqueryDataTable.StripData(cvUrl));
                    }
                    else
                        dataCVList.Add(JqueryDataTable.StripData(string.Empty));

                    dataList.Add(string.Concat("[", string.Join(",", dataCVList.ToArray()), "]"));


                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;

            }
            if (attachmentContent != null)
            {
                context.Response.OutputStream.Write(attachmentContent, 0, attachmentContent.Length);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}