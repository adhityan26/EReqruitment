﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.SessionState;
using System.Text;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerDataTableSettings
    /// </summary>
    public class HandlerDataTableSettings : IHttpHandler, IRequiresSessionState
    {
        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (HttpContext.Current.Session["maincompany"] != null)
                    mainCompany = HttpContext.Current.Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.ToString());
                    string host = myUri.Host;
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(host);
                    HttpContext.Current.Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            JqueryDataTable dt;
            int count;
            int sortCol = Utils.GetQueryString<int>(JqueryDataTable.iSortingCols);
            string sortDir = Utils.GetQueryString<string>(JqueryDataTable.sSortDir);
            string sortingProperty = string.Empty;
            List<string> dataList = new List<string>();
            List<Industries> industryList = new List<Industries>();
            List<Positions> positionList = new List<Positions>();
            List<EmploymentStatus> empStatusList = new List<EmploymentStatus>();
            List<EducationLevels> educationLevelList = new List<EducationLevels>();
            List<EducationFieldOfStudy> fieldOfStudyList = new List<EducationFieldOfStudy>();
            List<EducationMajor> educationMajorList = new List<EducationMajor>();
            List<EducationMappingHeader> educationMappingHeaderList = new List<EducationMappingHeader>();
            List<Languages> languageList = new List<Languages>();
            List<Awards> awardList = new List<Awards>();
            List<OrganizationType> orgTypeList = new List<OrganizationType>();
            List<Companies> companyList = new List<Companies>();
            List<VacancyStatus> statusList = new List<VacancyStatus>();
            List<Emails> emailList = new List<Emails>();
            List<Roles> roleList = new List<Roles>();
            List<Pipelines> pipelineList = new List<Pipelines>();
            List<BatchProcesses> batchProcessList = new List<BatchProcesses>();
            List<SMSTemplates> smsTemplateList = new List<SMSTemplates>();
            List<OrganizationPosition> organizationPositionList = new List<OrganizationPosition>();
            List<Departments> departmentList = new List<Departments>();
            List<Divisions> divisionList = new List<Divisions>();
            List<Universities> universityList = new List<Universities>();
            List<DisqualifierQuestions> disqualifierQuestionList = new List<DisqualifierQuestions>();
            List<SurveyQuestions> surveyQuestionList = new List<SurveyQuestions>();
            List<Country> countryList = new List<Country>();
            List<UserAccounts> userList = new List<UserAccounts>();
            List<VacancyCategories> categoryList = new List<VacancyCategories>();
            List<ReportAutomatic> reportAutoList = new List<ReportAutomatic>();
            List<SourceReferences> sourceReferencesList = new List<SourceReferences>();
            List<ApplicationConfiguration> appConfigList = new List<ApplicationConfiguration>();
            switch (commandName)
            {
                case "GetJobCategoryList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;
                    }

                    categoryList = ERecruitmentManager.GetCategoriesList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                             sortingProperty, sortDir);
                    count = ERecruitmentManager.CountCategoryList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (VacancyCategories item in categoryList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetDisqualifierQuestionList":

                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "Name";
                            break;
                    }
                    disqualifierQuestionList = DisqualificationQuestionManager.GetDisqualifierQuestionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = DisqualificationQuestionManager.CountDisqualifierQuestionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (DisqualifierQuestions item in disqualifierQuestionList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showDataForm(\"" + item.Code + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" data-attribute=\"" + item.Code + "\" class=\"cbChild\" value=\"false\"/>";

                        dataComponentList.Add(JqueryDataTable.StripData(check));

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetSurveyQuestionList":

                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "Name";
                            break;
                    }
                    surveyQuestionList = SurveyQuestionManager.GetSurveyQuestionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = SurveyQuestionManager.CountSurveyQuestionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (SurveyQuestions item in surveyQuestionList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showDataForm(\"" + item.Code + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" data-attribute=\"" + item.Code + "\" class=\"cbChild\" value=\"false\"/>";

                        dataComponentList.Add(JqueryDataTable.StripData(check));

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetBatchProcessList":
                    switch (sortCol)
                    {
                        case 0:
                            sortingProperty = "SchemaName";
                            break;
                        case 1:
                            sortingProperty = "FileName";
                            break;
                        case 2:
                            sortingProperty = "Date";
                            break;
                        case 3:
                            sortingProperty = "ProcesserName";
                            break;
                        case 4:
                            sortingProperty = "Status";
                            break;
                    }
                    batchProcessList = BatchProcessManager.GetBatchProcessList(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                               Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                               Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                               sortingProperty, sortDir);
                    count = BatchProcessManager.CountBatchProcessList(Utils.GetQueryString<string>("vacancyCode"), Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (BatchProcesses item in batchProcessList)
                    {
                        List<string> dataComponentList = new List<string>();
                        dataComponentList.Add(JqueryDataTable.StripData(item.SchemaName));
                        dataComponentList.Add(JqueryDataTable.StripData(item.FileName));
                        dataComponentList.Add(JqueryDataTable.StripData(Utils.FormatDateNumeric(item.Date)));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ProcesserName));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Status));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetAttachmentList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 2:
                            sortingProperty = "Name";
                            break;
                    }
                    List<TestAttachments> testAttachmentist = HiringManager.GetTestAttachment(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = HiringManager.CountTestAttachment(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (TestAttachments item in testAttachmentist)
                    {
                        List<string> dataComponentList = new List<string>();
                        string url = MainCompany.ATSBaseUrl + "recruiter/role-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.AttachmentCode + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.AttachmentCode + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataComponentList.Add(JqueryDataTable.StripData(item.AttachmentCode));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;

                case "GetTestList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 2:
                            sortingProperty = "TestName";
                            break;
                    }
                    List<Tests> testList = HiringManager.GetTestList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = HiringManager.CountTestList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Tests item in testList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string url = MainCompany.ATSBaseUrl + "recruiter/role-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.TestCode + "\");'> '" + item.TestName + "</div>";
                        string check = "<input name=\"" + "U_" + item.TestCode + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.TestName));
                        dataComponentList.Add(JqueryDataTable.StripData(item.TestCode));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;

                case "GetAssestmentList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 2:
                            sortingProperty = "TestName";
                            break;
                    }
                    List<Tests> assestmentList = HiringManager.GetTestList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = HiringManager.CountTestList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Tests item in assestmentList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string url = MainCompany.ATSBaseUrl + "recruiter/role-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.TestCode + "\");'> '" + item.TestName + "</div>";
                        string previewLink = "<div style='cursor:pointer;color:blue;' onclick='previewAssestment(\"" + item.TestCode + "\");'>Preview Assestment</div>";
                        string check = "<input name=\"" + "U_" + item.TestCode + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.TestName));
                        dataComponentList.Add(JqueryDataTable.StripData(previewLink));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetPipelineList":

                    pipelineList = HiringManager.GetPipelineList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = HiringManager.CountPipelineList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Pipelines item in pipelineList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../role/role-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.Code + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetUserList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;
                    }
                    userList = AuthenticationManager.GetUserAccountList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = AuthenticationManager.CountUserAccountList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (UserAccounts item in userList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetRoleList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;
                    }
                    roleList = ERecruitmentManager.GetRoleList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                      Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = ERecruitmentManager.CountRoleList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Roles item in roleList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.RoleCode + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.RoleCode + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.RoleCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetSMSTemplateList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "TemplateName";
                            break;
                    }
                    smsTemplateList = ERecruitmentManager.GetSMSTemplateList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                            sortingProperty, sortDir);
                    count = ERecruitmentManager.CountSMSTemplateList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (SMSTemplates item in smsTemplateList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../email/email-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.Id.ToString() + "\");'> '" + item.TemplateName + "</div>";
                        string check = "<input name=\"" + "U_" + item.Id.ToString() + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Category));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Id.ToString()));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetEmailList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "TemplateName";
                            break;
                    }
                    emailList = ERecruitmentManager.GetEmailList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                            sortingProperty, sortDir);
                    count = ERecruitmentManager.CountEmailList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Emails item in emailList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../email/email-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.Id.ToString() + "\");'> '" + item.TemplateName + "</div>";
                        string check = "<input name=\"" + "U_" + item.Id.ToString() + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Category));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Id.ToString()));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetUniversities":
                 
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;
                      
                    }
                   universityList = ERecruitmentManager.GetUniversityList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                     Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                     Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                         sortingProperty, sortDir);
                    count = ERecruitmentManager.CountUniversityList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Universities item in universityList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../status/status-form.aspx";
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        string issma = "";
                        if (item.Kategori == 1)
                            issma = "DIPLOMA/STRATA";
                        else if (item.Kategori == 2)
                            issma = "SMA";
                        else if (item.Kategori == 3)
                            issma = "SMK";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                       
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataComponentList.Add(JqueryDataTable.StripData(issma));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetUniversitiesByKategori":

                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;

                    }
                    universityList = ERecruitmentManager.GetUniversityListByKategori(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                       Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                       Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                       Utils.GetQueryString<int>("kategori"), sortingProperty, sortDir);
                    count = ERecruitmentManager.CountUniversityListByKategori(Utils.GetQueryString<string>(JqueryDataTable.sSearch), Utils.GetQueryString<int>("kategori"));
                    foreach (Universities item in universityList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<a href= \"#\" name=\"" + item.Code + "~" + item.Name + "\" class=\"choose\" >Select</a>";
                        //string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-check edit\" title=\"View\"></i>";
                        //string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";


                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        //dataComponentList.Add(JqueryDataTable.StripData(actionDelete));

                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
       
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetStatusList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "Name";
                            break;
                        case 2:
                            sortingProperty = "RecruiterLabel";
                            break;
                    }
                    statusList = ERecruitmentManager.GetVacancyStatusList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                            sortingProperty, sortDir);
                    count = ERecruitmentManager.CountVacancyStatusList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (VacancyStatus item in statusList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../status/status-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.Code + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.CandidateLabel));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetCompanyList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "Name";
                            break;
                        case 2:
                            sortingProperty = "ContactPerson";
                            break;
                        case 3:
                            sortingProperty = "IndustryObj.Name";
                            break;
                    }
                    companyList = ERecruitmentManager.GetCompanyList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                            sortingProperty, sortDir);
                    count = ERecruitmentManager.CountCompanyList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Companies item in companyList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../company/company-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showForm(\"" + item.Code + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        if (!string.IsNullOrEmpty(item.ContactPerson))
                            dataComponentList.Add(JqueryDataTable.StripData(item.ContactPerson));
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        if (!string.IsNullOrEmpty(item.Industry))
                        {
                            Industries industry = ERecruitmentManager.GetIndustry(item.Industry);
                            if (industry != null)
                                dataComponentList.Add(JqueryDataTable.StripData(industry.Name));
                            else
                                dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        }
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));

                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetIndustryList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;
                    }

                    industryList = ERecruitmentManager.GetIndustriesList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                             sortingProperty, sortDir);
                    count = ERecruitmentManager.CountIndustryList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Industries item in industryList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetPositionList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "CompanyName";
                            break;
                        case 4:
                            sortingProperty = "Name";
                            break;

                    }

                    positionList = ERecruitmentManager.GetPositionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                             sortingProperty, sortDir);
                    count = ERecruitmentManager.CountPositionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Positions item in positionList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.PositionCode + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.PositionCode + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.PositionCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.CompanyName));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetEmploymentStatusList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;


                    }

                    empStatusList = ERecruitmentManager.GetEmploymentStatusList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountEmploymentStatusList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (EmploymentStatus item in empStatusList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetDepartmentList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;


                    }

                    departmentList = ERecruitmentManager.GetDepartmentList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountDepartmentList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Departments item in departmentList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetDivisionList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;


                    }

                    divisionList = ERecruitmentManager.GetDivisionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                               sortingProperty, sortDir);
                    count = ERecruitmentManager.CountDivisionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Divisions item in divisionList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetEducationLevelList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;
                    }

                    educationLevelList = ERecruitmentManager.GetEducationLevelList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountEducationLevelList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (EducationLevels item in educationLevelList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.EducationLevel + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.EducationLevel + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.EducationLevel));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        //dataComponentList.Add(JqueryDataTable.StripData(item.Major));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetFieldOfStudyList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;


                    }

                    fieldOfStudyList = ERecruitmentManager.GetFieldOfStudyList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountFieldOfStudyList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (EducationFieldOfStudy item in fieldOfStudyList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        string issma = "";
                        if (item.Kategori == 1)
                            issma = "DIPLOMA/STRATA";
                        else if (item.Kategori == 2)
                            issma = "SMA";
                        else if (item.Kategori == 3)
                            issma = "SMK";

                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataComponentList.Add(JqueryDataTable.StripData(issma));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetEducationMappingHeader":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "UniversitiesName";
                            break;
                    }

                    educationMappingHeaderList = ERecruitmentManager.GetEducationMappingHeader(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountEducationMappingHeader(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (EducationMappingHeader item in educationMappingHeaderList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.EducationMappingHeaderCode + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.EducationMappingHeaderCode + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.EducationMappingHeaderCode));
                        //dataComponentList.Add(JqueryDataTable.StripData(item.UniversitiesCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.UniversitiesName));
                        //dataComponentList.Add(JqueryDataTable.StripData(item.EducationLevel));
                        dataComponentList.Add(JqueryDataTable.StripData(item.EducationLevelName));
                        //dataComponentList.Add(JqueryDataTable.StripData(item.FieldOfStudyCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.FieldOfStudyName));
                        //dataComponentList.Add(JqueryDataTable.StripData(item.MajorConcateCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.MajorConcate)); 
                        //dataComponentList.Add(JqueryDataTable.StripData(item.Major));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;

                case "GetCityList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "CityName";
                            break;


                    }

                  List<City>  cityList = ERecruitmentManager.GetCityDataTableList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountCityList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (City item in cityList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.CityCode + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.CityCode + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.CityCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.CityName));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ProvinceName));
                        dataComponentList.Add(JqueryDataTable.StripData(item.CountryName));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetProvinceList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "ProvinceName";
                            break;


                    }

                    List<Province> provinceList = ERecruitmentManager.GetProvinceList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                            Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                            Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                                sortingProperty, sortDir);
                    count = ERecruitmentManager.CountProvinceList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Province item in provinceList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.ProvinceCode + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.ProvinceCode + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ProvinceCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ProvinceName));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;

                case "GetCountryList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;


                    }

                    countryList = ERecruitmentManager.GetCountryList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                            Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                            Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                                sortingProperty, sortDir);
                    count = ERecruitmentManager.CountCountryList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Country item in countryList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetJobCodeList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;

                    }
                    List<Jobs> jobCodeList = ERecruitmentManager.GetJobList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountJobList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Jobs item in jobCodeList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.JobCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.JobCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.JobRole));
                        dataComponentList.Add(JqueryDataTable.StripData(item.JobStage));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Requirements.Replace('	', ' ').Replace('	', ' ')));
                        dataComponentList.Add(JqueryDataTable.StripData(item.JobDescription.Replace('	', ' ').Replace('	', ' ')));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());


                    break;
                case "GetEducationMajorList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Major";
                            break;

                    }

                    educationMajorList = ERecruitmentManager.GetEducationMajorList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                          Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                              sortingProperty, sortDir);
                    count = ERecruitmentManager.CountEducationMajorList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (EducationMajor item in educationMajorList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Major));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetLanguageList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Language";
                            break;


                    }

                    languageList = ERecruitmentManager.GetLanguageList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                               sortingProperty, sortDir);
                    count = ERecruitmentManager.CountLanguageList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Languages item in languageList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Language));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetAwardList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Award";
                            break;


                    }

                    awardList = ERecruitmentManager.GetAwardList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                               sortingProperty, sortDir);
                    count = ERecruitmentManager.CountAwardList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (Awards item in awardList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Award));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetOrganizationPositionList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Position";
                            break;
                    }
                    organizationPositionList = ERecruitmentManager.GetOrganizationPositionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                               sortingProperty, sortDir);
                    count = ERecruitmentManager.CountOrganizationPositionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (OrganizationPosition item in organizationPositionList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Position));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetOrganizationTypeList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Type";
                            break;


                    }

                    orgTypeList = ERecruitmentManager.GetOrganizationTypeList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                               sortingProperty, sortDir);
                    count = ERecruitmentManager.CountOrganizationTypeList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    foreach (OrganizationType item in orgTypeList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Type));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
               case "GetReportAutoList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Report";
                            break;
                    }

                    var startDate = Utils.GetQueryString<string>("fStartDate");
                    var endDate = Utils.GetQueryString<string>("fEndDate");

                    reportAutoList = ERecruitmentManager.GetReportAutomaticList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir, startDate, endDate);
                    count = ERecruitmentManager.CountReportAutomaticList(Utils.GetQueryString<string>(JqueryDataTable.sSearch), startDate, endDate);
                    foreach (ReportAutomatic item in reportAutoList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(ERecruitmentManager.GetReports(item.Report)));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Schedule));
                        if (item.StartDate != null && !string.IsNullOrEmpty(item.StartDate))
                        {
                            DateTime sdt = Convert.ToDateTime(item.StartDate);
                            dataComponentList.Add(JqueryDataTable.StripData(sdt.ToString("MM/dd/yyyy")));
                        }
                        else {
                            dataComponentList.Add(JqueryDataTable.StripData(item.StartDate));
                        }

                        if (item.EndDate != null && !string.IsNullOrEmpty(item.EndDate))
                        {
                            DateTime edt = Convert.ToDateTime(item.EndDate);
                            dataComponentList.Add(JqueryDataTable.StripData(edt.ToString("MM/dd/yyyy")));
                        }
                        else {
                            dataComponentList.Add(JqueryDataTable.StripData(item.EndDate));
                        }

                        dataComponentList.Add(JqueryDataTable.StripData(item.TimeSchedule));

                        if (!string.IsNullOrEmpty(item.Email))
                        {
                            string[] ucode = item.Email.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            string uList = "";
                            foreach (string u in ucode)
                            {
                                if (!string.IsNullOrEmpty(u))
                                {
                                    UserAccounts ua = AuthenticationManager.GetUserAccount(u);
                                    uList += "," + ua.UserName;
                                   
                                }

                            }

                            dataComponentList.Add(JqueryDataTable.StripData(uList.Substring(1)));
                        }
                        else {
                            dataComponentList.Add(JqueryDataTable.StripData(item.Email));
                        }
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetSourceReferencesList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "Name";
                            break;
                    }

                    sourceReferencesList = ERecruitmentManager.GetSourceReferencesList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = ERecruitmentManager.CountSourceReferencesList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (SourceReferences item in sourceReferencesList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        string actionDelete = "<i name=\"" + item.Code + "\" class=\"fa fa-trash-o delete\" title=\"Remove\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(actionEdit));
                        dataComponentList.Add(JqueryDataTable.StripData(actionDelete));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetAppConfigList":
                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 3:
                            sortingProperty = "ConfigName";
                            break;
                    }

                    appConfigList = ERecruitmentManager.GetApplicationConfigurationList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                          sortingProperty, sortDir);
                    count = ERecruitmentManager.CountApplicationConfiguration(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (ApplicationConfiguration item in appConfigList)
                    {
                        //string link = "<asp:LinkButton ID=\"" + item.Code + "\" OnCommand=\"showData\" CommandArgument=\"" + item.Code + "\" Text=\"" + item.ConfigName + "\" runat=\"server\"></asp:LinkButton>";
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showDataForm(\"" + item.Code.ToString() + "\");'> '" + item.ConfigName + "</div>";
                        List<string> dataComponentList = new List<string>();
                        string actionEdit = "<i name=\"" + item.Code + "\" class=\"fa fa-pencil edit\" title=\"View\"></i>";
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code.ToString()));
                       
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}