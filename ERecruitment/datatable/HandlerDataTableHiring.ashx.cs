﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerDataTableHiring
    /// </summary>
    public class HandlerDataTableHiring : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            JqueryDataTable dt;
            int count;
            int sortCol = Utils.GetQueryString<int>(JqueryDataTable.iSortingCols);
            string sortDir = Utils.GetQueryString<string>(JqueryDataTable.sSortDir);
            string sortingProperty = string.Empty;
            List<string> dataList = new List<string>();
            List<Vacancies> vacancyList = new List<Vacancies>();
            string categoryCode = string.Empty;
            string appCode = Utils.GetQueryString<string>("applicantCode");
            string status = Utils.GetQueryString<string>("pipelineCode");
            string vacancyCode = Utils.GetQueryString<string>("vacancyCode");
            switch (commandName)
            {
                case "GetApplicantTestResult":
                    List<ApplicantVacancyTestResults> testResultList = HiringManager.GetTestResult(appCode, vacancyCode);
                    count = HiringManager.CountTestResult(appCode, vacancyCode);
                    foreach (ApplicantVacancyTestResults item in testResultList)
                    {
                        List<string> dataComponentList = new List<string>();
                        dataComponentList.Add(JqueryDataTable.StripData(item.Result));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetApplicantOffering":
                    List<ApplicantVacancyOffering> offeringList = HiringManager.GetOfferingList(vacancyCode, appCode, status,
                                                                        Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                        sortingProperty, sortDir);
                    count = HiringManager.CountOfferingList(vacancyCode, appCode, status, Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    int counterOffering = 1;
                    foreach (ApplicantVacancyOffering item in offeringList)
                    {
                        List<string> dataComponentList = new List<string>();

                        string link = "<div style='cursor:pointer;color:blue;' onclick='updateOffering(\"" + item.Id.ToString() + "\");'> '" + counterOffering.ToString() + "</div>";

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(Utils.DisplayDateTime(item.CreatedDate)));
                        if (!string.IsNullOrEmpty(item.CreatedBy))
                        {
                            UserAccounts getAccount = AuthenticationManager.GetUserAccount(item.CreatedBy);
                            if (getAccount != null)
                                dataComponentList.Add(JqueryDataTable.StripData(getAccount.Name));
                            else
                                dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        }
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ApplicantStatusNotification));
                        counterOffering++;
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
                case "GetApplicantSchedule":
                    List<ApplicantVacancySchedules> scheduleList = HiringManager.GetScheduleList(vacancyCode,appCode,status,
                                                                        Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                        Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                        sortingProperty, sortDir);
                    count = HiringManager.CountScheduleList(vacancyCode,appCode,status, Utils.GetQueryString<string>(JqueryDataTable.sSearch));

                    int counter = 1;
                    foreach (ApplicantVacancySchedules item in scheduleList)
                    {
                        List<string> dataComponentList = new List<string>();
                      
                        string link = "<div style='cursor:pointer;color:blue;' onclick='updateSchedule(\"" + item.Id.ToString() + "\");'> '" + counter.ToString() + "</div>";
                       
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(Utils.DisplayDateTime(item.CreatedDate)));
                        if (!string.IsNullOrEmpty(item.CreatedBy))
                        {
                            UserAccounts getAccount = AuthenticationManager.GetUserAccount(item.CreatedBy);
                            if (getAccount != null)
                                dataComponentList.Add(JqueryDataTable.StripData(getAccount.Name));
                            else
                                dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        }
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        dataComponentList.Add(JqueryDataTable.StripData(item.ApplicantStatusNotification));
                        counter++;
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}