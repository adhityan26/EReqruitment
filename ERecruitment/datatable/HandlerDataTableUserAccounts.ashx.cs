﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;


namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerDataTableUserAccounts
    /// </summary>
    public class HandlerDataTableUserAccounts : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            JqueryDataTable dt;
            int count;
            int sortCol = Utils.GetQueryString<int>(JqueryDataTable.iSortingCols);
            string sortDir = Utils.GetQueryString<string>(JqueryDataTable.sSortDir);
            string sortingProperty = string.Empty;
            List<string> dataList = new List<string>();
            List<UserAccounts> userList = new List<UserAccounts>();
            List<Applicants> applicantList = new List<Applicants>();
            switch (commandName)
            {
                case "GetApplicantList":
                    applicantList = ApplicantManager.GetApplicantList(Utils.GetQueryString<string>("companyCode").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries),
                                                                           Utils.GetQueryString<string>("accountStatusCode"),
                                                                           Utils.GetQueryString<string>("genderCode"),
                                                                           Utils.GetQueryString<string>("maritalStatus"),
                                                                           Utils.GetQueryString<string>("flagStatus"),
                                                                           Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                           sortingProperty, sortDir);
                    count = ApplicantManager.CountApplicantList(Utils.GetQueryString<string>("companyCode").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries),
                                                                     Utils.GetQueryString<string>("accountStatusCode"),
                                                                     Utils.GetQueryString<string>("genderCode"),
                                                                     Utils.GetQueryString<string>("maritalStatus"),
                                                                     Utils.GetQueryString<string>("flagStatus"),
                                                                     Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Applicants item in applicantList)
                    {
                        List<string> dataComponentList = new List<string>();
                        dataComponentList.Add(JqueryDataTable.StripData(item.CompanyName));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataComponentList.Add(JqueryDataTable.StripData(item.GenderSpecification));
                        dataComponentList.Add(JqueryDataTable.StripData(item.MaritalStatusSpecification));
                        dataComponentList.Add(JqueryDataTable.StripData(item.JobAppliedCount.ToString()));
                        dataComponentList.Add(JqueryDataTable.StripData(item.LatestEducationLevelCode));
                        dataComponentList.Add(JqueryDataTable.StripData(item.LatestOrganizationExperience));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Verified ? "Verified" : "Unverified"));
                        UserAccounts account = AuthenticationManager.GetApplicantUserAccount(item.Code);
                        item.Account = account;
                        dataComponentList.Add(JqueryDataTable.StripData(item.Account != null ? (item.Account.LastLogin.HasValue ? Utils.DisplayDateTime(item.Account.LastLogin.Value) : "Never logged in") : "Never logged in"));
                        dataComponentList.Add(JqueryDataTable.StripData(item.FlagStatus));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }

                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetApplicantBlackList":
                    applicantList = ApplicantManager.GetFlaggedApplicantList("BlackList",
                                                                           Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                           sortingProperty, sortDir);
                    count = ApplicantManager.CountFlaggedApplicantList("BlackList",
                                                                     Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Applicants item in applicantList)
                    {
                        List<string> dataComponentList = new List<string>();

                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataComponentList.Add(JqueryDataTable.StripData(item.IDCardNumber));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Email));
                        dataComponentList.Add(JqueryDataTable.StripData(item.MaritalStatusSpecification));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Phone));
                        dataComponentList.Add(JqueryDataTable.StripData(item.GenderSpecification));
                        dataComponentList.Add(JqueryDataTable.StripData(item.FlagNotes));
                        dataComponentList.Add(JqueryDataTable.StripData(item.FlagIssuedByName));
                        dataComponentList.Add(JqueryDataTable.StripData(Utils.FormatDateNumeric(item.FlagIssuedDate)));

                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }

                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetApplicantGreenList":
                    applicantList = ApplicantManager.GetFlaggedApplicantList("GreenList",
                                                                           Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                           sortingProperty, sortDir);
                    count = ApplicantManager.CountFlaggedApplicantList("GreenList",
                                                                     Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Applicants item in applicantList)
                    {
                        List<string> dataComponentList = new List<string>();

                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataComponentList.Add(JqueryDataTable.StripData(item.IDCardNumber));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Email));
                        dataComponentList.Add(JqueryDataTable.StripData(item.MaritalStatusSpecification));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Phone));
                        dataComponentList.Add(JqueryDataTable.StripData(item.GenderSpecification));
                        dataComponentList.Add(JqueryDataTable.StripData(item.FlagNotes));
                        dataComponentList.Add(JqueryDataTable.StripData(item.FlagIssuedByName));
                        dataComponentList.Add(JqueryDataTable.StripData(Utils.FormatDateNumeric(item.FlagIssuedDate)));

                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }

                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
                case "GetUserAccountList":
                    userList = AuthenticationManager.GetUserAccountList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                         Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                             sortingProperty, sortDir);
                    count = AuthenticationManager.CountUserAccountList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (UserAccounts item in userList)
                    {
                        List<string> dataComponentList = new List<string>();
//                        string url = "../user/user-form.aspx";
                        string link = "<div style='cursor:pointer;color:blue;' class='applicantName' id='" + item.Code + "' onclick='showForm(\"" + item.Code + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" class=\"cbChild\" value=\"false\"/>";
                        dataComponentList.Add(JqueryDataTable.StripData(check));
                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Name));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Email));

                        List<string> roleList = new List<string>();
                        List<UserRoles> userRoleList = AuthenticationManager.GetRoleList(item.Code);
                        foreach (UserRoles role in userRoleList)
                        {
                            Roles rl = ERecruitmentManager.GetRole(role.RoleCode);
                            roleList.Add(rl.Name);
                        }

                        if (roleList.Count > 0)
                            dataComponentList.Add(JqueryDataTable.StripData(string.Join(",", roleList.ToArray())));
                        else
                            dataComponentList.Add(JqueryDataTable.StripData(string.Empty));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }

                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());

                    break;

            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}