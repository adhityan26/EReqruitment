﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.SessionState;

namespace ERecruitment
{
    /// <summary>
    /// Summary description for HandlerDataTableAssestments
    /// </summary>
    public class HandlerDataTableAssestments : IHttpHandler
    {
        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (HttpContext.Current.Session["maincompany"] != null)
                    mainCompany = HttpContext.Current.Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    Uri myUri = new Uri(HttpContext.Current.Request.Url.ToString());
                    string host = myUri.Host;
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(host);
                    HttpContext.Current.Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            string commandName = Utils.GetQueryString<string>("commandName");
            JqueryDataTable dt;
            int count;
            int sortCol = Utils.GetQueryString<int>(JqueryDataTable.iSortingCols);
            string sortDir = Utils.GetQueryString<string>(JqueryDataTable.sSortDir);
            string sortingProperty = string.Empty;
            List<string> dataList = new List<string>();

            List<Questions> disqualifierQuestionList = new List<Questions>();
            switch (commandName)
            {

                case "GetAssestmentQuestionList":

                    sortingProperty = string.Empty;
                    switch (sortCol)
                    {
                        case 1:
                            sortingProperty = "Name";
                            break;
                    }
                    disqualifierQuestionList = AssestmentManager.GetAssestmentQuestionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch),
                                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayStart),
                                                                                           Utils.GetQueryString<int>(JqueryDataTable.iDisplayLength),
                                                                                           sortingProperty, sortDir);
                    count = AssestmentManager.CountAssestmentQuestionList(Utils.GetQueryString<string>(JqueryDataTable.sSearch));
                    foreach (Questions item in disqualifierQuestionList)
                    {
                        List<string> dataComponentList = new List<string>();
                        string link = "<div style='cursor:pointer;color:blue;' onclick='showDataForm(\"" + item.Code + "\");'> '" + item.Name + "</div>";
                        string check = "<input name=\"" + "U_" + item.Code + "\" type=\"checkbox\" data-attribute=\"" + item.Code + "\" class=\"cbChild\" value=\"false\"/>";

                        dataComponentList.Add(JqueryDataTable.StripData(check));

                        dataComponentList.Add(JqueryDataTable.StripData(link));
                        dataComponentList.Add(JqueryDataTable.StripData(item.Code));
                        dataList.Add(string.Concat("[", string.Join(",", dataComponentList.ToArray()), "]"));
                    }
                    dt = new JqueryDataTable(dataList, Utils.GetQueryString<int>(JqueryDataTable.sEcho), count);
                    context.Response.Write(dt.ToJson());
                    break;
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}