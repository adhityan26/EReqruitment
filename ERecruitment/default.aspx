﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/masters/landing.Master" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="ERecruitment._default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">
   
    function stopCarousel() {
        $("#myCarousel").carousel("pause");
    }    
</script>    

<!-- home section -->
<div id="home">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="mainCover item active" id="mainCover" runat="server">
                
            </div>
            <asp:Repeater ID="repCarouselSlides"
                          runat="server"
                          OnItemDataBound="repCarouselSlides_ItemDataBound"
                >
                <ItemTemplate>
                    <div runat="server" id="carouselItemContainer" class="item">
                        
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
        <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
        <span class="sr-only">Next</span>
        </a>
    </div>
</div>

<div id="jobs">
	<div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 center">
                <a data-lang="FindJobButtonText" href="/public/joblist.aspx" class="btn btn-default smoothScroll">FIND JOBS</a>
            </div>
        </div>
	</div>
</div>

<!-- job categories -->
<div id="service">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h2 data-lang="JobCategoryTitle">Job Categories</h2>
			</div>
            <asp:Repeater ID="repJobCategories"
                          runat="server"
                          >
                <ItemTemplate>
                    <div class="col-md-6 col-sm-6">
                        <div class="col-md-3 col-sm-3">
                            <i class='<%# Eval("IconCode") %>'></i>
                        </div>
                        <div class="col-md-9 col-sm-9">
				            <h4 style="display:inline"><a href='/public/joblist.aspx?jobCategoryCode=<%# Eval("Code") %>'><%# Eval("Name") %></a></h4>
				            <p><%# Eval("Description") %></p>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
		</div>
	</div>
</div>

<!-- divider section -->
<div class="container">
	<div class="row">
		<div class="col-md-1 col-sm-1"></div>
		<div class="col-md-10 col-sm-10">
			<hr>
		</div>
		<div class="col-md-1 col-sm-1"></div>
	</div>
</div>

<!-- about section -->
<div id="about">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12">
				<h2 data-lang="AboutUsTitle">About Us</h2>
			</div>
			<div class="col-md-6 col-sm-6">
				<img src="/handlers/HandlerUI.ashx?commandName=GetMainSiteAboutUsImage" runat="server" alt="about img" />
			</div>
			<div class="col-md-6 col-sm-6">
				<h3 data-lang="OurStoryTitle"><asp:Literal ID="litAboutUsTitle" runat="server" /></h3>
				<h4 data-lang="OurStorySubTitle"><asp:Literal ID="litAboutUsSubTitle" runat="server" /></h4>
				<p data-lang="OurStoryContent"><asp:Literal ID="litAboutUsText" runat="server" /></p>
			</div>
		</div>
	</div>
</div>

<!-- divider section -->
<div class="container">
	<div class="row">
		<div class="col-md-1 col-sm-1"></div>
		<div class="col-md-10 col-sm-10">
			<hr>
		</div>
		<div class="col-md-1 col-sm-1"></div>
	</div>
</div>

<!-- footer section -->
<footer id="contactus">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6">
				<h2 data-lang="ContactUsTitle">Our Office</h2>
				<p><asp:Literal ID="litAddressLine1" runat="server" /></p>
                <p><asp:Literal ID="litAddressLine2" runat="server" /></p>
                <p><asp:Literal ID="litAddressLine3" runat="server" /></p>
			</div>
		</div>
	</div>
</footer>

<!-- divider section -->
<div class="container">
	<div class="row">
		<div class="col-md-1 col-sm-1"></div>
		<div class="col-md-10 col-sm-10">
			<hr>
		</div>
		<div class="col-md-1 col-sm-1"></div>
	</div>
</div>
</asp:Content>
