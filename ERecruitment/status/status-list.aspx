﻿<%@ Page Title="Recruitment Process List" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="status-list.aspx.cs" Inherits="ERecruitment.status_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

     <script type="text/javascript">
         $(document).ready(function () {

             loadData();
         });

         function showForm(code) {

            showDataForm(code);
         }

         function Add() {

             showDataForm("0");
         }

         function Remove() {

             var idList = "";
             var tableData = $('#tableData').dataTable();
             $('input:checked', tableData.fnGetNodes()).each(function () {

                 idList += (tableData.fnGetData($(this).closest('tr')[0])[3] + ',');
             });
             if (idList == "")
                 showNotification("please select status");
             else {
                 confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
                     $.ajax({
                         url: "../handlers/HandlerSettings.ashx?commandName=RemoveStatus&codes=" + idList,
                         async: true,
                         beforeSend: function () {

                         },
                         success: function (queryResult) {
                             showNotification("status removed");
                             refresh();
                         },
                         error: function (xhr, ajaxOptions, thrownError) {
                             showNotification(xhr.responseText, 'error');
                             refresh();
                         }
                     });
                 });
             }

         }

         function loadData() {
             var ex = document.getElementById('tableData');
             if ($.fn.DataTable.fnIsDataTable(ex)) {
                 // data table, then destroy first
                 $("#tableData").dataTable().fnDestroy();
             }

             var OTableData = $('#tableData').dataTable({
                 "bProcessing": true,
                 "bServerSide": true,
                 "iDisplayLength": 10,
                 "bJQueryUI": true,
                 "bAutoWidth": false,
                 "sDom": "ftipr",
                 "bDeferRender": true,
                 "aoColumnDefs": [
                        { "bSortable": false, "aTargets": [0] },
                        { "bVisible": false, "aTargets": [3] },
                        { "sClass": "controlIcon", "aTargets": [0] }

                 ],
                 "oLanguage":
                                 { "sSearch": "Search By Name" },
                 "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetStatusList"
             });
         }

         function refresh() {
             var tableData = $('#tableData').dataTable();
             tableData.fnDraw();
         }

         function showDataForm(dataId) {

             var handlerUrl = "../handlers/HandlerSettings.ashx?commandName=GetRecruitmentProcess&id=" + dataId;

             $.ajax({
                 url: handlerUrl,
                 async: true,
                 beforeSend: function () {

                 },
                 success: function (queryResult) {

                     var dataModel = $.parseJSON(queryResult);
                     // form
                     loadDataIntoForm("formDataFrameModel", dataModel);


                 },
                 error: function (xhr, ajaxOptions, thrownError) {
                     showNotification(xhr.responseText, 'error');
                 }
             });

             $('#formDataFrameModel').modal('show');
         }

         function updateDataForm() {

             var form = "formDataFrameModel";
             var actionUrl = "../handlers/HandlerSettings.ashx?commandName=UpdateRecruitmentProcess";
             var formData = {};

             if (!getNodeData(form, formData)) return false;
             $('#formDataFrameModel').modal('toggle');
             $.post(actionUrl, formData).done(function (data) {

                 refresh();
                 //$('#formDataFrameModel').modal('hide');

             });
         }

    </script>
      <asp:HiddenField ID="hidUrl" runat="server" />
    

    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
            <button class="btn btn-default commandIcon" onclick="Remove()" type="button"><i class="fa fa-trash"></i>Delete</button>
        </div>
    </div>

    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th><span data-lang="ProcessNameTitle">Process Name</span></th>
                <th><span data-lang="CandidateLabelTitle">Candidat Label</span></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
           
        </tbody>
    </table>

    
    
    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
            <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h1 class="modal-title center" data-lang="RecruitmentProcessTitle">Recruitment Process</h1>
		    </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="ProcessNameTitle">Process Name</span><span class="requiredmark">*</span>       
                        <input type="hidden" data-attribute="Code" />
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" name="processName" type="text" required="" data-attribute="Name"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="CandidateLabelTitle">Candidat Label</span><span class="requiredmark">*</span>
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" type="text" required="" data-attribute="CandidateLabel"/> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateDataForm();" >Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
            </div>
            </div>
        </div>
    </div>

</asp:Content>
