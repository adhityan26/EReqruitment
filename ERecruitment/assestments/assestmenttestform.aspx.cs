﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class assestmenttestform : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
                {
                    List<TestQuestions> questionList = new List<TestQuestions>();

                    TestQuestions question = new TestQuestions();
                    questionList.Add(question);

                    repSteps.DataSource = questionList;
                    repSteps.DataBind();
                }
                else
                    iniateForm(Utils.GetQueryString<string>("code"));
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            bool isNew = true;
            Tests assestment = new Tests();
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
            {
                assestment = HiringManager.GetTest(Utils.GetQueryString<string>("code"));
                isNew = false;
            }
            else
            {
                assestment.Active = true;
            }

            assestment.TestName = tbName.Text;
            assestment.DurationInSeconds = Utils.ConvertString<int>(tbAssestmentDuration.Text);
            assestment.PageSize = Utils.ConvertString<int>(tbSize.Text);
            assestment.Description = tbDescription.Text;
            assestment.QuestionList = getQuestionList();
            assestment.ShowTimer = cbShowTimer.Checked;

            if (isNew)
                HiringManager.SaveTest(assestment);
            else
                HiringManager.UpdateTest(assestment);

            JQueryHelper.InvokeJavascript("showNotification('Successfully saved');backToList();", Page);
        }

        private void iniateForm(string code)
        {
            Tests assestment = HiringManager.GetTest(code);
            if (assestment != null)
            {
                tbName.Text = assestment.TestName;
                tbAssestmentDuration.Text = assestment.DurationInSeconds.ToString();
                tbSize.Text = assestment.PageSize.ToString();
                tbDescription.Text = assestment.Description;
                cbShowTimer.Checked = assestment.ShowTimer;

                List<TestQuestions> questionList = AssestmentManager.GetTestQuestionList(assestment.TestCode);

                if (questionList.Count == 0)
                {
                    TestQuestions question = new TestQuestions();
                    questionList.Add(question);
                }

                repSteps.DataSource = questionList;
                repSteps.DataBind();
                repSteps.DataSource = questionList;
                repSteps.DataBind();
            }
        }

        protected void btnAddRow_ServerClick(object sender, EventArgs e)
        {
            List<TestQuestions> questionList = getQuestionList();
            TestQuestions step = new TestQuestions();
            questionList.Add(step);
            repSteps.DataSource = questionList;
            repSteps.DataBind();
        }

        #region support

        private List<TestQuestions> getQuestionList()
        {
            List<TestQuestions> questionList = new List<TestQuestions>();
            int orderId = 1;

            foreach (RepeaterItem step in repSteps.Items)
            {
                DropDownList ddQuestion = step.FindControl("ddQuestion") as DropDownList;
                TextBox tbDuration = step.FindControl("tbDuration") as TextBox;
                TextBox tbPoint = step.FindControl("tbPoint") as TextBox;
                CheckBox cbShowTimer = step.FindControl("cbShowTimer") as CheckBox;

                if (string.IsNullOrEmpty(Utils.GetSelectedValue<string>(ddQuestion)))
                    continue;

                TestQuestions addQuestion = new TestQuestions();
                addQuestion.QuestionCode = Utils.GetSelectedValue<string>(ddQuestion);
                addQuestion.DurationInSeconds = Utils.ConvertString<int>(tbDuration.Text);
                addQuestion.Point = Utils.ConvertString<int>(tbPoint.Text);
                addQuestion.ShowTimer = cbShowTimer.Checked;

                addQuestion.OrderId = orderId;
                orderId++;
                questionList.Add(addQuestion);
            }

            return questionList;
        }

        #endregion

        #region repeater

        protected void repSteps_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.DataItem != null)
            {

                TestQuestions testQuestion = e.Item.DataItem as TestQuestions;

                Label lbSteps = e.Item.FindControl("lbSteps") as Label;
                HiddenField hidStatusCode = e.Item.FindControl("hidStatusCode") as HiddenField;

                DropDownList ddQuestion = e.Item.FindControl("ddQuestion") as DropDownList;
                DropDownList ddEmailTemplate = e.Item.FindControl("ddEmailTemplate") as DropDownList;

                HtmlButton btnUp = e.Item.FindControl("btnUp") as HtmlButton;
                HtmlButton btnDown = e.Item.FindControl("btnDown") as HtmlButton;
                HtmlButton btnRemove = e.Item.FindControl("btnRemove") as HtmlButton;
                TextBox tbDuration = e.Item.FindControl("tbDuration") as TextBox;
                TextBox tbPoint = e.Item.FindControl("tbPoint") as TextBox;
                CheckBox cbShowTimer = e.Item.FindControl("cbShowTimer") as CheckBox;


                if (!string.IsNullOrEmpty(testQuestion.QuestionCode))
                {
                    ddQuestion.DataBind();
                    Utils.SelectItem<string>(ddQuestion, testQuestion.QuestionCode);
                    tbDuration.Text = testQuestion.DurationInSeconds.ToString();
                    tbPoint.Text = testQuestion.Point.ToString();
                    cbShowTimer.Checked = testQuestion.ShowTimer;
                }
            }
        }

        #endregion

        protected void repSteps_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void btnUp_ServerClick(object sender, EventArgs e)
        {
            Control btn = sender as Control;
            RepeaterItem currentItem = btn.Parent as RepeaterItem;
            List<TestQuestions> questionList = new List<TestQuestions>();
            foreach (RepeaterItem step in repSteps.Items)
            {
                DropDownList ddQuestion = step.FindControl("ddQuestion") as DropDownList;
                TextBox tbDuration = step.FindControl("tbDuration") as TextBox;
                TextBox tbPoint = step.FindControl("tbPoint") as TextBox;
                CheckBox cbShowTimer = step.FindControl("cbShowTimer") as CheckBox;

                TestQuestions testQuestion = new TestQuestions();
                testQuestion.QuestionCode = Utils.GetSelectedValue<string>(ddQuestion);
                testQuestion.DurationInSeconds = Utils.ConvertString<int>(tbDuration.Text);
                testQuestion.Point = Utils.ConvertString<int>(tbPoint.Text);
                testQuestion.ShowTimer = cbShowTimer.Checked;

                if (step == currentItem && questionList.Count > 0)
                    questionList.Insert(questionList.Count - 1, testQuestion);
                else
                    questionList.Add(testQuestion);
            }

            repSteps.DataSource = questionList;
            repSteps.DataBind();
        }

        protected void btnDown_ServerClick(object sender, EventArgs e)
        {
            Control btn = sender as Control;
            RepeaterItem currentItem = btn.Parent as RepeaterItem;
            List<TestQuestions> questionList = new List<TestQuestions>();
            TestQuestions tempPipeline = null;
            foreach (RepeaterItem step in repSteps.Items)
            {
                DropDownList ddQuestion = step.FindControl("ddQuestion") as DropDownList;
                TextBox tbDuration = step.FindControl("tbDuration") as TextBox;
                TextBox tbPoint = step.FindControl("tbPoint") as TextBox;
                CheckBox cbShowTimer = step.FindControl("cbShowTimer") as CheckBox;

                TestQuestions testQuestion = new TestQuestions();
                testQuestion.QuestionCode = Utils.GetSelectedValue<string>(ddQuestion);
                testQuestion.DurationInSeconds = Utils.ConvertString<int>(tbDuration.Text);
                testQuestion.Point = Utils.ConvertString<int>(tbPoint.Text);
                testQuestion.ShowTimer = cbShowTimer.Checked;

                if (step == currentItem)
                    tempPipeline = testQuestion;
                else
                {
                    questionList.Add(testQuestion);
                    if (tempPipeline != null)
                    {
                        questionList.Add(tempPipeline);
                        tempPipeline = null;
                    }
                }
            }

            repSteps.DataSource = questionList;
            repSteps.DataBind();
        }

        protected void btnRemove_ServerClick(object sender, EventArgs e)
        {
            Control btn = sender as Control;
            RepeaterItem currentItem = btn.Parent as RepeaterItem;
            List<TestQuestions> questionList = new List<TestQuestions>();
            foreach (RepeaterItem step in repSteps.Items)
            {
                if (step == currentItem)
                    continue;

                DropDownList ddQuestion = step.FindControl("ddQuestion") as DropDownList;
                TestQuestions newQuestion = new TestQuestions();
                newQuestion.QuestionCode = Utils.GetSelectedValue<string>(ddQuestion);
                questionList.Add(newQuestion);
            }

            repSteps.DataSource = questionList;
            repSteps.DataBind();
        }

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            Utils.InsertInitialListItem(dd);
        }
    }
}