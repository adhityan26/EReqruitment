﻿<%@ Page Title="Assestment Test" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="assestmenttestform.aspx.cs" Inherits="ERecruitment.assestmenttestform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function backToList()
        {
            window.location.href = "assestmenttests.aspx";
        }

    </script>

    <div class="row">
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbPipeline_PipelineName">Name</label>
                <span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <asp:TextBox ID="tbName" runat="server" CssClass="form-control" />     
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="Label1">Duration</label>
                <span class="requiredmark">*</span>
            </div>
            <div class="col-sm-1">
                <asp:TextBox ID="tbAssestmentDuration" runat="server" CssClass="form-control plainnumber" />                                        
            </div>  
            <div class="col-sm-2">
                Seconds
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="Label2">Size</label>
                <span class="requiredmark">*</span>
            </div>
            <div class="col-sm-1">
                <asp:TextBox ID="tbSize" runat="server" CssClass="form-control plainnumber" />                                        
            </div>   
            <div class="col-sm-2">
                Question(s)/Page
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="Label4">Show Timer</label>
                <span class="requiredmark">*</span>
            </div>
            <div class="col-sm-2">
                <asp:CheckBox ID="cbShowTimer" runat="server" />                                       
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="Label3">Description</label>
                <span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <asp:TextBox ID="tbDescription" runat="server" CssClass="form-control" TextMode="MultiLine" />                                        
            </div>  
        </div>
         <asp:Repeater ID="repSteps" OnItemDataBound="repSteps_ItemDataBound" OnItemCommand="repSteps_ItemCommand" runat="server">
            <HeaderTemplate>
                <table class="tableSteps table table-striped table-bordered dt-responsive nowrap">
                    <tr>
                        <th class="iconCell"></th>
                        <th class="iconCell"></th>
                        <th>Question</th>
                        <th class="iconCell">Duration (Seconds)</th>
                        <th class="iconCell">Point Weight</th>
                        <th class="iconCell">Show Timer</th>
                        <th class="iconCell"></th>
                    </tr>
            </HeaderTemplate>
            <ItemTemplate>
                    <tr>
                        <td class="iconCell">                                                                          
                            <button class="btn" id="btnUp" runat="server" onserverclick="btnUp_ServerClick">
                                <i class="fa fa-arrow-circle-up text-primary"></i>
                            </button>
                        </td>
                        <td class="iconCell">                                                                          
                            <button class="btn" id="btnDown" runat="server" onserverclick="btnDown_ServerClick">
                                <i class="fa fa-arrow-circle-down text-primary"></i>
                            </button>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddQuestion" 
                                              runat="server" 
                                              Width="150" 
                                              CssClass="form-control" 
                                              DataSourceID="odsQuestionList"
                                              OnDataBound="ddDatabound"
                                              DataValueField="Code" 
                                              DataTextField="Name" />
                                     
                        </td>
                        <td class="iconCell">
                            <asp:TextBox ID="tbDuration" runat="server" CssClass="form-control small plainnumber" />
                        </td>
                        <td class="iconCell">
                            <asp:TextBox ID="tbPoint" runat="server" CssClass="form-control small plainnumber" />
                        </td>
                        <td class="iconCell">
                            <asp:CheckBox ID="cbShowTimer" runat="server" />
                        </td>
                        <td class="iconCell">                                     
                            <button class="btn" id="btnRemove" runat="server" onserverclick="btnRemove_ServerClick">
                                <i class="fa fa-trash-o text-primary"></i>
                            </button>
                        </td>
                    </tr>
            </ItemTemplate>
            <FooterTemplate>
                    <tr>
                        <td colspan="2">
                            <button class="btn btn-default" causesvalidation="false" onserverclick="btnAddRow_ServerClick" runat="server" 
                            id="btnAddRow">
                            <i class="fa fa-plus-circle"></i> Add Question
                        </button>
                        </td>
                    </tr>
                </table>
            </FooterTemplate>
        </asp:Repeater>
        <div class="row">
            <div class="col-sm-3">
                <button class="btn btn-primary" validationgroup="formPipeline"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
                        Save
                </button>
            </div>
        </div>
    </div>
    
<asp:ObjectDataSource ID="odsQuestionList" 
                        runat="server"
                        DataObjectTypeName="ERecruitment.Domain.Questions"
                        TypeName="ERecruitment.Domain.AssestmentManager"
                        SelectMethod="GetAssestmentQuestionList"
                        />
</asp:Content>
