﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="assestmentpreview.aspx.cs" Inherits="ERecruitment.assestmentpreview" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">
        $(document).ready(function () {

            loadAssestmentQuestionList();

        });

        function loadAssestmentQuestionList()
        {
            var dataId = GetQueryStringParams("assestmentCode");
            var handlerUrl = "../handlers/HandlerAssestments.ashx?commandName=GetAssestmentQuestionList&assestmentCode=" + dataId;
            var listContainer = $("#assestmentQuestionsContainer");
            $.ajax({
                url: handlerUrl,
                async: true,
                beforeSend: function () {
                    $(".disqualifierQuestionItem").remove();
                },
                success: function (queryResult) {

                    var dataModel = $.parseJSON(queryResult);
                    $.each(dataModel, function (i, item) {

                        // survey question wrapper
                        var questionWrapper = $(document.createElement('div')).attr("class", "row jobSection disqualifierQuestionItem");
                        // question section
                        var questionSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('p')).attr("class", "text-muted").text(item["Question"]));
                        questionWrapper.append(questionSection);

                        answerIndex = 0;
                        $.each(item["AnswerOptionList"], function (i, answerItem) {

                            
                            var answerSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");
                            var checkboxInput = $(document.createElement('div')).attr("class", "col-md-1 col-sm-1").append($(document.createElement('input')).attr("type", "radio").attr("name", "iscorrectanswer").attr("data-role", "value").attr("data-attribute", item["Code"]).attr("value", answerItem["Label"]));

                            var answerInput = $(document.createElement('div')).attr("class", "col-md-8 col-sm-8").append($(document.createElement('span')).text(answerItem["Label"]));

                            
                            answerSection.append(checkboxInput)
                                         .append(answerInput);
                            questionWrapper.append(answerSection);
                            answerIndex++;

                        });

                        if (item["AnswerFormat"] == "Numeric") {
                            // answer wrapper
                            var answerSection = $(document.createElement('div')).attr("class", "col-md-2 col-sm-2").append($(document.createElement('input')).attr("type", "text").attr("class", "form-control plainnumber").attr("data-attribute", item["Code"]));
                            questionWrapper.append(answerSection);
                        }

                        listContainer.append(questionWrapper);
                        disqualifierQuestionExist = true;
                    });
                    $(".plainnumber").keypress(
                     function (event) {

                         var fld = $(this);
                         var milSep = ",";
                         var decSep = ".";
                         var sep = 0;
                         var key = '';
                         var i = j = 0;
                         var len = len2 = 0;
                         var strCheck = '-0123456789';
                         var aux = aux2 = '';
                         var whichCode = (window.Event) ? event.which : event.keyCode;
                         if (whichCode == 46) return true; // Enter
                         if (whichCode == 13) return true; // Enter
                         if (whichCode == 8) return true; // Delete (Bug fixed)
                         if (whichCode == 0) return true; // tab (Bug fixed)
                         key = String.fromCharCode(whichCode); // Get key value from key code
                         if (strCheck.indexOf(key) == -1) return false; // Not a valid key

                     });
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });
        }
    </script>

    <div class="row">
        <div class="col-md-12 col-sm-12" id="assestmentQuestionsContainer">

        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <button type="button" class="btn btn-primary" onclick="commitApplyJob(); return false;">Submit</button>
        </div>
    </div>
</asp:Content>
