$(document).ready(function () {

    $('.datepicker').datepicker({
        dateFormat: "dd/mm/yy",
        changeMonth: true, 
        changeYear: true,
        changeDay: true,
        autoclose: true,
        yearRange: "-100:+100"
    });

    $('.timepicker').mask("99:99");

    function imgError(image) {
        image.onerror = "";
        image.src = "../assets/images/team4.jpg";
        return true;
    }

    $("body").find("input[data-attribute]:text").each(function (index, node) {
        $(node).popover({
            container: 'body',
            content: 'Please fill in this field',
            trigger: 'manual'
        });
        $(node).popover("hide");
    });

    $("body").find("input[data-attribute]:password").each(function (index, node) {
        $(node).popover({
            container: 'body',
            content: 'Please fill in this field',
            trigger: 'manual'
        });
        $(node).popover("hide");
    });

    $("body").find("textarea[data-attribute]").each(function (index, node) {
        $(node).popover({
            container: 'body',
            content: 'Please fill in this field',
            trigger: 'manual'
        });
        $(node).popover("hide");
    });

    $("select.form-control").chosen({ no_results_text: "Oops, nothing found!" });
    
    $(".numeric").keypress(
                     function (event) {

                         var fld = $(this);
                         var milSep = ",";
                         var decSep = ".";
                         var sep = 0;
                         var key = '';
                         var i = j = 0;
                         var len = len2 = 0;
                         var strCheck = '-0123456789';
                         var aux = aux2 = '';
                         var whichCode = (window.Event) ? event.which : event.keyCode;
                         if (whichCode == 46) return true; // Enter
                         if (whichCode == 13) return true; // Enter
                         if (whichCode == 8) return true; // Delete (Bug fixed)
                         if (whichCode == 0) return true; // tab (Bug fixed)
                         key = String.fromCharCode(whichCode); // Get key value from key code
                         if (strCheck.indexOf(key) == -1) return false; // Not a valid key

                     });


    $(".plainnumber").keypress(
                     function (event) {

                         var fld = $(this);
                         var milSep = ",";
                         var decSep = ".";
                         var sep = 0;
                         var key = '';
                         var i = j = 0;
                         var len = len2 = 0;
                         var strCheck = '-0123456789';
                         var aux = aux2 = '';
                         var whichCode = (window.Event) ? event.which : event.keyCode;
                         if (whichCode == 46) return true; // Enter
                         if (whichCode == 13) return true; // Enter
                         if (whichCode == 8) return true; // Delete (Bug fixed)
                         if (whichCode == 0) return true; // tab (Bug fixed)
                         key = String.fromCharCode(whichCode); // Get key value from key code
                         if (strCheck.indexOf(key) == -1) return false; // Not a valid key

                     });

    $(".numeric").blur(
    function (event) {
        $(this).val(formatMoney($(this).val()));
        
    });

    $(".numeric").focus(
    function (event) {
        $(this).val(deFormatMoney($(this).val()));
        $(this).select();
    });

    initiateConfirmation();

    setLangContext();
    $('.carousel').carousel();

    window.fbAsyncInit = function () {
        FB.init({
            appId: $("#hidFacebookApiKey").val(),
            xfbml: true,
            version: 'v2.10'
        });
        //FB.init({
        //    appId: '1022469384512533',
        //    xfbml: true,
        //    version: 'v2.6'
        //});
    };
});

function changeLang()
{
    var lang = $("#altLang").text();

    setCookie("lang", lang, 365);
    setLangContext();
}

function setLangContext()
{
    // default lang config
    var lang = "EN";
    var altLang = "ID";

    if (getCookie("lang") != "") {

        lang = getCookie("lang");
    }
    else {
        setCookie("lang", lang, 365);
    }

    if (lang == "EN")
        altLang = "ID";
    else
        altLang = "EN";

    // set main lang
    $("#mainLang").text(lang);
    // set alt lang
    $("#altLang").text(altLang);

    // load language labels
    var atsSite = extractHostname(window.location.href);
    var pageSite = "default.aspx";
    var param = "&atsSite=" + atsSite;
    param += "&pageSite=" + pageSite;
    param += "&lang=" + lang;

    var handlerUrl = "handlers/HandlerSettings.ashx?commandName=GetPageLanguageSettings" + param;

    $.ajax({
        url: handlerUrl,
        async: true,
        beforeSend: function () {

        },
        success: function (queryResult) {

            var jsonData = $.parseJSON(queryResult);
            $.each(jsonData, function (i, translatablecontent) {

                var attrName = translatablecontent["Label"];
                var attrValue = translatablecontent["ENText"];
                if (lang == "ID")
                    attrValue = translatablecontent["IDText"];

                if (attrValue != null){
                    
                    console.log(attrName + ":" + attrValue);

                    // assign input text field
                    $("body").find("input[data-lang = '" + attrName + "']").val(attrValue);
                    // assign anchor text
                    $("body").find("a[data-lang = '" + attrName + "']").text(attrValue);
                    // assign text area field
                    $("body").find("textarea[data-lang = '" + attrName + "']").val(attrValue);
                    // assign span text
                    $("body").find("span[data-lang = '" + attrName + "']").text(attrValue);
                    $("body").find("h1[data-lang = '" + attrName + "']").text(attrValue);
                    $("body").find("h2[data-lang = '" + attrName + "']").text(attrValue);
                    $("body").find("h3[data-lang = '" + attrName + "']").text(attrValue);
                    $("body").find("h4[data-lang = '" + attrName + "']").text(attrValue);
                    $("body").find("h5[data-lang = '" + attrName + "']").text(attrValue);
                    $("body").find("small[data-lang = '" + attrName + "']").text(attrValue);
                    $("body").find("p[data-lang = '" + attrName + "']").html(attrValue);
                }
            });
        },
        error: function (xhr, ajaxOptions, thrownError) {

            console.log("Error");
        }
    });

}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getNodeData(formName, formData) {
    var validFormData = true;
    $("#" + formName).find("input[data-attribute]:hidden").each(function (index, node) {
        formData[$(node).attr("data-attribute")] = $(node).val();
    });

    $("#" + formName).find("input[data-attribute]:text").each(function (index, node) {

        formData[$(node).attr("data-attribute")] = $(node).val();
        var requiredVal = $(node).attr("required");
        if (typeof requiredVal !== typeof undefined && requiredVal !== false) {
            if ($(node).val() == "") {
                $(node).popover("show");
                validFormData = false;
                return;
            }
            else {
                $(node).popover("hide");
            }
        }
    });

    $("#" + formName).find("input[data-attribute]:password").each(function (index, node) {
        formData[$(node).attr("data-attribute")] = $(node).val();
        var requiredVal = $(node).attr("required");
        if (typeof requiredVal !== typeof undefined && requiredVal !== false) {
            if ($(node).val() == "") {
                $(node).popover("show");
                validFormData = false;
                return;
            }
            else {
                $(node).popover("hide");
            }
        }
    });

    $("#" + formName).find("textarea[data-attribute]").each(function (index, node) {
        formData[$(node).attr("data-attribute")] = $(node).val();
        var requiredVal = $(node).attr("required");
        if (typeof requiredVal !== typeof undefined && requiredVal !== false) {
            if ($(node).val() == "") {
                $(node).popover("show");
                validFormData = false;
                return;
            }
            else {
                $(node).popover("hide");
            }
        }
    });

    $("#" + formName).find("select[data-attribute]").each(function (index, node) {
        formData[$(node).attr("data-attribute")] = $(node).val();
        var requiredVal = $(node).attr("required");
        if (typeof requiredVal !== typeof undefined && requiredVal !== false) {
            if ($(node).val() == "") {
                $(node).popover("show");
                validFormData = false;
                return;
            }
            else {
                $(node).popover("hide");
            }
        }
    });

    $("#" + formName).find("input[data-attribute]:checkbox").each(function (index, node) {
        formData[$(node).attr("data-attribute")] = $(node).prop("checked");
    });

    $("#" + formName).find("input[data-attribute]:radio").each(function (index, node) {

        var attr = $(node).attr('data-role');

        if (typeof attr == typeof undefined || attr == false) {

            formData[$(node).attr("data-attribute")] = $(node).prop("checked");
        }
        else {
            if ($(node).attr("data-role") == "value") {
                if ($(node).prop("checked")) {
                    formData[$(node).attr("data-attribute")] = $(node).attr("value");
                }
            }
            else {
                formData[$(node).attr("data-attribute")] = $(node).prop("checked");
            }
        }
    });

    return validFormData;
}

function loadDataIntoForm(form, jsonData) {

    for (var key in jsonData) {

        var attrName = key;
        var attrValue = jsonData[key];
        if (attrValue == null)
            attrValue = "";

        if (attrValue.toString().indexOf("/Date") !== -1)
            attrValue = displayJsonDate(attrValue.toString());

        // assign input text field
        $("#" + form).find("input[data-attribute = '" + attrName + "']").val(attrValue);
        // assign input numeric field
        $("#" + form).find("input.numeric[data-attribute = '" + attrName + "']").val(formatMoney(attrValue));
        // assign text area field
        $("#" + form).find("textarea[data-attribute = '" + attrName + "']").val(attrValue);
        // assign span text
        $("#" + form).find("span[data-attribute = '" + attrName + "']").text(attrValue);
        // assign select field
        $("#" + form).find("select[data-attribute = '" + attrName + "']").val(attrValue).trigger("chosen:updated");
        // assign checkbox field
        $("#" + form).find("input[data-attribute = '" + attrName + "']:checkbox").prop("checked", attrValue);
        // assign checkbox field
        $("#" + form).find("input[data-attribute = '" + attrName + "']:radio").prop("checked", attrValue);
        // assign span text
        $("#" + form).find("span[data-attribute = '" + attrName + "'].numeric").text(formatMoney(attrValue));

    }
}

function initiateConfirmation()
{

    $('[data-toggle=confirmation]').confirmation({
        rootSelector: '[data-toggle=confirmation]',
        onConfirm: function (event) {

        }
    });
}

function formatMoney(cellValue) {

    var returnValue = cellValue;

    var number = cellValue;
    var negative = (number < 0);
    number = Math.abs(number);
    number = parseInt((number + .005) * 100); // parseInt((number + .005) * 100);
    number = number / 100;
    returnValue = new String(number);
    if (negative) returnValue = "-" + returnValue;
    returnValue = addCommas(returnValue);//"$" + returnValue; edited by yudi
    return returnValue;
}

function deFormatMoney(amount) {
    try {
        if (amount != undefined && amount != null && amount != "") {
            var originalValue = amount.toString();
            var minus = false;
            if (originalValue.indexOf("(") != -1 || originalValue.indexOf("-") != -1)
                minus = true;
            var valueString = originalValue.toString();
            if (!minus)
                return Number(valueString.replace(/[^0-9\.]+/g, "").replace(/,/, ""));
            else {
                return 0 - Number(valueString.replace(/[^0-9\.]+/g, "").replace(/,/, ""));
            }
        }
        return "0";
    }
    catch (err) {
        return "0";
    }
}

function addCommas(nStr) {

    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function GetQueryStringParams(sParam) {
    var url = window.location.href;
    if (url.split('?').length > 1) {
        var key = url.split('?')[1];
        var sURLVariables = key.split('&');

        for (var i = 0; i < sURLVariables.length; i++) {
            var sParameterName = sURLVariables[i].split('=');
            if (sParameterName[0] == sParam) {
                return sParameterName[1];
            }
        }
    }
    return "";
}

function extractHostname(url) {
    var hostname;
    //find & remove protocol (http, ftp, etc.) and get the hostname
    if (url.indexOf("://") > -1) {
        hostname = url.split('/')[2];
    }
    else {
        hostname = url.split('/')[0];
    }

    //find & remove port number
    hostname = hostname.split(':')[0];

    return hostname;
}

function showNotification(notificationMessage, notificationType)
{
    //window.alert(notificationMessage);
    if (notificationType == undefined) {
        notificationType = "information";
    }

    var n = noty({
        text: notificationMessage,
        type: notificationType,
        dismissQueue: true,
        layout: "topCenter",
        theme: 'defaultTheme'
    });
}

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) { return; }
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));



function shareFacebook(url, caption) {
    var currentUrl = url;
    FB.ui({
        method: 'share',
        href: url,
        title: caption
    }, function (response) { });
}

function shareTwitter(url, caption) {
    var left = (screen.width / 2) - (800 / 2);
    var top = (screen.height / 2) - (450 / 2);
    var currentUrl = url;
    window.open("https://twitter.com/share?url=" + currentUrl + "&text=" + caption + " - ", 'Tweet', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=800, height=490, top=' + top + ', left=' + left);
}

function shareWhatsApp(url, caption) {
    var left = (screen.width / 2) - (800 / 2);
    var top = (screen.height / 2) - (450 / 2);
    var currentUrl = caption + " - " + url;
    window.open("whatsapp://send?text=" + currentUrl, 'Whatsapp', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=800, height=490, top=' + top + ', left=' + left);
}


function displayJsonDate(jsonDate)
{
    
    var dateString = jsonDate.substr(6);
    
    var currentTime = new Date(parseInt(dateString));
    
    var month = currentTime.getMonth() + 1;
    if (month < 10)
        month = "0" + month.toString();
    var day = currentTime.getDate();
    if (day < 10)
        day = "0" + day.toString();
    var year = currentTime.getFullYear();
    var date = day + "/" + month + "/" + year;
    return date;
}

function highlightNavigationContainer(linkId)
{
    $("#" + linkId).parent().addClass("active");
}

function hideElement(elementSelector) {
    $(elementSelector).hide();
}

