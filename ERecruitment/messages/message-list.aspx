﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="message-list.aspx.cs" Inherits="ERecruitment.message_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


      <script type="text/javascript">
        $(document).ready(function () {

            loadData();
        });
        function showForm(code) {

            var left = (screen.width / 2) - (800 / 2);
            var top = (screen.height / 2) - (450 / 2);
            var urlPage = $("#<%= hidUrl.ClientID %>").val();
            urlPage = urlPage + "?id=" + code;
            window.open(urlPage, 'Message', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=800, height=490, top=' + top + ', left=' + left);
        }
        function Remove() {

            var idList = "";
            var tableData = $('#tableData').dataTable();
            $('input:checked', tableData.fnGetNodes()).each(function () {

                idList += (tableData.fnGetData($(this).closest('tr')[0])[3] + ',');
            });
            if (idList == "")
                showNotification("please select template");
            else {

                if (confirm("Are you sure want to delete ? ")) {
                    $.ajax({
                        url: "../handlers/HandlerSettings.ashx?commandName=RemoveSMSTemplate&ids=" + idList,
                        async: true,
                        beforeSend: function () {

                        },
                        success: function (queryResult) {
                            showNotification("message removed");
                            refresh();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            showNotification(xhr.responseText);
                            refresh();
                        }
                    });
                }
            }

        }

        function refresh() {
            var tableData = $('#tableData').dataTable();
            tableData.fnDraw();
        }

        function Add() {

            var left = (screen.width / 2) - (800 / 2);
            var top = (screen.height / 2) - (450 / 2);
            var foo = window.open('message-form.aspx', 'Message', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=800, height=490, top=' + top + ', left=' + left);

        }
        function loadData() {
            var ex = document.getElementById('tableData');
            if ($.fn.DataTable.fnIsDataTable(ex)) {
                // data table, then destroy first
                $("#tableData").dataTable().fnDestroy();
            }

            var OTableData = $('#tableData').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "bJQueryUI": true,
                "bAutoWidth": false,
                "sDom": "ftipr",
                "bDeferRender": true,
                "aoColumnDefs": [
                       { "bSortable": false, "aTargets": [0] },
                       { "bVisible": false, "aTargets": [3] },
                       { "sClass": "controlIcon", "aTargets": [0] }

                ],
                "oLanguage":
                                { "sSearch": "Search By Template Name" },
                "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetSMSTemplateList"
            });
        }


    </script>

  <div>
    <asp:HiddenField ID="hidUrl" runat="server" />
    <div>

        <h3>Message List</h3>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <button class="btn btn-primary" onclick="Add(); return false;" id="btnNew">
                                        Create New
                    </button>
                    <button class="btn btn-primary" onclick="Remove();return false;">&nbsp;&nbsp;Delete&nbsp;&nbsp;</button>
                </div>
            </div>
        </div>

         <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
        <tr>
            <th></th>
            <th><label runat="server" id="lbMessageList_Template">Template Name</label></th>
            <th><label runat="server" id="lbMessageList_Category">Category</label>   </th>
           <th></th>
        </tr>
        </thead>
        <tbody>
           
        </tbody>
            </table>
          
        </div>
      </div>




</asp:Content>
