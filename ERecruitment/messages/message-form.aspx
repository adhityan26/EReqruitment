﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/popupform.Master" AutoEventWireup="true" CodeBehind="message-form.aspx.cs" Inherits="ERecruitment.message_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row">
          <div class="col-sm-12">
              <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbMessage_TemplateName">Template Name</label>
                    </div>
                    <div class="col-sm-5">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbTemplateName"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                                        
                    </div>  
              </div>
              
              <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbMessage_Category">Category</label>
                    </div>
                    <div class="col-sm-5">
                         <select id="ddCategory" 
                                runat="server" class="form-control">
                             <option value="ApplicantNotification">Applicant Notification</option>
                             <option value="RecruiterNotification">Recruiter Notification</option>
                         </select>
                    </div>  
              </div>
              <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbMessage_Subject">Subject</label>
                    </div>
                    <div class="col-sm-5">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbSubject"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                    </div>  
              </div>
               
              <div class="row">
                   <div class="col-sm-3">
                        <label runat="server" id="lbMessage_Content">Content</label>
                    </div>
                   <div class="col-sm-8">
                 <textarea cols="55" rows="8" disallowed-chars="[^a-zA-Zs ]+" id="tbContent" 
                          name="name" ng-model="registerFormData.name" form-field="registerForm"
                          ng-minlength="3" min-length="3" required="" runat="server"
                          class="form-control" />
                                
                  </div>
              </div>

             <div class="row">
                 <div class="col-sm-3">
                
                    </div>
              <div class="col-sm-3">
                   <button class="btn btn-primary"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
                         Save
                    </button>
               </div>
              </div>
              
              <div class="row">
                   <div class="col-sm-3">
                        <label>Update Status : </label>
                    </div>
                    <div class="col-sm-3">
                        <label>Schedulling : </label>
                    </div>
                  <div class="col-sm-3">
                        <label>Offering : </label>
                    </div>
              </div>
               <div class="row">
                   <div class="col-sm-3">
                        <label>[Name] = user</label>
                    </div>
                   <div class="col-sm-3">
                        <label>[Name] = user</label>
                    </div>
                   <div class="col-sm-3">
                        <label>[Name] = user</label>
                    </div>
              </div>
                <div class="row">
                   <div class="col-sm-3">
                        <label>[Status] = status</label>
                    </div>
                    <div class="col-sm-3">
                        <label>[Date] = date</label>
                    </div>
                    <div class="col-sm-3">
                        <label>[AnnualSalary] = annual salary</label>
                    </div>
              </div>
                 <div class="row">
                   <div class="col-sm-3">
                        <label>[Company] = company</label>
                    </div>
                    <div class="col-sm-3">
                        <label>[TimeStart] = time start</label>
                    </div>
                     <div class="col-sm-3">
                        <label>[MonthlySalary] = monthly salary</label>
                    </div>
              </div>
               <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label>[TimeEnd] = time end</label>
                    </div>
                     <div class="col-sm-3">
                        <label>[THR] = thr</label>
                    </div>
              </div>
                 <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label>[Place] = place</label>
                    </div>
                     <div class="col-sm-3">
                        <label>[Leave] = leave</label>
                    </div>
              </div>
                 <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label>[PIC] = pic</label>
                    </div>
                     <div class="col-sm-3">
                        <label>[Transportation] = transportation</label>
                    </div>
              </div>
                 <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label>[Notes] = notes</label>
                    </div>
                     <div class="col-sm-3">
                        <label>[Telecommunication] = telecommunication</label>
                    </div>
              </div>
               <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label></label>
                    </div>
                     <div class="col-sm-3">
                        <label>[Bonus] = bonus</label>
                    </div>
              </div>
               <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label></label>
                    </div>
                     <div class="col-sm-3">
                        <label>[Incentive] = incentive</label>
                    </div>
              </div>
                  <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label></label>
                    </div>
                     <div class="col-sm-3">
                        <label>[OtherAllowance1] = other allowance 1</label>
                    </div>
              </div>
                  <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label></label>
                    </div>
                     <div class="col-sm-3">
                        <label>[OtherAllowance2] = other allowance 2</label>
                    </div>
              </div>
                  <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label></label>
                    </div>
                     <div class="col-sm-3">
                        <label>[OtherAllowance3] = other allowance 3</label>
                    </div>
              </div>
                    <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label></label>
                    </div>
                     <div class="col-sm-3">
                        <label>[Notes] = notes</label>
                    </div>
              </div>
                    <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label></label>
                    </div>
                     <div class="col-sm-3">
                        <label>[Company] = company</label>
                    </div>
              </div>
             </div>
           </div>
<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                       SelectMethod="GetVacancyCategoryList"
    />


</asp:Content>
