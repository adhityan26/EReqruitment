﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;
namespace ERecruitment
{
    public partial class message_form : AuthorizedPage

    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (Utils.GetQueryString<int>("id") > 0)
                    IniateForm();
        }
        private void IniateForm()
        {
            SMSTemplates getTemplate = ERecruitmentManager.GetSMSTemplate(Utils.GetQueryString<int>("id"));
            if (getTemplate != null)
            {
                tbContent.Value = getTemplate.Body;
                tbTemplateName.Value = getTemplate.TemplateName;
                tbSubject.Value = getTemplate.Subject;
                ddCategory.Value = getTemplate.Category;
            }
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            if (Utils.GetQueryString<int>("id") > 0)
            {
                SMSTemplates getTemplate = ERecruitmentManager.GetSMSTemplate(Utils.GetQueryString<int>("id"));
                if (getTemplate != null)
                {
                    getTemplate.TemplateName = tbTemplateName.Value;
                    getTemplate.Body = tbContent.Value;
                    getTemplate.Subject = tbSubject.Value;
                    getTemplate.Category = ddCategory.Value;
                    ERecruitmentManager.UpdateSMSTemplate(getTemplate);
                    JQueryHelper.InvokeJavascript("showNotification('Successfully updated');window.close();window.opener.refresh();", Page);
                }
            }
            else
            {
                SMSTemplates addTemplate = new SMSTemplates();
                addTemplate.TemplateName = tbTemplateName.Value;
                addTemplate.Body = tbContent.Value;
                addTemplate.Subject = tbSubject.Value;
                addTemplate.Category = ddCategory.Value;
                addTemplate.Active = true;
                ERecruitmentManager.SaveSMSTemplate(addTemplate);
                JQueryHelper.InvokeJavascript("showNotification('Successfully saved');window.close();window.opener.refresh();", Page);
            }
        }
    }
}