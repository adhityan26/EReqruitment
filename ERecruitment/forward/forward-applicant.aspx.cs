﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class forward_applicant: AuthorizedPage
    {
        #region init
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                // bind other active vacancy
                liActiveVacancyList.DataSource = ERecruitmentManager.GetOpenVacancyList(tbSearchVacancy.Value);
                liActiveVacancyList.DataValueField = "Code";
                liActiveVacancyList.DataTextField = "DisplayName";
                liActiveVacancyList.DataBind();
               
            }
        }
        #endregion

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            string selectedVacancyCode = liActiveVacancyList.SelectedItem.Value;
            if (string.IsNullOrEmpty(selectedVacancyCode))
            {
                JQueryHelper.InvokeJavascript("showNotification('Please select a vacancy');", Page);
                return;
            }
            else
            {
                ApplicantVacancies currentApplicantVacancy = ERecruitmentManager.GetApplicantVacancies(Utils.GetQueryString<string>("applicantCode"), Utils.GetQueryString<string>("vacancyCode"));

                ApplicantVacancies forwardApplicantVacancy = new ApplicantVacancies();
                forwardApplicantVacancy.VacancyCode = selectedVacancyCode;
                forwardApplicantVacancy.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                forwardApplicantVacancy.CompanyCode = currentApplicantVacancy.CompanyCode;
                forwardApplicantVacancy.AppliedDate = currentApplicantVacancy.AppliedDate;

                ApplicantVacancies checkForwardedJob = ERecruitmentManager.GetApplicantVacancies(Utils.GetQueryString<string>("applicantCode"), selectedVacancyCode);
                if (checkForwardedJob != null)
                {
                    JQueryHelper.InvokeJavascript("showNotification('Applicant already applied to this job');", Page);
                    return;
                }

                ERecruitmentManager.ForwardApplicantVacany(currentApplicantVacancy, forwardApplicantVacancy);

                JQueryHelper.InvokeJavascript("showNotification('Applicant forwarded'); window.opener.location.href = window.opener.location.href; window.close();", Page);
            }
        }

        protected void liActiveVacancyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            liActiveVacancyList.DataSource = ERecruitmentManager.GetOpenVacancyList(tbSearchVacancy.Value);
            liActiveVacancyList.DataValueField = "Code";
            liActiveVacancyList.DataTextField = "DisplayName";
            liActiveVacancyList.DataBind();
        }
    }
}