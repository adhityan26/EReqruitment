﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;
namespace ERecruitment
{
    public partial class email_form: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                if (Utils.GetQueryString<int>("id") > 0)
                    IniateForm();
        }
        private void IniateForm()
        {
            Emails getEmail = ERecruitmentManager.GetEmail(Utils.GetQueryString<int>("id"));
            if (getEmail != null)
            {
                tbContentBody.Text = getEmail.Body;
                tbTemplateName.Value = getEmail.TemplateName;
                tbSubject.Value = getEmail.Subject;
            }
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            if (Utils.GetQueryString<int>("id") > 0)
            {
                Emails getEmail = ERecruitmentManager.GetEmail(Utils.GetQueryString<int>("id"));
                if (getEmail != null)
                {
                    getEmail.TemplateName = tbTemplateName.Value;
                    getEmail.Body = tbContentBody.Text;
                    getEmail.Subject = tbSubject.Value;
                    ERecruitmentManager.UpdateEmail(getEmail);
                    JQueryHelper.InvokeJavascript("showNotification('Successfully updated');backToList();", Page);
                }
            }
            else
            {
                Emails email = new Emails();
                email.TemplateName = tbTemplateName.Value;
                email.Body = tbContentBody.Text;
                email.Subject = tbSubject.Value;
                email.Active = true;
                ERecruitmentManager.SaveEmail(email);
                JQueryHelper.InvokeJavascript("showNotification('Successfully saved');backToList();", Page);
            }
        }

        protected void btnPreview_ServerClick(object sender, EventArgs e)
        {

        }

        protected void btnSendTest_ServerClick(object sender, EventArgs e)
        {
            Companies company = ERecruitmentManager.GetCompany(MainCompany.Code);

            var title = "Mr.";
            var name = "Applicant";
            var status = "Success";
            var date = "Monday, 1 January 2016";
            var monthlySalary = 1000000;
            var timeStart = "08:00";
            var bonus = "Bonus";
            var timeEnd = "17:00";
            var notes = "Notes";
            var place = "Office";
            var pic = "Admin";
            var thr = "THR";
            var incentives = "Incentive";
            var approve = "<a href='#'>Approve</a>";
            var reject = "<a href='#'>Reject</a>";
            var revise = "<a href='#'>Revise</a>";

            var content = tbContentBody.Text
                .Replace("[Title]", title)
                .Replace("[Name]", name)
                .Replace("[Status]", status)
                .Replace("[Date]", date)
                .Replace("[MonthlySalary]", Utils.DisplayMoneyAmount(monthlySalary))
                .Replace("[Company]", company.Name)
                .Replace("[Time Start]", timeStart)
                .Replace("[Bonus]", bonus)
                .Replace("[Time End]", timeEnd)
                .Replace("[Notes]", notes)
                .Replace("[Place]", place)
                .Replace("[PIC]", pic)
                .Replace("[THR]", thr)
                .Replace("[Incentives]", incentives)
                .Replace("[Approve]", approve)
                .Replace("[Revise]", revise)
                .Replace("[Reject]", reject);

            ERecruitmentAppSettings.SendEmail(company, tbTestEmailAddress.Value, tbSubject.Value, content, true);
        }
    }
}