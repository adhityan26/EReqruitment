﻿<%@ Page Title="Email Template" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="email-form.aspx.cs" Inherits="ERecruitment.email_form" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">


        function backToList() {
            window.location.href = "email-list.aspx";
        }

        $(document).ready(function () {
            CKEDITOR.config.toolbar_Custom =
                [
                    { name: 'document', items: ['Source'] },
                    { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
                    { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
                    '/',
                    { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
                    {
                        name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                            '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
                    },
                    '/',
                    { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
                    { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
                    '/',
                    { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
                    { name: 'colors', items: ['TextColor', 'BGColor'] },
                    { name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'About'] }
                ];

            CKEDITOR.config.toolbar = "Custom";

            $("#btnTest").click(function () {
                var title = "Title";
                var name = "Applicant";
                var status = "Status";
                var date = "Monday, 1 January 2016";
                var monthlySalary = 1000000;
                var company = "Company";
                var timeStart = "08:00";
                var bonus = "Bonus";
                var timeEnd = "17:00";
                var notes = "Notes";
                var place = "Office";
                var pic = "Admin";
                var thr = "THR";
                var incentives = "Incentive";
                var approve = "<a href='#'>Approve</a>";
                var reject = "<a href='#'>Reject</a>";
                var revise = "<a href='#'>Revise</a>";

                var content = CKEDITOR.instances['ContentPlaceHolder1_tbContentBody'].getData()
                    .replace(/\[Title\]/ig, title)
                    .replace(/\[Name]/ig, name)
                    .replace(/\[Status]/ig, status)
                    .replace(/\[Date]/ig, date)
                    .replace(/\[MonthlySalary]/ig, monthlySalary)
                    .replace(/\[Company]/ig, company)
                    .replace(/\[Time Start]/ig, timeStart)
                    .replace(/\[Bonus]/ig, bonus)
                    .replace(/\[Time End]/ig, timeEnd)
                    .replace(/\[Notes]/ig, notes)
                    .replace(/\[Place]/ig, place)
                    .replace(/\[PIC]/ig, pic)
                    .replace(/\[THR]/ig, thr)
                    .replace(/\[Incentives]/ig, incentives)
                    //.replace(/\n/g, '<br>')
                    .replace(/\[Approve]/ig, approve)
                    .replace(/\[Revise]/ig, revise)
                    .replace(/\[Reject]/ig, reject);

                $("#previewContent").html(content);
            });
        });
    </script>
     <div class="row">
          <div class="col-sm-12">
              <div class="row">
                    <div class="col-sm-3">
                        <strong><span data-lang="TemplateNameFormInput">Template Name</span></strong><span class="requiredmark">*</span>
                    </div>
                    <div class="col-sm-5">
                        <textarea disallowed-chars="[^a-zA-Zs ]+" id="tbTemplateName"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" maxlength="150" max-length="150" required="" runat="server"
                        class="form-control"></textarea>
                                        
                    </div>  
              </div>
              <div class="row">
                    <div class="col-sm-3">
                        <strong><span data-lang="SubjectFormInput">Subject</span></strong><span class="requiredmark">*</span>
                    </div>
                    <div class="col-sm-5">
                        <textarea disallowed-chars="[^a-zA-Zs ]+" id="tbSubject"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" maxlength="150" max-length="150" required="" runat="server"
                        class="form-control"></textarea>
                    </div>  
              </div>
               
              <div class="row">
                   <div class="col-sm-3">
                        <strong><span data-lang="ContentFormInput">Content</span></strong><span class="requiredmark">*</span>
                    </div>
                   <div class="col-sm-8">
                       <CKEditor:CKEditorControl id="tbContentBody" form-field="registerForm" BasePath="/assets/plugins/ckeditor/" ng-model="registerFormData.name" runat="server"></CKEditor:CKEditorControl>
                 <%--<textarea cols="55" rows="8" disallowed-chars="[^a-zA-Zs ]+" id="tbContent" 
                          name="name" ng-model="registerFormData.name" form-field="registerForm"
                          ng-minlength="3" min-length="3" required="" runat="server"
                          class="form-control" />--%>
                                
                  </div>
              </div>

             <div class="row">
                 <div class="col-sm-3">
                
                    </div>
              <div class="col-sm-3">
                   <button class="btn btn-primary"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
                         Save
                   </button>
                   <button type="button" class="btn btn-primary" id="btnTest" data-target="#formTestEmail" data-toggle="modal" >
                         Preview
                   </button>
               </div>
              </div>
              <div class="row">
                   <div class="col-sm-3">
                        <strong><span data-lang="LabelUpdateStatus">Update Status :</span></strong>
                    </div>
                    <div class="col-sm-3">
                        <strong><span data-lang="LabelSchedulling">Schedulling :</span></strong>
                    </div>
                  <div class="col-sm-3">
                        <strong><span data-lang="LabelOffering">Offering :</span></strong>
                    </div>
              </div>
               <div class="row">
                   <div class="col-sm-3">
                        <label runat="server" id="Label4">[Title] = position's name</label>
                    </div>
                   <div class="col-sm-3">
                        <label runat="server" id="Label5">[Title] = position's name</label>
                    </div>
                   <div class="col-sm-3">
                        <label runat="server" id="Label6">[Title] = position's name</label>
                    </div>
              </div>
               <div class="row">
                   <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_UserStatus">[Name] = candidate's name</label>
                    </div>
                   <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_UserSchedule">[Name] = candidate's name</label>
                    </div>
                   <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_UserOffering">[Name] = candidate's name</label>
                    </div>
              </div>
                <div class="row">
                   <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_StatusChange">[Status] = status</label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_ScheduleData">[Date] = date</label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_MonthlSalary">[MonthlySalary] = monthly salary</label>
                    </div>
              </div>
                 <div class="row">
                   <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_StatusCompany">[Company] = company</label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_TimeStartSchedule">[Time Start] = time start</label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="Label1">[Bonus] = Bonus</label>
                    </div>
              </div>
               <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_TimeEndSchedule">[Time End] = time end</label>
                    </div>
                     <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_OfferingNotes">[Notes] = notes</label>
                    </div>
              </div>
                 <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbPlaceSchedule">[Place] = place</label>
                    </div>
                     <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_OfferingCompany">[Company] = company</label>
                    </div>
              </div>
                 <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_Pic">[PIC] = pic</label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="Label3">[THR] = THR</label>
                    </div>
              </div>
              <div class="row">
                   <div class="col-sm-3">
                        <label></label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbEmailForm_NotesSchedule">[Notes] = notes</label>
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="Label2">[Incentives] = Incentives</label>
                    </div>
              </div>
             </div>
           </div>
<div class="modal fade" id="formTestEmail">
    <div class="modal-dialog" style="width: 70%">
		<div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h1 class="modal-title center">Email Preview</h1>
		</div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-12">
                    <div id="previewContent">

                    </div>
                </div>
            </div>
            <div class="row">
                <hr />
            </div>
            <div class="row">
                <div class="col-sm-3">Email Address</div>
                <div class="col-sm-9"><input type="text" class="form-control" id="tbTestEmailAddress" runat="server" /></div>
            </div>
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <a class="btn btn-default" runat="server" onserverclick="btnSendTest_ServerClick">Test Email</a>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                       SelectMethod="GetVacancyCategoryList"
    />
 </asp:Content>
