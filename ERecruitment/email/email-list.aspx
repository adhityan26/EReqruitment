﻿<%@ Page Title="Email Templates" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="email-list.aspx.cs" Inherits="ERecruitment.email_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        $(document).ready(function () {

            loadData();
        });
        function showForm(code) {

            var left = (screen.width / 2) - (800 / 2);
            var top = (screen.height / 2) - (450 / 2);
            var urlPage = $("#<%= hidUrl.ClientID %>").val();
            urlPage = urlPage + "?id=" + code;
            window.location.href = urlPage;
        }
        function Remove() {

            var idList = "";
            var tableData = $('#tableData').dataTable();
            $('input:checked', tableData.fnGetNodes()).each(function () {

                idList += (tableData.fnGetData($(this).closest('tr')[0])[3] + ',');
            });
            if (idList == "")
                showNotification("please select email");
            else {
                confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
                    $.ajax({
                        url: "../handlers/HandlerSettings.ashx?commandName=RemoveEmails&ids=" + idList,
                        async: true,
                        beforeSend: function () {

                        },
                        success: function (queryResult) {
                            showNotification("email removed");
                            refresh();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            showNotification(xhr.responseText);
                            refresh();
                        }
                    });
                });
            }

        }

        function refresh() {
            var tableData = $('#tableData').dataTable();
            tableData.fnDraw();
        }

        function Add() {

            window.location.href = "email-form.aspx";
        }

        function loadData() {
            var ex = document.getElementById('tableData');
            if ($.fn.DataTable.fnIsDataTable(ex)) {
                // data table, then destroy first
                $("#tableData").dataTable().fnDestroy();
            }

            var OTableData = $('#tableData').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "bJQueryUI": true,
                "bAutoWidth": false,
                "sDom": "ftipr",
                "bDeferRender": true,
                "aoColumnDefs": [
                       { "bSortable": false, "aTargets": [0] },
                       { "bVisible": false, "aTargets": [2,3] },
                       { "sClass": "controlIcon", "aTargets": [0] }

                ],
                "oLanguage":
                                { "sSearch": "Search By Name" },
                "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetEmailList"
            });
        }


    </script>

  
<asp:HiddenField ID="hidUrl" runat="server" />
    
    
<div class="row">
    <div class="col-sm-12">
        <button class="btn btn-default commandIcon" onclick="Add(); return false;" id="btnNew"><i class="fa fa-file"></i>New</button>
        <button class="btn btn-default commandIcon" onclick="Remove();return false;" type="button"><i class="fa fa-trash"></i>Delete</button>
    </div>
</div>
<table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
    <thead>
        <tr>
            <th></th>
            <th><strong><span data-lang="TemplateNameTitle">Template Name</span></strong></th>
            <th><strong><span data-lang="CategoryTemplateTitle">Category</span></strong></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
           
    </tbody>
</table>


</asp:Content>
