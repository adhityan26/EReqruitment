﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FastReport.Web;
using FastReport;
using System.IO;
using System.Web.Hosting;
using System.Configuration;
using ERecruitment.Domain;
using SS.Web.UI;


namespace ERecruitment
{
    public partial class reportgen : System.Web.UI.Page
    {

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);

            Reports eReport = ERecruitment.Domain.ApplicationManager.GetReport(Utils.GetQueryString<string>("code"));
            string reportFileName = eReport.ReportFile;
            Report report = new Report();
            report.Load(ConfigurationManager.AppSettings["ReportPath"] + reportFileName);

            #region setting parameter
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("param")))
            {
                string[] paramList = Utils.GetQueryString<string>("param").Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < paramList.Length; i++)
                {
                    string[] parameterPair = paramList[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                    string type = parameterPair[2];
                    switch (type)
                    {
                        case "int":
                            report.SetParameterValue(parameterPair[0], Utils.ConvertString<int>(parameterPair[1]));
                            break;
                        case "decimal":
                            report.SetParameterValue(parameterPair[0], Utils.ConvertString<decimal>(parameterPair[1]));
                            break;
                        case "bool":
                            report.SetParameterValue(parameterPair[0], Utils.ConvertString<bool>(parameterPair[1]));
                            break;
                        case "date":

                            //string[] dateComponent = Utils.DisplayDateTime(Utils.ConvertToDateTime(parameterPair[1])).Split('/');
                            string[] dateComponent = parameterPair[1].Split('/');
                            DateTime date = new DateTime(Utils.ConvertString<int>(dateComponent[2]), Utils.ConvertString<int>(dateComponent[1]), Utils.ConvertString<int>(dateComponent[0]));
                            if (parameterPair[0].IndexOf("end", StringComparison.OrdinalIgnoreCase) > -1)
                            {
                                date = date.AddHours(23);
                                date = date.AddMinutes(59);
                                date = date.AddSeconds(59);   
                            }
                            report.SetParameterValue(parameterPair[0], date);
                            break;
                        default:
                            report.SetParameterValue(parameterPair[0], parameterPair[1]);
                            break;
                    }

                }
            }
            #endregion
            report.Dictionary.Connections[0].ConnectionString = ConfigurationManager.ConnectionStrings["ERecruitmentConnectionString"].ConnectionString;
            report.Dictionary.Connections[0].CommandTimeout = 3600;
            report.Prepare();

            FastReport.Export.OoXML.Excel2007Export xlsExport = new FastReport.Export.OoXML.Excel2007Export();

            using (MemoryStream strm = new MemoryStream())
            {
                report.Export(xlsExport, strm);

                // Stream the PDF back to the client as an attachment
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + eReport.Name + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx\"");

                strm.Position = 0;
                strm.WriteTo(Response.OutputStream);
                Response.End();

                JQueryHelper.InvokeJavascript("window.close();", Page);
            }
        }
    }
}