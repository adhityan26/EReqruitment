﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class applicant_vacancy : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidFacebookApiKey.Value = MainCompany.FacebookAPIKey;
                candidates.HRef = "../recruiter/detail-application.aspx?showArchives=true&vacancyCode=" + Utils.GetQueryString<string>("vacancyCode");
                //assestment.HRef = "../assestments/assestmentsessions.aspx?vacancyCode=" + Utils.GetQueryString<string>("vacancyCode");
                batchProcess.HRef = "../recruiter/batchprocess.aspx?vacancyCode=" + Utils.GetQueryString<string>("vacancyCode");
                rankQualification.HRef = "../recruiter/vacancy-rank-qualifications.aspx?vacancyCode=" + Utils.GetQueryString<string>("vacancyCode");
                repPages.DataSource = PageManager.GetRecruiterAdminPageList();
                repPages.DataBind();

                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("vacancyCode")))
                {
                    hidVacancyCode.Value = Utils.GetQueryString<string>("vacancyCode");
                    Vacancies getVacancy = ERecruitmentManager.GetVacancy(hidVacancyCode.Value);
                    if (getVacancy != null)
                        ltJob.Text = getVacancy.PositionName;
                    if (!string.IsNullOrEmpty(getVacancy.PipelineCode))
                    {
                        Pipelines getPipeline = HiringManager.GetPipeline(getVacancy.PipelineCode);
                        if (getPipeline != null)
                        {

                            List<PipelineSteps> pipelineSteps = HiringManager.GetPipelineStepList(getPipeline.Code);
                            repPipeline.DataSource = pipelineSteps;
                            repPipeline.DataBind();
                        }
                    }
                }
            }
        }

        protected void repPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Repeater repSubPages = e.Item.FindControl("repSubPages") as Repeater;
                Pages page = e.Item.DataItem as Pages;
                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                if (menuLink != null)
                {
                    menuLink.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
                    List<Pages> subPageList = PageManager.GetChildrenPageList(page.ID);
                    if (subPageList.Count > 0)
                    {
                        if (repSubPages != null)
                        {
                            repSubPages.DataSource = PageManager.GetChildrenPageList(page.ID);
                            repSubPages.DataBind();
                        }
                        menuLink.Attributes.Add("class", "dropdown-toggle");
                        menuLink.Attributes.Add("data-toggle", "dropdown");
                    }
                }
            }
        }

        protected void repSubPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Pages page = e.Item.DataItem as Pages;

                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                menuLink?.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
            }
        }

//        bool selectionMade = false;
        protected void repPipeline_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                PipelineSteps pipelineStep = e.Item.DataItem as PipelineSteps;
                HtmlButton btnStep = e.Item.FindControl("btnStep") as HtmlButton;
                if (pipelineStep != null && !string.IsNullOrEmpty(pipelineStep.StatusCode))
                {
                    VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(pipelineStep.StatusCode);
                    if (btnStep != null)
                    {
                        btnStep.InnerText = getStatus.RecruiterLabel + " (" + HiringManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), pipelineStep.StatusCode) + ")";
                        string pageFile = System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath);
                        string urlPage = "../recruiter/" + pageFile + "?vacancyCode=" + Utils.GetQueryString<string>("vacancyCode") + "&status=" + pipelineStep.StatusCode;
                        if (Utils.GetQueryString<string>("status") == pipelineStep.StatusCode)
                        {
                            btnStep.Attributes["class"] += " DTTT_selected";
//                        selectionMade = true;
                        }

                        btnStep.Attributes.Add("onclick", "window.location.href= '" + urlPage + "';return false;");
                    }
                }
            }
            if (e.Item.ItemType == ListItemType.Header)
            {
                HtmlButton btnAll = e.Item.FindControl("btnAll") as HtmlButton;
                if (btnAll != null)
                {
                    btnAll.InnerText = "All (" + HiringManager.CountVacancyApplicant(Utils.GetQueryString<string>("vacancyCode")) + ")";
                    string pageFile = System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath);
                    string urlPage = "../recruiter/" + pageFile + "?vacancyCode=" + Utils.GetQueryString<string>("vacancyCode");

                    if (string.IsNullOrEmpty(Utils.GetQueryString<string>("status")))
                    {
                        btnAll.Attributes["class"] += " DTTT_selected";
                    }

                    btnAll.Attributes.Add("onclick", "window.location.href= '" + urlPage + "';return false;");
                }
            }
        }
    }
}