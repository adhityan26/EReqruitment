﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class educations : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidFacebookApiKey.Value = MainCompany.FacebookAPIKey;
                //educationMapping.HRef = "../settings/educationmappingheader.aspx";
                educationLevel.HRef = "../settings/educationlevels.aspx";
                fieldOfStudy.HRef = "../settings/fieldofstudy.aspx";
                major.HRef = "../settings/educationmajor.aspx";
                university.HRef = "../settings/universities.aspx";
                repPages.DataSource = PageManager.GetRecruiterAdminPageList();
                repPages.DataBind();
            }
        }

        protected void repPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Repeater repSubPages = e.Item.FindControl("repSubPages") as Repeater;
                Pages page = e.Item.DataItem as Pages;
                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                menuLink.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
                List<Pages> subPageList = PageManager.GetChildrenPageList(page.ID);
                if (subPageList.Count > 0)
                {
                    repSubPages.DataSource = PageManager.GetChildrenPageList(page.ID);
                    repSubPages.DataBind();
                    menuLink.Attributes.Add("class", "dropdown-toggle");
                    menuLink.Attributes.Add("data-toggle", "dropdown");
                }
            }
        }

        protected void repSubPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Pages page = e.Item.DataItem as Pages;

                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                menuLink.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
            }
        }
    }
}