﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class applicant : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidFacebookApiKey.Value = MainCompany.FacebookAPIKey;
                if (CurrentUser != null)
                    litUserName.Text = CurrentUser.Name;

                repPages.DataSource = PageManager.GetApplicantPageList();
                repPages.DataBind();

                ApplicantSurveySessions openSurveySession = ERecruitmentManager.GetActiveApplicantSurveySession(CurrentUser.Code, MainCompany.Code);
                if (openSurveySession != null)
                {
                    Response.Redirect("../applicant/survey-questions.aspx?vacancyCode=" + openSurveySession.VacancyCode);
                }

                staticLink.Attributes.Add("href", MainCompany.ATSBaseUrl + "/applicant/applicant-profile.aspx");
            }
        }

        protected void repPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Repeater repSubPages = e.Item.FindControl("repSubPages") as Repeater;
                Pages page = e.Item.DataItem as Pages;

                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                menuLink.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
                List<Pages> subPageList = PageManager.GetChildrenPageList(page.ID);
                if (subPageList.Count > 0)
                {
                    repSubPages.DataSource = PageManager.GetChildrenPageList(page.ID);
                    repSubPages.DataBind();
                    menuLink.Attributes.Add("class", "dropdown-toggle");
                    menuLink.Attributes.Add("data-toggle", "dropdown");
                }
            }
        }

        protected void repSubPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Pages page = e.Item.DataItem as Pages;

                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                menuLink.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
            }
        }
    }
}