﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Configuration;


namespace ERecruitment
{
    public partial class portal : BaseMasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidFacebookApiKey.Value = MainCompany.FacebookAPIKey;
                if (CurrentUser == null)
                    repPages.DataSource = PageManager.GetPublicPageList();
                else
                    repPages.DataSource = PageManager.GetApplicantPageList();
                repPages.DataBind();

                if (CurrentUser != null)
                {
                    litUserName.Text = CurrentUser.Name;
                    aSignIn.Visible = false;
                    aSignUp.Visible = false;
                }

                staticLink.Attributes.Add("href", MainCompany.ATSBaseUrl + "/applicant/applicant-profile.aspx");
            }
        }

        protected void repPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Repeater repSubPages = e.Item.FindControl("repSubPages") as Repeater;
                Pages page = e.Item.DataItem as Pages;

                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                menuLink.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
                List<Pages> subPageList = PageManager.GetChildrenPageList(page.ID);
                if (subPageList.Count > 0)
                {
                    repSubPages.DataSource = PageManager.GetChildrenPageList(page.ID);
                    repSubPages.DataBind();
                    menuLink.Attributes.Add("class", "dropdown-toggle");
                    menuLink.Attributes.Add("data-toggle", "dropdown");
                }
            }
        }

        protected void repSubPages_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Pages page = e.Item.DataItem as Pages;

                HtmlAnchor menuLink = e.Item.FindControl("menuLink") as HtmlAnchor;
                menuLink.Attributes.Add("href", MainCompany.ATSBaseUrl + page.SubUrl);
            }
        }
    }
}