﻿<%@ Page Title="Automatic Reporting" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="automatic-reporting.aspx.cs" Inherits="ERecruitment.automatic_reporting" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            loadData();
        });

        function doFilter() {
            loadData();
        }

        $('body').on('click', '.edit', function () {
            var code = $(this).attr('name');
            showDataForm(code);
        });

        $('body').on('click', '.delete', function () {
            var code = $(this).attr('name');
            confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
                $.ajax({
                    url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveAutoReport&id=" + code,
                    async: true,
                    beforeSend: function () {

                    },
                    success: function (queryResult) {
                        var tableData = $('#tableData').dataTable();
                        tableData.fnDraw();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        showNotification(xhr.responseText);
                        var tableData = $('#tableData').dataTable();
                        tableData.fnDraw();
                    }
                });
            });
        });

        function loadData() {

            var param = "&fStartDate=" + $("#fStartDate").val();
                param += "&fEndDate=" + $("#fEndDate").val();


            var ex = document.getElementById('tableData');
            if ($.fn.DataTable.fnIsDataTable(ex)) {
                $("#tableData").dataTable().fnDestroy();
            }

            var OTableData = $('#tableData').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "bJQueryUI": true,
                "bAutoWidth": false,
                "sDom": "ftipr",
                "bDeferRender": true,
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [0, 1, 2] },
                    { "sClass": "controlIcon", "aTargets": [0, 1] },
                    { "bVisible": false, "aTargets": [2] }

                ],
                "oLanguage":
                { "sSearch": "Search By Report" },

                "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetReportAutoList" + param

            });
        }

        function refresh() {
            var tableData = $('#tableData').dataTable();
            tableData.fnDraw();
        }

        function showForm(code) {

            showDataForm(code);
        }

        function Add() {

            showDataForm("0");
        }

        function showDataForm(dataId) {

            var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetAutoReport&id=" + dataId;

            $.ajax({
                url: handlerUrl,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {

                    var dataModel = $.parseJSON(queryResult);
                    loadDataIntoForm("formDataFrameModel", dataModel);

                    $("#<%= ddReport.ClientID %> option[value='" + dataModel["Report"] + "']").attr('selected', true).trigger("chosen:updated");
                    $("#<%= ddSchedule.ClientID %> option[value='" + dataModel["Scheduler"] + "']").attr('selected', true).trigger("chosen:updated");
                    $("#<%= ddTimeScheduler.ClientID %> option[value='" + dataModel["TimeSchedule"] + "']").attr('selected', true).trigger("chosen:updated");

                    $.each(dataModel["Email"].split(","), function (i, e) {
                        $("#<%= listUser.ClientID %> option[value='" + e + "']").prop("selected", true).trigger("chosen:updated");
                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });

            $('#formDataFrameModel').modal('show');
        }

        function updateDataForm() {

            var form = "formDataFrameModel";
            var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateAutoReport";
            var formData = {};

            if (!getNodeData(form, formData)) return false;
            var dataSet = {
                "Code": $("input[data-attribute='Code']").val(),
                "Report": $("#<%= ddReport.ClientID %>").val(),
                "Schedule": $("#<%= ddSchedule.ClientID %>").val(),
                "TimeSchedule": $("#<%= ddTimeScheduler.ClientID %>").val(),
                "Email": $("#<%= listUser.ClientID %> ").val(),
                "ReportName": $("#<%= ddReport.ClientID %> :selected").text(),
                "StartDate": $("input[data-attribute='StartDate']").val(),
                "EndDate": $("input[data-attribute='EndDate']").val(),
            }

            $.post(actionUrl, dataSet).done(function (data) {
                refresh();
                $('#formDataFrameModel').modal('hide');

            }).fail(function (xhr, status, error) {
                showNotification(xhr.responseText);
            });
        }

        function doSetTo() {
            var start = $("input[data-attribute='StartDate']").val();
            var end = $("input[data-attribute='EndDate']").val();
            if (end != null && "" != end) {

            } else {
                $("input[data-attribute='EndDate']").val(start);
            }
        }
    </script>
    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="row">
                <div class="col-md-2 col-sm-2">
                    <span data-lang="RoleNameUserInputTitle">Data Period From</span>
                </div>
                <div class="col-md-3 col-sm-3">
                    <input class="form-control datepicker" data-date-format="mm/dd/yyyy" id="fStartDate" name="fStartDate" type="text" onchange="doFilter()"/>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2 col-sm-2">
                    <span data-lang="RoleNameUserInputTitle">Data Period To</span>
                </div>
                <div class="col-md-3 col-sm-3">
                    <input class="form-control datepicker" data-date-format="mm/dd/yyyy" id="fEndDate" name="fEndDate" onchange="doFilter()" />
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>Report</th>
                <th>Schedule</th>
                <th>Data Period From</th>
                <th>Data Period To</th>
                <th>Time Scheduler</th>
                <th>Email</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>


    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title center" data-lang="RoleUserCaptionTitle">Automatic Reporting</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                           <b><asp:Label ID="jobAgeCalculation" runat="server"></asp:Label></b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="RoleNameUserInputTitle">Report</span><span class="requiredmark">*</span>
                            <input type="hidden" data-attribute="Code" />
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <asp:DropDownList ID="ddReport"
                                runat="server"
                                CssClass="form-control"
                                DataValueField="Code"
                                DataTextField="Name" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="RoleNameUserInputTitle">Schedule</span><span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <asp:DropDownList ID="ddSchedule"
                                runat="server"
                                CssClass="form-control">
                                <asp:ListItem Value="Daily" Text="Daily" />
                                <asp:ListItem Value="Monday" Text="Every Monday" />
                                <asp:ListItem Value="Tuesday" Text="Every Tuesday" />
                                <asp:ListItem Value="Wednesday" Text="Every Wednesday" />
                                <asp:ListItem Value="Thursday" Text="Every Thursday" />
                                <asp:ListItem Value="Friday" Text="Every Friday" />
                                <asp:ListItem Value="Saturday" Text="Every Saturday" />
                                <asp:ListItem Value="Sunday" Text="Every Sunday" />
                                <asp:ListItem Value="FirstDayMonthly" Text="First Day Every Month" />
                                <asp:ListItem Value="LastDayMonthly" Text="Last Day Every Month" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="RoleNameUserInputTitle">Time</span><span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <asp:DropDownList ID="ddTimeScheduler"
                                runat="server"
                                CssClass="form-control">
                                <asp:ListItem Value="01:00" Text="01:00" />
                                <asp:ListItem Value="02:00" Text="02:00" />
                                <asp:ListItem Value="03:00" Text="03:00" />
                                <asp:ListItem Value="04:00" Text="04:00" />
                                <asp:ListItem Value="05:00" Text="05:00" />
                                <asp:ListItem Value="06:00" Text="06:00" />
                                <asp:ListItem Value="07:00" Text="07:00" />
                                <asp:ListItem Value="08:00" Text="08:00" />
                                <asp:ListItem Value="09:00" Text="09:00" />
                                <asp:ListItem Value="10:00" Text="10:00" />
                                <asp:ListItem Value="11:00" Text="11:00" />
                                <asp:ListItem Value="12:00" Text="12:00" />
                                <asp:ListItem Value="13:00" Text="13:00" />
                                <asp:ListItem Value="14:00" Text="14:00" />
                                <asp:ListItem Value="15:00" Text="15:00" />
                                <asp:ListItem Value="16:00" Text="16:00" />
                                <asp:ListItem Value="17:00" Text="17:00" />
                                <asp:ListItem Value="18:00" Text="18:00" />
                                <asp:ListItem Value="19:00" Text="19:00" />
                                <asp:ListItem Value="20:00" Text="20:00" />
                                <asp:ListItem Value="21:00" Text="21:00" />
                                <asp:ListItem Value="22:00" Text="22:00" />
                                <asp:ListItem Value="23:00" Text="23:00" />
                                <asp:ListItem Value="24:00" Text="24:00" />
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="RoleNameUserInputTitle">Data Period From</span><span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control datepicker" data-date-format="mm/dd/yyyy" required="" type="text" onchange="doSetTo()" data-attribute="StartDate" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="RoleNameUserInputTitle">Data Period To</span><span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control datepicker" type="text" data-date-format="mm/dd/yyyy" required="" data-attribute="EndDate" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="RoleNameUserInputTitle">Email</span><span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <asp:ListBox ID="listUser" SelectionMode="Multiple" runat="server" CssClass="form-control" DataValueField="Code" DataTextField="UserName" data-attribute="Email" />
                        </div>
                    </div>
                    
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>


            </div>
        </div>
    </div>
</asp:Content>
