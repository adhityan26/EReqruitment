﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/applicant-vacancy.Master" AutoEventWireup="true" CodeBehind="test-applicant.aspx.cs" Inherits="ERecruitment.test_applicant" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">


    $(document).ready(function () {

        loadData();
    });

    

    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var vacancyCode = GetQueryStringParams("vacancyCode");
        var param = "&vacancyCode=" + vacancyCode;
        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [



            ],
            "sAjaxSource": "../datatable/HandlerDataTableVacancies.ashx?commandName=GetReceievedApplicationList" + param

        });
    }
    function updateStatus() {

        var left = (screen.width / 2) - (800 / 2);
        var top = (screen.height / 2) - (450 / 2);
        var foo = window.open('<%= MainCompany.ATSBaseUrl %>recruiter/update-status-applicant.aspx', 'Applicant', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=400, height=350, top=' + top + ', left=' + left);
    }

        function updateSchedule() {

        var left = (screen.width / 2) - (800 / 2);
        var top = (screen.height / 2) - (450 / 2);
        var foo = window.open('<%= MainCompany.ATSBaseUrl %>recruiter/update-schedule.aspx', 'Applicant', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=no, copyhistory=no, width=800, height=490, top=' + top + ', left=' + left);
    }
</script>

     <style type="text/css">
        .jobs .panel-body > div > div{
            padding: 5px !important;
        }
    </style>
<div>
    <div>
<h4>
    IQ Test
</h4> 

        <div class="row">
            <div class="col-sm-12">
                 <div class="row">
                <div class="col-sm-2"><button class="btn btn-primary" onclick="updateStatus();" id="btnNew">
                    &nbsp;&nbsp;&nbsp;Update Status&nbsp;&nbsp;&nbsp;</button></div>  
                <div class="col-sm-1"><button class="btn btn-primary"  id="btnUpdateSchedule" onclick="updateSchedule();return false;">
                    &nbsp;&nbsp;&nbsp;Update Schedule&nbsp;&nbsp;&nbsp;</button></div> 
                     </div> 
            </div>
        </div>
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Status</th>
            <th>Phone Number</th>
            <th>Gender</th>
            <th>City</th>

        </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
   </div>
    </div>
 

</asp:Content>