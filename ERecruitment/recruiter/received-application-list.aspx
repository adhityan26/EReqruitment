﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="received-application-list.aspx.cs" Inherits="ERecruitment.received_application_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div>
    <div>
        <div>
              <div class="row">
            <div class="col-sm-12">
            <div class="row">
                <div class="col-sm-2">
                   Category
                </div>
                <div class="col-sm-3">
                   <asp:DropDownList ID="ddCategory" OnDataBound="ddDatabound" AutoPostBack="true"
                       OnSelectedIndexChanged="ddCategory_SelectedIndexChanged" 
                       DataSourceID="odsCategory" DataTextField="Name" DataValueField="Code"
                        runat="server" CssClass="form-control" />
                </div>
                <div class="col-sm-1">
                    Status
                </div>
                <div class="col-sm-3">
                   <asp:DropDownList ID="ddStatus" runat="server" CssClass="form-control"
                        OnDataBound="ddDatabound" AutoPostBack="true" OnSelectedIndexChanged="ddStatus_SelectedIndexChanged"
                        DataSourceID="odsStatus"  DataTextField="Code" DataValueField="Code"/>
                </div>
            </div>            
            </div>
        </div>
   
            <asp:Repeater ID="repApplicantVacancy" runat="server" OnItemDataBound="repApplicantVacancy_ItemDataBound">
                <HeaderTemplate>
                    <div class="panel-heading row hidden-xs hidden-sm">
                <div class="col-sm-12">
                    <div class="col-sm-1"></div>
                    <div class="col-sm-2">Code</div>
                    <div class="col-sm-2">Job Title</div>
                    <div class="col-sm-2">Job Req.</div>
                    <div class="col-sm-1">Hiring Manager</div>
                    <div class="col-sm-1">Recruiter</div>
                    <div class="col-sm-1">Due Date</div>
                    <div class="col-sm-1">Applicant</div>
                    <div class="col-sm-1">Status</div>
                </div>
            </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="row">
                <div class="col-sm-12">
                    <div class="col-sm-1"><input type="checkbox" class="form-control" /></div>
                    <div class="col-sm-2">
                        <p><span><asp:Literal ID="ltCode" runat="server" Text='<%#Eval("ApplicantCode") %>' />
                           </span>
                         </p>
                    </div>
                    <div class="col-sm-2">
                        <span><asp:Literal ID="ltJobTitle" runat="server" /></span>
                    </div>
                    <div class="col-sm-2">
                        <span><asp:Literal ID="ltVacancyCode" runat="server" Text='<%#Eval("VacancyCode") %>' /></span>
                    </div>
                    <div class="col-sm-1">
                        <span><asp:Literal ID="ltHiringManager" runat="server" /></span>
                    </div>
                    <div class="col-sm-1">
                        <span><asp:Literal ID="ltRecruiter" runat="server" /></span>
                    </div>
                    <div class="col-sm-1">
                        <span><asp:Literal ID="ltDueDate" runat="server" /></span>
                    </div>
                    <div class="col-sm-1">
                        <span><asp:Literal ID="ltApplicantName" runat="server" /></span>
                    </div>
                    <div class="col-sm-1">
                        <span><asp:Literal ID="ltStatus" runat="server" Text='<%#Bind("Status") %>' /></span>
                    </div>
                </div>
            </div>
                </ItemTemplate>
            </asp:Repeater>
           
        </div>
    </div>
</div>

<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                       SelectMethod="GetVacancyCategoryList"
    />

    <asp:ObjectDataSource ID="odsStatus" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyStatus"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                       SelectMethod="GetIsApplicantStatusList"
    />


</asp:Content>
