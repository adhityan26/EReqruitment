﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Reflection;
using System.IO;

namespace ERecruitment
{
    public partial class automatic_reporting : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                ddReport.DataSource = ApplicationManager.GetReportList();
                ddReport.DataBind();

                listUser.DataSource = AuthenticationManager.GetUserAccount();
                listUser.DataBind();

                ApplicationSettings recruiterSetting = ERecruitmentManager.GetApplicationSetting();
                if (recruiterSetting != null)
                {
                    String s = recruiterSetting.JobAgeCalculationMethod;
                    jobAgeCalculation.Text = "Job Age Calculation : " + s;
                }
                else
                {
                    jobAgeCalculation.Text = "Job Age Calculation : ";
                }

                Reports report = ApplicationManager.GetReport(Utils.GetQueryString<string>("reportCode"));
                if (report != null)
                {

                    Utils.SelectItem<string>(ddReport, Utils.GetQueryString<string>("reportCode"));
                    Utils.SelectItem<string>(ddSchedule, report.Schedule);
                }
            }
        }

    }
}