﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/popupform.Master" CodeBehind="input-test-attachment-result.aspx.cs" Inherits="ERecruitment.input_test_attachment_result" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        $('body').on('click', '.file', function () {

            var aData = this.id;
            var url = "getvacancyapplicantfile.aspx?code=" + aData;
            popitup(url, 1, 1);

        });

        function popitup(url, popupname) {
            newwindow = window.open(url, popupname, 'height=550,width=750, toolbar=0, location=0,resizable=1, scrollbars=1');
            newwindow.moveTo(20, 20);
            if (window.focus) { newwindow.focus() }
            return newwindow;
        }

        function popitupSmall(url) {
            return popitup(url, 190, 230);
        }

        function popitup(url, height, width) {
            newwindow = window.open(url, 'Page Info', 'height=' + height + ',width=' + width + ', toolbar=0, location=0, resizable=1, scrollbars=1');
            newwindow.moveTo(20, 20);
            if (window.focus) { newwindow.focus() }
            return newwindow;
        }

        function popitup(url, height, width, name) {
            newwindow = window.open(url, name, 'height=' + height + ',width=' + width + ',location=0,resizable=1, scrollbars=1');
            newwindow.moveTo(20, 20);
            if (window.focus) { newwindow.focus() }
            return newwindow;
        }

    </script>
<div class="row">
    <div class="col-sm-12">
<h4>Test Results</h4>
<asp:Repeater ID="repTest"
              runat="server"
    >
    <ItemTemplate>
        <div class="row">
            <div class="col-sm-12">
            <div class="col-sm-3">
                <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("TestCode") %>' />
                <label class="muted"><%# Eval("TestObj.TestName") %></label>
            </div>
            <div class="col-sm-2">
                <asp:TextBox ID="tbTestValue" runat="server" CssClass="form-control" />
            </div>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>         

<h4>Attachments</h4>
<asp:Repeater ID="repAttachment"
              runat="server"
    >
    <ItemTemplate>
        <div class="row">
            <div class="col-sm-12">
            <div class="col-sm-3">
                <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("AttachmentCode") %>' />
                <label class="muted"><%# Eval("AttachmentObj.Name") %></label>
            </div>
            <div class="col-sm-6">
                <asp:FileUpload ID="fileUpload" runat="server" />
            </div>
            <div class="col-sm-3">
                <asp:Literal ID="litAttachmentName" runat="server" />
            </div>
            </div>
        </div>
    </ItemTemplate>
</asp:Repeater>     

        <div class="row">
            <div class="col-sm-12">
            <div class="col-sm-3">
                <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn btn-primary" OnClick="btnSave_Click" />
            </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>       
