﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class test_form: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                IniateForm(Utils.GetQueryString<string>("code"));
        }
        private void IniateForm(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                Tests getTest = HiringManager.GetTest(code);
                if (getTest != null)
                    tbName.Value = getTest.TestName;
            }
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
            {
                Tests getTest = HiringManager.GetTest(Utils.GetQueryString<string>("code"));
                if (getTest != null)
                    getTest.TestName = tbName.Value;
                HiringManager.UpdateTest(getTest);
                JQueryHelper.InvokeJavascript("showNotification('Successfully updated');window.close();window.opener.refresh();", Page);
            }
            else
            {
                Tests addTest = new Tests();
                addTest.TestName = tbName.Value;
                addTest.Active = true;
                addTest.TestCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-")); ;
                HiringManager.SaveTest(addTest);
                JQueryHelper.InvokeJavascript("showNotification('Successfully saved');window.close();window.opener.refresh();", Page);
            }
        }
    }
}