﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/popupform.Master" AutoEventWireup="true" CodeBehind="survey-question-form.aspx.cs" Inherits="ERecruitment.survey_question_form1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div class="row">
    <div class="col-sm-12">
        <asp:FormView ID="formData"
                        runat="server"
                        DefaultMode="Insert"
                        RenderOuterTable="false"
                        OnItemInserting="formData_ItemInserting"
                        OnDataBound="formData_DataBound"
                        OnItemUpdating="formData_ItemUpdating"
                        DataSourceID="odsData"
                        DataKeyNames="Code"
            >
            <InsertItemTemplate>
                <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_Name">Name</label>
                    </div>
                    <div class="col-sm-3">
                        <asp:TextBox ID="tbName" runat="server" Text='<%# Bind("Name") %>' CssClass="form-control" />                     
                    </div>
                </div>
                <div class="row childField">
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_Question">Question</label>
                    </div>
                    <div class="col-sm-5">
                        <asp:TextBox ID="tbQuestion" Text='<%# Bind("Question") %>' runat="server" CssClass="form-control" />                     
                    </div>
                </div>
                <div class="row childField">
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_DesiredAnswer">Desired Answer</label>
                    </div>
                </div>
                <div class="row childField multipleChoice">
                    <div class="col-sm-3">
                            
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_AnswerRange">Answer Choices</label>
                    </div>
                </div>
                <asp:ListView ID="repAnswerRange"
                                runat="server"
                                OnItemCommand="repAnswerRange_ItemCommand"
                                OnItemDataBound="repAnswerRange_ItemDataBound"
                                >
                    <ItemTemplate>
                        <div class="row childField multipleChoice">
                            <div class="col-sm-3">
                            
                            </div>
                            <div class="col-sm-3">
                                <asp:TextBox ID="tbAnswerLabel" Text='<%# Eval("Label") %>' runat="server" CssClass="form-control" />                     
                            </div>
                            <div class="col-sm-2">
                                <asp:LinkButton ID="liRemove" CommandName="del" CommandArgument='<%# Eval("Label") %>' runat="server"><i class="fa fa-trash-o"></i></asp:LinkButton>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <div class="row childField multipleChoice">
                    <div class="col-sm-3">
                            
                    </div>
                    <div class="col-sm-3">
                        <asp:LinkButton ID="liAddAnotherAnswer" OnClick="liAddAnotherAnswer_Click" runat="server" Text="add another answer" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick">Save</button>                
                    </div>
                </div>
            </InsertItemTemplate>
            <EditItemTemplate>
                <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_Name">Name</label>
                    </div>
                    <div class="col-sm-3">
                        <asp:TextBox ID="tbName" runat="server" Text='<%# Bind("Name") %>' CssClass="form-control" />                     
                    </div>
                </div>
                <div class="row childField">
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_Question">Question</label>
                    </div>
                    <div class="col-sm-5">
                        <asp:TextBox ID="tbQuestion" Text='<%# Bind("Question") %>' runat="server" CssClass="form-control" />                     
                    </div>
                </div>
                <div class="row childField">
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_DesiredAnswer">Desired Answer</label>
                    </div>
                </div>
                <div class="row childField multipleChoice">
                    <div class="col-sm-3">
                            
                    </div>
                    <div class="col-sm-3">
                        <label runat="server" id="lbDisForm_AnswerRange">Answer Choices</label>
                    </div>
                </div>
                <asp:ListView ID="repAnswerRange"
                                runat="server"
                                OnItemCommand="repAnswerRange_ItemCommand"
                                OnItemDataBound="repAnswerRange_ItemDataBound"
                                >
                    <ItemTemplate>
                        <div class="row childField multipleChoice">
                            <div class="col-sm-3">
                            
                            </div>
                            <div class="col-sm-3">
                                <asp:TextBox ID="tbAnswerLabel" Text='<%# Eval("Label") %>' runat="server" CssClass="form-control" />                     
                            </div>
                            <div class="col-sm-2">
                                <asp:LinkButton ID="liRemove" CommandName="del" CommandArgument='<%# Eval("Label") %>' runat="server"><i class="fa fa-trash-o"></i></asp:LinkButton>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
                <div class="row childField multipleChoice">
                    <div class="col-sm-3">
                            
                    </div>
                    <div class="col-sm-3">
                        <asp:LinkButton ID="liAddAnotherAnswer" OnClick="liAddAnotherAnswer_Click" runat="server" Text="add another answer" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick">Save</button>                
                    </div>
                </div>

            </EditItemTemplate>
        </asp:FormView>
    </div>
</div>



<asp:ObjectDataSource ID="odsData"
                      runat="server"
                      TypeName="ERecruitment.Domain.DisqualificationQuestionManager"
                      DataObjectTypeName="ERecruitment.Domain.DisqualifierQuestions"
                      InsertMethod="SaveDisqualificationQuestion"
                      UpdateMethod="UpdateDisqualificationQuestion"
                      SelectMethod="GetDisqualificationQuestion"
                      OnInserted="odsData_Inserted"
                      OnUpdated="odsData_Updated"
    >
    <SelectParameters>
        <asp:QueryStringParameter Name="code" QueryStringField="code" />
    </SelectParameters>
</asp:ObjectDataSource>

    
<asp:ObjectDataSource ID="odsParent" runat="server"
                      TypeName="ERecruitment.Domain.DisqualificationQuestionManager"
                      DataObjectTypeName="ERecruitment.Domain.DisqualifierQuestions"
                      SelectMethod="GetParentDisqualificationQuestionList"
                      />
<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetVacancyCategoryList"
                      />
</asp:Content>
