﻿<%@ Page Title="Job Requisitions" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="job-requisition-list.aspx.cs" Inherits="ERecruitment.job_requisition_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<input type="hidden" id="bitLyAccessToken" value="<%= ConfigurationManager.AppSettings["bitLyAccessToken"] %>"/>
    
<script type="text/javascript">

    var listObj;
    $(document).ready(function () {
        
        if (GetQueryStringParams("showArchive") == "true")
            $("#cbShowArchive").prop("checked", true);
        else
            $("#cbShowArchive").prop("checked", false);

        initiateList();

        $(".btnGo").click(function(e) {
            e.preventDefault();
            var pageSize = $("#pageSize").val();
            var page = parseInt($("#go-to").val());
            
            if ($(".pagination").children(":last-child").text() < page) {
                page = parseInt($(".pagination").children(":last-child").text());
            } else if (page < 0) {
                page = 0;
            } 
            
            listObj.show(((page - 1) * pageSize) + 1, pageSize);
            $(".pagination .active").removeClass("active");
            $(".pagination li").filter(function() { return $(this).text() == page }).addClass("active");
        });
    });
    
    function initiateList() {

        var pageSize = $("#pageSize").val();
        
        var options = {
            valueNames: ['name'],
            page: pageSize,
            plugins: [
              ListPagination({
                  outerWindow: 2
              })
            ]
        };

        listObj = new List('listContainer', options);
        
        if (listObj.items.length == 0) {
            $(".btnGo").parent().parent().hide();
        } else {
            $(".btnGo").parent().parent().show();
        }

        listObj.on("updated",
            function() {
                if (listObj.visibleItems.length == 0) {
                    $(".btnGo").parent().parent().hide();
                } else {
                    $(".btnGo").parent().parent().show();
                }
            });
    }

    function updatePage()
    {
        var pageSize = $("#pageSize").val();
        listObj.show(0, pageSize);
        // highlight first page
        $(".pagination").children(":first-child").addClass("active");
    }

    function exportReport(type) {
        var fileName = "";
        var param="";
        popitup("contentfastreport.aspx?report=jobrequisitionlist.frx&type=" + type + "&fileName=" + fileName + "&param=" + param, 1, 1, "SalesRevenue");
    }

    function exportToExcel()
    { 
        exportReport("xls");
    }

    function showNewForm() {

        window.location.href = "job-requisition-form.aspx";
       
    }

    function showDetail(code) {

        window.location.href = "job-requisition-form.aspx?vacancyCode=" + code;
    }



    var selectedItemArray = [];
    $('body').on('click', '.cbChild', function () {

        var code = $(this).attr("data-attribute");

        var index = $.inArray(code, selectedItemArray);

        if ($(this).is(':checked')) {
            selectedItemArray.push(code);

        }
        else {
            selectedItemArray.splice(index, 1);
        }
    });

    function postAdv() {

        var validStatus = true;
        var isValidDate = true;
        var codeList = selectedItemArray;
            
        $(selectedItemArray).each(function() {
            var status = $(".cbChild[data-attribute=" + this + "]").parents("li").find("[data-status]")
                .attr("data-status");
            if (status !== "Unposted" && status !== "Draft") {
                validStatus = false;
                return validStatus;
            }
        });

        if (codeList === "") {
            showNotification("please select job", "warning");
        } else {
              
            if (!validStatus) {
                showNotification("Job already open");
                return;
            }
            if (!isValidDate) {
                showNotification("Invalid Operation, Post Date or Expired post date is empty");
                return;
            } 
                
            if (codeList.length > 0) {
                $.ajax({
                    url: "../handlers/HandlerVacancies.ashx?commandName=PostAdvertisement&codes=" + codeList,
                    async: true,
                    beforeSend: function() {

                    },
                    success: function(queryResult) {
                        showNotification("vacancy posted");
                        filterJobList();
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        showNotification(xhr.responseText, "error");
                    }
                });
            } else {
                showNotification("Please select a vacancy first", "error");
            }
        }
    }

    function closeJob() {
        var validStatus = true;
        var codeList = selectedItemArray;
            
        $(selectedItemArray).each(function() {
            var status = $(".cbChild[data-attribute=" + this + "]").parents("li").find("[data-status]")
                .attr("data-status");
            if (status !== "Unposted") {
                validStatus = false;
                return validStatus;
            }
        });

        if (codeList == "")
            showNotification("please select job", "error");
        else {
            if (!validStatus) {
                showNotification("Invalid operation, one or more selected job is not in 'unposted' status", "error");
                return;
            }
            else {
                if (codeList.length > 0) {
                    confirmMessage("Are you sure to close this requisition ? closed job requisition can not be reopened", "warning", function() {
                        $.ajax({
                            url: "../handlers/HandlerVacancies.ashx?commandName=CloseJob&codes=" + codeList,
                            async: true,
                            beforeSend: function() {

                            },
                            success: function(queryResult) {
                                showNotification("vacancy closed");
                                filterJobList();
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                showNotification(xhr.responseText);
                                filterJobList();
                            }
                        });
                    });
                }
            }

        }
        
    }

    function shareJob(medSoc, jobCode, jobName) {
        var url = $("#" + '<%= hidJobDetailUrl.ClientID %>').val() + "?vacancyCode=" + jobCode;
        postJob(medSoc, url, jobName);
    }

    function deleteJob() {

        
        var validStatus = true;
        var codeList = selectedItemArray;

        if (codeList == "")
            showNotification("please select job");
        else {
            
            $(selectedItemArray).each(function() {
                var row = $(".cbChild[data-attribute=" + this + "]").parents("li");
                var status = row.find("[data-status]")
                    .attr("data-status");
                var totalApplicant = row.find("[data-total-applicant]")
                    .attr("data-total-applicant");
                if (status !== "Unposted" || totalApplicant > 0) {
                    validStatus = false;
                    return validStatus;
                }
            });
            
            if (!validStatus) {
                showNotification("Invalid Operation, One or more selected job is not in 'Unposted' status or already have applicant", "error");
                return;
            }
            else {
                if (codeList.length > 0) {
                    confirmMessage("Are you sure want to delete the selected jobs?", "warning", function () {
                        $.ajax({
                            url: "../handlers/HandlerVacancies.ashx?commandName=DeleteJob&codes=" + codeList,
                            async: true,
                            beforeSend: function () {

                            },
                            success: function (queryResult) {
                                showNotification("vacancy deleted");
                                filterJobList();
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                showNotification(xhr.responseText);
                                filterJobList();
                            }
                        });
                    });
                }
            }

        }

               
    }

    function unPostAdv() {

        var validStatus = true;
        var codeList = selectedItemArray;

        if (codeList == "")
            showNotification("please select job");
        else {
            if (!validStatus) {
                showNotification("Invalid Operation, One or more selected job is not in 'Posted' status");
                return;
            }
            else {
                if (codeList.length > 0) {
                    $.ajax({
                        url: "../handlers/HandlerVacancies.ashx?commandName=UnpostAdvertisement&codes=" + codeList,
                        async: true,
                        beforeSend: function () {
                        },
                        success: function (queryResult) {
                            alert("success");
                            showNotification("job unposted");
                            filterJobList();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            alert("error");
                            showNotification(xhr.responseText);
                            filterJobList();
                        }
                    });
                }
            }

        }
        
    }

    function filterJobList()
    {
        var param = "?companyCode=" + $("#" + '<%= ddCompany.ClientID %>').val();
        param += "&jobCategoryCode=" + $("#" + '<%= ddJobCategory.ClientID %>').val();
        param += "&locationCode=" + $("#" + '<%= ddJobLocation.ClientID %>').val();
        param += "&status=" + $("#" + '<%= ddStatus.ClientID %>').val(); 
        param += "&advStatus=" + $("#" + '<%= ddAdvertisementStatus.ClientID %>').val(); 
        param += "&showArchive=" + $("#cbShowArchive").prop("checked");
        param += "&orderField=" + $("#" + '<%= ddOrderField.ClientID %>').val(); 
        param += "&orderDirection=" + $("#" + '<%= ddOrderDirection.ClientID %>').val(); 

        window.location.href = "job-requisition-list.aspx" + param;
    }
    
    function downloadVacancyFromPeer()
    {
        showMessage("Are you sure want to download open vacancies from SAP database?");
    }

    function showMessage(message_alert) {
        BootstrapDialog.show({
            type: BootstrapDialog.TYPE_INFO,
            title: 'Notification',
            message: message_alert,
            buttons: [{
                icon: 'glyphicon glyphicon-ok',
                label: 'Yes',
                cssClass: 'btn btn-success',
                action: function (dialogItself) {
                    dialogItself.close();
                    waitingDialog.show('Loading...');
                    execDownloadVacancy();
                }
            }, {
                icon: 'glyphicon glyphicon-remove',
                label: 'No',
                cssClass: 'btn btn-danger',
                action: function (dialogItself) {
                    dialogItself.close();
                }
            }]
        });
    }

    
    function execDownloadVacancy() {
        var comcode = '<%=MainCompany.Code %>';
        $.ajax({
            url: "../handlers/HandlerVacancies.ashx?commandName=SynchronizePeerJobs&CompanyCode=" + comcode,
            async: true,
            beforeSend: function () {

            },
            success: function (response) {
                waitingDialog.hide();
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_SUCCESS,
                    title: 'Notification',
                    message: response,
                    buttons: [{
                        icon: 'glyphicon glyphicon-ok',
                        label: 'OK',
                        cssClass: 'btn btn-success',
                        action: function (dialogItself) {
                            dialogItself.close();
                            filterJobList();
                        }
                    }]
                    
                });
          
            },
            error: function (xhr, ajaxOptions, thrownError) {
                waitingDialog.hide();
                BootstrapDialog.danger(xhr.responseText);
            }
        });
    }


</script>

<div class="row">
    <div class="col-sm-12">
        
        <asp:HiddenField ID="hidJobDetailUrl" runat="server" />
        <button class="btn btn-default commandIcon" id="btnCreateJob" runat="server"><i class="fa fa-file"></i>New</button>
        <button class="btn btn-default commandIcon" id="btnPostJob" runat="server" onclick="postAdv(); return false;"><i class="fa fa-check"></i>Post</button>
        <button class="btn btn-default commandIcon" id="btnUnPostJob" runat="server" onclick="unPostAdv(); return false;"><i class="fa fa-undo"></i>Unpost</button>
        <button class="btn btn-default commandIcon" id="btnArchiveJob" runat="server" onclick="closeJob(); return false;"><i class="fa fa-archive"></i>Close</button>
        <button class="btn btn-default commandIcon" id="btnDeleteJob" runat="server" onclick="deleteJob(); return false;" type="button"><i class="fa fa-trash"></i>Delete</button>
        <button class="btn btn-default commandIcon" id="btnDownloadVacancy" runat="server" onclick="downloadVacancyFromPeer(); return false;"><i class="fa fa-download"></i>Download Vacancy From SISDM</button>
    </div>
</div>


<div class="row searchPanel">
    <div class="col-md-12 col-sm-12">  
        <div class="col-md-2 col-sm-2">
           <span data-lang="CompanyLabel">  Company</span>
        </div>                  
        <div class="col-md-2 col-sm-2">
           <span data-lang="JobCategoryLabel"> Job Category</span>
        </div>
        <div class="col-md-2 col-sm-2">
           <span data-lang="WorkLocationLabel">  Work Location</span>
        </div>
        <div class="col-md-2 col-sm-2">
           <span data-lang="StatusLabel">  Status</span>
        </div>
        <div class="col-md-2 col-sm-2">
           <span data-lang="AdvLabel">  Advertisement Status</span>
        </div>
    </div>
    <div class="col-md-12 col-sm-12">  
        <div class="col-md-2 col-sm-2">
            <asp:DropDownList ID="ddCompany" onchange="filterJobList();" OnDataBound="ddDatabound" DataValueField="Code" DataTextField="Name" runat="server" CssClass="form-control" />
        </div>                  
        <div class="col-md-2 col-sm-2">
            <asp:DropDownList ID="ddJobCategory" onchange="filterJobList();" DataSourceID="odsJobCategories" OnDataBound="ddDatabound" DataValueField="Code" DataTextField="Name" runat="server" CssClass="form-control" />
        </div>
        <div class="col-md-2 col-sm-2">
            <asp:DropDownList ID="ddJobLocation" onchange="filterJobList();" DataSourceID="odsCity" OnDataBound="ddDatabound" DataValueField="CityCode" DataTextField="CityName" runat="server" CssClass="form-control" />
        </div>
        <div class="col-md-2 col-sm-2">
            <asp:DropDownList ID="ddStatus" onchange="filterJobList();" runat="server" CssClass="form-control">
                <asp:ListItem Value="" Text="All" />
                <asp:ListItem Value="Pending" Text="Pending" />
                <asp:ListItem Value="Open" Text="Open" />
            </asp:DropDownList>
        </div>
        <div class="col-md-2 col-sm-2">
            <asp:DropDownList ID="ddAdvertisementStatus" onchange="filterJobList();" runat="server" CssClass="form-control">
                <asp:ListItem Value="" Text="All" />
                <asp:ListItem Value="Draft" Text="Draft" />
                <asp:ListItem Value="Posted" Text="Posted" />
                <asp:ListItem Value="UnPosted" Text="UnPosted" />
                <asp:ListItem Value="Closed" Text="Closed" />
            </asp:DropDownList>
        </div>
        <div class="col-md-2 col-sm-2">
            <input type="checkbox" id="cbShowArchive" onchange="filterJobList();" /> <span data-lang="ArchivedLabel"> Show Closed Jobs</span> 
        </div>
    </div>
</div>

<div id="listContainer">
    <div class="row">
        <div class="col-md-1 col-sm-1">
            <select id="pageSize" class="select2" onchange="updatePage();">
                <option>5</option>
                <option>10</option>
                <option>20</option>
                <option>50</option>
            </select>
        </div>
        <div class="col-md-2 col-sm-2">

        </div>
        <div class="col-md-5 col-sm-5">
            <div class="col-md-3 col-sm-3">
                Order By:
            </div>
            <div class="col-md-4 col-sm-4">
                <asp:DropDownList ID="ddOrderField" runat="server" CssClass="form-control" onchange="filterJobList();" >
                    <asp:ListItem Value="PositionName" Text="Position Name" />
                    <asp:ListItem Value="InsertStamp" Text="Created Date" />
                    <asp:ListItem Value="PostDate" Text="Post Date" />
                </asp:DropDownList>
            </div>
            <div class="col-md-4 col-sm-4">
                <asp:DropDownList ID="ddOrderDirection" runat="server" CssClass="form-control" onchange="filterJobList();" >
                    <asp:ListItem Value="asc" Text="Ascending" />
                    <asp:ListItem Value="desc" Text="Descending" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-1 col-sm-1">

        </div>
        <div class="col-md-3 col-sm-3">
            <input class="search form-control" placeholder="Search" />
        </div>
    </div>   
    <ul class="list">
    <asp:Repeater ID="repJobList"
                    runat="server"
                    OnItemDataBound="repJobList_ItemDataBound"
                    >
        <ItemTemplate>
            <li>
            <div class="row jobSection">
                    <div class="col-md-1 col-sm-1 center" style="width:18px;margin-right:2px">
                        <input type="checkbox" class="cbChild" data-attribute='<%# Eval("Code") %>' />
                    </div>
                    <div class="col-md-11 col-sm-11">
                        <div class="text-uppercase" >
                            <strong><a <%# Eval("Active").Equals(false) ? "style=\"text-decoration: line-through;\"" : "" %> href='job-requisition-form.aspx?vacancyCode=<%# Eval("Code") %>' class="name"><%# Eval("InsertedBy").ToString() == "SAP" ?"<span class='custom-icon icon-sap normal'></span>&nbsp" + Eval("PositionName") :Eval("PositionName")%></a>
                            </strong></div>                    
                    </div>     
                    <div class="col-md-1 col-sm-1 center"></div>  
                    <div class="col-md-3 col-sm-3">                 
                        <div class="block"><i><i class="fa fa-calendar-o"></i><%# Eval("PostDate","{0:dd, MMM yyyy}") %> - <%# Eval("ExpiredPostDate","{0:dd, MMM yyyy}") %></i></div>   
                        <div class="block"><i class="fa fa-user"></i>Creator: <%# Eval("CreatorName") %></div>
                        <div class="block"><i class="fa fa-user"></i>Recruiter: <%# Eval("HiringManager") %> - <%# Eval("CompanyName") %></div>                
                    </div>                    
                    <div class="col-md-5 col-sm-5">
                        <div class="block"><i class="fa fa-bullseye"></i>Target Group: <%# Eval("RecruitmentTargetGroup") %></div>
                        <div class="block"><i class="fa fa-map-marker"></i>Location: <%# Eval("CityName") %></div>
                        <div class="block"><i class="fa fa-money"></i>Salary: <asp:Literal ID="litSalaryRange" runat="server" /></div>
                        <div class="block">
                            <i onclick='shareJob("linkedin","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-linkedin" title="share linkedin"></i>
                            <i onclick='shareJob("facebook","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-facebook-official" title="share facebook"></i>
                            <i onclick='shareJob("twitter","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-twitter" title="share twitter"></i>
                            <i class="visible-xs" onclick='shareJob("whatsapp","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-whatsapp" title="share whatsapp"></i>
                        </div> 
                    </div>
                    <div class="col-md-3 col-sm-3">                        
                        <div class="block"><i><i class="fa fa-file"></i>Status: <%# Eval("Status") %></i></div>
                        <div class="block" data-status="<%# Eval("AdvertisementStatus") %>"><i><i class="fa fa-file"></i>Adv. Status:<%# Eval("AdvertisementStatus") %></i></div>
                        <div class="block" data-total-applicant="<%# Eval("ApplicantCount") %>"><a class="btn btn-default" href='detail-application.aspx?showArchives=true&vacancyCode=<%# Eval("Code") %> '><i class="fa fa-users"></i>Applicants: <%# Eval("ApplicantCount") %></a></div>
                    </div>                  
            </div>
            </li>
        </ItemTemplate>
    </asp:Repeater>
    </ul>
    <ul style="list-style-type: none">
        <li>
            <div class="col-sm-6">
                <ul class="pagination">
                
                </ul>
            </div>
            <div class="col-sm-6">
                <div style="margin: 20px 0">
                    <div class="col-sm-3" style="padding-right: 0px"><input type="text" class="form-control" id="go-to"/></div>
                    <div class="col-sm-3" style="padding-left: 0px"><button type="button" class="btn btn-primary btnGo">Go</button></div>
                </div>
            </div>
        </li>
    </ul>
</div>

    <asp:ObjectDataSource ID="odsCompanyList"
                          runat="server"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          DataObjectTypeName="ERecruitment.Domain.Companies"
                          SelectMethod="GetCompanyList"
                          />
    <asp:ObjectDataSource ID="odsJobCategories"
                          runat="server"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                          SelectMethod="GetVacancyCategoryList"
                          />
    <asp:ObjectDataSource ID="odsCity"
                          runat="server"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                          SelectMethod="GetCityList"
                          />
</asp:Content>
