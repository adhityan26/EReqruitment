﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="send-offering.aspx.cs" Inherits="ERecruitment.send_offering" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta charset="utf-8" />
    
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no"/>
    <meta name="theme-color" content="#000080"/>
    <meta name="msapplication-navbutton-color" content="#000080"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="#000080"/>
    <meta name="keywords" content="DAYALIMA Recruitment"/>
    <meta name="description" content="DAYALIMA Recruitment"/>
    <meta property="og:title" content="DAYALIMA Recruitment"/>
    <link href="apple-touch-icon.png" rel="apple-touch-icon"/>
    <link id="linkFavicon" href="../handlers/HandlerUI.ashx?commandName=GetMainSiteFavicon" runat="server" rel="shortcut icon" type="image/x-icon" />
     <link href="<%=""+ MainCompany.ATSBaseUrl %>system/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<%=""+ MainCompany.ATSBaseUrl %>system/css/chosen.min.css" rel="stylesheet"  type="text/css"/>
    <link href="<%= ""+MainCompany.ATSBaseUrl %>system/css/jquery-ui-1.10.3.custom.min.css" rel="stylesheet" type="text/css"/>
    <link href="<%= ""+MainCompany.ATSBaseUrl %>system/css/styles.css?v=2.0.2" rel="stylesheet"/>
    <link href="<%= ""+MainCompany.ATSBaseUrl %>system/css/font-awesome.min.css" rel="stylesheet"/>
    <link href="<%= ""+MainCompany.ATSBaseUrl %>system/css/angular.css" rel="stylesheet"/>
    
    <script src="<%= ""+MainCompany.ATSBaseUrl %>system/js/jquery.min.js" ></script>
    <script src="<%= ""+MainCompany.ATSBaseUrl %>system/js/bootstrap.min.js" ></script>
    <script src="<%= ""+MainCompany.ATSBaseUrl %>system/js/jquery-ui-1.10.3.min.js" ></script>
     <style type="text/css">
        .jobs .panel-body > div > div{
            padding: 5px !important;
        }
    </style>
    <style type="text/css">
        .ui-autocomplete{
            z-index:10000;
        }

        .profileTable{
            width: 1325px !important;
            margin: auto !important;
        }
        
    </style>
    <script type="text/javascript" src="http://platform.linkedin.com/in.js">
        api_key: 757bhaygtfn4m4
    </script>
    <!-- [if lt IE 8] <link rel="stylesheet" href="stylesheets/bootstrap-ie7.css"> -->
   
</head> 
<body>
    <form id="form1" runat="server">
    <div>
     <div>
        <div>
        <div class="row">
         <div class="row">
              <div class="col-sm-12">

                  <div class="row">
                     <div class="col-sm-12">
                        <asp:TextBox ID="tbTextArea"
                                     runat="server"
                                     TextMode="MultiLine"     
                                     CssClass="form-controls"
                                     />
                    </div>
                  </div>                  
                  <div class="row">
                      <div class="col-sm-3">
                        
                    </div>
                    <div class="col-sm-3">
                        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" />
                    </div>
                  </div>
              </div>
           </div>
        </div>
        </div>
     </div>
     </div>
    </form>
</body>
</html>
