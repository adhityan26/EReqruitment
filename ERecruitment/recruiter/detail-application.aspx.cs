﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SS.Web.UI;
using ERecruitment.Domain;


namespace ERecruitment
{
    public partial class detail_application : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                listCompanies.DataSource = ERecruitmentManager.GetCompanyList(MainCompany.Code);
                listCompanies.DataBind();

                hidUrl.Value = MainCompany.ATSBaseUrl + "recruiter/update-status-applicant.aspx";
                hidVacancyCode.Value = Utils.GetQueryString<string>("vacancyCode");
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("status")))
                {
                    VacancyStatus getStatus = ERecruitmentManager.GetVacancyStatus(Utils.GetQueryString<string>("status"));
                    if (getStatus != null)
                        ltPipeline.Text = getStatus.Name;
                }

                Vacancies vacancy = ERecruitmentManager.GetVacancy(hidVacancyCode.Value);
                hidSourceJob.Value = vacancy.InsertedBy;
                Pipelines pipeline = HiringManager.GetPipeline(vacancy.PipelineCode);

                Utils.SelectItem<string>(ddGender, Utils.GetQueryString<string>("gender"));
                Utils.SelectItem<string>(ddMaritalStatus, Utils.GetQueryString<string>("maritalStatus"));

                if (!string.IsNullOrEmpty(pipeline.TestCodes))
                {
                    hidTestCodePipeline.Value = pipeline.TestCodes;
                    repTest.DataSource = HiringManager.GetPipelineTestListByCodes(pipeline.TestCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                    repTest.DataBind();
                }

                repAttachment.DataSource = HiringManager.GetPipelineAttachmentList(vacancy.PipelineCode);
                repAttachment.DataBind();

                List<string> statusCodeList = new List<string>();

                if (!string.IsNullOrEmpty(vacancy.PipelineCode))
                {
                    List<PipelineSteps> pipelineStepList = VacancyManager.GetPipelineStepList(vacancy.PipelineCode);

                    ddNewStatus.DataSource = pipelineStepList;
                    ddNewStatus.DataTextField = "StatusName";
                    ddNewStatus.DataValueField = "StatusCode";
                    ddNewStatus.DataBind();

                    IDictionary<string, string> statusList = new Dictionary<string, string>();

                    foreach (PipelineSteps pipelineStep in pipelineStepList)
                    {
                        VacancyStatus pipelineStatus = ERecruitmentManager.GetVacancyStatus(pipelineStep.StatusCode);
                        if (pipelineStatus.IsArchive)
                            continue;
                        statusList.Add(pipelineStatus.Code, pipelineStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), pipelineStep.StatusCode) + ")");
                        statusCodeList.Add(pipelineStatus.Code);
                    }

                    if (Utils.GetQueryString<bool>("showArchives"))
                    {
                        List<VacancyStatus> archiveStatusList = VacancyManager.GetArchiveRecruitmentProcessList();
                        // add archived status
                        foreach (VacancyStatus archiveStatus in archiveStatusList)
                        {
                            statusList.Add(archiveStatus.Code, archiveStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), archiveStatus.Code) + ")");
                            statusCodeList.Add(archiveStatus.Code);
                        }

                        if (!statusCodeList.Contains(pipeline.AutoDisqualifiedRecruitmentStatusCode))
                        {
                            VacancyStatus defaultStatus = ERecruitmentManager.GetVacancyStatus(pipeline.AutoDisqualifiedRecruitmentStatusCode);
                            statusList.Add(defaultStatus.Code, defaultStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), defaultStatus.Code) + ")");
                            statusCodeList.Add(defaultStatus.Code);
                        }

                        if (!statusCodeList.Contains(pipeline.DisqualifiedRecruitmentStatusCode))
                        {
                            VacancyStatus defaultStatus = ERecruitmentManager.GetVacancyStatus(pipeline.DisqualifiedRecruitmentStatusCode);
                            statusList.Add(defaultStatus.Code, defaultStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), defaultStatus.Code) + ")");
                            statusCodeList.Add(defaultStatus.Code);
                        }

                        if (!statusCodeList.Contains(pipeline.ReferedDisqualifiedRecruitmentStatusCode))
                        {
                            VacancyStatus defaultStatus = ERecruitmentManager.GetVacancyStatus(pipeline.ReferedDisqualifiedRecruitmentStatusCode);
                            statusList.Add(defaultStatus.Code, defaultStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), defaultStatus.Code) + ")");
                            statusCodeList.Add(defaultStatus.Code);
                        }
                    }

                    ddStatus.DataSource = statusList;
                    ddStatus.DataTextField = "Value";
                    ddStatus.DataValueField = "Key";
                    ddStatus.DataBind();

                    ddStatus.Items.Insert(0, new ListItem("All" + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), statusCodeList.ToArray()) + ")", ""));
                }

                statusCodeList = new List<string>();
                Utils.SelectItem<string>(ddStatus, Utils.GetQueryString<string>("status"));
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("status")))
                    statusCodeList.Add(Utils.GetQueryString<string>("status"));
                else
                {
                    foreach (ListItem item in ddStatus.Items)
                    {
                        if (!string.IsNullOrEmpty(item.Value))
                            statusCodeList.Add(item.Value);
                    }
                }


                repApplicantList.DataSource = VacancyManager.GetApplicantVacancyListByVacancyCode(Utils.GetQueryString<string>("vacancyCode"),
                                                                                                  Utils.GetQueryString<string>("gender"),
                                                                                                  Utils.GetQueryString<string>("maritalStatus"),
                                                                                                  statusCodeList.ToArray(),
                                                                                                  Utils.GetQueryString<bool>("rankOnly"));
                repApplicantList.DataBind();

                hidCurrentUser.Value = CurrentUser.Code;

                #region check access page

                UserAccounts getUser = Session["currentuser"] as UserAccounts;
                if (getUser != null)
                {
                    List<UserRoles> getRoleList = AuthenticationManager.GetRoleList(getUser.Code);
                    if (getRoleList != null)
                    {
                        bool _accessSearchApplicant = false;
                        foreach (UserRoles userRole in getRoleList)
                        {

                            Roles checkRole = ERecruitmentManager.GetRole(userRole.RoleCode);
                            if (checkRole.SearchApplicant == false)
                            {
                                if (_accessSearchApplicant == false)
                                    dvDetailApplication.Visible = false;
                            }
                            else
                            {
                                dvDetailApplication.Visible = true;
                                _accessSearchApplicant = true;
                            }
                        }

                        if (_accessSearchApplicant == false)
                        {
                            ltMsgNotAuthorized.Text = "You're not authorized to access this page";
                        }
                    }
                }
                #endregion
            }
        }

        protected void btnSubmitFlag_ServerClick(object sender, EventArgs e)
        {
            UserAccounts getUser = Session["currentuser"] as UserAccounts;
            string[] applicantCodes = hidApplicantCodeFlag.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < applicantCodes.Length; i++)
            {
                ApplicantFlagStatuses flagRecord = new ApplicantFlagStatuses();
                flagRecord.ApplicantCode = applicantCodes[i];
                flagRecord.FlagStatus = hidSelectedFlag.Value;
                flagRecord.Notes = tbDescription.Text;
                flagRecord.IssuedByCode = getUser.Code;
                flagRecord.IssuedDate = DateTime.Now;

                ApplicantManager.AddApplicantFlag(flagRecord);
            }

            ApplicantVacancies applicantVacancy = VacancyManager.GetApplicantVacancy(hidVacancyCode.Value, hidApplicantCodeFlag.Value);
            VacancyManager.UpdateApplicantVacancyByFlagApplicant(applicantVacancy, "Rejected", DateTime.Now);
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        protected void repApplicantList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                ApplicantVacancies application = e.Item.DataItem as ApplicantVacancies;
                HtmlImage imgAvatar = e.Item.FindControl("imgAvatar") as HtmlImage;
                Applicants applicant = ApplicantManager.GetApplicant(application.ApplicantCode);

                if (applicant.Photo == null)
                {
                    if (applicant.SocMedName == "facebook")
                        imgAvatar.Src = "http://graph.facebook.com/" + applicant.SocMedID + "/picture?type=large";
                    else if (applicant.SocMedName == "linkedin")
                        imgAvatar.Src = applicant.LinkedInImageUrl;
                    else
                        imgAvatar.Src = "../assets/images/avatar.png";
                }
                else
                    imgAvatar.Src = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + application.ApplicantCode;


                HtmlGenericControl flagPanel = e.Item.FindControl("flagPanel") as HtmlGenericControl;
                if (!string.IsNullOrEmpty(applicant.FlagStatus))
                    flagPanel.Visible = false;
                if (applicant.FlagStatus == "GreenList")
                {
                    HtmlGenericControl flagGreen = e.Item.FindControl("flagGreen") as HtmlGenericControl;
                    flagGreen.Visible = true;
                }
                if (applicant.FlagStatus == "BlackList")
                {
                    HtmlGenericControl flagBlack = e.Item.FindControl("flagBlack") as HtmlGenericControl;
                    flagBlack.Visible = true;
                }

                HtmlGenericControl commandTestOffering = e.Item.FindControl("commandTestOffering") as HtmlGenericControl;
                HtmlGenericControl commandSocial = e.Item.FindControl("commandSocial") as HtmlGenericControl;
                HtmlGenericControl commandAction = e.Item.FindControl("commandAction") as HtmlGenericControl;

                commandTestOffering.Visible = application.Active;
                commandSocial.Visible = application.Active;
                commandAction.Visible = application.Active;
                
                Literal litRank = e.Item.FindControl("litRank") as Literal;
                if (application.Ranked)
                    litRank.Text = application.RankNumber.ToString();
                else
                    litRank.Text = "N/A";
            }
        }
    }
}