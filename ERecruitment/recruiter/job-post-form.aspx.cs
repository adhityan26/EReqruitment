﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;

namespace ERecruitment
{
    public partial class job_post_form: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            string[] postJobCodes = Utils.GetQueryString<string>("codes").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < postJobCodes.Length; i++)
            {
                Vacancies job = ERecruitmentManager.GetVacancy(postJobCodes[i]);
                ERecruitmentManager.PostAdvertisement(job, Utils.ConvertString<DateTime>(tbStartDate.Value), Utils.ConvertString<DateTime>(tbEndDate.Value));
            }

            string url =MainCompany.ATSBaseUrl + "recruiter/job-requisition-list.aspx";

            JQueryHelper.InvokeJavascript("showNotification('Vacancy successfully posted');window.close();window.location.href='" + url + "'", Page);
        }
    }
}