﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class getbatchprocessfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Clear();
            string processCode = Utils.GetQueryString<string>("code");
            BatchProcesses batchProcess = BatchProcessManager.GetBatchProcess(processCode);
            if (batchProcess.Attachment != null)
            {
                byte[] streams = batchProcess.Attachment;
                if (streams != null)
                {
                    Response.OutputStream.Write(streams, 0, streams.Length);
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + batchProcess.FileName + "\"");
                    Response.ContentType = batchProcess.FileType;

                    Response.End();
                }
            }
        }
    }
}