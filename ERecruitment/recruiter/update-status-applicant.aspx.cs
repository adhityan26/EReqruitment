﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class update_status_applicant : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidCurrentUser.Value = CurrentUser.Code;
                Vacancies getVacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));
                if (!string.IsNullOrEmpty(getVacancy.PipelineCode))
                {
                    List<PipelineSteps> pipelineSteps = HiringManager.GetPipelineStepList(getVacancy.PipelineCode);
                    ddStatus.DataSource = pipelineSteps;
                    ddStatus.DataTextField = "StatusName";
                    ddStatus.DataValueField = "StatusCode";
                    ddStatus.DataBind();
                }
            }
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        { }
    }
}