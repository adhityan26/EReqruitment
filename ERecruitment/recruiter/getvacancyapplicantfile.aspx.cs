﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class getvacancyapplicantfile : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Clear();
            string[] keys = Utils.GetQueryString<string>("code").Split(new char[] { '_' }, StringSplitOptions.RemoveEmptyEntries);
            ApplicantVacancyAttachmentFiles attachmentFile = ERecruitmentManager.GetVacancyApplicantAttachment(keys[0], keys[1], keys[2]);
            if (attachmentFile.Attachment != null)
            {
                byte[] streams = attachmentFile.Attachment;
                if (streams != null)
                {
                    Response.OutputStream.Write(streams, 0, streams.Length);
                    Response.AddHeader("Content-Disposition", "attachment; filename=\"" + attachmentFile.FileName + "\"");
                    Response.ContentType = attachmentFile.FileType;
                }
            }
        }
    }
}