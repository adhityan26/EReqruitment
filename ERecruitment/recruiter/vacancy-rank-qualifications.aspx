﻿<%@ Page Title="Rank Qualifications" Language="C#" MasterPageFile="~/masters/applicant-vacancy.Master" AutoEventWireup="true" CodeBehind="vacancy-rank-qualifications.aspx.cs" Inherits="ERecruitment.vacancy_rank_qualifications" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">


    $(document).ready(function () {

        $("#liRank").addClass("active");
    });

    function clearRankSettings() {
        $(".rankInput").val("");
        $(".rankInput").trigger("chosen:updated");
    }

    </script>
    
    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="clearRankSettings(); return false;"><i class="fa fa-refresh"></i>Clear Rank Settings</button>
            <button class="btn btn-primary commandIcon" id="btnUpdate" onserverclick="btnUpdate_ServerClick" runat="server">
                <i class="fa fa-recycle"></i>Update Rank
            </button>
        </div>
    </div>
    <h1 data-lang="ExperienceQTitle">Experience</h1>
    <div class="row">
        <div class="col-sm-12">           
            <div class="col-sm-2">
                <strong><span data-lang="MinimumYearQTitle">Minimum Year</span></strong>
            </div>             
            <div class="col-sm-1">
                <input id="tbMinimumExperienceYear" runat="server" class="form-control rankInput plainnumber" type="text" />                                        
            </div>           
            <div class="col-sm-3"></div>
            <div class="col-sm-1">
                <span data-lang="WeightQTitle"><strong>Weight</strong></span>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbMinimumExperienceYearWeight" runat="server" type="text" maxLength="2" />
            </div>             

        </div>
    </div>
    <h1 data-lang="EducationsQTitle">Educations</h1>
    
    <div class="row">
        <div class="col-sm-12">      
            <div class="col-sm-2">
                <strong><span data-lang="LevelQTitle">Level</span></strong>
            </div>         
            <div class="col-sm-4">
                <asp:DropDownList id="ddEducationLevel" DataSourceID="odsLevel" DataValueField="EducationLevel" 
                        DataTextField="Name"    
                        OnDataBound="ddDatabound"
                        runat="server" class="form-control rankInput" />  
            </div>           
            <div class="col-sm-1">
                <strong><span data-lang="WeightQTitle">Weight</span></strong>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbEducationLevelWeight" runat="server" type="text" maxLength="2" />                                        
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">      
            <div class="col-sm-2">
                <strong><span data-lang="FieldQTitle">Field</span></strong>
            </div>          
            <div class="col-sm-4">
                <asp:DropDownList id="ddFieldOfStudy" DataSourceID="odsFieldOfStudy" DataValueField="Code" 
                        DataTextField="Name"    
                        OnDataBound="ddDatabound"
                        runat="server" class="form-control rankInput" />  
            </div>           
            <div class="col-sm-1">
                <strong><span data-lang="WeightQTitle">Weight</span></strong>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbFieldOfStudyWeight" runat="server" type="text" maxLength="2" />                                        
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-sm-12">
                  
            <div class="col-sm-2">
                <strong><span data-lang="GPAQTitle">GPA</span></strong>
            </div>          
            <div class="col-sm-4">
                <input id="tbGpaStart" runat="server" class="form-control rankInput mediuminline plainnumber" type="text" />
                -
                <input id="tbGpaEnd" runat="server" class="form-control rankInput mediuminline plainnumber" type="text" />
            </div>           
            <div class="col-sm-1">
                <strong><span data-lang="WeightQTitle">Weight</span></strong>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbGpaWeight" runat="server" type="text" maxLength="2" />                                        
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-2">
                <strong><span data-lang="LocationQTitle">Location</span></strong>
            </div> 
            <div class="col-sm-4">
                <asp:DropDownList ID="ddLocation" DataSourceID="odsCity"  CssClass="form-control rankInput"
                        runat="server" DataTextField="CityName" DataValueField="CityCode"
                        OnDataBound="ddDatabound"
                                      />
            </div>           
            <div class="col-sm-1">
                <strong><span data-lang="WeightQTitle">Weight</span></strong>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbLocationWeight" runat="server" type="text" maxLength="2" />                                        
            </div>
        </div>
    </div>
    <h1 data-lang="OthersQTitle">Others</h1>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-2">
                <strong><span data-lang="ResidenceQTitle">Residence</span></strong>
            </div> 
            <div class="col-sm-4">
                <asp:DropDownList ID="ddResidence" DataSourceID="odsCity"  CssClass="form-control rankInput"
                        runat="server" DataTextField="CityName" DataValueField="CityCode"
                        OnDataBound="ddDatabound"
                                      />
            </div>           
            <div class="col-sm-1">
                <strong><span data-lang="WeightQTitle">Weight</span></strong>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbResidenceWeight" runat="server" type="text" maxLength="2" />                                        
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-2">
                <strong><span data-lang="SalaryQTitle">Salary</span></strong>
            </div> 
            <div class="col-sm-4">
                <input id="tbSalaryStart" runat="server" class="form-control rankInput mediuminline numeric" type="text" />
                                        
            -
                <input id="tbSalaryEnd" runat="server" class="form-control rankInput mediuminline numeric" type="text" />
                                        
            </div>           
            <div class="col-sm-1">
                <strong><span data-lang="WeightQTitle">Weight</span></strong>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbSalaryWeight" runat="server" type="text" maxLength="2" />                                        
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="col-sm-2">
                <strong><span data-lang="AgeQTitle">Age</span></strong>
            </div>
            <div class="col-sm-4">
                <input id="tbAgeStart" runat="server" class="form-control rankInput mediuminline plainnumber" type="text" />
                                        
            -
                <input id="tbAgeEnd" runat="server" class="form-control rankInput mediuminline plainnumber" type="text" />
                                        
            </div>           
            <div class="col-sm-1">
                <strong><span data-lang="WeightQTitle">Weight</span></strong>
            </div>             
            <div class="col-sm-1">
                <input class="form-control rankInput plainnumber" id="tbAgeWeight" runat="server" type="text" maxLength="2" />                                        
            </div>
        </div>
    </div>

    <asp:ObjectDataSource ID="odsLevel" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.EducationLevels"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetEducationLevels"
    />

    <asp:ObjectDataSource ID="odsCity" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.City"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCityList"
    />
    <asp:ObjectDataSource ID="odsFieldOfStudy" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.EducationFieldOfStudy"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                       SelectMethod="GetEducationFieldOfStudy"
    />
    <asp:ObjectDataSource ID="odsIndustry" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Industries"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetIndustryList"
    />
    <asp:ObjectDataSource ID="odsPosition" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Positions"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetPositionList"
    />

</asp:Content>
