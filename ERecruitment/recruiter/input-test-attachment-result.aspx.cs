﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class input_test_attachment_result : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Vacancies vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));
                Applicants applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("applicantCode"));

                repTest.DataSource = HiringManager.GetPipelineTestList(vacancy.PipelineCode);
                repTest.DataBind();

                repAttachment.DataSource = HiringManager.GetPipelineAttachmentList(vacancy.PipelineCode);
                repAttachment.DataBind();

                List<ApplicantVacancyTestResults> existingTestList = ERecruitmentManager.GetVacancyApplicantTestList(Utils.GetQueryString<string>("applicantCode"), Utils.GetQueryString<string>("vacancyCode"));
                foreach (RepeaterItem item in repTest.Items)
                {
                    HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                    ApplicantVacancyTestResults testResult = existingTestList.Find(delegate (ApplicantVacancyTestResults temp) { return temp.TestCode == hidCode.Value; });
                    if (testResult != null)
                    {
                        TextBox tbTestValue = item.FindControl("tbTestValue") as TextBox;
                        tbTestValue.Text = testResult.Result;
                    }

                }

                List<ApplicantVacancyAttachmentFiles> existingAttachmentList = ERecruitmentManager.GetVacancyApplicantAttachmentList(Utils.GetQueryString<string>("applicantCode"), Utils.GetQueryString<string>("vacancyCode"));
                foreach (RepeaterItem item in repAttachment.Items)
                {
                    HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                    ApplicantVacancyAttachmentFiles attachmentItem = existingAttachmentList.Find(delegate (ApplicantVacancyAttachmentFiles temp) { return temp.AttachmentCode == hidCode.Value; });
                    if (attachmentItem != null)
                    {
                        Literal litAttachmentName = item.FindControl("litAttachmentName") as Literal;
                        litAttachmentName.Text = attachmentItem.FileName;
                        litAttachmentName.Text = "<div id='" + attachmentItem.ApplicantCode + "_" + attachmentItem.VacancyCode + "_" + attachmentItem.AttachmentCode + "' class='link file'>" + attachmentItem.FileName + "</div>";
                    }

                }

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<ApplicantVacancyTestResults> testResultList = new List<ApplicantVacancyTestResults>();
            foreach (RepeaterItem item in repTest.Items)
            {
                HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                TextBox tbTestValue = item.FindControl("tbTestValue") as TextBox;
                ApplicantVacancyTestResults testResult = new ApplicantVacancyTestResults();
                testResult.TestCode = hidCode.Value;
                testResult.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                testResult.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                testResult.Result = tbTestValue.Text;
                testResultList.Add(testResult);
            }

            List<ApplicantVacancyAttachmentFiles> attachmentList = new List<ApplicantVacancyAttachmentFiles>();
            foreach (RepeaterItem item in repAttachment.Items)
            {
                HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                FileUpload fileUpload = item.FindControl("fileUpload") as FileUpload;
                ApplicantVacancyAttachmentFiles attachment = new ApplicantVacancyAttachmentFiles();
                attachment.AttachmentCode = hidCode.Value;
                attachment.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                attachment.ApplicantCode = Utils.GetQueryString<string>("applicantCode");
                attachment.Attachment = Utils.GetFile(fileUpload);
                attachment.FileName = fileUpload.FileName;
                attachment.FileType = fileUpload.PostedFile.ContentType;
                attachmentList.Add(attachment);
            }

            List<ApplicantVacancyTestResults> existingTestList = ERecruitmentManager.GetVacancyApplicantTestList(Utils.GetQueryString<string>("applicantCode"), Utils.GetQueryString<string>("vacancyCode"));
            foreach (ApplicantVacancyTestResults item in existingTestList)
                ERecruitmentManager.DeleteVacancyApplicantTest(item);

            List<ApplicantVacancyAttachmentFiles> existingAttachmentList = ERecruitmentManager.GetVacancyApplicantAttachmentList(Utils.GetQueryString<string>("applicantCode"), Utils.GetQueryString<string>("vacancyCode"));
            foreach (ApplicantVacancyAttachmentFiles item in existingAttachmentList)
                ERecruitmentManager.DeleteVacancyApplicantAttachment(item);

            foreach (ApplicantVacancyTestResults item in testResultList)
                ERecruitmentManager.SaveVacancyApplicantTest(item);
            foreach (ApplicantVacancyAttachmentFiles item in attachmentList)
                ERecruitmentManager.SaveVacancyApplicantAttachment(item);

            JQueryHelper.InvokeJavascript("showNotification('Test/Attachment submitted'); window.opener.location.href = window.opener.location.href; window.close();", Page);
        }
    }
}