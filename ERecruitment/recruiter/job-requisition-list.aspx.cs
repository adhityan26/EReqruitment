﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;


namespace ERecruitment
{
    public partial class job_requisition_list : AuthorizedPage
    {

        #region init

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                btnCreateJob.Attributes.Add("onclick", "showNewForm(); return false;");
                hidJobDetailUrl.Value = MainCompany.ATSBaseUrl + "public/jobdetail.aspx";
                                
                #region check company role list

                UserAccounts getUserAccount = CurrentUser;
                MainCompany.CompanyCodes += "," + MainCompany.Code;

                if (!string.IsNullOrEmpty(MainCompany.CompanyCodes))
                {

                    string[] userCompanyCodes = MainCompany.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Companies> getCompanyAccessList = ERecruitmentManager.GetCompanyListByCodes(userCompanyCodes);
                    List<Companies> companyList = new List<Companies>();
                    foreach (Companies item in getCompanyAccessList)
                    {
                        if (companyList.Contains(item))
                            continue;
                        else
                            companyList.Add(item);
                    }

                    ddCompany.DataSource = companyList;
                    ddCompany.DataBind();
                }

                #endregion

                #region setup filter

                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("companyCode")))
                    Utils.SelectItem<string>(ddCompany, Utils.GetQueryString<string>("companyCode"));


                ddJobCategory.DataBind();
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("jobCategoryCode")))
                    Utils.SelectItem<string>(ddJobCategory, Utils.GetQueryString<string>("jobCategoryCode"));

                ddJobLocation.DataBind();
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("locationCode")))
                    Utils.SelectItem<string>(ddJobLocation, Utils.GetQueryString<string>("locationCode"));

                if (Utils.GetQueryString<bool>("showArchive"))
                {
                    ddStatus.Items.Add(new ListItem("Closed", "Closed"));
                    ddStatus.Items.Add(new ListItem("Deleted", "Deleted"));
                }

                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("status")))
                    Utils.SelectItem<string>(ddStatus, Utils.GetQueryString<string>("status"));

                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("advStatus")))
                    Utils.SelectItem<string>(ddAdvertisementStatus, Utils.GetQueryString<string>("advStatus"));
                
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("orderField")))
                    Utils.SelectItem<string>(ddOrderField, Utils.GetQueryString<string>("orderField"));
                
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("orderDirection")))
                    Utils.SelectItem<string>(ddOrderDirection, Utils.GetQueryString<string>("orderDirection"));
                #endregion

                #region binding job list

                List<string> companyCodeList = new List<string>();
                if (string.IsNullOrEmpty(Utils.GetQueryString<string>("companyCode")))
                {
                    foreach (ListItem item in ddCompany.Items)
                    {
                        if (!string.IsNullOrEmpty(item.Value))
                            companyCodeList.Add(item.Value);
                    }
                }
                else
                    companyCodeList.Add(Utils.GetQueryString<string>("companyCode"));

                List<string> statusCodeList = new List<string>();
                if (string.IsNullOrEmpty(Utils.GetQueryString<string>("status")))
                {
                    foreach (ListItem item in ddStatus.Items)
                    {
                        if (!string.IsNullOrEmpty(item.Value))
                            statusCodeList.Add(item.Value);
                    }
                }
                else
                    statusCodeList.Add(Utils.GetQueryString<string>("status"));

                var vacancyList = ERecruitmentManager.GetVacancyList(companyCodeList.ToArray(),
                    Utils.GetQueryString<string>("jobCategoryCode"),
                    Utils.GetQueryString<string>("locationCode"),
                    statusCodeList.ToArray(),
                    Utils.GetQueryString<string>("advStatus"));

                string orderFieldParam = "PositionName";
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("orderField")))
                    orderFieldParam = Utils.GetQueryString<string>("orderField");
                
                var orderParam = typeof(Vacancies).GetProperty(orderFieldParam);

                if (orderParam != null)
                {
                    if (Utils.GetQueryString<string>("orderDirection") == "desc")
                    {
                        vacancyList = vacancyList.OrderByDescending(o => orderParam.GetValue(o, null)).ToList();
                    }
                    else
                    {
                        vacancyList = vacancyList.OrderBy(o => orderParam.GetValue(o, null)).ToList();   
                    }   
                }

                repJobList.DataSource = vacancyList; 
                repJobList.DataBind();

                #endregion
            }
        }

        #endregion

        #region support

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            if (dd != null)
            {
                dd.Items.Insert(0, new ListItem("All", string.Empty));
            }
        }

        #endregion

        protected void repJobList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Vacancies vacancy = e.Item.DataItem as Vacancies;
                //HtmlImage imgLogo = e.Item.FindControl("imgLogo") as HtmlImage;
                //imgLogo.Src = "../handlers/HandlerUI.ashx?commandName=GetCompanyLogo&code=" + vacancy.CompanyCode;

                Literal litSalaryRange = e.Item.FindControl("litSalaryRange") as Literal;
                litSalaryRange.Text = Utils.DisplayMoneyAmount(vacancy.SalaryRangeBottom) + " - " + Utils.DisplayMoneyAmount(vacancy.SalaryRangeTop);
            }
        }
    }
}
