﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="job-post-form.aspx.cs" Inherits="ERecruitment.job_post_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .jobs .panel-body > div > div{
            padding: 5px !important;
        }
    </style>
<div>
<div>
<div>
<div class="row">
<div class="row">
<div class="col-sm-12">

    <div class="row">
        <div class="col-sm-3">
            <label>Active Date</label><span class="requiredmark">*</span>
        </div>
        <div class="col-sm-3">
            <div class="controls">
			<div class="input-group date col-sm-9">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				<input type="text" data-date-format="mm/dd/yyyy" id="tbStartDate" runat="server" required="" 
                    class="form-control datepicker" />
			</div>
            </div>              
        </div>
    </div>
  
    <div class="row">
        <div class="col-sm-3">
            <label>Expired Date</label><span class="requiredmark">*</span>
        </div>
        <div class="col-sm-3">
            <div class="controls">
			<div class="input-group date col-sm-9">
				<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
				<input type="text" data-date-format="mm/dd/yyyy" id="tbEndDate" required=""
                    runat="server" class="form-control datepicker" />
			</div>
            </div>              
        </div>
    </div>

    
    <div class="row">
        <div class="col-sm-3">
                
        </div>
        <div class="col-sm-3">
            <button class="btn btn-primary" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick">Post Job</button>                
        </div>
    </div>

</div>
</div>        
</div>
</div>
</div>
</div>
</asp:Content>
