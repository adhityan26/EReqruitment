﻿<%@ Page Title="Generate Report" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="reports.aspx.cs" Inherits="ERecruitment.reports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            $('.date-picker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });

        });

        function generateReport() {
            var startDate = $("#<%=tbStartDate.ClientID %>").val();
                var endDate = $("#<%=tbEndDate.ClientID %>").val();

                if (startDate == "" || endDate == "") {
                    showNotification("date range required");
                }
                else {
                    var param = "StartDate:" + startDate + ":date;";
                    param += "EndDate:" + endDate + ":date;";
                    param += "CompanyCode:" + $("#<%=hidCompanyCode.ClientID %>").val() + ":string;";
                    var url = $("#<%= hidReportGeneratorUrl.ClientID %>").val() + "?code=" + $("#<%= ddReport.ClientID %>").val() + "&param=" + param;
                    var dlWindow = popitup(url, 1, 1);
                }
        }

        function popitup(url, height, width) {
            newwindow = window.open(url, 'Page Info', 'height=' + height + ',width=' + width + ', toolbar=0, location=0, resizable=1, scrollbars=1');
            newwindow.moveTo(20, 20);
            if (window.focus) { newwindow.focus() }
            return newwindow;
        }

    </script>

    <asp:HiddenField ID="hidReportGeneratorUrl" runat="server" />
    <asp:HiddenField ID="hidCompanyCode" runat="server" />
    <div>
        <div class="row">
            <div class="col-sm-12">
                <b>
                    <asp:Label ID="jobAgeCalculation" runat="server"></asp:Label></b>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-1">
                <strong><span data-lang="DateReportTitle">Date</span></strong>
            </div>
            <div class="col-sm-3">
                <div class="controls">
                    <div class="input-group date col-sm-12">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" data-date-format="dd/mm/yyyy"
                            id="tbStartDate" runat="server" required="" placeholder="start date"
                            class="form-control datepicker" />
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="controls">

                    <div class="input-group date col-sm-12">
                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                        <input type="text" data-date-format="dd/mm/yyyy"
                            id="tbEndDate" runat="server" required="" placeholder="end date"
                            class="form-control datepicker" />
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-1">
                <strong><span data-lang="ReportTitle">Report</span></strong>
            </div>
            <div class="col-sm-3">
                <asp:DropDownList ID="ddReport"
                    runat="server"
                    CssClass="form-control"
                    DataValueField="Code"
                    DataTextField="Name" />
            </div>
        </div>

        <div class="row">

            <div class="col-sm-1">
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary" onclick="generateReport(); return false;" id="btnDownloadTemplate">Generate Report</button>
            </div>

        </div>

    </div>
</asp:Content>
