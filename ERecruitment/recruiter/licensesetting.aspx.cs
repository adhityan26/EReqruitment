﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class licensesetting: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                IniateSetting();
        }

        private void IniateSetting()
        {
            ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
            if (getSetting != null)
            {
                cbApplicantPortal.Checked = getSetting.ApplicantPortal;
                cbAts.Checked = getSetting.ApplicantTrackingSystem;
                cbApprovalFunction.Checked = getSetting.ApprovalFunction;
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
            if (getSetting != null)
            {
                getSetting.ApplicantPortal = cbApplicantPortal.Checked;
                getSetting.ApplicantTrackingSystem = cbAts.Checked;
                getSetting.ApprovalFunction = cbApprovalFunction.Checked;

                ERecruitmentManager.UpdateApplicationSetting(getSetting);
                JQueryHelper.InvokeJavascript("showNotification('Successfully updated')", Page);
            }
            else
            {
                ApplicationSettings applicationSetting = new ApplicationSettings();
                applicationSetting.ApplicantPortal = cbApplicantPortal.Checked;
                applicationSetting.ApplicantTrackingSystem = cbAts.Checked;
                applicationSetting.ApprovalFunction = cbApprovalFunction.Checked;
                ERecruitmentManager.SaveApplicationSetting(applicationSetting);
                JQueryHelper.InvokeJavascript("showNotification('Successfully saved')", Page);
            }
        }
    }
}