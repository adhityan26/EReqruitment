﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="detail-attribute-applicant.aspx.cs" Inherits="ERecruitment.detail_attribute_applicant" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link id="linkFavicon" href="../handlers/HandlerUI.ashx?commandName=GetMainSiteFavicon" runat="server" rel="shortcut icon" type="image/x-icon" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../assets/js/jquery.min.js"></script>
    <script src="../assets/js/bootstrap.min.js"></script>
    <script src="../assets/js/jquery.easyPaginate.js"></script>
    <script src="../assets/js/list.min.js"></script>
    <script src="../assets/js/jquery.dataTables.min.js"></script>
    <script src="../assets/js/utilities.js"></script>
    <script src="../assets/js/jquery.flot.min.js" ></script>
	<script src="../assets/js/jquery.flot.pie.min.js" ></script>
	<script src="../assets/js/jquery.flot.resize.min.js" ></script>
	<script src="../assets/js/jquery.flot.time.min.js" ></script>
    <script src="../assets/js/jquery.flot.categories.min.js" ></script>
    <script src="../assets/js/jquery.flot.distinct-bars.js" ></script>
    <script src="../assets/js/bootstrap-datepicker.min.js"></script>
    <script src="../assets/js/jquery.cleditor.min.js"></script>
    <script src="../assets/js/jquery.chosen.min.js"></script>
    <!-- Custom Theme files -->
    <link href="../assets/css/bootstrap-3.1.1.min.css" rel='stylesheet' type='text/css' />
    <link href="../assets/css/style.css" rel='stylesheet' type='text/css' />
    <link href="../assets/css/chosen.min.css" rel='stylesheet' type='text/css' />
    <link href="../assets/css/style-erecruitment.css" rel='stylesheet' type='text/css' />
    <asp:Literal ID="litCssPlaceHolder" runat="server" />
    <link href="../assets/css/jquery.cleditor.css" rel='stylesheet' type='text/css' />
    <link href='//fonts.googleapis.com/css?family=Roboto:100,200,300,400,500,600,700,800,900' rel='stylesheet' type='text/css' />
    <!----font-Awesome----->
    <link href="../assets/css/font-awesome.css" rel="stylesheet" />
    <!----font-Awesome----->
 

<script type="text/javascript">
    function loadEducation(appCode) {
        
        var ex = document.getElementById('tableEducation');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableEducation").dataTable().fnDestroy();
        }

        var param = "&applicantCode=" + appCode;
        var OTableData = $('#tableEducation').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "bVisible": false, "aTargets": [0,1, 2, 3] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] }

            ],

            "oLanguage":
                    { "sSearch": "Search By School/Institute/University" },

            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetApplicantEducationList" + param

        });
    }
    function loadSkill(appCode) {
        var ex = document.getElementById('tableSkill');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableSkill").dataTable().fnDestroy();
        }

       
        var param = "&applicantCode=" + appCode;
        var OTableData = $('#tableSkill').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [0,1,2, 3] }


            ],
            "oLanguage":
                                          { "sSearch": "Search By Skill" },
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetApplicantSkillList" + param

        });
    }

    function loadLanguage(appCode) {
        var ex = document.getElementById('tableLanguage');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableLanguage").dataTable().fnDestroy();
        }

        var param = "&applicantCode=" + appCode;
        var OTableData = $('#tableLanguage').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "gvColSelect", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [0,1,2, 3] },
                   { "sClass": "controlIcon vw", "aTargets": [0] },
                   { "sClass": "controlIcon remove", "aTargets": [1] }

            ],
            "oLanguage":
                                          { "sSearch": "Search By Language" },
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetApplicantLanguageList" + param

        });
    }
    function loadExperience(appCode) {
        var ex = document.getElementById('tableExperience');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableExperience").dataTable().fnDestroy();
        }

        var param = "&applicantCode=" + appCode;
        var OTableData = $('#tableExperience').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [0,1,2, 3] }

            ],
            "oLanguage":
                               { "sSearch": "Search By Position" },

            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetApplicantExperienceList" + param

        });
    }
    function loadDisqualifierAnswer(appCode)
    {
        var ex = document.getElementById('tableDisqualifierAnswer');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableDisqualifierAnswer").dataTable().fnDestroy();
        }

        var param = "&applicantCode=" + appCode;
        param += "&vacancyCode="+$("#<%= hidVacancyCode.ClientID %>").val();
        var OTableData = $('#tableDisqualifierAnswer').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] }


            ],
            "oLanguage":
                           { "sSearch": "Search By Question" },
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetDisqualifierAnswer" + param

        });

    }
    function loadOrganizationExperience(appCode)
    {
        var ex = document.getElementById('tableOrgExperience');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableOrgExperience").dataTable().fnDestroy();
        }

        var param = "&applicantCode=" + appCode;
        var OTableData = $('#tableOrgExperience').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [2, 3] }


            ],
            "oLanguage":
                           { "sSearch": "Search By Organization Name" },
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetApplicantOrganizationExperienceList" + param

        });
    }
    
    function loadAttachedFiles(appCode)
    { 
        var ex = document.getElementById('tableCV');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableCV").dataTable().fnDestroy();
        }

        var param = "&applicantCode=" + appCode;
        var OTableData = $('#tableCV').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 2] },
                     { "sClass": "controlIcon", "aTargets": [0] },
                     { "bVisible": false, "aTargets": [0] }

            ],
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetCurriculumVitaes" + param

        });

        var ex = document.getElementById('tableOtherAttachment');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableOtherAttachment").dataTable().fnDestroy();
        }

        var paramOtherAttachment = "&applicantCode=" + appCode;
        var OTableData = $('#tableOtherAttachment').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 2] },
                     { "sClass": "controlIcon", "aTargets": [0] },
                     { "bVisible": false, "aTargets": [0] }

            ],
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetOtherAttachments" + paramOtherAttachment

        });

    }
</script>


</head>
<body>
    <form id="form1" runat="server">
        <asp:HiddenField ID="hidVacancyCode" runat="server" />

   <button class="accordion" onclick="return false;" type="button">Basic Information<i class="fa fa-chevron-down" style="float:right;"></i></button>
<div class="panel">
    <div class="row" >
                <div class="col-sm-12">
                       <div class="col-sm-4">
                        <img id="imgfoto" width="150" height="150" runat="server" src="" alt="Photo"  />
                           <br />
                         </div>
                         <br />
                    <div class="row">
                         <div class="col-sm-3">
                            <label runat="server" id="lbDetailAtributeApp_Name">Name &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                            
                            <label class="col-sm-1">:</label>
                             <label id="lbName" runat="server" />
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-sm-3">
                            <label runat="server" id="lbDetailAtributeApp_Email">Email&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                            <label class="col-sm-1" style="padding-left:13px;">:</label>
                             <label id="lbEmail" runat="server"></label>
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-sm-3">
                            <label runat="server" id="lbDetailAtributeApp_Status">Status&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                             <label class="col-sm-1">:</label>
                             <label id="lbStatus" runat="server"></label>
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-sm-3">
                            <label runat="server" id="lbDetailAtributeApp_PhoneNumber">Phone Number</label>
                             <label class="col-sm-1">:</label>
                             <label id="lbPhoneNumber" runat="server"></label>
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-sm-3">
                            <label runat="server" id="lbDetailAtributeApp_Gender">Gender&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </label>
                             <label class="col-sm-1">:</label>
                             <label id="lbGender" runat="server"></label>
                        </div>
                    </div>
                     <div class="row">
                         <div class="col-sm-3">
                            <label runat="server" id="lbDetailAtributeApp_Address">Address&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                             <label class="col-sm-1">:</label>
                             <label id="lbAddress" runat="server"></label>
                        </div>
                    </div>
                </div>
             
        </div>
    </div>
    <button class="accordion" onclick="return false;" type="button">Education<i class="fa fa-chevron-down" style="float:right;"></i></button>   
        <div class="panel">
          
               <table class="table table-striped table-bordered dt-responsive nowrap" id="tableEducation">
                 <thead>
                    <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Level</th>
                        <th>School/Institute/University</th>
                        <th>Graduation Month</th>
                        <th>Graduation Year</th>
                        <th>Field of Study</th>
                    </tr>
               </thead>
                <tbody>
                </tbody>
            </table> 
             </div>
  <button class="accordion" onclick="return false;" type="button">Skill<i class="fa fa-chevron-down" style="float:right;"></i></button> 
        <div class="panel">
            <table class="table table-striped table-bordered dt-responsive nowrap" id="tableSkill">
                     <thead>
                        <tr>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>Skill</th>
                        <th>Proficiency</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
        </div>
   <button class="accordion" onclick="return false;" type="button">Language<i class="fa fa-chevron-down" style="float:right;"></i></button>
        <div class="panel">
            <table class="table table-striped table-bordered dt-responsive nowrap" id="tableLanguage">
        <thead>
        <tr>
             <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>Language</th>
            <th>Speaking</th>
            <th>Writing</th>
            <th>Listening</th>
            <th>Score</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
        </div>
 <button class="accordion" onclick="return false;" type="button">Experience<i class="fa fa-chevron-down" style="float:right;"></i></button>
    <div class="panel">      
                 <table class="table table-striped table-bordered dt-responsive nowrap" id="tableExperience">
        <thead>
        <tr>
             <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>Position</th>
            <th>Company</th>
            <th>Start</th>
            <th>End</th>
        </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
             </div>
 <button class="accordion" onclick="return false;" type="button">Organization Experience<i class="fa fa-chevron-down" style="float:right;"></i></button>
    <div class="panel">
               <table class="table table-striped table-bordered dt-responsive nowrap" id="tableOrgExperience">
        <thead>
        <tr>
             <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th>Organization Name</th>
            <th>Category</th>
            <th>Position In Organization</th>
            <th>Start Year</th>
            <th>End Year</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
              </div>
 <button class="accordion" onclick="return false;" type="button">Disqualifier Questions<i class="fa fa-chevron-down" style="float:right;"></i></button>
       <div class="panel">    
                 <table class="table striped hovered dataTable" id="tableDisqualifierAnswer">
        <thead>
        <tr>
            <th>Answer</th>
        </tr>
        <tr>
          
            <th>Question</th>
            <th>Answer</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
             </div>
     <button class="accordion" onclick="return false;" type="button">Attached Files<i class="fa fa-chevron-down" style="float:right;"></i></button>
       <div class="panel">
             <table class="table striped hovered dataTable" id="tableCV">
        <thead>
        <tr>
            <th>CV</th>
        </tr>
        <tr>
            <th></th>
            <th>File Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
        
           <table class="table striped hovered dataTable" id="tableOtherAttachment">
        <thead>
        <tr>
            <th>Other Attachment</th>
         </tr>
        <tr>
            <th></th>
            <th>File Name</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        </tbody>
    </table> 
    </div>
       
   
    </form>
  <script>
      var acc = document.getElementsByClassName("accordion");
      var i;

      for (i = 0; i < acc.length; i++) {
          acc[i].onclick = function () {
              this.classList.toggle("active");
              this.nextElementSibling.classList.toggle("show");
              var currState = $(this).find('i').prop("class");
              if(currState == "fa fa-chevron-down")
              {
                  $(this).find('i').removeClass('fa fa-chevron-down');
                  $(this).find('i').addClass('fa fa-chevron-right');
              }
              else {

                  $(this).find('i').removeClass('fa fa-chevron-right');
                  $(this).find('i').addClass('fa fa-chevron-down');
              }
              
          }
      }
      
</script>
  
</body>
</html>
