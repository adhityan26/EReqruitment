﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class job_requisition_form : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                listDisqQuestions.DataSource = DisqualificationQuestionManager.GetDisqualifierQuestionList();
                listDisqQuestions.DataBind();

                #region check company role list

                UserAccounts getUserAccount = CurrentUser;

                if (CurrentUser.IsAdmin)
                {
                    ddCompany.DataSource = ERecruitmentManager.GetCompanyList();
                    ddCompany.DataBind();

                }
                else
                {
                    if (!string.IsNullOrEmpty(MainRole.CompanyCodes))
                    {
                        string[] userCompanyCodes = MainRole.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        List<Companies> getCompanyAccessList = ERecruitmentManager.GetCompanyListByCodes(userCompanyCodes);
                        List<Companies> companyList = new List<Companies>();
                        foreach (Companies item in getCompanyAccessList)
                        {
                            if (companyList.Contains(item))
                                continue;
                            else
                                companyList.Add(item);
                        }
                        ddCompany.DataSource = companyList;
                        ddCompany.DataBind();
                    }
                }

                #endregion

                initiateData();
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("vacancyCode")))
                    IsEditable(Utils.GetQueryString<string>("vacancyCode"));
                else
                {
                    JQueryHelper.InvokeJavascript("toggleIDCardProvinceAndCity(); ", Page);
                }

            }
        }

        private void initiateData()
        {
            hidPrevPipeline.Value = "";
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("vacancyCode")))
            {
                Vacancies jobRequisition = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));
                if (jobRequisition != null)
                {
                    #region check question list

                    if (!string.IsNullOrEmpty(jobRequisition.DisqualifierQuestions))
                    {
                        string[] surveyQuestionCodes = jobRequisition.DisqualifierQuestions.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < surveyQuestionCodes.Length; i++)
                        {
                            ListItem item = listDisqQuestions.Items.FindByValue(surveyQuestionCodes[i]);
                            if (item != null)
                                item.Selected = true;
                        }
                    }

                    #endregion

                    tbHiringManager.Value = jobRequisition.HiringManager;
                    tbJobStage.Value = jobRequisition.JobStage;
                    tbCostCentre.Value = Utils.DisplayMoneyAmount(jobRequisition.CostCentre);
                    ddTypeofEmployment.DataBind();
                    ddTypeofEmployment.Value = jobRequisition.TypeOfEmployment;
                    ddCountry.DataBind();
                    Utils.SelectItem(ddCountry, jobRequisition.Country);

                    if (jobRequisition.Country == "Indonesia")
                    {
                        ddProvince.DataBind();
                        ddProvince.Value = jobRequisition.Province;

                        ddCity.DataBind();
                        ddCity.Value = jobRequisition.City;
                    }
                    else
                    {
                        tbProvince.Value = jobRequisition.Province;
                        tbCity.Value = jobRequisition.City;
                    }


                    tbBusinessJustification.Value = jobRequisition.BusinessJustification;
                    tbHiringManager.Value = jobRequisition.HiringManager;
                    ddJobCode.DataBind();
                    Utils.SelectItem(ddJobCode, jobRequisition.JobCode);
                    tbJobRole.Value = jobRequisition.JobRole;
                    tbJobStage.Value = jobRequisition.JobStage;
                    //ddDepartment.DataBind();
                    //ddDepartment.Value = jobRequisition.Department;
                    //ddDivision.DataBind();
                    //ddDivision.Value = jobRequisition.Division;
                    inpDepartment.Value = jobRequisition.Department;
                    inpDivision.Value = jobRequisition.Division;
                    tbNumberOfOpening.Value = jobRequisition.NumberOfOpening.ToString();
                    tbJobDescription.Value = jobRequisition.JobDescription;
                    tbJobRequirements.Value = jobRequisition.JobRequirement;
                    ddRequiredEducationLevel.DataBind();
                    ddRequiredEducationLevel.Value = jobRequisition.RequiredEducationLevel;
                    ddCategory.DataBind();
                    ddCategory.Value = jobRequisition.CategoryCode;
                    ddCompany.DataBind();
                    ddCompany.Value = jobRequisition.CompanyCode;
                    hidPrevPipeline.Value = jobRequisition.PipelineCode;

                    ddJobPosition.DataSource = ERecruitmentManager.GetPositionListByCompanyCode(jobRequisition.CompanyCode);
                    ddJobPosition.DataBind();
                    ddJobPosition.Value = jobRequisition.PositionCode;

                    ddPipeline.DataBind();
                    ddPipeline.Value = jobRequisition.PipelineCode;
                    ListItem newPosition = ddNewPosition.Items.FindByText(jobRequisition.NewPosition);
                    if (newPosition != null)
                        newPosition.Selected = true;
                    ListItem jobPostingOption = ddJobPostingOption.Items.FindByText(jobRequisition.JobPostingOption);
                    if (jobPostingOption != null)
                        jobPostingOption.Selected = true;


                    ListItem recruitmentGroup = ddRecruitmentTargetGroup.Items.FindByText(jobRequisition.RecruitmentTargetGroup);
                    if (recruitmentGroup != null)
                        recruitmentGroup.Selected = true;

                    tbSalaryRangeBottom.Value = Utils.DisplayMoneyAmount(jobRequisition.SalaryRangeBottom);
                    tbSalaryRangeTop.Value = Utils.DisplayMoneyAmount(jobRequisition.SalaryRangeTop);
                    tbPreferredStartDate.Value = jobRequisition.PreferredStartDate.ToString("dd/MM/yyyy");
                    if (jobRequisition.PostDate.HasValue)
                        tbPostStartDate.Value = jobRequisition.PostDate.Value.ToString("dd/MM/yyyy");
                    if (jobRequisition.ExpiredPostDate.HasValue)
                        tbPostEndDate.Value = jobRequisition.ExpiredPostDate.Value.ToString("dd/MM/yyyy");

                    tbApprovedDate.Value = jobRequisition.ApprovedDate.ToString("dd/MM/yyyy");

                    ListItem showCompany = new ListItem();
                    ListItem showSalary = new ListItem();
                    if (jobRequisition.IsShowCompany)
                    {
                        showCompany = ddShowCompany.Items.FindByText("Yes");
                        if (showCompany != null)
                            showCompany.Selected = true;
                    }
                    else
                    {
                        showCompany = ddShowCompany.Items.FindByText("No");
                        if (showCompany != null)
                            showCompany.Selected = true;
                    }
                    if (jobRequisition.IsShowSalary)
                    {
                        showSalary = ddShowSalary.Items.FindByText("Yes");
                        if (showSalary != null)
                            showSalary.Selected = true;
                    }
                    else
                    {
                        showSalary = ddShowSalary.Items.FindByText("No");
                        if (showSalary != null)
                            showSalary.Selected = true;
                    }
                }
            }
        }

        private void IsEditable(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                Vacancies getVacancy = ERecruitmentManager.GetVacancy(code);
                if (getVacancy.AdvertisementStatus == "Closed" || getVacancy.AdvertisementStatus == "Posted" ||
                    getVacancy.Status == "Closed")
                {
                    tbHiringManager.Disabled = true;
                    tbJobStage.Disabled = true;
                    ddTypeofEmployment.Disabled = true;
                    ddJobPosition.Disabled = true;
                    ddCountry.Enabled = false;
                    if (getVacancy.Country == "Indonesia")
                    {
                        ddProvince.Disabled = true;
                        ddCity.Disabled = true;
                    }
                    else
                    {
                        tbProvince.Disabled = true;
                        tbCity.Disabled = true;
                    }

                    tbBusinessJustification.Disabled = true;
                    tbHiringManager.Disabled = true;
                    ddJobCode.Enabled = false;
                    tbJobRole.Disabled = true;
                    tbJobStage.Disabled = true;
                    ddDepartment.Disabled = true;
                    tbNumberOfOpening.Disabled = true;
                    tbJobDescription.Disabled = true;
                    tbJobRequirements.Disabled = true;
                    ddRequiredEducationLevel.Disabled = true;
                    ddCategory.Disabled = true;
                    ddCompany.Disabled = true;
                    ddNewPosition.Disabled = true;
                    ddJobPostingOption.Disabled = true;
                    ddRecruitmentTargetGroup.Disabled = true;
                    tbSalaryRangeBottom.Disabled = true;
                    tbSalaryRangeTop.Disabled = true;
                    tbPreferredStartDate.Disabled = true;
                    tbPostStartDate.Disabled = true;
                    tbPostEndDate.Disabled = true;
                    tbApprovedDate.Disabled = true;
                    ddShowCompany.Disabled = true;
                    ddShowSalary.Disabled = true;
                    ddDivision.Disabled = true;
                    tbCostCentre.Disabled = true;
                    btnSubmit.Visible = false;
                    ddPipeline.Disabled = true;
                    inpDepartment.Disabled = true;
                    inpDivision.Disabled = true;
                }
            }
        }

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            Utils.InsertInitialListItem(dd);
        }

        #region button

        protected void nextbutton_ServerClick(object sender, EventArgs e)
        {

            Response.Redirect(MainCompany.ATSBaseUrl + "recruiter/job-requisition-list.aspx");
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                UserAccounts user = Session["currentuser"] as UserAccounts;
                if (string.IsNullOrEmpty(Utils.GetQueryString<string>("vacancyCode")))
                {
                    Vacancies addVacancy = new Vacancies();
                    addVacancy.CompanyCode = ddCompany.Value;
                    addVacancy.PositionCode = Request[ddJobPosition.UniqueID];
                    addVacancy.PositionName = hJobName.Value;
                    addVacancy.HiringManager = tbHiringManager.Value;
                    addVacancy.JobStage = tbJobStage.Value;
                    addVacancy.TypeOfEmployment = ddTypeofEmployment.Value;
                    addVacancy.Country = Utils.GetSelectedValue<string>(ddCountry);
                    addVacancy.CostCentre = Utils.ConvertString<decimal>(tbCostCentre.Value);
                    if (addVacancy.Country == "Indonesia")
                    {
                        addVacancy.Province = ddProvince.Value;
                        addVacancy.City = ddCity.Value;
                    }
                    else
                    {
                        addVacancy.Province = tbProvince.Value;
                        addVacancy.City = tbCity.Value;
                    }
                    addVacancy.BusinessJustification = tbBusinessJustification.Value;
                    addVacancy.HiringManager = tbHiringManager.Value;
                    addVacancy.JobCode = hJobCode.Value;
                    addVacancy.JobRole = tbJobRole.Value;
                    addVacancy.JobStage = tbJobStage.Value;
                    //addVacancy.Department = ddDepartment.Value;
                    addVacancy.Department = inpDepartment.Value;
                    addVacancy.NumberOfOpening = Utils.ConvertString<int>(tbNumberOfOpening.Value);
                    addVacancy.JobRequirement = tbJobRequirements.Value;
                    addVacancy.JobDescription = tbJobDescription.Value;
                    addVacancy.RecruitmentTargetGroup = ddRecruitmentTargetGroup.Value;
                    addVacancy.JobPostingOption = ddJobPostingOption.Value;
                    addVacancy.NewPosition = ddNewPosition.Value;
                    
                    if (Utils.ConvertString<decimal>(tbSalaryRangeBottom.Value) < 1 || Utils.ConvertString<decimal>(tbSalaryRangeTop.Value) < 1)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Salary range cannot be 0', 'error');",
                            Page);
                        return;
                    }
                    
                    addVacancy.SalaryRangeBottom = Utils.ConvertString<decimal>(tbSalaryRangeBottom.Value);
                    addVacancy.SalaryRangeTop = Utils.ConvertString<decimal>(tbSalaryRangeTop.Value);
                    addVacancy.Active = true;
                    //addVacancy.Division = ddDivision.Value;
                    addVacancy.Division = inpDivision.Value;
                    addVacancy.RequiredEducationLevel = ddRequiredEducationLevel.Value;
                    addVacancy.PipelineCode = ddPipeline.Value;
                    addVacancy.InsertedBy = user.Code;
                    addVacancy.UpdatedBy = user.Code;
                    addVacancy.UpdateStamp = DateTime.Now;
                    addVacancy.InsertStamp = DateTime.Now;
                    addVacancy.Code = Guid.NewGuid().ToString();
                    addVacancy.CategoryCode = ddCategory.Value;

                    //preferred start date
                    addVacancy.PreferredStartDate =
                        Utils.ConvertFormatDateNumericToDateTime(tbPreferredStartDate.Value);
                    addVacancy.PostDate = Utils.ConvertFormatDateNumericToDateTime(tbPostStartDate.Value);
                    addVacancy.ExpiredPostDate = Utils.ConvertFormatDateNumericToDateTime(tbPostEndDate.Value);
                    addVacancy.ApprovedDate = Utils.ConvertFormatDateNumericToDateTime(tbApprovedDate.Value);

                    if (ddShowSalary.Value == "Yes")
                        addVacancy.IsShowSalary = true;
                    else
                        addVacancy.IsShowSalary = false;

                    if (ddShowCompany.Value == "Yes")
                        addVacancy.IsShowCompany = true;
                    else
                        addVacancy.IsShowCompany = false;

                    #region validate post date and expected joining date

                    if (addVacancy.ExpiredPostDate < addVacancy.PostDate && addVacancy.PreferredStartDate < addVacancy.ExpiredPostDate)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Job posting end date must be greater than or equal to job posting start date and Expected joining date must be greater than or equal to job posting end date', 'error');",
                            Page);
                        return;
                    }

                    if (addVacancy.ExpiredPostDate < addVacancy.PostDate)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Job posting end date must be greater than or equal to job posting start date', 'error');",
                            Page);
                        return;
                    }

                    if (addVacancy.PreferredStartDate < addVacancy.ExpiredPostDate)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Expected joining date must be greater than or equal to job posting end date', 'error');",
                            Page);
                        return;
                    }

                    #endregion

                    List<string> disqQuestionCodeList = new List<string>();
                    foreach (ListItem item in listDisqQuestions.Items)
                        if (item.Selected)
                            disqQuestionCodeList.Add(item.Value);
                    addVacancy.DisqualifierQuestions = string.Join(",", disqQuestionCodeList.ToArray());

                    ERecruitmentManager.SaveVacancy(addVacancy);
                    JQueryHelper.InvokeJavascript("showNotification('Data Saved');backToList();", Page);
                    //Response.Redirect (MainCompany.ATSBaseUrl + "recruiter/job-requisition-list.aspx");
                }
                else
                {
                    Vacancies getVacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));
                    bool resetApplicant = getVacancy.PipelineCode != ddPipeline.Value;
                    getVacancy.CompanyCode = ddCompany.Value;
                    getVacancy.HiringManager = tbHiringManager.Value;
                    getVacancy.JobStage = tbJobStage.Value;
                    getVacancy.TypeOfEmployment = ddTypeofEmployment.Value;
                    getVacancy.Country = Utils.GetSelectedValue<string>(ddCountry);
                    getVacancy.CostCentre = Utils.ConvertString<decimal>(tbCostCentre.Value);
                    if (getVacancy.Country == "Indonesia")
                    {
                        getVacancy.Province = ddProvince.Value;
                        getVacancy.City = ddCity.Value;
                    }
                    else
                    {
                        getVacancy.Province = tbProvince.Value;
                        getVacancy.City = tbCity.Value;
                    }
                    getVacancy.BusinessJustification = tbBusinessJustification.Value;
                    getVacancy.PipelineCode = ddPipeline.Value;
                    getVacancy.HiringManager = tbHiringManager.Value;
                    getVacancy.JobCode = hJobCode.Value;
                    getVacancy.JobRole = tbJobRole.Value;
                    getVacancy.JobStage = tbJobStage.Value;
                    getVacancy.Department = inpDepartment.Value;
                    getVacancy.Division = inpDivision.Value;
                    //getVacancy.Department = ddDepartment.Value;
                    //getVacancy.Division = ddDivision.Value;
                    getVacancy.NumberOfOpening = Utils.ConvertString<int>(tbNumberOfOpening.Value);
                    getVacancy.JobRequirement = tbJobRequirements.Value;
                    getVacancy.JobDescription = tbJobDescription.Value;
                    getVacancy.RecruitmentTargetGroup = ddRecruitmentTargetGroup.Value;
                    getVacancy.JobPostingOption = ddJobPostingOption.Value;
                    getVacancy.NewPosition = ddNewPosition.Value;
                    getVacancy.CategoryCode = ddCategory.Value;
                    
                    if (Utils.ConvertString<decimal>(tbSalaryRangeBottom.Value) < 1 || Utils.ConvertString<decimal>(tbSalaryRangeTop.Value) < 1)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Salary range cannot be 0', 'error');",
                            Page);
                        return;
                    }
                    
                    getVacancy.SalaryRangeBottom = Utils.ConvertString<decimal>(tbSalaryRangeBottom.Value);
                    getVacancy.SalaryRangeTop = Utils.ConvertString<decimal>(tbSalaryRangeTop.Value);
                    getVacancy.RequiredEducationLevel = ddRequiredEducationLevel.Value;
                    getVacancy.PipelineCode = ddPipeline.Value;
                    getVacancy.PreferredStartDate =
                        Utils.ConvertFormatDateNumericToDateTime(tbPreferredStartDate.Value);
                    getVacancy.PostDate = Utils.ConvertFormatDateNumericToDateTime(tbPostStartDate.Value);
                    getVacancy.ExpiredPostDate = Utils.ConvertFormatDateNumericToDateTime(tbPostEndDate.Value);
                    getVacancy.ApprovedDate = Utils.ConvertFormatDateNumericToDateTime(tbApprovedDate.Value);
                    getVacancy.UpdateStamp = DateTime.Now;
                    getVacancy.UpdatedBy = user.Code;

                    if (ddShowSalary.Value == "Yes")
                        getVacancy.IsShowSalary = true;
                    else
                        getVacancy.IsShowSalary = false;

                    if (ddShowCompany.Value == "Yes")
                        getVacancy.IsShowCompany = true;
                    else
                        getVacancy.IsShowCompany = false;

                    #region validate post date and expected joining date

                    if (getVacancy.ExpiredPostDate < getVacancy.PostDate && getVacancy.PreferredStartDate < getVacancy.ExpiredPostDate)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Job posting end date must be greater than or equal to job posting start date and Expected joining date must be greater than or equal to job posting end date', 'error');",
                            Page);
                        return;
                    }

                    if (getVacancy.ExpiredPostDate < getVacancy.PostDate)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Job posting end date must be greater than or equal to job posting start date', 'error');",
                            Page);
                        return;
                    }

                    if (getVacancy.PreferredStartDate < getVacancy.ExpiredPostDate)
                    {
                        JQueryHelper.InvokeJavascript(
                            "showNotification('Expected joining date must be greater than or equal to job posting end date', 'error');",
                            Page);
                        return;
                    }

                    #endregion

                    List<string> disqQuestionCodeList = new List<string>();
                    foreach (ListItem item in listDisqQuestions.Items)
                        if (item.Selected)
                            disqQuestionCodeList.Add(item.Value);
                    getVacancy.DisqualifierQuestions = string.Join(",", disqQuestionCodeList.ToArray());
                    ERecruitmentManager.UpdateVacancy(getVacancy, resetApplicant);

                    JQueryHelper.InvokeJavascript("showNotification('Data updated');backToList();", Page);
                }
            }
            catch (Exception ex)
            {
                JQueryHelper.InvokeJavascript(
                    "showNotification('" + ex.Message + "', 'error');",
                    Page);
            }
        }

        #endregion

        protected void repQuestionList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                HiddenField hidCode = e.Item.FindControl("hidCode") as HiddenField;
                HtmlInputCheckBox cbParent = e.Item.FindControl("cbParent") as HtmlInputCheckBox;
                cbParent.Attributes["class"] = "cbParent" + hidCode.Value;
                cbParent.Attributes["onchange"] = "checkParentQuestion('" + hidCode.Value + "');";

                Repeater repChildQuestionList = e.Item.FindControl("repChildQuestionList") as Repeater;
                repChildQuestionList.DataSource = DisqualificationQuestionManager.GetChildrenDisqualificationQuestionList(hidCode.Value);
                repChildQuestionList.DataBind();
            }
        }

        protected void repChildQuestionList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                DisqualifierQuestions disqQuestion = e.Item.DataItem as DisqualifierQuestions;

                HiddenField hidCode = e.Item.FindControl("hidCode") as HiddenField;
                HtmlInputCheckBox cbParent = e.Item.FindControl("cbParent") as HtmlInputCheckBox;
                cbParent.Attributes["class"] = "cb" + disqQuestion.ParentCode + " cbChild";
            }
        }
    }
}