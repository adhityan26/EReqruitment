﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Reflection;
using System.IO;


namespace ERecruitment
{
    public partial class reports : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ApplicationSettings recruiterSetting = ERecruitmentManager.GetApplicationSetting();
                if (recruiterSetting != null)
                {
                    String s = recruiterSetting.JobAgeCalculationMethod;
                    jobAgeCalculation.Text = "Job Age Calculation : " + s;
                }
                else {
                    jobAgeCalculation.Text = "Job Age Calculation : ";
                }
                ddReport.DataSource = ApplicationManager.GetReportList();
                ddReport.DataBind();

                hidReportGeneratorUrl.Value =  "../reportgen.aspx";
                hidCompanyCode.Value = MainCompany.Code;
            }
        }
    }
}