﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class attachment_form: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                IniateForm(Utils.GetQueryString<string>("code"));
        }
        private void IniateForm(string code)
        {
            if (!string.IsNullOrEmpty(code))
            {
                TestAttachments getTest = HiringManager.GetTestAttachment(code);
                if (getTest != null)
                    tbName.Value = getTest.Name;
            }
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
            {
                TestAttachments getTest = HiringManager.GetTestAttachment(Utils.GetQueryString<string>("code"));
                if (getTest != null)
                    getTest.Name = tbName.Value;
                HiringManager.UpdateTestAttachment(getTest);
                JQueryHelper.InvokeJavascript("showNotification('Successfully updated');window.close();window.opener.refresh();", Page);
            }
            else
            {
                TestAttachments addTest = new TestAttachments();
                addTest.Name = tbName.Value;
                addTest.Active = true;
                addTest.AttachmentCode = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-")); ;
                HiringManager.SaveTestAttachment(addTest);
                JQueryHelper.InvokeJavascript("showNotification('Successfully saved');window.close();window.opener.refresh();", Page);
            }
        }
    }
}