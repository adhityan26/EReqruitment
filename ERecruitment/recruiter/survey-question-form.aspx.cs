﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;


namespace ERecruitment
{
    public partial class survey_question_form1 : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
                    formData.ChangeMode(FormViewMode.Edit);
            }
        }

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            if (dd != null)
            {
                dd.Items.Insert(0, new ListItem("Select an Item", string.Empty));
            }
        }

        protected void liAddAnotherAnswer_Click(object sender, EventArgs e)
        {
            List<DisqualifierQuestionAnswerOptions> answerList = new List<DisqualifierQuestionAnswerOptions>();
            ListView repAnswerRange = formData.FindControl("repAnswerRange") as ListView;
            foreach (ListViewItem item in repAnswerRange.Items)
            {
                DisqualifierQuestionAnswerOptions answerOption = new DisqualifierQuestionAnswerOptions();
                TextBox tbAnswerLabel = item.FindControl("tbAnswerLabel") as TextBox;
                answerOption.Label = tbAnswerLabel.Text;
                if (!string.IsNullOrEmpty(answerOption.Label))
                    answerList.Add(answerOption);
            }

            answerList.Add(new DisqualifierQuestionAnswerOptions());

            repAnswerRange.DataSource = answerList;
            repAnswerRange.DataBind();

            JQueryHelper.InvokeJavascript("bindToggleRdl();", Page);
        }

        protected void repAnswerRange_ItemCommand(object source, ListViewCommandEventArgs e)
        {
            List<DisqualifierQuestionAnswerOptions> answerList = new List<DisqualifierQuestionAnswerOptions>();
            ListView repAnswerRange = formData.FindControl("repAnswerRange") as ListView;
            foreach (ListViewItem item in repAnswerRange.Items)
            {
                if (item == e.Item)
                    continue;
                DisqualifierQuestionAnswerOptions answerOption = new DisqualifierQuestionAnswerOptions();
                TextBox tbAnswerLabel = item.FindControl("tbAnswerLabel") as TextBox;
                answerOption.Label = tbAnswerLabel.Text;
                if (!string.IsNullOrEmpty(answerOption.Label))
                    answerList.Add(answerOption);
            }

            repAnswerRange.DataSource = answerList;
            repAnswerRange.DataBind();


        }

        protected void repAnswerRange_ItemDataBound(object sender, ListViewItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                DisqualifierQuestionAnswerOptions answerOption = e.Item.DataItem as DisqualifierQuestionAnswerOptions;

                RadioButton rdlCorrectAnswer = e.Item.FindControl("rdlCorrectAnswer") as RadioButton;
                if (answerOption.IsCorrectAnswer)
                    rdlCorrectAnswer.Checked = true;
            }

        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            if (formData.CurrentMode == FormViewMode.Insert)
                formData.InsertItem(true);
            else
                formData.UpdateItem(true);
        }

        protected void formData_ItemInserting(object sender, FormViewInsertEventArgs e)
        {


            e.Values["Type"] = "Parent";
            e.Values["QuestionType"] = "Survey";
            List<DisqualifierQuestionAnswerOptions> answerList = new List<DisqualifierQuestionAnswerOptions>();

            ListView repAnswerRange = formData.FindControl("repAnswerRange") as ListView;
            foreach (ListViewItem item in repAnswerRange.Items)
            {
                DisqualifierQuestionAnswerOptions answerOption = new DisqualifierQuestionAnswerOptions();
                TextBox tbAnswerLabel = item.FindControl("tbAnswerLabel") as TextBox;

                answerOption.Label = tbAnswerLabel.Text;
                if (!string.IsNullOrEmpty(answerOption.Label))
                    answerList.Add(answerOption);
            }
            e.Values["AnswerOptionList"] = answerList;
        }

        protected void formData_DataBound(object sender, EventArgs e)
        {
            if (formData.DataItem != null)
            {
                DisqualifierQuestions data = formData.DataItem as DisqualifierQuestions;
                List<DisqualifierQuestionAnswerOptions> answerLst = DisqualificationQuestionManager.GetAnswerOptionList(data.Code);

                ListView repAnswerRange = formData.FindControl("repAnswerRange") as ListView;
                repAnswerRange.DataSource = answerLst;
                repAnswerRange.DataBind();
                DropDownList ddNumericOperator = formData.FindControl("ddNumericOperator") as DropDownList;
                Utils.SelectItem<string>(ddNumericOperator, data.CorrectAnswerOperator);
            }
        }

        protected void formData_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {


            e.NewValues["Type"] = "Parent";
            e.NewValues["QuestionType"] = "Survey";
            List<DisqualifierQuestionAnswerOptions> answerList = new List<DisqualifierQuestionAnswerOptions>();

            ListView repAnswerRange = formData.FindControl("repAnswerRange") as ListView;
            foreach (ListViewItem item in repAnswerRange.Items)
            {
                DisqualifierQuestionAnswerOptions answerOption = new DisqualifierQuestionAnswerOptions();
                TextBox tbAnswerLabel = item.FindControl("tbAnswerLabel") as TextBox;

                answerOption.Label = tbAnswerLabel.Text;
                if (!string.IsNullOrEmpty(answerOption.Label))
                    answerList.Add(answerOption);
            }
            e.NewValues["AnswerOptionList"] = answerList;
        }

        protected void odsData_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
                if (getSetting != null)
                {
                    if (!getSetting.EnableSurveyQuestion)
                    {
                        getSetting.EnableSurveyQuestion = true;
                        ERecruitmentManager.UpdateApplicationSetting(getSetting);
                    }
                }
                JQueryHelper.InvokeJavascript("showNotification('Question saved'); window.close(); window.opener.location.href = window.opener.location.href;", Page);
            }
        }

        protected void odsData_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
                if (getSetting != null)
                {
                    if (!getSetting.EnableSurveyQuestion)
                    {
                        getSetting.EnableSurveyQuestion = true;
                        ERecruitmentManager.UpdateApplicationSetting(getSetting);
                    }
                }

                JQueryHelper.InvokeJavascript("showNotification('Question saved'); window.close(); window.opener.location.href = window.opener.location.href;", Page);
            }
                
        }
    }
}