﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Reflection;
using System.IO;


namespace ERecruitment
{
    public partial class batchprocess : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidDownloadUrl.Value = "../downloadimportschema.aspx?code=";

                Vacancies vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));
                Pipelines pipeline = HiringManager.GetPipeline(vacancy.PipelineCode);

                List<PipelineSteps> pipelineStepList = VacancyManager.GetPipelineStepList(vacancy.PipelineCode);

                List<string> statusCodeList = new List<string>();

                IDictionary<string, string> statusList = new Dictionary<string, string>();

                foreach (PipelineSteps pipelineStep in pipelineStepList)
                {
                    VacancyStatus pipelineStatus = ERecruitmentManager.GetVacancyStatus(pipelineStep.StatusCode);
                    if (pipelineStatus.IsArchive)
                        continue;
                    statusList.Add(pipelineStatus.Code, pipelineStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), pipelineStep.StatusCode) + ")");
                    statusCodeList.Add(pipelineStatus.Code);
                }

                if (Utils.GetQueryString<bool>("showArchives"))
                {
                    List<VacancyStatus> archiveStatusList = VacancyManager.GetArchiveRecruitmentProcessList();
                    // add archived status
                    foreach (VacancyStatus archiveStatus in archiveStatusList)
                    {
                        statusList.Add(archiveStatus.Code, archiveStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), archiveStatus.Code) + ")");
                        statusCodeList.Add(archiveStatus.Code);
                    }

                    if (!statusCodeList.Contains(pipeline.AutoDisqualifiedRecruitmentStatusCode))
                    {
                        VacancyStatus defaultStatus = ERecruitmentManager.GetVacancyStatus(pipeline.AutoDisqualifiedRecruitmentStatusCode);
                        statusList.Add(defaultStatus.Code, defaultStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), defaultStatus.Code) + ")");
                        statusCodeList.Add(defaultStatus.Code);
                    }

                    if (!statusCodeList.Contains(pipeline.DisqualifiedRecruitmentStatusCode))
                    {
                        VacancyStatus defaultStatus = ERecruitmentManager.GetVacancyStatus(pipeline.DisqualifiedRecruitmentStatusCode);
                        statusList.Add(defaultStatus.Code, defaultStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), defaultStatus.Code) + ")");
                        statusCodeList.Add(defaultStatus.Code);
                    }

                    if (!statusCodeList.Contains(pipeline.ReferedDisqualifiedRecruitmentStatusCode))
                    {
                        VacancyStatus defaultStatus = ERecruitmentManager.GetVacancyStatus(pipeline.ReferedDisqualifiedRecruitmentStatusCode);
                        statusList.Add(defaultStatus.Code, defaultStatus.RecruiterLabel + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), defaultStatus.Code) + ")");
                        statusCodeList.Add(defaultStatus.Code);
                    }
                }

                ddStatus.DataSource = statusList;
                ddStatus.DataTextField = "Value";
                ddStatus.DataValueField = "Key";
                ddStatus.DataBind();

                ddStatus.Items.Insert(0, new ListItem("All" + " (" + VacancyManager.CountVacancyApplicantStatus(Utils.GetQueryString<string>("vacancyCode"), statusCodeList.ToArray()) + ")", ""));
            }
        }

        protected void btnUpload_ServerClick(object sender, EventArgs e)
        {
            try
            {
                String type = fileUploadbatchProcess.PostedFile.ContentType;
                object batchProcessor = null;
                MethodInfo batchProcessCommand = null;
                BatchProcessSchemas processSchema = BatchProcessManager.GetBatchProcessSchemaByCode(Utils.GetSelectedValue<string>(ddBatchSchema));
                batchProcessor = Activator.CreateInstance(Type.GetType(processSchema.ProcesserAssemblyName));
                batchProcessCommand = batchProcessor.GetType().GetMethod(processSchema.ProcessMethodName);

                BatchProcesses process = new BatchProcesses();
                process.VacancyCode = Utils.GetQueryString<string>("vacancyCode");

                string[] listType =
                {
                    "application/vnd.ms-excel",
                    "text/plain",
                    "text/csv",
                    "text/tsv",
                    "application/octet-stream"
                };
                
                if (Array.Exists(listType, o => o == type))
                {
                    int fileSize = fileUploadbatchProcess.PostedFile.ContentLength;
                    if (fileSize > 5000000)
                    {
                        JQueryHelper.InvokeJavascript("showNotification('File size too large, Maximal file size is 5MB', 'error');", Page);
                        return;
                    } 
                    else if (fileSize == 0)
                    {
                        JQueryHelper.InvokeJavascript("showNotification('File is empty', 'error');", Page);
                        return;
                    }
                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid file type, allowed file type : csv', 'error');", Page);
                    return;
                }
                Dictionary<String, int> batchOption = new Dictionary<string, int>();
                batchOption.Add("emailTemplateId", Int32.Parse(ddEmailTemplate.SelectedValue));
                batchOption.Add("feedbackEmailTemplateId", Int32.Parse(ddEmailTemplateFeedback.SelectedValue));

                batchProcessCommand.Invoke(batchProcessor, new object[] { process, fileUploadbatchProcess.FileContent, processSchema.Code, CurrentUser.Code, batchOption });
                fileUploadbatchProcess.FileContent.Seek(0, SeekOrigin.Begin);
                process.Attachment = Utils.GetFile(fileUploadbatchProcess);
                process.FileName = fileUploadbatchProcess.FileName;
                process.FileType = fileUploadbatchProcess.PostedFile.ContentType;
                process.Date = DateTime.Now;
                BatchProcessManager.SaveBatchProcess(process);
                JQueryHelper.InvokeJavascript("showNotification('Batch process succeed'); window.loadData();", Page);
            }
            catch (Exception ex)
            {
                JQueryHelper.InvokeJavascript("showNotification('" + Utils.ExtractInnerExceptionMessage(ex) + "', 'error')", Page);
            }

        }
    }
}