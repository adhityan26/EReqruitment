﻿<%@ Page Title="Batch Process" Language="C#" MasterPageFile="~/masters/applicant-vacancy.Master" AutoEventWireup="true" CodeBehind="batchprocess.aspx.cs" Inherits="ERecruitment.batchprocess" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

  <script type="text/javascript">

      $(document).ready(function () {
          $("#liBatchProcess").addClass("active");
          showTemplateEmail();
          loadData();
      });
      
      function showTemplateEmail() {
          var emailSelect = $("#<%= ddEmailTemplate.ClientID %>").parent();
          var emailSelectFeedback = $("#<%= ddEmailTemplateFeedback.ClientID %>").parent();
          
          if ($("#<%= ddBatchSchema.ClientID %>").val() !== "04") {
              emailSelect.show();
          } else {
              emailSelect.hide();
          }
          if ($("#<%= ddBatchSchema.ClientID %>").val() === "02") {
              emailSelectFeedback.show();
          } else {
              emailSelectFeedback.hide();
          }
      }

      function loadData() {
          var ex = document.getElementById('tableData');
          if ($.fn.DataTable.fnIsDataTable(ex)) {
              // data table, then destroy first
              $("#tableData").dataTable().fnDestroy();
          }
                  
          var vacancyCode = GetQueryStringParams("vacancyCode");
          var status = $("#<%= ddStatus.ClientID %>").val();          
          var param = "&vacancyCode=" + vacancyCode;
          param += "&status=" + status;
          
          var OTableData = $('#tableData').dataTable({
              "bProcessing": true,
              "bServerSide": true,
              "iDisplayLength": 10,
              "bJQueryUI": true,
              "bAutoWidth": false,
              "sDom": "ftipr", responsive: true,
              "aaSorting": [],
              "bDeferRender": true,
              "aoColumnDefs": [

                   { "bVisible": false, "aTargets": [5] },
                   { "sClass": "downloadFile", "aTargets": [1] }

              ],              
              "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetBatchProcessList" + param

          });
      }

      $('body').on('click', '.downloadFile', function () {

          var oTable = $('#tableData').dataTable();
          var nTr = this.parentNode;

          var aData = oTable.fnGetData(nTr)[5];
          var url = "getbatchprocessfile.aspx?code=" + aData;
          popitup(url, 1, 1);

      });



      function popitup(url, popupname) {
          newwindow = window.open(url, popupname, 'height=550,width=750, toolbar=0, location=0,resizable=1, scrollbars=1');
          newwindow.moveTo(20, 20);
          if (window.focus) { newwindow.focus() }
          return newwindow;
      }

      function popitupSmall(url) {
          return popitup(url, 190, 230);
      }

      function popitup(url, height, width) {
          newwindow = window.open(url, 'Page Info', 'height=' + height + ',width=' + width + ', toolbar=0, location=0, resizable=1, scrollbars=1');
          newwindow.moveTo(20, 20);
          if (window.focus) { newwindow.focus() }
          return newwindow;
      }

      function popitup(url, height, width, name) {
          newwindow = window.open(url, name, 'height=' + height + ',width=' + width + ',location=0,resizable=1, scrollbars=1');
          newwindow.moveTo(20, 20);
          if (window.focus) { newwindow.focus() }
          return newwindow;
      }

      function downloadSchema() {
          
          var vacancyCode = GetQueryStringParams("vacancyCode");
          var status = $("#<%= ddStatus.ClientID %>").val();
          var param = "&vacancyCode=" + vacancyCode;
          param += "&status=" + status;
          var url =  $("#<%= hidDownloadUrl.ClientID %>").val() + $("#<%= ddBatchSchema.ClientID %>").val() + param;    
          var dlWindow = popitup(url, 1, 1);

      }

      function popitup(url, height, width) {
          newwindow = window.open(url, 'Page Info', 'height=' + height + ',width=' + width + ', toolbar=0, location=0, resizable=1, scrollbars=1');
          newwindow.moveTo(20, 20);
          if (window.focus) { newwindow.focus() }
          return newwindow;
      }

  </script>

    <asp:HiddenField ID="hidDownloadUrl" runat="server" />
    <div>
        
        <div class="row searchPanel">
            <div class="col-sm-2">
                <asp:DropDownList ID="ddStatus"
                                  runat="server"
                                  CssClass="form-control" 
                                  onchange="search();"
                                  />
            </div>
            <div class="col-sm-2">
                <asp:DropDownList ID="ddBatchSchema"
                                    runat="server"
                                    CssClass="form-control"
                                    DataSourceID="odsBatchSchema"
                                    DataValueField="Code"
                                    DataTextField="Name"
                                    onchange="showTemplateEmail();"
                                    />
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary commandIcon" onclick="downloadSchema(); return false;" runat="server" id="btnDownloadTemplate"><i class="fa fa-download"></i>D/L Template</button>
            </div>
            <div class="col-sm-2">
                <div id="emailTemplate">
                    <label style="font-weight: normal">Email Template</label>
                    
                    <asp:DropDownList ID="ddEmailTemplate"
                                      runat="server"
                                      CssClass="form-control"
                                      Width="150"
                                      DataSourceID="odsApplicantEmail"
                                      DataValueField="Id"
                                      DataTextField="TemplateName"/>
                </div>
                <div id="feedbackEmailTemplate" style="margin-top: 5px;">
                    <label style="font-weight: normal">Feedback Email Template</label>
                    
                    <asp:DropDownList ID="ddEmailTemplateFeedback"
                                      runat="server"
                                      CssClass="form-control"
                                      Width="150"
                                      DataSourceID="odsApplicantEmailEmpty"
                                      DataValueField="Id"
                                      DataTextField="TemplateName"/>
                </div>
            </div>
            <div class="col-sm-2">
                <asp:FileUpload ID="fileUploadbatchProcess" runat="server" />
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary commandIcon" runat="server" 
                    onserverclick="btnUpload_ServerClick" id="btnUploadBatchProcess"><i class="fa fa-upload"></i>Upload</button>
            </div>
        </div>
        
        <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
            <thead>
            <tr>
                <th><span data-lang="BatchTypeTitle">Batch Type</span></th>
                <th><span data-lang="BatchFileTitle">Batch File</span></th>
                <th><span data-lang="BatchUploadDateTitle">Upload Date</span></th>
                <th><span data-lang="BatchUploadedByTitle">Uploaded By</span></th>
                <th><span data-lang="BatchStatusTitle">Status</span></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>

    <asp:ObjectDataSource ID="odsBatchSchema"
                          runat="server"
                          TypeName="ERecruitment.Domain.BatchProcessManager"
                          DataObjectTypeName="ERecruitment.Domain.BatchProcessSchemas"
                          SelectMethod="GetBatchProcessSchemaList" />
    
    <asp:ObjectDataSource ID="odsApplicantEmail" 
                          runat="server"
                          DataObjectTypeName="ERecruitment.Domain.Emails"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          SelectMethod="GetEmailList" />
    
    <asp:ObjectDataSource ID="odsApplicantEmailEmpty" 
                          runat="server"
                          DataObjectTypeName="ERecruitment.Domain.Emails"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          SelectMethod="GetEmailListEmpty" />
    
</asp:Content>
