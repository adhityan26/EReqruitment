﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ERecruitment
{
    public partial class test_list: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                hidUrl.Value = MainCompany.ATSBaseUrl + "recruiter/test-form.aspx";
        }
    }
}