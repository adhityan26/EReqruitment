﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class my_profile : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (CurrentUser != null)
                {
                    UserAccounts getUserAccount = Session["currentuser"] as UserAccounts;
                    if (getUserAccount.Photo != null)
                        imgfoto.Src = "~/system/httphandlers/HandlerApplicants.ashx?commandName=GetUserAccountPhoto&code=" + getUserAccount.Code;
                    else
                        imgfoto.Src = "~/system/img/photodefault.gif";
                }
            }
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {

            string url = MainCompany.ATSBaseUrl + "recruiter/my-profile.aspx";
            UserAccounts getUserAccount = AuthenticationManager.GetUserAccount(CurrentUser.Code);
            if (fuPhoto.HasFile)
            {
                // validate size
                if (((double)fuPhoto.PostedFile.ContentLength / 1000) > 500)
                {
                    JQueryHelper.InvokeJavascript("showNotification('Photo must not be larger than 500 kb');window.location.href = ' " + url + "'", Page);
                    return;
                }

                bool invalidType = false;
                string ext = System.IO.Path.GetExtension(fuPhoto.PostedFile.FileName).ToLowerInvariant();

                if (ext != ".jpeg")
                    if (ext != ".jpg")
                        if (ext != ".png")
                            invalidType = true;

                if (invalidType)
                {
                    JQueryHelper.InvokeJavascript("showNotification('Invalid photo file, must select a *.jpeg, *.jpg, or *.png file.');window.location.href = ' " + url + "'", Page);
                    return;
                }

                getUserAccount.FileType = fuPhoto.PostedFile.ContentType;
                getUserAccount.FileName = fuPhoto.PostedFile.FileName;
                getUserAccount.Photo = Utils.GetFile(fuPhoto);

            }

            #region validate current password
            if (getUserAccount.Password != tbCurrentPassword.Value)
            {
                JQueryHelper.InvokeJavascript("showNotification('Invalid current password');", Page);
                return;
            }
            #endregion
            #region validate password match

            if (tbPassword.Value != tbConfirmPassword.Value)
            {
                JQueryHelper.InvokeJavascript("showNotification('Password not match');", Page);
                return;
            }

            #endregion

            getUserAccount.Password = tbPassword.Value;
            AuthenticationManager.UpdateUserAccountStamp(getUserAccount);

            // refresh user session
            Session["currentuser"] = getUserAccount;
            JQueryHelper.InvokeJavascript("showNotification('Successfully updated');window.location.href = ' " + url + "'", Page);

        }
    }
}