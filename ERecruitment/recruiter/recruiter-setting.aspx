﻿<%@ Page Title="Recruiter Setting" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="recruiter-setting.aspx.cs" Inherits="ERecruitment.recruiter_setting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <style>
        .tooltip-inner {
            white-space: pre-wrap;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <asp:HiddenField ID="confirmValue" runat="server" />
    <ul class="nav tab-menu nav-tabs" id="myTab" role="tablist"  >
        <li role="presentation" class="active"><a role="tab" data-toggle="tab" aria-controls="basicInfo" aria-expanded="true"  href="#basicInfo" data-lang="BasicSettingsTitle">Basic Settings</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">  
        <div class="tab-pane active" id="basicInfo" role="tabpanel">
            <div class="row">
                <div class="col-sm-2">
                    <strong><span data-lang="JobAgeCalculationTitle">Job Age Calculation</span></strong>
                </div>
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddJobAgeCalculation"
                                      runat="server"
                                      CssClass="form-control"
                        >
                        <asp:ListItem Value="PostDate" Text="Post Date" />
                        <asp:ListItem Value="CreateDate" Text="Create Date" />
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <i class="glyphicon glyphicon-exclamation-sign text-info" title="Post date = job requsition age will be counted from post date
Create date = job requsition will be counted from create date"></i>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <strong><span data-lang="JobAppModeTitle">Job Application Mode</span></strong>
                </div>
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddApplicationMode"
                                      runat="server"
                                      CssClass="form-control"
                        >
                        <asp:ListItem Value="Multi" Text="Multi" />
                        <asp:ListItem Value="Single" Text="Single" />
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <i class="glyphicon glyphicon-exclamation-sign text-info" title="Multi = applicant can be processed in paralel
Single = applicant can be processed in serial"></i>
                </div>
            </div>
            
        </div>
    </div>

    
    <div class="row">
        <div class="col-sm-12 text-center">
            <button class="btn btn-default"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
                Save Settings
        </button>         
        </div>
    </div>
    

    <asp:ObjectDataSource ID="odsRecruitmentStatus"
                          runat="server"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          DataObjectTypeName="ERecruitment.Domain.VacancyStatus"
                          SelectMethod="GetVacancyStatusList"
                          />
    <script>
        $(function () {

            $(".glyphicon-exclamation-sign").tooltip();
        });
    </script>
</asp:Content>