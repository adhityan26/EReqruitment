﻿<%@ Page Title="Home" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="dashboard.aspx.cs" Inherits="ERecruitment.dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {

            drawPieChart();
            drawBarChart();
            drawSurveyGraph();
            
            $('.date-picker').datepicker({
                format: "dd/mm/yyyy",
                autoclose: true
            });

            var table = $('#tableAppointment').dataTable({
                "ordering": true,
                "lengthChange": false,
                "searching": false,
                "processing": true,
                "pageLength": 10,
                "serverSide": true,
                "ajax": {
                    "url": "/handlers/HandlerVacancies.ashx?commandName=ListAppointment&companyCode=" + $("#<%= hidCompanyCode.ClientID %>").val(),
                    "data": function(params) {
                        params.filter = $('.filter').val();
                        params.dateFrom = $('.date-from').val();
                        params.dateTo = $('.date-to').val();
                    },
                    "type": "POST"
                },
                "columns": [
                    {
                        "data": "InviteTo",
                        "orderable": true
                    },
                    {
                        "data": "JobName",
                        "orderable": true
                    },
                    {
                        "data": "Date",
                        "orderable": true,
                        "render": function (data) {
                            return moment(parseInt(data.replace("/Date(", "").replace(")/", ""), 10)).format("YYYY/MM/DD");
                        }
                    },
                    {
                        "data": "Place",
                        "orderable": true
                    },
                    {
                        "data": "Accepted",
                        "orderable": true
                    },
                    {
                        "data": "Reschedule",
                        "orderable": true
                    },
                    {
                        "data": "Rejected",
                        "orderable": true
                    }
                ]
            });

            $(".search-appointment").on("click",
                function() {
                    table.fnDraw();
                });

            $('#tableAppointment tbody').on('click', '.delete-appointment', function () {
                var id = $(this).data("id");
                confirmMessage("Are you sure want to delete this record?", "warning", function () {
                    $.ajax({
                        "url": "/handlers/HandlerVacancies.ashx?commandName=DeleteAppointment&id=" + id,
                    }).done(function (response) {
                        showNotification("Deleted successfully");
                        table.fnDraw();
                    }).fail(function (err) {
                        showNotification(err, "error");
                    });
                });
            });
        });

        function drawSurveyGraph(exportCsv) {
            //var param = "commandName=GetSurveyDemographicAnswer";
            var report = 0;
            var param = "commandName=GetSurveySatisfactoryAnswer";
            if (exportCsv) {
                param = "commandName=GetReportAnswer";
                report = 1;
            }

            param += "&surveyCode=" + $("#<%= ddSurveyQuestion.ClientID %>").val();
            param += "&companyCode=" + $("#<%= hidCompanyCodes.ClientID %>").val();
            $.ajax({
                url: "../handlers/HandlerDashboard.ashx?" + param,
                async: false,
                beforeSend: function () {

                },
                success: function (queryResult) {

                    var rv = queryResult.split("|");
                    var data = new Array();
                    var dataColor = new Array();
                    var total = 0;

                    if (report > 0) {
                        var summary;
                        for (var i = 0; i < rv.length; i++) {
                            var rvComponents = rv[i].split("^");
                            var dataComponent = '{ "Name": "' + rvComponents[0] + '", "Email": "' + rvComponents[1] + '", "' + $("#<%= ddSurveyQuestion.ClientID %> :selected").text() + '": "' + rvComponents[2] + '" , "Note": "' + rvComponents[3] + '" , "PositionName": "' + rvComponents[5]+'"}';
                            data.push(JSON.parse(dataComponent));

                            summary = rvComponents[4];
                        }

                        JSONToCSVConvertor2(data, $("#<%= ddSurveyQuestion.ClientID %> :selected").text(), true, summary);
                    }
                    else {

                        if (rv != null) {
                            var sangatTidakMemuaskan = rv[0].split("^");
                            var tidakMemuaskan = rv[1].split("^");
                            var netral = rv[2].split("^");
                            var memuaskan = rv[3].split("^");
                            var sangatMemuaskan = rv[4].split("^");

                            if (sangatMemuaskan.length > 0) {
                                data.push([sangatMemuaskan[1], "Sangat Puas"]);
                                total = sangatMemuaskan[2];
                            }

                            if (memuaskan.length > 0) {
                                data.push([memuaskan[1], "Puas"]);
                            }

                            if (netral.length > 0) {
                                data.push([netral[1], "Netral"]);
                            }

                            if (tidakMemuaskan.length > 0) {
                                data.push([tidakMemuaskan[1], "Tidak Puas"]);
                            }

                            if (sangatTidakMemuaskan.length > 0) {
                                data.push([sangatTidakMemuaskan[1], "Sangat Tidak Puas"]);
                            }
                        }

                        if (data != null && data.length > 0) {
                            $.plot($("#surveychart"), [
                                {
                                    data: data,
                                    bars: {
                                        horizontal: true,
                                        show: true,
                                        align: "center",
                                        barWidth: 0.7
                                    },
                                    color: "#4F82BE"
                                }],
                                {
                                    yaxis: {
                                        mode: "categories",
                                    }
                                });
                            $("#survey-id tbody").empty();
                            $("#survey-id tbody").append("<tr>"
                                + "<td class='text-center'>Number</td>"
                                + "<td class='text-right'>" + data[0][0] + "</td>"
                                + "<td class='text-right'>" + data[1][0] + "</td>"
                                + "<td class='text-right'>" + data[2][0] + "</td>"
                                + "<td class='text-right'>" + data[3][0] + "</td>"
                                + "<td class='text-right'>" + data[4][0] + "</td>"
                                + "</tr>"
                                + "<tr>"
                                + "<td class='text-center'>%</td>"
                                + "<td class='text-right'>" + getNum((data[0][0] / total) * 100).toFixed(2) + "% </td>"
                                + "<td class='text-right'>" + getNum((data[1][0] / total) * 100).toFixed(2) + "% </td>"
                                + "<td class='text-right'>" + getNum((data[2][0] / total) * 100).toFixed(2) + "% </td>"
                                + "<td class='text-right'>" + getNum((data[3][0] / total) * 100).toFixed(2) + "% </td>"
                                + "<td class='text-right'>" + getNum((data[4][0] / total) * 100).toFixed(2) + "% </td>"
                                + "</tr> "
                            );
                        }

                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                }
            });

        }

        function getNum(val) {
            if (isNaN(val)) {
                return 0;
            }
            return val;
        }

        function drawPieChart(exportCsv) {

            var param = "commandName=" + $("#<%= ddDemographicFilter.ClientID %>").val();
            param += "&status=" + $("#<%= ddJobStatusFilter.ClientID %>").val();
            param += "&companyCode=" + $("#<%= hidCompanyCodes.ClientID %>").val();

            $.ajax({
                url: "../handlers/HandlerDashboard.ashx?" + param,
                async: false,
                beforeSend: function () {

                },
                success: function (queryResult) {

                    var rv = queryResult.split("|");
                    var data = new Array();
                    var dataColor = new Array();

                    for (var i = 0; i < rv.length; i++) {
                        var rvComponents = rv[i].split("^");
                        var dataComponent = '{ "label": "' + rvComponents[0] + '", "data": ' + rvComponents[1] + ' }';
                        data.push(JSON.parse(dataComponent));
                    }

                    if (exportCsv) {
                        JSONToCSVConvertor(data, $("#<%= ddDemographicFilter.ClientID %>").val(), true);
                    }
                    else {

                        if ($("#piechart").length) {
                            $.plot($("#piechart"), data,
                                {
                                    series: {
                                        pie: {
                                            show: true
                                        }
                                    },
                                    grid: {
                                        hoverable: true,
                                        clickable: true
                                    },
                                    legend: {
                                        show: false
                                    }
                                });
                        }



                        function pieHover(event, pos, obj) {
                            if (!obj)
                                $("#tooltip").remove();
                            else {

                                $("#tooltip").remove();

                                var key = obj.series.label;
                                for (var i = 0; i < data.length; i++) {
                                    var labelKey = data[i].label;
                                    if (labelKey == key) {
                                        percent = data[i].data;
                                        break;
                                    }
                                }


                                $("<div id='tooltip'></div>").css({
                                    position: "relative",
                                    border: "1px solid #fdd",
                                    padding: "2px",
                                    "background-color": "#fee",
                                    opacity: 0.80
                                }).text(obj.series.label + ' : ' + percent + ' jobs')
                                    .appendTo("#piechart");
                            }
                        }

                        $("#piechart").bind("plothover", pieHover);
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText, "error");
                }
            });

        }

        function drawBarChart(exportCsv) {
            var x_axis = $("#<%= ddBarChartXAxis.ClientID %> :selected").text();
            var y_axis = $("#<%= ddBarChartValueAxis.ClientID %> :selected").text();

            if (exportCsv) {
                if("Position" == x_axis && "Source" == y_axis){
                    var param = "commandName=GetPositionVsSource";
                    param += "&xParams=" + $("#<%= ddBarChartXAxis.ClientID %>").val();
                    param += "&vParams=" + $("#<%= ddBarChartValueAxis.ClientID %>").val();
                    param += "&status=" + $("#<%= ddBarStatusFilter.ClientID %>").val();
                    param += "&companyCode=" + $("#<%= hidCompanyCodes.ClientID %>").val();

                    $.ajax({
                        url: "../handlers/HandlerDashboard.ashx?" + param,
                        async: false,
                        beforeSend: function () {},
                        success: function (queryResult) {
                            var rv = queryResult.split("|");
                            var data = new Array();

                            for (var i = 0; i < rv.length; i++) {
                                var rvComponents = rv[i].split("^");
                                var dataComponent = '{ "Company": "' + rvComponents[0] + '", "Job_Categories": "' + rvComponents[1] + '", "Positions": "' + rvComponents[2] + '" , "Sources": "' + rvComponents[3] + '","Total_Applicants":' + rvComponents[4]+'}';
                                data.push(JSON.parse(dataComponent));
                            }
                            JSONToCSVConvertor(data, "Position VS Source", true);
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            showNotification(xhr.responseText, "error");
                        }
                    });

                    return;
                }
                
            }

            var param = "commandName=GetVacancyBarChart";
            param += "&xParams=" + $("#<%= ddBarChartXAxis.ClientID %>").val();
            param += "&vParams=" + $("#<%= ddBarChartValueAxis.ClientID %>").val();
            param += "&status=" + $("#<%= ddBarStatusFilter.ClientID %>").val();
            param += "&fromGPA=" + $("#<%= fromGPA.ClientID %>").val();
            param += "&toGPA=" + $("#<%= toGPA.ClientID %>").val();
            param += "&companyCode=" + $("#<%= hidCompanyCodes.ClientID %>").val();

            if ("Position" == $("#<%= ddBarChartXAxis.ClientID %> :selected").text() && "University" == $("#<%= ddBarChartValueAxis.ClientID %> :selected").text()) {
                $("#gpa-from").show();
                $("#gpa-to").show();
            } else {
                $("#gpa-from").hide();
                $("#gpa-to").hide();
            }

            $.ajax({
                url: "../handlers/HandlerDashboard.ashx?" + param,
                async: false,
                beforeSend: function () {

                },
                success: function (queryResult) {

                    var data = new Array();
                    var tickSeries = new Array();
                    var dataComponents = queryResult.split("+");
                    var seriesList = dataComponents[1].split("~");

                    for (var i = 0; i < seriesList.length; i++) {
                        var seriesSet = seriesList[i].split("_");
                        var label = seriesSet[0];
                        var rvComponents = seriesSet[1];
                        if (exportCsv) {

                            var dataSeries = seriesSet[1].split("],[");
                            var xlabelList = dataComponents[0].split(",");
                            for (var z = 0; z < dataSeries.length; z++) {
                                var dataPair = dataSeries[z].replace("[", "").replace("]", "").split(",");
                                var xAxis = xlabelList[dataPair[0]];
                                var value = dataPair[1];
                                var dataComponent = '{ "label-axis": "' + label + '", "x-axis": "' + xAxis + '", "data": "' + value + '" }';
                                data.push(JSON.parse(dataComponent));
                            }

                        }
                        else {
                            var dataComponent = '{ "label": "' + label + '", "data": [' + seriesSet[1] + '] }';
                            data.push(JSON.parse(dataComponent));
                        }
                    }

                    var xlabelList = dataComponents[0].split(",");
                    for (var i = 0; i < xlabelList.length; i++) {
                        tickSeries.push([i, xlabelList[i]]);
                    }

                    if (exportCsv) {
                        JSONToCSVConvertor(data, "bar data", true);
                    }
                    else {

                        $.plot($("#barChart"), data, {
                            xaxis: {
                                mode: "categories",
                                ticks: tickSeries,
                            },
                            series: {
                                bars: {
                                    show: true,
                                    barWidth: 0.3,
                                    align: "center",
                                    order: 2
                                }
                            }
                        });
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText, "error");
                }
            });
        }




        function JSONToCSVConvertor(JSONData, ReportTitle, ShowLabel) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + arrData[i][index] + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += row + '\r\n';
            }

            if (CSV == '') {
                showNotification("Invalid data", "error");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        function JSONToCSVConvertor2(JSONData, ReportTitle, ShowLabel, Summary) {
            //If JSONData is not an object then JSON.parse will parse the JSON string in an Object
            var arrData = typeof JSONData != 'object' ? JSON.parse(JSONData) : JSONData;
            var CSV = '';
            //Set Report title in first row or line

            CSV += ReportTitle + '\r\n\n';

            //This condition will generate the Label/Header
            if (ShowLabel) {
                var row = "";

                //This loop will extract the label from 1st index of on array
                for (var index in arrData[0]) {

                    //Now convert each value to string and comma-seprated
                    row += index + ',';
                }

                row = row.slice(0, -1);

                //append Label row with line break
                CSV += row + '\r\n';
            }

            //1st loop is to extract each row
            for (var i = 0; i < arrData.length; i++) {
                var row = "";

                //2nd loop will extract each column and convert it in string comma-seprated
                for (var index in arrData[i]) {
                    row += '"' + toString(arrData[i][index]) + '",';
                }

                row.slice(0, row.length - 1);

                //add a line break after each row
                CSV += toString(row) + '\r\n';
            }

            if (Summary != null) {
                var tmp = Summary.split("#");
                if (tmp != null && tmp.length > 0) {
                    CSV += '\r\n';
                    CSV += 'Keterangan,Sangat Puas, Puas, Netral, Tidak Puas, Sangat Tidak Puas, Total';
                    CSV += '\r\n';
                    var lbl = "Number,";
                    var count = 0;
                    var value = new Array();
                    for (var i = 1; i < tmp.length; i++) {
                        var dt = tmp[tmp.length - i].split(",");
                        lbl += dt[1] + ",";
                        count = count + parseInt(dt[1]);
                        value.push(parseInt(dt[1]));
                    }
                    lbl += count;
                    CSV += lbl;
                    CSV += '\r\n';
                    lbl = "%,";
                    if (value != null && value.length > 0) {
                        var perc = 0;
                        for (var i = 0; i < value.length; i++) {
                            perc = perc + ((value[i] / count) * 100);
                            lbl += ((value[i] / count) * 100) + " %,";
                        }

                        lbl += perc + " %";
                        CSV += lbl;
                        CSV += '\r\n';
                    }

                }
            } else {
                CSV += '\r\n';
                CSV += 'Keterangan,Sangat Puas, Puas, Netral, Tidak Puas, Sangat Tidak Puas, Total';
                CSV += '\r\n';
                CSV += 'Number,0,0,0,0,0,0';
                CSV += '\r\n';
                CSV += '%,0 %,0 %,0 %,0 %,0 %,0 %';
            }

            if (CSV == '') {
                showNotification("Invalid data", "error");
                return;
            }

            //Generate a file name
            var fileName = "MyReport_";
            //this will remove the blank-spaces from the title and replace it with an underscore
            fileName += ReportTitle.replace(/ /g, "_");

            //Initialize file format you want csv or xls
            var uri = 'data:text/csv;charset=utf-8,' + escape(CSV);

            // Now the little tricky part.
            // you can use either>> window.open(uri);
            // but this will not work in some browsers
            // or you will not get the correct file extension    

            //this trick will generate a temp <a /> tag
            var link = document.createElement("a");
            link.href = uri;

            //set the visibility hidden so it will not effect on your web-layout
            link.style = "visibility:hidden";
            link.download = fileName + ".csv";

            //this part will append the anchor tag and remove it after automatic click
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }

        function toString(variable){
            if (variable === null || variable === "undefined") {
                return "";
            } else {
                return variable;
            }
        }

    </script>
    <asp:HiddenField ID="hidCurrUserCode" runat="server" />
    <asp:HiddenField ID="hidCompanyCode" runat="server" />
    <asp:HiddenField ID="hidCompanyCodes" runat="server" />
<div>
    <div>
        <div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:DropDownList ID="ddDemographicFilter" runat="server" CssClass="form-control" onchange="drawPieChart();">
                        <asp:ListItem Value="GetActiveJobDemographicsByCompany" Text="Company" />
                        <asp:ListItem Value="GetActiveJobDemographicsByJobCode" Text="Job Code" />
                        <asp:ListItem Value="GetActiveJobDemographicsByRecruitmentTargetGroup" Text="Recruitment Target Group" />
                        <asp:ListItem Value="GetActiveJobDemographicsByPositionType" Text="New Position/Replacement" />
                        <asp:ListItem Value="GetActiveJobDemographicsByCountry" Text="Country" />                        
                        <asp:ListItem Value="GetActiveJobDemographicsByEducationLevel" Text="Education Level" />
                        <asp:ListItem Value="GetActiveJobDemographicsByEmploymentType" Text="Type of Employment" />
                        <asp:ListItem Value="GetActiveJobDemographicsByCategory" Text="Industry" />                        
                        <asp:ListItem Value="GetActiveJobDemographicsByDepartment" Text="Function/Department" />
                        <asp:ListItem Value="GetActiveJobDemographicsByDivision" Text="Division" />                        
                        <asp:ListItem Value="GetJobPerYear" Text="Year" />
                        <asp:ListItem Value="GetJobPerMonth" Text="Month" />
                        <asp:ListItem Value="GetActiveJobDemographicsByCandidateSource" Text="Candidate Source" />                        
                    </asp:DropDownList>
                </div>
                <div class="col-sm-1">
                    X-Axis
                </div>
                <div class="col-sm-2">
          
                    <asp:DropDownList ID="ddBarChartXAxis" runat="server" CssClass="form-control" onchange="drawBarChart();"> 
                        <asp:ListItem Value="GetDivisionList_Code_Name_Division" Text="Division" />         
                        <asp:ListItem Value="GetPositionReplacementList_Key_Value_NewPosition" Text="New Position/Replacement" />   
                        <asp:ListItem Value="GetCompanyList_Code_Name_CompanyCode" Text="Company" />
                        <asp:ListItem Value="GetJobsList_JobCode_Name_JobCode" Text="Job Code" />
                        <asp:ListItem Value="GetRecruitmentTargetGroupList_Key_Value_RecruitmentTargetGroup" Text="Recruitment Target Group" />
                        <asp:ListItem Value="GetCountryList_Code_Name_Country" Text="Country" />                        
                        <asp:ListItem Value="GetEducationLevelList_EducationLevel_Name_RequiredEducationLevel" Text="Education Level" />
                        <asp:ListItem Value="GetEmploymentStatusType_Code_Name_TypeOfEmployment" Text="Type of Employment" />
                        <asp:ListItem Value="GetIndustryList_Code_Name_TypeOfEmployment" Text="Industry" />                        
                        <asp:ListItem Value="GetDepartmentList_Code_Name_Department" Text="Function/Department" />               
                        <asp:ListItem Value="GetYearList_Year_YearString_Year" Text="Year" />
                        <asp:ListItem Value="GetMonthList_Id_Name_Month" Text="Month" />
                        <asp:ListItem Value="GetUniversityList_Name_Name_InstitutionName" Text="University" />
                        <asp:ListItem Value="GetPositionList_PositionCode_PositionName_PositionCode" Text="Position" />
                    </asp:DropDownList>
                    <div id="gpa-from" style="display:none">
                        GPA-From
                        <asp:DropDownList ID="fromGPA" runat="server" CssClass="form-control" onchange="drawBarChart();">
                        <asp:ListItem Value="1" Text="1" />
                        <asp:ListItem Value="1.5" Text="1.5" />
                        <asp:ListItem Value="2" Text="2" />
                        <asp:ListItem Value="2.5" Text="2.5" />
                        <asp:ListItem Value="3" Text="3" /> 
                        <asp:ListItem Value="3.5" Text="3.5" /> 
                        <asp:ListItem Value="4" Text="4" /> 
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-sm-1">
                    Data-Axis
                </div>
                <div class="col-sm-2">
                    
                    <asp:DropDownList ID="ddBarChartValueAxis" runat="server" CssClass="form-control" onchange="drawBarChart();">
                        <asp:ListItem Value="GetPositionReplacementList_Key_Value_NewPosition" Text="New Position/Replacement" />
                        <asp:ListItem Value="GetCompanyList_Code_Name_CompanyCode" Text="Company" />
                        <asp:ListItem Value="GetJobsList_JobCode_Name_JobCode" Text="Job Code" />
                        <asp:ListItem Value="GetRecruitmentTargetGroupList_Key_Value_RecruitmentTargetGroup" Text="Recruitment Target Group" />
                        <asp:ListItem Value="GetCountryList_Code_Name_Country" Text="Country" />                        
                        <asp:ListItem Value="GetEducationLevelList_EducationLevel_Name_RequiredEducationLevel" Text="Education Level" />
                        <asp:ListItem Value="GetEmploymentStatusType_Code_Name_TypeOfEmployment" Text="Type of Employment" />
                        <asp:ListItem Value="GetIndustryList_Code_Name_TypeOfEmployment" Text="Industry" />                        
                        <asp:ListItem Value="GetDepartmentList_Code_Name_Department" Text="Function/Department" />
                        <asp:ListItem Value="GetDivisionList_Code_Name_Division" Text="Division" />                
                        <asp:ListItem Value="GetYearList_Year_YearString_Year" Text="Year" />
                        <asp:ListItem Value="GetMonthList_Id_Name_Month" Text="Month" />
                        <asp:ListItem Value="GetVacanciesCategoryList_Code_Name_CategoryCode" Text="Job Category" />
                        <asp:ListItem Value="GetSourceList_Name_Name_Name" Text="Source" />
                        <asp:ListItem Value="GetUniversityList_Name_Name_InstitutionName" Text="University" />
                    </asp:DropDownList>
                    <div id="gpa-to" style="display:none">
                     GPA-To
                        <asp:DropDownList ID="toGPA" runat="server" CssClass="form-control" onchange="drawBarChart();">
                        <asp:ListItem Value="1" Text="1" />
                        <asp:ListItem Value="1.5" Text="1.5" />
                        <asp:ListItem Value="2" Text="2" />
                        <asp:ListItem Value="2.5" Text="2.5" />
                        <asp:ListItem Value="3" Text="3" /> 
                        <asp:ListItem Value="3.5" Text="3.5" /> 
                        <asp:ListItem Value="4" Text="4" />    
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <asp:DropDownList ID="ddJobStatusFilter" runat="server" CssClass="form-control" onchange="drawPieChart();">
                        <asp:ListItem Value="" Text="All" />                        
                        <asp:ListItem Value="Draft" Text="Pending Jobs" />
                        <asp:ListItem Value="Open" Text="Open Jobs" />
                        <asp:ListItem Value="Closed" Text="Closed Jobs" />
                    </asp:DropDownList>
                </div>
                <div class="col-sm-6">                    
                    <asp:DropDownList ID="ddBarStatusFilter" runat="server" CssClass="form-control" onchange="drawBarChart();">
                        <asp:ListItem Value="" Text="All" />                        
                        <asp:ListItem Value="Draft" Text="Pending Jobs" />
                        <asp:ListItem Value="Open" Text="Open Jobs" />
                        <asp:ListItem Value="Closed" Text="Closed Jobs" />
                    </asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <button id="btnDownloadPieData" class="btn btn-primary" onclick="drawPieChart(true); return false;" runat="server">
                        Download Data
                    </button>
                </div>
                <div class="col-sm-6">
                    <button id="btnDownloadBarData" class="btn btn-primary" onclick="drawBarChart(true); return false;" runat="server">
                        Download Data
                    </button>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6" id="piePlaceHolder">
                    
                    <div id="piechart" style="height:300px"></div>
                </div>
                <div class="col-sm-6">
                    <div id="barChart" class="center" style="height:300px;"></div>
                </div>
            </div>
        </div>
    </div>
</div>

  <div>
    <div>   
      <div class="row">
          <div class="col-sm-7">
              <h3><span data-lang="AppointmentTitle"><label id="lbDashboard_AppointmentList" runat="server">Appointment List</label></span></h3>            
           </div>  
           <div class="col-sm-5" id="dvApproval" runat="server">
            <h3><span data-lang="SurveyTitle">Survey Results</span></h3>    
           </div>
      </div>   
      <div class="row">
          <div class="col-sm-7 table-responsive" >
              <div class="row">
                  <div class="col-sm-12" style="padding-left: 0; padding-right: 0">
                      <div class="col-sm-2">
                          Date From
                      </div>
                      <div class="col-sm-4">
                          <input type="text" id="txtDateFrom" runat="server" class="form-control date-picker date-from"/>
                      </div>
                      <div class="col-sm-2">
                          Date To
                      </div>
                      <div class="col-sm-4">
                          <input type="text" id="txtDateTo" runat="server" class="form-control date-picker date-to"/>
                      </div>
                  </div>
              </div>
              
              <div class="row">
                  <div class="col-sm-12" style="padding-left: 0; padding-right: 0">
                      <div class="col-sm-2">
                          Filter
                      </div>
                      <div class="col-sm-4">
                          <input type="text" id="txtFilter" runat="server" class="form-control filter"/>
                      </div>
                      <div class="col-sm-6" align="right">
                          <button type="button" class="btn btn-primary search-appointment">
                              Search
                          </button>
                      </div>
                  </div>
              </div>
              
              <div class="row">
                  <div class="col-sm-12" style="padding-left: 0; padding-right: 0">
                      <table class="table table-striped table-bordered table-condensed" id="tableAppointment">
                          <thead>
                              <tr>
                                  <th><span data-lang="TableTypeTitle"><label id="lbDashboard_AppointmentType" runat="server">Steps</label></span></th>
                                  <th><span data-lang="TableJobReqTitle"><label id="lbDashboard_AppointmentJobReqName" runat="server">Job Req Name</label></span></th>
                                  <th><span data-lang="TableDateTitle"><label id="lbDashboard_AppointmentDate" runat="server">Date</label></span>  </th>
                                  <th><span data-lang="TablePlaceTitle"><label id="lbDashboard_AppointmentPlace" runat="server">Place</label></span></th>
                                  <th><span data-lang="TableAcceptedTitle"><label id="lbDashboard_AppointmentAccepted" runat="server">Accepted</label></span></th>
                                  <th><span data-lang="TableRescheduleTitle"><label id="lbDashboard_AppointmentReschedule" runat="server">Reschedule</label></span></th>
                                  <th><span data-lang="TableRejectedStatus"><label id="lbDashboard_AppointmentRejected" runat="server">Rejected</label></span></th>
                              </tr>
                          </thead>
                      </table>
                  </div>
              </div>
              
              <div class="row">
                  <div class="col-sm-12" style="padding: 0" align="right">
                      <button id="btnDownloadAppointment" class="btn btn-primary" runat="server" onserverclick="btnDownloadAppointment_ServerClick">
                          Download Data
                      </button>
                  </div>
              </div>
          </div>
          <div class="col-sm-5">
              <div class="col-sm-12">
                  <asp:DropDownList ID="ddSurveyQuestion" runat="server" CssClass="form-control" DataValueField="Code" DataTextField="Name" onchange="drawSurveyGraph();">
                    
                  </asp:DropDownList>
              </div>
              <div class="col-sm-12" style="padding-top: 10px">
                  <button id="btnDownloadSurveyData" class="btn btn-primary" onclick="drawSurveyGraph(true); return false;" runat="server">
                      Download Data
                  </button>
              </div>
              <div class="col-sm-12" style="padding-top: 10px">
                  <div id="surveychart" style="height:300px"></div>
                  <br>
                  <table id="survey-id" name="survay-id" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                            <th class="text-center">Keterang</th>
                            <th class="text-center">Sangat Puas</th>
                            <th class="text-center">Puas</th>
                            <th class="text-center">Netral</th>
                            <th class="text-center">Tidak Puas</th>
                            <th class="text-center">Sangat Tidak Puas</th>
                        </tr>
                        <tr>
                            <th class="text-center"></th>
                            <th class="text-center"><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i></th>
                            <th class="text-center"><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i></th>
                            <th class="text-center"><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i></th>
                            <th class="text-center"><i class='fa fa-star' style="color:yellow;font-size:12px;"></i><i class='fa fa-star' style="color:yellow;font-size:12px;"></i></th>
                            <th class="text-center"><i class='fa fa-star' style="color:yellow;font-size:12px;"></i></th>
                        </tr>
                     </thead>
                      <tbody>
                        <tr>
                            <td colspan="6">No data available in table</td>
                         </tr>
                      </tbody>
                  </table>
              </div>
          </div>
      </div>
  </div>
 </div>
</asp:Content>
