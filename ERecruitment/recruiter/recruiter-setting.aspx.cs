﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class recruiter_setting : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                initiateSetting();
        }

        private void initiateSetting()
        {
            ApplicationSettings recruiterSetting = ERecruitmentManager.GetApplicationSetting();
            if (recruiterSetting != null)
            {
                Utils.SelectItem<string>(ddJobAgeCalculation, recruiterSetting.JobAgeCalculationMethod);
                Utils.SelectItem<string>(ddApplicationMode, recruiterSetting.VacancyApplicationMode);            
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            ApplicationSettings recruiterSetting = ERecruitmentManager.GetApplicationSetting();
            if (recruiterSetting != null)
            {
                recruiterSetting.JobAgeCalculationMethod = Utils.GetSelectedValue<string>(ddJobAgeCalculation);
                recruiterSetting.VacancyApplicationMode = Utils.GetSelectedValue<string>(ddApplicationMode);
                ERecruitmentManager.UpdateApplicationSetting(recruiterSetting);
                JQueryHelper.InvokeJavascript("showNotification('Successfully updated');", Page);
                initiateSetting();
            }
            else
            {
                recruiterSetting = new ApplicationSettings();
                recruiterSetting.JobAgeCalculationMethod = Utils.GetSelectedValue<string>(ddJobAgeCalculation);
                recruiterSetting.VacancyApplicationMode = Utils.GetSelectedValue<string>(ddApplicationMode);
                
                ERecruitmentManager.SaveApplicationSetting(recruiterSetting);
                JQueryHelper.InvokeJavascript("showNotification('Successfully saved');", Page);
                initiateSetting();
            }
        }
    }
}