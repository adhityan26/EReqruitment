﻿<%@ Page Language="C#" MasterPageFile="~/masters/popupform.Master" AutoEventWireup="true" CodeBehind="update-status-applicant.aspx.cs" Inherits="ERecruitment.update_status_applicant" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        function GetQueryStringParams(sParam) {
            var url = window.location.href;
            var key = url.split('?')[1];
            var sURLVariables = key.split('&');

            for (var i = 0; i < sURLVariables.length; i++) {
                var sParameterName = sURLVariables[i].split('=');
                if (sParameterName[0] == sParam) {
                    return sParameterName[1];
                }
            }
            return "";
        }
        function updatePipeline() {
            var updateStatus = $("#<%= ddStatus.ClientID %>").val();
            var issuedBy = $("#<%= hidCurrentUser.ClientID %>").val();

            var codeList = GetQueryStringParams("appCodes");
            var vacancyCode = GetQueryStringParams("vacancyCode");
            var currentPipeline = GetQueryStringParams("status");
            var tableData = $('#tableData').dataTable();

            $.ajax({
                url: "../handlers/HandlerProcessHiring.ashx?commandName=UpdateApplicantPipeline&appCodes=" + codeList + "&vacancyCode=" + vacancyCode + "&statusCode=" + updateStatus,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    showNotification("successfully updated");
                    window.opener.location.href = window.opener.location.href;
                    window.close();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                }
            });
        }
    </script>
    <asp:HiddenField ID="hidCurrentUser" runat="server" />
     <div class="row">
          <div class="col-sm-12">
            
              <div class="row">
                    <div class="col-sm-1">
                        <label>Update to</label>
                    </div>
                    <div class="col-sm-3">
                         <select id="ddStatus" 
                                runat="server" class="form-control" >
                            
                         </select>
                    </div>  
              </div>
           
             <div class="row">
                 <div class="col-sm-1">
                
                    </div>
              <div class="col-sm-3">
                   <button class="btn btn-primary"  runat="server" id="btnSave" onclick="updatePipeline();return false;" >
                         Submit
                    </button>
               </div>
              </div>
             </div>
           </div>
<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                       SelectMethod="GetVacancyCategoryList"
    />
    
 </asp:Content>
