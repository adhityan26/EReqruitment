﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class received_application_list: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                LoadData(ddCategory,ddStatus);
        }
        
        #region support
        private void LoadData(DropDownList ddCategory, DropDownList ddStatus)
        {
            string vacancyCategory = ddCategory.SelectedValue;
            string status = ddStatus.SelectedValue;
            repApplicantVacancy.DataSource = ERecruitmentManager.GetReceivedApplicantList(vacancyCategory,status);
            repApplicantVacancy.DataBind();
        }

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            Utils.InsertInitialListItem(dd);
        }

        #endregion

        #region repeater
        protected void repApplicantVacancy_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {

                ApplicantVacancies application = e.Item.DataItem as ApplicantVacancies;

                Literal ltJobTitle = e.Item.FindControl("ltJobTitle") as Literal;
                Literal ltHiringManager = e.Item.FindControl("ltHiringManager") as Literal;
                Literal ltRecruiter = e.Item.FindControl("ltRecruiter") as Literal;
                Literal ltDueDate = e.Item.FindControl("ltDueDate") as Literal;
                Literal ltApplicantName = e.Item.FindControl("ltApplicantName") as Literal;

                //ltJobTitle.Text = application.Vacancy.Subject;
                //ltHiringManager.Text = application.Vacancy.HiringManager;
                //ltRecruiter.Text = application.Vacancy.Recruiter;
                // ltDueDate.Text = Utils.DisplayDateTime(application.Vacancy.ExpirationDate);
                ltDueDate.Text = string.Empty;
                Applicants getApplicant = ApplicantManager.GetApplicant(application.ApplicantCode);
                if (getApplicant != null)
                    ltApplicantName.Text = getApplicant.FirstName;
            }
        }
        #endregion

        protected void ddCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData(ddCategory, ddStatus);
        }

        protected void ddStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            LoadData(ddCategory, ddStatus);
        }
    }
}