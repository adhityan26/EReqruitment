﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using FastReport.Web;
using FastReport;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Configuration;
using System.IO;

namespace ERecruitment
{
    public partial class dashboard: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            hidCurrUserCode.Value = CurrentUser.Code;
            if (!string.IsNullOrEmpty(MainCompany.SurveyQuestionCodes))
            {
                ddSurveyQuestion.DataSource = SurveyQuestionManager.GetSurveyQuestionList(MainCompany.SurveyQuestionCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries));
                ddSurveyQuestion.DataBind();
            } else {
                ddSurveyQuestion.DataSource = SurveyQuestionManager.GetSurveyQuestionList();
                ddSurveyQuestion.DataBind();
            }

            hidCompanyCode.Value = MainCompany.Code;
            hidCompanyCodes.Value = MainCompany.CompanyCodes;

        }

        protected void btnDownloadAppointment_ServerClick(object sender, EventArgs e)
        {
            string reportFileName = "Appointment List";
            Report report = new Report();
            report.Load(ConfigurationManager.AppSettings["ReportPath"] + "applicantschedule.frx");
            
            report.SetParameterValue("CompanyCode", MainCompany.Code);
            report.SetParameterValue("Filter", "%" + txtFilter.Value + "%");
            report.SetParameterValue("DateFrom", DateTime.ParseExact((String.IsNullOrEmpty(txtDateFrom.Value) ? "01/01/1900" : txtDateFrom.Value) + " 00:00:00", "dd/MM/yyyy HH:mm:ss",
                System.Globalization.CultureInfo.InvariantCulture));
            report.SetParameterValue("DateTo", DateTime.ParseExact((String.IsNullOrEmpty(txtDateTo.Value) ? "31/12/2099" : txtDateTo.Value) + " 23:59:59", "dd/MM/yyyy HH:mm:ss",
                System.Globalization.CultureInfo.InvariantCulture));
            report.Dictionary.Connections[0].ConnectionString = ConfigurationManager.ConnectionStrings["ERecruitmentConnectionString"].ConnectionString;
            report.Dictionary.Connections[0].CommandTimeout = 3600;
            report.Prepare();

            FastReport.Export.OoXML.Excel2007Export xlsExport = new FastReport.Export.OoXML.Excel2007Export();

            using (MemoryStream strm = new MemoryStream())
            {
                report.Export(xlsExport, strm);

                // Stream the PDF back to the client as an attachment
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("Content-Disposition", "attachment;filename=\"" + reportFileName + "_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xlsx\"");

                strm.Position = 0;
                strm.WriteTo(Response.OutputStream);
                Response.End();
            }
        }
    }
}