﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;
using System.Web.UI.HtmlControls;


namespace ERecruitment
{
    public partial class detail_attribute_applicant: AuthorizedPage
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region custom themes config

                HtmlLink linkTheme = this.FindControl("linkTheme") as HtmlLink;
                if (linkTheme != null)
                {
                    linkTheme.Href = MainCompany.ATSBaseUrl + "assets/themes/" + MainCompany.CssTheme;

                }

                #endregion

                string applicantCode = Utils.GetQueryString<string>("code");
                string vacancyCode = Utils.GetQueryString<string>("vacancyCode");
                hidVacancyCode.Value = vacancyCode;
                string status = Utils.GetQueryString<string>("status");

                if (!string.IsNullOrEmpty(applicantCode))
                {
                    ApplicantVacancies getApplicantVacancy = ERecruitmentManager.GetApplicantVacancy(applicantCode, vacancyCode, status);
                  
                    if (getApplicantVacancy != null)
                    {
                        Applicants getApplicant = ApplicantManager.GetApplicant(getApplicantVacancy.ApplicantCode);
                        lbName.InnerText = string.Concat(getApplicant.FirstName, " ", getApplicant.LastName);
                        lbEmail.InnerText = getApplicant.Email;
                        lbStatus.InnerText = getApplicant.MaritalStatusSpecification;
                        lbPhoneNumber.InnerText = getApplicant.Phone;
                        lbGender.InnerText = getApplicant.GenderSpecification;
                        lbAddress.InnerText = getApplicant.Address;

                        if (getApplicant.Photo != null)
                            imgfoto.Src = "~/system/httphandlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + getApplicant.Code;
                        else
                            imgfoto.Src = "~/system/img/photodefault.gif";

                        JQueryHelper.InvokeJavascript("loadEducation(\"" + getApplicant.Code + "\");", Page);
                        JQueryHelper.InvokeJavascript("loadSkill(\"" + getApplicant.Code + "\");", Page);
                        JQueryHelper.InvokeJavascript("loadLanguage(\"" + getApplicant.Code + "\");", Page);
                        JQueryHelper.InvokeJavascript("loadExperience(\"" + getApplicant.Code + "\");", Page);
                        JQueryHelper.InvokeJavascript("loadOrganizationExperience(\"" + getApplicant.Code + "\");", Page);
                        JQueryHelper.InvokeJavascript("loadDisqualifierAnswer(\"" + getApplicant.Code + "\");", Page);
                        JQueryHelper.InvokeJavascript("loadAttachedFiles(\"" + getApplicant.Code + "\");", Page);
                    }
                }
            }
        }
    }
}