﻿<%@ Page Language="C#" MasterPageFile="~/masters/popupform.Master" AutoEventWireup="true" CodeBehind="attachment-form.aspx.cs" Inherits="ERecruitment.attachment_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
          <div class="col-sm-12">
           
              <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbAttachmentForm_Name">Name</label><span class="requiredmark">*</span>
                    </div>
                    <div class="col-sm-5">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbName"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                                        
                    </div>  
              </div>
               

             <div class="row">
                 <div class="col-sm-3">
                
                    </div>
              <div class="col-sm-3">
                   <button class="btn btn-primary"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
                         Save
                    </button>
               </div>
              </div>
             </div>
           </div>
<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                       SelectMethod="GetVacancyCategoryList"
    />
 </asp:Content>