﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="my-profile.aspx.cs" Inherits="ERecruitment.my_profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
<div>
    <div class="row">
          <div class="col-sm-12">
              <div class="row">
                    <div class="col-sm-3">
                        <img id="imgfoto" width="150" height="150" runat="server"  alt="Photo" />
                    </div>
              </div>
                   <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="Label1">Current Password</label>
                    </div>
                    <div class="col-sm-5">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbCurrentPassword"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="password" />
                                        
                    </div> 
              </div>            
              <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbMyProfile_ChangePassword">New Password</label>
                    </div>
                    <div class="col-sm-5">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbPassword"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="password" />
                                        
                    </div> 
              </div>
              <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbMyProfile_ConfirmPassword">Confirm Password</label>
                    </div>
                    <div class="col-sm-5">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbConfirmPassword"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="password" />
                                        
                    </div>  
              </div>
                <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbMyProfile_UploadPhoto">Upload Photo</label>
                    </div>
                    <div class="col-sm-5">
                       <asp:FileUpload ID="fuPhoto" runat="server" />             
                    </div>  
              </div>
               
        <div class="row">
            <div class="col-sm-3">
                
            </div>
            <div class="col-sm-3">
                <button class="btn btn-primary" runat="server" id="btnSubmit" 
                    onserverclick="btnSubmit_ServerClick">Save</button>                
            </div>
        </div>    
          </div>
         </div>
      </div>
</asp:Content>
