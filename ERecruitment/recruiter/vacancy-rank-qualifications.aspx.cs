﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SS.Web.UI;
using ERecruitment.Domain;

namespace ERecruitment
{
    public partial class vacancy_rank_qualifications : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Vacancies vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));

                tbMinimumExperienceYear.Value = vacancy.RankQualificationExperienceMinimumYear.ToString();
                tbMinimumExperienceYearWeight.Value = vacancy.RankQualificationExperienceMinimumYearWeight.ToString();

                ddEducationLevel.DataBind();
                Utils.SelectItem<string>(ddEducationLevel, vacancy.RankQualificationMinimumEducationLevelCode);
                tbEducationLevelWeight.Value = vacancy.RankQualificationMinimumEducationLevelCodeWeight.ToString();

                ddFieldOfStudy.DataBind();
                Utils.SelectItem<string>(ddFieldOfStudy, vacancy.RankQualificationEducationFieldCode);
                tbFieldOfStudyWeight.Value = vacancy.RankQualificationEducationFieldCodeWeight.ToString();

                tbGpaStart.Value = vacancy.RankQualificationEducationGpaRangeBottom.ToString();
                tbGpaEnd.Value = vacancy.RankQualificationEducationGpaRangeTop.ToString();
                tbGpaWeight.Value = vacancy.RankQualificationEducationGpaRangeWeight.ToString();

                ddLocation.DataBind();
                Utils.SelectItem<string>(ddLocation, vacancy.RankQualificationEducationLocationCode);
                tbLocationWeight.Value = vacancy.RankQualificationEducationLocationCodeWeight.ToString();
                
                ddResidence.DataBind();
                Utils.SelectItem<string>(ddResidence, vacancy.RankQualificationResidenceLocationCode);
                tbResidenceWeight.Value = vacancy.RankQualificationResidenceLocationCodeWeight.ToString();

                tbSalaryStart.Value = vacancy.RankQualificationSalaryRangeBottom.ToString();
                tbSalaryEnd.Value = vacancy.RankQualificationSalaryRangeTop.ToString();
                tbSalaryWeight.Value = vacancy.RankQualificationSalaryRangeWeight.ToString();

                tbAgeStart.Value = vacancy.RankQualificationAgeRangeBottom.ToString();
                tbAgeEnd.Value = vacancy.RankQualificationAgeRangeTop.ToString();
                tbAgeWeight.Value = vacancy.RankQualificationAgeRangeWeight.ToString();
            }
        }

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dl = sender as DropDownList;
            if (dl != null)
                dl.Items.Insert(0, new ListItem("Choose an item", ""));
        }

        protected void btnUpdate_ServerClick(object sender, EventArgs e)
        {
            Vacancies vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));

            vacancy.RankQualificationExperienceMinimumYear = Utils.ConvertString<int>(tbMinimumExperienceYear.Value);
            vacancy.RankQualificationExperienceMinimumYearWeight = Utils.ConvertString<int>(tbMinimumExperienceYearWeight.Value);

            vacancy.RankQualificationEducationFieldCode = Utils.GetSelectedValue<string>(ddFieldOfStudy);
            vacancy.RankQualificationEducationFieldCodeWeight = Utils.ConvertString<int>(tbFieldOfStudyWeight.Value);

            vacancy.RankQualificationMinimumEducationLevelCode = Utils.GetSelectedValue<string>(ddEducationLevel);
            vacancy.RankQualificationMinimumEducationLevelCodeWeight = Utils.ConvertString<int>(tbEducationLevelWeight.Value);

            vacancy.RankQualificationEducationGpaRangeBottom = Utils.ConvertString<int>(tbGpaStart.Value);
            vacancy.RankQualificationEducationGpaRangeTop = Utils.ConvertString<int>(tbGpaEnd.Value);
            vacancy.RankQualificationEducationGpaRangeWeight = Utils.ConvertString<int>(tbGpaWeight.Value);

            vacancy.RankQualificationEducationLocationCode = Utils.GetSelectedValue<string>(ddLocation);
            vacancy.RankQualificationEducationLocationCodeWeight = Utils.ConvertString<int>(tbLocationWeight.Value);

            vacancy.RankQualificationResidenceLocationCode = Utils.GetSelectedValue<string>(ddResidence);
            vacancy.RankQualificationResidenceLocationCodeWeight = Utils.ConvertString<int>(tbResidenceWeight.Value);

            vacancy.RankQualificationSalaryRangeBottom = Utils.ConvertString<int>(tbSalaryStart.Value);
            vacancy.RankQualificationSalaryRangeTop = Utils.ConvertString<int>(tbSalaryEnd.Value);
            vacancy.RankQualificationSalaryRangeWeight = Utils.ConvertString<int>(tbSalaryWeight.Value);

            vacancy.RankQualificationAgeRangeBottom = Utils.ConvertString<int>(tbAgeStart.Value);
            vacancy.RankQualificationAgeRangeTop = Utils.ConvertString<int>(tbAgeEnd.Value);
            vacancy.RankQualificationAgeRangeWeight = Utils.ConvertString<int>(tbAgeWeight.Value);

            VacancyManager.UpdateVacancyRank(vacancy);

            JQueryHelper.InvokeJavascript("showNotification('Rank updated');", Page);
        }
    }
}