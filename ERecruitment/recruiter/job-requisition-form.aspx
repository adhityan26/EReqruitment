﻿<%@ Page Title="Job Requisition Form" EnableEventValidation="False" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="job-requisition-form.aspx.cs" Inherits="ERecruitment.job_requisition_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script type="text/javascript">
    $(document).ready(function() {
        toggleIDCardProvinceAndCity();
        if (GetQueryStringParams("vacancyCode") == "") {
            loadJobPositions();
        }
    });
    
    function validate(e) {
        if ($("form [required]").filter(function() { return this.value == "" }).length > 0) {
            return true;
        }
        
        if ($(e).data("submit") == false) {
            var message = "";
            if ($("#<%= hidPrevPipeline.ClientID %>").val() == "") {
                message = "Are you sure want to save this data? Once a pipeline is stored it can not be changed";
            } else if ($("#<%= hidPrevPipeline.ClientID %>").val() != $("#<%= ddPipeline.ClientID %>").val()) {
                message = "Are you sure want to save this data? There will be a reset due to pipeline changes";
            } else {
                message = "Are you sure want to save this data?";
            }
            
            confirmMessage(message, "warning", function() {
                $(e).data("submit", true);
                $("#<%= btnSubmit.ClientID %>").click();
            });
            return false;
        }
        
        return true;
    }

    function toggleChild(parentCode) {
        $("." + parentCode).toggle();
        $("#parent" + parentCode).toggleClass("fa-plus");
        $("#parent" + parentCode).toggleClass("fa-minus");
    }

    function checkParentQuestion(parentCode) {

        var checked = $(".cbParent" + parentCode).prop("checked");
        $(".cb" + parentCode).prop("checked", checked);

    }


    function loadJobCategory() {

        var company = $("#<%= ddCompany.ClientID %>").val();

        $.ajax({
            url: "../handlers/HandlerSettings.ashx?commandName=GetCompanyIndustry&code=" + company,
            async: true,
            beforeSend: function() {

            },
            success: function(queryResult) {

                $("#<%= ddCategory.ClientID %>").val(queryResult);
            }
        });
    }


    function loadJobPositions() {

        var company = $("#<%= ddCompany.ClientID %>").val();
        var ddTarget = "<%= ddJobPosition.ClientID %>";

        $.ajax({
            url: "../handlers/HandlerGlobalSettings.ashx?commandName=GetPositionByCompanyList&companyCode=" + company,
            async: true,
            beforeSend: function() {
                waitingDialog.show();
            },
            success: function(queryResult) {

                var dataList = queryResult.toString().split("|");
                $("#" + ddTarget).children().remove();

                if (dataList.length > 0) {
                    $("#" + ddTarget).append('<option value="">Select an Option</option>');
                    for (var i = 0; i < dataList.length; i++) {
                        var dataPair = dataList[i].split(",");
                        if (typeof dataPair[1] === "undefined") continue;

                        if (dataPair.length > 0)
                            $("#" + ddTarget)
                                .append('<option value="' + dataPair[0] + '">' + dataPair[1] + '</option>');
                    }
                }
                $("#" + ddTarget).trigger("chosen:updated");
                waitingDialog.hide();
            }
        });
    }

    function loadCity() {

        var provinceCode = $("#<%= ddProvince.ClientID %>").val();
        var ddTarget = "<%= ddCity.ClientID %>";

        $.ajax({
            url: "../handlers/HandlerLocations.ashx?commandName=GetCity&provinceCode=" + provinceCode,
            async: true,
            beforeSend: function() {
                waitingDialog.show();
            },
            success: function(queryResult) {

                var dataList = queryResult.toString().split("|");
                $("#" + ddTarget).children().remove();

                if (dataList.length > 0) {
                    for (var i = 0; i < dataList.length; i++) {
                        var dataPair = dataList[i].split(",");
                        if (typeof dataPair[1] === "undefined") continue;

                        if (dataPair.length > 0)
                            $("#" + ddTarget)
                                .append('<option value="' + dataPair[0] + '">' + dataPair[1] + '</option>');
                    }
                }

                $("#" + ddTarget).trigger("chosen:updated");
                waitingDialog.hide();
            }
        });
    }

    function toggleIDCardProvinceAndCity() {

        var country = $("#<%= ddCountry.ClientID %>").val();

        if (country == "Indonesia") {

            $("#<%= ddProvince.ClientID %>_chosen").show();
            $("#<%= ddCity.ClientID %>_chosen").show();

            $("#<%= tbProvince.ClientID %>").val("");
            $("#<%= tbCity.ClientID %>").val("");

            $("#<%= tbProvince.ClientID %>").hide();
            $("#<%= tbCity.ClientID %>").hide();
        } else {

            $("#<%= ddProvince.ClientID %>_chosen").hide();
            $("#<%= ddCity.ClientID %>_chosen").hide();
            $("#<%= ddProvince.ClientID %>").hide();
            $("#<%= ddCity.ClientID %>").hide();

            $("#<%= tbProvince.ClientID %>").show();
            $("#<%= tbCity.ClientID %>").show();
        }
    }

    function backToList() {
        window.location.href = "job-requisition-list.aspx";
    }

    function SetSelectedText() {
        var selectedText = $("#<%= ddJobPosition.ClientID %> option:selected").text();
        $("#<%= hJobName.ClientID %>").val(selectedText);
    }

    function SetSelectedJobCode(){
        var jobCode = $("#<%= ddJobCode.ClientID %>").val();
        $("#<%= hJobCode.ClientID %>").val(jobCode);
        $.ajax({
            url: "../handlers/HandlerSettings.ashx?commandName=GetJobCode&code=" + jobCode,
            async: true,
            beforeSend: function() {
                $("#<%= tbJobRole.ClientID %>").val("");
                $("#<%= tbJobStage.ClientID %>").val("");
                $("#<%= tbJobRequirements.ClientID %>").val("");
                $("#<%= tbJobDescription.ClientID %>").val("");

            },
            success: function (queryResult) {
                var data = queryResult.split(',');
                $("#<%= tbJobRole.ClientID %>").val(data[0]);
                $("#<%= tbJobStage.ClientID %>").val(data[1]);
                $("#<%= tbJobRequirements.ClientID %>").val(data[2]);
                $("#<%= tbJobDescription.ClientID %>").val(data[5]);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                showNotification(xhr.responseText);

            }
        });
    }

</script>

<asp:HiddenField ID="hidDisqualifiedQuestionCodes" runat="server"/>
<asp:HiddenField ID="hidPrevPipeline" runat="server"/>
<asp:HiddenField ID="hJobName" runat="server"/>
    <asp:HiddenField ID="hJobCode" runat="server"/>
<div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="CompanyInput"> Company </span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddCompany"
                        datavaluefield="Code"
                        datatextfield="Name"
                        onchange="loadJobCategory(); loadJobPositions(); return false;"
                        runat="server"
                        class="form-control"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="JobCodeInput">Job Code</span>
                </strong>
            </div>
            <div class="col-sm-3">
       <%--         <select id="ddJobCode" onchange="SetSelectedJobCode();return false;" 
                        runat="server" class="form-control" />--%>
                       <asp:DropDownList ID="ddJobCode" runat="server" DataTextField="JobCode"
                                  DataValueField="Code" onchange="SetSelectedJobCode();return false"
                                  CssClass="form-control" DataSourceID="odsJobs"
                                  OnDataBound="ddDatabound"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="JobNameInput">Job Position</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <select id="ddJobPosition"
                        datatextfield="Name"
                        datavaluefield="PositionCode"
                        runat="server"
                        required
                        class="form-control"
                        onchange="SetSelectedText();return false"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="ShowCompanyInput">Show Company</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddShowCompany" runat="server" class="form-control">
                    <option>No</option>
                    <option>Yes</option>
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="JobPostingInput">Job Posting Options</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddJobPostingOption" runat="server" class="form-control">
                    <option>Internal</option>
                    <option>Eksternal</option>
                    <option>Internal and Eksternal</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="NumOpInput">Number of Opening</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-1">
                <input id="tbNumberOfOpening"
                       required
                       runat="server"
                       class="form-control plainnumber"
                       type="text"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="RecTargetInput">Recruitment Target Group</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <select id="ddRecruitmentTargetGroup" runat="server" class="form-control" required>
                    <option></option>
                    <option>Fresh Graduate</option>
                    <option>Experienced</option>
                    <option>Fresh Graduate or Experienced</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="RecInput">Recruiter</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <input disallowed-chars="[^a-zA-Zs ]+"
                       id="tbHiringManager"
                       required runat="server"
                       class="form-control" type="text"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="AppJobDateInput">Approved Job Requisition Date</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <div class="controls">
                    <div class="input-group date col-sm-9">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input type="text" data-date-format="dd/mm/yyyy" required form-field="registerForm" runat="server"
                               id="tbApprovedDate"
                               class="form-control datepicker"/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="NewPosInput">New Position/Replacement</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <select id="ddNewPosition" runat="server" class="form-control" required>
                    <option></option>
                    <option>New Position</option>
                    <option>Replacement</option>
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="BisJusInput">Business Justification</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <textarea cols="40" rows="4" disallowed-chars="[^a-zA-Zs ]+" id="tbBusinessJustification" 
                                        required runat="server" 
                                        class="form-control"   />
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="ExJoDateInput">Expected Joining Date</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <div class="controls">
                    <div class="input-group date col-sm-9">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input type="text" data-date-format="dd/mm/yyyy" id="tbPreferredStartDate" form-field="registerForm" runat="server" required
                               class="form-control datepicker"/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="PosStartInput">Job Posting Start Date</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <div class="controls">
                    <div class="input-group date col-sm-9">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input type="text" data-date-format="dd/mm/yyyy" id="tbPostStartDate" form-field="registerForm" required
                               runat="server" class="form-control datepicker"/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="PosEndInput">Job Posting End Date</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <div class="controls">
                    <div class="input-group date col-sm-9">
                        <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                        </span>
                        <input type="text" data-date-format="dd/mm/yyyy" id="tbPostEndDate" form-field="registerForm" required
                               runat="server" class="form-control datepicker"/>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="JobRoleInput">Job Role</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-3">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbJobRole"
                       required runat="server"
                       class="form-control" type="text"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="JobStageInput">Job Stage</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbJobStage"
                       runat="server"
                       class="form-control" type="text"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="CountryInput">Country</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <asp:DropDownList ID="ddCountry" runat="server" DataTextField="Name"
                                  DataValueField="Code" onchange="toggleIDCardProvinceAndCity();return false"
                                  CssClass="form-control" DataSourceID="odsCountry"
                                  OnDataBound="ddDatabound"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="ProvinceInput">Province</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddProvince" datasourceid="odsProvince" datavaluefield="ProvinceCode"
                        datatextfield="ProvinceName" onchange="loadCity();return false;"
                        runat="server" class="form-control"/>
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbProvince"
                       runat="server"
                       class="form-control" type="text"/>
        
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="CityInput">City</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddCity" datasourceid="odsCity" datavaluefield="CityCode"
                        datatextfield="CityName"
                        runat="server" class="form-control"/>
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCity"
                       runat="server"
                       class="form-control" type="text"/>
        
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="ReqEduLevelInput">Required Education Level</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddRequiredEducationLevel" datasourceid="odsLevel" datavaluefield="EducationLevel"
                        datatextfield="Name"
                        runat="server" class="form-control"/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="TypeEmpInput">Type of Employment</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddTypeofEmployment" datasourceid="odsEmploymentStatus"
                        datatextfield="Name" datavaluefield="Code"
                        runat="server" class="form-control">
        
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="CategoryInput">Category</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddCategory" datasourceid="odsJobCategory" datatextfield="Name" datavaluefield="Code"
                        runat="server" class="form-control"/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="FuncInput">Function/Department</span>
                </strong><%--<span class="requiredmark">*</span>--%>
            </div>
            <div class="col-sm-3">
                <!--<select id="ddDepartment" datasourceid="odsDepartment"
                        datavaluefield="Code" datatextfield="Name"
                        runat="server" class="form-control"/>-->
                <input disallowed-chars="[^a-zA-Zs ]+" id="inpDepartment"
                       runat="server"
                       class="form-control" type="text"/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="DivInput">Division</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <!--<select id="ddDivision" datasourceid="odsDivision" datavaluefield="Code" datatextfield="Name"
                        runat="server" class="form-control"/>-->
                <input disallowed-chars="[^a-zA-Zs ]+" id="inpDivision"
                       runat="server"
                       class="form-control" type="text"/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="CostCentreInput">Cost Centre</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCostCentre"
                       runat="server"
                       class="form-control" type="text"/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="JobReqInput">Job Requirements</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <textarea cols="40" rows="4" disallowed-chars="[^a-zA-Zs ]+" id="tbJobRequirements"                                   
                                  required runat="server"
                                  class="form-control" />
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="SalaryInput">Salary Range</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-2">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbSalaryRangeBottom"
                       required runat="server"
                       class="form-control numeric" type="text"/>
            </div>
            <div class="col-sm-1 center">
                -
            </div>
            <div class="col-sm-2">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbSalaryRangeTop"
                       required runat="server"
                       class="form-control numeric" type="text"/>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="ShowSalaryInput">Show Salary</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddShowSalary" runat="server" class="form-control">
                    <option>No</option>
                    <option>Yes</option>
                </select>
            </div>
        </div>
        
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="JobDesInput">Job Description</span>
                </strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <textarea cols="40" rows="5" disallowed-chars="[^a-zA-Zs ]+" id="tbJobDescription"
                                  required runat="server" 
                                  class="form-control" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="PipelineInput">Pipeline</span>
                </strong>
            </div>
            <div class="col-sm-3">
                <select id="ddPipeline" datasourceid="odsPipeline"
                        datatextfield="Name" datavaluefield="Code"
                        runat="server" class="form-control"/>
            </div>
        </div>
        <div class="row" id="rowJobDisqQuestion" runat="server">
            <div class="col-sm-3">
                <strong>
                    <span data-lang="DisQuestInput">Disqualifier Question</span>
                </strong>
            </div>
            <div class="col-sm-5">
                <asp:ListBox ID="listDisqQuestions" SelectionMode="Multiple" runat="server" CssClass="form-control" DataValueField="Code" DataTextField="Name" data-attribute="RoleCodes"/>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
        
            </div>
            <div class="col-sm-3">
<%--                <button class="btn btn-primary" runat="server" id="btnSubmit" onclick="return validate(this, event);" onserverclick="btnSubmit_ServerClick">Save</button>--%>
                <asp:Button data-submit="false" class="btn btn-primary" runat="server" ID="btnSubmit" OnClientClick="return validate(this);" OnClick="btnSubmit_ServerClick" Text="Save" />
            </div>
        </div>
    </div>
</div>


<%--<asp:ObjectDataSource ID="odsCompany" runat="server"
                          DataObjectTypeName="ERecruitment.Domain.Companies"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          SelectMethod="GetGrantedCompanyListByRoleCode"
                          >
        <SelectParameters>
            <asp:SessionParameter Name="roleCode" SessionField="roleCode" />
        </SelectParameters>
    </asp:ObjectDataSource>--%>

<asp:ObjectDataSource ID="odsCompany" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Companies"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCompanyList"/>

<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetVacancyCategoryList"/>

<asp:ObjectDataSource ID="odsJobs" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Jobs"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetJobsList"/>

<asp:ObjectDataSource ID="odsCity" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.City"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCityList"/>

<asp:ObjectDataSource ID="odsProvince" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Province"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetProvinceList"/>

<asp:ObjectDataSource ID="odsJobCategory"
                      runat="server"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      SelectMethod="GetVacancyCategoryList"/>

<asp:ObjectDataSource ID="odsCountry" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Country"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCountryList"/>

<asp:ObjectDataSource ID="odsLevel" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.EducationLevels"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetEducationLevels"/>

<asp:ObjectDataSource ID="odsEmploymentStatus" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.EmploymentStatus"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetEmploymentStatusList"/>

<asp:ObjectDataSource ID="odsDepartment" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Departments"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetDepartmentList"/>

<asp:ObjectDataSource ID="odsDivision" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Divisions"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetDivisionList"/>

<asp:ObjectDataSource ID="odsPipeline" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Pipelines"
                      TypeName="ERecruitment.Domain.HiringManager"
                      SelectMethod="GetPipelineList"/>

<asp:ObjectDataSource ID="odsDisqualifier" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.DisqualifierQuestions"
                      TypeName="ERecruitment.Domain.DisqualificationQuestionManager"
                      SelectMethod="GetParentDisqualificationQuestionList"/>

</asp:Content>