﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="licensesetting.aspx.cs" Inherits="ERecruitment.licensesetting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div>
    <div class="row">
    <div class="col-sm-12">
        <div class="row">
            <div class="col-sm-3">
                <label>Modul</label>
            </div>
            <div class="col-sm-1">
                <input type="checkbox" disabled="disabled" runat="server" class="form-control" id="cbApplicantPortal"/>                    
            </div>
            <div class="col-sm-3">
                <label>Applicant Portal</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label></label>
            </div>
            <div class="col-sm-1">
                <input type="checkbox" runat="server" class="form-control" id="cbAts" />                    
            </div>
            <div class="col-sm-3">
                <label>Applicant Tracking System</label>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label>Approval Function</label>
            </div>
            <div class="col-sm-1">
                <input type="checkbox" runat="server" class="form-control" id="cbApprovalFunction" />         
            </div>
        </div>
        <div class="row">
            <div class="col-sm-3">
                <label></label>
            </div>
            <div class="col-sm-3">
                <button class="btn btn-primary"  runat="server" id="btnSave" 
                    onserverclick="btnSave_ServerClick" >
                    Save
            </button>         
            </div>
        </div>
        </div>
    </div>
</div>



</asp:Content>
