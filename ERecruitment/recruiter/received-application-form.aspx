﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="received-application-form.aspx.cs" Inherits="ERecruitment.received_application_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <style type="text/css">
        .jobs .panel-body > div > div{
            padding: 5px !important;
        }
    </style>

<div>
    <div>

    <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_AppCode">Applicant Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbApplicantCode" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_AppName">Applicant Name</label>
                    </div>
                    <div class="col-sm-6">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbApplicantName" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_MaritalStatus">Marital Status</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbMaritalStatus" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_Email">Email</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbEmail" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_Experience">Experience (Years)</label>
                    </div>
                    <div class="col-sm-2">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="Text1" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_LastDegree">Last Degree</label>
                    </div>
                    <div class="col-sm-2">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="Text2" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_University">University</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbUniversity" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_Major">Major</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbMajor" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_GPA">GPA</label>
                    </div>
                    <div class="col-sm-1">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbGPA" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>

            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_JobCode">Job Code</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="Text3" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_JobTitle">Job Title</label>
                    </div>
                    <div class="col-sm-6">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="Text4" readonly="true"
                               name="name" ng-model="registerFormData.name" form-field="registerForm"
                               ng-minlength="3" min-length="3" required="" runat="server"
                               class="form-control" type="text" />
                    </div> 
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_TypeOfEmployment">Type of Employment</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="Text5" readonly="true"
                                   name="name" ng-model="registerFormData.name" form-field="registerForm"
                                   ng-minlength="3" min-length="3" required="" runat="server"
                                   class="form-control" type="text" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <label runat="server" id="lbReceivedApp_JobDesc">Job Description</label>
                    </div>
                    <div class="col-sm-6">
                        <textarea cols="40" rows="4" disallowed-chars="[^a-zA-Zs ]+" readonly="readonly" id="tbJobDescription" 
                                  name="name" ng-model="registerFormData.name" form-field="registerForm"
                                  ng-minlength="3" min-length="3" required="" runat="server"
                                  class="form-control" />
                
                    </div>
                </div>

            </div>
        </div>
    </div>

    </div>
</div>
</asp:Content>
