﻿<%@ Page Title="Job Requisition Profile" Language="C#" MasterPageFile="~/masters/applicant-vacancy.Master" AutoEventWireup="true" CodeBehind="detail-application.aspx.cs" Inherits="ERecruitment.detail_application" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        var listObj;
        $(document).ready(function () {

            $("#liCandidates").addClass("active");
            if (GetQueryStringParams("rankOnly") == "true")
                $("#cbShowRank").prop("checked", true);
            else
                $("#cbShowRank").prop("checked", false);

            if (GetQueryStringParams("showArchives") == "true")
                $("#cbShowArchive").prop("checked", true);
            else
                $("#cbShowArchive").prop("checked", false);

            initiateList();

            $(".btnGo").click(function(e) {
                e.preventDefault();
                var pageSize = $("#pageSize").val();
                var page = parseInt($("#go-to").val());
            
                if ($(".pagination").children(":last-child").text() < page) {
                    page = parseInt($(".pagination").children(":last-child").text());
                } else if (page < 0) {
                    page = 0;
                } 
            
                listObj.show(((page - 1) * pageSize) + 1, pageSize);
                $(".pagination .active").removeClass("active");
                $(".pagination li").filter(function() { return $(this).text() == page }).addClass("active");
            });

            var testCode = $("#<%= hidTestCodePipeline.ClientID %>").val();
            if (testCode != null && "" != testCode) {
                $(".btnTestResult").prop("disabled", false);
            } else {
                $(".btnTestResult").prop("disabled", true);
            }
        });

        function initiateList() {

            var options = {
                valueNames: ['name'],
                page: 5,
                plugins: [
                  ListPagination({
                      outerWindow: 2
                  })
                ]
            };

            listObj = new List('listContainer', options);
        
        if (listObj.items.length == 0) {
            $(".btnGo").parent().parent().hide();
        } else {
            $(".btnGo").parent().parent().show();
        }

        listObj.on("updated",
            function() {
                if (listObj.visibleItems.length == 0) {
                    $(".btnGo").parent().parent().hide();
                } else {
                    $(".btnGo").parent().parent().show();
                }
            });
        }

        function updatePage() {
            var pageSize = $("#pageSize").val();
            listObj.show(0, pageSize);
            // highlight first page
            $(".pagination").children(":first-child").addClass("active");
        }

        function showFlagForm(applicantCode, flagStatus) {
            $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);
            $("#<%= hidSelectedFlag.ClientID %>").val(flagStatus);

            $('#flagApplicantForm').modal('show');
        }

        function showApplicantProfile(applicantCode) {
            $("#<%= hidCurrentApplicantCode.ClientID %>").val(applicantCode);

            loadPersonalInformation();
            loadEducationInfoList();
            loadExperienceInfoList();
            loadSkillInfoList();
            loadHobbyInterestInfoList();
            loadAwardInfoList();
            loadTrainingInfoList();
            loadAttachmentList();
            $('#applicantProfileForm').modal('show');
        }

        function showApplicantRankProfile(applicantCode) {
            $("#<%= hidCurrentApplicantCode.ClientID %>").val(applicantCode);
            loadApplicantRankInfo();
        }

        function showApplicantCommentList(applicantCode) {
            $("#<%= hidCurrentApplicantCode.ClientID %>").val(applicantCode);
            loadApplicantVacancyCommentList(applicantCode);

        }

        function search() {

            var gender = $("#<%= ddGender.ClientID %>").val();
            var maritalStatus = $("#<%= ddMaritalStatus.ClientID %>").val();
            var status = $("#<%= ddStatus.ClientID %>").val();
            var vacancyCode = GetQueryStringParams("vacancyCode");

            var param = "&status=" + status;
            param += "&gender=" + gender;
            param += "&maritalStatus=" + maritalStatus;
            param += "&rankOnly=" + $("#cbShowRank").prop("checked");
            param += "&showArchives=" + $("#cbShowArchive").prop("checked");

            window.location.href = "detail-application.aspx?vacancyCode=" + vacancyCode + param;

        }

        function loadPersonalInformation() {

            var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantInfo";

            $.ajax({
                url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {

                    var applicantModel = $.parseJSON(queryResult);

                    if (applicantModel["Photo"] == null)
                        $("#imgAvatar").attr("src", "../assets/images/avatar.png");
                    else
                        $("#imgAvatar").attr("src", "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + applicantModel["Code"]);

                    if (applicantModel["Name"] != null)
                        $("#spApplicantName").text(applicantModel["Name"]);
                    else
                        $("#spApplicantName").text("Unknown");

                    $("#spApplicantEmail").text(applicantModel["Email"]);
                    $("#spApplicantPhone").text(applicantModel["Phone"]);
                    $("#spGenderSpecification").text(applicantModel["GenderSpecification"]);
                    $("#spMaritalSpecification").text(applicantModel["MaritalStatusSpecification"]);


                    if (applicantModel["CurrentCardAddress"] != null)
                        $("#spApplicantAddress").text(applicantModel["CurrentCardAddress"]);

                    if (applicantModel["CurrentCardCityName"] != null)
                        $("#spApplicantCity").text(applicantModel["CurrentCardCityName"]);

                    if (applicantModel["CurrentCardCountry"] != null)
                        $("#spApplicantCountry").text(applicantModel["CurrentCardCountry"]);

                    $("#spApplicantSalary").text(formatMoney(applicantModel["ExpectedSalary"]));

                    $("#spApplicantPriorNotification").text(applicantModel["NoticePeriod"]);

                    if (applicantModel["CurrentCardCountry"] != "true")
                        $("#spApplicantAssignedAnywhere").text("Yes");
                    else
                        $("#spApplicantAssignedAnywhere").text("No");

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });
        }

        function loadApplicantRankInfo() {
            var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=GetVacancyApplicant";
            var vacancyCode = GetQueryStringParams("vacancyCode");
            $.ajax({
                url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val() + "&vacancyCode=" + vacancyCode,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {

                    var dataModel = $.parseJSON(queryResult);
                    console.log(dataModel);
                    // form
                    loadDataIntoForm("rankApplicantForm", dataModel);

                    $('#rankApplicantForm').modal('show');
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });
        }

        function loadEducationInfoList() {
            var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationInfoList";
            var listContainer = $("#containerEducationList");

            $.ajax({
                url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
                async: true,
                beforeSend: function () {
                    $(".educationData").remove();
                },
                success: function (queryResult) {

                    var modelDataList = $.parseJSON(queryResult);
                    $.each(modelDataList, function (i, item) {

                        var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["LevelName"] + " at " + item["InstitutionName"];
                        if (item["FieldOfStudyName"] != null)
                            dataInfoText += ", Field of Study: " + item["FieldOfStudyName"];
                        if (item["MajorName"] != null)
                            dataInfoText += ", Majoring in: " + item["MajorName"];


                        var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteEducationInfoForm(" + item["Id"] + ")");
                        var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showEducationInfoForm(" + item["Id"] + ")").text(dataInfoText);
                        var dataNode = $(document.createElement('span')).attr("class", "block educationData").append(dataInfoWrapper);

                        listContainer.append(dataNode);

                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });
        }

        function loadExperienceInfoList() {
            var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantExperienceInfoList";
            var listContainer = $("#containerExperienceList");

            $.ajax({
                url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".experienceData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["PositionName"] + " at " + item["OrganizationName"];
                    if (item["StillUntillNow"] == true)
                        dataInfoText = item["StartYear"] + " - Now, " + item["PositionName"] + " at " + item["OrganizationName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteExperienceInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showExperienceInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block experienceData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadSkillInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantSkillInfoList";
        var listContainer = $("#containerSkillList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".skillData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["SkillName"] + " with Proficiency: " + item["Proficiency"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteSkillInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showSkillInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block skillData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAttachmentList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAttachmentInfoList";
        var listContainer = $("#containerAttachmentList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".attachmentData").remove();
            },
            success: function (queryResult) {


                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["FileName"] + " - " + item["Notes"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteAttachmentInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('a')).attr("href", "../handlers/HandlerApplicants.ashx?commandName=DownloadAttachment&id=" + item["Id"]).attr("target", "_blank").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block attachmentData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadHobbyInterestInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantHobbyInterestInfoList";
        var listContainer = $("#containerHobbyInterestList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".hobbyInterestData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Description"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteHobbyInterestInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showHobbyInterestInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block hobbyInterestData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAwardInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAwardInfoList";
        var listContainer = $("#containerAwardList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".awardData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Year"] + " with " + item["AwardName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteAwardInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showAwardInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block awardData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadTrainingInfoList() {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantTrainingInfoList";
        var listContainer = $("#containerTrainingList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".trainingData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["TrainingName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteTrainingInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showTrainingInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block trainingData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadApplicantVacancyCommentList(applicantCode) {
        var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=GetApplicantVacancyCommentList";

        var param = "&vacancyCode=" + GetQueryStringParams("vacancyCode");
        param += "&applicantCode=" + applicantCode;
        $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);

        var listContainer = $("#commentContainer");

        $.ajax({
            url: handlerUrl + param,
            async: true,
            beforeSend: function () {
                $(".commentData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var wrapper = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");

                    var avatarSection = $(document.createElement('div')).attr("class", "col-md-2 col-sm-2");
                    var avatar = $(document.createElement('img')).attr("class", "img-circle");
                    if (item["CommentatorPhoto"] == null)
                        avatar.attr("src", "../assets/images/avatar.png");
                    else
                        avatar.attr("src", "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + item["CommentBy"]);
                    avatarSection.append(avatar);

                    var nameSection = $(document.createElement('div')).attr("class", "col-md-7 col-sm-7");
                    var name = $(document.createElement('h4')).text(item["CommentatorName"]);
                    nameSection.append(name);

                    var dateSection = $(document.createElement('div')).attr("class", "col-md-3 col-sm-3");
                    var date = $(document.createElement('span')).attr("class", "text-muted").text(item["DateString"]);
                    dateSection.append(date);

                    var breakSection = $(document.createElement('div')).attr("class", "col-md-2 col-sm-2");

                    var commentSection = $(document.createElement('div')).attr("class", "col-md-10 col-sm-10");
                    var comment = $(document.createElement('p')).attr("class", "text-muted").text(item["Comment"]);
                    commentSection.append(comment);


                    var dataNode = $(document.createElement('div')).attr("class", "row commentData").append(wrapper);
                    dataNode.append(avatarSection);
                    dataNode.append(nameSection);
                    dataNode.append(dateSection);
                    dataNode.append(breakSection);
                    dataNode.append(commentSection);

                    listContainer.append(dataNode);

                });

                $('#applicantCommentForm').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function addComment() {
        var form = "applicantCommentForm";
        var actionUrl = "../handlers/HandlerVacancies.ashx?commandName=AddComments";
        var formData = {};

        $("#" + form).find("input[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });

        $("#" + form).find("select[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });

        $("#" + form).find("textarea[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });


        var param = "&commentByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();
        param += "&vacancyCode=" + GetQueryStringParams("vacancyCode");
        param += "&applicantCode=" + $("#<%= hidApplicantCodeFlag.ClientID %>").val();

        $.post(actionUrl + param, formData).done(function (data) {

            $("#comment").val("");
            loadApplicantVacancyCommentList($("#<%= hidApplicantCodeFlag.ClientID %>").val());


        }).fail(function (xhr, status, error) {

            showNotification(xhr.responseText, 'error');

        });
    }

    var selectedItemArray = [];
    $('body').on('click', '.cbChild', function () {

        var code = $(this).attr("data-attribute");

        var index = $.inArray(code, selectedItemArray);

        if ($(this).is(':checked')) {
            selectedItemArray.push(code);

        }
        else {
            selectedItemArray.splice(index, 1);
        }
    });



    function showUpdateStatusForm(applicantCode) {
        $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);
        $('#updateStatusApplicantForm').modal('show');
    }

    function updatePipeline() {

        var updateStatus = $("#<%= ddNewStatus.ClientID %>").val();
        var issuedBy = $("#<%= hidCurrentUser.ClientID %>").val();

        var codeList = $("#<%= hidApplicantCodeFlag.ClientID %>").val();;
        var vacancyCode = GetQueryStringParams("vacancyCode");
        var currentPipeline = GetQueryStringParams("status");
        var urlString = "";
        if ($('#<%=ddNewStatus.ClientID %> option:selected').text() == "Hired") {
            checkSAPJobPosition(codeList);
        }
        else {
            urlString = "../handlers/HandlerProcessHiring.ashx?commandName=UpdateApplicantPipeline&appCodes=" + codeList + "&vacancyCode=" + vacancyCode + "&statusCode=" + updateStatus
            $.ajax({
                url: urlString,
                async: true,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    showNotification("successfully updated");
                    search();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText, 'error');
                }
            });
        }


    }

    function disqualifyApplicant(applicantCode) {

        confirmMessage("Are you sure want to disqualify the applicant?",
            "warning",
            function() {
                var updateStatus = "Rejected";
                var issuedBy = $("#<%= hidCurrentUser.ClientID %>").val();

                var codeList = applicantCode;
                var vacancyCode = GetQueryStringParams("vacancyCode");
                var currentPipeline = GetQueryStringParams("status");

                $.ajax({
                    url: "../handlers/HandlerProcessHiring.ashx?commandName=DisqualifyApplicant&appCodes=" + codeList + "&vacancyCode=" + vacancyCode + "&statusCode=" + updateStatus,
                    async: true,
                    beforeSend: function () {

                    },
                    success: function (queryResult) {
                        showNotification("applicant disqualified");
                        search();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        showNotification(xhr.responseText, 'error');
                    }
                });
            });
    }


    function checkSAPJobPosition(applicantCode)
    {
        var vacancyCode = GetQueryStringParams("vacancyCode");
        var source = $("#<%= hidSourceJob.ClientID %>").val();
        $('#applicantCode').val(applicantCode);
            $.ajax({
                url: "../handlers/HandlerProcessHiring.ashx?commandName=countHiredApplicant&vacancyCode=" + vacancyCode,
                async: true,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    if (queryResult == 1) {
                        showNotification("This Position has reach quota of opening position. You can't hired applicant again.");
                    }
                    else {
                        if (source != "SAP") {
                            confirmMessage("Are you sure want to hire the applicant?", "info", function () {
                                hireApplicant();
                            });
                        } else
                        {
                            hireFromSAP(vacancyCode);
                        }
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText, 'error');
                }
            });

    }

    function hireFromSAP(vacancyCode) {
            $.ajax({
                url: "../handlers/HandlerProcessHiring.ashx?commandName=GetSAPJobPositionID&vacancyCode=" + vacancyCode,
                async: true,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    var options = "";
                    var dataModel = $.parseJSON(queryResult);
                    if (dataModel.length == 0) {
                        options += '<option value="">Select an option</option>';
                    }
                    else {
                        for (var i = 0; i < dataModel.length; i++) {
                            if (i == 0) {
                                options += '<option value="">Select an option</option>';
                            }

                            options += '<option value="' + dataModel[i].SAPJobPositionID + '">' + dataModel[i].SAPJobPositionID + '</option>';

                        }
                    }
                    $('#ddSAPJobPosition').html(options);

                    confirmMessage("Are you sure want to hire the applicant?", "info", function () {
                        $('#mappingSAPPositionForm').modal('show');
                    });
                    
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText, 'error');
                }
            });
        }

    function hireApplicant() {
                var updateStatus = "Hired";
                var issuedBy = $("#<%= hidCurrentUser.ClientID %>").val();

                var codeList = $('#applicantCode').val();
                var vacancyCode = GetQueryStringParams("vacancyCode");
                var currentPipeline = GetQueryStringParams("status");
                var source = $("#<%= hidSourceJob.ClientID %>").val();
                var sapJob =  $('#ddSAPJobPosition').val();

                if (source == "SAP" && sapJob == "")
                {
                    showNotification("Please choose SAP Position ID.");
                }
                else if (source == "SAP" && sapJob != "")
                {
                    $.ajax({
                        url: "../handlers/HandlerProcessHiring.ashx?commandName=HireApplicant&appCodes=" + codeList + "&vacancyCode=" + vacancyCode + "&statusCode=" + updateStatus + "&sapJob=" + sapJob,
                        async: true,
                        beforeSend: function () {
                        },
                        success: function (queryResult) {
                            $('#mappingSAPPositionForm').modal('hide');
                            showNotification("applicant hired");
                            search();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            showNotification(xhr.responseText, 'error');
                        }

                    });
                } else {
                    $.ajax({
                        url: "../handlers/HandlerProcessHiring.ashx?commandName=HireApplicant&appCodes=" + codeList + "&vacancyCode=" + vacancyCode + "&statusCode=" + updateStatus + "&sapJob=nonesap",
                        async: true,
                        beforeSend: function () {

                        },
                        success: function (queryResult) {
                            showNotification("applicant hired");
                            search();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            showNotification(xhr.responseText, 'error');
                        }

                    });
                }

    }

    function showTestResultForm(applicantCode) {

        $(".testResult").val("");
        $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);

        var param = "&vacancyCode=" + GetQueryStringParams("vacancyCode");
        param += "&applicantCode=" + $("#<%= hidApplicantCodeFlag.ClientID %>").val();

        var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=GetApplicantTestResultList";

        $.ajax({
            url: handlerUrl + param,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var testResultModelList = $.parseJSON(queryResult);
                var form = "applicantTestResultForm";
                if (testResultModelList != null) {
                    $.each(testResultModelList, function (i, item) {

                        var attrName = item["TestCode"];
                        var attrValue = item["Result"];
                        // assign input field
                        $("#" + form).find("input[data-attribute = '" + attrName + "']").val(attrValue);


                    });
                }
                if (testResultModelList.length == 0) {
                    $('button.btn-primary', '#applicantTestResultForm').hide();
                } else {
                    $('button.btn-primary', '#applicantTestResultForm').show();
                }
                // show form
                $('#applicantTestResultForm').modal('show');

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function updateTestResult() {
        var form = "applicantTestResultForm";
        var actionUrl = "../handlers/HandlerVacancies.ashx?commandName=SubmitApplicantTestResult";
        var formData = {};

        $("#" + form).find("input[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });

        $("#" + form).find("select[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });

        $("#" + form).find("textarea[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });


        var param = "&commentByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();
        param += "&vacancyCode=" + GetQueryStringParams("vacancyCode");
        param += "&applicantCode=" + $("#<%= hidApplicantCodeFlag.ClientID %>").val();

        $.post(actionUrl + param, formData).done(function (data) {

            $('#applicantTestResultForm').modal('hide');
            showNotification("Test result submitted");

        }).fail(function (xhr, status, error) {

            showNotification(xhr.responseText, 'error');

        });
    }

    function showOfferingForm(applicantCode) {

        $(".offeringFields").val("");
        $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);

        $('#applicantOfferingForm').modal('show');
    }

    function submitOffering() {
        var form = "applicantOfferingForm";
        var actionUrl = "../handlers/HandlerVacancies.ashx?commandName=SubmitOffering&emailTemplateId=" + $("#<%= ddOfferingEmailTemplate.ClientID %>").val();
        var formData = {};

        $("#" + form).find("input[data-attribute]").each(function (index, node) {
            if ($(this).attr('type') === "checkbox") {
                formData[$(node).attr("data-attribute")] = $(node).is(":checked");
            } else {
                formData[$(node).attr("data-attribute")] = $(node).val();    
            }
            
        });

        $("#" + form).find("select[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });

        $("#" + form).find("textarea[data-attribute]").each(function (index, node) {
            formData[$(node).attr("data-attribute")] = $(node).val();
        });


        var param = "&commentByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();
        param += "&vacancyCode=" + GetQueryStringParams("vacancyCode");
        param += "&applicantCode=" + $("#<%= hidApplicantCodeFlag.ClientID %>").val();
        param += "&issuedByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();

        $.post(actionUrl + param, formData).done(function (data) {

            $('#applicantOfferingForm').modal('hide');
            showNotification("Offering submitted");

        }).fail(function (xhr, status, error) {

            showNotification(xhr.responseText, 'error');

        });
    }

    function showInvitationForm(applicantCode) {

        $(".invitationField").val("");
        $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);
        $('#applicantInvitationForm').modal('show');

    }

    function showReferApplicantForm(applicantCode) {
        $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);
        $("#<%= listCompanies.ClientID %>").val("").trigger("chosen:updated");
        $(".applicantReferenceField").val("");

        $('#referApplicantForm').modal('show');
    }

    function updateApplicantReference() {
        var form = "referApplicantForm";
        var actionUrl = "../handlers/HandlerVacancies.ashx?commandName=ReferApplicant";

        var param = "&commentByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();
        param += "&vacancyCode=" + GetQueryStringParams("vacancyCode");
        param += "&applicantCode=" + $("#<%= hidApplicantCodeFlag.ClientID %>").val();
        param += "&issuedByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();
        param += "&comments=" + $("#tbComments").val();
        param += "&companyCodes=" + $("#<%= listCompanies.ClientID %>").val();
        param += "&emailTemplateId=" + $("#<%= ddReferApplicantEmailTemplate.ClientID %>").val();

        $.post(actionUrl + param).done(function (data) {

            $('#referApplicantForm').modal('hide');
            showNotification("Applicant referenced");
            search();

        }).fail(function (xhr, status, error) {
            showNotification(xhr.responseText, 'error');
        });
    }

    function submitInvitation() {
        var form = "applicantInvitationForm";
        var actionUrl = "../handlers/HandlerVacancies.ashx?commandName=SubmitInvitation&emailTemplateId=" + $("#<%= ddInvitationEmailTemplate.ClientID %>").val();
        var formData = {};

        if (!getNodeData(form, formData)) return false;

        var param = "&commentByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();
        param += "&vacancyCode=" + GetQueryStringParams("vacancyCode");
        param += "&applicantCode=" + $("#<%= hidApplicantCodeFlag.ClientID %>").val();
        param += "&issuedByCode=" + $("#<%= hidCurrentUser.ClientID %>").val();

        waitingDialog.show('Loading...');
        $.post(actionUrl + param, formData).done(function (data) {

            waitingDialog.hide();
            $('#applicantInvitationForm').modal('hide');
            showNotification("Invitation submitted");

        }).fail(function (xhr, status, error) {

            waitingDialog.hide();
            showNotification(xhr.responseText, 'error');

        });
    }

    function showAttachmentForm() {
        $('#applicantAttachmentForm').modal('show');
    }

    function showApplicantOtherJobs(applicantCode) {
        $('#applicantApplicationForm').modal('show');
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantApplicationList";
        var listContainer = $("#applicationContainer");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: true,
            beforeSend: function () {
                $(".applicationData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    if (item["VacancyCode"] != GetQueryStringParams("vacancyCode")) {

                        // vacancy info section
                        var vacancyInfoSection = $(document.createElement('div')).attr("class", "col-md-8 col-sm-8");
                        var positionInfo = $(document.createElement('span')).attr("class", "block text-uppercase").append($(document.createElement('h4')).text(item["VacancyPositionName"]));
                        var companyInfo = $(document.createElement('span')).attr("class", "block").append($(document.createElement('i')).attr("class", "fa fa-building")).append($(document.createElement('span')).text("Company: " + item["VacancyCompanyName"]));
                        var locationInfo = $(document.createElement('span')).attr("class", "block").append($(document.createElement('i')).attr("class", "fa fa-map-marker")).append($(document.createElement('span')).text("Location: " + item["VacancyCityName"]));

                        vacancyInfoSection.append(positionInfo);
                        vacancyInfoSection.append(companyInfo);
                        vacancyInfoSection.append(locationInfo);

                        // time section
                        var timeInfoSection = $(document.createElement('div')).attr("class", "col-md-4 col-sm-4");
                        var appliedDateInfo = $(document.createElement('span')).attr("class", "block").append($(document.createElement('i')).attr("class", "fa fa-clock-o")).append($(document.createElement('span')).text("Applied Date: " + displayJsonDate(item["AppliedDate"])));
                        var lastUpdateInfo = $(document.createElement('span')).attr("class", "block").append($(document.createElement('i')).attr("class", "fa fa-clock-o")).append($(document.createElement('span')).text("Update Date: " + displayJsonDate(item["UpdateStamp"])));

                        timeInfoSection.append(appliedDateInfo);
                        timeInfoSection.append(lastUpdateInfo);

                        // comments section
                        var commentInfoSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");
                        var commentCaption = $(document.createElement('span')).attr("class", "block").append($(document.createElement('i')).attr("class", "fa fa-clipboard")).append($(document.createElement('span')).text("Status: " + item["InternalStatusLabel"]));
                        commentInfoSection.append(commentCaption);

                        var commentContentSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");
                        var commentContent = $(document.createElement('p')).attr("class", "text-info").text("");
                        commentContentSection.append(commentContent);

                        var dataNode = $(document.createElement('div')).attr("class", "row jobSection applicationData");
                        dataNode.append(vacancyInfoSection);
                        dataNode.append(timeInfoSection);
                        dataNode.append(commentInfoSection);
                        dataNode.append(commentContentSection);

                        listContainer.append(dataNode);
                    }
                });

                $('#applicantApplicationForm').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function showComment(notes, typeApp) {
        var typeDialog, typeNotif;
        if (typeApp == 1) {
            typeDialog = BootstrapDialog.TYPE_SUCCESS;
            typeNotif = "Green Listed Applicant Notes";
        }
        else {
            typeDialog = BootstrapDialog.TYPE_DANGER;
            typeNotif = "Black Listed Applicant Notes";
        }
        BootstrapDialog.show({
            type: typeDialog,
            message: notes,
            title: typeNotif
        });
    }

    var docDefinition = {
        // a string or { width: number, height: number }
        pageSize: 'A4',

        // by default we use portrait, you can change it to landscape if you wish
        pageOrientation: 'portrait',

        // [left, top, right, bottom] or [horizontal, vertical] or just a number for equal margins
        pageMargins: [40, 40, 40, 40],
        styles: {
            header: {
                fontSize: 18,
                bold: true
            },
            bigger: {
                fontSize: 15,
                italics: true,
            },
            subheader: {
                fontSize: 15,
                bold: true
            },
        },
        defaultStyle: {
            columnGap: 20,
        },
        content: []
    };


    function convertImgToDataURLviaCanvas(url, callback, outputFormat) {
        var img = new Image();
        img.crossOrigin = 'Anonymous';
        img.onload = function () {
            var canvas = document.createElement('CANVAS');
            var ctx = canvas.getContext('2d');
            var dataURL;
            canvas.height = this.height;
            canvas.width = this.width;
            ctx.drawImage(this, 0, 0);
            dataURL = canvas.toDataURL(outputFormat);
            callback(dataURL);
            canvas = null;
        };
        img.src = url;
    }

    function ToJavaScriptDate(value) {
        var pattern = /Date\(([^)]+)\)/;
        var results = pattern.exec(value);
        var dt = new Date(parseFloat(results[1]));
        return dt.getDate()  + "/" + (dt.getMonth() + 1) + "/" + dt.getFullYear();
    }

    function generatePDF(applicantCode) {
        waitingDialog.show('Generate CV, Please Wait ...');
        //loadPersonalInformation2(applicantCode);
        docDefinition.content = [];
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantInfo";
        var name;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {

            },
            success: function (queryResult) {

                queryResult = queryResult.replace(/\r?\n|\r/g, " ")
                var applicantModel;
                applicantModel = $.parseJSON(queryResult);

                if (applicantModel["Photo"] == null) {
                    profileimage = "../assets/images/avatar.png";
                }
                else {
                    // profileimage = "../assets/images/avatar.png";
                    profileimage = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + applicantModel["Code"];
                }

                if (applicantModel["Name"] != null)
                    name = applicantModel["Name"].toUpperCase();
                else
                    name = "UNKNOWN";

                convertImgToDataURLviaCanvas(profileimage, function (base64Img) {

                    var salary = formatMoney(applicantModel["ExpectedSalary"]);
                    var address, birthPlace, birthDate;
                    if (applicantModel["CurrentCardAddress"] != null) {
                        address = applicantModel["CurrentCardAddress"].replace(/\r?\n|\r/g, " ");
                    }
                    else {
                        address = '-';
                    }

                    if (applicantModel["BirthPlace"] != null)
                    {
                        birthPlace = applicantModel["BirthPlace"].charAt(0).toUpperCase() + applicantModel["BirthPlace"].slice(1);
                    }
                    else {
                        birthPlace = '-';
                    }

                    if (applicantModel["Birthday"] != null) {
                        birthDate = ToJavaScriptDate(applicantModel["Birthday"]);
                    }
                    else {
                        birthDate = '-';
                    }

                    var willingAssign;
                    if (applicantModel["CurrentCardCountry"] != "true")
                        willingAssign = "Yes";
                    else
                        willingAssign = "No";

                    docDefinition.content.push({
                        columns: [{
                            image: base64Img,
                            width: 140,
                            height: 150
                        },
                        [
                        {
                            text: name ,
                            style: 'header'
                        },
                            '\nGender : ' + applicantModel["GenderSpecification"] + '\n',
                            'Marital Status : ' + applicantModel["MaritalStatusSpecification"] + '\n',
                            'Birth : ' + birthPlace + ', ' + birthDate + '\n',
                            'Email : ' + applicantModel["Email"] + '\n',
                            'Phone : ' + applicantModel["Phone"] + '\n',
                            'Religion : ' + applicantModel["Religion"] + '\n',
                            'Nationality : ' + applicantModel["Nationality"] + '\n',
                            'ID Number : ' + applicantModel["IDCardNumber"] + '\n'
                        ]
                        ]
                    },
                            '\n\n',
                            {
                                text: 'Address Information',
                                style: 'subheader'
                            },
                            address,
                            applicantModel["CurrentCardCityName"] + '\n\n',
                            {
                                text: 'Career & Placements',
                                style: 'subheader'
                            },
                            "Expected Salary : " + salary,
                            "Prior Notification Period : " + applicantModel["NoticePeriod"] + " month(s)",
                            "Willing to be assigned in difference cities in Indonesia? " + willingAssign
                        )
                    loadEducationInfoList2(applicantCode);
                    loadExperienceInfoList2(applicantCode)
                    loadSkillInfoList2(applicantCode);
                    loadHobbyInterestInfoList2(applicantCode);
                    loadAwardInfoList2(applicantCode);
                    loadTrainingInfoList2(applicantCode);
                    loadAttachmentList2(applicantCode);
                    //pdfMake.createPdf(docDefinition).open();
                    waitingDialog.hide();
                    pdfMake.createPdf(docDefinition).download("CV_" + name + '.pdf');
                });
            }
        });
    }

    function loadEducationInfoList2(applicantCode) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationInfoList";

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                if (modelDataList.length > 0) {
                    docDefinition.content.push(
                    {
                        text: '\nEducation Information',
                        style: 'subheader'
                    })
                }
                else {
                    docDefinition.content.push(
                    {
                        text: '\nEducation Information',
                        style: 'subheader'
                    }, '-')
                }
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["LevelName"] + " at " + item["InstitutionName"];
                    if (item["FieldOfStudyName"] != null)
                        dataInfoText += ", Field of Study: " + item["FieldOfStudyName"];
                    if (item["MajorName"] != null)
                        dataInfoText += ", Majoring in: " + item["MajorName"];

                    docDefinition.content.push({ text: dataInfoText })
                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadExperienceInfoList2(applicantCode) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantExperienceInfoList";

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                if (modelDataList.length > 0) {
                    docDefinition.content.push(
                    {
                        text: '\nExperiences',
                        style: 'subheader'
                    })
                }
                else {
                    docDefinition.content.push(
                    {
                        text: '\nExperiences',
                        style: 'subheader'
                    }, '-')
                }
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["PositionName"] + " at " + item["OrganizationName"];
                    if (item["StillUntillNow"] == true)
                        dataInfoText = item["StartYear"] + " - Now, " + item["PositionName"] + " at " + item["OrganizationName"];

                    docDefinition.content.push({ text: dataInfoText })

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadSkillInfoList2(applicantCode) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantSkillInfoList";

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                if (modelDataList.length > 0) {
                    docDefinition.content.push(
                    {
                        text: '\nSkills',
                        style: 'subheader'
                    })
                }
                else {
                    docDefinition.content.push(
                    {
                        text: '\nSkills',
                        style: 'subheader'
                    }, '-')
                }
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["SkillName"] + " with Proficiency: " + item["Proficiency"];

                    docDefinition.content.push({ text: dataInfoText })

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAttachmentList2(applicantCode) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAttachmentInfoList";
        var listContainer = $("#containerAttachmentList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {

            },
            success: function (queryResult) {


                var modelDataList = $.parseJSON(queryResult);
                if (modelDataList.length > 0) {
                    docDefinition.content.push(
                    {
                        text: '\nAttachments',
                        style: 'subheader'
                    })
                }
                else {
                    docDefinition.content.push(
                    {
                        text: '\nAttachments',
                        style: 'subheader'
                    }, '-')
                }
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["FileName"] + " - " + item["Notes"];

                    docDefinition.content.push({ text: dataInfoText })

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadHobbyInterestInfoList2(applicantCode) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantHobbyInterestInfoList";

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                if (modelDataList.length > 0) {
                    docDefinition.content.push(
                    {
                        text: '\nHobbies & Interest',
                        style: 'subheader'
                    })
                }
                else {
                    docDefinition.content.push(
                    {
                        text: '\nHobbies & Interest',
                        style: 'subheader'
                    }, '-')
                }
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Description"];
                    docDefinition.content.push({ text: dataInfoText })

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAwardInfoList2(applicantCode) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAwardInfoList";


        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                if (modelDataList.length > 0) {
                    docDefinition.content.push(
                    {
                        text: '\nAwards',
                        style: 'subheader'
                    })
                }
                else {
                    docDefinition.content.push(
                    {
                        text: '\nAwards',
                        style: 'subheader'
                    }, '-')
                }
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Year"] + " with " + item["AwardName"];

                    docDefinition.content.push({ text: dataInfoText })

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadTrainingInfoList2(applicantCode) {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantTrainingInfoList";

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode,
            async: false,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                if (modelDataList.length > 0) {
                    docDefinition.content.push(
                    {
                        text: '\nTrainings',
                        style: 'subheader'
                    })
                }
                else {
                    docDefinition.content.push(
                    {
                        text: '\nTrainings',
                        style: 'subheader'
                    }, '-')
                }
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["TrainingName"];
                    docDefinition.content.push({ text: dataInfoText })

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }
    </script>

    <asp:HiddenField ID="hidCurrentApplicantCode" runat="server" />
    <asp:HiddenField ID="hidSelectedFlag" runat="server" />
    <div id="dvDetailApplication" runat="server">

        <asp:Literal ID="ltMsgNotAuthorized" runat="server" Visible="false" />
        <asp:HiddenField ID="hidUrl" runat="server" />
        <asp:HiddenField ID="hidVacancyCode" runat="server" />
        <asp:HiddenField ID="hidSourceJob" runat="server" />

        <h4>
            <asp:Literal ID="ltPipeline" runat="server" /></h4>



        <div class="row searchPanel">
            <div class="col-md-12 col-sm-12">
                <div class="col-sm-2">
                    <span data-lang="GenderTitle">Gender</span>
                </div>
                <div class="col-sm-2">
                    <span data-lang="MaritalStatusTitle">Marital Status</span>
                </div>
                <div class="col-sm-2">
                    <span data-lang="FlagTitle">Flag</span>
                </div>
                <div class="col-sm-2">
                    <span data-lang="RecruitmentStatusTitle">Recruitment Status</span>
                </div>
                <div class="col-md-4 col-sm-4">
                    <input type="checkbox" id="cbShowArchive" onchange="search();" checked="checked" />
                    <span data-lang="ShowDisAppTitle">Show Disqualified Applicants</span>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddGender"
                        runat="server"
                        onchange="search();"
                        CssClass="form-control">
                        <asp:ListItem Value="" Text="Select an Item" />
                        <asp:ListItem Value="Male" Text="Male" />
                        <asp:ListItem Value="Female" Text="Female" />
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddMaritalStatus"
                        runat="server"
                        onchange="search();"
                        CssClass="form-control">
                        <asp:ListItem Value="" Text="Select an Item" />
                        <asp:ListItem Value="Single" Text="Single" />
                        <asp:ListItem Value="Married" Text="Married" />
                        <asp:ListItem Value="Divorce" Text="Divorced" />
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddFlag"
                        runat="server"
                        onchange="search();"
                        CssClass="form-control">
                        <asp:ListItem Value="" Text="All" />
                        <asp:ListItem Value="GreenList" Text="Green List" />
                        <asp:ListItem Value="BlackList" Text="Black List" />
                    </asp:DropDownList>
                </div>
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddStatus"
                        runat="server"
                        CssClass="form-control"
                        onchange="search();" />
                </div>
                <div class="col-md-4 col-sm-4">
                    <input type="checkbox" id="cbShowRank" onchange="search();" />
                    <span data-lang="ShowRankedAppTitle">Show Ranked Applicants Only</span>
                </div>
            </div>
        </div>
        <div id="listContainer">
            <div class="row">
                <div class="col-md-1 col-sm-1">
                    <select id="pageSize" class="" onchange="updatePage();">
                        <option>5</option>
                        <option>10</option>
                        <option>20</option>
                        <option>50</option>
                    </select>
                </div>
                <div class="col-md-8 col-sm-8">
                </div>
                <div class="col-md-3 col-sm-3">
                    <input class="search form-control" placeholder="Search" />
                </div>
            </div>
            <ul class="list">
                <asp:Repeater ID="repApplicantList"
                    runat="server"
                    OnItemDataBound="repApplicantList_ItemDataBound">
                    <ItemTemplate>
                        <li>
                            <div class="row jobSection">
                                <div class="col-md-12 col-sm-12">
                                    <div class="col-md-2 col-sm-2 text-center">
                                        <img class="img-thumbnail" onerror='imgError(this);' id="imgAvatar" runat="server" width="304" height="236" />
                                    </div>
                                    <div class="col-md-8 col-sm-8">
                                        <div class="col-md-12 col-sm-12">
                                            <a href="#" onclick='showApplicantProfile("<%# Eval("ApplicantCode") %>"); return false;'>
                                                <h4 class="capitalLetter"><span class="name"><%# Eval("ApplicantName") %></span></h4>
                                            </a>
                                        </div>
                                        <div class="col-md-8 col-sm-8">
                                            <%# Eval("ApplicantGender") %>, <%# Eval("ApplicantAge") %> years old, <%# Eval("ApplicantMaritalStatus") %>
                                        </div>
                                        <div class="col-md-4 col-sm-4 text-muted">
                                            <div class="block">
                                                <i class="fa fa-clock-o"></i><%# Eval("AppliedDate", "{0:dd/MM/yyyy}") %>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-8"><i class="fa fa-envelope"></i><%# Eval("ApplicantEmail") %></div>
                                        <div class="col-md-4 col-sm-4">
                                            <i class="fa fa-file"></i>Other Applied Jobs: <span class="link" onclick='showApplicantOtherJobs("<%# Eval("ApplicantCode") %>");' data-key='<%# Eval("ApplicantCode") %>'><%# Eval("ApplicantJobAppliedCount") %></span>
                                        </div>
                                        <div class="col-md-8 col-sm-8"><i class="fa fa-phone"></i><%# Eval("ApplicantPhone") %></div>
                                        <div class="col-md-4 col-sm-4">
                                            <div id="flagGreen" visible="false" runat="server">
                                                <i class="fa fa-flag text-success"></i><a href="#" onclick='showComment("<%# Eval("FlagNotes") %>", 1);'>Green Listed</a>
                                            </div>
                                            <div id="flagBlack" visible="false" runat="server">
                                                <i class="fa fa-flag text-danger"></i><a href="#" onclick='showComment("<%# Eval("FlagNotes") %>", 0);'>Black Listed</a>
                                            </div>
                                        </div>
                                        <div class="col-md-8 col-sm-8"><i class="fa fa-money"></i><%# Eval("ApplicantExpectedSalary","{0:N}") %></div>
                                        <div class="col-md-4 col-sm-4">
                                            <button id='btnProceeedStep' class='btn btn-default commandIcon' onclick='showUpdateStatusForm("<%# Eval("ApplicantCode") %>");  return false;' data-key='<%# Eval("ApplicantCode") %>' <%# Eval("IsProcessed") %>><i class="fa fa-adjust"></i>Proceed Steps</button>
                                        </div>
                                        <div class="col-md-8 col-sm-8"><i class="fa fa-book"></i><%# Eval("ApplicantLastEducationInfo") %></div>
                                        <div class="col-md-4 col-sm-4 block">
                                            <button id='btnProceeedCV' class='btn btn-default commandIcon' onclick='generatePDF("<%# Eval("ApplicantCode") %>");  return false;' data-key='<%# Eval("ApplicantCode") %>'><i class="fa fa-print"></i>Generate CV</button>
                                        </div>
                                        <div class="col-md-12 col-sm-12"><i class="fa fa-briefcase"></i><%# Eval("ApplicantLastExperienceInfo") %></div>
                                    </div>
                                    <div class="col-md-2 col-sm-2">
                                        <div class="block center">&nbsp;</div>
                                        <div class="block center">
                                            <h4><%# Eval("InternalStatusLabel") %></h4>
                                        </div>
                                        <div class="block center link" onclick='showApplicantRankProfile("<%# Eval("ApplicantCode") %>"); return false;'>
                                            <h4>Rank:
                                                <asp:Literal ID="litRank" runat="server" /></h4>
                                        </div>
                                        <div id="flagPanel" runat="server" class="block center">
                                            <button title="Green Applicants" onclick='showFlagForm("<%# Eval("ApplicantCode") %>", "GreenList"); return false;' <%# Eval("IsGreenFlag") %> class="btn btn-default halfButton"><i class="fa fa-thumbs-o-up"></i></button>
                                            <button title="Black List" onclick='showFlagForm("<%# Eval("ApplicantCode") %>", "BlackList"); return false;' <%# Eval("IsBlackFlag") %> class="btn btn-default halfButton"><i class="fa fa-thumbs-o-down"></i></button>
                                        </div>
                                        <div id="commandTestOffering" runat="server" class="block center">
                                            <button title="Test Results" class="btn btn-default halfButton btnTestResult" onclick='showTestResultForm("<%# Eval("ApplicantCode") %>"); return false;' <%# Eval("IsTestResult") %>><i class="fa fa-clipboard"></i></button>
                                            <button title="Offerings" class="btn btn-default halfButton btnIssueOffering" onclick='showOfferingForm("<%# Eval("ApplicantCode") %>"); return false;' <%# Eval("IsOffer") %>><i class="fa fa-money"></i></button>
                                        </div>
                                        <div id="commandSocial" runat="server" class="block center">
                                            <button title="Comments" class="btn btn-default halfButton btnIssueComments" onclick='showApplicantCommentList("<%# Eval("ApplicantCode") %>"); return false;' <%# Eval("IsComments") %>><i class="fa fa-comments-o"></i></button>
                                            <button title="Appointments" class="btn btn-default halfButton btnIssueInvitation" onclick='showInvitationForm("<%# Eval("ApplicantCode") %>"); return false;' <%# Eval("IsAppointment") %>><i class="fa fa-calendar-o"></i></button>
                                        </div>
                                        <div id="commandAction" runat="server" class="block center">
                                            <button title="Refer Applicants" data-key='<%# Eval("ApplicantCode") %>' onclick='showReferApplicantForm("<%# Eval("ApplicantCode") %>"); return false;' class="btn btn-default halfButton btnReferApplicant" <%# Eval("IsRefer") %>><i class="fa fa-mail-forward"></i></button>
                                            <button title="Reject" data-key='<%# Eval("ApplicantCode") %>' onclick='disqualifyApplicant("<%# Eval("ApplicantCode") %>"); return false;' class="btn btn-default halfButton btnDisqualify" <%# Eval("IsReject") %>><i class="fa fa-trash"></i></button>
                                        </div>
                                        <div class="block center">
                                            <button title="Hire" data-key='<%# Eval("ApplicantCode") %>' onclick='checkSAPJobPosition("<%# Eval("ApplicantCode") %>"); return false;' class="btn btn-default" <%# Eval("IsHire") %>><i class="fa fa-check-square-o"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ItemTemplate>
                </asp:Repeater>
            </ul>
            <ul style="list-style-type: none">
                <li>
                    <div class="col-sm-6">
                        <ul class="pagination">
                
                        </ul>
                    </div>
                    <div class="col-sm-6">
                        <div style="margin: 20px 0">
                            <div class="col-sm-3" style="padding-right: 0px"><input type="text" class="form-control" id="go-to"/></div>
                            <div class="col-sm-3" style="padding-left: 0px"><button type="button" class="btn btn-primary btnGo">Go</button></div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>

    </div>

    <asp:HiddenField ID="hidCurrentUser" runat="server" />
    <asp:HiddenField ID="hidApplicantCodeFlag" runat="server" />
    <asp:HiddenField ID="hidTestCodePipeline" runat="server" />

    <div class="modal fade" id="flagApplicantForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title" data-lang="FlagAppTitle">Flag Applicant</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <span data-lang="CommentTitle">Comments</span>
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="tbDescription" runat="server"
                                MaxLength="200"
                                CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnSubmitFlag" runat="server" onserverclick="btnSubmitFlag_ServerClick">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updateStatusApplicantForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title">Update Status Applicant</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>New Status</label>
                        </div>
                        <div class="col-sm-8">
                            <asp:DropDownList ID="ddNewStatus"
                                runat="server"
                                CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnUpdateApplicantStatus" onclick="updatePipeline(); return false;">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

        <div class="modal fade" id="mappingSAPPositionForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title">Mapping SAP Position ID</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Position ID</label>
                        </div>
                        <div class="col-sm-8">
                              <select  id="ddSAPJobPosition" data-attribute="SAPJobPosition"/>  
                                <input type="hidden"  id="applicantCode" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnHired" onclick="hireApplicant(); return false;">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantProfileForm">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row jobSection" style="border-top: none;">
                                <div class="col-md-2 col-sm-2">
                                    <img class="img-circle" onerror='imgError(this);' id="imgAvatar" width="304" height="236" />
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="block capitalLetter">
                                        <h3><span id="spApplicantName"></span></h3>
                                    </div>
                                    <div class="block"><span id="spApplicantEmail"></span></div>
                                    <div class="block"><span id="spApplicantPhone"></span></div>
                                    <div class="block"><span id="spGenderSpecification"></span>, <span id="spMaritalSpecification"></span></div>
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="AddressInfoTitle">Address Information</h4>
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="block"><span id="spApplicantAddress"></span></div>
                                    <div class="block"><span id="spApplicantCity"></span></div>
                                    <div class="block"><span id="spApplicantCountry"></span></div>
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CareerPlaceTitle">Career & Placements</h4>
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="block"><span data-lang="ExpectedSalaryTitle">Expected Salary : </span><span id="spApplicantSalary"></span></div>
                                    <div class="block"><span data-lang="PriorNotifPeriodTitle">Prior Notification Period : </span><span id="spApplicantPriorNotification"></span><span data-lang="MonthTitle">month(s)</span></div>
                                    <div class="block"><span data-lang="WillingAssignTitle">Willing to be assigned in difference cities in Indonesia?  </span><span id="spApplicantAssignedAnywhere"></span></div>
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="EduInfoTile">Education Information</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerEducationList">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="ExperiencesTitle">Experiences</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerExperienceList">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="SkillsTitle">Skills</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerSkillList">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="HobbyTitle">Hobbies & Interest</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerHobbyInterestList">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="AwardsTitle">Awards</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerAwardList">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="TrainingsTitle">Trainings</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerTrainingList">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="AttachmentsTitle">Attachments</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerAttachmentList">
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantCommentForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title" data-lang="CommentTitle">Comments</h1>
                </div>
                <div class="modal-body" id="commentContainer">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-2 col-sm-2">
                                <img class="img-circle" onerror='imgError(this);' src="../assets/images/avatar.png" />
                            </div>
                            <div class="col-md-10 col-sm-10">
                                <textarea name="comment" id="comment" class="form-control" data-attribute="Comment"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="col-md-2 col-sm-2">
                            </div>
                            <div class="col-md-10 col-sm-10">
                                <button type="button" class="btn btn-primary" onclick="addComment(); return false;">Submit</button>
                                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantTestResultForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title" data-lang="TestResultsTitle">Test Results</h1>
                </div>
                <div class="modal-body">

                    <asp:Repeater ID="repTest"
                        runat="server">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("TestCode") %>' />
                                        <label class="muted"><%# Eval("TestName") %></label>
                                    </div>
                                    <div class="col-sm-2">
                                        <asp:TextBox ID="tbTestValue" runat="server" data-attribute='<%# Eval("TestCode") %>' CssClass="form-control testResult" />
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="updateTestResult(); return false();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantOfferingForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title" data-lang="OfferingTitle">Offering Form</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-3" data-lang="EmailTemplateTitle">
                            Email Template
                        </div>
                        <div class="col-sm-9">
                            <asp:DropDownList ID="ddOfferingEmailTemplate"
                                runat="server"
                                CssClass="form-control"
                                Width="150"
                                DataSourceID="odsApplicantEmail"
                                DataValueField="Id"
                                DataTextField="TemplateName" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" data-lang="MonthlySalaryTitle">
                            Monthly Salary
                        </div>
                        <div class="col-sm-3">
                            <input required="" data-attribute="MonthlySalary" class="form-control offeringFields numeric" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" data-lang="THRTitle">
                            THR
                        </div>
                        <div class="col-sm-5">
                            <input type="checkbox" class="offeringFields" data-attribute="EligibleForTHR" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" data-lang="IncentiveTitle">
                            Incentives
                        </div>
                        <div class="col-sm-5">
                            <input type="checkbox" class="offeringFields" data-attribute="EligibleForIncentive" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" data-lang="BonusTitle">
                            Bonus
                        </div>
                        <div class="col-sm-5">
                            <input type="checkbox" class="offeringFields" data-attribute="EligibleForBonus" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3" data-lang="NotesTitle">
                            Notes
                        </div>
                        <div class="col-sm-9">
                            <textarea data-attribute="Notes" class="form-control"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitOffering(); return false;">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantAttachmentForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title">Attachments</h1>
                </div>
                <div class="modal-body">
                    <asp:Repeater ID="repAttachment"
                        runat="server">
                        <ItemTemplate>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-3">
                                        <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("AttachmentCode") %>' />
                                        <label class="muted"><%# Eval("AttachmentObj.Name") %></label>
                                    </div>
                                    <div class="col-sm-6">
                                        <asp:FileUpload ID="fileUpload" runat="server" />
                                    </div>
                                    <div class="col-sm-3">
                                        <asp:Literal ID="litAttachmentName" runat="server" />
                                    </div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="return false();">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantInvitationForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title" data-lang="InviteTitle">Invitation</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4" data-lang="EmailTemplateTitle">
                            Email Template
                        </div>
                        <div class="col-sm-8">
                            <asp:DropDownList ID="ddInvitationEmailTemplate"
                                runat="server"
                                CssClass="form-control"
                                Width="150"
                                DataSourceID="odsApplicantEmail"
                                DataValueField="Id"
                                DataTextField="TemplateName" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" data-lang="UserFeedbackEmailTemplateTitle">
                            User Feedback Email Template
                        </div>
                        <div class="col-sm-8">
                            <asp:DropDownList ID="ddUserFeedbackInvitationEmailTemplate"
                                runat="server"
                                CssClass="form-control"
                                Width="150"
                                DataSourceID="odsApplicantEmailEmpty"
                                DataValueField="Id"
                                data-attribute="FeedbackInvitationEmail"
                                DataTextField="TemplateName" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" data-lang="InviteForTitle">Invitation For</div>
                        <div class="col-sm-8">
                            <textarea data-attribute="InviteTo" class="form-control invitationField"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" data-lang="DateInviteTitle">Date</div>
                        <div class="col-sm-6">
                            <div class="controls">
                                <div class="input-group date col-sm-9">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" data-date-format="dd/mm/yyyy" required=""
                                        data-attribute="Date"
                                        class="form-control datepicker" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" data-lang="TimeInviteTitle">Time</div>
                        <div class="col-sm-2">
                            <input type="text" data-attribute="TimeStart" class="form-control timepicker invitationField" />
                        </div>
                        <div class="col-sm-1 text-center">-</div>
                        <div class="col-sm-2">
                            <input type="text" data-attribute="TimeEnd" class="form-control timepicker invitationField" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" data-lang="PlaceInviteTitle">Place</div>
                        <div class="col-sm-8">
                            <textarea data-attribute="Place" class="form-control invitationField"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" data-lang="PICTitle">PIC</div>
                        <div class="col-sm-8">
                            <textarea data-attribute="Pic" class="form-control invitationField"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4" data-lang="NotesTitle">Notes</div>
                        <div class="col-sm-8">
                            <textarea class="form-control invitationField" data-attribute="Notes"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="submitInvitation(); return false;">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantApplicationForm">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title" data-lang="OtherAppliedJobTitle">Other Applied Jobs</h1>
                </div>
                <div class="modal-body" id="applicationContainer">
                    <div class="row jobSection applicationData">
                        <div class="col-md-7 col-sm-7">
                            <div class="block"><strong></strong></div>
                            <div class="block"><i class="fa fa-building"></i>Company: </div>
                            <div class="block"><i class="fa fa-map-marker"></i>Location: </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="block"><i class="fa fa-clock-o"></i>Applied On: </div>
                            <div class="block"><i class="fa fa-clock-o"></i>Last Update: </div>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="block"><i class="fa fa-clipboard"></i>Closed On: </div>
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <p class="text-info">The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from "de Finibus Bonorum et Malorum" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="disqualifyApplicantForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title">Disqualify Applicant</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Method</label>
                        </div>
                        <div class="col-sm-8">
                            <select id="ddDisqualificationMethod">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Reason</label>
                        </div>
                        <div class="col-sm-8">
                            <select id="ddDisqualificationReason">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <label>Comments</label>
                        </div>
                        <div class="col-sm-8">
                            <asp:TextBox ID="TextBox1" runat="server"
                                MaxLength="200"
                                CssClass="form-control" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="updatePipeline(); return false;">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="referApplicantForm">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title" data-lang="ReferAppTitle">Refer Applicant</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-12">
                            <p class="text-info">
                                <i class="fa fa-info-circle"></i>
                                <span data-lang="ReferDescTitle">Refering applicant to another company/business unit, will cause the applicant to be disqualified from the current recruitment process.</span>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span data-lang="EmailTemplateTitle">Email Template</span>
                        </div>
                        <div class="col-sm-8">
                            <asp:DropDownList ID="ddReferApplicantEmailTemplate"
                                runat="server"
                                CssClass="form-control"
                                Width="150"
                                DataSourceID="odsApplicantEmail"
                                DataValueField="Id"
                                DataTextField="TemplateName" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span data-lang="ReferToTitle">Refer To</span>
                        </div>
                        <div class="col-sm-8">
                            <asp:ListBox ID="listCompanies" SelectionMode="Multiple" runat="server" CssClass="form-control" DataValueField="Code" DataTextField="Name" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <span data-lang="CommentTitle">Comments</span>
                        </div>
                        <div class="col-sm-8">
                            <textarea id="tbComments" class="form-control applicantReferenceField"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="updateApplicantReference(); return false;">Submit</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="rankApplicantForm">
        <div class="modal-dialog modal-lg">

            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" data-lang="RankQualificationTitle">Rank Qualification</h4>
                </div>
                <div class="modal-body">
                    <h4 data-lang="ExperienceTitle">Experience</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <span data-lang="YearExpWeightTitle">Year of Experience</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="ApplicantYearsOfExperience"></span>
                            </div>
                            <div class="col-sm-3"></div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationExperienceMinimumYearWeight"></span>
                            </div>

                        </div>
                    </div>

                    <h4 data-lang="EducationsTitle">Educations</h4>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <span data-lang="LevelWeightTitle">Level</span>
                            </div>
                            <div class="col-sm-4">
                                <span data-attribute="ApplicantLastEducationLevel"></span>
                            </div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationMinimumEducationLevelCodeWeight"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <span data-lang="FieldTitle">Field</span>
                            </div>
                            <div class="col-sm-4">
                                <span data-attribute="ApplicantLastEducationField"></span>
                            </div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationEducationFieldCodeWeight"></span>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">

                            <div class="col-sm-2">
                                <span data-lang="GPATitle">GPA</span>
                            </div>
                            <div class="col-sm-4">
                                <span data-attribute="ApplicantLastEducationGPA"></span>
                            </div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationEducationGPARangeWeight"></span>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <span data-lang="LocationWeightTitle">Location</span>
                            </div>
                            <div class="col-sm-4">
                                <span data-attribute="ApplicantLastEducationLocation"></span>
                            </div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationEducationLocationCodeWeight"></span>
                            </div>
                        </div>
                    </div>

                    <h4>Others</h4>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <span data-lang="ResidenceTitle">Residence</span>
                            </div>
                            <div class="col-sm-4">
                                <span data-attribute="ApplicantCurrentResidence"></span>
                            </div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationResidenceLocationCodeWeight"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <span data-lang="SalaryWeightTitle">Salary</span>
                            </div>
                            <div class="col-sm-4">
                                <span class="numeric" data-attribute="ApplicantExpectedSalary"></span>
                            </div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationSalaryRangeWeight"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-2">
                                <span data-lang="AgeTitle">Age</span>
                            </div>
                            <div class="col-sm-4">
                                <span data-attribute="ApplicantAge"></span>
                            </div>
                            <div class="col-sm-1">
                                <span data-lang="WeightTitle">Weight</span>
                            </div>
                            <div class="col-sm-1">
                                <span data-attribute="RankQualificationAgeRangeWeight"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:ObjectDataSource ID="odsApplicantEmail" runat="server"
        DataObjectTypeName="ERecruitment.Domain.Emails"
        TypeName="ERecruitment.Domain.ERecruitmentManager"
        SelectMethod="GetEmailListByCategory">
        <SelectParameters>
            <asp:Parameter Name="category" DefaultValue="ApplicantNotification" />
        </SelectParameters>
    </asp:ObjectDataSource>

    <asp:ObjectDataSource ID="odsApplicantEmailEmpty" runat="server"
                          DataObjectTypeName="ERecruitment.Domain.Emails"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          SelectMethod="GetEmailListByCategoryEmpty">
        <SelectParameters>
            <asp:Parameter Name="category" DefaultValue="ApplicantNotification" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
