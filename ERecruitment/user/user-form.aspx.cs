﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class user_form: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
                IniateForm(Utils.GetQueryString<string>("code"));
        }
        private void IniateForm(string code)
        {
            UserAccounts getUserAccount = AuthenticationManager.GetUserAccount(code);
            tbEmailAddress.Value = getUserAccount.Email;
            tbName.Value = getUserAccount.Name;
            tbPosition.Value = getUserAccount.Position;
            tbPassword.Value = getUserAccount.Password;
            List<UserRoles> roleList = AuthenticationManager.GetRoleList(code);
            cbRoles.DataBind();
            foreach (ListItem item in cbRoles.Items)
            {
                UserRoles findRole = roleList.Find(delegate (UserRoles temp)
                { return temp.RoleCode == item.Value; });
                if (findRole != null)
                    item.Selected = true;
            }
        }
        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
            {
                UserAccounts addAccount = new UserAccounts();
                addAccount.Active = true;
                addAccount.UserName = tbEmailAddress.Value;
                addAccount.IsRecruiter = true;
                addAccount.Password = tbPassword.Value;
                addAccount.Name = tbName.Value;
                addAccount.Email = tbEmailAddress.Value;
                addAccount.Position = tbPosition.Value;
                List<UserRoles> roleList = new List<UserRoles>();
                foreach (ListItem item in cbRoles.Items)
                {
                    if (item.Selected == true)
                    {
                        UserRoles addRole = new UserRoles();
                        addRole.RoleCode = item.Value;
                        roleList.Add(addRole);
                    }
                }
                addAccount.UserRoles = roleList;
                AuthenticationManager.SaveUserAccount(addAccount);
                JQueryHelper.InvokeJavascript("showNotification('Successfully saved');window.close();window.opener.refresh();", Page);
            }
            else
            {
                UserAccounts getUserAccount = AuthenticationManager.GetUserAccount(Utils.GetQueryString<String>("code"));
                if (getUserAccount != null)
                {
                    getUserAccount.Name = tbName.Value;
                    getUserAccount.Email = tbEmailAddress.Value;
                    getUserAccount.Password = tbPassword.Value;
                    getUserAccount.Position = tbPosition.Value;
                    List<UserRoles> roleList = new List<UserRoles>();
                    foreach (ListItem item in cbRoles.Items)
                    {
                        if (item.Selected == true)
                        {
                            UserRoles addRole = new UserRoles();
                            addRole.RoleCode = item.Value;
                            roleList.Add(addRole);
                        }
                    }
                    getUserAccount.UserRoles = roleList;
                    AuthenticationManager.UpdateUserAccount(getUserAccount);
                    JQueryHelper.InvokeJavascript("showNotification('Successfully updated');window.close();window.opener.refresh();", Page);
                }
            }

        }
    }
}