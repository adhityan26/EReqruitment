﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="applicant-green-list.aspx.cs" Inherits="ERecruitment.applicant_green_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
<script type="text/javascript">

    $(document).ready(function () {

        loadData();
    });

    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function loadData() {

        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var param = "";
        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   
                { "bSortable": false, "aTargets": [0,1,2,3,4,5,6,7,8] }
            ],
            "oLanguage":
                            { "sSearch": "Search By Name" },
            "sAjaxSource": "../datatable/HandlerDataTableUserAccounts.ashx?commandName=GetApplicantGreenList" + param
        });
    }


</script>
<div>

<h3>Green List Applicant</h3>

<table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
    <thead>
    <tr>
        <th>Name</th>
        <th>ID Card</th>
        <th>Email</th>
        <th>Marital Status</th>        
        <th>Phone Nr</th>
        <th>Gender</th>
        <th>Flag Notes</th>
        <th>Flagged By</th>
        <th>Flagged Date</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
           

</div>
</asp:Content>
