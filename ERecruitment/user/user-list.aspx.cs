﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using SS.Web.UI;
using ERecruitment.Domain;

namespace ERecruitment
{
    public partial class user_list : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                listRoles.DataSource = ERecruitmentManager.GetRoleList();
                listRoles.DataBind();
            }
        }
    }
}