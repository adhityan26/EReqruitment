﻿<%@ Page Title="Referenced Applicants" Language="C#" MasterPageFile="~/masters/applicantlist.Master" AutoEventWireup="true" CodeBehind="reference-list.aspx.cs" Inherits="ERecruitment.reference_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
<script type="text/javascript">

    var listObj;
    $(document).ready(function () {

        $("#liReferenced").addClass("active");
        initiateList();

        $(".btnGo").click(function(e) {
            e.preventDefault();
            var pageSize = 5;
            var page = parseInt($("#go-to").val());
            
            if ($(".pagination").children(":last-child").text() < page) {
                page = parseInt($(".pagination").children(":last-child").text());
            } else if (page < 0) {
                page = 0;
            } 
            
            listObj.show(((page - 1) * pageSize) + 1, pageSize);
            $(".pagination .active").removeClass("active");
            $(".pagination li").filter(function() { return $(this).text() == page }).addClass("active");
        });
    });

    function initiateList() {

        var options = {
            valueNames: ['name'],
            page: 5,
            plugins: [
              ListPagination({
                  outerWindow: 2
              })
            ]
        };

        listObj = new List('listContainer', options);
        
        if (listObj.items.length == 0) {
            $(".btnGo").parent().parent().hide();
        } else {
            $(".btnGo").parent().parent().show();
        }

        listObj.on("updated",
            function() {
                if (listObj.visibleItems.length == 0) {
                    $(".btnGo").parent().parent().hide();
                } else {
                    $(".btnGo").parent().parent().show();
                }
            });
    }

    function loadData() {

        var gender = $("#<%= ddGender.ClientID %>").val();
        var maritalStatus = $("#<%= ddMaritalStatus.ClientID %>").val();
        var status = $("#<%= ddAccountStatus.ClientID %>").val();
        var companyCode = $("#<%= ddCompany.ClientID %>").val();
        var flag = $("#<%= ddFlag.ClientID %>").val(); 

        var param = "?status=" + status;
        param += "&gender=" + gender;
        param += "&maritalStatus=" + maritalStatus;
        param += "&companyCode=" + companyCode;
        param += "&flag=" + flag;


        window.location.href = "reference-list.aspx" + param;
    }
    
    function showFlagForm(applicantCode)
    {
        $("#<%= hidApplicantCodeFlag.ClientID %>").val(applicantCode);
        $('#flagApplicantForm').modal('show');
    }
    
    function showApplicantProfile(applicantCode)
    {
        $("#<%= hidCurrentApplicantCode.ClientID %>").val(applicantCode);

        loadPersonalInformation();
        loadEducationInfoList();
        loadExperienceInfoList();
        loadSkillInfoList();
        loadHobbyInterestInfoList();
        loadAwardInfoList();
        loadTrainingInfoList();
        loadAttachmentList();
        $('#applicantProfileForm').modal('show');        
    }
    

    function loadPersonalInformation(){

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantInfo";
        
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var applicantModel = $.parseJSON(queryResult);

                if (applicantModel["Photo"] == null)
                    $("#imgAvatar").attr("src", "../assets/images/avatar.png");
                else
                    $("#imgAvatar").attr("src", "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + applicantModel["Code"]);

                if (applicantModel["Name"] != null)
                    $("#spApplicantName").text(applicantModel["Name"]);
                else
                    $("#spApplicantName").text("Unknown");

                $("#spApplicantEmail").text(applicantModel["Email"]);
                $("#spApplicantPhone").text(applicantModel["Phone"]);
                $("#spGenderSpecification").text(applicantModel["GenderSpecification"]);
                $("#spMaritalSpecification").text(applicantModel["MaritalStatusSpecification"]);
                

                if (applicantModel["CurrentCardAddress"] != null)
                    $("#spApplicantAddress").text(applicantModel["CurrentCardAddress"]);

                if (applicantModel["CurrentCardCityName"] != null)
                    $("#spApplicantCity").text(applicantModel["CurrentCardCityName"]);

                if (applicantModel["CurrentCardCountry"] != null)
                    $("#spApplicantCountry").text(applicantModel["CurrentCardCountry"]);
                    
                $("#spApplicantSalary").text(formatMoney(applicantModel["ExpectedSalary"]));

                $("#spApplicantPriorNotification").text(applicantModel["NoticePeriod"]);

                if (applicantModel["CurrentCardCountry"] != "true")
                    $("#spApplicantAssignedAnywhere").text("Yes");
                else
                    $("#spApplicantAssignedAnywhere").text("No");

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadEducationInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationInfoList";
        var listContainer = $("#containerEducationList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".educationData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["LevelName"] + " at " + item["InstitutionName"];
                    if (item["FieldOfStudyName"] != null)
                        dataInfoText += ", Field of Study: " + item["FieldOfStudyName"];
                    if (item["MajorName"] != null)
                        dataInfoText += ", Majoring in: " + item["MajorName"];


                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteEducationInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showEducationInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block educationData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadExperienceInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantExperienceInfoList";
        var listContainer = $("#containerExperienceList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".experienceData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["PositionName"] + " at " + item["OrganizationName"];
                    
                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteExperienceInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showExperienceInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block experienceData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadSkillInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantSkillInfoList";
        var listContainer = $("#containerSkillList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".skillData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["SkillName"] + " with Proficiency: " + item["Proficiency"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteSkillInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showSkillInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block skillData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAttachmentList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAttachmentInfoList";
        var listContainer = $("#containerAttachmentList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".attachmentData").remove();
            },
            success: function (queryResult) {

                
                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["FileName"] + " - " + item["Notes"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteAttachmentInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('a')).attr("href", "../handlers/HandlerApplicants.ashx?commandName=DownloadAttachment&id=" + item["Id"]).attr("target", "_blank").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block attachmentData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadHobbyInterestInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantHobbyInterestInfoList";
        var listContainer = $("#containerHobbyInterestList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".hobbyInterestData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Description"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteHobbyInterestInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showHobbyInterestInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block hobbyInterestData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAwardInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAwardInfoList";
        var listContainer = $("#containerAwardList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".awardData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Year"] + " with " + item["AwardName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteAwardInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showAwardInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block awardData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadTrainingInfoList() {
        
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantTrainingInfoList";
        var listContainer = $("#containerTrainingList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".trainingData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["TrainingName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteTrainingInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showTrainingInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block trainingData").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }



    function showApplicantReferenceList(applicantCode) {

        var companyCode = $("#<%= hidCurrentCompanyCode.ClientID %>").val();
        $('#applicantApplicationForm').modal('show');
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantReferenceList";
        var listContainer = $("#applicationContainer");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + applicantCode + "&companyCode=" + companyCode,
            async: true,
            beforeSend: function () {
                $(".applicationData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var wrapper = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");
                    var br = document.createElement('br');
                    var br1 = document.createElement('br');
                    var br2 = document.createElement('br');
                    var originReferIcon = $(document.createElement('i')).attr("class", "fa fa-building");
                    var originRefer = "Refered From " + item["OriginCompanyName"] + " to " + item["ReferenceCompanyName"];
                    var referDateIcon = $(document.createElement('i')).attr("class", "fa fa-calendar-o");
                    var referDate = "Refered at " + displayJsonDate(item["IssuedDate"]);
                    var referByIcon = $(document.createElement('i')).attr("class", "fa fa-user");
                    var referBy = "Refered by " + item["CommentatorName"];
                    var notesIcon = $(document.createElement('i')).attr("class", "fa fa-file");
                    var notes = "Notes : " + item["Notes"];
                    wrapper.append(originReferIcon);
                    wrapper.append(originRefer);
                    wrapper.append(br1);
                    wrapper.append(referDateIcon);
                    wrapper.append(referDate);
                    wrapper.append(br2);
                    wrapper.append(referByIcon);
                    wrapper.append(referBy);
                    wrapper.append(br);
                    wrapper.append(notesIcon);
                    wrapper.append(notes);
                    var dataNode = $(document.createElement('div')).attr("class", "row jobSection applicationData").append(wrapper);
                    //dataNode.append(originRefer);
                    //dataNode.append(referDate);
                    //dataNode.append(referBy);
                    //dataNode.append(notes);


                    listContainer.append(dataNode);
                });

                $('#applicantApplicationForm').modal('show');
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function showTestResultForm(applicantCode, vacancyCode) {
        var param = "&vacancyCode=" + vacancyCode;
            param += "&applicantCode=" + applicantCode;
           

        var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=GetApplicantTestResultList";

        $.ajax({
            url: handlerUrl + param,
            async: true,
            beforeSend: function () {
                $(".applicationResultData").remove();
            },
            success: function (queryResult) {

                var testResultModelList = $.parseJSON(queryResult);
                var listContainer = $("#resultContainer");
                if(testResultModelList != null)
                {
                    $.each(testResultModelList, function (i, item) {


                            var wrapper = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");

                            var breakSection = $(document.createElement('div')).attr("class", "col-md-3 col-sm-32");

                            var nameSection = $(document.createElement('div')).attr("class", "col-md-3 col-sm-3");
                            var name = $(document.createElement('p')).attr("class", "text-muted").text(item["TestName"]);
                            nameSection.append(name);

                            var breakSection2 = $(document.createElement('div')).attr("class", "col-md-1 col-sm-1");
                         
                            var commentSection = $(document.createElement('div')).attr("class", "col-md-3 col-sm-3");
                            var comment = $(document.createElement('p')).attr("class", "text-muted").text(item["Result"]);
                            commentSection.append(comment);


                            var dataNode = $(document.createElement('div')).attr("class", "row applicationResultData").append(wrapper);
                            dataNode.append(breakSection);
                            dataNode.append(nameSection);
                            dataNode.append(breakSection2);
                            dataNode.append(commentSection);

                            listContainer.append(dataNode);

                        });

                }
                // show form
                $('#applicantTestResultForm').modal('show');

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function showApplicantCommentList(applicantCode, vacancyCode)
    {
        var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=GetApplicantVacancyCommentList";
        
        var param = "&vacancyCode=" + vacancyCode;
        param += "&applicantCode=" + applicantCode;

        var listContainer = $("#commentContainer");

        $.ajax({
            url: handlerUrl + param,
            async: true,
            beforeSend: function () {
                $(".commentData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var wrapper = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");

                    var avatarSection = $(document.createElement('div')).attr("class", "col-md-2 col-sm-2");                    
                    var avatar = $(document.createElement('img')).attr("class", "img-circle");
                    if(item["CommentatorPhoto"] == null)
                        avatar.attr("src", "../assets/images/avatar.png");
                    else
                        avatar.attr("src", "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + item["CommentBy"]);
                    avatarSection.append(avatar);

                    var nameSection = $(document.createElement('div')).attr("class", "col-md-7 col-sm-7");                    
                    var name = $(document.createElement('h4')).text(item["CommentatorName"]);
                    nameSection.append(name);

                    var dateSection = $(document.createElement('div')).attr("class", "col-md-3 col-sm-3");                    
                    var date = $(document.createElement('span')).attr("class","text-muted").text(item["DateString"]);
                    dateSection.append(date);

                    var breakSection = $(document.createElement('div')).attr("class", "col-md-2 col-sm-2");

                    var commentSection = $(document.createElement('div')).attr("class", "col-md-10 col-sm-10");                    
                    var comment = $(document.createElement('p')).attr("class","text-muted").text(item["Comment"]);
                    commentSection.append(comment);


                    var dataNode = $(document.createElement('div')).attr("class", "row commentData").append(wrapper);
                    dataNode.append(avatarSection);
                    dataNode.append(nameSection);
                    dataNode.append(dateSection);
                    dataNode.append(breakSection);
                    dataNode.append(commentSection);

                    listContainer.append(dataNode);

                });
                
                $('#applicantCommentForm').modal('show');   
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

</script>

<asp:HiddenField ID="hidApplicantCodeFlag" runat="server" />
<asp:HiddenField ID="hidCurrentApplicantCode" runat="server" />
<asp:HiddenField ID="hidCurrentCompanyCode" runat="server" />



<div class="row searchPanel">
<div class="col-sm-12">
    <div class="col-sm-2">
        <span data-lang="CompanyRefTitle">Company</span>
    </div>
    <div class="col-sm-2">
        <span data-lang="GenderRefTitle">Gender</span>
    </div>
    <div class="col-sm-2">
        <span data-lang="MaritalStatusRefTitle">Marital Status</span>
    </div>
    <div class="col-sm-2">
       <span data-lang="AccountStatusRefTitle">Account Status</span>
    </div>
    <div class="col-sm-2">
        <span data-lang="FlagStatusRefTitle">Flag</span>
    </div>
</div>
<div class="col-sm-12">
    <div class="col-sm-2">
        <asp:DropDownList ID="ddCompany" 
                            runat="server"  
                            DataTextField="Name"
                            CssClass="form-control" 
                            onchange="loadData();"
                            DataValueField="Code" 
                            OnDataBound="ddDatabound" />
    </div>
    <div class="col-sm-2">
        <asp:DropDownList ID="ddGender" 
                            runat="server"
                            onchange="loadData();"
                            CssClass="form-control"
            >
            <asp:ListItem Value="" Text="Select an Item" />
            <asp:ListItem Value="Male" Text="Male" />
            <asp:ListItem Value="Female" Text="Female" />
        </asp:DropDownList>
    </div>
    <div class="col-sm-2">
        <asp:DropDownList ID="ddMaritalStatus" 
                            runat="server" 
                            onchange="loadData();"
                            CssClass="form-control"
            >
            <asp:ListItem Value="" Text="Select an Item" />
            <asp:ListItem Value="Single" Text="Single" />
            <asp:ListItem Value="Married" Text="Married" />
            <asp:ListItem Value="Divorce" Text="Divorced" />
        </asp:DropDownList>            
    </div>
    <div class="col-sm-2">
        <asp:DropDownList ID="ddAccountStatus" 
                            runat="server"
                            onchange="loadData();"
                            CssClass="form-control"
            >
            <asp:ListItem Value="" Text="Select an Item" />
            <asp:ListItem Value="Verified" Text="Verified" />
            <asp:ListItem Value="UnVerified" Text="Unverified" />
        </asp:DropDownList>
            
    </div>
    <div class="col-sm-2">
        <asp:DropDownList ID="ddFlag" 
                            runat="server" 
                            onchange="loadData();"
                            CssClass="form-control"
            >
            <asp:ListItem Value="" Text="All" />
            <asp:ListItem Value="GreenList" Text="Green List" />
            <asp:ListItem Value="BlackList" Text="Black List" />
        </asp:DropDownList>
    </div>
</div>
</div>
<div id="listContainer">
    <div class="row">
        <div class="col-md-9 col-sm-9">

        </div>
        <div class="col-md-3 col-sm-3">
            <input class="search form-control" placeholder="Search" />
        </div>
    </div>   
    <ul class="list">           
    <asp:Repeater ID="repApplicantList"
                  runat="server"
                  OnItemDataBound="repApplicantList_ItemDataBound"
                  >
        <ItemTemplate>
            <li>
            <div class="row jobSection">                  
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-2 col-sm-2 text-center">
                        <img class="img-thumbnail" onerror='imgError(this);' id="imgAvatar" runat="server"  width="304" height="236"/>
                    </div>
                    <div class="col-md-8 col-sm-8">
                        <div class="col-md-8 col-sm-8">
                            <a href="#" onclick='showApplicantProfile("<%# Eval("Code") %>"); return false;'><h5 class="capitalLetter name"><%# Eval("Name") %></h5></a>
                        </div>
                        <div class="col-md-4 col-sm-4 text-muted">
                            <i class="fa fa-clock-o"></i><%# Eval("RegisteredDate", "{0:dd/MM/yyyy}") %>   
                        </div>
                        <div class="col-md-12 col-sm-12">
                            <%# Eval("GenderSpecification") %>, <%# Eval("ApplicantAge") %> years old, <%# Eval("MaritalStatusSpecification") %>
                        </div>
                        <div class="col-md-8 col-sm-8"><i class="fa fa-envelope"></i><%# Eval("Email") %></div>
                            <div class="col-md-4 col-sm-4">
                                <i class="fa fa-file"></i><span data-lang="ReferedTitle">Refered: </span> <span class="link" onclick='showApplicantReferenceList("<%# Eval("Code") %>");' data-key='<%# Eval("Code") %>'><asp:Literal ID="litReferalCount" runat="server" /></span> <span data-lang="RefTimesTitle"> times</span> 
                            </div>
                        <div class="col-md-12 col-sm-12"><i class="fa fa-phone"></i><%# Eval("Phone") %></div>
                        <div class="col-md-12 col-sm-12"><i class="fa fa-book"></i><%# Eval("ApplicantLastEducationInfo") %></div>
                        <div class="col-md-12 col-sm-12"><i class="fa fa-briefcase"></i><%# Eval("ApplicantLastExperienceInfo") %></div>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <div class="block center">
                            <button onclick='showFlagForm("<%# Eval("Code") %>"); return false;' class="btn btn-default halfButton"><i class="fa fa-thumbs-o-up"></i></button>
                            <button onclick='showFlagForm("<%# Eval("Code") %>"); return false;' class="btn btn-default halfButton"><i class="fa fa-thumbs-o-down"></i></button>
                        </div>
                        <div id="referencedResult" runat="server" class="block center">
                             <asp:HiddenField ID="ApplicantCode" runat="server" Value='<%# Eval("Code") %>' />
                             <asp:HiddenField ID="RefVacancyCode" runat="server" Value='<%# Eval("RefVacancyCode") %>' />
                            <button title="Test Results" class="btn btn-default halfButton btnTestResult" onclick='showTestResultForm("<%# Eval("Code") %>", "<%# Eval("RefVacancyCode") %>"); return false;'><i class="fa fa-clipboard"></i></button>
                            <button title="Comments" class="btn btn-default halfButton btnIssueComments" onclick='showApplicantCommentList("<%# Eval("Code") %>", "<%# Eval("RefVacancyCode") %>"); return false;'><i class="fa fa-comments-o"></i></button>
                        </div> 
                        <div id="flagGreen" visible="false" runat="server" class="block center">
                            <h1><i class="fa fa-flag text-success"></i></h1>
                            <span data-lang="MarkedByRefTitle">Marked By: </span> <%# Eval("FlagIssuedByName") %><br />
                            <span data-lang="ReasonRefTitle">Reason : </span> <%# Eval("FlagNotes") %>
                        </div>
                        <div id="flagBlack" visible="false" runat="server" class="block center">
                            <h1><i class="fa fa-flag text-danger"></i></h1>
                            <span data-lang="MarkedByRefTitle">Marked By: </span>  <%# Eval("FlagIssuedByName") %><br />
                            <span data-lang="ReasonRefTitle">Reason : </span> <%# Eval("FlagNotes") %>
                        </div>
                    </div>
                </div>
            </div>
            </li>
        </ItemTemplate>
    </asp:Repeater>
    </ul>
    <ul style="list-style-type: none">
        <li>
            <div class="col-sm-6">
                <ul class="pagination">
                
                </ul>
            </div>
            <div class="col-sm-6">
                <div style="margin: 20px 0">
                    <div class="col-sm-3" style="padding-right: 0px"><input type="text" class="form-control" id="go-to"/></div>
                    <div class="col-sm-3" style="padding-left: 0px"><button type="button" class="btn btn-primary btnGo">Go</button></div>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="modal fade" id="applicantProfileForm">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
        <div class="modal-body">
            <div class="row">                
                <div class="col-md-12 col-sm-12">
                    <div class="row jobSection" style="border-top: none;">                    
                        <div class="col-md-2 col-sm-2">
                            <img class="img-circle" onerror='imgError(this);' id="imgAvatar"  width="304" height="236"/>
                        </div>
                        <div class="col-md-10 col-sm-10">
                            <div class="block capitalLetter"><h3><span id="spApplicantName"></span></h3></div>
                            <div class="block"><span id="spApplicantEmail"></span></div>
                            <div class="block"><span id="spApplicantPhone"></span></div>
                            <div class="block"><span id="spGenderSpecification"></span>, <span id="spMaritalSpecification"></span></div>
                        </div>                               
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="AddressInfoRefTitle">Address Information</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10">
                            <div class="block"><span id="spApplicantAddress"></span></div>
                            <div class="block"><span id="spApplicantCity"></span></div>
                            <div class="block"><span id="spApplicantCountry"></span></div>
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="CareerRefTitle">Career & Placements</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10">
                            <div class="block"><span data-lang="ExpectedSalaryRefTitle">Expected Salary: </span> <span id="spApplicantSalary"></span></div>
                            <div class="block"><span data-lang="PriorNotifRefTitle">Prior Notification Period: </span> <span id="spApplicantPriorNotification"></span> <span data-lang="MonthRefTitle"> month(s)</span></div>
                            <div class="block"><span data-lang="WillingAssignRefTitle">Willing to be assigned in difference cities in Indonesia? </span> <span id="spApplicantAssignedAnywhere"></span></div>
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="EduInfoRefTitle">Education Information</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10" id="containerEducationList">
                
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="ExperiencesRefTitle">Experiences</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10" id="containerExperienceList">
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="SkillsRefTitle">Skills</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10" id="containerSkillList">
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="HobbyRefTitle">Hobbies & Interest</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10" id="containerHobbyInterestList">
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="AwardsRefTitle">Awards</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10" id="containerAwardList">
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="TrainingsRefTitle">Trainings</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10" id="containerTrainingList">
                        </div>
                    </div>

                    <div class="row jobSection">
                        <div class="col-md-2 col-sm-2">
                            <h4 data-lang="AttachmentsRefTitle">Attachments</h4>
                        </div>                    
                        <div class="col-md-10 col-sm-10" id="containerAttachmentList">
                        </div>
                    </div>
        
                </div>
            </div>                       
            
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="flagApplicantForm">
    <div class="modal-dialog">
		<div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h1 class="modal-title" data-lang="FlagAppRefTitle">Flag Applicant</h1>
		</div>
        <div class="modal-body">
            <div class="row">                
                <div class="col-sm-4"><strong><span data-lang="ListTypeRefTitle">List Type</span></strong></div>
                <div class="col-sm-8">
                    <asp:DropDownList ID="ddListType"
                                      runat="server" 
                                      CssClass="form-control"
                                      >
                        <asp:ListItem Value="GreenList" Text="Green List" />
                        <asp:ListItem Value="BlackList" Text="Black List" />
                    </asp:DropDownList>                                        
                </div>
            </div>                       
            <div class="row">                
                <div class="col-sm-4"><strong><span data-lang="CommentsRefTitle">Comments</span></strong></div>
                <div class="col-sm-8">
                    <asp:TextBox ID="tbDescription" runat="server"
                                    MaxLength="200"
                                    CssClass="form-control"
                                    />
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" id="btnSubmitFlag" runat="server" onserverclick="btnSubmitFlag_ServerClick">Submit</button>
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="applicantApplicationForm">
    <div class="modal-dialog modal-lg">
		<div class="modal-content">
         <div class="modal-header">
			<h1 class="modal-title" data-lang="ReferenceHistoryTitle">Reference History</h1>
		</div>
            <div class="modal-body" id="applicationContainer">
            </div>
        </div>
        </div>
    </div>

    <div class="modal fade" id="applicantTestResultForm">
    <div class="modal-dialog">
		<div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h1 class="modal-title" data-lang="TestResultsRefTitle">Test Results</h1>
		</div>
        <div class="modal-body" id="resultContainer">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
        </div>
        </div>
    </div>
</div>

    <div class="modal fade" id="applicantCommentForm">
    <div class="modal-dialog">
		<div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h1 class="modal-title" data-lang="CommentsResultRefTitle">Comments</h1>
		</div>
        <div class="modal-body" id="commentContainer">           
        </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
        </div>
        </div>
    </div>
</div>    


</asp:Content>

