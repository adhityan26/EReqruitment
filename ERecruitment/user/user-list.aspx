﻿<%@ Page Title="User List" Language="C#" MasterPageFile="~/masters/userandroles.Master" AutoEventWireup="true" CodeBehind="user-list.aspx.cs" Inherits="ERecruitment.user_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">


    $(document).ready(function () {

        $("#liUsers").addClass("active");
    });

    $(document).ready(function () {

        loadData();
    });

    $('body').on('click', '.delete', function () {

        var code = $(this).attr('name');
        confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
            $.ajax({
                url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveUser&id=" + code,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                }
            });
        });
    });



    // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
    $('body').on('click', '.edit', function () {

        var code = $(this).attr('name');
        showDataForm(code);

    });


    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [2] }

            ],
            "oLanguage":
                               { "sSearch": "Search By Name" },

            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetUserList"

        });
    }


    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function showForm(code) {

        showDataForm(code);
    }

    function Add() {

        showDataForm("0");
    }

    function showDataForm(dataId) {

        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetUser&id=" + dataId;

        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("formDataFrameModel", dataModel);

                if (dataModel["RoleCode"] != null) {
                    // assign roles
                    var rolecodes = dataModel["RoleCode"].split(",");

                    for (var i = 0; i < rolecodes.length; i++) {
                        $("#<%= listRoles.ClientID %> option[value=" + rolecodes[i] + "]").attr('selected', true).trigger("chosen:updated");
                    }
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#formDataFrameModel').modal('show');
    }

    function updateDataForm() {

        var form = "formDataFrameModel";
        var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateUser";
        var formData = {};

        if (!getNodeData(form, formData)) return false;

        // validate email input
        if(!ValidateEmail($("#userName").val())) return false;

        /*$.post(actionUrl, formData).done(function (data) {
            alert(JSON.stringify(data));
            refresh();
            $('#formDataFrameModel').modal('hide');

        });*/

        $.ajax({
            url: actionUrl,
            type: "post",
            contentType: "application/x-www-form-urlencoded",
            dataType: "json",
            data: formData,
            statusCode: {
                666: function (response) {
                    if (response.responseText != null){
                        if (response.responseText.indexOf("NIK") !== -1) {
                            showNotification(response.responseText);
                        } else {
                            showNotification("Email address already used");
                        }
                    }
                },
                200: function (response) {
                    refresh();
                    $('#formDataFrameModel').modal('toggle');
                },
            },
            success: function (response) {
                refresh();
                $('#formDataFrameModel').modal('toggle');
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(textStatus, errorThrown);
            }
        });
    }

    </script>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
        </div>
    </div>    
           
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                    <th><span data-lang="UsersSetupTitle">Name</span></th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
       

    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
            <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h1 class="modal-title center" data-lang="UsersSetupCaptionTitle">User</h1>
		    </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                           <span data-lang="NIKUserSetupInputTitle">NIK</span><span class="requiredmark">*</span>       
                        <input type="hidden" data-attribute="Code" />
                    </div>
                    <div class="col-md-5 col-sm-5"> 
                        <input class="form-control" type="text" required="" data-attribute="NIK"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="EmailUserSetupInputTitle">Email</span><span class="requiredmark">*</span>       
                        <input type="hidden" data-attribute="Code" />
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" type="text" required="" id="userName" data-attribute="UserName"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="NameUserSetupInputTitle">Name</span><span class="requiredmark">*</span>
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" type="text" required="" data-attribute="Name"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="PasswordUserSetupInputTitle">Password</span><span class="requiredmark">*</span>
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" type="password" required="" data-attribute="Password"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="RolesUserSetupInputTitle">Roles</span><span class="requiredmark">*</span>
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <asp:ListBox ID="listRoles" SelectionMode="Multiple" runat="server" CssClass="form-control" DataValueField="RoleCode" DataTextField="Name" data-attribute="RoleCode" />  
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
            </div>
            </div>
        </div>
    </div>
</asp:Content>
