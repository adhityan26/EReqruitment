﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;


namespace ERecruitment
{
    public partial class reference_list : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                #region check company role list

                UserAccounts getUserAccount = CurrentUser;

                if (CurrentUser.IsAdmin)
                {
                    ddCompany.DataSource = ERecruitmentManager.GetCompanyList();
                    ddCompany.DataBind();
                }
                else
                {
                    if (!string.IsNullOrEmpty(MainRole.CompanyCodes))
                    {
                        string[] userCompanyCodes = MainRole.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        List<Companies> getCompanyAccessList = ERecruitmentManager.GetCompanyListByCodes(userCompanyCodes);
                        List<Companies> companyList = new List<Companies>();
                        foreach (Companies item in getCompanyAccessList)
                        {
                            if (companyList.Contains(item))
                                continue;
                            else
                                companyList.Add(item);
                        }
                        ddCompany.DataSource = companyList;
                        ddCompany.DataBind();
                    }
                }

                #endregion

                Utils.SelectItem<string>(ddGender, Utils.GetQueryString<string>("gender"));
                Utils.SelectItem<string>(ddMaritalStatus, Utils.GetQueryString<string>("maritalStatus"));
                Utils.SelectItem<string>(ddAccountStatus, Utils.GetQueryString<string>("status"));
                Utils.SelectItem<string>(ddCompany, Utils.GetQueryString<string>("companyCode"));
                Utils.SelectItem<string>(ddFlag, Utils.GetQueryString<string>("flag"));
                hidCurrentCompanyCode.Value = MainCompany.Code;
                List<string> companyCodeList = new List<string>();
                if (string.IsNullOrEmpty(Utils.GetQueryString<string>("companyCode")))
                {
                    foreach (ListItem item in ddCompany.Items)
                    {
                        if (!string.IsNullOrEmpty(item.Value))
                            companyCodeList.Add(item.Value);
                    }
                }
                else
                    companyCodeList.Add(Utils.GetQueryString<string>("companyCode"));

                repApplicantList.DataSource = ApplicantManager.GetReferencedApplicantList(MainCompany.Code,
                                                                                          Utils.GetQueryString<string>("status"),
                                                                                          Utils.GetQueryString<string>("gender"),
                                                                                          Utils.GetQueryString<string>("maritalStatus"),
                                                                                          Utils.GetQueryString<string>("flag"));
                repApplicantList.DataBind();

            }
        }

        #region support

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            if (dd != null)
            {
                dd.Items.Insert(0, new ListItem("All", string.Empty));
            }
        }

        #endregion

        protected void repApplicantList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            Applicants applicant = e.Item.DataItem as Applicants;
            HtmlImage imgAvatar = e.Item.FindControl("imgAvatar") as HtmlImage;

            if (applicant.Photo == null)
            {
                if (applicant.SocMedName == "facebook")
                    imgAvatar.Src = "http://graph.facebook.com/" + applicant.SocMedID + "/picture?type=large";
                else if (applicant.SocMedName == "linkedin")
                    imgAvatar.Src = applicant.LinkedInImageUrl;
                else
                    imgAvatar.Src = "../assets/images/avatar.png";
            }
            else
                imgAvatar.Src = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + applicant.Code;

            if (applicant.FlagStatus == "GreenList")
            {
                HtmlGenericControl flagGreen = e.Item.FindControl("flagGreen") as HtmlGenericControl;
                flagGreen.Visible = true;
            }
            if (applicant.FlagStatus == "BlackList")
            {
                HtmlGenericControl flagBlack = e.Item.FindControl("flagBlack") as HtmlGenericControl;
                flagBlack.Visible = true;
            }

            Literal litReferalCount = e.Item.FindControl("litReferalCount") as Literal;
            litReferalCount.Text = ApplicantManager.CountReferencedApplicantListBasedAppCode(applicant.Code).ToString();
        }


        protected void btnSubmitFlag_ServerClick(object sender, EventArgs e)
        {
            UserAccounts getUser = Session["currentuser"] as UserAccounts;
            string[] applicantCodes = hidApplicantCodeFlag.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < applicantCodes.Length; i++)
            {
                ApplicantFlagStatuses flagRecord = new ApplicantFlagStatuses();
                flagRecord.ApplicantCode = applicantCodes[i];
                flagRecord.FlagStatus = Utils.GetSelectedValue<string>(ddListType);
                flagRecord.Notes = tbDescription.Text;
                flagRecord.IssuedByCode = getUser.Code;
                flagRecord.IssuedDate = DateTime.Now;

                ApplicantManager.AddApplicantFlag(flagRecord);
            }

            JQueryHelper.InvokeJavascript("loadData();", Page);
        }
    }
}