﻿<%@ Page Language="C#" MasterPageFile="~/masters/popupform.Master" AutoEventWireup="true" CodeBehind="user-form.aspx.cs" Inherits="ERecruitment.user_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <div class="row">
          <div class="col-sm-12">
            <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label runat="server" id="lbUserForm_Email">Email</label>
                    </div>
                    <div class="col-sm-3">
                        
                                        <input placeholder="Email Address (format: mail@example.com)"
                                               data-container="body" runat="server"
                                               data-placement="top"
                                               data-template="&lt;div class=&quot;popover&quot; role=&quot;tooltip&quot;&gt;&lt;div class=&quot;arrow&quot;&gt;&lt;/div&gt;&lt;div class=&quot;popover-body&quot;&gt;&lt;div class=&quot;popover-content-override&quot;&gt;Maksud Anda&lt;/div&gt; &lt;span class=&quot;popover-content bold&quot;&gt;&lt;/span&gt;&lt;span&gt;?&lt;/span&gt;&lt;div class=&quot;popover-content-override&quot;&gt;&lt;a onClick=&quot;$('#email').val($('.popover-content').text());$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-check fa-stack-1x fa-inverse&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;a onClick=&quot;$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle-thin fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-times fa-stack-1x text-muted&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;"
                                               id="tbEmailAddress"
                                               ng-model="registerFormData.email"
                                               form-field="registerForm" type="text"
                                               pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
                                               required
                                               class="form-control auto-lower-case auto-no-space suggest-email ng-pristine ng-untouched 
                                            ng-invalid ng-invalid-email ng-invalid-required ng-invalid-email-available" />
       
                   </div>
                      </div>
              </div>

               <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label runat="server" id="lbUserForm_Name">Name</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbName"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                                        
                    </div>  
                      </div>
              </div>
              
              <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label runat="server" id="lbUserForm_Password">Password</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbPassword"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="password" />
                                        
                    </div>  
                      </div>
              </div>
               <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label runat="server" id="lbUserForm_Position">Position</label>
                    </div>
                    <div class="col-sm-3">
                        <input disallowed-chars="[^a-zA-Zs ]+" id="tbPosition"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                                        
                    </div>  
                      </div>
              </div>
             

              <div class="row">
                  <div class="col-sm-12">
                    <div class="col-sm-3">
                        <label runat="server" id="lbUserForm_Roles">Roles</label>
                    </div>
                    <div class="col-sm-9">
                    <asp:CheckBoxList ID="cbRoles" DataSourceID="odsRoles"
                                     DataValueField="RoleCode" DataTextField="Name"
                                     RepeatColumns="3"
                                     RepeatDirection="Horizontal"
                                     runat="server">

                    </asp:CheckBoxList>

                    </div>  
                      </div>
              </div>
             
             <div class="row">
                 <div class="col-sm-3">
                
                    </div>
              <div class="col-sm-3">
                   <button class="btn btn-primary"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
                         Save
                    </button>
               </div>
              </div>
             </div>
           </div>
<asp:ObjectDataSource ID="odsRoles" DataObjectTypeName="ERecruitment.Domain.Roles"
                      runat="server"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetRoleList"
    />
 </asp:Content>