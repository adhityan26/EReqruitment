﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;
namespace ERecruitment
{
    public partial class role_form: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
                    formData.ChangeMode(FormViewMode.Edit);
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            if (formData.CurrentMode == FormViewMode.Insert)
                formData.InsertItem(true);
            else
                formData.UpdateItem(true);
        }

        protected void formData_ItemInserting(object sender, FormViewInsertEventArgs e)
        {
            Repeater repCompany = formData.FindControl("repCompany") as Repeater;
            List<RoleCompanyAccessList> accessList = new List<RoleCompanyAccessList>();
            foreach (RepeaterItem item in repCompany.Items)
            {
                CheckBox cbCheck = item.FindControl("cbCheck") as CheckBox;
                HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                if (cbCheck.Checked)
                {
                    RoleCompanyAccessList access = new RoleCompanyAccessList();
                    access.CompanyCode = hidCode.Value;

                    accessList.Add(access);
                }
            }
            e.Values["Active"] = true;
            e.Values["CompanyAccess"] = accessList;
        }

        protected void formData_DataBound(object sender, EventArgs e)
        {
            if (formData.DataItem != null)
            {
                Roles data = formData.DataItem as Roles;
                List<RoleCompanyAccessList> accessList = ERecruitmentManager.GetCompanyAccessList(data.RoleCode);
                Repeater repCompany = formData.FindControl("repCompany") as Repeater;
                foreach (RepeaterItem item in repCompany.Items)
                {
                    CheckBox cbCheck = item.FindControl("cbCheck") as CheckBox;
                    HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                    RoleCompanyAccessList access = accessList.Find(delegate (RoleCompanyAccessList temp) { return temp.CompanyCode == hidCode.Value; });
                    if (access != null)
                    {
                        cbCheck.Checked = true;
                    }
                }
            }
        }

        protected void formData_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {
            Repeater repCompany = formData.FindControl("repCompany") as Repeater;
            List<RoleCompanyAccessList> accessList = new List<RoleCompanyAccessList>();
            foreach (RepeaterItem item in repCompany.Items)
            {
                CheckBox cbCheck = item.FindControl("cbCheck") as CheckBox;
                HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                if (cbCheck.Checked)
                {
                    RoleCompanyAccessList access = new RoleCompanyAccessList();
                    access.CompanyCode = hidCode.Value;

                    accessList.Add(access);
                }
            }

            e.NewValues["CompanyAccess"] = accessList;
        }

        protected void odsData_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
                JQueryHelper.InvokeJavascript("showNotification('Role saved');window.close();window.opener.refresh();", Page);
        }

        protected void odsData_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
                JQueryHelper.InvokeJavascript("showNotification('Role updated');window.close();window.opener.refresh();", Page);
        }
    }
}