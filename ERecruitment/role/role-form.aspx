﻿<%@ Page Language="C#" MasterPageFile="~/masters/popupform.Master" AutoEventWireup="true" CodeBehind="role-form.aspx.cs" Inherits="ERecruitment.role_form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function toggleCheckAllActions()
        {
            $(".cbActions").children().prop("checked", true);
        }
    </script>
     <div class="row">
          <div class="col-sm-12">

              <asp:FormView ID="formData"
                            runat="server"
                            DefaultMode="Insert"
                            RenderOuterTable="false"
                            DataSourceID="odsData"
                            OnItemInserting="formData_ItemInserting"
                            OnDataBound="formData_DataBound"
                            OnItemUpdating="formData_ItemUpdating"
                            DataKeyNames="RoleCode,Active,Name,AccessSetting,ImportBulkData,CreateJob,
                                        AccessCompensationData,DeleteJob,SchedulingFunctionality,
                                ChangeApplicantStatus,MassScheduling,UpdateApplicantApplicationData,SearchApplicant,ExportBulkData"                            
                            >
                  <InsertItemTemplate>
                      
                      <div class="row">
                            <div class="col-sm-3">
                                <label runat="server" id="lbRoleForm_RoleName">Role Name</label>
                            </div>
                            <div class="col-sm-3">
                                  <input disallowed-chars="[^a-zA-Zs ]+" id="tbName" value='<%#Bind("Name") %>'
                                     name="name" ng-model="registerFormData.name" form-field="registerForm"
                                         ng-minlength="3" min-length="3" required="" runat="server"
                            class="form-control" type="text" />
                                        
                            </div> 
                      </div>                      
                      <div class="row">
                          <div class="col-sm-12">
                              <hr />
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                <label runat="server" id="lbRoleForm_Companies">Companies</label>
                          </div>
                          <asp:Repeater ID="repCompany"
                                        runat="server"
                                        DataSourceID="odsCompany"
                                        >
                            <ItemTemplate>
                                <div class="col-sm-3">
                                    <asp:CheckBox ID="cbCheck"
                                                    runat="server"
                                                    />
                                    <%# Eval("Name") %>
                                    <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("Code") %>' />
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="col-sm-3">
                                    <asp:CheckBox ID="cbCheck"
                                                    runat="server"
                                                    />
                                    <%# Eval("Name") %>
                                    <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("Code") %>' />
                                </div>
                                </div>
                                <div class="row">
                                    
                                <div class="col-sm-3">
                                    
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                      </div>                      
                      <div class="row">
                          <div class="col-sm-12">
                              <hr />
                          </div>
                      </div>                      
                      <div class="row">
                          <div class="col-sm-3">
                                <label runat="server" id="lbRoleForm_Actions">Actions</label>
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox10"
                                             runat="server"
                                             onchange="toggleCheckAllActions();"
                                             />
                               All 
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>                          
                          <div class="col-sm-3">
                               <asp:CheckBox ID="cbAccessSetting"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("AccessSetting") %>'
                                             />
                               Access Setting 
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="cbImportBulkData"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("ImportBulkData") %>'
                                             />
                               Import Bulk Data
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox1"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("CreateJob") %>'
                                             />
                               Create Job Req 
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox2"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("AccessCompensationData") %>'
                                             />
                               Access Compensation Data
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox3"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("DeleteJob") %>'
                                             />
                               Delete Job Req
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox4"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("SchedulingFunctionality") %>'
                                             />
                               Scheduling Functionality
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox5"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("ChangeApplicantStatus") %>'
                                             />
                               Change Application Status
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox6"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("MassScheduling") %>'
                                             />
                               Mass Scheduling
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox7"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("UpdateApplicantApplicationData") %>'
                                             />
                               Update Application Application Data
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox8"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("SearchApplicant") %>'
                                             />
                               Search Applicant
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox9"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("ExportBulkData") %>'
                                             />
                               Export Bulk Data
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox11"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("Approval") %>'
                                             />
                               Approval
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox12"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("AccessCSSTheme") %>'
                                             />
                               Access CSS Setting
                          </div>
                      </div>                           
                      <div class="row">
                         <div class="col-sm-3">
                
                            </div>
                         <div class="col-sm-3">
                           <button class="btn btn-primary"  runat="server" id="Button1"
                                 onserverclick="btnSave_ServerClick" >
                               Save
                                </button>
                       </div>
                  </InsertItemTemplate>
                  <EditItemTemplate>
                      
                      <div class="row">
                            <div class="col-sm-3">
                                <label runat="server" id="lbRoleForm_RoleName">Role Name</label>
                            </div>
                            <div class="col-sm-3">
                                 <asp:TextBox ID="tbRoleName"
                                              runat="server"
                                              CssClass="form-control"
                                              Text='<%# Bind("Name") %>'
                                              />
                            </div> 
                      </div>                      
                      <div class="row">
                          <div class="col-sm-12">
                              <hr />
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                <label runat="server" id="lbRoleForm_Companies">Companies</label>
                          </div>
                          <asp:Repeater ID="repCompany"
                                        runat="server"
                                        DataSourceID="odsCompany"
                                        >
                            <ItemTemplate>
                                <div class="col-sm-3">
                                    <asp:CheckBox ID="cbCheck"
                                                    runat="server"
                                                    />
                                    <%# Eval("Name") %>
                                    <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("Code") %>' />
                                </div>
                            </ItemTemplate>
                            <AlternatingItemTemplate>
                                <div class="col-sm-3">
                                    <asp:CheckBox ID="cbCheck"
                                                    runat="server"
                                                    />
                                    <%# Eval("Name") %>
                                    <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("Code") %>' />
                                </div>
                                </div>
                                <div class="row">
                                    
                                <div class="col-sm-3">
                                    
                                </div>
                            </AlternatingItemTemplate>
                        </asp:Repeater>
                      </div>                      
                      <div class="row">
                          <div class="col-sm-12">
                              <hr />
                          </div>
                      </div>                      
                      <div class="row">
                          <div class="col-sm-3">
                                <label runat="server" id="lbRoleForm_Actions">Actions</label>
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox10"
                                             runat="server"
                                             onchange="toggleCheckAllActions();"
                                             />
                               All 
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>                          
                          <div class="col-sm-3">
                               <asp:CheckBox ID="cbAccessSetting"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("AccessSetting") %>'
                                             />
                               Access Setting 
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="cbImportBulkData"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("ImportBulkData") %>'
                                             />
                               Import Bulk Data
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox1"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("CreateJob") %>'
                                             />
                               Create Job Req 
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox2"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("AccessCompensationData") %>'
                                             />
                               Access Compensation Data
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox3"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("DeleteJob") %>'
                                             />
                               Delete Job Req
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox4"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("SchedulingFunctionality") %>'
                                             />
                               Scheduling Functionality
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox5"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("ChangeApplicantStatus") %>'
                                             />
                               Change Application Status
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox6"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("MassScheduling") %>'
                                             />
                               Mass Scheduling
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox7"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("UpdateApplicantApplicationData") %>'
                                             />
                               Update Application Application Data
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox8"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("SearchApplicant") %>'
                                             />
                               Search Applicant
                          </div>
                      </div>
                      <div class="row">
                          <div class="col-sm-3">
                                
                          </div>
                          <div class="col-sm-3">
                               <asp:CheckBox ID="CheckBox9"
                                             runat="server" CssClass="cbActions"
                                             Checked='<%# Bind("ExportBulkData") %>'
                                             />
                               Export Bulk Data
                          </div>
                      </div> 
                      <div class="row">
                         <div class="col-sm-3">
                
                            </div>
                         <div class="col-sm-3">
                           <button class="btn btn-primary"  runat="server" id="Button1"
                                 onserverclick="btnSave_ServerClick" >
                               Save
                                </button>
                       </div>
                  </EditItemTemplate>
              </asp:FormView>

          </div>
     </div>



<asp:ObjectDataSource ID="odsData"
                      runat="server"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      DataObjectTypeName="ERecruitment.Domain.Roles"
                      InsertMethod="SaveRole"
                      UpdateMethod="UpdateRole"
                      SelectMethod="GetRole"
                      OnInserted="odsData_Inserted"
                      OnUpdated="odsData_Updated"
    >
    <SelectParameters>
        <asp:QueryStringParameter Name="code" QueryStringField="code" />
    </SelectParameters>
</asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsCategory" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetVacancyCategoryList"
                      />

<asp:ObjectDataSource ID="odsCompany" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Companies"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCompanyList"
                      />
    

 </asp:Content>
