﻿<%@ Page Title="Role List" Language="C#" MasterPageFile="~/masters/userandroles.Master" AutoEventWireup="true" CodeBehind="role-list.aspx.cs" Inherits="ERecruitment.role_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">


    $(document).ready(function () {

        $("#liRoles").addClass("active");

    });

    $(document).ready(function () {

        loadData();
    });


    $('body').on('click', '.delete', function () {

        var code = $(this).attr('name');
        confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
            $.ajax({
                url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveRole&id=" + code,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                }
            });
        });
    });



    // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
    $('body').on('click', '.edit', function () {

        var code = $(this).attr('name');
        showDataForm(code);

    });


    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [2] }

            ],
            "oLanguage":
                               { "sSearch": "Search By Name" },

            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetRoleList"

        });
    }

    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function showForm(code) {

        showDataForm(code);
    }

    function Add() {

        showDataForm("0");
    }

    function showDataForm(dataId) {

        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetRole&id=" + dataId;

        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("formDataFrameModel", dataModel);
                var companycodes = dataModel["CompanyCodes"].split(",");
               for(var i = 0; i< companycodes.length; i++)
                {                    
                    $("#<%= listCompanies.ClientID %> option[value=" + companycodes[i] + "]").attr('selected', true).trigger("chosen:updated");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#formDataFrameModel').modal('show');
    }

    function updateDataForm() {

        var form = "formDataFrameModel";
        var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateRole";
        var formData = {};
        
        if (!getNodeData(form, formData)) return false;
        if ($("#ContentPlaceHolder1_listCompanies").val() == "") {
            $(".default").focus();
            return false;
        }

        $.post(actionUrl, formData).done(function (data) {

            refresh();
            $('#formDataFrameModel').modal('hide');

        });
    }

    function checkAll() {
        var isAllChecked = $('#All').prop('checked');
        if (isAllChecked)
            $('input:checkbox').prop('checked', true);
        else
            $('input:checkbox').prop('checked', false);
    }

    function releaseBox() {
        $('#All').prop('checked', false);
    }
    </script>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
        </div>
    </div>    
           
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th><span data-lang="RoleUserTitle">Name</span></th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
       

    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
            <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h1 class="modal-title center" data-lang="RoleUserCaptionTitle">Role</h1>
		    </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="RoleNameUserInputTitle">Role Name</span><span class="requiredmark">*</span>       
                        <input type="hidden" data-attribute="RoleCode" />
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" type="text" required="" data-attribute="Name"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="ComAccessUserInputTitle">Company Access</span><span class="requiredmark">*</span>   
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <asp:ListBox ID="listCompanies" SelectionMode="Multiple" runat="server" CssClass="form-control required" DataValueField="Code" DataTextField="Name" data-attribute="CompanyCodes" />                                    
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" id="All" onclick="checkAll();"> All Access
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="AccessCSSTheme"/>
                        <span data-lang="CSSThemeUserInputTitle">Access CSS Theme</span> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="AccessSetup"/>
                        <span data-lang="AccessSetupUserInputTitle">Access Setup</span>
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="AccessSetting" />
                        <span data-lang="AccessSettingUserInputTitle">Access Setting</span> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="AccessReport"/>
                        <span data-lang="AccessReportUserInputTitle">Access Report</span>
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="AccessApplicant" />
                       <span data-lang="AccessApplicantUserInputTitle">Access Applicant</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="CreateJob" />
                        <span data-lang="CreateJobUserInputTitle">Create Job Req.</span>
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="PostUnPostJob" />
                        <span data-lang="PostUnPostJobUserInputTitle">Post/Unpost Job Req.</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="ArchiveJob" />
                        <span data-lang="ArchiveJobUserInputTitle">Show Closed Job Req.</span>
                    </div>
                    <div class="col-sm-6"> 
                        <input type="checkbox" onclick="releaseBox();" data-attribute="DeleteJob"/>
                         <span data-lang="DeleteJobUserInputTitle">Delete Job</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="ChangeApplicantStatus"/>
                        <span data-lang="UpdateReqProcUserInputTitle">Update Recruitment Process</span>
                     
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="DownloadDashboardData" />
                        <span data-lang="DownloadDashboardUserInputTitle">Download Dashboard Data</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="OfferingFunctionality"/>
                        <span data-lang="IssueOfferingUserInputTitle">Issue Offering</span> 
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="SchedulingFunctionality" />
                         <span data-lang="IssueInvitUserInputTitle">Issue Invitation</span> 
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="RankFunctionality"/>
                        <span data-lang="RankAppUserInputTitle">Rank Applicant</span>
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="ReferFunctionality" />
                        <span data-lang="ReferAppUserInputTitle">Refer Applicant</span>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="FlagFunctionality"/>
                         <span data-lang="FlagAppUserInputTitle">Flag Applicant</span>
                    </div>
                    <div class="col-sm-6">
                        <input type="checkbox" onclick="releaseBox();" data-attribute="DisqualifyFunctionality" />
                       <span data-lang="DisAppUserInputTitle">Disqualify Applicant</span> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
            </div>
            </div>
        </div>
    </div>        
</asp:Content>
