﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Text;
using SS.Web.UI;
using System.Web.Security;

namespace ERecruitment
{
    public partial class forgotpassword : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { }
        }
        private void SendEmail()
        {
            string urlPage = "../default.aspx";
            Applicants applicant = ERecruitment.Domain.ApplicantManager.GetVerifiedActiveApplicantByEmailAddress(tbEmail.Value);
            if (applicant != null)
            {
                UserAccounts applicantAccount = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(applicant.Code);
                applicantAccount.Password = Membership.GeneratePassword(8, 2);
                ERecruitment.Domain.AuthenticationManager.UpdateUserAccount(applicantAccount);
                if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
                {
                    Companies emailSetting = ERecruitmentManager.GetCompany(MainCompany.Code);
                    SmtpClient client = new SmtpClient();
                    client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                    client.Host = emailSetting.EmailSmtpHostAddress;
                    if (emailSetting.EmailSmtpUseSSL)
                        client.EnableSsl = true;
                    client.Timeout = 20000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                    string address = emailSetting.EmailSmtpEmailAddress;
                    string displayName = emailSetting.MailSender;

                    if (!address.Contains("@"))
                    {
                        address = displayName;
                    }
            
                    MailMessage mm = new MailMessage(new MailAddress(address,
                            displayName),
                                                                     new MailAddress(applicant.Email));
                    mm.Subject = "New password";
                    mm.Body = "Your password has been changed to: " + applicantAccount.Password;
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.IsBodyHtml = true;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    client.Send(mm);

                    JQueryHelper.InvokeJavascript("showNotification('Email has been sent');", Page);
                }
            }
            else
            {
                // try admin user
                UserAccounts applicantAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccountByEmail(tbEmail.Value);
                if (applicantAccount != null)
                {
                    applicantAccount.Password = Membership.GeneratePassword(8, 2);
                    ERecruitment.Domain.AuthenticationManager.UpdateUserAccount(applicantAccount);
                    Companies emailSetting = ERecruitmentManager.GetCompany(MainCompany.Code);
                    SmtpClient client = new SmtpClient();
                    client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                    client.Host = emailSetting.EmailSmtpHostAddress;
                    if (emailSetting.EmailSmtpUseSSL)
                        client.EnableSsl = true;
                    client.Timeout = 20000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                    string address = emailSetting.EmailSmtpEmailAddress;
                    string displayName = emailSetting.MailSender;

                    if (!address.Contains("@"))
                    {
                        address = displayName;
                    }
            
                    MailMessage mm = new MailMessage(new MailAddress(address,
                            displayName),
                                                                     new MailAddress(applicantAccount.UserName));
                    mm.Subject = "New password";
                    mm.Body = "Your password has been changed to: " + applicantAccount.Password;
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.IsBodyHtml = true;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    client.Send(mm);


                    JQueryHelper.InvokeJavascript("showNotification('Email sent, please check your email');window.close();window.location.href='" + urlPage + "';", Page);
                }
                else
                    JQueryHelper.InvokeJavascript("showNotification('User not registered');window.close();window.location.href = '" + urlPage + "';", Page);
            }
        }
        protected void btnResend_ServerClick(object sender, EventArgs e)
        {
            SendEmail();

        }
    }
}