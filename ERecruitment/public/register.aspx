﻿
<%@ Page Title="" Language="C#"MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="register.aspx.cs" Inherits="ERecruitment.register" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script type="text/javascript">
        
  function registerFaceBook()
  {
      console.log("checking facebook login status");
      FB.login(function (response) {
          
          if (response.authResponse) {
              console.log('Welcome!  Fetching your information.... ');
              FB.api('/me', { fields: 'name,email,id,birthday,gender' }, function (apiResponse) {
                  console.log('Good to see you, ' + apiResponse.name + '.');
                  
                  var name = apiResponse.name;
                  var userEmail = apiResponse.email;
                  var userId = apiResponse.id;
                  var gender = apiResponse.gender;
                  var birthday = apiResponse.birthday;

                  var param = "&name=" + name;
                  param += "&email=" + userEmail;
                  param += "&id=" + userId;
                  param += "&gender=" + gender;
                  param += "&birthday=" + birthday;
                  param += "&appName=facebook";
                  // authenticate via facebook
                  console.log("authenticating..., param:" + param);
                  $.ajax({
                      url: "../handlers/HandlerAuthentication.ashx?commandName=registersosmed" + param,
                      async: false,
                      beforeSend: function () {

                      },
                      success: function (queryResult) {
                          // redirect to user profile
                          console.log("authentication succeed...");
                          window.location.href="registration-form.aspx";
                      },
                      error: function (xhr, ajaxOptions, thrownError) {

                          showNotification(xhr.responseText);

                      }
                  });

              });
          } else {
              console.log('User cancelled login or did not fully authorize.');
          }
      }, { scope: 'public_profile,email,user_birthday', return_scopes: true });
      
  }

  function registerLinkedIn()
  {
      console.log("Checking linkedin");
      IN.User.authorize(function () {
          IN.API.Profile('me').fields([
              'id',
              'firstName',
              'lastName',
              'emailAddress'
          ]).result(function (profiles) {
              var me = profiles.values[0];
              

              var name = me.firstName + me.lastName;
              var userEmail = me.emailAddress;
              var userId = me.id;

              var param = "&name=" + name;
              param += "&email=" + userEmail;
              param += "&id=" + userId;
              param += "&appName=linkedin";
              // authenticate via linkedin
              console.log("authenticating..., param:" + param);
              $.ajax({
                  url: "../handlers/HandlerAuthentication.ashx?commandName=registersosmed" + param,
                  async: false,
                  beforeSend: function () {

                  },
                  success: function (queryResult) {
                      // redirect to user profile
                      console.log("authentication succeed...");
                      window.location.href="registration-form.aspx";
                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      showNotification(xhr.responseText);

                  }
              });
          });
      });
  }

  function registerUser() {
      // redirect to user profile
      window.location.href="registration-form.aspx";
  }

  function registerAffiliatedUser() {
      // redirect to user profile
      window.location.href = "affiliation-page.aspx";
  }

  

  $(document).keydown(function (e) {
      if (e.which == 13) {
         registerUser();
      }

  });

</script>
    
<div style="float:left;margin:5px; margin-top:14px; text-align:center; padding:15px; width:100%; height:100%;">
    <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px;">
        Register to DayaLima E-Recruitment</h1>
   <div class="row">
   
    <div class="col-md-10 col-md-offset-1">
    <button class="btn btn-lg btn-primary" onclick="registerUser(); return false;">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Register&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </button>
    </div>

    </div>
</div>

<div id="status">
</div>
</asp:Content>
