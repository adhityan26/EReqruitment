﻿<%@ Page Title="Forgot Password" Language="C#" MasterPageFile="~/masters/landing.Master" AutoEventWireup="true" CodeBehind="forgotpassword.aspx.cs" Inherits="ERecruitment.forgotpassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
<div style="float:left;margin:5px; margin-top:14px; text-align:center; padding:15px; width:100%; height:100%;">
    <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px;">
        Enter your email address and we'll send you a new password which you can change later on
    </h1>
    <div class="form-group col-md-6 col-md-offset-3">
        <input class="form-control" type="text" pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
            name="email" id="tbEmail" required="required" placeholder="Email Address (format: mail@example.com)"
            runat="server"  />
    </div>
    <div class="col-md-6 col-md-offset-3">
        <button class="btn btn-primary col-xs-12" id="btnResend" 
            runat="server" onserverclick="btnResend_ServerClick">
            Send New Password
        </button>
    </div>
</div>

</asp:Content>
