﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace ERecruitment
{
    public partial class affiliation_page : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                generateCapctha();
            }
        }

        private void generateCapctha()
        {

            try

            {

                Random random = new Random();

                string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

                StringBuilder captcha = new StringBuilder();

                for (int i = 0; i < 6; i++)

                    captcha.Append(combination[random.Next(combination.Length)]);

                Session["captcha"] = captcha.ToString();
                System.Web.UI.WebControls.Image imgCaptcha = (System.Web.UI.WebControls.Image)formProfile.FindControl("imgCaptcha");
                if (imgCaptcha != null)
                    imgCaptcha.ImageUrl = "../handlers/HandlerCaptcha.ashx?" + DateTime.Now.Ticks.ToString();

            }

            catch (Exception ex)

            {



                throw ex;

            }

        }

        protected void nextbutton_ServerClick(object sender, EventArgs e)
        {
            #region validate email

            HtmlInputControl tbEmailAddress = formProfile.FindControl("tbEmailAddress") as HtmlInputControl;
            Applicants conflictApplicant = ERecruitment.Domain.ApplicantManager.GetVerifiedActiveApplicantByEmailAddress(tbEmailAddress.Value);
            if (conflictApplicant != null)
            {
                JQueryHelper.InvokeJavascript("showNotification('Email already registered');", Page);
                return;
            }
            Applicants registeredApplicant = ERecruitment.Domain.ApplicantManager.GetUnVerifiedActiveApplicantByEmailAddress(tbEmailAddress.Value);
            if (conflictApplicant != null)
            {
                JQueryHelper.InvokeJavascript("showNotification('Email already registered, please resend verification and verify email');", Page);
                return;
            }

            #endregion

            #region validate password match

            HtmlInputControl tbPassword = formProfile.FindControl("tbPassword") as HtmlInputControl;
            HtmlInputControl tbConfirmPassword = formProfile.FindControl("tbConfirmPassword") as HtmlInputControl;
            if (tbPassword.Value != tbConfirmPassword.Value)
            {
                JQueryHelper.InvokeJavascript("showNotification('Password not match');", Page);
                return;
            }

            #endregion

            #region validate captcha

            HtmlInputControl tbCaptcha = formProfile.FindControl("tbCaptcha") as HtmlInputControl;
            string keyValidator = Session["captcha"] as string;
            if (tbCaptcha.Value != keyValidator)
            {
                JQueryHelper.InvokeJavascript("showNotification('Security code not match');", Page);
                return;
            }

            #endregion

            #region validate aggree terms

            HtmlInputCheckBox cbTOC = formProfile.FindControl("cbTOC") as HtmlInputCheckBox;
            if (!cbTOC.Checked)
            {
                JQueryHelper.InvokeJavascript("showNotification('Terms and condition not aggreed');", Page);
                return;
            }

            #endregion

            formProfile.InsertItem(true);
        }

        #region form

        protected void formProfile_ItemUpdating(object sender, FormViewUpdateEventArgs e)
        {

            HtmlInputControl tbEmailAddress = formProfile.FindControl("tbEmailAddress") as HtmlInputControl;
            HtmlInputControl tbPassword = formProfile.FindControl("tbPassword") as HtmlInputControl;
            HtmlInputControl tbConfirmPassword = formProfile.FindControl("tbConfirmPassword") as HtmlInputControl;

            HtmlInputControl tbFirstName = formProfile.FindControl("tbFirstName") as HtmlInputControl;
            HtmlInputControl tbLastName = formProfile.FindControl("tbLastName") as HtmlInputControl;
            HtmlInputControl tbPhoneNumber = formProfile.FindControl("tbPhoneNumber") as HtmlInputControl;
            HtmlInputControl tbAlternativePhoneNumber = formProfile.FindControl("tbAlternativePhoneNumber") as HtmlInputControl;


            e.NewValues["Email"] = tbEmailAddress.Value;
            e.NewValues["FirstName"] = tbFirstName.Value;
            e.NewValues["LastName"] = tbLastName.Value;
            // user account

            UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(Utils.GetQueryString<string>("code"));
            if (userAccount == null)
            {
                userAccount = new UserAccounts();

            }

            userAccount.UserName = tbEmailAddress.Value;
            userAccount.Password = tbPassword.Value;
            userAccount.IsApplicant = true;
            e.NewValues["Account"] = userAccount;

        }

        protected void formProfile_DataBound(object sender, EventArgs e)
        {
            if (formProfile.DataItem != null)
            {
                Applicants applicantUser = formProfile.DataItem as Applicants;

                HtmlSelect selectExistingApplicant = formProfile.FindControl("selectExistingApplicant") as HtmlSelect;

                HtmlInputControl tbEmailAddress = formProfile.FindControl("tbEmailAddress") as HtmlInputControl;
                HtmlInputControl tbPassword = formProfile.FindControl("tbPassword") as HtmlInputControl;
                HtmlInputControl tbConfirmPassword = formProfile.FindControl("tbConfirmPassword") as HtmlInputControl;


                HtmlInputControl tbFullName = formProfile.FindControl("tbFullName") as HtmlInputControl;
                HtmlInputControl tbIdCardNumber = formProfile.FindControl("tbIdCardNumber") as HtmlInputControl;

                HtmlInputControl tbBirthDate = formProfile.FindControl("tbBirthDate") as HtmlInputControl;
                HtmlInputControl tbBirthMonth = formProfile.FindControl("tbBirthMonth") as HtmlInputControl;
                HtmlInputControl tbBirthYear = formProfile.FindControl("tbBirthYear") as HtmlInputControl;

                HtmlInputRadioButton rdGenderMale = formProfile.FindControl("rdGenderMale") as HtmlInputRadioButton;
                HtmlInputRadioButton rdGenderFemale = formProfile.FindControl("rdGenderFemale") as HtmlInputRadioButton;

                if (applicantUser.PreviouslyEmployed)
                {
                    ListItem item = selectExistingApplicant.Items.FindByValue("Yes");
                    if (item != null)
                        item.Selected = true;
                }
                else
                {
                    ListItem item = selectExistingApplicant.Items.FindByValue("No");
                    if (item != null)
                        item.Selected = true;
                }

                tbEmailAddress.Value = applicantUser.Email;
                UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(applicantUser.Code);

                tbFullName.Value = applicantUser.Name;
                tbIdCardNumber.Value = applicantUser.IDCardNumber;
                if (applicantUser.Birthday.HasValue)
                {
                    tbBirthDate.Value = Utils.ComposeSequenceNumber(applicantUser.Birthday.Value.Date.Day, 2);
                    tbBirthMonth.Value = Utils.ComposeSequenceNumber(applicantUser.Birthday.Value.Date.Month, 2);
                    tbBirthYear.Value = Utils.ComposeSequenceNumber(applicantUser.Birthday.Value.Date.Year, 4);
                }

                if (applicantUser.GenderSpecification == EnumGender.male.ToString())
                    rdGenderMale.Checked = true;
                else
                    rdGenderFemale.Checked = true;

                generateCapctha();
            }
        }

        protected void formProfile_ItemInserting(object sender, FormViewInsertEventArgs e)
        {

            HtmlSelect selectExistingApplicant = formProfile.FindControl("selectExistingApplicant") as HtmlSelect;

            HtmlInputControl tbEmailAddress = formProfile.FindControl("tbEmailAddress") as HtmlInputControl;
            HtmlInputControl tbPassword = formProfile.FindControl("tbPassword") as HtmlInputControl;
            HtmlInputControl tbConfirmPassword = formProfile.FindControl("tbConfirmPassword") as HtmlInputControl;

            HtmlInputControl tbFirstName = formProfile.FindControl("tbFirstName") as HtmlInputControl;
            HtmlInputControl tbLastName = formProfile.FindControl("tbLastName") as HtmlInputControl;
            HtmlInputControl tbPhoneNumber = formProfile.FindControl("tbPhoneNumber") as HtmlInputControl;
            HtmlInputControl tbAlternativePhoneNumber = formProfile.FindControl("tbAlternativePhoneNumber") as HtmlInputControl;

            DropDownList ddAffiliatedUniversity = formProfile.FindControl("ddAffiliatedUniversity") as DropDownList;
            DropDownList ddMajor = formProfile.FindControl("ddMajor") as DropDownList;

            e.Values["AffiliatedInstitutionCode"] = Request[ddAffiliatedUniversity.UniqueID];
            e.Values["AffiliatedMajorCode"] = Request[ddMajor.UniqueID];
            e.Values["Email"] = tbEmailAddress.Value;
            e.Values["FirstName"] = tbFirstName.Value;
            e.Values["Name"] = tbFirstName.Value;
            e.Values["LastName"] = tbLastName.Value;
            e.Values["Phone"] = tbPhoneNumber.Value;
            e.Values["AlternativePhoneNumber"] = tbAlternativePhoneNumber.Value;
            // user account
            UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(Utils.GetQueryString<string>("code"));
            if (userAccount == null)
            {
                userAccount = new UserAccounts();

            }
            userAccount.Name = string.Concat(tbFirstName.Value, " ", tbLastName.Value);
            userAccount.UserName = tbEmailAddress.Value;
            userAccount.Password = tbPassword.Value;
            userAccount.IsApplicant = true;
            e.Values["Account"] = userAccount;
            e.Values["Active"] = true;
            e.Values["RegisteredDate"] = DateTime.Now;
        }

        #endregion

        #region ods

        protected void odsProfile_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Response.Redirect("../private/update-profile.aspx?code=" + Utils.GetQueryString<string>("code"));
            }
        }

        protected void odsProfile_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Applicants applicant = e.ReturnValue as Applicants;
                if (!applicant.Verified)
                {
                    if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
                    {
                        Companies emailSetting = ERecruitmentManager.GetCompany(MainCompany.Code);
                        SmtpClient client = new SmtpClient();
                        client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                        client.Host = emailSetting.EmailSmtpHostAddress;
                        if (emailSetting.EmailSmtpUseSSL)
                            client.EnableSsl = true;
                        client.Timeout = 20000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                        string address = emailSetting.EmailSmtpEmailAddress;
                        string displayName = emailSetting.MailSender;

                        if (!address.Contains("@"))
                        {
                            address = displayName;
                        }
            
                        MailMessage mm = new MailMessage(new MailAddress(address,
                                displayName),
                                                                         new MailAddress(applicant.Email, ""));
                        mm.Subject = "Email verification from " + emailSetting.ApplicationTitle;
                        string body = @"<div style=padding:10px>
                         <div style='width:100%;border:solid 1px #A6C9E2;background-color:#ffffff'>
                             
                                <table>
                                    <tr><td>Dear [candidate],</td></tr>
                                </table> 
                                <table>
                                    <tr><td>Welcome to E-Recruitment Application</td></tr>
                                    <tr>
                                         <td>Your registration is completed, please activate your account within 1x24 hours by clicking the link below:</td>
                                    </tr>
                                    <tr>
                                        <td><a href=[link]>Activate my account</a></td>
                                    </tr>      
                                    <tr>
                                        <td>If you found this email in your Trash/Spam Folder, please click “NOT SPAM/JUNK” in order to ensure that our email not delivered to your Trash/Spam Folder.</td>
                                    </tr>
                                    <tr><td></td></tr>
                                    <tr>
                                       <td>Regards,</td>
                                    </tr>
                                     <tr><td></td></tr>
                                     <tr><td></td></tr>
                                     <tr><td>Recruitment Team</td></tr>
                                </table> 
                        </div>
                     </div>
                            ";
                        mm.Body = body;
                        string link = MainCompany.ATSBaseUrl  + "/applicant/emailverificationcompleted.aspx?code=" + applicant.Code;
                        mm.Body = mm.Body.Replace("[link]", link).Replace("[candidate]", applicant.FirstName);
                        mm.BodyEncoding = UTF8Encoding.UTF8;
                        mm.IsBodyHtml = true;
                        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        client.Send(mm);
                    }
                    JQueryHelper.InvokeJavascript("window.showNotification('Registration completed, please check your email');window.close();window.location.href='" + MainCompany.ATSBaseUrl + "default.aspx';", Page);
                }
                else
                {
                    Session["currentuser"] = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(applicant.Code);

                    Response.Redirect("../applicant/update-profile.aspx?code=" + applicant.Code);
                }
            }
            else
            {
                e.ExceptionHandled = true;
                JQueryHelper.InvokeJavascript("showNotification('" + Utils.ExtractInnerExceptionMessage(e.Exception) + "');", Page);
            }
        }

        #endregion
    }
}