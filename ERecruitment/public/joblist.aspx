﻿<%@ Page Title="Job Opportunities" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="joblist.aspx.cs" Inherits="ERecruitment.joblist" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="bitLyAccessToken" value="<%= ConfigurationManager.AppSettings["bitLyAccessToken"] %>" />
    <script type="text/javascript">
        var state;
        var disqualifierQuestionExist = false;
        var listObj;

        $(document).ready(function () {
            $(".glyphicon-exclamation-sign").tooltip();
            toggleInformationSourceNotes();
            var isValidateCaptcha;
            var isValidateEmail;
            if (GetQueryStringParams("apply") != "") {
                applyJob(GetQueryStringParams("apply"));
            }
            initiateList();

            $(".btnGo").click(function (e) {
                e.preventDefault();
                var pageSize = 5;
                var page = parseInt($("#go-to").val());

                if ($(".pagination").children(":last-child").text() < page) {
                    page = parseInt($(".pagination").children(":last-child").text());
                } else if (page < 0) {
                    page = 0;
                }

                listObj.show(((page - 1) * pageSize) + 1, pageSize);
                $(".pagination .active").removeClass("active");
                $(".pagination li").filter(function () { return $(this).text() == page }).addClass("active");
            });
        });

        function initiateList() {

            var options = {
                valueNames: ['name'],
                page: 5,
                plugins: [
                  ListPagination({
                      outerWindow: 2
                  })
                ]
            };

            listObj = new List('listContainer', options);

            if (listObj.items.length == 0) {
                $(".btnGo").parent().parent().hide();
            } else {
                $(".btnGo").parent().parent().show();
            }

            listObj.on("updated",
                function () {
                    if (listObj.visibleItems.length == 0) {
                        $(".btnGo").parent().parent().hide();
                    } else {
                        $(".btnGo").parent().parent().show();
                    }
                });
        }

        function toggleInformationSourceNotes() {

        }


        var appMode;
        function applyJob(vacancyCode) {

            getAppMode();
            $("#" + '<%= hidActiveVacancyCode.ClientID %>').val(vacancyCode);

            if ($("#" + '<%= hidCurrUserCode.ClientID %>').val() == "")
                showLoginForm();
            else {

                var vacancyCode = $("#" + '<%= hidActiveVacancyCode.ClientID %>').val();
                var applicantCode = $("#" + '<%= hidCurrUserCode.ClientID %>').val();
                var param = "&vacancyCode=" + vacancyCode;
                param += "&applicantCode=" + applicantCode;
                var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=CheckJobApplicant" + param;
                if (appMode == "Single") {
                    $('#appModeFrameModel').modal('show');

                } else if (appMode == "Multi") {
                    $.ajax({
                        url: handlerUrl,
                        async: false,
                        beforeSend: function () {
                        },
                        success: function (queryResult) {
                            loadDisqQuestion(vacancyCode);
                            if (disqualifierQuestionExist) {
                                $('#applyJobFrameModel').modal('show');
                            }
                            else {
                                commitApplyJob();
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            showNotification(xhr.responseText);

                        }
                    });
                }


            }
        }

        function continueApply() {
            $('#appModeFrameModel').modal('hide');
            var vacancyCode = $("#" + '<%= hidActiveVacancyCode.ClientID %>').val();
            var applicantCode = $("#" + '<%= hidCurrUserCode.ClientID %>').val();
            var param = "&vacancyCode=" + vacancyCode;
            param += "&applicantCode=" + applicantCode;
            var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=CheckJobApplicant" + param;
            $.ajax({
                url: handlerUrl,
                async: false,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    loadDisqQuestion(vacancyCode);
                    if (disqualifierQuestionExist) {
                        $('#applyJobFrameModel').modal('show');
                    }
                    else {
                        commitApplyJob();
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);

                }
            });
        }

        function getAppMode() {
            var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=GetAppMode";
            $.ajax({
                url: handlerUrl,
                async: false,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    appMode = queryResult;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);

                }
            });
        }



        function commitApplyJob() {
            var form = "applyJobFrameModel";

            var vacancyCode = $("#" + '<%= hidActiveVacancyCode.ClientID %>').val();
           var applicantCode = $("#" + '<%= hidCurrUserCode.ClientID %>').val();
           var param = "&vacancyCode=" + vacancyCode;
           param += "&applicantCode=" + applicantCode;

           var actionUrl = "../handlers/HandlerVacancies.ashx?commandName=ApplyJob" + param;
           var formData = {};

           if (!getNodeData(form, formData)) return false;
           showModal();
           $.post(actionUrl, formData).done(function (data) {
               $('#applyJobFrameModel').modal('hide');
           }).fail(function (response) {
               $('#applyJobFrameModel').modal('hide');
               $('#modal-gs').modal('hide');
               BootstrapDialog.show({
                   type: BootstrapDialog.TYPE_DANGER,
                   id: 'alert-msg',
                   title: 'Notification',
                   message: response.responseText

               });
           });

       }

       function showModal() {
           $('#modal-gs').modal({
               backdrop: 'static',
               keyboard: true,
               show: true
           });

           $('#modal-gs').on('shown.bs.modal', function () {
               $('#<%=ddsurvayIklan.ClientID%>').focus();
            });
            }

            function doClose() {
                BootstrapDialog.show({
                    type: BootstrapDialog.TYPE_SUCCESS,
                    title: 'Notification',
                    message: "Apply Success",
                    id: 'alert-msg',
                    closable: false,
                    buttons: [{
                        icon: 'glyphicon glyphicon-remove',
                        label: 'Close',
                        cssClass: 'btn btn-success',
                        action: function (dialogItself) {
                            dialogItself.close();
                            filterJobList();
                        }
                    }]
                });
            }

            function myFunction(val) {
                $("#" + '<%= note.ClientID %>').val("");
                if ("Lainnya" == val.value) {
                    $("#" + '<%= note.ClientID %>').prop("readonly", false);
                } else {
                    $("#" + '<%= note.ClientID %>').prop("readonly", true);
                }
            }

            function doCommitSurvay() {
                var vacancyCode = $("#" + '<%= hidActiveVacancyCode.ClientID %>').val();
                var applicantCode = $("#" + '<%= hidCurrUserCode.ClientID %>').val();
                var param = "&vacancyCode=" + vacancyCode;
                param += "&applicantCode=" + applicantCode;
                if ($("#" + '<%= note.ClientID %>').val() != null && $("#" + '<%= note.ClientID %>').val() != "") {
                param += "&sourceResource=" + $("#" + '<%= note.ClientID %>').val();
            } else {
                param += "&sourceResource=" + $("#" + '<%= ddsurvayIklan.ClientID %>').val();
            }

            if ($("#" + '<%= ddsurvayIklan.ClientID %>').val() != null && "" != $("#" + '<%= ddsurvayIklan.ClientID %>').val()) {

                } else {
                    showNotification("Source references should not empty");
                    return;
                }

                var formData = {};


                var actionUrl = "../handlers/HandlerVacancies.ashx?commandName=ApplySurvay" + param;
                $.post(actionUrl, formData).done(function () {
                    $('#modal-gs').modal('toggle');
                    BootstrapDialog.show({
                        type: BootstrapDialog.TYPE_SUCCESS,
                        title: 'Notification',
                        message: "Apply Success",
                        id: 'alert-msg',
                        closable: false,
                        buttons: [{
                            icon: 'glyphicon glyphicon-remove',
                            label: 'Close',
                            cssClass: 'btn btn-success',
                            action: function (dialogItself) {
                                dialogItself.close();
                                filterJobList();
                            }
                        }]
                    });
                });
            }

            function shareJob(medSoc, jobCode, jobName) {
                var url = $("#" + '<%= hidJobDetailUrl.ClientID %>').val() + "?vacancyCode=" + jobCode;
            postJob(medSoc, url, jobName);
        }

        function showLoginForm() {
            state = 'login';
            $('#signUpFrameModel').modal('hide');
            $('#loginFrameModel').modal('show');
        }

        function getCaptcha() {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

            for (var i = 0; i < 5; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            var textCaptcha = "../handlers/HandlerCaptcha.ashx?commandName=setCaptcha" + text;

            $("#ContentPlaceHolder1_imgCaptcha").attr("src", textCaptcha);

        }

        function showSignUpForm() {
            regMethod = "manual";
            state = 'signup';
            $('#loginFrameModel').modal('hide');
            $('#signUpFrameModel').modal('show');
        }

        function showSignUp() {
            $('#personalInfoFrameModelRegister').modal('hide');
            $('#signUpFrameModel').modal({ backdrop: 'static', keyboard: false })
            $('#signUpFrameModel').modal('show');
        }

        function signUpUser() {
            validateCaptcha();
            validateEmail();
            if ($("#tbFirstName").val() == "" || $("#tbLastName").val() == "" ||
                $("#tbEmailAddress").val() == "" || $("#tbPassword").val() == "" ||
                $("#tbCaptcha").val() == "") {
                if ($("#tbFirstName").val() == "")
                    $("#tbFirstName").popover("show");
                else
                    $("#tbFirstName").popover("hide");

                if ($("#tbLastName").val() == "")
                    $("#tbLastName").popover("show");
                else
                    $("#tbLastName").popover("hide");

                if ($("#tbEmailAddress").val() == "")
                    $("#tbEmailAddress").popover("show");
                else
                    $("#tbEmailAddress").popover("hide");

                if ($("#tbPassword").val() == "")
                    $("#tbPassword").popover("show");
                else
                    $("#tbPassword").popover("hide");

                if ($("#tbCaptcha").val() == "")
                    $("#tbCaptcha").popover("show");
                else
                    $("#tbCaptcha").popover("hide");
            } else
                if (isValidateCaptcha != "1" || isValidateEmail != "1") {
                    if (isValidateCaptcha != "1" && isValidateEmail != "1")
                        showNotification(isValidateCaptcha + ", " + isValidateEmail);
                    else if (isValidateCaptcha != "1")
                        showNotification(isValidateCaptcha);
                    else if (isValidateEmail != "1")
                        showNotification(isValidateEmail);

                }
                else
                    if (!validateEmailFormat($("#tbEmailAddress").val()))
                        showNotification("Invalid Email Format!");
                    else {
                        state = "manual";
                        $("#tbFirstName").popover("hide");
                        $("#tbLastName").popover("hide");
                        $("#tbEmailAddress").popover("hide");
                        $("#tbPassword").popover("hide");
                        $("#tbCaptcha").popover("hide");

                        var applicantModel = [];
                        applicantModel['FirstName'] = $("#tbFirstName").val();
                        applicantModel['LastName'] = $("#tbLastName").val();
                        applicantModel['Email'] = $("#tbEmailAddress").val();
                        $("#btnPrevSignUp").removeClass("hidden");
                        loadDataIntoForm("personalInfoFrameModelRegister", applicantModel);
                        $('#signUpFrameModel').modal('hide');
                        $('#personalInfoFrameModelRegister').modal({ backdrop: 'static', keyboard: false })
                        $('#personalInfoFrameModelRegister').modal('show');
                    }
        }


        function showNotif() {
            state = "regsosmed4";
            getAppMode();
            if (appMode == "Single") {
                $('#completedRegistration').modal('hide');
                $('#appModeSingleFrameModel').modal({ backdrop: 'static', keyboard: false })
                $('#appModeSingleFrameModel').modal('show');

            } else if (appMode == "Multi") {
                $('#completedRegistration').modal('hide');
                $('#appModeMultiFrameModel').modal({ backdrop: 'static', keyboard: false })
                $('#appModeMultiFrameModel').modal('show');
            }

        }



        var regMethod;
        function registerFaceBook() {
            state = "";
            regMethod = "facebook";
            console.log("checking facebook login status");
            FB.login(function (response) {

                if (response.authResponse) {
                    console.log('Welcome!  Fetching your information.... ');
                    FB.api('/me', { fields: 'name,email,id,gender,birthday' }, function (apiResponse) {
                        console.log('Good to see you, ' + apiResponse.name + '.');

                        var name = apiResponse.name;
                        var userEmail = apiResponse.email;
                        var userId = apiResponse.id;
                        var gender = apiResponse.gender;
                        var birthday = apiResponse.birthday;

                        var param = "&name=" + name;
                        param += "&email=" + userEmail;
                        param += "&id=" + userId;
                        param += "&gender=" + gender;
                        //param += "&birthday=" + birthday;
                        param += "&appName=facebook";
                        param += "&applyJob=true";
                        // authenticate via facebook
                        console.log("authenticating..., param:" + param);

                        $.ajax({
                            url: "/handlers/HandlerAuthentication.ashx?commandName=loginsosmed" + param,
                            async: false,
                            beforeSend: function () {

                            },
                            success: function (queryResult) {
                                // redirect to job listing
                                if (queryResult == "notregistered") {
                                    var applicantModel = [];
                                    applicantModel['FirstName'] = name;
                                    applicantModel['SocMedName'] = "facebook";
                                    applicantModel['Email'] = userEmail
                                    applicantModel['FacebookUrl'] = "https://www.facebook.com/profile.php?id=" + userId;
                                    applicantModel['FacebookID'] = userId;
                                    applicantModel['GenderSpecification'] = gender.charAt(0).toUpperCase() + gender.slice(1);;

                                    loadDataIntoForm("personalInfoFrameModelRegister", applicantModel);
                                    $('#loginFrameModel').modal('hide');
                                    $('#personalInfoFrameModelRegister').modal({ backdrop: 'static', keyboard: false })
                                    $('#personalInfoFrameModelRegister').modal('show');
                                    state = 'regsosmed1';
                                } else {
                                    var param = "?companyCode=" + $("#" + '<%= ddCompany.ClientID %>').val();
                                    param += "&jobCategoryCode=" + $("#" + '<%= ddJobCategory.ClientID %>').val();
                                    param += "&locationCode=" + $("#" + '<%= ddJobLocation.ClientID %>').val();
                                    param += "&apply=" + $("#" + '<%= hidActiveVacancyCode.ClientID %>').val();
                                    if (queryResult != "")
                                        window.location.href = queryResult;
                                    else
                                        window.location.href = $("#" + '<%= hidJobListUrl.ClientID %>').val() + param;
                                }

                            },
                            error: function (xhr, ajaxOptions, thrownError) {

                                showNotification(xhr.responseText);

                            }
                        });

                    });
                } else {
                    console.log('User cancelled login or did not fully authorize.');
                }
            }, { scope: 'public_profile,email,user_birthday', return_scopes: true });

        }

        function registerLinkedIn() {
            state = "";
            regMethod = "linkedin";
            console.log("Checking linkedin");

            IN.User.authorize(function () {
                IN.API.Raw('/people/~:(id,first-name,last-name,email-address,picture-url,picture-urls::(original))?format=json')
                .method('GET')
                .result(function (me) {
                    var name = me["firstName"] + " " + me["lastName"];
                    var userEmail = me["emailAddress"];
                    var userId = me["id"];
                    var pictureUrl = me["pictureUrls"].values;
                    var param = "&name=" + name;
                    param += "&email=" + userEmail;
                    param += "&id=" + userId;
                    param += "&pictureUrl=" + pictureUrl;
                    param += "&appName=linkedin";
                    param += "&applyJob=true";
                    // authenticate via linkedin
                    console.log("authenticating..., param:" + param);
                    $.ajax({
                        url: "/handlers/HandlerAuthentication.ashx?commandName=loginsosmed" + param,
                        async: false,
                        beforeSend: function () {
                        },
                        success: function (queryResult) {

                            if (queryResult == "notregistered") {
                                var applicantModel = [];
                                applicantModel['FirstName'] = me["firstName"];
                                applicantModel['LastName'] = me["lastName"];
                                applicantModel['SocMedName'] = "linkedin";
                                applicantModel['Email'] = userEmail;
                                applicantModel['LinkedInUrl'] = "http://www.linkedin.com/profile/view?id=" + userId;
                                applicantModel['LinkedInID'] = userId;
                                applicantModel['LinkedInImageUrl'] = pictureUrl;

                                loadDataIntoForm("personalInfoFrameModelRegister", applicantModel);
                                $('#loginFrameModel').modal('hide');
                                $('#personalInfoFrameModelRegister').modal({ backdrop: 'static', keyboard: false })
                                $('#personalInfoFrameModelRegister').modal('show');
                                state = 'regsosmed1';
                            } else {
                                var param = "?companyCode=" + $("#" + '<%= ddCompany.ClientID %>').val();
                                param += "&jobCategoryCode=" + $("#" + '<%= ddJobCategory.ClientID %>').val();
                                param += "&locationCode=" + $("#" + '<%= ddJobLocation.ClientID %>').val();
                                param += "&apply=" + $("#" + '<%= hidActiveVacancyCode.ClientID %>').val();
                                if (queryResult != "")
                                    window.location.href = queryResult;
                                else
                                    window.location.href = $("#" + '<%= hidJobListUrl.ClientID %>').val() + param;
                            }


                        },
                        error: function (xhr, ajaxOptions, thrownError) {

                            showNotification(xhr.responseText);

                        }
                    });

                })
            });
        }

        function registerUser() {
            $('#appModeSingleFrameModel').modal('hide');
            $('#appModeMultiFrameModel').modal('hide');
            if (regMethod == "facebook") {
                registerFaceBook();
            }
            else {
                registerLinkedIn()
            }
        }

        function nextModalPassword2() {
            validateEmail();
            if ($("#FirstName").val() == "" || $("#Birthday").val() == "" ||
                $("#BirthPlace").val() == "" || $("#Email").val() == "" ||
                $("#Phone").val() == "" || $("#IDCardNumber").val() == "" ||
                $("#tbAddress").val() == "" || $("#GenderSpecification option:selected").val() == "" ||
                $("#MaritalStatusSpecification option:selected").val() == "" ||
                $("#Religion option:selected").val() == "" ||
                $("#<%= ddNationality.ClientID %> option:selected").val() == "" ||
                    $("#<%= ddCity.ClientID %> option:selected").val() == "") {

                        if ($("#FirstName").val() == "")
                            $("#FirstName").popover("show");
                        else
                            $("#FirstName").popover("hide");

                        if ($("#Birthday").val() == "")
                            $("#Birthday").popover("show");
                        else
                            $("#Birthday").popover("hide");

                        if ($("#BirthPlace").val() == "")
                            $("#BirthPlace").popover("show");
                        else
                            $("#BirthPlace").popover("hide");

                        if ($("#Email").val() == "")
                            $("#Email").popover("show");
                        else
                            $("#Email").popover("hide");

                        if ($("#Phone").val() == "")
                            $("#Phone").popover("show");
                        else
                            $("#Phone").popover("hide");

                        if ($("#IDCardNumber").val() == "")
                            $("#IDCardNumber").popover("show");
                        else
                            $("#IDCardNumber").popover("hide");

                        if ($("#tbAddress").val() == "")
                            $("#tbAddress").popover("show");
                        else
                            $("#tbAddress").popover("hide");

                        if ($("#GenderSpecification option:selected").val() == "") {
                            $('#GenderSpecification').popover('destroy');
                            $("#GenderSpecification").popover({
                                trigger: 'manual',
                                placement: 'right',
                                content: "Please select an item in the list"
                            }).popover('show');
                        }
                        else
                            $('#GenderSpecification').popover('destroy');

                        if ($("#MaritalStatusSpecification option:selected").val() == "") {
                            $('#MaritalStatusSpecification').popover('destroy');
                            $("#MaritalStatusSpecification").popover({
                                trigger: 'manual',
                                placement: 'right',
                                content: "Please select an item in the list"
                            }).popover('show');
                        }
                        else
                            $('#MaritalStatusSpecification').popover('destroy');

                        if ($("#Religion option:selected").val() == "") {
                            $('#Religion').popover('destroy');
                            $("#Religion").popover({
                                trigger: 'manual',
                                placement: 'right',
                                content: "Please select an item in the list"
                            }).popover('show');
                        }
                        else
                            $('#Religion').popover('destroy');

                        if ($("#<%= ddNationality.ClientID %> option:selected").val() == "") {
                            $('#<%= ddNationality.ClientID %>').popover('destroy');
                            $("#<%= ddNationality.ClientID %>").popover({
                                trigger: 'manual',
                                placement: 'right',
                                content: "Please select an item in the list"
                            }).popover('show');
                        }
                        else
                            $('#<%= ddNationality.ClientID %>').popover('destroy');

                        if ($("#<%= ddCity.ClientID %> option:selected").val() == "") {
                            $('#<%= ddCity.ClientID %>').popover('destroy');
                    $("#<%= ddCity.ClientID %>").popover({
                        trigger: 'manual',
                        placement: 'right',
                        content: "Please select an item in the list"
                    }).popover('show');
                }
                else
                    $('#<%= ddCity.ClientID %>').popover('destroy');

            } else if (!validateEmailFormat($("#Email").val()))
                showNotification("Invalid Email Format!");
            else if (isValidateEmail != "1")
                showNotification(isValidateEmail);

            else {
                if (regMethod != "manual") {
                    state = 'regsosmed2';
                    $("#FirstName").popover("hide");
                    $("#Birthday").popover("hide");
                    $("#BirthPlace").popover("hide");
                    $("#Email").popover("hide");
                    $("#Phone").popover("hide");
                    $("#IDCardNumber").popover("hide");
                    $("#tbAddress").popover("hide");
                    $('#GenderSpecification').popover('destroy');
                    $('#MaritalStatusSpecification').popover('destroy');
                    $('#Religion').popover('destroy');

                    $('#<%= ddNationality.ClientID %>').popover('destroy');
                            $('#<%= ddCity.ClientID %>').popover('destroy');

                            $('#personalInfoFrameModelRegister').modal('hide');
                            $('#loginFrameModel').modal('hide');

                            $('#updatePasswordFrameModel').modal({ backdrop: 'static', keyboard: false })
                            $('#updatePasswordFrameModel').modal('show');
                        }
                        else {
                            manualRegistration();

                        }
                    }
        }


        function validateCaptcha() {
            var param = "&captcha=" + $("#tbCaptcha").val();
            $.ajax({
                url: "/handlers/HandlerAuthentication.ashx?commandName=validateCaptcha" + param,
                async: false,
                beforeSend: function () {
                },
                success: function (queryResult) {

                    if (queryResult == "1") {
                        isValidateCaptcha = "1";
                    }
                    else {
                        isValidateCaptcha = "Security code not match!";
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    isValidateCaptcha = xhr.responseText;
                }
            });
        }

        function validateEmail() {
            var param = "&email=" + $("#tbEmailAddress").val();
            $.ajax({
                url: "/handlers/HandlerAuthentication.ashx?commandName=validateEmail" + param,
                async: false,
                beforeSend: function () {
                },
                success: function (queryResult) {

                    if (queryResult == "1") {
                        isValidateEmail = "1";
                    }
                    else {
                        isValidateEmail = "This email already registered!";
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    isValidateEmail = xhr.responseText;
                }
            });
        }

        function validateEmailFormat($email) {
            var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
            return emailReg.test($email);
        }

        function manualRegistration() {

            var actionUrl = "../handlers/HandlerAuthentication.ashx?commandName=register";
            var formData = {};
            formData["Password"] = $("#tbPassword").val();
            if (!getNodeData("personalInfoFrameModelRegister", formData)) return false;

            $.post(actionUrl, formData).done(function (data) {
                $('#personalInfoFrameModelRegister').modal('hide');
                // redirect to homepage
                window.location.href = data;
            }).fail(function (response) {
                showNotification(response.responseText);
            });
        }

        function prevModalPersonalInfo() {
            state = "regsosmed1";
            $('#updatePasswordFrameModel').modal('hide');
            $('#personalInfoFrameModelRegister').modal({ backdrop: 'static', keyboard: false })
            $('#personalInfoFrameModelRegister').modal('show');
        }

        function saveAllData() {
            if ($("#tbUpdatePassword").val() == "" || $("#tbUpdatePasswordConfirm").val() == "") {

                if ($("#tbUpdatePassword").val() == "")
                    $("#tbUpdatePassword").popover("show");
                else
                    $("#tbUpdatePassword").popover("hide");

                if ($("#tbUpdatePasswordConfirm").val() == "")
                    $("#tbUpdatePasswordConfirm").popover("show");
                else
                    $("#tbUpdatePasswordConfirm").popover("hide");

            }
            else {
                $("#tbUpdatePassword").popover("hide");
                $("#tbUpdatePasswordConfirm").popover("hide");
                if ($("#tbUpdatePassword").val() != $("#tbUpdatePasswordConfirm").val()) {
                    showNotification("Password does not match the confirm password!");
                }
                else {
                    state = 'regsosmed3';
                    var actionUrl = "../handlers/HandlerAuthentication.ashx?commandName=regsosmed&appName=" + regMethod;
                    var formData = {};
                    formData["Password"] = $("#tbUpdatePassword").val();
                    if (!getNodeData("personalInfoFrameModelRegister", formData)) return false;
                    $.post(actionUrl, formData).done(function (data) {
                        $('#updatePasswordFrameModel').modal('hide');
                        $('#completedRegistration').modal({ backdrop: 'static', keyboard: false })
                        $('#completedRegistration').modal('show');
                        //alert("sucess");

                    }).fail(function (response) {
                        showNotification(response.responseText);
                    });
                }
            }
        }


        $(document).keypress(function (e) {
            if (e.which == 13) {
                e.preventDefault();

                switch (state) {
                    case "login":
                        loginUser();
                        break;
                    case "signup":
                        signUpUser();
                        break;
                    case "regsosmed1":
                        nextModalPassword2();
                        break;
                    case "regsosmed2":
                        saveAllData();
                        break;
                    case "regsosmed3":
                        showNotif();
                        break;
                    case "regsosmed4":
                        registerUser();
                        break;
                    case "manual":
                        nextModalPassword2();
                        break;
                }
            }
        });

        $(document).on('hide.bs.modal', '#loginFrameModel', function () {
            $("#email").popover("hide");
            $("#password").popover("hide");
        })

        $(document).on('hide.bs.modal', '#updatePasswordFrameModel', function () {
            $("#tbUpdatePassword").popover("hide");
            $("#tbUpdatePasswordConfirm").popover("hide");
        })

        function loginUser() {
            state = "";
            var param = "&email=" + $("#email").val();
            param += "&password=" + encodeURIComponent($("#password").val());
            param += "&applyJob=true";
            if ($("#email").val() == "" || $("#password").val() == "") {

                if ($("#email").val() == "")
                    $("#email").popover("show");
                if ($("#password").val() == "")
                    $("#password").popover("show");
            }
            else {
                // manual registration
                $.ajax({
                    url: "../handlers/HandlerAuthentication.ashx?commandName=login" + param,
                    async: false,
                    beforeSend: function () {

                    },
                    success: function (queryResult) {

                        var param = "?companyCode=" + $("#" + '<%= ddCompany.ClientID %>').val();
                    param += "&jobCategoryCode=" + $("#" + '<%= ddJobCategory.ClientID %>').val();
                    param += "&locationCode=" + $("#" + '<%= ddJobLocation.ClientID %>').val();
                    param += "&apply=" + $("#" + '<%= hidActiveVacancyCode.ClientID %>').val();
                    if (queryResult != "")
                        window.location.href = queryResult;
                    else
                        window.location.href = $("#" + '<%= hidJobListUrl.ClientID %>').val() + param;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification('invalid username or password');
                }
            });
        }
    }

    </script>

    <script type="text/javascript">
        function filterJobList() {
            var param = "?companyCode=" + $("#" + '<%= ddCompany.ClientID %>').val();
            param += "&jobCategoryCode=" + $("#" + '<%= ddJobCategory.ClientID %>').val();
            param += "&locationCode=" + $("#" + '<%= ddJobLocation.ClientID %>').val();

            window.location.href = $("#" + '<%= hidJobListUrl.ClientID %>').val() + param;
        }

    </script>

    <script type="text/javascript">


        function loadDisqQuestion(dataId) {
            var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetJobDisqualifierQuestionList&id=" + dataId;
            var listContainer = $("#disqualifierQuestionsContainer");
            $.ajax({
                url: handlerUrl,
                async: false,
                beforeSend: function () {
                    $(".disqualifierQuestionItem").remove();
                },
                success: function (queryResult) {

                    if (queryResult != "") {
                        disqualifierQuestionExist = true;
                        var dataModel = $.parseJSON(queryResult);
                        $.each(dataModel, function (i, item) {

                            // survey question wrapper
                            var questionWrapper = $(document.createElement('div')).attr("class", "row jobSection disqualifierQuestionItem");
                            // question section
                            var questionSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('p')).attr("class", "text-muted").text(item["Question"]));
                            questionWrapper.append(questionSection);

                            answerIndex = 0;
                            $.each(item["AnswerOptionList"], function (x, answerItem) {


                                var answerSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");
                                var checkboxInput = $(document.createElement('div')).attr("class", "col-md-1 col-sm-1").append($(document.createElement('input')).attr("type", "radio").attr("name", "iscorrectanswer" + i).attr("data-role", "value").attr("data-attribute", item["Code"]).attr("value", answerItem["Label"]));

                                var answerInput = $(document.createElement('div')).attr("class", "col-md-8 col-sm-8").append($(document.createElement('span')).text(answerItem["Label"]));


                                answerSection.append(checkboxInput)
                                             .append(answerInput);
                                questionWrapper.append(answerSection);
                                answerIndex++;

                            });

                            if (item["AnswerFormat"] == "Numeric") {
                                // answer wrapper
                                var answerSection = $(document.createElement('div')).attr("class", "col-md-2 col-sm-2").append($(document.createElement('input')).attr("type", "text").attr("class", "form-control plainnumber").attr("data-attribute", item["Code"]));
                                questionWrapper.append(answerSection);
                            }

                            listContainer.append(questionWrapper);


                        });
                    }
                    $(".plainnumber").keypress(
                     function (event) {

                         var fld = $(this);
                         var milSep = ",";
                         var decSep = ".";
                         var sep = 0;
                         var key = '';
                         var i = j = 0;
                         var len = len2 = 0;
                         var strCheck = '-0123456789';
                         var aux = aux2 = '';
                         var whichCode = (window.Event) ? event.which : event.keyCode;
                         if (whichCode == 46) return true; // Enter
                         if (whichCode == 13) return true; // Enter
                         if (whichCode == 8) return true; // Delete (Bug fixed)
                         if (whichCode == 0) return true; // tab (Bug fixed)
                         key = String.fromCharCode(whichCode); // Get key value from key code
                         if (strCheck.indexOf(key) == -1) return false; // Not a valid key

                     });
                    $('#formDataFrameModel').modal('show');

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });

        }
    </script>

    <!-- service section -->
    <div class="no-padding">
        <div class="container">
            <div class="row searchPanel">
                <div class="col-md-12 col-sm-12">
                    <div class="col-md-1 col-sm-1">
                        <span data-lang="CompanyFilterLabel">Company</span>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <asp:DropDownList ID="ddCompany" onchange="filterJobList();" OnDataBound="ddDatabound" DataValueField="Code" DataTextField="Name" runat="server" CssClass="form-control" />
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <span data-lang="JobCategoryFilterLabel">Job Category</span>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <asp:DropDownList ID="ddJobCategory" onchange="filterJobList();" DataSourceID="odsJobCategories" OnDataBound="ddDatabound" DataValueField="Code" DataTextField="Name" runat="server" CssClass="form-control" />
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <span data-lang="WorkLocationFilterLabel">Work Location</span>
                    </div>
                    <div class="col-md-2 col-sm-2">
                        <asp:DropDownList ID="ddJobLocation" onchange="filterJobList();" DataSourceID="odsCity" OnDataBound="ddDatabound" DataValueField="CityCode" DataTextField="CityName" runat="server" CssClass="form-control" />
                    </div>
                </div>
            </div>

            <div id="listContainer">
                <div class="row">
                    <div class="col-md-9 col-sm-9">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <input class="search form-control" placeholder="Search" />
                    </div>
                </div>
                <ul class="list">
                    <asp:Repeater ID="repJobList"
                        runat="server"
                        DataSourceID="odsJobList"
                        OnItemDataBound="repJobList_ItemDataBound">
                        <ItemTemplate>
                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <img id="imgLogo" runat="server" alt="" />
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div class="block text-uppercase"><strong><a href='jobdetail.aspx?vacancyCode=<%# Eval("Code") %>'><span class="name"><%# Eval("PositionName") %></span>,</a></strong></div>
                                </div>
                                <div class="col-md-2 col-sm-2">
                                    <div class="block">
                                        <strong id="applylink" runat="server">
                                            <a class="btn btn-default" onclick='applyJob("<%# Eval("Code") %>"); return false;'>Apply</a>
                                        </strong>
                                        <strong id="applied" runat="server" visible="false">
                                            <i class="fa fa-check">Applied</i>
                                        </strong>
                                    </div>
                                </div>
                                <div class="col-md-7 col-sm-7">
                                    <div class="block"><i><%# Eval("CategoryName") %>, <%# Eval("RecruitmentTargetGroup") %></i></div>
                                    <div class="block">
                                        <i><i class="fa fa-map-marker"></i><%# Eval("CityName") %></i>,  <i class="fa fa-money"></i>
                                        <asp:Literal ID="litSalaryRange" runat="server" />
                                    </div>
                                    <div class="block">
                                        <i onclick='shareJob("linkedin","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-linkedin" title="share linkedin"></i>
                                        <i onclick='shareJob("facebook","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-facebook-official" title="share facebook"></i>
                                        <i onclick='shareJob("twitter","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-twitter" title="share twitter"></i>
                                        <i class="visible-xs" onclick='shareJob("whatsapp","<%# Eval("Code") %>","<%# Eval("PositionName") %>"); return false;' class="fa fa-whatsapp" title="share whatsapp"></i>
                                    </div>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <div class="block"><i><i class="fa fa-calendar-o"></i>Posted <%# Eval("PostDate","{0:dd, MMM yyyy}") %></i></div>
                                    <div class="block"><i><i class="fa fa-calendar-o"></i>Apply before <%# Eval("ExpiredPostDate","{0:dd, MMM yyyy}") %></i></div>

                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
                <ul style="list-style-type: none">
                    <li>
                        <div class="col-sm-6">
                            <ul class="pagination">
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <div style="margin: 20px 0">
                                <div class="col-sm-3" style="padding-right: 0px">
                                    <input type="text" class="form-control" id="go-to" />
                                </div>
                                <div class="col-sm-3" style="padding-left: 0px">
                                    <button type="button" class="btn btn-primary btnGo">Go</button>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <asp:HiddenField ID="hidCurrUserCode" runat="server" />
    <asp:HiddenField ID="hidJobDetailUrl" runat="server" />
    <asp:HiddenField ID="hidJobListUrl" runat="server" />
    <asp:HiddenField ID="hidActiveVacancyCode" runat="server" />

    <div class="modal fade" id="applyJobFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title center">Apply Job</h1>
                </div>
                <div class="modal-body" id="disqualifierQuestionsContainer">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="commitApplyJob(); return false;">Proceed To Apply Job</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="appModeFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title center">Information</h1>
                </div>
                <div class="modal-body" id="informationContainer">
                    <asp:Literal ID="litInformation" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="continueApply(); return false;">Continue Apply</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="loginFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <!--<h1 data-lang="LoginTitle" class="modal-title">Login</h1>-->
                    <img src="../assets/images/daya-lima-logo.png" style="width: 50%">
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <input class="form-control" type="text" data-attribute="email" name="email" id="email" placeholder="Email" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <input class="form-control" type="password" data-attribute="password" name="password" id="password" placeholder="Password" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <a data-lang="SignUpLabel" href="#" onclick="showSignUpForm(); return false;">Sign Up</a>
                        </div>
                        <div class="col-md-6 col-sm-6 right">
                            <a data-lang="ForgotPasswordLabel" href="/public/forgotpassword.aspx">Forgot Password ?</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <button data-lang="LoginButton" class="btn btn-success btn-lg col-xs-12" id="btnDefault" onclick="loginUser(); return false;">
                                <i class="fa fa-sign-in"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Login&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           
                            </button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 center">
                            <hr />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 col-sm-5">
                            <button data-lang="LinkedInButton" id="loginLinkedIn" runat="server" class="btn btn-primary " onclick="registerLinkedIn(); return false;">
                                <i class="fa fa-linkedin"></i>
                                &nbsp;&nbsp;
                             Login Using LinkedIn &nbsp;
                           
                            </button>
                        </div>
                        <div class="col-md-2 col-sm-2">
                        </div>
                        <div class="col-md-5 col-sm-5 right">
                            <button data-lang="LoginFacebookButton" id="loginFacebook" runat="server" class="btn btn-primary" style="background-color: #3C5899" onclick="registerFaceBook(); return false;">
                                <i class="fa fa-facebook"></i>
                                &nbsp;
                                Login Using Facebook
                           
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="signUpFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header center">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <!--<h1 data-lang="SignUpTitle" class="modal-title">Sign Up</h1>-->
                    <img src="/assets/images/daya-lima-logo.png" style="width: 50%" />
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control capitalLetter" type="text" data-attribute="firstname" name="firstname" id="tbFirstName" placeholder="First Name" />
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control capitalLetter" type="text" data-attribute="lastname" name="lastname" id="tbLastName" placeholder="Last Name" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <input class="form-control" type="text" data-attribute="registeremail" name="registeremail" id="tbEmailAddress" placeholder="Email" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <input class="form-control" type="password" data-attribute="registerpassword" name="registerpassword" id="tbPassword" placeholder="Password" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <asp:Image ID="imgCaptcha" runat="server" />
                        </div>
                        <div class="col-md-4 col-sm-4 left">
                            <button data-lang="RefreshCaptchaButton" class="btn btn-default commandIcon" id="btnRefreshCaptcha" runat="server" onclick="getCaptcha(); return false;">
                                <i class="fa fa-refresh"></i>
                            </button>

                        </div>
                        <div class="col-md-5 col-sm-5">
                            <input class="form-control" type="text" data-attribute="captcha" name="captcha" id="tbCaptcha" placeholder="key code" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <button data-lang="SignUpButton" class="btn btn-primary btn-lg col-xs-12 commandIcon" id="btnSignUp" onclick="signUpUser(); return false;">
                                <i class="fa fa-pencil-square-o"></i>Sign Up
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-gs">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <!--<button type="button" class="close" data-dismiss="modal" aria-hidden="true" onclick="doClose();">&times;</button>-->
                    <h4>Dari informasi manakah Anda mengetahui lowongan ini?</h4>
                </div>
                <div class="modal-body">
                    <select class="form-control" id="ddsurvayIklan" runat="server" datavaluefield="Name" datatextfield="Name" required="" data-attribute="Name" onchange="myFunction(this)" />

                    Tuliskan keterangan lainnya *:
                    <input class="form-control" type="text" name="note" id="note" runat="server" placeholder="Tuliskan keterangan lainnya" required="" readonly="" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="doCommitSurvay(); return false;">Submit</button>
                    <!--<button type="button" class="btn btn-default" data-dismiss="modal" onclick="doClose();">Close</button>-->
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="updatePasswordFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 data-lang="updatePasswordFrameModel_Title" class="modal-title center">Step 2. Please Create Password for next login without FB/Linked In</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="updatePasswordFrameModel_PasswordLabel" class="col-md-3 col-sm-3">
                            Password<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" type="password" data-attribute="Password" name="password" id="tbUpdatePassword" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="updatePasswordFrameModel_PasswordConfirmLabel" class="col-md-3 col-sm-3">
                            Confirm Password<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" type="password" data-attribute="Password" name="password" id="tbUpdatePasswordConfirm" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonPrev" class="btn btn-default" onclick="prevModalPersonalInfo(); return true;">Prev</button>
                    <button type="button" data-lang="ButtonNext" class="btn btn-default" onclick="saveAllData(); return false;">Next</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="personalInfoFrameModelRegister" style="overflow-y: auto;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 data-lang="personalInfoFrameModel_Title" class="modal-title center">Step 1. Please Complete Your Personal Information</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_FirstNameLabel" class="col-md-3 col-sm-3">
                            First Name<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control capitalLetter" type="text" required="" data-attribute="FirstName" id="FirstName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_LastNameLabel" class="col-md-3 col-sm-3">
                            Last Name
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control capitalLetter" type="text" data-attribute="LastName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_GenderLabel" class="col-md-3 col-sm-3">
                            Gender<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="GenderSpecification" class="form-control" id="GenderSpecification">
                                <option></option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_MaritalStatusLabel" class="col-md-3 col-sm-3">
                            Marital Status<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="MaritalStatusSpecification" class="form-control" id="MaritalStatusSpecification">
                                <option></option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Divorce</option>
                            </select>

                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_BirthDateLabel" class="col-md-3 col-sm-3">
                            Birth Date<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="controls">
                                <div class="input-group date col-sm-9">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" data-date-format="dd/mm/yyyy" required=""
                                        data-attribute="Birthday"
                                        class="form-control datepicker" id="Birthday" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_BirthPlaceLabel" class="col-md-3 col-sm-3">
                            Birth Place<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" required="" data-attribute="BirthPlace" id="BirthPlace" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang=" " class="col-md-3 col-sm-3">
                            Email<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <input disallowed-chars="[^a-zA-Zs ]+" data-attribute="Email"
                                required=""
                                class="form-control" type="text" id="Email" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_PhoneLabel" class="col-md-3 col-sm-3">
                            Phone<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input disallowed-chars="[^a-zA-Zs ]+" data-attribute="Phone" required="" class="form-control plainnumber" type="text" id="Phone" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_ReligionLabel" class="col-md-3 col-sm-3">
                            Religion<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="Religion" class="form-control" id="Religion">
                                <option></option>
                                <option>Moslem</option>
                                <option>Christian</option>
                                <option>Catholic</option>
                                <option>Hindhu</option>
                                <option>Buddha</option>
                                <option>Kong Hu cu</option>
                                <option>Kepercayaan</option>
                            </select>

                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_NationalityLabel" class="col-md-3 col-sm-3">
                            Nationality<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="Nationality" id="ddNationality" datavaluefield="Code" datatextfield="Name" runat="server" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_IDNumberLabel" class="col-md-3 col-sm-3">
                            ID Number<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input disallowed-chars="[^a-zA-Zs ]+" data-attribute="IDCardNumber"
                                required=""
                                class="form-control" type="text" id="IDCardNumber" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_AddressLabel" class="col-md-3 col-sm-3">
                            Address<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <textarea class="form-control" required="" data-attribute="CurrentCardAddress" id="tbAddress"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_CityLabel" class="col-md-3 col-sm-3">
                            City<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select id="ddCity" runat="server" datavaluefield="CityCode" data-attribute="CurrentCardCity" datatextfield="CityName" class="form-control"></select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_DesiredSalaryLabel" class="col-md-3 col-sm-3">
                            Desired Salary
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input data-attribute="ExpectedSalary" class="form-control numeric" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_PlacedInDifferentCitiesLabel" class="col-md-4 col-sm-4">
                            Placed in Different Cities?
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="EligibleForAnyCities" class="form-control">
                                <option></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_PriorNotificationLabel" class="col-md-5 col-sm-5">
                            Prior Notification Period (Months)
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control numeric" type="text" data-attribute="NoticePeriod" />
                        </div>
                        <div class="col-sm-2">
                            <i class="glyphicon glyphicon-exclamation-sign text-info" title="2 months, means, your first day will be 2 months after signing the offering letter."></i>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_FacebookLabel" class="col-md-3 col-sm-3">
                            Facebook
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input data-attribute="FacebookUrl" class="form-control" type="text" />
                            <input data-attribute="FacebookID" id="FacebookID" type="hidden" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_TwitterLabel" class="col-md-3 col-sm-3">
                            Twitter
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input data-attribute="TwitterUrl" class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_LinkedInLabel" class="col-md-3 col-sm-3">
                            Linked IN
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input data-attribute="LinkedInUrl" class="form-control" type="text" />
                            <input data-attribute="LinkedInID" id="LinkedInID" type="hidden" />
                            <input data-attribute="LinkedInImageUrl" id="LinkedInImageUrl" type="hidden" />
                            <input data-attribute="SocMedName" id="SocMedName" type="hidden" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="btnPrevSignUp" data-lang="ButtonPrev" class="btn btn-default hidden" onclick="showSignUp(); return false;">Prev</button>
                    <button type="button" data-lang="ButtonNext" class="btn btn-default" onclick="nextModalPassword2(); return false;">Next</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="completedRegistration">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 data-lang="completedRegistrationFrameModel_Title" class="modal-title center">Step 3. Registration Completed</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <h4 data-lang="completedRegistrationFrameModelNotif_Title" class="modal-title center">Registration Succeeded. Your e-mail has been registered for all of business unit KOMPAS-GRAMEDIA. Please click 'Completed' Button to Continue Login.</h4>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonCompleted" class="btn btn-default" onclick="showNotif(); return false;">Completed</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="appModeSingleFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title center">Information</h1>
                </div>
                <div class="modal-body" id="informationContainer1" style="height: 400px; overflow-y: auto;">
                    <asp:Literal ID="litInformationSingle" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="registerUser(); return false;">Continue Login</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="appModeMultiFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title center">Information</h1>
                </div>
                <div class="modal-body" id="informationContainer2" style="height: 400px; overflow-y: auto;">
                    <asp:Literal ID="litInformationMulti" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="registerUser(); return false;">Continue Login</button>
                </div>
            </div>
        </div>
    </div>

    <asp:ObjectDataSource ID="odsProfile"
        runat="server"
        DataObjectTypeName="ERecruitment.Domain.Applicants"
        TypeName="ERecruitment.Domain.ApplicantManager"
        SelectMethod="GetApplicant"
        OnUpdated="odsProfile_Updated"
        UpdateMethod="UpdateApplicant"
        InsertMethod="SaveApplicant"
        OnInserted="odsProfile_Inserted">
        <SelectParameters>
            <asp:QueryStringParameter Name="code" QueryStringField="code" />
        </SelectParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="odsJobCategories"
        runat="server"
        TypeName="ERecruitment.Domain.ERecruitmentManager"
        DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
        SelectMethod="GetVacancyCategoryList" />
    <asp:ObjectDataSource ID="odsCity"
        runat="server"
        TypeName="ERecruitment.Domain.ERecruitmentManager"
        DataObjectTypeName="ERecruitment.Domain.VacancyCategories"
        SelectMethod="GetCityList" />


    <asp:ObjectDataSource ID="odsJobList"
        runat="server"
        TypeName="ERecruitment.Domain.ERecruitmentManager"
        DataObjectTypeName="ERecruitment.Domain.Vacancies"
        SelectMethod="GetPostedVacancyList">
        <SelectParameters>
            <asp:ControlParameter Name="companyCode" ControlID="ddCompany" PropertyName="SelectedValue" />
            <asp:QueryStringParameter Name="jobCategoryCode" QueryStringField="jobCategoryCode" />
            <asp:QueryStringParameter Name="locationCode" QueryStringField="locationCode" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
