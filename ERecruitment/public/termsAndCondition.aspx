﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="termsAndCondition.aspx.cs" Inherits="ERecruitment.termsAndCondition" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    This Web Site (and any other site accessed through this site) and the services provided therein are only available to users seeking employment ("Jobseekers") and are subject to the Terms and Conditions which are set out below. If you do not accept the Terms and Conditions, please do not use this Web Site. Upon your acceptance of the Terms and Conditions, they are binding upon you. Daya5 Rekrutmen ("the Company") may revise the Terms and Conditions at anytime by updating this section. You are advised to visit this page periodically to review the Terms and Conditions. Your access to the Company's Web Site and its services will be terminated upon your notice to the Company that any change is unacceptable otherwise your continued use will constitute your acceptance of all changes and they will be binding upon you.
        <br /><br />
1. Acceptable Web Site Uses<br />

a. Specific Uses - Jobseekers<br />
Jobseekers agree that they will only use the Company's Web Sites 
(and any other sites accessed through them) for lawful purposes and 
to seek employment. Jobseekers may send their resume through the Company Web Sites. 
Alternatively the Jobseeker 
 may send his resume to the Company through the Company's Web Sites 
 in reply to any job advertisement. In such event the Jobseeker's 
  resume will also remain active on the 
        Company's Web Sites for a predetermined period and, \
        unless the Jobseeker chooses to delete it, it will thereafter 
        be stored in the Company's Database at the Jobseeker's own risk. 
        The Company also reserves the right to edit any Jobseeker's resume as it sees fit. 
        Jobseekers accept that personal data are given entirely at their own risk.<br />
        <br />
b. Prohibited Uses - Jobseekers<br />
Jobseekers agree not to use any of the Company's Web Sites for any of the following purposes which are expressly prohibited:- <br />
  i.   Jobseekers shall not post any non-resume information and/or incomplete, false or inaccurate resume information which is not their own accurate resume.<br />
  ii.  Jobseekers shall not respond to any employment opportunity for any reason other than to apply for the job advertised. <br />
  iii. All users are prohibited from violating or attempting to violate the security of the Company's Web Sites 
        (or any other sites accessed through the Company's       Web Sites) including, 
        without limitation, accessing data not intended for
         them or logging into a server or account which they are not authorized to access,       
        attempting to probe, scan or test the vulnerability of a system or network or attempting to breach security or 
        authentication measures without proper  
        authorization, attempting to interfere with service to any user, 
        host or network or sending unsolicited e-mail, 
        including promotions and/or advertising of products or services, Violations of system or network security may result in civil or criminal liability. <br />
  iv.  All users will not use the Company's Web Sites in order to transmit, distribute or store material in violation of any applicable law or regulation, 
        or in any manner that will infringe the copyright, trademark, 
        trade secrets or other intellectual property rights of others or violate 
        the privacy or publicity or other personal rights of others, or that is libelous, obscene, threatening, abusive or hateful.<br />
        <br />
2. Copyright, Trademarks etc.<br />
The graphics, images, editorial content and HTMC on all the Company's Web Sites 
        are the intellectual property of the Company and are protected by 
        copyright and trademark laws and may not be downloaded or 
        otherwise duplicated without the express written permission 
        of the Company. Re-use of any of the foregoing is strictly prohibited and the Company reserves all rights. 
        <br />
        <br />
3. Responsibility <br />
The Company may not monitor its Web Sites at all times but reserves the right to do so. 
        The Company takes no responsibility whatsoever for any material input by any user 
        or others and not posted by the Company. All users acknowledge and agree that t
        hey are solely responsible for the form, content and accuracy of any resume, 
        advertisement, web page or material contained therein placed by them. T
        he Company is not responsible for the content of any other web sites linked to the 
        Company's Web Sites; links are provided as internet navigation tools only. 
        The Company does not warrant that any resume, advertisement or web page will be viewed 
        by any specific number of users or that it will be viewed by any specific user. 
        The Company is not in anyway to be considered to be an agent with respect to any user 
        use of the Company's Web Sites and the Company will not be responsible in any way for any decision, 
        for whatever reason made, made by any party seeking or posting jobs on the Company's Web Sites. 
        THE COMPANY DOES NOT WARRANT THAT ITS WEB SITES WILL OPERATE ERROR-FREE 
        OR THAT ITS WEB SITES AND ITS SERVER ARE FREE OF VIRUSES OR OTHER HARMFUL MECHANISMS. 
        IF USE OF THE WEB SITES OR THEIR CONTENTS RESULT IN THE NEED FOR SERVICING OR REPLACING EQUIPMENT OR DATA, 
        BY ANY USER, THE COMPANY WILL NOT RESPONSIBLE FOR THOSE COSTS. 
        THE COMPANY'S WEB SITES AND THEIR CONTENTS ARE PROVIDED ON AN "AS IS" BASIS WITHOUT 
        ANY WARRANTIES OF ANY KIND. TO THE FULLEST EXTENT PERMITTED BY LAW, THE COMPANY DISCLAIMS ALL WARRANTIES, 
        INCLUDING WITHOUGH PREJUDICE TO THE FOREGOING, ANY IN RESPECT OF MERCHANTABILITY, 
        NON-INFERINGEMENT OF THIRD PARTY RIGHTS, FITNESS FOR PARTICULAR PURPOSE, OR ABOUT THE ACCURACY, 
        RELIABILITY COMPLETENESS OR TIMELINESS OF THE CONTENTS, SERVICES, SOFTWARE, TEXT, GRAPHICS AND LINKS OF THE COMPANY'S WEB SITES. 
        <br />
<br />
4. Own Risk<br />
All USERS USE THE COMPANY'S WEB SITES AND ANY OTHER WEB SITES ACCESSED THROUGH IT, 
        AT ENTIRELY THEIR OWN RISK. ALL users are responsible for their own communications and are responsible for the consequences of their posting. 
        The Company does not represent or guarantee the truthfulness, accuracy or reliability of any of the communications posted by 
        other users or endorses any opinions expressed by users. Any reliance by users on material posted by other users will be at their own risk. 
        The Company reserves the right to expel any user and prevent their further access to the Company's Web Sites, at any time 
        for breaching this Agreement or violating the law and also reserves the right to remove any material which is abusive, illegal or disruptive.
        <br />
<br />
5. Links to Other Sites<br />
The Company's Web Sites may contain links to third party Web Sites. 
        These are provided solely as a convenience to you and 
        not in any way as an endorsement by the Company of the contents on such third-party Web Sites. 
        If any user accesses any linked third party Web Sites, they do so entirely at their own risk. 
        The Company is not responsible for the content of any third party Web Sites linked to its Web Sites and 
        does not make any representations or warranties regarding the contents or accuracy of materials on such third-party Web Sites. 
        Further any material or details posted in the Company Web Sites by any user may be viewed by users of other web sites linked to 
        the Company's Web Sites and the Company will not be responsible for any improper use by any user or third party from linked third 
        party web sites of any data or materials posted on the Company's Web Sites. <br />
<br />
6. Indemnity <br />
All users agree to indemnify, and hold harmless the Company, 
        its officers, directors, employees and agents from and against any claims, 
        actions, demands, losses or damages arising from or resulting from their use 
        of the Company's Web Sites or their breach of the terms of this Agreement. 
        The Company shall provide prompt notice of any such claim, suit or proceeding to the relevant user. 
        <br />
<br />
7. Disclaimer <br />
THE COMPANY, ITS OFFICERS, DIRECTORS, EMPLOYEES OR AGENTS SHALL NOT BE LIABLE IN ANY EVENT FOR ANY LOSSES OR DAMAGES 
        SUFFERED BY ANY USER WHATSOEVER ARISING OR RESULTING FROM THEIR USE OR INABILITY TO USE THE COMPANY'S WEB SITES AND ITS CONTENTS. 
        <br />
<br />
8. Limitation of Liability <br />
Without prejudice to the above, 
        the aggregate liability by the Company to any user for all claims arising from their 
        use of the Company's services and the Company's Web Sites shall be limited to the amount of 
        the existing package fee subscribed to and paid for by the user (if any) and operative at that time. 
        <br />
<br />
9. Governing Law and Jurisdiction <br />
This Agreement is governed by the laws of the Republic of Indonesia and users hereby submit to the jurisdiction of the Indonesia courts.
  



    </div>
    </form>
</body>
</html>
