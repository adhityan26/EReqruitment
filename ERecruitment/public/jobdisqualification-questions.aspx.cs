﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class jobdisqualification_questions : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                UserAccounts getUserAccount = Session["currentuser"] as UserAccounts;

                ApplicationSettings applicationSetting = ERecruitmentManager.GetApplicationSetting();
                if (applicationSetting.VacancyApplicationMode == "Single")
                {
                    List<string> activeApplicationStatusList = VacancyManager.GetNonArchiveRecruitmentProcessCodeList();

                    List<ApplicantVacancies> activeApplicantVacancyList = ERecruitmentManager.GetApplicantVacancyListByApplicantCode(getUserAccount.Code, activeApplicationStatusList.ToArray());
                    if (activeApplicantVacancyList.Count > 0)
                    {
                        JQueryHelper.InvokeJavascript("showNotification('You can only have one active application'); window.close();", Page);
                        return;
                    }
                }

                List<DisqualifierQuestions> questionList = ERecruitmentManager.GetVacancyQuestionList(Utils.GetQueryString<string>("vacancyCode"));
                repQuestion.DataSource = questionList;
                repQuestion.DataBind();

                ddInformationSource.DataSource = ERecruitmentManager.GetApplicantVacancySources();
                ddInformationSource.DataValueField = "Code";
                ddInformationSource.DataTextField = "Name";
                ddInformationSource.DataBind();

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool _correctAnswer = true;

            #region disqualifier question
            foreach (RepeaterItem item in repQuestion.Items)
            {
                ApplicantDisqualifierAnswers applicantAnswer = new ApplicantDisqualifierAnswers();

                HiddenField hidCode = item.FindControl("hidCode") as HiddenField;
                TextBox tbNumericAnswer = item.FindControl("tbNumericAnswer") as TextBox;
                DropDownList ddMultipleChoiceAnswer = item.FindControl("ddMultipleChoiceAnswer") as DropDownList;

                DisqualifierQuestions question = DisqualificationQuestionManager.GetDisqualificationQuestion(hidCode.Value);
                string answer = string.Empty;
                // get answer
                if (question.AnswerFormat == "MultipleChoice")
                {
                    answer = Utils.GetSelectedValue<string>(ddMultipleChoiceAnswer);

                    if (answer != question.CorrectAnswerLabel)
                        _correctAnswer = false;
                }
                else
                {
                    answer = tbNumericAnswer.Text;

                    #region checking numeric answer
                    _correctAnswer = false;
                    int numericAnswer = Utils.ConvertString<int>(answer);
                    int correctNumericAnswer = Utils.ConvertString<int>(question.CorrectAnswerLabel);
                    switch (question.CorrectAnswerOperator)
                    {
                        case "=":
                            if (numericAnswer == correctNumericAnswer)
                                _correctAnswer = true;
                            break;
                        case ">":
                            if (numericAnswer > correctNumericAnswer)
                                _correctAnswer = true;
                            break;
                        case "<":
                            if (numericAnswer < correctNumericAnswer)
                                _correctAnswer = true;
                            break;
                        case ">=":
                            if (numericAnswer >= correctNumericAnswer)
                                _correctAnswer = true;
                            break;
                        case "<=":
                            if (numericAnswer <= correctNumericAnswer)
                                _correctAnswer = true;
                            break;
                    }

                    #endregion
                }

                applicantAnswer.Answer = answer;
                applicantAnswer.DisqualifierCode = question.Code;
                applicantAnswer.VacancyCode = Utils.GetQueryString<string>("vacancyCode");
                UserAccounts getUserAccount = Session["currentuser"] as UserAccounts;
                applicantAnswer.ApplicantCode = getUserAccount.Code;
                DisqualificationQuestionManager.SaveDisqualifierAnswer(applicantAnswer);
            }

            #endregion

            if (Session["currentuser"] != null)
            {
                UserAccounts getUserAccount = Session["currentuser"] as UserAccounts;
                //UserAccounts getUserAccount = AuthenticationManager.GetUserAccount(Utils.GetQueryString<string>("appCode"));
                string vacancyCode = Utils.GetQueryString<string>("vacancyCode");
                if (getUserAccount != null && !string.IsNullOrEmpty(vacancyCode))
                {
                    Applicants applicant = ApplicantManager.GetApplicant(getUserAccount.Code);

                    if (applicant != null)
                    {
                        //check id card number
                        #region check id card
                        if (string.IsNullOrEmpty(applicant.IDCardNumber))
                        {
                            JQueryHelper.InvokeJavascript("showNotification('Please update your ID Card number to apply jobs');window.close();", Page);
                            return;
                        }
                        #endregion
                        else
                        {
                            ApplicantVacancies checkVacancy = ERecruitmentManager.GetApplicantVacancies(applicant.Code, vacancyCode);
                            if (checkVacancy == null)
                            {

                                try
                                {
                                    Vacancies jobReq = ERecruitmentManager.GetVacancy(vacancyCode);

                                    ERecruitmentManager.SaveApplicantVacancy(applicant.Code, vacancyCode, _correctAnswer, ddInformationSource.Value, tbInformationSourceNotes.Text);

                                    JQueryHelper.InvokeJavascript("showNotification('Job Applied'); window.opener.location.href = window.opener.location.href; window.close();", Page);
                                }
                                catch
                                {
                                    JQueryHelper.InvokeJavascript("showNotification('Ops.. sorry something wrong');", Page);
                                    Response.Redirect(MainCompany.ATSBaseUrl + "public/joblist.aspx");
                                }
                            }
                        }
                    }
                }

            }
            else
            {
                JQueryHelper.InvokeJavascript("showNotification ('please login for apply a job');window.location.href='" + MainCompany.ATSBaseUrl + "default.aspx';", Page);
            }
        }


        protected void repQuestion_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                DisqualifierQuestions question = e.Item.DataItem as DisqualifierQuestions;
                TextBox tbNumericAnswer = e.Item.FindControl("tbNumericAnswer") as TextBox;
                DropDownList ddMultipleChoiceAnswer = e.Item.FindControl("ddMultipleChoiceAnswer") as DropDownList;

                if (question.AnswerFormat == "MultipleChoice")
                {
                    tbNumericAnswer.Visible = false;
                    ddMultipleChoiceAnswer.Visible = true;
                    ddMultipleChoiceAnswer.DataSource = DisqualificationQuestionManager.GetAnswerOptionList(question.Code);
                    ddMultipleChoiceAnswer.DataBind();
                }
                else
                {
                    tbNumericAnswer.Visible = true;
                    ddMultipleChoiceAnswer.Visible = false;
                }
            }
        }
    }
}
