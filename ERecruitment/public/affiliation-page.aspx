﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="affiliation-page.aspx.cs" Inherits="ERecruitment.affiliation_page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {

            loadUniversityMajor();
        });

        function ShowTermsAndCondition()
        {
      

            var left = (screen.width / 2) - (800 / 2);
            var top = (screen.height / 2) - (450 / 2);
            var foo = window.open('termsAndCondition.aspx', 'terms and condition', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no, width=800, height=490, top=' + top + ', left=' + left);

        }

        function loadUniversityMajor()
        {
            var ddAffiliatedUniversity = '<%= formProfile.FindControl("ddAffiliatedUniversity").ClientID %>';
            var ddMajor = '<%= formProfile.FindControl("ddMajor").ClientID %>';

            var universityCode = $("#" + ddAffiliatedUniversity).val();

            var param = "&universityCode=" + locationCode;

            var url = "../handlers/HandlerApplicants.ashx?commandName=GetUniversityMajor" + param;

            $.ajax({
                url: url,
                async: false,
                beforeSend: function () {
                    $("#" + ddMajor).after('<span class="loadingIndicator"></span>');
                },
                success: function (queryResult) {
                    var dataList = queryResult.toString().split("|");
                    $("#" + ddMajor).children().remove();
                    $("#" + ddMajor).append('<option value="">Select an Item</option>');
                    for (var i = 0; i < dataList.length; i++) {
                        var dataPair = dataList[i].split(","); if (typeof dataPair[1] === "undefined") continue;
                        if (typeof dataPair[1] === "undefined") continue;
                        $("#" + ddMajor).append('<option value="' + dataPair[0] + '">' + dataPair[1] + '</option>');
                    }
                    $(".loadingIndicator").remove();
                }
            });
        }

    </script>
    

    <asp:FormView ID="formProfile" runat="server" 
                  DataSourceID="odsProfile"
                  OnItemUpdating="formProfile_ItemUpdating"
                  OnItemInserting="formProfile_ItemInserting"
                  OnDataBound="formProfile_DataBound" RenderOuterTable="false"
                  DataKeyNames="Code,Active,RegisteredDate,Email,Address,GenderSpecification,"
                  DefaultMode="Insert">
        <InsertItemTemplate>
            <asp:HiddenField ID="hidSocMedName" runat="server" Value='<%# Bind("SocMedName") %>' />
            <asp:HiddenField ID="hidSocMedId" runat="server" Value='<%# Bind("SocMedID") %>' />
            <div>
              <div class="row">
                <div class="col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">

                    
                    <form disabled="disabled" action="" 
                        class="ng-dirty ng-valid-parse ng-invalid ng-invalid-required ng-valid-minlength ng-valid-disallowed-chars ng-valid-maxlength ng-invalid-email ng-invalid-email-available ng-valid-matches"
                         method="post" enctype="multipart/form-data" autocomplete="false" 
                        ng-class="{'submitted' : registerForm.$submitted}" 
                        id="form" ng-disabled="registerForm.$invalid" novalidate="" js-form="" name="registerForm">
                        <input id="linkedin" value="0" name="linkedin" ng-model="registerFormData.linkedin"
                         form-field="registerForm" class="ng-pristine ng-untouched ng-valid ng-valid-parse" type="hidden"/>
                        
                        <div class="row">
                            <label>University</label><span class="requiredmark">*</span>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <asp:DropDownList ID="ddAffiliatedUniversity"
                                                  runat="server"
                                                  DataValueField="Code"
                                                  DataTextField="Name"
                                                  CssClass="form-control"
                                                  DataSourceID="odsAffiliatedUniversity" onchange="loadUniversityMajor();"
                                                  />
                                </div>
                            </div>
                            <label>Major</label><span class="requiredmark">*</span>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                <asp:DropDownList ID="ddMajor"
                                                  runat="server"
                                                  CssClass="form-control"
                                                  />
                                </div>
                            </div>
                        </div>
                            
                        <div class="row">
                            <label>First Name</label><span class="requiredmark">*</span>
                        
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <input disallowed-chars="[^a-zA-Zs ]+" placeholder="First Name" id="tbFirstName"
                                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                                        ng-minlength="3" min-length="3" required runat="server"
                                        class="form-control" type="text" />
                                    <div class="visible-xs-inline">
                                        <p class="space10">
                                            <span original-message="" form="registerForm" model-name="name"
                                                class="label label-danger validation-error ng-binding ng-isolate-scope"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <label>Last Name</label><span class="requiredmark">*</span>
                        
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <input disallowed-chars="[^a-zA-Zs ]+" required placeholder="Last Name" id="tbLastName"
                                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                                        ng-minlength="3" min-length="3" runat="server"
                                        class="form-control" type="text"/>
                                    <div class="visible-xs-inline">
                                        <p class="space10">
                                            <span original-message="" form="registerForm" model-name="name"
                                                class="label label-danger validation-error ng-binding ng-isolate-scope"></span>
                                        </p>
                                    </div>
                                </div>
                            </div>

                            <label>Email Address</label><span class="requiredmark">*</span>

                            <div class="row">
                                <div class="col-xs-12">
                                    <input placeholder="Email Address (format: mail@example.com)"
                                            data-container="body" runat="server"
                                            data-placement="top"
                                            data-template="&lt;div class=&quot;popover&quot; role=&quot;tooltip&quot;&gt;&lt;div class=&quot;arrow&quot;&gt;&lt;/div&gt;&lt;div class=&quot;popover-body&quot;&gt;&lt;div class=&quot;popover-content-override&quot;&gt;Maksud Anda&lt;/div&gt; &lt;span class=&quot;popover-content bold&quot;&gt;&lt;/span&gt;&lt;span&gt;?&lt;/span&gt;&lt;div class=&quot;popover-content-override&quot;&gt;&lt;a onClick=&quot;$('#email').val($('.popover-content').text());$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-check fa-stack-1x fa-inverse&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;a onClick=&quot;$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle-thin fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-times fa-stack-1x text-muted&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;"
                                            id="tbEmailAddress"
                                            ng-model="registerFormData.email"
                                            form-field="registerForm" type="text"
                                            pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"
                                            required
                                            class="form-control auto-lower-case auto-no-space suggest-email ng-pristine ng-untouched 
                                        ng-invalid ng-invalid-email ng-invalid-required ng-invalid-email-available" />

                                </div>

                            </div>

                            <label>Password</label><span class="requiredmark">*</span>
                        
                            <div class="row">
                                <div class="col-xs-12">
                                    <input placeholder="Password"
                                        data-container="body" runat="server"
                                        data-placement="top"
                                        data-template="&lt;div class=&quot;popover&quot; role=&quot;tooltip&quot;&gt;&lt;div class=&quot;arrow&quot;&gt;&lt;/div&gt;&lt;div class=&quot;popover-body&quot;&gt;&lt;div class=&quot;popover-content-override&quot;&gt;Maksud Anda&lt;/div&gt; &lt;span class=&quot;popover-content bold&quot;&gt;&lt;/span&gt;&lt;span&gt;?&lt;/span&gt;&lt;div class=&quot;popover-content-override&quot;&gt;&lt;a onClick=&quot;$('#email').val($('.popover-content').text());$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-check fa-stack-1x fa-inverse&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;a onClick=&quot;$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle-thin fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-times fa-stack-1x text-muted&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;"
                                        id="tbPassword" type="password"
                                        ng-model="registerFormData.email"
                                        form-field="registerForm"
                                        required
                                        class="form-control auto-lower-case auto-no-space suggest-email ng-pristine ng-untouched ng-invalid ng-invalid-email ng-invalid-required ng-invalid-email-available" />

                                </div>

                            </div>

                            <label>Confirm Password</label><span class="requiredmark">*</span>
                        
                            <div class="row">
                                <div class="col-xs-12">
                                    <input placeholder="Confirm Password"
                                        data-container="body" runat="server"
                                        data-placement="top"
                                        data-template="&lt;div class=&quot;popover&quot; role=&quot;tooltip&quot;&gt;&lt;div class=&quot;arrow&quot;&gt;&lt;/div&gt;&lt;div class=&quot;popover-body&quot;&gt;&lt;div class=&quot;popover-content-override&quot;&gt;Maksud Anda&lt;/div&gt; &lt;span class=&quot;popover-content bold&quot;&gt;&lt;/span&gt;&lt;span&gt;?&lt;/span&gt;&lt;div class=&quot;popover-content-override&quot;&gt;&lt;a onClick=&quot;$('#email').val($('.popover-content').text());$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-check fa-stack-1x fa-inverse&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;a onClick=&quot;$('.popover').popover('destroy');&quot;&gt;&lt;span class=&quot;fa-stack fa-lg&quot;&gt;&lt;i class=&quot;fa fa-circle-thin fa-stack-2x&quot;&gt;&lt;/i&gt;&lt;i class=&quot;fa fa-times fa-stack-1x text-muted&quot;&gt;&lt;/i&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;"
                                        id="tbConfirmPassword" type="password"
                                        ng-model="registerFormData.email"
                                        form-field="registerForm"
                                        required=""
                                        class="form-control auto-lower-case auto-no-space suggest-email ng-pristine ng-untouched ng-invalid ng-invalid-email ng-invalid-required ng-invalid-email-available" />

                                </div>
                            </div>

                                    <label>Phone Number</label><span class="requiredmark">*</span>
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <input disallowed-chars="[^a-zA-Zs ]+"
                                            placeholder="Phone Number (Format: 999999)" 
                                        id="tbPhoneNumber"
                                        name="name" ng-model="registerFormData.name"
                                            form-field="registerForm"
                                        ng-minlength="3" min-length="3" required="" 
                                        runat="server" 
                                        pattern="^[0-9]{6,20}"
                                           
                                        class="form-control" type="text"/>
                                        
                                </div>
                            </div>

                                <label>Alternative Phone Number</label>
                                <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <input disallowed-chars="[^a-zA-Zs ]+" placeholder="Alternative Phone Number (Format: 999999)" id="tbAlternativePhoneNumber"
                                        name="name"   form-field="registerForm"  pattern="^[0-9]{6,20}"
                                        ng-minlength="3" min-length="3" runat="server" 
                                        class="form-control" type="text" />
                                       
                                </div>
                            </div>
                        </div>
                            
                        <div class="row">
                                
                            <div class="panel-footer">
                                <label><a href="#" onclick="ShowTermsAndCondition();return false;">Terms and Conditions</a></label>
                                <br />
                                <div class="row">

                                    <div class="col-sm-12 col-md-12">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    </div>

                                </div>   
                                                             
                            </div>
                            <div class="row">
                                <div class="row">
                                    <div class="col-sm-12 col-md-12">
                                    <input id="cbTOC" runat="server" type="checkbox" />
                                    <label>I accept Terms and Conditions above</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                 <div class="row">
                                    <div class="col-sm-6 col-md-4">
                                        <asp:Image ID="imgCaptcha" runat="server" />
                                    </div>
                                    <div class="col-sm-6 col-md-4">
                                        <input disallowed-chars="[^a-zA-Zs ]+" placeholder="Enter Code"
                                            id="tbCaptcha" name="year" ng-model="registerFormData.name" runat="server"
                                            form-field="registerForm" ng-minlength="3" min-length="3" required=""
                                            class="form-control" type="text" />
                                        <div class="visible-xs-inline">
                                            <p class="space10">
                                                <span original-message="" form="registerForm" model-name="Year"
                                                    class="label label-danger validation-error ng-binding ng-isolate-scope"></span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                
                            <div class="row">
                                <div class="col-sm-12 col-md-12">
                                    <button class="btn btn-primary btn-lg btn-block" 
                                        runat="server" onserverclick="nextbutton_ServerClick" 
                                        id="nextbutton">
                                        Register
                                        <i class="fa fa-arrow-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                  </form>
                </div>
              </div>
            </div>


        </InsertItemTemplate>
    </asp:FormView>

    

    
    

    
<asp:ObjectDataSource ID="odsProfile"
                      runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Applicants"
                      TypeName="ERecruitment.Domain.AuthenticationManager"
                      SelectMethod="GetApplicant"
                      OnUpdated="odsProfile_Updated"
                      UpdateMethod="UpdateApplicant"
                      InsertMethod="SaveApplicant"
                      OnInserted="odsProfile_Inserted"
                >
      <SelectParameters>
          <asp:QueryStringParameter Name="code" QueryStringField="code" />
      </SelectParameters>
</asp:ObjectDataSource>

<asp:ObjectDataSource ID="odsAffiliatedUniversity"
                      runat="server"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      DataObjectTypeName="ERecruitment.Domain.Universities"
                      SelectMethod="GetAffiliatedUniversityList"
                      />

</asp:Content>
