﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace ERecruitment
{
    public partial class joblist : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                MainCompany.CompanyCodes += "," + MainCompany.Code;

                if (!string.IsNullOrEmpty(MainCompany.CompanyCodes))
                {
                    string[] userCompanyCodes = MainCompany.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    List<Companies> getCompanyAccessList = ERecruitmentManager.GetCompanyListByCodes(userCompanyCodes);
                    List<Companies> companyList = new List<Companies>();
                    foreach (Companies item in getCompanyAccessList)
                    {
                        if (companyList.Contains(item))
                            continue;
                        else
                            companyList.Add(item);
                    }

                    ddCompany.DataSource = companyList;
                    ddCompany.DataBind();
                }

                ddNationality.DataSource = ERecruitmentManager.GetNationalityList();
                ddNationality.DataBind();
                ddNationality.Items.Insert(0, new ListItem("Select an option", ""));

                ddCity.DataSource = ERecruitmentManager.GetCityList();
                ddCity.DataBind();
                ddCity.Items.Insert(0, new ListItem("Select an option", ""));

                ddsurvayIklan.DataSource = ERecruitmentManager.GetSourceReferencesList();
                ddsurvayIklan.DataBind();
                ddsurvayIklan.Items.Insert(0, new ListItem("Select an option", ""));

                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("categoryCode")))
                {
                    ddJobCategory.DataBind();
                    Utils.SelectItem<string>(ddJobCategory, Utils.GetQueryString<string>("categoryCode"));
                }

                hidJobDetailUrl.Value = MainCompany.ATSBaseUrl + "public/jobdetail.aspx";
                hidJobListUrl.Value = MainCompany.ATSBaseUrl + "public/joblist.aspx";

                #region setup filter

                ddCompany.DataBind();
                if (string.IsNullOrEmpty(Utils.GetQueryString<string>("companyCode")))
                    Utils.SelectItem<string>(ddCompany, MainCompany.Code);
                else
                    Utils.SelectItem<string>(ddCompany, Utils.GetQueryString<string>("companyCode"));

                ddJobCategory.DataBind();
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("jobCategoryCode")))
                    Utils.SelectItem<string>(ddJobCategory, Utils.GetQueryString<string>("jobCategoryCode"));

                ddJobLocation.DataBind();
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("locationCode")))
                    Utils.SelectItem<string>(ddJobLocation, Utils.GetQueryString<string>("locationCode"));


                #endregion

                #region binding job list

                repJobList.DataBind();

                #endregion

                #region captcha
                //System.Web.UI.WebControls.Image imgCaptcha = (System.Web.UI.WebControls.Image)FindControl("imgCaptcha");
                Random random = new Random();

                string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

                StringBuilder captcha = new StringBuilder();

                for (int i = 0; i < 6; i++)

                    captcha.Append(combination[random.Next(combination.Length)]);

                Session["captcha"] = captcha.ToString();
                imgCaptcha.ImageUrl =  "../handlers/HandlerCaptcha.ashx?" + DateTime.Now.Ticks.ToString();
                // if (imgCaptcha != null)
               // imgCaptcha.ImageUrl = "../handlers/HandlerCaptcha.ashx?" + DateTime.Now.Ticks.ToString();

                #endregion

                #region notifAppMode
                ApplicationConfiguration appConfig = new ApplicationConfiguration();
                appConfig = ERecruitmentManager.GetApplicationConfiguration(1);
                if (appConfig != null)
                    litInformation.Text = appConfig.ConfigValue;
               
                ApplicationConfiguration appConfig1 = new ApplicationConfiguration();
                appConfig1 = ERecruitmentManager.GetApplicationConfiguration(2);
                if (appConfig1 != null)
                    litInformationSingle.Text = appConfig1.ConfigValue;

                ApplicationConfiguration appConfig2 = new ApplicationConfiguration();
                appConfig2 = ERecruitmentManager.GetApplicationConfiguration(3);
                if (appConfig2 != null)
                    litInformationMulti.Text = appConfig2.ConfigValue;
                #endregion

                if (CurrentUser != null)
                    hidCurrUserCode.Value = CurrentUser.ApplicantCode;
            }
        }

        protected void repJobList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                Vacancies vacancy = e.Item.DataItem as Vacancies;
                HtmlImage imgLogo = e.Item.FindControl("imgLogo") as HtmlImage;
                imgLogo.Src = "../handlers/HandlerUI.ashx?commandName=GetCompanyLogo&code=" + vacancy.CompanyCode;

                Literal litSalaryRange = e.Item.FindControl("litSalaryRange") as Literal;
                if (vacancy.IsShowSalary)
                    litSalaryRange.Text = Utils.DisplayMoneyAmount(vacancy.SalaryRangeBottom) + " - " + Utils.DisplayMoneyAmount(vacancy.SalaryRangeTop);
                else
                    litSalaryRange.Text = "Undisclosed";

                if (CurrentUser != null)
                {
                    ApplicantVacancies applicantVacancy = VacancyManager.GetApplicantVacancy(vacancy.Code, CurrentUser.ApplicantCode);
                    if (applicantVacancy != null)
                    {
                        HtmlGenericControl applylink = e.Item.FindControl("applylink") as HtmlGenericControl;
                        HtmlGenericControl applied = e.Item.FindControl("applied") as HtmlGenericControl;

                        applylink.Visible = false;
                        applied.Visible = true;
                    }
                }
            }
        }

        protected void btnSearch_ServerClick(object sender, EventArgs e)
        {
            repJobList.DataBind();
        }

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            if (dd != null)
            {
                dd.Items.Insert(0, new ListItem("All", string.Empty));
            }
        }

        //protected void btnSignUp_ServerClick(object sender, EventArgs e)
        //{
        //    #region validate captcha

        //    HtmlInputControl tbCaptcha = formProfile.FindControl("tbCaptcha") as HtmlInputControl;
        //    string keyValidator = Session["captcha"] as string;
        //    if (tbCaptcha.Value != keyValidator)
        //    {
        //        JQueryHelper.InvokeJavascript("showNotification('Security code not match');", Page);
        //        return;
        //    }

        //    #endregion

        //    formProfile.InsertItem(true);
        //}

        //protected void formProfile_ItemInserting(object sender, FormViewInsertEventArgs e)
        //{
        //    HtmlInputControl tbEmailAddress = formProfile.FindControl("tbEmailAddress") as HtmlInputControl;
        //    HtmlInputControl tbPassword = formProfile.FindControl("tbPassword") as HtmlInputControl;

        //    HtmlInputControl tbFirstName = formProfile.FindControl("tbFirstName") as HtmlInputControl;
        //    HtmlInputControl tbLastName = formProfile.FindControl("tbLastName") as HtmlInputControl;

        //    e.Values["Email"] = tbEmailAddress.Value;
        //    e.Values["FirstName"] = tbFirstName.Value;
        //    e.Values["Name"] = tbFirstName.Value;
        //    e.Values["LastName"] = tbLastName.Value;
        //    e.Values["CompanyCode"] = MainCompany.Code;

        //    // user account
        //    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(Utils.GetQueryString<string>("code"));
        //    if (userAccount == null)
        //    {
        //        userAccount = new UserAccounts();

        //    }
        //    userAccount.Name = string.Concat(tbFirstName.Value, " ", tbLastName.Value);
        //    userAccount.UserName = tbEmailAddress.Value;
        //    userAccount.Password = tbPassword.Value;
        //    userAccount.IsApplicant = true;
        //    e.Values["Account"] = userAccount;
        //    e.Values["Active"] = true;
        //    e.Values["RegisteredDate"] = DateTime.Now;
        //}

        #region ods

        protected void odsProfile_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Response.Redirect("../private/update-profile.aspx?code=" + Utils.GetQueryString<string>("code"));
            }
        }

        protected void odsProfile_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Applicants applicant = e.ReturnValue as Applicants;
                if (!applicant.Verified)
                {
                    if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
                    {
                        Companies emailSetting = ERecruitmentManager.GetCompany(MainCompany.Code);
                        SmtpClient client = new SmtpClient();
                        client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                        client.Host = emailSetting.EmailSmtpHostAddress;
                        if (emailSetting.EmailSmtpUseSSL)
                            client.EnableSsl = true;
                        client.Timeout = 20000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                        string address = emailSetting.EmailSmtpEmailAddress;
                        string displayName = emailSetting.MailSender;

                        if (!address.Contains("@"))
                        {
                            address = displayName;
                        }
            
                        MailMessage mm = new MailMessage(new MailAddress(address,
                                displayName),
                                                                         new MailAddress(applicant.Email, ""));
                        mm.Subject = "Email verification from " + emailSetting.ApplicationTitle;
                        string body = @"<div style=padding:10px>
                         <div style='width:100%;border:solid 1px #A6C9E2;background-color:#ffffff'>
                             
                                <table>
                                    <tr><td>Dear [candidate],</td></tr>
                                </table> 
                                <table>
                                    <tr><td>Welcome to E-Recruitment Application</td></tr>
                                    <tr>
                                         <td>Your registration is completed, please activate your account within 1x24 hours by clicking the link below:</td>
                                    </tr>
                                    <tr>
                                        <td><a href=[link]>Activate my account</a></td>
                                    </tr>      
                                    <tr>
                                        <td>If you found this email in your Trash/Spam Folder, please click “NOT SPAM/JUNK” in order to ensure that our email not delivered to your Trash/Spam Folder.</td>
                                    </tr>
                                    <tr><td></td></tr>
                                    <tr>
                                       <td>Regards,</td>
                                    </tr>
                                     <tr><td></td></tr>
                                     <tr><td></td></tr>
                                     <tr><td>Recruitment Team</td></tr>
                                </table> 
                        </div>
                     </div>
                            ";
                        mm.Body = body;
                        string link = MainCompany.ATSBaseUrl + "/applicant/emailverificationcompleted.aspx?code=" + applicant.Code;
                        mm.Body = mm.Body.Replace("[link]", link).Replace("[candidate]", applicant.FirstName);
                        mm.BodyEncoding = UTF8Encoding.UTF8;
                        mm.IsBodyHtml = true;
                        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        client.Send(mm);
                    }
                    JQueryHelper.InvokeJavascript("window.showNotification('Registration completed, please check your email');window.close();window.location.href='" + MainCompany.ATSBaseUrl + "default.aspx';", Page);
                }
                else
                {
                    Session["currentuser"] = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(applicant.Code);

                    Response.Redirect("../applicant/update-profile.aspx?code=" + applicant.Code);
                }
            }
            else
            {
                e.ExceptionHandled = true;
                JQueryHelper.InvokeJavascript("showNotification('" + Utils.ExtractInnerExceptionMessage(e.Exception) + "');", Page);
            }
        }

        #endregion


    }
}