﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="ERecruitment.login" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
<script type="text/javascript">
        
  function registerFaceBook()
  {
      console.log("checking facebook login status");
      FB.login(function (response) {
          
          if (response.authResponse) {
              console.log('Welcome!  Fetching your information.... ');
              FB.api('/me', {fields: 'name,email,id,gender'}, function (apiResponse) {
                  console.log('Good to see you, ' + apiResponse.name + '.');
                  
                  var name = apiResponse.name;
                  var userEmail = apiResponse.email;
                  var userId = apiResponse.id;
                  var gender = apiResponse.gender;
                 
                  var param = "&name=" + name;
                  param += "&email=" + userEmail;
                  param += "&id=" + userId;
                  param += "&gender=" + gender;
                  param += "&appName=facebook";
                  // authenticate via facebook
                  console.log("authenticating..., param:" + param);
                  $.ajax({
                      url: "../handlers/HandlerAuthentication.ashx?commandName=loginsosmed" + param,
                      async: false,
                      beforeSend: function () {

                      },
                      success: function (queryResult) {
                          // redirect to job listing
                          window.location.href = queryResult;
                      },
                      error: function (xhr, ajaxOptions, thrownError) {

                          showNotification(xhr.responseText);

                      }
                  });

              });
          } else {
              console.log('User cancelled login or did not fully authorize.');
          }
      }, { scope: 'public_profile,email' });
      
  }

  function registerLinkedIn()
  {
      console.log("Checking linkedin");
      IN.User.authorize(function () {
          IN.API.Profile('me').fields([
              'id',
              'firstName',
              'lastName',
              'emailAddress'
          ]).result(function (profiles) {
              var me = profiles.values[0];
              

              var name = me.firstName + me.lastName;
              var userEmail = me.emailAddress;
              var userId = me.id;

              var param = "&name=" + name;
              param += "&email=" + userEmail;
              param += "&id=" + userId;
              param += "&appName=linkedin";
              // authenticate via linkedin
              console.log("authenticating..., param:" + param);
              $.ajax({
                  url: "../handlers/HandlerAuthentication.ashx?commandName=loginsosmed" + param,
                  async: false,
                  beforeSend: function () {

                  },
                  success: function (queryResult) {
                      // redirect to job listing
                      window.location.href = queryResult;
                  },
                  error: function (xhr, ajaxOptions, thrownError) {

                      showNotification(xhr.responseText);

                  }
              });
          });
      });
  }

  function registerUser() {
      // redirect to user profile
      window.location.href = "registration-form.aspx";
  }

  $(document).ready(function () {
      
  });
    
  $(document).keypress(function (e) {
      if (e.which == 13) {
          e.preventDefault();
          loginUser();
      }
  });

  function loginUser()
  {
      var param = "&email=" + $("#email").val();
      param += "&password=" + encodeURIComponent($("#password").val());
      if ($("#email").val() == "" || $("#password").val() == "")
          showNotification("All field must be filled");
      else {
          // manual registration
          $.ajax({
              url: "../handlers/HandlerAuthentication.ashx?commandName=login" + param,
              async: false,
              beforeSend: function () {

              },
              success: function (queryResult) {
                  // redirect to homepage
                  window.location.href = queryResult;
                  
              },
              error: function (xhr, ajaxOptions, thrownError) {
                  showNotification('invalid username or password');
                  window.location.href="login.aspx";
              }
          });
      }
  }

</script>
<div style="float:left;margin:5px; margin-top:14px; text-align:center; padding:15px; width:100%; height:100%;">
   <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:20px;">
        Login to <asp:Literal ID="litApplicationName" runat="server" /></h1>
  
  <div>
    <div class="form-group col-md-6 col-md-offset-3">
        <input class="form-control" type="text" data-attribute="email" name="email" id="email" placeholder="Email" />
    </div>
    <div class="form-group col-md-6 col-md-offset-3">
        <input class="form-control" type="password" data-attribute="password" name="password" id="password" placeholder="Password" />
    </div>
    <div class="col-md-6 col-md-offset-3">
        <button class="btn btn-primary btn-lg col-xs-12" id="btnDefault" onclick="loginUser(); return false;">
            Login
        </button>    
        <a href="/public/forgotpassword.aspx">Forgot Password ?</a> <br />
        <a href="../applicant/resendemailverification.aspx">Resend Email Verification</a>     
    </div>

    <div class="space10"></div>
    <div class="space10"></div>
    <div class="col-md-10 col-md-offset-1" >
    <button class="btn btn-lg btn-primary" id="btnLogin" style="display:none;"  onclick="registerUser(); return false;">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Register &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    </button>
    </div>

    <div class="space10"></div>
    <div class="col-md-10 col-md-offset-1">
    
    </div>
    <div class="space10"></div>
    <div class="col-md-10 col-md-offset-1">
    <button id="loginLinkedIn" runat="server" class="btn btn-lg btn-primary" onclick="registerLinkedIn(); return false;">
        <i class="fa fa-linkedin"></i>
        &nbsp;&nbsp;
     Login Using LinkedIn &nbsp;
    </button>
    <div class="space10"></div><br />
    <button id="loginFacebook" runat="server" class="btn btn-lg btn-primary" onclick="registerFaceBook(); return false;">
        <i class="fa fa-facebook"></i>
        &nbsp;
        Login Using Facebook
    </button>
    </div>
    </div>
</div>

<div id="status">
</div>
</asp:Content>