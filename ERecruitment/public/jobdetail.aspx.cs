﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Net;
using System.Net.Mail;
using System.Configuration;

namespace ERecruitment
{
    public partial class jobdetail : BasePage
    {
        #region init
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Vacancies vacancy = ERecruitmentManager.GetVacancy(Utils.GetQueryString<string>("vacancyCode"));
                
                HtmlHead objHead = new HtmlHead();
                this.Title = vacancy.PositionName;
                objHead = (HtmlHead)Master.FindControl("master_head");
                HtmlMeta objMetaDescription = (HtmlMeta)objHead.FindControl("og_image");
                objMetaDescription.Content = "../handlers/HandlerUI.aspx?commandName=GetCompanyLogo=" + vacancy.CompanyCode;
                hidJobDetailUrl.Value = MainCompany.ATSBaseUrl + "public/jobdetail.aspx";

                if (Session["currentuser"] != null)
                {
                    UserAccounts getUserAccount = Session["currentuser"] as UserAccounts;
                    if (getUserAccount != null && getUserAccount.ApplicantCode != null)
                    {
                        Applicants getApplicant = ApplicantManager.GetApplicant(getUserAccount.ApplicantCode);

                        ApplicantVacancies checkApplicantVacancy = ERecruitmentManager.GetApplicantVacancies(getApplicant.Code, Utils.GetQueryString<string>("vacancyCode"));
                        if (checkApplicantVacancy != null)
                        {
                            HtmlButton applyButton = formJobDetail.FindControl("applyButton") as HtmlButton;
                            applyButton.Disabled = true;
                            applyButton.InnerText = "Applied";
                        }
                    }
                }

                Companies company = ERecruitmentManager.GetCompany(vacancy.CompanyCode);


                HtmlImage companyLogo = formJobDetail.FindControl("companyLogo") as HtmlImage;

                if (company.Logo != null)
                {
                    companyLogo.Src = "../handlers/HandlerUI.ashx?commandName=GetCompanyLogo&code=" + vacancy.CompanyCode;
                }
                else
                {
                    Companies mainCompany = null;
                    if (Session["maincompany"] != null)
                        mainCompany = Session["maincompany"] as Companies;

                    if (mainCompany == null)
                    {
                        mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(Request.Url.Authority);
                        Session["maincompany"] = mainCompany;
                    }
                }


                #region captcha
                //System.Web.UI.WebControls.Image imgCaptcha = (System.Web.UI.WebControls.Image)FindControl("imgCaptcha");
                Random random = new Random();

                string combination = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

                StringBuilder captcha = new StringBuilder();

                for (int i = 0; i < 6; i++)

                    captcha.Append(combination[random.Next(combination.Length)]);

                Session["captcha"] = captcha.ToString();
                imgCaptcha.ImageUrl = "../handlers/HandlerCaptcha.ashx?" + DateTime.Now.Ticks.ToString();
                // if (imgCaptcha != null)
                // imgCaptcha.ImageUrl = "../handlers/HandlerCaptcha.ashx?" + DateTime.Now.Ticks.ToString();

                #endregion

                #region notifAppMode
                ApplicationConfiguration appConfig = new ApplicationConfiguration();
                if (appConfig != null)
                    appConfig = ERecruitmentManager.GetApplicationConfiguration(1);
                litInformation.Text = appConfig.ConfigValue;

                ApplicationConfiguration appConfig1 = new ApplicationConfiguration();
                if (appConfig1 != null)
                    appConfig1 = ERecruitmentManager.GetApplicationConfiguration(2);
                litInformationSingle.Text = appConfig1.ConfigValue;

                ApplicationConfiguration appConfig2 = new ApplicationConfiguration();
                if (appConfig2 != null)
                    appConfig2 = ERecruitmentManager.GetApplicationConfiguration(3);
                litInformationMulti.Text = appConfig2.ConfigValue;
                #endregion

                ddNationality.DataSource = ERecruitmentManager.GetNationalityList();
                ddNationality.DataBind();
                ddNationality.Items.Insert(0, new ListItem("Select an option", ""));

                ddCity.DataSource = ERecruitmentManager.GetCityList();
                ddCity.DataBind();
                ddCity.Items.Insert(0, new ListItem("Select an option", ""));

                if (CurrentUser != null)
                    hidCurrUserCode.Value = CurrentUser.ApplicantCode;
            }
        }

        #endregion

        #region form
        protected void formJobDetailDatabound(object sender, EventArgs e)
        {
            if (formJobDetail.DataItem != null)
            {
                Vacancies vacancy = formJobDetail.DataItem as Vacancies;
                Literal ltCompany = formJobDetail.FindControl("ltJobDetail_Company") as Literal;
                Literal ltLocation = formJobDetail.FindControl("ltJobDetail_Company") as Literal;
                Literal ltSalary = formJobDetail.FindControl("ltSalary") as Literal;
                Literal ltEffectiveDate = formJobDetail.FindControl("ltEffectiveDate") as Literal;
                Literal ltExpirationDate = formJobDetail.FindControl("ltExpirationDate") as Literal;
                Literal ltIndustrieses = formJobDetail.FindControl("ltIndustrieses") as Literal;
                Literal ltCompanies = formJobDetail.FindControl("ltCompanies") as Literal;
                Literal ltJobDescription = formJobDetail.FindControl("ltJobDescription") as Literal;
                Literal ltJobRequirement = formJobDetail.FindControl("ltJobRequirement") as Literal;
                Literal ltCategory = formJobDetail.FindControl("ltCategory") as Literal;
                if (!string.IsNullOrEmpty(vacancy.JobDescription))
                {
                    string jobDesc = vacancy.JobDescription.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />")
                                                .Replace("\n", "<br />");
                    ltJobDescription.Text = jobDesc;
                }
                if (!string.IsNullOrEmpty(vacancy.JobRequirement))
                {
                    string jobRequirement = vacancy.JobRequirement.Replace(Environment.NewLine, "<br />").Replace("\r", "<br />")
                                                .Replace("\n", "<br />");
                    ltJobRequirement.Text = jobRequirement;
                }
                if (vacancy.IsShowCompany)
                {
                    if (!string.IsNullOrEmpty(vacancy.CompanyCode))
                    {
                        ltCompany.Text = vacancy.CompanyName;
                        ltCompanies.Text = vacancy.CompanyName;
                    }
                }
                else
                {
                    ltCompany.Text = string.Empty;
                    ltCompanies.Text = string.Empty;
                }
                if (vacancy.IsShowSalary)
                {
                    if (vacancy.SalaryRangeBottom > 0 && vacancy.SalaryRangeTop > 0)
                        ltSalary.Text = string.Concat(Utils.DisplayMoneyAmount(vacancy.SalaryRangeBottom), "-", Utils.DisplayMoneyAmount(vacancy.SalaryRangeTop));
                }
                else
                    ltSalary.Text = "Confidential";
                if (vacancy.PostDate.HasValue)
                    ltEffectiveDate.Text = Utils.DisplayDateTime(vacancy.PostDate.Value);
                if (vacancy.ExpiredPostDate.HasValue)
                    ltExpirationDate.Text = Utils.DisplayDateTime(vacancy.ExpiredPostDate.Value);
                CompanyIndustries getCompanyIndustry = ERecruitmentManager.GetCompanyIndustry(vacancy.CompanyCode);
                if (getCompanyIndustry != null)
                    ltIndustrieses.Text = getCompanyIndustry.Industries.Name;

            }
        }

        #endregion

        #region button

        protected void applyButton_ServerClick(object sender, EventArgs e)
        {


            if (Session["currentuser"] != null)
            {
                UserAccounts getUserAccount = Session["currentuser"] as UserAccounts;
                //UserAccounts getUserAccount = AuthenticationManager.GetUserAccount(Utils.GetQueryString<string>("code"));
                string vacancyCode = Utils.GetQueryString<string>("vacancyCode");
                if (getUserAccount != null && !string.IsNullOrEmpty(vacancyCode))
                {
                    Applicants applicant = ApplicantManager.GetApplicant(getUserAccount.Code);

                    if (applicant != null)
                    {
                        bool validateIdCard = false;
                        //check id card number
                        #region check id card

                        if (string.IsNullOrEmpty(applicant.IDCardNumber) && validateIdCard)
                        {
                            JQueryHelper.InvokeJavascript("showNotification('Please update your ID Card number to apply jobs');window.close();", Page);
                            return;
                        }

                        #endregion

                        else
                        {
                            ApplicantVacancies checkVacancy = ERecruitmentManager.GetApplicantVacancies(applicant.Code, vacancyCode);
                            if (checkVacancy == null)
                            {

                                try
                                {
                                    ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
                                    if (getSetting != null)
                                    {
                                        if (!getSetting.ApplicantTrackingSystem)
                                        {
                                            ERecruitmentManager.Save(applicant.Code, vacancyCode);

                                            HtmlButton applyButton = formJobDetail.FindControl("applyButton") as HtmlButton;
                                            applyButton.Disabled = true;
                                            applyButton.InnerText = "Applied";

                                            JQueryHelper.InvokeJavascript("showNotification('Job Applied');", Page);
                                        }
                                        else
                                        {
                                            Vacancies jobReq = ERecruitmentManager.GetVacancy(vacancyCode);
                                            // pop up questions
                                            JQueryHelper.InvokeJavascript("popUpDisqualificationQuestion('" + jobReq.DisqualifierQuestion + "','" + vacancyCode + "','" + applicant.Code + "');", Page);
                                            return;
                                        }
                                    }
                                    else
                                    {
                                        ERecruitmentManager.Save(applicant.Code, vacancyCode);

                                        HtmlButton applyButton = formJobDetail.FindControl("applyButton") as HtmlButton;
                                        applyButton.Disabled = true;
                                        applyButton.InnerText = "Applied";

                                        JQueryHelper.InvokeJavascript("showNotification('Job Applied');", Page);
                                    }

                                }
                                catch
                                {
                                    JQueryHelper.InvokeJavascript("showNotification('Ops.. sorry something wrong');", Page);
                                    Response.Redirect("joblist.aspx");
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                JQueryHelper.InvokeJavascript("showNotification ('please login for apply a job');window.location.href='" + "login.aspx';", Page);

            }
        }
        #endregion

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            if (dd != null)
            {
                dd.Items.Insert(0, new ListItem("All", string.Empty));
            }
        }
        
        //protected void btnSignUp_ServerClick(object sender, EventArgs e)
        //{
        //    #region validate captcha

        //    HtmlInputControl tbCaptcha = formProfile.FindControl("tbCaptcha") as HtmlInputControl;
        //    string keyValidator = Session["captcha"] as string;
        //    if (tbCaptcha.Value != keyValidator)
        //    {
        //        JQueryHelper.InvokeJavascript("showNotification('Security code not match');", Page);
        //        return;
        //    }

        //    #endregion

        //    formProfile.InsertItem(true);
        //}

        //protected void formProfile_ItemInserting(object sender, FormViewInsertEventArgs e)
        //{
        //    HtmlInputControl tbEmailAddress = formProfile.FindControl("tbEmailAddress") as HtmlInputControl;
        //    HtmlInputControl tbPassword = formProfile.FindControl("tbPassword") as HtmlInputControl;

        //    HtmlInputControl tbFirstName = formProfile.FindControl("tbFirstName") as HtmlInputControl;
        //    HtmlInputControl tbLastName = formProfile.FindControl("tbLastName") as HtmlInputControl;

        //    e.Values["Email"] = tbEmailAddress.Value;
        //    e.Values["FirstName"] = tbFirstName.Value;
        //    e.Values["Name"] = tbFirstName.Value;
        //    e.Values["LastName"] = tbLastName.Value;
        //    e.Values["CompanyCode"] = MainCompany.Code;

        //    // user account
        //    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(Utils.GetQueryString<string>("code"));
        //    if (userAccount == null)
        //    {
        //        userAccount = new UserAccounts();

        //    }
        //    userAccount.Name = string.Concat(tbFirstName.Value, " ", tbLastName.Value);
        //    userAccount.UserName = tbEmailAddress.Value;
        //    userAccount.Password = tbPassword.Value;
        //    userAccount.IsApplicant = true;
        //    e.Values["Account"] = userAccount;
        //    e.Values["Active"] = true;
        //    e.Values["RegisteredDate"] = DateTime.Now;
        //}

        #region ods

        protected void odsProfile_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Response.Redirect("../private/update-profile.aspx?code=" + Utils.GetQueryString<string>("code"));
            }
        }

        protected void odsProfile_Inserted(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Applicants applicant = e.ReturnValue as Applicants;
                if (!applicant.Verified)
                {
                    if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
                    {
                        Companies emailSetting = ERecruitmentManager.GetCompany(MainCompany.Code);
                        SmtpClient client = new SmtpClient();
                        client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                        client.Host = emailSetting.EmailSmtpHostAddress;
                        if (emailSetting.EmailSmtpUseSSL)
                            client.EnableSsl = true;
                        client.Timeout = 20000;
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.UseDefaultCredentials = false;
                        client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                        string address = emailSetting.EmailSmtpEmailAddress;
                        string displayName = emailSetting.MailSender;

                        if (!address.Contains("@"))
                        {
                            address = displayName;
                        }
            
                        MailMessage mm = new MailMessage(new MailAddress(address,
                                displayName),
                                                                         new MailAddress(applicant.Email, ""));
                        mm.Subject = "Email verification from " + emailSetting.ApplicationTitle;
                        string body = @"<div style=padding:10px>
                         <div style='width:100%;border:solid 1px #A6C9E2;background-color:#ffffff'>
                             
                                <table>
                                    <tr><td>Dear [candidate],</td></tr>
                                </table> 
                                <table>
                                    <tr><td>Welcome to E-Recruitment Application</td></tr>
                                    <tr>
                                         <td>Your registration is completed, please activate your account within 1x24 hours by clicking the link below:</td>
                                    </tr>
                                    <tr>
                                        <td><a href=[link]>Activate my account</a></td>
                                    </tr>      
                                    <tr>
                                        <td>If you found this email in your Trash/Spam Folder, please click “NOT SPAM/JUNK” in order to ensure that our email not delivered to your Trash/Spam Folder.</td>
                                    </tr>
                                    <tr><td></td></tr>
                                    <tr>
                                       <td>Regards,</td>
                                    </tr>
                                     <tr><td></td></tr>
                                     <tr><td></td></tr>
                                     <tr><td>Recruitment Team</td></tr>
                                </table> 
                        </div>
                     </div>
                            ";
                        mm.Body = body;
                        string link = MainCompany.ATSBaseUrl + "/applicant/emailverificationcompleted.aspx?code=" + applicant.Code;
                        mm.Body = mm.Body.Replace("[link]", link).Replace("[candidate]", applicant.FirstName);
                        mm.BodyEncoding = UTF8Encoding.UTF8;
                        mm.IsBodyHtml = true;
                        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                        client.Send(mm);
                    }
                    JQueryHelper.InvokeJavascript("window.showNotification('Registration completed, please check your email');window.close();window.location.href='" + MainCompany.ATSBaseUrl + "default.aspx';", Page);
                }
                else
                {
                    Session["currentuser"] = ERecruitment.Domain.AuthenticationManager.GetApplicantUserAccount(applicant.Code);

                    Response.Redirect("../applicant/update-profile.aspx?code=" + applicant.Code);
                }
            }
            else
            {
                e.ExceptionHandled = true;
                JQueryHelper.InvokeJavascript("showNotification('" + Utils.ExtractInnerExceptionMessage(e.Exception) + "');", Page);
            }
        }

        #endregion
    }
}