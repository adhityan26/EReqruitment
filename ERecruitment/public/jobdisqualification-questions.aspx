﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/popupform.Master" CodeBehind="jobdisqualification-questions.aspx.cs" Inherits="ERecruitment.jobdisqualification_questions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        $(document).ready(function () {

            toggleInformationSourceNotes();            
        });

        function toggleInformationSourceNotes()
        {
            var ddInformationSource = '<%= ddInformationSource.ClientID %>';
            var tbInformationSourceNotes = '<%= tbInformationSourceNotes.ClientID %>';
    
            var toggleInformationSourceNoteHandlerUrl = "../handlers/HandlerSettings.ashx?commandName=CheckToggleVacancyInformationSourceAdditionalInfo&typeCode=" + $("#" + ddInformationSource).val();
    
            $.ajax({
                url: toggleInformationSourceNoteHandlerUrl,
                async: false,
                beforeSend: function () {
                    $("#" + ddInformationSource).after('<span class="loadingIndicator"></span>');
                    $("#" + tbInformationSourceNotes).val("");
                },
                success: function (queryResult) {
                    
                    if (queryResult == "True")
                        $("#" + tbInformationSourceNotes).show();
                    else
                        $("#" + tbInformationSourceNotes).hide();
                }
            });
        }

    </script>
        
        <div class="row">
            <div class="col-sm-12">
                <h1>Some questions before you move on..</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                  <div class="row">
                    <div class="col-sm-3">
                        <label runat="server" id="lbJobDisq_Question_Source">Where did you get this information ?</label>
                    </div>
                    <div class="col-sm-2">
                         <select id="ddInformationSource" onchange="toggleInformationSourceNotes();" runat="server" class="form-control">
                             <option>LinkedIn</option>
                             <option>Facebook</option>
                             <option>Twitter</option>
                             <option>Newsletter</option>
                             <option>Website</option>
                             <option>Others</option>
                         </select>
                    </div>                        
                    <div class="col-sm-2">
                        <asp:TextBox ID="tbInformationSourceNotes" runat="server" CssClass="form-control" />
                    </div>
                </div>
                <asp:Repeater ID="repQuestion"
                              runat="server"
                              OnItemDataBound="repQuestion_ItemDataBound"
                              >
                    <ItemTemplate>
                        <div class="row">
                            <div class="col-sm-3">
                                <label><%# Eval("Question") %></label>
                                <asp:HiddenField ID="hidCode" runat="server" Value='<%# Eval("Code") %>' />
                            </div>
                            <div class="col-sm-2">
                                
                                <asp:TextBox ID="tbNumericAnswer" runat="server" CssClass="form-control" />                     
                                <asp:DropDownList ID="ddMultipleChoiceAnswer"
                                                  runat="server"
                                                  CssClass="form-control"
                                                  DataValueField="Label"
                                                  DataTextField="Label"
                                                  />
                            </div>
                        </div>
                    </ItemTemplate>
                    <FooterTemplate>
                        <div class="row">
                            <div class="col-sm-3">

                            </div>
                            <div class="col-sm-1">
                                <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Submit" OnClick="btnSave_Click" />
                            </div>
                        </div>
                    </FooterTemplate>
                </asp:Repeater>

            </div>
        </div>
        
</asp:Content>
