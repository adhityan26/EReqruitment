﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using System.IO;
using FastReport.Web;
using System.Configuration;
using FastReport;
namespace ERecruitment
{
    public partial class contentfastreport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("report")))
                    loadReport(Utils.GetQueryString<string>("report"));
            }
        }

        protected void scriptManager_AsyncPostBackError(object sender, AsyncPostBackErrorEventArgs e)
        {
            Exception error = e.Exception;

            //scriptManager.AsyncPostBackErrorMessage = "Oops.. sorry, the system has encounter an exception. The administrator has been notified and will handle the exception imidiately";

            if (error != null)// && error.GetType() != typeof(HttpException))
            {
               // IErrorLogger logger = new MailErrorLogExecutor();
                //logger.LogError(error, SSWebMemberships.CurrentUser);

            }
        }

        #region load report

        private void loadReport(string reportFileName)
        {

            if (!Utils.GetQueryString<bool>("preview"))
            {
                string fileName = Utils.GetQueryString<string>("fileName");
                if (string.IsNullOrEmpty(fileName))
                    fileName = "report.pdf";
                Report report = new Report();
                report.Load(ConfigurationManager.AppSettings["ReportPath"] + reportFileName);

                #region setting parameter
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("param")))
                {
                    string[] paramList = Utils.GetQueryString<string>("param").Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < paramList.Length; i++)
                    {
                        string[] parameterPair = paramList[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        string type = parameterPair[2];
                        switch (type)
                        {
                            case "int":
                                report.SetParameterValue(parameterPair[0], Utils.ConvertString<int>(parameterPair[1]));
                                break;
                            case "decimal":
                                report.SetParameterValue(parameterPair[0], Utils.ConvertString<decimal>(parameterPair[1]));
                                break;
                            case "bool":
                                report.SetParameterValue(parameterPair[0], Utils.ConvertString<bool>(parameterPair[1]));
                                break;
                            case "date":

                                string[] dateComponent = Utils.FormatDateNumeric(Utils.ConvertToDateTime(parameterPair[1])).Split('/');
                                DateTime date = new DateTime(Utils.ConvertString<int>(dateComponent[2]), Utils.ConvertString<int>(dateComponent[1]), Utils.ConvertString<int>(dateComponent[0]));
                                report.SetParameterValue(parameterPair[0], date);
                                break;
                            default:
                                report.SetParameterValue(parameterPair[0], parameterPair[1]);
                                break;
                        }

                    }
                }
                #endregion

                report.Dictionary.Connections[0].ConnectionString = ConfigurationManager.ConnectionStrings["CRMConnectionString"].ConnectionString;
                report.Prepare();

                string exportType = Utils.GetQueryString<string>("type");
                if (string.IsNullOrEmpty(exportType))
                    exportType = "pdf"; // default value
                if (exportType == "pdf")
                {

                    // Export report to PDF stream
                    FastReport.Export.Pdf.PDFExport pdfExport = new FastReport.Export.Pdf.PDFExport();

                    using (MemoryStream strm = new MemoryStream())
                    {
                        report.Export(pdfExport, strm);

                        // Stream the PDF back to the client as an attachment
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Buffer = true;
                        Response.ContentType = "Application/PDF";
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

                        strm.Position = 0;
                        strm.WriteTo(Response.OutputStream);
                        Response.End();

                        JQueryHelper.InvokeJavascript("window.close();", Page);
                    }
                }
                else if (exportType == "xlsx" || exportType == "xls")
                {
                    // Export report to PDF stream
                    FastReport.Export.OoXML.Excel2007Export xlsExport = new FastReport.Export.OoXML.Excel2007Export();

                    using (MemoryStream strm = new MemoryStream())
                    {
                        report.Export(xlsExport, strm);

                        // Stream the PDF back to the client as an attachment
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Buffer = true;
                        Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

                        strm.Position = 0;
                        strm.WriteTo(Response.OutputStream);
                        Response.End();

                        JQueryHelper.InvokeJavascript("window.close();", Page);
                    }
                }
                else if (exportType == "csv")
                {
                    FastReport.Export.Csv.CSVExport csvReport = new FastReport.Export.Csv.CSVExport();
                    csvReport.Separator = ";";
                    using (MemoryStream strm = new MemoryStream())
                    {
                        report.Export(csvReport, strm);

                        // Stream the PDF back to the client as an attachment
                        Response.ClearContent();
                        Response.ClearHeaders();
                        Response.Buffer = true;
                        Response.ContentType = "application/csv";
                        Response.AddHeader("Content-Disposition", "attachment;filename=" + fileName);

                        strm.Position = 0;
                        strm.WriteTo(Response.OutputStream);
                        Response.End();

                        JQueryHelper.InvokeJavascript("window.close();", Page);
                    }
                }

            }
            else
            {
                WebReport1.Report.Load(ConfigurationManager.AppSettings["ReportPath"] + reportFileName);

                #region setting parameter
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("param")))
                {
                    string[] paramList = Utils.GetQueryString<string>("param").Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < paramList.Length; i++)
                    {
                        string[] parameterPair = paramList[i].Split(new char[] { ':' }, StringSplitOptions.RemoveEmptyEntries);
                        WebReport1.Report.SetParameterValue(parameterPair[0], parameterPair[1]);

                    }
                }
                #endregion

                WebReport1.Report.Dictionary.Connections[0].ConnectionString = ConfigurationManager.ConnectionStrings["HRISConnectionString"].ConnectionString;

                WebReport1.Prepare();
            }
        }

        #endregion
    }
}