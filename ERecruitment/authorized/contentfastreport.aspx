﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contentfastreport.aspx.cs" Inherits="ERecruitment.contentfastreport" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Register Assembly="FastReport.Web" Namespace="FastReport.Web" TagPrefix="cc1" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>ERecruitment</title>
</head>
<body>
    <form id="form1" runat="server">
    
    <div id="masterContainer">
    
      
    <table id="masterLayout" summary="layout" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div class="dvContent ui-widget-content ui-corner-all">
                <div id="dvTitle">
                <%--page title--%>
                <h4 class="h4Title"><asp:Literal ID="litTitle" runat="server" /></h4>
                </div>
               
                <%-- dynamic content placeholder --%>
               <cc1:WebReport ID="WebReport1" runat="server" width="800px" />
                </div>
            </td>
        </tr>
    </table>    
    
           
    </div>
    </form>
</body>
</html>

