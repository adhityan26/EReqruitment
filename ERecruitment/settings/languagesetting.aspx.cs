﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class languagesetting: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (IsPostBack)
            {

            }
        }

        protected void ddPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            repControlList.DataBind();
            fillInText();
        }

        protected void ddLanguage_SelectedIndexChanged(object sender, EventArgs e)
        {
            fillInText();

        }

        private void fillInText()
        {
            List<ApplicationPageControlTexts> pageControlList = ApplicationManager.GetPageControlTextList(Utils.GetSelectedValue<string>(ddPage), Utils.GetSelectedValue<string>(ddLanguage));
            foreach (RepeaterItem item in repControlList.Items)
            {
                HiddenField hidControlId = item.FindControl("hidControlId") as HiddenField;
                ApplicationPageControlTexts controlText = pageControlList.Find(delegate (ApplicationPageControlTexts temp) { return temp.ControlId == hidControlId.Value; });
                if (controlText != null)
                {
                    TextBox tbText = item.FindControl("tbText") as TextBox;
                    tbText.Text = controlText.Text;
                }

            }
        }

        private List<ApplicationPageControlTexts> getTextList()
        {
            List<ApplicationPageControlTexts> pageControlList = new List<ApplicationPageControlTexts>();
            foreach (RepeaterItem item in repControlList.Items)
            {
                HiddenField hidControlId = item.FindControl("hidControlId") as HiddenField;
                TextBox tbText = item.FindControl("tbText") as TextBox;
                ApplicationPageControlTexts controlText = new ApplicationPageControlTexts();
                controlText.ControlId = hidControlId.Value;
                controlText.LanguageCode = Utils.GetSelectedValue<string>(ddLanguage);
                controlText.PageName = Utils.GetSelectedValue<string>(ddPage);
                controlText.Text = tbText.Text;
                pageControlList.Add(controlText);
            }

            return pageControlList;
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            List<ApplicationPageControlTexts> pageControlList = ApplicationManager.GetPageControlTextList(Utils.GetSelectedValue<string>(ddPage), Utils.GetSelectedValue<string>(ddLanguage));
            ApplicationManager.RemovePageControlTextList(pageControlList);
            ApplicationManager.SavePageControlTextList(getTextList());
            JQueryHelper.InvokeJavascript("showNotification('Successfully saved')", Page);
        }
    }
}