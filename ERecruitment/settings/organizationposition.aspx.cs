﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class organizationposition: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.Form["__EVENTARGUMENT"] == "View")
            {
                // Fire event
                IniateInformation();
            }
        }

        private void IniateInformation()
        {
            string code = hidCode.Value;
            OrganizationPosition data = ERecruitmentManager.GetOrganizationPosition(code);
            tbName.Value = data.Position;
        }
        private void ClearControl()
        {
            tbName.Value = string.Empty;
            hidCode.Value = string.Empty;
            JQueryHelper.InvokeJavascript("showNotification('Data successfully saved');loadData();", Page);
        }
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hidCode.Value))
            {
                //save
                OrganizationPosition addType = new OrganizationPosition();
                addType.Position = tbName.Value;

                addType.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                addType.Active = true;
                ERecruitmentManager.SaveOrganizationPosition(addType);
                ClearControl();
            }
            else
            {
                OrganizationPosition getType = ERecruitmentManager.GetOrganizationPosition(hidCode.Value);
                getType.Position = tbName.Value;

                ERecruitmentManager.UpdateOrganizationPosition(getType);
                ClearControl();
            }

        }

        protected void btnDetail_Click(object sender, EventArgs e)
        {

        }
    }
}