﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class educationmappingheader : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ddEduLevel.DataSource = ERecruitmentManager.GetEducationLevels();
            ddEduLevel.DataBind();
            ddEduLevel.Items.Insert(0, new ListItem("Select an option", ""));

            ddFieldOfStudy.DataSource = ERecruitmentManager.GetFieldOfStudyList();
            ddFieldOfStudy.DataBind();
            ddFieldOfStudy.Items.Insert(0, new ListItem("Select an option", ""));

            ddInstitution.DataSource = ERecruitmentManager.GetUniversityList();
            ddInstitution.DataBind();
            ddInstitution.Items.Insert(0, new ListItem("Select an option", ""));

            listMajor.DataSource = ERecruitmentManager.GetEducationMajorList();
            listMajor.DataBind();
   
        }
    }
}