﻿<%@ Page Title="Language Settings" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="languagesetting.aspx.cs" Inherits="ERecruitment.languagesetting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
    <ul class="nav tab-menu nav-tabs" id="myTab" role="tablist"  >
        <li role="presentation" class="active"><a role="tab" data-toggle="tab" aria-controls="pageInfo" aria-expanded="true"  href="#pageInfo">Pages</a></li>
        <li role="presentation"><a role="tab" data-toggle="tab" aria-controls="menuInfo" aria-expanded="true"  href="#menuInfo">Menus</a></li>
    </ul>
    <div id="myTabContent" class="tab-content">  
        <div class="tab-pane active" id="pageInfo" role="tabpanel">   
            <div class="row searchPanel">
                <div class="col-md-12 col-sm-12">
                    <div class="col-sm-1 "><label>Language</label></div>
                    <div class="col-sm-2">

                        <asp:DropDownList ID="ddLanguage"
                                          runat="server"
                                          DataSourceID="odsLanguageList"
                                          CssClass="form-control"
                                          DataValueField="Code"
                                          DataTextField="Name"
                                          AutoPostBack="true"
                                          OnSelectedIndexChanged="ddLanguage_SelectedIndexChanged"
                                          />

                    </div>
                    <div class="col-sm-1"><label>Page</label></div>
                    <div class="col-sm-2">
                        <asp:DropDownList ID="ddPage"
                                          runat="server"
                                          DataSourceID="odsPageList"
                                          CssClass="form-control"
                                          AutoPostBack="true"
                                          OnSelectedIndexChanged="ddPage_SelectedIndexChanged"
                                          />
                    </div>
                </div>
            </div>
            <asp:Repeater ID="repControlList"
                          runat="server"
                          DataSourceID="odsControlList"
                          >
                <HeaderTemplate>
                    <div class="panel-heading row">
                        <div class="col-sm-2">
                            <label>Control Name</label>
                        </div>
                        <div class="col-sm-5">
                            <label>Text</label>
                        </div>
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="row">
                        <div class="col-sm-2">
                            <label><%# Eval("ControlID") %></label>
                            <asp:HiddenField ID="hidControlId" Value='<%# Eval("ControlID") %>' runat="server" />
                        </div>
                        <div class="col-sm-5">
                            <asp:TextBox ID="tbText" runat="server" CssClass="form-control" />
                        </div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    <div class="row">
                        <div class="col-sm-2">
                
                        </div>
                        <div class="col-sm-5">
                            <button class="btn btn-primary"  runat="server" id="btnSave" 
                                onserverclick="btnSave_ServerClick" >
                                Save
                            </button>    
                        </div>
                    </div>
                </FooterTemplate>
            </asp:Repeater>               
        </div>  
        <div class="tab-pane active" id="menuInfo" role="tabpanel">                  
        </div>
    </div>

<asp:ObjectDataSource ID="odsControlList"
                      runat="server"
                      TypeName="ERecruitment.Domain.ApplicationManager"
                      DataObjectTypeName="ERecruitment.Domain.ApplicationPageControls"
                      SelectMethod="GetPageControlList"
                      >
    <SelectParameters>
        <asp:ControlParameter Name="pageName" ControlID="ddPage" PropertyName="SelectedValue" />
    </SelectParameters>
</asp:ObjectDataSource>  
<asp:ObjectDataSource ID="odsPageList"
                      runat="server"
                      TypeName="ERecruitment.Domain.ApplicationManager"
                      SelectMethod="GetPageList"
                      />
<asp:ObjectDataSource ID="odsLanguageList"
                      runat="server"
                      TypeName="ERecruitment.Domain.ApplicationManager"
                      DataObjectTypeName="ERecruitment.Domain.ApplicationLanguages"
                      SelectMethod="GetApplicationLanguageList"
                      />
</asp:Content>
