﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class organizationtype: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.Form["__EVENTARGUMENT"] == "View")
            {
                // Fire event
                IniateInformation();
            }
        }

        private void IniateInformation()
        {
            string code = hidCode.Value;
            OrganizationType data = ERecruitmentManager.GetOrganizationType(code);
            tbName.Value = data.Type;
        }
        private void ClearControl()
        {
            tbName.Value = string.Empty;
            hidCode.Value = string.Empty;
            JQueryHelper.InvokeJavascript("showNotification('Data successfully saved');loadData();", Page);
        }
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hidCode.Value))
            {
                //save
                OrganizationType addType = new OrganizationType();
                addType.Type = tbName.Value;

                addType.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                addType.Active = true;
                ERecruitmentManager.SaveOrganizationType(addType);
                ClearControl();
            }
            else
            {
                OrganizationType getType = ERecruitmentManager.GetOrganizationType(hidCode.Value);
                getType.Type = tbName.Value;

                ERecruitmentManager.UpdateOrganizationType(getType);
                JQueryHelper.InvokeJavascript("showNotification('Data successfully saved')", Page);
                ClearControl();
            }

        }

        protected void btnDetail_Click(object sender, EventArgs e)
        {

        }
    }
}