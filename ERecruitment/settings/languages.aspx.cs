﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class languages: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.Form["__EVENTARGUMENT"] == "View")
            {
                // Fire event
                IniateInformation();
            }
        }

        private void IniateInformation()
        {
            string code = hidCode.Value;
            Languages data = ERecruitmentManager.GetLanguage(code);
            tbName.Value = data.Language;
        }
        private void ClearControl()
        {
            tbName.Value = string.Empty;
            hidCode.Value = string.Empty;
            JQueryHelper.InvokeJavascript("showNotification('Data successfully saved');loadData();", Page);
        }
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hidCode.Value))
            {
                //save
                Languages addLanguages = new Languages();
                addLanguages.Language = tbName.Value;

                addLanguages.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                addLanguages.Active = true;
                ERecruitmentManager.SaveLanguage(addLanguages);
                ClearControl();
            }
            else
            {
                Languages getLanguage = ERecruitmentManager.GetLanguage(hidCode.Value);
                getLanguage.Language = tbName.Value;

                ERecruitmentManager.UpdateLanguage (getLanguage);
                ClearControl();
            }

        }

        protected void btnDetail_Click(object sender, EventArgs e)
        {

        }
    }
}