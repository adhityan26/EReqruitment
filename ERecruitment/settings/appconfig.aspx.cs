﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class appconfig : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }


        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            try
            {
                ApplicationConfiguration appConfig = new ApplicationConfiguration();
                appConfig.Code = Utils.ConvertString<int>(hidConfigCode.Value);
                appConfig.ConfigName = hidConfigName.Value;
                appConfig.ConfigValue = hidConfigValue.Value;
                    ERecruitmentManager.UpdateApplicationConfiguration(appConfig);
                    JQueryHelper.InvokeJavascript("showNotification('Data Saved');loadData();", Page);
              
            }
            catch (Exception ex)
            {
                JQueryHelper.InvokeJavascript(
                    "showNotification('" + ex.Message + "', 'error');",
                    Page);
            }
        }

    }
}