﻿<%@ Page Title="Job Codes" Language="C#" MasterPageFile="~/masters/jobs.Master" AutoEventWireup="true" CodeBehind="jobcodes.aspx.cs" Inherits="ERecruitment.jobcodes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">


    $(document).ready(function () {

        $("#liJobCodes").addClass("active");
    });

    $(document).ready(function () {

        loadData();
    });

    $('body').on('click', '.delete', function () {

        var code = $(this).attr('name');
        confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
            $.ajax({
                url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveJobCode&id=" + code,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                }
            });
        });
    });



    // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
    $('body').on('click', '.edit', function () {

        var code = $(this).attr('name');
        showDataForm(code);

    });


    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [2] }

            ],
            "oLanguage":
                               { "sSearch": "Search By Name" },

            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetJobCodeList"

        });
    }


    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function showForm(code) {

        showDataForm(code);
    }

    function Add() {

        showDataForm("0");
    }

    function showDataForm(dataId) {

        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetJobCode&id=" + dataId;

        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("formDataFrameModel", dataModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#formDataFrameModel').modal('show');
    }

    function updateDataForm() {

        var form = "formDataFrameModel";
        var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateJobCode";
        var formData = {};

        if (!getNodeData(form, formData)) return false;

        $.post(actionUrl, formData).done(function (data) {

            refresh();
            $('#formDataFrameModel').modal('hide');

        });
    }

    </script>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
        </div>
    </div>    
           
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th><span data-lang="JobCodesSetupTitle">Job Code</span></th>
                <th><span data-lang="JobRoleSetupTitle">Job Role</span></th>
                <th><span data-lang="JobStageSetupTitle">Job Stage</span></th>
                <th><span data-lang="JobDescSetupTitle">Job Description</span></th>
                <th><span data-lang="JobReqSetupTitle">Job Requirements</span></th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
       

    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
            <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h1 class="modal-title center" data-lang="JobCodeCaptionTitle">Job Code</h1>
		    </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="JobCodeSetupInputTitle">Job Code</span><span class="requiredmark">*</span>     
                        <input type="hidden" data-attribute="Code" />
                    </div>
                    <div class="col-md-3 col-sm-3"> 
                        <input class="form-control" type="text" required="" data-attribute="JobCode"/>  
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="RoleSetupInputTitle">Job Role</span><span class="requiredmark">*</span>     
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" type="text" required="" data-attribute="JobRole"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="StageSetupInputTitle">Job Stage</span><span class="requiredmark">*</span>     
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <input class="form-control" type="text" required="" data-attribute="JobStage"/>  
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="JobDescSetupInputTitle">Job Description</span><span class="requiredmark">*</span>    
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <textarea class="form-control" required="" data-attribute="JobDescription"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="JobReqSetupInputTitle">Job Requirements</span><span class="requiredmark">*</span>    
                    </div>
                    <div class="col-md-9 col-sm-9"> 
                        <textarea class="form-control" required="" data-attribute="Requirements"></textarea> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
            </div>
            </div>
        </div>
    </div>

</asp:Content>
