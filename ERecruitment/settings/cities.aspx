﻿<%@ Page Title="Locations" Language="C#" MasterPageFile="~/masters/locations.Master" AutoEventWireup="true" CodeBehind="cities.aspx.cs" Inherits="ERecruitment.cities" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">


    $(document).ready(function () {

        $("#liCities").addClass("active");

    });

    $(document).ready(function () {

        loadData();
    });

    $('body').on('click', '.delete', function () {

        var code = $(this).attr('name');
        confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
            $.ajax({
                url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveCity&id=" + code,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                }
            });
        });
    });



    // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
    $('body').on('click', '.edit', function () {

        var code = $(this).attr('name');
        showDataForm(code);

    });


    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [2] }

            ],
            "oLanguage":
                               { "sSearch": "Search By Name" },

            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetCityList"

        });
    }


    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function showForm(code) {
        var options = '<option value="">Select an option</option>';
        $('#ddProvince').html(options);
        showDataForm(code);
    }

    function Add() {
        showDataForm("0");
    }

    function showDataForm(dataId) {

        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetCity&id=" + dataId;

        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("formDataFrameModel", dataModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#formDataFrameModel').modal('show');
    }

    function updateDataForm() {

        var form = "formDataFrameModel";
        var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateCity";
        var formData = {};

        if ($("#cityName").val=="" || $("#<%= ddCountry.ClientID %>").val() =="" || $("#ddProvince").val() == "")
        {
            showNotification("Please fill required(*) field.", "warning");
        }
        else {
            if (!getNodeData(form, formData)) return false;

            $.post(actionUrl, formData).done(function (data) {

                refresh();
                $('#formDataFrameModel').modal('hide');

            });
        }

    }

    function toggleProvince()
    {
        var selectedCountry = $("#<%= ddCountry.ClientID %>").val();
        loadProvince(selectedCountry);
    }

    function loadProvince(selectedCountry) {
        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetProvinceList&countryCode=" + selectedCountry ;
        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {
            },
            success: function (queryResult) {
                var dataModel = $.parseJSON(queryResult);
                //EducationLevels = $.parseJSON(queryResult);
                var options = "";
                if (dataModel.length == 0) {
                    options += '<option value="">Select an option</option>';
                } else {
                    for (var i = 0; i < dataModel.length; i++) {
                        if (i == 0) {
                            options += '<option value="">Select an option</option>';
                        }
                        options += '<option value="' + dataModel[i].ProvinceCode + '">' + dataModel[i].ProvinceName + '</option>';
                    }
                }
                $('#ddProvince').html(options);
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    </script>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
        </div>
    </div>    
           
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th><span data-lang="NameLocationTitle">City</span></th>
                <th><span data-lang="ProvinceLocationTitle">Province</span></th>
                <th><span data-lang="CountryLocationTitle">Country</span></th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
       

    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
                <div class="modal-header">
			        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			        <h1 class="modal-title center" data-lang="CityCaptionTitle">City</h1>
		        </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="NameCityTitle">City Name</span><span class="requiredmark">*</span>       
                            <input type="hidden" data-attribute="CityCode" />
                        </div>
                        <div class="col-md-9 col-sm-9"> 
                            <input class="form-control" type="text" required="" data-attribute="CityName" id="cityName"/>  
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="NameCountryTitle">Country</span><span class="requiredmark">*</span>   
                        </div>
                        <div class="col-md-4 col-sm-4"> 
                            <select class="form-control" id="ddCountry" runat="server" datavaluefield="Code" datatextfield="Name" required="" data-attribute="CountryCode" onchange="toggleProvince();"></select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="NameProvinceTitle">Province</span><span class="requiredmark">*</span>   
                        </div>
                        <div class="col-md-4 col-sm-4"> 
                            <select  id="ddProvince"  required="" data-attribute="ProvinceCode"></select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
