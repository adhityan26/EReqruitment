﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class positions : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                #region check company role list

                UserAccounts getUserAccount = CurrentUser;

                if (CurrentUser.IsAdmin)
                {
                    ddCompany.DataSource = ERecruitmentManager.GetCompanyList();
                    ddCompany.DataBind();
                }
                else
                {
                    if (!string.IsNullOrEmpty(MainRole.CompanyCodes))
                    {
                        string[] userCompanyCodes = MainRole.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        List<Companies> getCompanyAccessList = ERecruitmentManager.GetCompanyListByCodes(userCompanyCodes);
                        List<Companies> companyList = new List<Companies>();
                        foreach (Companies item in getCompanyAccessList)
                        {
                            if (companyList.Contains(item))
                                continue;
                            else
                                companyList.Add(item);
                        }

                        ddCompany.DataSource = companyList;
                        ddCompany.DataBind();
                    }
                }

                #endregion
            }
        }
    }
}