﻿<%@ Page Title="Education Mapping " Language="C#" MasterPageFile="~/masters/educations.Master" AutoEventWireup="true" CodeBehind="educationmappingheader.aspx.cs" Inherits="ERecruitment.educationmappingheader" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">


    $(document).ready(function () {

        $("#liEducationMapping").addClass("active");
    });

    $(document).ready(function () {

        loadData();
    });

    $('body').on('click', '.delete', function () {

        var code = $(this).attr('name');
        confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
            $.ajax({
                url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveEducationMapping&id=" + code,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                }
            });
        });
    });



    // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
    $('body').on('click', '.edit', function () {

        var code = $(this).attr('name');
        showDataForm(code);

    });


    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [2] }

            ],
            "oLanguage":
                               { "sSearch": "Search By Institution" },

            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetEducationMappingHeader"

        });
    }


    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function showForm(code) {

        showDataForm(code);
    }

    function Add() {

        showDataForm("0");
    }

    function showDataForm(dataId) {
        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetEducationMappingHeader&id=" + dataId;

        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {
               
                var dataModel = $.parseJSON(queryResult);
               
     
                if (dataModel["MajorConcateCode"] != null)
                {
                    dataModel["MajorConcateCode"] = dataModel["MajorConcateCode"].split(',');
                }
                
                loadDataIntoForm("formDataFrameModel", dataModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#formDataFrameModel').modal('show');
    }

    var formData = {};
    function updateDataForm() {

        var form = "formDataFrameModel";
        var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateEducationMappingHeader";

        
        var MajorElemen = $.map($("#<%= listMajor.ClientID %> option:selected"), function (el, i) {
            return $(el).text();
        });
        var MajorConcate = MajorElemen.join(", ");
      
        if (!getNodeData(form, formData)) return false;
        
        formData["MajorConcate"] = MajorConcate;
        formData["MajorConcateCode"] = String($("#<%= listMajor.ClientID %>").val());
        
        if (formData["UniversitiesCode"]!="" && formData["EducationLevel"]!="" && formData["FieldOfStudyCode"]!="") {
            $.post(actionUrl, formData).done(function (data) {
                refresh();
                $('#formDataFrameModel').modal('hide');
            }).fail(function (xhr, status, error) {
                showNotification(xhr.responseText, 'error');
            });
        }
        else
        {
            showNotification("Please Fill Required Field (*)", "error");
        }
    }


    </script>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="Add(); return false;"><i class="fa fa-file"></i>New</button>
        </div>
    </div>    
           
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th></th>
                 <th><span data-lang="EduMappingUniversitiesTitle">Education Mapping Header Code </span></th>
                 <th>Institution</th>
                 <th>Education Level</th>
                 <th>Field Of Study</th>
                 <th>Major</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
       

    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
            <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h1 class="modal-title center" data-lang="EduMappingHeaderCaptionTitle">Education Mapping</h1>
		    </div>
            <div class="modal-body">
                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                        Institution <span class="requiredmark">*</span>
                        <input type="hidden" data-attribute="EducationMappingHeaderCode" />
                        </div>
                          <div class="col-md-9 col-sm-9"> 
                              <select class="form-control" id="ddInstitution" runat="server" datavaluefield="Code" datatextfield="Name" data-attribute="UniversitiesCode"/>      
                    </div>
                </div>

                  <div class="row">
                    <div class="col-md-3 col-sm-3">
                        Education Level <span class="requiredmark">*</span> 
                        </div>
                          <div class="col-md-9 col-sm-9"> 
                              <select class="form-control" id="ddEduLevel" runat="server" DataValueField="EducationLevel" DataTextField="Name" data-attribute="EducationLevel"/>      
                    </div>
                </div>

                 <div class="row">
                    <div class="col-md-3 col-sm-3">
                        Field Of Study<span class="requiredmark">*</span> 
                        </div>
                          <div class="col-md-9 col-sm-9"> 
                              <select class="form-control" id="ddFieldOfStudy" runat="server" datavaluefield="Code" datatextfield="Name" data-attribute="FieldOfStudyCode"/>      
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-3 col-sm-3">
                        <span data-lang="MappingEduMajorSetupInputTitle">Major</span>
                        </div>
                          <div class="col-md-9 col-sm-9">    
                               <asp:ListBox ID="listMajor" SelectionMode="Multiple" runat="server" CssClass="form-control" DataValueField="Code" DataTextField="Major" data-attribute="MajorConcateCode"   /> 
                                
                          </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
            </div>
            </div>
        </div>
    </div>
</asp:Content>
