﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class provincelist : System.Web.UI.Page
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);


            ddCountry.DataSource = ERecruitmentManager.GetCountryList();
            ddCountry.DataBind();
            ddCountry.Items.Insert(0, new ListItem("Select an option", ""));
        }
    }
}