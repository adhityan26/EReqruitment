﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="organizationtype.aspx.cs" Inherits="ERecruitment.organizationtype" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
     <style type="text/css">
        .jobs .panel-body > div > div{
            padding: 5px !important;
        }
    </style>
          
<script type="text/javascript"> 


    $(document).ready(function () {

        loadData();
    });

    $('body').on('click', '.delete', function () {

        var code = $(this).attr('name');
        confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
            $.ajax({
                url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveOrganizationType&code=" + code,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                }
            });
        });
    });



    // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
    $('body').on('click', '.edit', function () {
        
        var code = $(this).attr('name');
          <%= hidCode.ClientID %>.value =code;
        __doPostBack('btnDetail', 'View');
      
    });

   
    function loadData()
    {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }
      
        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 1, 2] },
                   { "sClass": "controlIcon", "aTargets": [0, 1] },
                   { "bVisible": false, "aTargets": [2] }
                              
            ],
            "oLanguage": 
                               { "sSearch": "Search By Name" },
                        
            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetOrganizationTypeList"

        });
    }

    
    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

</script>
    <asp:Button ID="btnDetail" OnClick="btnDetail_Click" runat="server" Visible="false" />
    <asp:HiddenField ID="hidCode" runat="server" />
<div>
      <h3><asp:Literal ID="ltTitleOrgType" Text="Organization Type" runat="server"></asp:Literal></h3>
<div>
    
     <div class="row">
          <div class="col-sm-12">
          
        <div class="row">
            <div class="col-sm-3">
                <label runat="server" id="lbOrgType_Name">Name</label><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-6">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbName"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                                        
            </div> 
        </div>

            
       <div class="row">
            <div class="col-sm-3">
                
            </div>
            <div class="col-sm-3">
                <button class="btn btn-primary" runat="server" id="btnSubmit" onserverclick="btnSubmit_ServerClick">Save</button>                
            </div>
        </div>
       
   </div>
 </div>      
  
        <div>
   <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
        <tr>
             <th></th>
            <th></th>
            <th></th>
            <th>Name</th>
           
        </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
    </div>
</div>
</div>

    





</asp:Content>
