﻿
<%@ Page Title="Application Config" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="appconfig.aspx.cs" Inherits="ERecruitment.appconfig" ValidateRequest="false" %>
<%--<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
       <script src="/assets/plugins/ckeditor/ckeditor.js"></script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="ScriptMgr" runat="server" EnablePageMethods="true"></asp:ScriptManager>
     <script type="text/javascript">
         $(document).ready(function () {
             CKEDITOR.config.toolbar_Custom =
            [
                { name: 'document', items: ['Source'] },
                { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
                { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
                '/',
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
                {
                    name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                        '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
                },
                '/',
                { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
                { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
                '/',
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
                { name: 'colors', items: ['TextColor', 'BGColor'] },
                { name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'About'] }
            ];

             CKEDITOR.config.toolbar = "Custom";
             CKEDITOR.replace('configValue');
             loadData();
         });


         function loadData() {
             var ex = document.getElementById('tableData');
             if ($.fn.DataTable.fnIsDataTable(ex)) {
                 // data table, then destroy first
                 $("#tableData").dataTable().fnDestroy();
             }

             var OTableData = $('#tableData').dataTable({
                 "bProcessing": true,
                 "bServerSide": true,
                 "iDisplayLength": 10,
                 "bJQueryUI": true,
                 "bAutoWidth": false,
                 "sDom": "ftipr",
                 "bDeferRender": true,
                 "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [1] },
                   { "sClass": "controlIcon", "aTargets": [0] },
                   { "bVisible": false, "aTargets": [1] }

       

                 ],
                 "oLanguage":
                                 { "sSearch": "Search By Config Name" },
                 "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetAppConfigList"
             });
         }

         function refresh() {
             var tableData = $('#tableData').dataTable();
             tableData.fnDraw();
         }

         function showDataForm(dataId) {
             var handlerUrl = "../handlers/HandlerSettings.ashx?commandName=GetAppConfig&id=" + dataId;

             $.ajax({
                 url: handlerUrl,
                 async: true,
                 beforeSend: function () {

                 },
                 success: function (queryResult) {

                     var dataModel = $.parseJSON(queryResult);
                     $("#<%= hidConfigCode.ClientID %>").val(dataId);
                     $("#configName").val(dataModel["ConfigName"]);
                     CKEDITOR.instances["configValue"].setData(dataModel["ConfigValue"]);
                 },
                 error: function (xhr, ajaxOptions, thrownError) {

                     console.log("Error");
                 }
             });

             $('#formDataFrameModel').modal('show');
         }

         function moveData()
         {
             var configName = $("#configName").val();
             var configValue = CKEDITOR.instances["configValue"].getData();
             $("#<%= hidConfigName.ClientID %>").val(configName);
             $("#<%= hidConfigValue.ClientID %>").val(configValue);
         }

         //function updateDataForm() {
            
      
         //    alert(configValue);
         //    var handlerUrl = "../handlers/HandlerSettings.ashx?commandName=UpdateAppConfig&id=" + $("#Code").value + "&name=" + configName + "&value=" + configValue;

         //    $.ajax({
         //        url: handlerUrl,
         //        async: true,
         //        beforeSend: function () {

         //        },
         //        success: function (queryResult) {
         //            refresh();
         //            $('#formDataFrameModel').modal('hide');
         //        },
         //        error: function (xhr, ajaxOptions, thrownError) {

         //            console.log("Error");
         //        }
         //    });

  
         //}

    </script>


   <asp:HiddenField ID="hidConfigCode" runat="server"/>
    <asp:HiddenField ID="hidConfigName" runat="server"/>
    <asp:HiddenField ID="hidConfigValue" runat="server"/>

    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th><span data-lang="ConfigNameTitle">Name</span></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
           
        </tbody>
    </table>

    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
            <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h1 class="modal-title center" data-lang="AppConfigCaptionTitle">Application Configuration</h1>
		    </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <span data-lang="ConfigNameSetupInputTitle">Name</span><span class="requiredmark">*</span>     
                        <input type="hidden" data-attribute="Code" id="Code" />
                    </div>
                    <div class="col-md-10 col-sm-10"> 
                        <input class="form-control" type="text" required=""  id="configName" />  
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-2 col-sm-2">
                        <span data-lang="ConfigValueSetupInputTitle">Value</span><span class="requiredmark">*</span>     
                    </div>
                    <div class="col-md-10 col-sm-10"> 
                         <textarea class="form-control" name="editor1" id="configValue" rows="100" cols="80"  required="" >
                        </textarea>     
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" runat="server" onclick="moveData();" onserverclick="btnSubmit_ServerClick" >Save</button>
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
            </div>
            </div>
        </div>
    </div>

    </asp:Content>

    
