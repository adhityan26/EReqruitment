﻿<%@ Page Title="Source References" Language="C#" AutoEventWireup="true" MasterPageFile="~/masters/recruiter.Master" CodeBehind="sourcereferences.aspx.cs" Inherits="ERecruitment.sourcereferences" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            loadData();
        });

        function doFilter() {
            loadData();
        }

        $('body').on('click', '.edit', function () {
            var code = $(this).attr('name');
            showDataForm(code);
        });

        $('body').on('click', '.delete', function () {
            var code = $(this).attr('name');
            confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
                $.ajax({
                    url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveSourceReferences&Code=" + code,
                    async: true,
                    beforeSend: function () {

                    },
                    success: function (queryResult) {
                        var tableData = $('#tableData').dataTable();
                        tableData.fnDraw();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        showNotification(xhr.responseText);
                        var tableData = $('#tableData').dataTable();
                        tableData.fnDraw();
                    }
                });
            });
        });

        function loadData() {
            var ex = document.getElementById('tableData');
            if ($.fn.DataTable.fnIsDataTable(ex)) {
                $("#tableData").dataTable().fnDestroy();
            }

            var OTableData = $('#tableData').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "bJQueryUI": true,
                "bAutoWidth": false,
                "sDom": "ftipr",
                "bDeferRender": true,
                "aoColumnDefs": [
                    { "bSortable": false, "aTargets": [0, 1, 2] },
                    { "sClass": "controlIcon", "aTargets": [0, 1] },
                    { "bVisible": false, "aTargets": [2] }

                ],
                "oLanguage":
                { "sSearch": "Search By Name" },

                "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetSourceReferencesList"
            });
        }

        function refresh() {
            var tableData = $('#tableData').dataTable();
            tableData.fnDraw();
        }

        function showForm(code) {

            showDataForm(code);
        }

        function Add() {

            showDataForm("0");
        }

        function showDataForm(dataId) {

            var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetSourceReferences&Code=" + dataId;

            $.ajax({
                url: handlerUrl,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {

                    var dataModel = $.parseJSON(queryResult);
                    loadDataIntoForm("formDataFrameModel", dataModel);

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });

            $('#formDataFrameModel').modal('show');
        }

        function updateDataForm() {

            var form = "formDataFrameModel";
            var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateSourceReference";
            var formData = {};

            if (!getNodeData(form, formData)) return false;
            var dataSet = {
                "Code": $("input[data-attribute='Code']").val(),
                "Name": $("input[data-attribute='Name']").val()
            }

            $.post(actionUrl, dataSet).done(function (data) {
                refresh();
                $('#formDataFrameModel').modal('hide');

            }).fail(function (xhr, status, error) {
                showNotification(xhr.responseText);
            });
        }
        
    </script>
    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
        </div>
    </div>
    
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th>Name</th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>


    <div class="modal fade" id="formDataFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 class="modal-title center" data-lang="RoleUserCaptionTitle">Source References</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3 col-sm-3">
                            <span data-lang="RoleNameUserInputTitle">Nama</span><span class="requiredmark">*</span>
                            <input type="hidden" data-attribute="Code" />
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" type="text" required="" data-attribute="Name" maxlength="50"/>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
