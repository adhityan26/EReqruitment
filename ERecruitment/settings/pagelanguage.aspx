﻿<%@ Page Title="Page Language" Language="C#" MasterPageFile="~/masters/languagesetting.Master" AutoEventWireup="true" CodeBehind="pagelanguage.aspx.cs" Inherits="ERecruitment.pagelanguage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">
        
        $(document).ready(function () {

            $("#liPages").addClass("active");
            loadPageLabel();

        });

        function loadPageLabel() {

            var companyCode = $("#<%=hidMainCompanyCode.ClientID %>").val();
            var pageCode = $("#<%=ddPage.ClientID %>").val();

            var param = "&companyCode=" + companyCode;
            param += "&pageCode=" + pageCode;
            var listContainer = $("#contentContainer");

            $.ajax({
                url: "../handlers/HandlerSettings.ashx?commandName=LoadPageLabels" + param,
                async: true,
                beforeSend: function () {

                    $(".labelRow").remove();

                },
                success: function (queryResult) {

                    var modelDataList = $.parseJSON(queryResult);
                    $.each(modelDataList, function (i, item) {

                        var wrapper = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");

                        var labelSection = $(document.createElement('div')).attr("class", "col-sm-2");
                        var label = $(document.createElement('label')).text(item["Caption"]);
                        labelSection.append(label);

                        var indonesianTextSection = $(document.createElement('div')).attr("class", "col-sm-5");
                        var indonesianText = $(document.createElement('textarea')).attr("data-attribute", item["Label"] + "_IDText").attr("class", "form-control").val(item["IDText"]);
                        indonesianTextSection.append(indonesianText);

                        var englishTextSection = $(document.createElement('div')).attr("class", "col-sm-5");
                        var englishText = $(document.createElement('textarea')).attr("data-attribute", item["Label"] + "_ENText").attr("class", "form-control").val(item["ENText"]);
                        englishTextSection.append(englishText);
                        wrapper.append(labelSection);
                        wrapper.append(indonesianTextSection);
                        wrapper.append(englishTextSection);

                        var dataNode = $(document.createElement('div')).attr("class", "row labelRow").append(wrapper);

                        listContainer.append(dataNode);

                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);

                }
            });
        }

        function saveLanguageSetting() {
            
            var companyCode = $("#<%=hidMainCompanyCode.ClientID %>").val();
            var pageCode = $("#<%=ddPage.ClientID %>").val();

            var param = "&companyCode=" + companyCode;
            param += "&pageCode=" + pageCode;

            var form = "contentContainer";
            var actionUrl = "../handlers/HandlerSettings.ashx?commandName=UpdatePageLanguageSettings" + param;
            var formData = {};

            if (!getNodeData(form, formData)) return false;

            $.post(actionUrl, formData).done(function (data) {

                showNotification("Language settings updated");

            });
        }

    </script>

    <asp:HiddenField ID="hidMainCompanyCode" runat="server" />

    <div id="contentContainer">
        <div class="row searchPanel">
            <div class="col-md-12 col-sm-12">
                <div class="col-sm-2">
                    <asp:DropDownList ID="ddPage"
                                        runat="server"
                                        CssClass="form-control"
                                        DataValueField="ID"
                                        DataTextField="Name"
                                        onchange="loadPageLabel();"
                                        />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="col-sm-2"><strong><span data-lang="LabelsLangTitle">Labels</span></strong></div>
                <div class="col-sm-5"><strong><span data-lang="IndonesiaLangTitle">Indonesia</span></strong></div>
                <div class="col-sm-5"><strong><span data-lang="EnglishLangTitle">English</span></strong></div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="col-sm-2"></div>
            <div class="col-sm-5">
                <button class="btn btn-default commandIcon" onclick="saveLanguageSetting(); return false;"><i class="fa fa-save"></i>Save</button>
            </div>
        </div>
    </div>
</asp:Content>
