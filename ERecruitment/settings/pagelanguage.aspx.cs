﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;

namespace ERecruitment
{
    public partial class pagelanguage : AuthorizedPage
    {
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                ddPage.DataSource = PageManager.GetTranslatablePageList();
                ddPage.DataBind();

                hidMainCompanyCode.Value = MainCompany.Code;
            }
        }
    }
}