﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
namespace ERecruitment
{
    public partial class awardtype: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                
            }
            if (Request.Form["__EVENTARGUMENT"] == "View")
            {
                // Fire event
                IniateInformation();
            }
        }

        private void IniateInformation()
        {
            string code = hidCode.Value;
            Awards data = ERecruitmentManager.GetAward(code);
            tbName.Value = data.Award;
        }
        private void ClearControl()
        {
            tbName.Value = string.Empty;
            hidCode.Value = string.Empty;
            JQueryHelper.InvokeJavascript("showNotification('Data successfully saved');loadData();", Page);
        }
        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(hidCode.Value))
            {
                //save
                Awards addAwards = new Awards();
                addAwards.Award = tbName.Value;

                addAwards.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                addAwards.Active = true;
                ERecruitmentManager.SaveAward(addAwards);
                ClearControl();
            }
            else
            {
                Awards getAward = ERecruitmentManager.GetAward(hidCode.Value);
                getAward.Award = tbName.Value;

                ERecruitmentManager.UpdateAward(getAward);
                ClearControl();
            }

        }

        protected void btnDetail_Click(object sender, EventArgs e)
        {

        }
    }
}