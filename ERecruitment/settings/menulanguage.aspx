﻿<%@ Page Title="Menu Language Settings" Language="C#" MasterPageFile="~/masters/languagesetting.Master" AutoEventWireup="true" CodeBehind="menulanguage.aspx.cs" Inherits="ERecruitment.menulanguage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">


    $(document).ready(function () {

        $("#liMenus").addClass("active");
    });

    </script>

    <div class="row searchPanel">
        <div class="col-md-12 col-sm-12">
            <div class="col-sm-1 "><label>Language</label></div>
            <div class="col-sm-2">

                <asp:DropDownList ID="ddLanguage"
                                    runat="server"
                                    CssClass="form-control"
                                    DataValueField="Code"
                                    DataTextField="Name"
                                    />

            </div>
        </div>
    </div>
</asp:Content>
