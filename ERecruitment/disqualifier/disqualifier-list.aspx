﻿<%@ Page Title="Disqualifier Questions" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="disqualifier-list.aspx.cs" Inherits="ERecruitment.disqualifier_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<script type="text/javascript"> 


    $(document).ready(function () {

        loadData();
        bindToggleAnswerType();
        
        $(".multipleChoice").hide();
        $(".numericAnswer").hide();
        var ddType = 'ddAnswerType';
        $("#" + ddType).unbind("change");
        $("#" + ddType).change(function (event) {

            var type = $("#" + ddType).val();
            if (type == "Numeric") {
                $(".multipleChoice").hide();
                $(".numericAnswer").show();

            }
            else {
                $(".multipleChoice").show();
                $(".numericAnswer").hide();
            }

            $(".answerOption").remove();
            answerIndex = 0;
        });
    });

    

    function bindToggleAnswerType() {
        var ddType = 'ddAnswerType';
        $("#" + ddType).unbind("change");
        $("#" + ddType).change(function (event) {

            var type = $("#" + ddType).val();
            if (type == "Numeric") {
                $(".multipleChoice").hide();
                $(".numericAnswer").show();

            }
            else {
                $(".multipleChoice").show();
                $(".numericAnswer").hide();
            }

            $(".answerOption").remove();
            answerIndex = 0;
        });
        
    }

    $('body').on('click', '.delete', function () {

        var code = $(this).attr('name');

        confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
            $.ajax({
                url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveDisqualifierQuestion&id=" + code,
                async: true,
                beforeSend: function () {

                },
                success: function (queryResult) {
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);
                    // refresh
                    var tableData = $('#tableData').dataTable();
                    tableData.fnDraw();
                }
            });
        });
    });

    var selectedItemArray = [];
    $('body').on('click', '.cbChild', function () {

        var code = $(this).attr("data-attribute");

        var index = $.inArray(code, selectedItemArray);

        if ($(this).is(':checked')) {
            selectedItemArray.push(code);

        }
        else {
            selectedItemArray.splice(index, 1);
        }
    });

    function deleteQuestion() {


        var validStatus = true;
        var codeList = selectedItemArray;

        if (codeList == "")
            showNotification("please select a question");
        else {
            if (!validStatus) {
                showNotification("Invalid Operation, One or more selected job is not in 'Unposted' status");
                return;
            }
            else {
                if (codeList.length > 0) {
                    confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
                        $.ajax({
                            url: "../handlers/HandlerGlobalSettings.ashx?commandName=RemoveDisqualifierQuestion&id=" + codeList,
                            async: true,
                            beforeSend: function () {

                            },
                            success: function (queryResult) {
                                showNotification("question deleted");
                                refresh();
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                showNotification(xhr.responseText);
                                refresh();
                            }
                        });
                    });
                }
            }

        }


    }



    // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
    $('body').on('click', '.edit', function () {
        
        var code = $(this).attr('name');
        window.location.href = "disqualifier-form.aspx?code=" + code;
      
    });
   
    function loadData()
    {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }
      
        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0] },
                   { "bVisible": false, "aTargets": [2] },
                   { "sClass": "controlIcon", "aTargets": [0] }
                              
            ],
            "oLanguage": 
                               { "sSearch": "Search By Name" },
                        
            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetDisqualifierQuestionList"

        });
    }

    
    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function showForm(code) {

        showDataForm(code);
    }

    function showDataForm(dataId) {

        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetDisqualifierQuestion&id=" + dataId;

        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {
                $(".answerOption").remove();
            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("formDataFrameModel", dataModel);

                var ddType = 'ddAnswerType';
                var type = $("#" + ddType).val();
                if (type == "Numeric") {
                    $(".multipleChoice").hide();
                    $(".numericAnswer").show();

                }
                else {
                    $(".multipleChoice").show();
                    $(".numericAnswer").hide();
                }

                // list
                var listContainer = $("#answerContainer");
                answerIndex = 0;
                $.each(dataModel["AnswerOptionList"], function (i, item) {

                    var answerSection = $(document.createElement('div')).attr("class", "row answerOption");
                    var answerInput = $(document.createElement('div')).attr("class", "col-md-8 col-sm-8").append($(document.createElement('input')).attr("type", "text").attr("class", "form-control").attr("data-attribute", "List.AnswerOptionList.Label." + answerIndex).val(item["Label"]));
                    
                    var checkboxInput = $(document.createElement('div')).attr("class", "col-md-1 col-sm-1").append($(document.createElement('input')).attr("type", "radio").attr("name", "iscorrectanswer").attr("data-attribute", "List.AnswerOptionList.IsCorrectAnswer." + answerIndex).attr("checked", item["IsCorrectAnswer"]));
                    if (item["IsCorrectAnswer"] == true)
                        checkboxInput = $(document.createElement('div')).attr("class", "col-md-1 col-sm-1").append($(document.createElement('input')).prop("checked",true).attr("type", "radio").attr("name", "iscorrectanswer").attr("data-attribute", "List.AnswerOptionList.IsCorrectAnswer." + answerIndex).attr("checked", item["IsCorrectAnswer"]));
                    var correctAnswerLabel = $(document.createElement('div')).attr("class", "col-md-3 col-sm-3").attr("data-lang", "CorrectAnswerInputTitle").text("Correct Answer");

                    answerSection.append(answerInput)
                                 .append(checkboxInput)
                                 .append(correctAnswerLabel);
                    listContainer.append(answerSection);
                    answerIndex++;

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#formDataFrameModel').modal('show');
    }

    function updateDataForm() {

        var form = "formDataFrameModel";
        var actionUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=UpdateDisqualifierQuestion";
        var formData = {};

        if (!getNodeData(form, formData)) return false;

        $.post(actionUrl, formData).done(function (data) {

            refresh();
            $('#formDataFrameModel').modal('hide');

        });
    }

    var answerIndex = 0;
    function addAnswer() {
        
        var listContainer = $("#answerContainer");
        var answerSection = $(document.createElement('div')).attr("class", "row answerOption");
        var answerInput = $(document.createElement('div')).attr("class", "col-md-8 col-sm-8").append($(document.createElement('input')).attr("type", "text").attr("class", "form-control").attr("data-attribute", "List.AnswerOptionList.Label." + answerIndex));
        var checkboxInput = $(document.createElement('div')).attr("class", "col-md-1 col-sm-1").append($(document.createElement('input')).attr("type", "radio").attr("name", "iscorrectanswer").attr("data-attribute", "List.AnswerOptionList.IsCorrectAnswer." + answerIndex));
        var correctAnswerLabel = $(document.createElement('div')).attr("class", "col-md-3 col-sm-3").text("Correct Answer");

        answerSection.append(answerInput)
                     .append(checkboxInput)
                     .append(correctAnswerLabel);
        listContainer.append(answerSection);
        answerIndex++;
    }

</script>


    <div class="row">
        <div class="col-sm-12">
            <button class="btn btn-default commandIcon" onclick="showForm(); return false;"><i class="fa fa-file"></i>New</button>
       <button class="btn btn-default commandIcon" onclick="deleteQuestion();" type="button"><i class="fa fa-trash"></i>Delete</button>
        </div>
    </div>    
           
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
            <tr>
                <th><input id="checkMaster" type="checkbox" onchange="checkAllAndHighLight('checkMaster','.cbChild');" /></th>
                <th><span data-lang="DisqualifierInputTitle">Name</span></th>
                <th></th>
            </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
       

<div class="modal fade" id="formDataFrameModel">
    <div class="modal-dialog">
		<div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h1 class="modal-title center" data-lang="DisqualifierQuestionCaptionTitle">Disqualifier Question</h1>
		</div>
        <div class="modal-body">
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <span data-lang="DisNameInputTitle">Name</span><span class="requiredmark">*</span>
                    <input type="hidden" data-attribute="Code" />
                </div>
                <div class="col-md-7 col-sm-7"> 
                    <input class="form-control" type="text" required="" data-attribute="Name"/>  
                    <input type="hidden" data-attribute="Type" value="Parent" />
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <span data-lang="QuestionNameInputTitle">Question</span><span class="requiredmark">*</span>
                </div>
                <div class="col-md-9 col-sm-9"> 
                    <input class="form-control" type="text" required="" data-attribute="Question"/> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-3">
                    <span data-lang="AnswerTypeInputTitle">Answer Type</span>
                </div>
                <div class="col-md-4 col-sm-4"> 
                    <select class="form-control" id="ddAnswerType" data-attribute="AnswerFormat">
                        <option value="MultipleChoice">Multiple Choice</option>
                        <option value="Numeric">Numeric</option>
                    </select>
                </div>
            </div>            
            <div class="row multipleChoice">
                <div class="col-md-3 col-sm-3">
                    <span data-lang="AnswerRangeInputTitle">Answer Range:</span>
                </div>
            </div>
            <div class="row multipleChoice">
                <div class="col-md-12 col-sm-12" id="answerContainer">
                </div>
            </div>
            <div class="row multipleChoice">
                <div class="col-md-8 col-sm-8"> 
                    <a class="btn btn-default" onclick="addAnswer(); return false;"><i class="fa fa-plus-circle"></i> Add Answer</a>
                </div>
            </div>
            <div class="row numericAnswer">
                <div class="col-md-3 col-sm-3">
                    <span data-lang="CorrectAnswerInputTitle">Correct Answer</span>
                </div>
                <div class="col-md-2 col-sm-2">
                    <select class="form-control" id="ddAnswerOperator" data-attribute="CorrectAnswerOperator">
                        <option value="=">=</option>
                        <option value=">">></option>
                        <option value="<"><</option>
                        <option value=">=">>=</option>
                        <option value="<="><=</option>
                        <option value="!=">!=</option>
                    </select>
                </div>
                <div class="col-md-2 col-sm-2"> 
                    <input class="form-control plainnumber" type="text" data-attribute="CorrectAnswerLabel"/> 
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-primary" onclick="updateDataForm(); return false;">Save</button>
            <button type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
        </div>
        </div>
    </div>
</div>



</asp:Content>
