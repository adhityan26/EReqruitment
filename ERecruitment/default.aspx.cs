﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Configuration;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.WebControls;

namespace ERecruitment
{
    public partial class _default : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string nextPage = "";
                if (HttpContext.Current.Session["nextPage"] != null)
                {
                    nextPage = HttpContext.Current.Session["nextPage"].ToString();
                }
                litAddressLine1.Text = MainCompany.AddressLine1;
                litAddressLine2.Text = MainCompany.AddressLine2;
                litAddressLine3.Text = MainCompany.AddressLine3;

                List<string> videoPlayerCommandList = new List<string>();
                if (!string.IsNullOrEmpty(MainCompany.CoverVideoUrl))
                {
                    // clear background, set video
                    mainCover.Attributes["class"] = "videoCover item active";
                    string videoScript = @"var mainVideo = new YT.Player('mainVideo', {
                                              videoId: '[videoId]',
                                              events: {
                                                'onStateChange': function(event) {
                                                  if (event.data == YT.PlayerState.PLAYING) {
                                                      stopCarousel();
                                                  }
                                                }
                                              }
                                            });";

                    videoScript = videoScript.Replace("[videoId]", MainCompany.CoverVideoUrl);
                    mainCover.InnerHtml = "<div id='mainVideo'></div>";
                    videoPlayerCommandList.Add(videoScript);
                }

                List<string> commandImage = new List<string>();

                if (MainCompany.CoverImage1 != null)
                    commandImage.Add("mainCover1");
                else if (!string.IsNullOrEmpty(MainCompany.CoverVideoUrl1))
                {
                    commandImage.Add("videoCover1");
                    string videoScript = @"  var video1 = new YT.Player('video1', {
                                              videoId: '[videoId]',
                                              events: {
                                                'onStateChange': function(event) {
                                                  if (event.data == YT.PlayerState.PLAYING) {
                                                      stopCarousel();
                                                  }
                                                }
                                              }
                                            });
                                          ";
                    videoScript = videoScript.Replace("[videoId]", MainCompany.CoverVideoUrl1);
                    videoPlayerCommandList.Add(videoScript);
                }

                if (MainCompany.CoverImage2 != null)
                    commandImage.Add("mainCover2");
                else if (!string.IsNullOrEmpty(MainCompany.CoverVideoUrl2))
                {
                    commandImage.Add("videoCover2");
                    string videoScript = @"  var video2 = new YT.Player('video2', {
                                              videoId: '[videoId]',
                                              events: {
                                                'onStateChange': function(event) {
                                                  if (event.data == YT.PlayerState.PLAYING) {
                                                      stopCarousel();
                                                  }
                                                }
                                              }
                                            });
                                          ";
                    videoScript = videoScript.Replace("[videoId]", MainCompany.CoverVideoUrl2);
                    videoPlayerCommandList.Add(videoScript);
                }

                if (MainCompany.CoverImage3 != null)
                    commandImage.Add("mainCover3");
                else if (!string.IsNullOrEmpty(MainCompany.CoverVideoUrl3))
                {
                    commandImage.Add("videoCover3");
                    string videoScript = @"  var video3 = new YT.Player('video3', {
                                              videoId: '[videoId]',
                                              events: {
                                                'onStateChange': function(event) {
                                                  if (event.data == YT.PlayerState.PLAYING) {
                                                      stopCarousel();
                                                  }
                                                }
                                              }
                                            });
                                          ";
                    videoScript = videoScript.Replace("[videoId]", MainCompany.CoverVideoUrl3);
                    videoPlayerCommandList.Add(videoScript);
                }

                if (MainCompany.CoverImage4 != null)
                    commandImage.Add("mainCover4");
                else if (!string.IsNullOrEmpty(MainCompany.CoverVideoUrl4))
                {
                    commandImage.Add("videoCover4");
                    string videoScript = @"  var video4 = new YT.Player('video4', {
                                              videoId: '[videoId]',
                                              events: {
                                                'onStateChange': function(event) {
                                                  if (event.data == YT.PlayerState.PLAYING) {
                                                      stopCarousel();
                                                  }
                                                }
                                              }
                                            });
                                          ";
                    videoScript = videoScript.Replace("[videoId]", MainCompany.CoverVideoUrl4);
                    videoPlayerCommandList.Add(videoScript);
                }

                repCarouselSlides.DataSource = commandImage;
                repCarouselSlides.DataBind();

                string script = @"function onYouTubeIframeAPIReady() {
                                            [videoPlayerScripts]
                                            
                                          }";

                script = script.Replace("[videoPlayerScripts]", string.Join(Environment.NewLine, videoPlayerCommandList.ToArray()));
                JQueryHelper.InvokeJavascript(script, Page);

                repJobCategories.DataSource = ERecruitmentManager.GetVacancyCategoryList();
                repJobCategories.DataBind();

                Session.RemoveAll();
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetExpires(DateTime.Now);
                Response.Cache.SetNoServerCaching();
                Response.Cache.SetNoStore();

                if (Utils.GetQueryString<bool>("logout"))
                    Session.RemoveAll();

                if (Utils.GetQueryString<bool>("registerd"))
                {
                    Session.RemoveAll();
                    JQueryHelper.InvokeJavascript("showNotification('Registration completed, please check your email');", Page);
                }

                if (!String.IsNullOrEmpty(nextPage))
                {
                    HttpContext.Current.Session["nextPage"] = nextPage;   
                }
            }
        }

        protected void repCarouselSlides_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                string carouselId = e.Item.DataItem as string;

                HtmlGenericControl coverContainer = e.Item.FindControl("carouselItemContainer") as HtmlGenericControl;
                if (carouselId.Contains("video"))
                {
                    // clear background, set video
                    coverContainer.Attributes["class"] = "videoCover item";
                    if (carouselId.Contains("1"))
                    {

                        coverContainer.InnerHtml = "<div id='video1'></div>";
                    }

                    if (carouselId.Contains("2"))
                    {

                        coverContainer.InnerHtml = "<div id='video2'></div>";
                    }

                    if (carouselId.Contains("3"))
                    {

                        coverContainer.InnerHtml = "<div id='video3'></div>";
                    }

                    if (carouselId.Contains("4"))
                    {
                        coverContainer.InnerHtml = "<div id='video4'></div>";
                    }

                }
                else
                {
                    coverContainer.Attributes["class"] = "item " + carouselId;
                }

            }
        }
    }
}