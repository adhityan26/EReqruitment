﻿<%@ Page Title="Pipeline List" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="pipeline-list.aspx.cs" Inherits="ERecruitment.pipeline_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

   <script type="text/javascript">
       $(document).ready(function () {

           loadData();
       });

       function showForm(code) {

           var left = 0;
           var top = 0;
           var width = 1050;
           var height = 600;
           var urlPage = "pipeline-profile.aspx";
           urlPage = urlPage + "?code=" + code;
           window.location.href = urlPage;
       }

       function Add() {

           window.location.href = "pipeline-profile.aspx";
       }

       function deletePipeline() {
           var idList = "";
           var tableData = $('#tableData').dataTable();
           $('input:checked', tableData.fnGetNodes()).each(function () {

               idList += (tableData.fnGetData($(this).closest('tr')[0])[2] + ',');
           });
           if (idList == "")
               showNotification("please select pipeline");
           else {
               confirmMessage("Are you sure want to delete the selected records?", "warning", function () {
                   $.ajax({
                       url: "../handlers/HandlerSettings.ashx?commandName=RemovePipeline&codes=" + idList,
                       async: true,
                       beforeSend: function () {

                       },
                       success: function (queryResult) {
                           showNotification("pipeline removed");
                           refresh();
                       },
                       error: function (xhr, ajaxOptions, thrownError) {
                           showNotification(xhr.responseText);

                       }
                   });
               });
           }

       }

       function refresh() {
           var tableData = $('#tableData').dataTable();
           tableData.fnDraw();
       }

       function loadData() {
           var ex = document.getElementById('tableData');
           if ($.fn.DataTable.fnIsDataTable(ex)) {
               // data table, then destroy first
               $("#tableData").dataTable().fnDestroy();
           }

           var OTableData = $('#tableData').dataTable({
               "bProcessing": true,
               "bServerSide": true,
               "iDisplayLength": 10,
               "bJQueryUI": true,
               "bAutoWidth": false,
               "sDom": "ftipr",
               "bDeferRender": true,
               "aoColumnDefs": [
                      { "bSortable": false, "aTargets": [0] },
                      { "bVisible": false, "aTargets": [2] },
                      { "sClass": "controlIcon", "aTargets": [0] }

               ],
               "oLanguage":
                               { "sSearch": "Search By Name" },
               "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetPipelineList"
           });
       }


    </script>
<asp:HiddenField ID="hidUrl" runat="server" />
<div class="row">
    <div class="col-sm-12">
       <button class="btn btn-default commandIcon" onclick="Add(); return false;" id="btnNew"><i class="fa fa-file"></i>New</button>
       <button class="btn btn-default commandIcon" onclick="deletePipeline();" type="button"><i class="fa fa-trash"></i>Delete</button>
    </div>
</div>

<table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
    <thead>
        <tr>
            <th></th>
            <th><span data-lang="PipelineNameTitle">Pipeline Name</span></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
           
    </tbody>
</table>

</asp:Content>
