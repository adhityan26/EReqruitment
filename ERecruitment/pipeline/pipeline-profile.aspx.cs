﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class pipeline_profile : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
                {
                    List<PipelineSteps> stepList = new List<PipelineSteps>();

                    PipelineSteps step = new PipelineSteps();
                    stepList.Add(step);

                    repSteps.DataSource = stepList;
                    repSteps.DataBind();
                }
                else
                    iniateForm(Utils.GetQueryString<string>("code"));
            }
        }

        #region support

        private List<PipelineSteps> getSteps()
        {
            List<PipelineSteps> stepList = new List<PipelineSteps>();
            int orderId = 1;

            foreach (RepeaterItem step in repSteps.Items)
            {

                DropDownList ddStatusCode = step.FindControl("ddStatusCode") as DropDownList;
                DropDownList ddEmailTemplate = step.FindControl("ddEmailTemplate") as DropDownList;
                DropDownList ddAssestment = step.FindControl("ddAssestment") as DropDownList;

                PipelineSteps addStep = new PipelineSteps();
                addStep.StatusCode = Utils.GetSelectedValue<string>(ddStatusCode);
                addStep.ApplicantEmailTemplateId = Utils.GetSelectedValue<int>(ddEmailTemplate);
                addStep.SortOrder = orderId;
                addStep.TestCode = Utils.GetSelectedValue<string>(ddAssestment);
                orderId++;
                stepList.Add(addStep);
            }

            return stepList;
        }

        private bool checkLicense()
        {
            ApplicationSettings getSetting = ERecruitmentManager.GetApplicationSetting();
            if (getSetting != null)
                if (!getSetting.ApplicantTrackingSystem)
                    return false;
                else
                    return true;
            else
                return true;
        }

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            Utils.InsertInitialListItem(dd);
        }

        private void iniateForm(string code)
        {
            Pipelines getPipeline = HiringManager.GetPipeline(code);
            if (getPipeline != null)
            {
                tbName.Text = getPipeline.Name;
                List<PipelineSteps> getSteps = HiringManager.GetPipelineStepList(code, false);
                repSteps.DataSource = getSteps;
                repSteps.DataBind();

                if (!string.IsNullOrEmpty(getPipeline.TestCodes))
                {
                    listTests.DataBind();
                    string[] testCodes = getPipeline.TestCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < testCodes.Length; i++)
                    {
                        ListItem item = listTests.Items.FindByValue(testCodes[i]);
                        if (item != null)
                            item.Selected = true;
                    }
                }

//                if (!string.IsNullOrEmpty(getPipeline.AttachmentCodes))
//                {
//                    listAttachments.DataBind();
//                    string[] attachmentCodes = getPipeline.AttachmentCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
//                    for (int i = 0; i < attachmentCodes.Length; i++)
//                    {
//                        ListItem item = listAttachments.Items.FindByValue(attachmentCodes[i]);
//                        if (item != null)
//                            item.Selected = true;
//                    }
//                }

                ddAutoDisqualifiedStatus.DataBind();
                ddReferedStatus.DataBind();
                ddHiredStatus.DataBind();
                ddDisqualifiedStatus.DataBind();

                ddAutoDisqualifiedStatusEmailTemplate.DataBind();
                ddReferedStatusEmailTemplate.DataBind();
                ddHiredStatusEmailTemplate.DataBind();
                ddDisqualifiedStatusEmailTemplate.DataBind();

                Utils.SelectItem<string>(ddAutoDisqualifiedStatus, getPipeline.AutoDisqualifiedRecruitmentStatusCode);
                Utils.SelectItem<string>(ddReferedStatus, getPipeline.ReferedDisqualifiedRecruitmentStatusCode);
                Utils.SelectItem<string>(ddHiredStatus, getPipeline.HiredRecruitmentStatusCode);
                Utils.SelectItem<string>(ddDisqualifiedStatus, getPipeline.DisqualifiedRecruitmentStatusCode);

                Utils.SelectItem<int>(ddAutoDisqualifiedStatusEmailTemplate, getPipeline.AutoDisqualifiedRecruitmentStatusEmailTemplateId);
                Utils.SelectItem<int>(ddReferedStatusEmailTemplate, getPipeline.ReferedDisqualifiedRecruitmentStatusEmailTemplateId);
                Utils.SelectItem<int>(ddHiredStatusEmailTemplate, getPipeline.HiredRecruitmentStatusEmailTemplateId);
                Utils.SelectItem<int>(ddDisqualifiedStatusEmailTemplate, getPipeline.DisqualifiedRecruitmentStatusEmailTemplateId);
            }
        }

        #endregion

        #region button

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
                {
                    Pipelines getPipeline = HiringManager.GetPipeline(Utils.GetQueryString<string>("code"));

                    getPipeline.Name = tbName.Text;

                    List<string> testCodeList = new List<string>();
                    foreach (ListItem item in listTests.Items)
                        if (item.Selected)
                            testCodeList.Add(item.Value);
                    getPipeline.TestCodes = string.Join(",", testCodeList.ToArray());

//                    List<string> attachmentCodeList = new List<string>();
//                    foreach (ListItem item in listAttachments.Items)
//                        if (item.Selected)
//                            attachmentCodeList.Add(item.Value);
//                    getPipeline.AttachmentCodes = string.Join(",", attachmentCodeList.ToArray());

                    getPipeline.AutoDisqualifiedRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddAutoDisqualifiedStatus);
                    getPipeline.ReferedDisqualifiedRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddReferedStatus);
                    getPipeline.HiredRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddHiredStatus);
                    getPipeline.DisqualifiedRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddDisqualifiedStatus);

                    getPipeline.AutoDisqualifiedRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddAutoDisqualifiedStatusEmailTemplate);
                    getPipeline.ReferedDisqualifiedRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddReferedStatusEmailTemplate);
                    getPipeline.HiredRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddHiredStatusEmailTemplate);
                    getPipeline.DisqualifiedRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddDisqualifiedStatusEmailTemplate);


                    List<PipelineTests> testList = new List<PipelineTests>();
//                    List<PipelineAttachments> attachmentList = new List<PipelineAttachments>();
                    List<PipelineSteps> stepList = getSteps();
                    List<string> tempStatusCodeList = new List<string>();
                    foreach (PipelineSteps step in stepList)
                    {
                        if (tempStatusCodeList.Contains(step.StatusCode))
                        {
                            JQueryHelper.InvokeJavascript("showNotification('Fail to save, duplicate status was found', 'error');", Page);
                            return;
                        }
                        else
                            tempStatusCodeList.Add(step.StatusCode);
                    }

                    if (stepList.Count > 0)
                        getPipeline.StepList = stepList;
                    HiringManager.UpdatePipeline(getPipeline);
                    JQueryHelper.InvokeJavascript("showNotification('Successfully updated');backToList();", Page);
                }
                else
                {
                    Pipelines addPipeline = new Pipelines();
                    addPipeline.Active = true;
                    addPipeline.Name = tbName.Text;

                    List<string> testCodeList = new List<string>();
                    foreach (ListItem item in listTests.Items)
                        if (item.Selected)
                            testCodeList.Add(item.Value);
                    addPipeline.TestCodes = string.Join(",", testCodeList.ToArray());


                    addPipeline.AutoDisqualifiedRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddAutoDisqualifiedStatus);
                    addPipeline.ReferedDisqualifiedRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddReferedStatus);
                    addPipeline.HiredRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddHiredStatus);
                    addPipeline.DisqualifiedRecruitmentStatusCode = Utils.GetSelectedValue<string>(ddDisqualifiedStatus);

                    addPipeline.AutoDisqualifiedRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddAutoDisqualifiedStatusEmailTemplate);
                    addPipeline.ReferedDisqualifiedRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddReferedStatusEmailTemplate);
                    addPipeline.HiredRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddHiredStatusEmailTemplate);
                    addPipeline.DisqualifiedRecruitmentStatusEmailTemplateId = Utils.GetSelectedValue<int>(ddDisqualifiedStatusEmailTemplate);

//                    List<string> attachmentCodeList = new List<string>();
//                    foreach (ListItem item in listAttachments.Items)
//                        if (item.Selected)
//                            attachmentCodeList.Add(item.Value);
//                    addPipeline.AttachmentCodes = string.Join(",", attachmentCodeList.ToArray());


                    List<PipelineTests> testList = new List<PipelineTests>();
//                    List<PipelineAttachments> attachmentList = new List<PipelineAttachments>();
                    List<PipelineSteps> stepList = getSteps();
                    List<string> tempStatusCodeList = new List<string>();
                    foreach (PipelineSteps step in stepList)
                    {
                        if (tempStatusCodeList.Contains(step.StatusCode))
                        {
                            JQueryHelper.InvokeJavascript("showNotification('Failed to save, duplicate step was found', 'error')", Page);
                            return;
                        }
                        else
                            tempStatusCodeList.Add(step.StatusCode);
                    }

                    if (stepList.Count > 0)
                        addPipeline.StepList = stepList;
                    HiringManager.SavePipeline(addPipeline);
                    JQueryHelper.InvokeJavascript("showNotification('Successfully saved');backToList();", Page);
                }
            }
            catch (Exception ex)
            {
                JQueryHelper.InvokeJavascript("showNotification('" + Utils.ExtractInnerExceptionMessage(ex) + "', 'error');", Page);
            }
        }

        protected void btnAddRow_ServerClick(object sender, EventArgs e)
        {
            List<PipelineSteps> stepList = getSteps();
            PipelineSteps step = new PipelineSteps();
            stepList.Add(step);
            repSteps.DataSource = stepList;
            repSteps.DataBind();
        }

        #endregion

        #region repeater

        protected void repFixPipeline_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.DataItem != null)
            {

                PipelineSteps pipelineStep = e.Item.DataItem as PipelineSteps;

                Label lbSteps = e.Item.FindControl("lbSteps") as Label;
                HiddenField hidStatusCode = e.Item.FindControl("hidStatusCode") as HiddenField;

                DropDownList ddStatusCode = e.Item.FindControl("ddStatusCode") as DropDownList;

                HtmlButton btnUp = e.Item.FindControl("btnUp") as HtmlButton;
                HtmlButton btnDown = e.Item.FindControl("btnDown") as HtmlButton;
                HtmlButton btnRemove = e.Item.FindControl("btnRemove") as HtmlButton;

                if (!string.IsNullOrEmpty(pipelineStep.StatusCode))
                {
                    ddStatusCode.DataBind();
                    Utils.SelectItem<string>(ddStatusCode, pipelineStep.StatusCode);
                }
            }

        }

        protected void repSteps_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {

            if (e.Item.DataItem != null)
            {

                PipelineSteps pipelineStep = e.Item.DataItem as PipelineSteps;

                Label lbSteps = e.Item.FindControl("lbSteps") as Label;
                HiddenField hidStatusCode = e.Item.FindControl("hidStatusCode") as HiddenField;

                DropDownList ddStatusCode = e.Item.FindControl("ddStatusCode") as DropDownList;
                DropDownList ddEmailTemplate = e.Item.FindControl("ddEmailTemplate") as DropDownList;
                DropDownList ddAssestment = e.Item.FindControl("ddAssestment") as DropDownList;


                HtmlButton btnUp = e.Item.FindControl("btnUp") as HtmlButton;
                HtmlButton btnDown = e.Item.FindControl("btnDown") as HtmlButton;
                HtmlButton btnRemove = e.Item.FindControl("btnRemove") as HtmlButton;

                ddAssestment.DataBind();
                Utils.SelectItem<string>(ddAssestment, pipelineStep.TestCode);
                if (!string.IsNullOrEmpty(pipelineStep.StatusCode))
                {
                    ddStatusCode.DataBind();
                    Utils.SelectItem<string>(ddStatusCode, pipelineStep.StatusCode);
                }

                if (pipelineStep.ApplicantEmailTemplateId > 0)
                {
                    ddEmailTemplate.DataBind();
                    Utils.SelectItem<int>(ddEmailTemplate, pipelineStep.ApplicantEmailTemplateId);
                }
            }
        }

        #endregion

        protected void repSteps_ItemCommand(object source, RepeaterCommandEventArgs e)
        {

        }

        protected void btnUp_ServerClick(object sender, EventArgs e)
        {
            Control btn = sender as Control;
            RepeaterItem currentItem = btn.Parent as RepeaterItem;
            List<PipelineSteps> stepList = new List<PipelineSteps>();
            foreach (RepeaterItem step in repSteps.Items)
            {
                DropDownList ddStatusCode = step.FindControl("ddStatusCode") as DropDownList;
                DropDownList ddEmailTemplate = step.FindControl("ddEmailTemplate") as DropDownList;
                DropDownList ddAssestment = step.FindControl("ddAssestment") as DropDownList;

                PipelineSteps addStep = new PipelineSteps();
                addStep.StatusCode = Utils.GetSelectedValue<string>(ddStatusCode);
                addStep.ApplicantEmailTemplateId = Utils.GetSelectedValue<int>(ddEmailTemplate);
                addStep.TestCode = Utils.GetSelectedValue<string>(ddAssestment);

                HtmlButton btnRemove = step.FindControl("btnRemove") as HtmlButton;
                if (step == currentItem && stepList.Count > 0)
                    stepList.Insert(stepList.Count - 1, addStep);
                else
                    stepList.Add(addStep);
            }

            repSteps.DataSource = stepList;
            repSteps.DataBind();
        }

        protected void btnDown_ServerClick(object sender, EventArgs e)
        {
            Control btn = sender as Control;
            RepeaterItem currentItem = btn.Parent as RepeaterItem;
            List<PipelineSteps> stepList = new List<PipelineSteps>();
            PipelineSteps tempPipeline = null;
            foreach (RepeaterItem step in repSteps.Items)
            {
                DropDownList ddStatusCode = step.FindControl("ddStatusCode") as DropDownList;
                DropDownList ddEmailTemplate = step.FindControl("ddEmailTemplate") as DropDownList;
                DropDownList ddAssestment = step.FindControl("ddAssestment") as DropDownList;

                PipelineSteps addStep = new PipelineSteps();
                addStep.StatusCode = Utils.GetSelectedValue<string>(ddStatusCode);
                addStep.ApplicantEmailTemplateId = Utils.GetSelectedValue<int>(ddEmailTemplate);
                addStep.TestCode = Utils.GetSelectedValue<string>(ddAssestment);

                if (step == currentItem)
                    tempPipeline = addStep;
                else
                {
                    stepList.Add(addStep);
                    if (tempPipeline != null)
                    {
                        stepList.Add(tempPipeline);
                        tempPipeline = null;
                    }
                }
            }

            repSteps.DataSource = stepList;
            repSteps.DataBind();
        }

        protected void btnRemove_ServerClick(object sender, EventArgs e)
        {
            Control btn = sender as Control;
            RepeaterItem currentItem = btn.Parent as RepeaterItem;
            List<PipelineSteps> stepList = new List<PipelineSteps>();
            foreach (RepeaterItem step in repSteps.Items)
            {
                if (step == currentItem)
                    continue;

                DropDownList ddStatusCode = step.FindControl("ddStatusCode") as DropDownList;
                PipelineSteps addStep = new PipelineSteps();
                addStep.StatusCode = Utils.GetSelectedValue<string>(ddStatusCode);
                stepList.Add(addStep);
            }

            repSteps.DataSource = stepList;
            repSteps.DataBind();
        }
    }
}