﻿<%@ Page Title="Pipeline Profile" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="pipeline-profile.aspx.cs" Inherits="ERecruitment.pipeline_profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">

        function backToList()
        {
            window.location.href = "pipeline-list.aspx";
        }

    </script>

    <div class="row">
        <div class="col-sm-12">
            
            <div class="row">
                <div class="col-sm-2">
                    <strong><span data-lang="PipelineNameInputTitle">Pipeline Name</span></strong>
                    <span class="requiredmark">*</span>
                </div>
                <div class="col-sm-5">
                    <asp:TextBox ID="tbName" runat="server" CssClass="form-control" required="" />    
                </div>  
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <strong><span data-lang="PipelineTestsInputTitle">Tests</span></strong>
                </div>
                <div class="col-sm-5">
                    <asp:ListBox ID="listTests" SelectionMode="Multiple" DataSourceID="odsTests" runat="server" CssClass="form-control" DataValueField="TestCode" DataTextField="TestName" data-attribute="TestCodes" />
                </div>  
            </div>
<%--            <div class="row">--%>
<%--                <div class="col-sm-2">--%>
<%--                    <strong><span data-lang="PipelineAttachmentsInputTitle">Attachments</span></strong>--%>
<%--                </div>--%>
<%--                <div class="col-sm-5">--%>
<%--                    <asp:ListBox ID="listAttachments" SelectionMode="Multiple" DataSourceID="odsAttachments" runat="server" CssClass="form-control" DataValueField="AttachmentCode" DataTextField="Name" data-attribute="AttachmentCodes" />                                        --%>
<%--                </div>  --%>
<%--            </div>--%>
            
            <div class="row">
                <div class="col-sm-2">
                    
                </div>
                <div class="col-sm-4">
                    <strong><span data-lang="PipelineStepsTitle">Steps</span></strong>
                </div> 
                <div class="col-sm-4">
                    <strong><span data-lang="PipelineEmailNotifTitle">Email Notification Template</span></strong>
                </div>  
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <strong><span data-lang="PipelineAutoDisqualifiedTitle">Auto Disqualified</span></strong>
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddAutoDisqualifiedStatus"
                                      runat="server"
                                      CssClass="form-control"
                                      DataSourceID="odsRecruitmentStatus"
                                      DataValueField="Code"
                                      DataTextField="Name"
                                      />
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddAutoDisqualifiedStatusEmailTemplate"
                                      runat="server"
                                      CssClass="form-control"
                                      Width="150"
                                      DataSourceID="odsApplicantEmail"
                                      DataValueField="Id"
                                      DataTextField="TemplateName"
                                      />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <strong><span data-lang="PipelineReferedTitle">Refered</span></strong>
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddReferedStatus"
                                      runat="server"
                                      CssClass="form-control"
                                      DataSourceID="odsRecruitmentStatus"
                                      DataValueField="Code"
                                      DataTextField="Name"
                                      />
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddReferedStatusEmailTemplate"
                                      runat="server"
                                      CssClass="form-control"
                                      Width="150"
                                      DataSourceID="odsApplicantEmail"
                                      DataValueField="Id"
                                      DataTextField="TemplateName"
                                      />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <strong><span data-lang="PipelineHiredTitle">Hired</span></strong>
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddHiredStatus"
                                      runat="server"
                                      CssClass="form-control"
                                      DataSourceID="odsRecruitmentStatus"
                                      DataValueField="Code"
                                      DataTextField="Name"
                                      />
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddHiredStatusEmailTemplate"
                                      runat="server"
                                      CssClass="form-control"
                                      Width="150"
                                      DataSourceID="odsApplicantEmail"
                                      DataValueField="Id"
                                      DataTextField="TemplateName"
                                      />
                </div>
            </div>
            <div class="row">
                <div class="col-sm-2">
                   <strong><span data-lang="PipelineRejectedTitle">Rejected</span></strong>
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddDisqualifiedStatus"
                                      runat="server"
                                      CssClass="form-control"
                                      DataSourceID="odsRecruitmentStatus"
                                      DataValueField="Code"
                                      DataTextField="Name"
                                      />
                </div>
                <div class="col-sm-4">
                    <asp:DropDownList ID="ddDisqualifiedStatusEmailTemplate"
                                      runat="server"
                                      CssClass="form-control"
                                      Width="150"
                                      DataSourceID="odsApplicantEmail"
                                      DataValueField="Id"
                                      DataTextField="TemplateName"
                                      />
                </div>
            </div>
            <asp:Repeater ID="repSteps" OnItemDataBound="repSteps_ItemDataBound" OnItemCommand="repSteps_ItemCommand" runat="server">
                <HeaderTemplate>
                    <table class="tableSteps table table-striped table-bordered dt-responsive nowrap">
                        <tr>
                            <th class="iconCell"></th>
                            <th class="iconCell"></th>
                            <th><strong><span data-lang="PipelineStepsTitle">Steps</span></strong></th>
                            <th><strong><span data-lang="PipelineEmailNotifTitle">Email Notification Template</span></strong></th>
                            <th class="hidden">Assestment</th>
                            <th class="iconCell"></th>
                        </tr>
                </HeaderTemplate>
                <ItemTemplate>
                        <tr>
                            <td>                                                                          
                                <button class="btn" id="btnUp" runat="server" onserverclick="btnUp_ServerClick">
                                    <i class="fa fa-arrow-circle-up text-primary"></i>
                                </button>
                            </td>
                            <td>                                                                          
                                <button class="btn" id="btnDown" runat="server" onserverclick="btnDown_ServerClick">
                                    <i class="fa fa-arrow-circle-down text-primary"></i>
                                </button>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddStatusCode" runat="server" Width="150" 
                                    CssClass="form-control" DataSourceID="odsRecruitmentSteps"
                                    DataValueField="Code" DataTextField="Name" />
                                     
                            </td>
                            <td>
                                <asp:DropDownList ID="ddEmailTemplate"
                                                  runat="server"
                                                  CssClass="form-control"
                                                  Width="150"
                                                  DataSourceID="odsApplicantEmail"
                                                  DataValueField="Id"
                                                  DataTextField="TemplateName"
                                                  />
                            </td>
                            <td class="hidden">
                                <asp:DropDownList ID="ddAssestment"
                                                  runat="server"
                                                  CssClass="form-control"
                                                  Width="150"
                                                  DataSourceID="odsTests"
                                                  DataValueField="TestCode"
                                                  DataTextField="TestName"
                                                  OnDataBound="ddDatabound"
                                                  />
                            </td>
                            <td>                                     
                                <button class="btn" id="btnRemove" runat="server" onserverclick="btnRemove_ServerClick">
                                    <i class="fa fa-trash-o text-primary"></i>
                                </button>
                            </td>
                        </tr>
                </ItemTemplate>
                <FooterTemplate>
                        <tr>
                            <td colspan="2">
                                <button class="btn btn-default" causesvalidation="false" onserverclick="btnAddRow_ServerClick" runat="server" 
                                id="btnAddRow">
                                <i class="fa fa-plus-circle"></i> Add Steps
                            </button>
                            </td>
                        </tr>
                    </table>
                </FooterTemplate>
            </asp:Repeater>   
            <div class="row">
                <div class="col-sm-3">
                    <button class="btn btn-primary"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
                            Save
                    </button>
                </div>
            </div>
            </div>
    </div>
    <asp:ObjectDataSource ID="odsApplicantEmail" runat="server"
                          DataObjectTypeName="ERecruitment.Domain.Emails"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          SelectMethod="GetEmailListByCategory"
        >
     <SelectParameters>
         <asp:Parameter Name="category" DefaultValue="ApplicantNotification" />
     </SelectParameters>
 </asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsRecruitmentSteps" 
                        runat="server"
                        DataObjectTypeName="ERecruitment.Domain.VacancyStatus"
                        TypeName="ERecruitment.Domain.VacancyManager"
                        SelectMethod="GetNonArchiveRecruitmentProcessList"
                        />
<asp:ObjectDataSource ID="odsRecruitmentStatus"
                          runat="server"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          DataObjectTypeName="ERecruitment.Domain.VacancyStatus"
                          SelectMethod="GetActiveVacancyStatusList"
                          />
<asp:ObjectDataSource ID="odsTests" 
                        runat="server"
                        DataObjectTypeName="ERecruitment.Domain.Tests"
                        TypeName="ERecruitment.Domain.HiringManager"
                        SelectMethod="GetTestList"
                        />
<asp:ObjectDataSource ID="odsAttachments" 
                        runat="server"
                        DataObjectTypeName="ERecruitment.Domain.TestAttachments"
                        TypeName="ERecruitment.Domain.HiringManager"
                        SelectMethod="GetTestAttachment"
                        />
</asp:Content>
