﻿<%@ Page Title="Site Setting" Language="C#" MasterPageFile="~/masters/recruiter.Master"  ValidateRequest="false" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="company-profile.aspx.cs" Inherits="ERecruitment.company_profile" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<style>
    .content-img a {
        position:absolute;
        right: 25px;
        top: 10px;
        width: 31px;
    }
</style>
    
<script type="text/javascript">

    $(document).ready(function () {
        CKEDITOR.config.toolbar_Custom =
            [
                { name: 'document', items: ['Source'] },
                { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo'] },
                { name: 'editing', items: ['Find', 'Replace', '-', 'SelectAll'] },
                '/',
                { name: 'basicstyles', items: ['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'] },
                {
                    name: 'paragraph', items: ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', 'CreateDiv',
                        '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock', '-', 'BidiLtr', 'BidiRtl']
                },
                '/',
                { name: 'links', items: ['Link', 'Unlink', 'Anchor'] },
                { name: 'insert', items: ['Image', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak', 'Iframe'] },
                '/',
                { name: 'styles', items: ['Styles', 'Format', 'Font', 'FontSize'] },
                { name: 'colors', items: ['TextColor', 'BGColor'] },
                { name: 'tools', items: ['Maximize', 'ShowBlocks', '-', 'About'] }
            ];

        CKEDITOR.config.toolbar = "Custom";

        $("#landingPage img.img-thumbnail").each(function() {

            var image_url = $(this).attr('src');
            var _this = this;
            if (image_url) {
                $.get(image_url)
                    .done(function(response) {
                        if (response !== "") {
                            attach_remove_button(_this);
                        }
                    });
            }
        });

        $("input[id*=Youtube]").change(function() {
            var row = $(this).parent().parent();
            if ($(this).val() != "") {
                $(row).find("input[type=file]").val("");
                $(row).find(".img-thumbnail").attr("src", "");
                $(row).find("input[type=hidden]").val("true");
            }
        });

        $(".img-cover").change(function() {
            var _this = this;
            var row = $(this).parent().parent();
            if ($(_this).val() != "") {
                var imgPrev = $(row).find(".img-thumbnail"); 
                $(row).find("input[id*=Youtube]").val("");
                $(imgPrev).attr("src", "");
                imgPrev = imgPrev.eq(0);
                $(row).find("input[type=hidden]").val("false");
                
                if (_this.files && _this.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $(imgPrev).attr('src', e.target.result);
                        attach_remove_button(imgPrev);
                    }

                    reader.readAsDataURL(_this.files[0]);
                }
            }
        });
    });
    
    function attach_remove_button(el) {
        var imgWrapper = $(el).parents("div").eq(1).find(".content-img");
        if (imgWrapper.length == 0) {
            $(el).wrap("<div class=\"content-img\"></div>");
            imgWrapper = $(el).parent();
        }
        imgWrapper.append("<a class=\"btn btn-danger btn-sm remove-img\" onclick=\"remove_image(this)\"><i class=\"fa fa-close\"></i></a>");
    }
    
    function remove_image(el) {
        var wrapper = $(el).parent();
        var wrapperParent = wrapper.parent();
        wrapperParent.find("input[type=file]").val("");
        wrapperParent.find("input[type=hidden]").val("true");
        wrapper.find("img").attr("src", "");
        wrapper.find("a").remove();
    }
    
    function backToList()
    {
        window.location.href = "company-list.aspx";
    }

    function showSurveyQuestion()
    {
        var dataId = $('#' + '<%= listSurveyQuestions.ClientID %>').val();

        var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetSurveyQuestionList&id=" + dataId;
        var listContainer = $("#surveyQuestionsContainer");
        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {
                $(".surveyQuestionItem").remove();
            },
            success: function (queryResult) {


                    var dataModel = $.parseJSON(queryResult);
                    $.each(dataModel, function (i, item) {

                        // survey question wrapper
                        var questionWrapper = $(document.createElement('div')).attr("class", "row jobSection surveyQuestionItem");
                        // question section
                        var questionSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('p')).attr("class", "text-muted").text(item["Question"]));
                        questionWrapper.append(questionSection);
                        type = item["Type"];
                        if (item["Type"] == "FiveStarPolls") {
                            // answer wrapper
                            var answerSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");
                            var starSelector = $(document.createElement('input')).attr("id", "input-1").attr("type", "number").attr("class", "rating").attr("min", "1").attr("max", "5").attr("step", "1").attr("size", "md").attr("name", item["Code"]).attr("data-attribute", item["Code"]);
                            var hiddenSelector = $(document.createElement('input')).attr("id", "hidden-1").attr("type", "hidden").attr("name", item["Code"]).attr("data-attribute", item["Code"]).attr("value", "");

                            answerSection.append(starSelector);

                            var notesLabel = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('p')).attr("class", "text-muted").text("Komentar Anda : "));
                            var notesAnswer = $(document.createElement('div')).attr("class", "col-md-6 col-sm-6").append($(document.createElement('textarea')).attr("id", "notes").attr("class", "form-control").attr("cols", 40).attr("rows", 4).attr("disallowed-chars", "[^a-zA-Zs ]+").attr("data-attribute", "Notes"));

                            questionWrapper.append(answerSection);
                            questionWrapper.append(hiddenSelector);
                            questionWrapper.append(notesLabel);
                            questionWrapper.append(notesAnswer);

                        }

                        if (item["Type"] == "Essay") {
                            // answer wrapper
                            var answerSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('input')).attr("type", "text").attr("id", "essay").attr("class", "form-control").attr("data-attribute", item["Code"]));
                            questionWrapper.append(answerSection);
                        }

                        listContainer.append(questionWrapper);

                        $('#input-1').rating('create');
                        $('#input-1').on('rating.change', function (event, value, caption) {
                            $('#hidden-1').val(value);
                        });
                        $('#input-1').on('rating.clear', function (event) {
                            $('#hidden-1').val("");
                        });

                    })

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#formSurveyQuestion').modal('show');
    }

    function showSendEmailForm()
    {
        $("#tbTestEmailAddress").val("");
        $('#formTestSMTP').modal('show');
    }

    function sendEmail()
    {
        var param = "&companyCode=" + GetQueryStringParams("code");
        param += "&emailAddress=" + $("#tbTestEmailAddress").val();

        var handlerUrl = "../handlers/HandlerSettings.ashx?commandName=SendTestEmail" + param;

        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {
                
            },
            success: function (queryResult) {
                showNotification("Email has been sent");
                $('#formTestSMTP').modal('hide');
            },
            error: function (xhr, ajaxOptions, thrownError) {

                showNotification(xhr.responseText);
            }
        });
    }

</script>



    
<ul class="nav tab-menu nav-tabs" id="myTab" role="tablist">
    <li class="active" role="presentation"><a role="tab" data-toggle="tab" aria-controls="basicInfo" aria-expanded="true" href="#basicInfo" data-lang="CompanyInfoSubMenu">Company Information</a></li>
    <li role="presentation" class="tab"><a role="tab" data-toggle="tab" aria-controls="landingPage" aria-expanded="true"  href="#landingPage" data-lang="LandingPageSubMenu">Landing Page Editor</a></li>
    <li role="presentation"><a role="tab" data-toggle="tab" aria-controls="applicationSetting" aria-expanded="true"  href="#applicationSetting" data-lang="AppSettingSubMenu">Application Setting</a></li> 
    <li role="presentation"><a role="tab" data-toggle="tab" aria-controls="emailSetting" aria-expanded="true"  href="#emailSetting" data-lang="EmailSettingSubMenu">Email Setting </a></li> 
    <li id="liCssTheme" runat="server" role="presentation"><a role="tab" data-toggle="tab" aria-controls="cssTheme" aria-expanded="true"  href="#cssTheme" data-lang="CSSThemeSubMenu">CSS Theme</a></li> 
</ul>
<div id="myTabContent" class="tab-content">   
    <div class="tab-pane active" id="basicInfo" role="tabpanel"> 
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="BusinessUnitCodeTitle">Business Unit Code</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-2">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbBusinessUnitCode"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CompanyNameTitle">CompanyName</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCompanyName"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
               <strong><span data-lang="CompanyAddressTitle">CompanyAddress</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input cols="50" rows="4" disallowed-chars="[^a-zA-Zs ]+" id="tbCompanyAddress1" 
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server" 
                        class="form-control" />         
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                
            </div>
            <div class="col-sm-5">
                <input cols="50" rows="4" disallowed-chars="[^a-zA-Zs ]+" id="tbCompanyAddress2" 
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" runat="server" 
                        class="form-control" />         
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                
            </div>
            <div class="col-sm-5">
                <input cols="50" rows="4" disallowed-chars="[^a-zA-Zs ]+" id="tbCompanyAddress3" 
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" runat="server" 
                        class="form-control" />         
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="ContactPersonTitle">Contact Person</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbContactPerson"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="ContactNumberTitle">ContactNumber</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-2">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbContactNumber"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
            </div> 
            <div class="col-sm-1">
                <label runat="server" id="Label6">Ext.</label><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-2">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbContactExtensionNumber"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="MobileNumberTitle">Mobile Number</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-2">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbMobileNumber"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
            </div> 
            <div class="col-sm-1">
                <strong><span data-lang="ContactTitle">Contact</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-2">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbMobileContact"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="IndustrySettingTitle">Industry</span></strong>
            </div>
            <div class="col-sm-5">
                    <select id="ddIndustry" datasourceid="odsIndustries" datatextfield="Name" datavaluefield="Code"
                        runat="server" class="form-control" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CompanyATSSiteTitle">Company ATS Site</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbATSSite"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server" required=""
                class="form-control" type="text" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CompanyWebsiteTitle">Company Website</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbWebsite"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server" required=""
                class="form-control" type="text" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CompanyFacebookTitle">Company Facebook</span></strong>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCompanyFacebook"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server"
                class="form-control" type="text" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CompanyLinkedIn">Company Linked In</span></strong>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCompanyLinkedIn"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server"
                class="form-control" type="text" />
            </div>  
        </div>
    </div>   
    <div class="tab-pane" id="landingPage" role="tabpanel"> 
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CoverPageImageTitle">Cover Page Image</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverImage" runat="server"> 
                <asp:FileUpload ID="fileCoverPageImage" runat="server" CssClass="img-cover"/>
                <p style="font-size:10px"> *Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
                <input type="hidden" id="hidRemoveCoverPageImage" value="false" runat="server"/>
            </div>
            <div class="col-sm-2">
                <strong><span data-lang="YoutubeVideoIDTitle">or Youtube Video ID</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverVideo" runat="server"> 
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbMainYoutubeID"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server"
                class="form-control" type="text" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CoverPageImage1Title">Cover Page Image 1</span></strong>
            </div>
            <div class="col-sm-3">
                 <img class="img-thumbnail" id="coverImage1" runat="server"> 
                 <asp:FileUpload ID="fileCoverPageImage1" runat="server" CssClass="img-cover" />
                 <p style="font-size:10px"> *Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
                <input type="hidden" id="hidRemoveCoverPageImage1" value="false" runat="server"/>
            </div>
            <div class="col-sm-2">
                <strong><span data-lang="YoutubeVideoIDTitle">or Youtube Video ID</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverVideo1" runat="server"> 
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbYoutubeID1"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server"
                class="form-control" type="text" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CoverPageImage2Title">Cover Page Image 2</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverImage2" runat="server"> 
                <asp:FileUpload ID="fileCoverPageImage2" runat="server" CssClass="img-cover" />
                <p style="font-size:10px"> *Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
                <input type="hidden" id="hidRemoveCoverPageImage2" value="false" runat="server"/>
            </div> 
            <div class="col-sm-2">
                 <strong><span data-lang="YoutubeVideoIDTitle">or Youtube Video ID</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverVideo2" runat="server"> 
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbYoutubeID2"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server"
                class="form-control" type="text" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CoverPageImage3Title">Cover Page Image 3</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverImage3" runat="server"> 
                <asp:FileUpload ID="fileCoverPageImage3" runat="server" CssClass="img-cover" />
                <p style="font-size:10px"> *Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
                <input type="hidden" id="hidRemoveCoverPageImage3" value="false" runat="server"/>
            </div>
            <div class="col-sm-2">
                 <strong><span data-lang="YoutubeVideoIDTitle">or Youtube Video ID</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverVideo3" runat="server"> 
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbYoutubeID3"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server"
                class="form-control" type="text" />
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CoverPageImage4Title">Cover Page Image 4</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverImage4" runat="server"> 
                <asp:FileUpload ID="fileCoverPageImage4" runat="server" CssClass="img-cover" />
                <p style="font-size:10px"> *Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
                <input type="hidden" id="hidRemoveCoverPageImage4" value="false" runat="server"/>
            </div>
            <div class="col-sm-2">
                 <strong><span data-lang="YoutubeVideoIDTitle">or Youtube Video ID</span></strong>
            </div>
            <div class="col-sm-3">
                <img class="img-thumbnail" id="coverVideo4" runat="server"> 
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbYoutubeID4"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3"  runat="server"
                class="form-control" type="text" />
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="AboutUsImageTitle">About Us Image</span></strong>
            </div>
            <div class="col-sm-5">
                <img class="img-thumbnail" id="aboutUsImage" runat="server"> 
                <asp:FileUpload ID="fileAboutUs" runat="server" />
                <p style="font-size:10px"> *Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
                <input type="hidden" id="hidRemoveAboutUsImage" value="false" runat="server"/>
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="CopyrightTextTitle">Copyright Text</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input required="" id="tbCopyRightText" runat="server" class="form-control" type="text"/>
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                
            </div>  
            <div class="col-sm-5">
                <strong><span data-lang="CopyrightTextTitle">Indonesia</span></strong>
            </div>
            <div class="col-sm-5">
                <strong><span data-lang="CopyrightTextTitle">English</span></strong>
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong>Our Story Title</strong>
            </div>  
            <div class="col-sm-5">
                <input id="tbOurStoryTitleId" runat="server" class="form-control" type="text"/>
            </div>
            <div class="col-sm-5">
                <input id="tbOurStoryTitleEn" runat="server" class="form-control" type="text"/>
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong>Our Story Sub Title</strong>
            </div>  
            <div class="col-sm-5">
                <input id="tbOurStorySubTitleId" runat="server" class="form-control" type="text"/>
            </div>
            <div class="col-sm-5">
                <input id="tbOurStorySubTitleEn" runat="server" class="form-control" type="text"/>
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong>Our Story</strong>
            </div>  
            <div class="col-sm-5">
                <CKEditor:CKEditorControl id="tbOurStoryId" form-field="registerForm" BasePath="/assets/plugins/ckeditor/" runat="server"></CKEditor:CKEditorControl>
            </div>
            <div class="col-sm-5">
                <CKEditor:CKEditorControl id="tbOurStoryEn" form-field="registerForm" BasePath="/assets/plugins/ckeditor/" runat="server"></CKEditor:CKEditorControl>
            </div>  
        </div>
    </div>   
    <div class="tab-pane" id="applicationSetting" role="tabpanel"> 
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="ApplicationTitle">Application Title</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <asp:TextBox ID="tbApplicationTitle" runat="server" required="" CssClass="form-control" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="LogoURLTitle">Logo Url</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">    <link rel="stylesheet" href="../assets/css/star-rating.css" />
                <img class="img-thumbnail" id="logoImage" runat="server"> 
                <asp:FileUpload ID="fileLogo" runat="server" />
                 <p style="font-size:10px"> *Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
            </div> 
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="FaviconUrlTitle">Favicon Url</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <img class="img-thumbnail" id="faviconImage" runat="server"> 
                <asp:FileUpload ID="fileFavicon" runat="server" />
                 <p style="font-size:10px"> *Max file size is 500KB </br> type : .ico</p>
            </div>  
        </div>
        
        <% if (CurrentUser.IsAdmin)
           { %>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="FacebookAPIKeyTitle">Facebook API Key</span></strong>
            </div>
            <div class="col-sm-5">
                <asp:TextBox ID="tbFacebookAPIKey" runat="server" CssClass="form-control" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
               <strong><span data-lang="LinkeInAPIKeyTitle">Linked In API Key</span></strong>
            </div>
            <div class="col-sm-5">
                <asp:TextBox ID="tbLinkedInAPIKey" runat="server" CssClass="form-control" />
            </div>  
        </div>
        <% } %>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="SurveyQuestionsTitle">Survey Questions*</span></strong>
            </div>
            <div class="col-sm-5">
                <asp:ListBox ID="listSurveyQuestions" SelectionMode="Multiple" runat="server" CssClass="form-control" DataValueField="Code" DataTextField="Name" data-attribute="RoleCodes" />  
            </div>  
            <div class="col-sm-2">
                <a class="btn btn-default" onclick="showSurveyQuestion(); return false;"><i class="fa fa-eye"></i>Preview</a>
            </div>
        </div>
        <div class="row" style="display:none">
            <div class="col-sm-2">
                <strong><span data-lang="InvitationTemplateTitle">Invitation Template</span></strong>
            </div>
            <div class="col-sm-5">
                <asp:DropDownList ID="ddInvitationEmailTemplate"
                                            runat="server"
                                            CssClass="form-control"
                                            Width="150"
                                            DataSourceID="odsApplicantEmail"
                                            DataValueField="Id"
                                            DataTextField="TemplateName"
                                            />
            </div>
        </div>
        <div class="row" style="display:none">
            <div class="col-sm-2">
                <strong><span data-lang="SurveyDeliveryTermsTitle">Survey Delivery Terms</span></strong>
            </div>
            <div class="col-sm-5">
                <asp:DropDownList ID="ddSurveyDeliveryTerms" runat="server" CssClass="form-control">
                    <asp:ListItem Value="OneMonthAfterRegistration" Text="One Month After Registration" />
                </asp:DropDownList>
            </div>
        </div>
        <% if (CurrentUser.IsAdmin)
           { %>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="SiteCoverageTitle">Site Coverage</span><span class="requiredmark">*</span></strong>
            </div>
                <div class="col-sm-5">
                    <asp:ListBox ID="listCompanies" SelectionMode="Multiple" runat="server" CssClass="form-control" DataValueField="Code" DataTextField="Name" data-attribute="CompanyCodes" />
                </div>
        </div>
        <% }  %>
    </div>   
    <div class="tab-pane" id="emailSetting" role="tabpanel"> 
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="UseSSLTitle">Use SSL</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
               <asp:CheckBox ID="cbUseSsl" runat="server" />
            </div>  
        </div>             
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="SMTPHostAddress">SMTP Host Address</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbSmtpHostAddress"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
            </div>  
        </div>              
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="PortNumberTitle">Port Number</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-1">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbSmtpHostPortNumber"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control plainnumber" type="text" />
            </div>  
        </div>              
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="SMTPEmailAddressTitle">SMTP Email Address</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbSmtpEmailAddress"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
            </div>  
        </div>              
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="SMTPEmailPasswordTitle">SMTP Email Password</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbSmtpEmailPassword"
                name="name" ng-model="registerFormData.name"
                ng-minlength="3" min-length="3" runat="server"
                class="form-control" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <strong><span data-lang="MailSenderNameTitle">Mail Sender Name</span></strong><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-5">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbMailSender"
                name="name" ng-model="registerFormData.name" form-field="registerForm"
                ng-minlength="3" min-length="3" required="" runat="server"
                class="form-control" type="text" />
            </div>  
        </div> 
        <div class="row">
            <div class="col-sm-2">
                
            </div>
            <div class="col-sm-5">
                <a class="btn btn-default" onclick="showSendEmailForm(); return false;" ><i class="fa fa-envelope-o"></i><span data-lang="TestSMTPConnTitle">Test SMTP Connection</span></a>
            </div>  
        </div>                     
    </div>
    <div id="cssTheme" class="tab-pane" role="tabpanel">                      
        <div class="row">
            <div class="col-sm-12">
                <textarea id="tbCssTheme" class="form-control" style="height: 350px;" runat="server"></textarea>
            </div>  
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12" style="text-align: center; margin: 12px 0 12px 0">
        <button class="btn btn-primary"  runat="server" id="btnSave" onserverclick="btnSave_ServerClick" >
            Save
        </button>
        <button id="btnCancel" runat="server" class="btn btn-primary" onclick="backToList(); return false;">
            Cancel
        </button>
    </div>
</div>

<div class="modal fade" id="formSurveyQuestion">
    <div class="modal-dialog">
		<div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h1 class="modal-title center" data-lang="SurveyQuestionsSettingTitle">Survey Questions</h1>
		</div>
        <div class="modal-body" id="surveyQuestionsContainer">
            
        </div>
        </div>
    </div>
</div>


<div class="modal fade" id="formTestSMTP">
    <div class="modal-dialog">
		<div class="modal-content">
        <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h1 class="modal-title center" data-lang="TestEmailSMTPTitle">Test Email SMTP</h1>
		</div>
        <div class="modal-body">
            <div class="row">
                <div class="col-sm-3"><span data-lang="EmailAddressSMTPTitle">Email Address</span></div>
                <div class="col-sm-9"><input type="text" class="form-control" id="tbTestEmailAddress" /></div>
            </div>
            <div class="row">
                <div class="col-sm-3"></div>
                <div class="col-sm-9">
                    <a class="btn btn-default" onclick="sendEmail(); return false;" data-lang="SendEmailTitle">Send Email</a>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>


    <asp:ObjectDataSource ID="odsApplicantEmail" runat="server"
                          DataObjectTypeName="ERecruitment.Domain.Emails"
                          TypeName="ERecruitment.Domain.ERecruitmentManager"
                          SelectMethod="GetEmailListByCategory"
        >
     <SelectParameters>
         <asp:Parameter Name="category" DefaultValue="ApplicantNotification" />
     </SelectParameters>
 </asp:ObjectDataSource>
<asp:ObjectDataSource ID="odsIndustries" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Industries"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetIndustryList"
                      />
<asp:ObjectDataSource ID="odsThemes" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.ApplicationThemes"
                      TypeName="ERecruitment.Domain.ApplicationManager"
                      SelectMethod="GetApplicationThemeList"
                      />
</asp:Content>
