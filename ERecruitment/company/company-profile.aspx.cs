﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class company_profile : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                listSurveyQuestions.DataSource = SurveyQuestionManager.GetSurveyQuestionList();
                listSurveyQuestions.DataBind();

                listCompanies.DataSource = ERecruitmentManager.GetCompanyList();
                listCompanies.DataBind();

                if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
                    initiateForm();
                else if (Utils.GetQueryString<string>("mode") == "main")
                {
                    Response.Redirect("company-profile.aspx?code=" + MainCompany.Code + "&mode=site");
                }

                tbSmtpEmailPassword.Attributes["type"] = "Password";

                if (Utils.GetQueryString<string>("mode") == "site")
                    btnCancel.Visible = false;
            }
        }

        private void initiateForm()
        {
            Companies company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
            if (company != null)
            {
                ddIndustry.DataBind();

                tbBusinessUnitCode.Value = company.Code;
                tbBusinessUnitCode.Disabled = true;

                #region basic info

                tbCompanyName.Value = company.Name;
                tbCompanyAddress1.Value = company.AddressLine1;
                tbCompanyAddress2.Value = company.AddressLine2;
                tbCompanyAddress3.Value = company.AddressLine3;
                ddIndustry.Value = company.Industry;
                tbContactNumber.Value = company.ContactNumber;
                tbWebsite.Value = company.Website;
                tbATSSite.Value = company.ATSSite;
                tbCompanyFacebook.Value = company.FacebookUrl;
                tbCompanyLinkedIn.Value = company.LinkedINUrl;
                tbContactPerson.Value = company.ContactPerson;

                #endregion

                #region landing page

                tbCopyRightText.Value = company.CopyrightText;
                tbContactExtensionNumber.Value = company.ContactExtensionNumber;
                tbMobileNumber.Value = company.ContactMobileNumber;
                tbMobileContact.Value = company.ContactMobilePerson;

                if (company.CoverImage != null)
                    coverImage.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanySiteCoverImage&code=" + company.Code;
                
                if (company.CoverImage1 != null)
                    coverImage1.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanySiteCoverImage1&code=" + company.Code;

                if (company.CoverImage2 != null)
                    coverImage2.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanySiteCoverImage2&code=" + company.Code;

                if (company.CoverImage3 != null)
                    coverImage3.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanySiteCoverImage3&code=" + company.Code;

                if (company.CoverImage4 != null)
                    coverImage4.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanySiteCoverImage4&code=" + company.Code;

                if (company.AboutUsImage != null)
                    aboutUsImage.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanySiteAboutUsImage&code=" + company.Code;

                if(company.Logo !=null)
                    logoImage.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanyLogo&code=" + company.Code;

                if (company.FaviconLogo != null)
                    faviconImage.Src = "../../handlers/HandlerUI.ashx?commandName=GetCompanyFavicon&code=" + company.Code;

                if (!string.IsNullOrEmpty(company.CoverVideoUrl))
                    coverVideo.Src = "https://img.youtube.com/vi/" + company.CoverVideoUrl + "/0.jpg";

                if (!string.IsNullOrEmpty(company.CoverVideoUrl1))
                    coverVideo.Src = "https://img.youtube.com/vi/" + company.CoverVideoUrl1 + "/0.jpg";

                if (!string.IsNullOrEmpty(company.CoverVideoUrl2))
                    coverVideo.Src = "https://img.youtube.com/vi/" + company.CoverVideoUrl2 + "/0.jpg";

                if (!string.IsNullOrEmpty(company.CoverVideoUrl3))
                    coverVideo.Src = "https://img.youtube.com/vi/" + company.CoverVideoUrl3 + "/0.jpg";

                if (!string.IsNullOrEmpty(company.CoverVideoUrl4))
                    coverVideo.Src = "https://img.youtube.com/vi/" + company.CoverVideoUrl4 + "/0.jpg";

                tbMainYoutubeID.Value = company.CoverVideoUrl;
                tbYoutubeID1.Value = company.CoverVideoUrl1;
                tbYoutubeID2.Value = company.CoverVideoUrl2;
                tbYoutubeID3.Value = company.CoverVideoUrl3;
                tbYoutubeID4.Value = company.CoverVideoUrl4;

                cbUseSsl.Checked = company.EmailSmtpUseSSL;
                tbSmtpHostAddress.Value = company.EmailSmtpHostAddress;
                tbSmtpHostPortNumber.Value = company.EmailSmtpPortNumber;
                tbSmtpEmailAddress.Value = company.EmailSmtpEmailAddress;
                tbSmtpEmailPassword.Value = company.EmailSmtpEmailPassword;
                tbMailSender.Value = company.MailSender;

                if (!string.IsNullOrEmpty(company.SurveyQuestionCodes))
                {
                    string[] surveyQuestionCodes = company.SurveyQuestionCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < surveyQuestionCodes.Length; i++)
                    {
                        ListItem item = listSurveyQuestions.Items.FindByValue(surveyQuestionCodes[i]);
                        if (item != null)
                            item.Selected = true;
                    }
                }

                Utils.SelectItem<string>(ddSurveyDeliveryTerms, company.SurveyDeliveryTerms);
                Utils.SelectItem<int>(ddInvitationEmailTemplate, company.SurveyEmailTemplateId);

                PageTranslatableLabelContents pageLabelStoryTitle = PageManager.GetPageTranslatableContent("A.01", company.Code, "OurStoryTitle");
                PageTranslatableLabelContents pageLabelStorySubTitle = PageManager.GetPageTranslatableContent("A.01", company.Code, "OurStorySubTitle");
                PageTranslatableLabelContents pageLabelStory = PageManager.GetPageTranslatableContent("A.01", company.Code, "OurStoryContent");

                if (pageLabelStoryTitle != null)
                {
                    tbOurStoryTitleEn.Value = pageLabelStoryTitle.ENText;
                    tbOurStoryTitleId.Value = pageLabelStoryTitle.IDText;   
                }

                if (pageLabelStorySubTitle != null)
                {
                    tbOurStorySubTitleEn.Value = pageLabelStorySubTitle.ENText;
                    tbOurStorySubTitleId.Value = pageLabelStorySubTitle.IDText;   
                }

                if (pageLabelStory != null)
                {
                    tbOurStoryEn.Text = pageLabelStory.ENText;
                    tbOurStoryId.Text = pageLabelStory.IDText;   
                }

                #endregion

                #region application setting

                tbApplicationTitle.Text = company.ApplicationTitle;
                tbFacebookAPIKey.Text = company.FacebookAPIKey;
                tbLinkedInAPIKey.Text = company.LinkedINAPIKey;
                tbMailSender.Value = company.MailSender;

                #endregion

                // site coverage
                if (!string.IsNullOrEmpty(company.CompanyCodes))
                {
                    string[] companyCodes = company.CompanyCodes.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 0; i < companyCodes.Length; i++)
                    {
                        ListItem item = listCompanies.Items.FindByValue(companyCodes[i]);
                        if (item != null)
                            item.Selected = true;
                    }
                }

                tbCssTheme.Value = company.CssContent;
            }
        }

        protected void btnSave_ServerClick(object sender, EventArgs e)
        {
            Companies company = new Companies();
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
            {
                company = ERecruitmentManager.GetCompany(Utils.GetQueryString<string>("code"));
            }
            else
                company.IsNew = true;

            #region basic info

            company.Code = tbBusinessUnitCode.Value;
            company.Name = tbCompanyName.Value;
            company.AddressLine1 = tbCompanyAddress1.Value;
            company.AddressLine2 = tbCompanyAddress2.Value;
            company.AddressLine3 = tbCompanyAddress3.Value;
            company.Industry = ddIndustry.Value;
            company.ContactNumber = tbContactNumber.Value;
            company.Website = tbWebsite.Value;
            company.ATSSite = tbATSSite.Value;
            company.FacebookUrl = tbCompanyFacebook.Value;
            company.LinkedINUrl = tbCompanyLinkedIn.Value;
            company.ContactPerson = tbContactPerson.Value;

            #endregion

            #region landing page

            if (hidRemoveCoverPageImage.Value == "true")
            {
                company.CoverImage = null;
            }
            if (fileCoverPageImage.HasFile)
            {
                String type = fileCoverPageImage.PostedFile.ContentType;
                if ("image/jpg".Equals(type) || "image/gif".Equals(type) || "image/jpeg".Equals(type) || "image/png".Equals(type) || "image/bmp".Equals(type))
                {

                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : jpg, png, gif and bmp', 'error');", Page);
                    return;
                }
                int fileSize = fileCoverPageImage.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File cover image too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.CoverImage = Utils.GetFile(fileCoverPageImage);
            }

            if (hidRemoveCoverPageImage1.Value == "true")
            {
                company.CoverImage1 = null;
            }
            if (fileCoverPageImage1.HasFile)
            {
                String type = fileCoverPageImage1.PostedFile.ContentType;
                if ("image/jpg".Equals(type) || "image/gif".Equals(type) || "image/jpeg".Equals(type) || "image/png".Equals(type) || "image/bmp".Equals(type))
                {

                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : jpg, png, gif and bmp', 'error');", Page);
                    return;
                }

                int fileSize = fileCoverPageImage1.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File cover image 1 too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.CoverImage1 = Utils.GetFile(fileCoverPageImage1);
            }

            if (hidRemoveCoverPageImage2.Value == "true")
            {
                company.CoverImage2 = null;
            }
            if (fileCoverPageImage2.HasFile)
            {
                String type = fileCoverPageImage2.PostedFile.ContentType;
                if ("image/jpg".Equals(type) || "image/gif".Equals(type) || "image/jpeg".Equals(type) || "image/png".Equals(type) || "image/bmp".Equals(type))
                {

                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : jpg, png, gif and bmp', 'error');", Page);
                    return;
                }

                int fileSize = fileCoverPageImage2.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File cover image 2 too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.CoverImage2 = Utils.GetFile(fileCoverPageImage2);
            }

            if (hidRemoveCoverPageImage3.Value == "true")
            {
                company.CoverImage3 = null;
            }
            if (fileCoverPageImage3.HasFile)
            {
                String type = fileCoverPageImage3.PostedFile.ContentType;
                if ("image/jpg".Equals(type) || "image/gif".Equals(type) || "image/jpeg".Equals(type) || "image/png".Equals(type) || "image/bmp".Equals(type))
                {

                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : jpg, png, gif and bmp', 'error');", Page);
                    return;
                }

                int fileSize = fileCoverPageImage3.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File cover image 3 too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.CoverImage3 = Utils.GetFile(fileCoverPageImage3);
            }

            if (hidRemoveCoverPageImage4.Value == "true")
            {
                company.CoverImage4 = null;
            }
            if (fileCoverPageImage4.HasFile)
            {
                String type = fileCoverPageImage4.PostedFile.ContentType;
                if ("image/jpg".Equals(type) || "image/gif".Equals(type) || "image/jpeg".Equals(type) || "image/png".Equals(type) || "image/bmp".Equals(type))
                {

                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : jpg, png, gif and bmp', 'error');", Page);
                    return;
                }

                int fileSize = fileCoverPageImage4.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File cover image 4 too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.CoverImage4 = Utils.GetFile(fileCoverPageImage4);
            }


            company.CoverVideoUrl = tbMainYoutubeID.Value;
            company.CoverVideoUrl1 = tbYoutubeID1.Value;
            company.CoverVideoUrl2 = tbYoutubeID2.Value;
            company.CoverVideoUrl3 = tbYoutubeID3.Value;
            company.CoverVideoUrl4 = tbYoutubeID4.Value;


            if (hidRemoveAboutUsImage.Value == "true")
            {
                company.AboutUsImage = null;
            }
            if (fileAboutUs.HasFile)
            {
                String type = fileAboutUs.PostedFile.ContentType;
                if ("image/jpg".Equals(type) || "image/gif".Equals(type) || "image/jpeg".Equals(type) || "image/png".Equals(type) || "image/bmp".Equals(type))
                {

                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : jpg, png, gif and bmp', 'error');", Page);
                    return;
                }

                int fileSize = fileAboutUs.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File image about us too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.AboutUsImage = Utils.GetFile(fileAboutUs);
            }
            company.CopyrightText = tbCopyRightText.Value;
            company.ContactExtensionNumber = tbContactExtensionNumber.Value;
            company.ContactMobileNumber = tbMobileNumber.Value;
            company.ContactMobilePerson = tbMobileContact.Value;
            company.SurveyDeliveryTerms = Utils.GetSelectedValue<string>(ddSurveyDeliveryTerms);
            List<string> surveyQuestionCodes = new List<string>();
            foreach (ListItem item in listSurveyQuestions.Items)
                if (item.Selected)
                    surveyQuestionCodes.Add(item.Value);
            company.SurveyQuestionCodes = string.Join(",", surveyQuestionCodes.ToArray());
            
            if (String.IsNullOrEmpty(company.SurveyQuestionCodes))
            {
                JQueryHelper.InvokeJavascript("showNotification('Survey Question Codes cannot be empty', 'error');", Page);
                return;
            }
            company.SurveyEmailTemplateId = Utils.GetSelectedValue<int>(ddInvitationEmailTemplate);
            company.EmailSmtpUseSSL = cbUseSsl.Checked;
            company.EmailSmtpHostAddress = tbSmtpHostAddress.Value;
            company.EmailSmtpPortNumber = tbSmtpHostPortNumber.Value;
            company.EmailSmtpEmailAddress = tbSmtpEmailAddress.Value;
            if (!string.IsNullOrEmpty(tbSmtpEmailPassword.Value))
                company.EmailSmtpEmailPassword = tbSmtpEmailPassword.Value;
            company.MailSender = tbMailSender.Value;

            #endregion

            #region application setting

            company.ApplicationTitle = tbApplicationTitle.Text;
            if (fileLogo.HasFile)
            {
                String type = fileLogo.PostedFile.ContentType;
                if ("image/jpg".Equals(type) || "image/gif".Equals(type) || "image/jpeg".Equals(type) || "image/png".Equals(type) || "image/bmp".Equals(type))
                {

                }
                else
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : jpg, png, gif and bmp', 'error');", Page);
                    return;
                }

                int fileSize = fileLogo.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File logo image too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.Logo = Utils.GetFile(fileLogo);
            }

            if (fileFavicon.HasFile)
            {
                String type = fileFavicon.PostedFile.ContentType;
                if (!"image/x-icon".Equals(type))
                {
                    JQueryHelper.InvokeJavascript("showNotification('You have uploaded an invalid image file type, allowed file type : ico', 'error');", Page);
                    return;
                }

                int fileSize = fileFavicon.PostedFile.ContentLength;
                if (fileSize > 500000)
                {
                    JQueryHelper.InvokeJavascript("showNotification('File favicon too large, Maximal file size is 500KB', 'error');", Page);
                    return;
                }
                company.FaviconLogo = Utils.GetFile(fileFavicon);
            }

            if (CurrentUser.IsAdmin)
            {
                company.FacebookAPIKey = tbFacebookAPIKey.Text;
                company.LinkedINAPIKey = tbLinkedInAPIKey.Text;
                
                // site coverage
                List<string> siteCodeList = new List<string>();
                foreach (ListItem item in listCompanies.Items)
                    if (item.Selected)
                        siteCodeList.Add(item.Value);

                if (siteCodeList.Count == 0)
                {   
                    JQueryHelper.InvokeJavascript("showNotification('Site Coverage cannot be empty', 'error');", Page);
                    return;
                }
            
                company.CompanyCodes = string.Join(",", siteCodeList.ToArray());
            }
            
            company.MailSender = tbMailSender.Value;

            #endregion

            if (company.IsNew)
            {
                if (string.IsNullOrEmpty(company.Code))
                    company.Code = Guid.NewGuid().ToString().Substring(0, Guid.NewGuid().ToString().IndexOf("-"));
                company.InsertedBy = CurrentUser.Code;
                company.InsertStamp = DateTime.Now;
                company.Active = true;
            }

            company.UpdatedBy = CurrentUser.Code;
            company.UpdateStamp = DateTime.Now;
//            company.FacebookAPIKey = tbFacebookAPIKey.Text;
//            company.LinkedINAPIKey = tbLinkedInAPIKey.Text;
            company.CssContent = tbCssTheme.Value;
            company.ApplicationTitle = tbApplicationTitle.Text;

            company.ATSBaseUrl = "http://" + company.ATSSite + "/";

            if (company.IsNew)
            {
                ERecruitmentManager.SaveCompany(company);
            }
            else
            {
                ERecruitmentManager.UpdateCompany(company);   
            }   
                
            List<PageTranslatableLabelContents> labelList = new List<PageTranslatableLabelContents>();
            
            PageTranslatableLabelContents ourStoryTitle = PageManager.GetPageTranslatableContent(
                "A.01",
                company.Code,
                "OurStoryTitle");
            if (ourStoryTitle == null)
            {
                ourStoryTitle = new PageTranslatableLabelContents();
                ourStoryTitle.CompanyCode = company.Code;
                ourStoryTitle.PageCode = "A.01";
                ourStoryTitle.Label = "OurStoryTitle";   
                ourStoryTitle.IsNew = true;
            }
            ourStoryTitle.IDText = tbOurStoryTitleId.Value;
            ourStoryTitle.ENText = tbOurStoryTitleEn.Value;
            
            PageTranslatableLabelContents ourStorySubTitle = PageManager.GetPageTranslatableContent(
                "A.01",
                company.Code,
                "OurStorySubTitle");
            if (ourStorySubTitle == null)
            {
                ourStorySubTitle = new PageTranslatableLabelContents();
                ourStorySubTitle.CompanyCode = company.Code;
                ourStorySubTitle.PageCode = "A.01";
                ourStorySubTitle.Label = "OurStorySubTitle"; 
                ourStorySubTitle.IsNew = true;
            }
            ourStorySubTitle.IDText = tbOurStorySubTitleId.Value;
            ourStorySubTitle.ENText = tbOurStorySubTitleEn.Value;
            
            PageTranslatableLabelContents ourStory = PageManager.GetPageTranslatableContent(
                "A.01",
                company.Code,
                "OurStoryContent");
            if (ourStory == null)
            {
                ourStory = new PageTranslatableLabelContents();
                ourStory.CompanyCode = company.Code;
                ourStory.PageCode = "A.01";
                ourStory.Label = "OurStoryContent"; 
                ourStory.IsNew = true;
            }
            ourStory.IDText = tbOurStoryId.Text;
            ourStory.ENText = tbOurStoryEn.Text;
                
            labelList.Add(ourStoryTitle);
            labelList.Add(ourStorySubTitle);
            labelList.Add(ourStory);
                
            PageManager.UpdateTranslatableContentList(labelList);

            if (Utils.GetQueryString<string>("mode") == "site")
            {
                JQueryHelper.InvokeJavascript("showNotification('Successfully updated');", Page);
                Session["maincompany"] = company;
            }
            else
                JQueryHelper.InvokeJavascript("showNotification('Successfully updated'); window.location.href = 'company-profile.aspx?code=" + company.Code + "'", Page);


        }
    }
}