﻿<%@ Page Title="Company List" Language="C#" MasterPageFile="~/masters/recruiter.Master" AutoEventWireup="true" CodeBehind="company-list.aspx.cs" Inherits="ERecruitment.company_list" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
   
<script type="text/javascript">

    $(document).ready(function () {

        loadData();
    });

    function showForm(code) {

        var urlPage = $("#<%= hidUrl.ClientID %>").val();
        urlPage = urlPage + "?code=" + code;
        window.location.href = urlPage;
    }

    function Add() {

        window.location.href = "company-profile.aspx";
        
    }

    function refresh() {
        var tableData = $('#tableData').dataTable();
        tableData.fnDraw();
    }

    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0] },
                   { "sClass": "controlIcon", "aTargets": [0] },
                   { "bVisible": false, "aTargets": [4] }

            ],
            "oLanguage":
                            { "sSearch": "Search By Name" },
            "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetCompanyList"
        });
    }

    function Remove() {

        var idList = "";
        var tableData = $('#tableData').dataTable();
        $('input:checked', tableData.fnGetNodes()).each(function () {

            idList += (tableData.fnGetData($(this).closest('tr')[0])[4] + ',');
        });
        if (idList == "")
            showNotification("please select company");
        else {

            if (confirm("Are you sure want to delete ? ")) {
                $.ajax({
                    url: "../handlers/HandlerSettings.ashx?commandName=RemoveCompanys&codes=" + idList,
                    async: true,
                    beforeSend: function () {

                    },
                    success: function (queryResult) {
                        showNotification("company removed");
                        refresh();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        showNotification(xhr.responseText);
                        refresh();
                    }
                });
            }
        }

    }

    </script>
  
    <div>
      

    <h3>Company List</h3>
     <asp:HiddenField ID="hidUrl" runat="server" />
       <div>
        <div class="row">
            <div class="col-sm-12">
                <div class="col-sm-12">
                    <button class="btn btn-primary" onclick="Add(); return false;" id="btnNew">
                                        Create New
                    </button>
                   
                </div>
            </div>
        </div>

         <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
        <tr>
            <th></th>
            <th><strong><span data-lang="CompanyNameSetupTitle">Company Name</span></strong></th>
            <th><strong><span data-lang="ContactPersonSetupTitle">Contact Person</span></strong></th>
            <th><strong><span data-lang="IndustrySetupTitle">Industry</span></strong></th>
            <th></th>
        </tr>
       
        </thead>
        <tbody>
           
        </tbody>
            </table>
          
        </div>
      </div>
  
</asp:Content>
