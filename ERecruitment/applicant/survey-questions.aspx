﻿<%@ Page Title="Candidate Survey" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="survey-questions.aspx.cs" Inherits="ERecruitment.survey_questions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <script type="text/javascript">
        var type = "";
        $(document).ready(function () {

            showSurveyQuestion();

        });

        function showSurveyQuestion() {
            var dataId = $('#' + '<%= hidSurveyIds.ClientID %>').val();

            var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetSurveyQuestionList&id=" + dataId;
            var listContainer = $("#surveyQuestionsContainer");

            $.ajax({
                url: handlerUrl,
                async: true,
                beforeSend: function () {
                    $(".surveyQuestionsContainer").remove();
                },
                success: function (queryResult) {

                    var dataModel = $.parseJSON(queryResult);
                    $.each(dataModel, function (i, item) {

                        // survey question wrapper
                        var questionWrapper = $(document.createElement('div')).attr("class", "row jobSection surveyQuestionItem");
                        // question section
                        var questionSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('p')).attr("class", "text-muted").text(item["Question"]));
                        questionWrapper.append(questionSection);
                        type = item["Type"];
                        if (item["Type"] == "FiveStarPolls") {
                            // answer wrapper
                            var answerSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12");
                            var starSelector = $(document.createElement('input')).attr("id", "input-1").attr("type", "number").attr("class", "rating").attr("min", "1").attr("max", "5").attr("step", "1").attr("size", "md").attr("name", item["Code"]).attr("data-attribute", item["Code"]);
                            var hiddenSelector = $(document.createElement('input')).attr("id", "hidden-1").attr("type", "hidden").attr("name", item["Code"]).attr("data-attribute", item["Code"]).attr("value", "");

                            answerSection.append(starSelector);

                            var notesLabel = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('p')).attr("class", "text-muted").text("Komentar Anda : "));
                            var notesAnswer = $(document.createElement('div')).attr("class", "col-md-6 col-sm-6").append($(document.createElement('textarea')).attr("id", "notes").attr("class", "form-control").attr("cols", 40).attr("rows", 4).attr("disallowed-chars", "[^a-zA-Zs ]+").attr("data-attribute", "Notes"));

                            questionWrapper.append(answerSection);
                            questionWrapper.append(hiddenSelector);
                            questionWrapper.append(notesLabel);
                            questionWrapper.append(notesAnswer);

                        }

                        if (item["Type"] == "Essay") {
                            // answer wrapper
                            var answerSection = $(document.createElement('div')).attr("class", "col-md-12 col-sm-12").append($(document.createElement('input')).attr("type", "text").attr("id", "essay").attr("class", "form-control").attr("data-attribute", item["Code"]));
                            questionWrapper.append(answerSection);
                        }

                        listContainer.append(questionWrapper);

                        $('#input-1').rating('create');
                        $('#input-1').on('rating.change', function (event, value, caption) {
                            $('#hidden-1').val(value);
                        });
                        $('#input-1').on('rating.clear', function (event) {
                            $('#hidden-1').val("");
                        });

                    });

                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });

            $('#formDataFrameModel').modal('show');
        }

        function saveAnswer() {

            var form = "surveyQuestionsContainer";
            var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateSurveyQuestion&applicantCode=" + $('#' + '<%= hidCurrentApplicantCode.ClientID %>').val() + "&companyCode=" + $('#' + '<%= hidCompanyCode.ClientID %>').val();
            var formData = {};
            if (!getNodeData(form, formData)) return false;

            if (type = "FiveStarPolls") {
                if ($("#hidden-1").val() == "" || $("#notes").val().length < 6) {
                    if ($("#notes").val().length < 6) {
                        showNotification("Please fill column 'Komentar' and minimum 6 character! ");
                    }
                    else {
                        showNotification("Please give rate for the question! ");
                    }
                } 
                else {
                    if (!getNodeData(form, formData)) return false;
                    $.post(actionUrl, formData).done(function (data) {
                        showNotification("Thank you for your participation");
                        window.location.href = "applicant-profile.aspx";
                    }).fail(function (response) {
                        showNotification(response.responseText);
                    });
                }
            }
            else {
                if ($("#essay").val().length < 1) {
                    showNotification("Please fill blank column!");
                }
                else {
                    if (!getNodeData(form, formData)) return false;
                    $.post(actionUrl, formData).done(function (data) {
                        showNotification("Thank you for your participation");
                        window.location.href = "applicant-profile.aspx";

                    });
                }
            }

        }

    </script>

    <asp:HiddenField ID="hidSurveyIds" runat="server" />
    <asp:HiddenField ID="hidCompanyCode" runat="server" />
    <asp:HiddenField ID="hidCurrentApplicantCode" runat="server" />
    <asp:HiddenField ID="hidVacancyCode" runat="server" />

    <div id="surveyQuestionsContainer">
    </div>

    <div class="row jobSection">
        <div class="col-sm-22">
            <button class="btn btn-primary" onclick="saveAnswer(); return false;">
                Save
            </button>
        </div>
    </div>
</asp:Content>
