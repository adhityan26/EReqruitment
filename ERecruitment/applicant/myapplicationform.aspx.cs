﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class myapplicationform : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                odsJobList.SelectParameters["applicantCode"].DefaultValue = CurrentUser.ApplicantCode;
            }
        }

        protected void repJobList_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.DataItem != null)
            {
                ApplicantVacancies vacancy = e.Item.DataItem as ApplicantVacancies;
                HtmlImage imgLogo = e.Item.FindControl("imgLogo") as HtmlImage;
                imgLogo.Src = "../handlers/HandlerUI.ashx?commandName=GetCompanyLogo&code=" + vacancy.CompanyCode;
            }
        }
    }
}