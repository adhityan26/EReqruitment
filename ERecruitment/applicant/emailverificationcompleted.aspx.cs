﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class emailverificationcompleted : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            #region notifAppMode
            ApplicationConfiguration appConfig1 = new ApplicationConfiguration();
            appConfig1 = ERecruitmentManager.GetApplicationConfiguration(2);
            litInformationSingle.Text = appConfig1.ConfigValue;

            ApplicationConfiguration appConfig2 = new ApplicationConfiguration();
            appConfig2 = ERecruitmentManager.GetApplicationConfiguration(3);
            litInformationMulti.Text = appConfig2.ConfigValue;
            #endregion

            Applicants applicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("code"));
            if (applicant == null)
                Response.Redirect("../default.aspx");
            if (applicant.Verified)
            {
                UserAccounts loginUser = AuthenticationManager.GetApplicantUserAccount(applicant.Code);
                Session["currentuser"] = loginUser;
                Response.Redirect("applicant-profile.aspx");
            }
            else
            {
                applicant.Verified = true;
                applicant.ExpiredDate = null;
                ApplicantManager.UpdateApplicant(applicant);
                UserAccounts loginUser = AuthenticationManager.GetApplicantUserAccount(applicant.Code);
                Session["currentuser"] = loginUser;
                JQueryHelper.InvokeJavascript("showNotif();", Page);
                //Response.Redirect("applicant-profile.aspx");
            }
        }

        protected void btnSubmit_ServerClick(object sender, EventArgs e)
        {
            Response.Redirect("applicant-profile.aspx");
        }
    }
}