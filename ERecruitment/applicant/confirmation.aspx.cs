﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;
using ERecruitment.Domain.Helper;

namespace ERecruitment
{
    public partial class confirmation : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string type = Utils.GetQueryString<string>("type");
                string landingPage = "/applicant/myapplicationform.aspx";
                UserAccounts loginUser = new UserAccounts();
                if (!string.IsNullOrEmpty(type))
                {
                    switch (type)
                    {
                        case "schedulling":
                            string param = Encriptor.Decrypt(Utils.GetQueryString<string>("_x").Replace(' ', '+'));
                            string[] paramValue = param.Split('&');
                            int idAppointment = 0;
                            string response = "";

                            foreach (string val in paramValue)
                            {
                                string[] keyPair = val.Split('=');
                                if (keyPair[0] == "id")
                                {
                                    idAppointment = int.Parse(keyPair[1]);
                                }
                                else if (keyPair[0] == "response")
                                {
                                    response = keyPair[1];
                                } 
                            }
                            
                            ApplicantVacancySchedules getSchedule = HiringManager.GetSchedule(idAppointment);
                            if (getSchedule != null &&
                                (getSchedule.ApplicantStatusNotification == null || getSchedule.ApplicantStatusNotification == "EmailSent"))
                            {
//                                string response = Utils.GetQueryString<string>("response");

                                getSchedule.ApplicantStatusNotification = response;
                                HiringManager.UpdateSchedule(getSchedule);
                                loginUser = AuthenticationManager.GetApplicantUserAccount(getSchedule.ApplicantCode);
                                HttpContext.Current.Session["currentuser"] = loginUser;
                                spanPage.HRef = landingPage;

                                ERecruitmentAppSettings.SendInvitationFeedbackEmail(getSchedule);
                            }
                            else
                            {
                                Response.Redirect("~/default.aspx?errorMessage=Schedule is already responded");   
                            }

                            break;
                        case "offering":
                            ApplicantVacancyOffering getOffering = HiringManager.GetOffering(Utils.GetQueryString<int>("id"));
                            if (getOffering != null)
                            {
                                getOffering.ApplicantStatusNotification = Utils.GetQueryString<string>("response");
                                HiringManager.UpdateOffering(getOffering);
                                loginUser = AuthenticationManager.GetApplicantUserAccount(getOffering.ApplicantCode);
                                HttpContext.Current.Session["currentuser"] = loginUser;
                                spanPage.HRef = landingPage;
                            }
                            break;
                    }
                }
                else
                    Response.Redirect("~/default.aspx");

            }
        }
    }
}