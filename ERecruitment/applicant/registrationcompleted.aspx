﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="registrationcompleted.aspx.cs" Inherits="ERecruitment.registrationcompleted" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    
<div style="float:left;margin:5px; margin-top:14px; text-align:center; padding:15px; width:100%; height:100%;">
    <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px;">
        Registration completed!

    </h1>
    <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px;">
        Before you can start using this application, you must verify your email address.
    </h1>
    
    <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px;">
        Click <a href="resendemailverification.aspx">Here</a> 
        to resend email verification if you haven't receive the email.
    </h1>
</div>
</asp:Content>
