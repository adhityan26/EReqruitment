﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using System.Web.UI.HtmlControls;
using SS.Web.UI;


namespace ERecruitment
{
    public partial class update_profile : AuthorizedPage
    {
        #region init
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                fillForm();
            }
        }
        #endregion

        private void fillForm()
        {
            string applicantCode = CurrentUser.Code;
            Applicants getApplicant = ApplicantManager.GetApplicant(applicantCode);
            tbFirstName.Value = getApplicant.FirstName;
            tbLastName.Value = getApplicant.LastName;
            tbPhoneNumber.Value = getApplicant.Phone;
            tbAlternativePhoneNumber.Value = getApplicant.AlternativePhoneNumber;
            tbEmailAddress.Value = getApplicant.Email;
            tbPlaceOfBirth.Value = getApplicant.BirthPlace;
            tbIDCardNumber.Value = getApplicant.IDCardNumber;
            ddNationality.DataBind();
            Utils.SelectItem(ddNationality, getApplicant.Nationality);
            Utils.SelectItem(ddGender, getApplicant.GenderSpecification);
            Utils.SelectItem(ddMaritalStatus, getApplicant.MaritalStatusSpecification);
            Utils.SelectItem(ddReligion, getApplicant.Religion);
            Utils.SelectItem(ddNumberOfChildren, getApplicant.NumberOfChildren);
            if (getApplicant.Birthday.HasValue)
                tbBirthDate.Value = Utils.FormatDateNumeric(getApplicant.Birthday.Value);
        }

        private bool _isValidDate = true;
        private string _msg = string.Empty;

        #region support

        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            Utils.InsertInitialListItem(dd);
        }

        private void Update(string code)
        {
            string applicantCode = Utils.GetQueryString<string>("code");
            Applicants getApplicant = ApplicantManager.GetApplicant(applicantCode);

            getApplicant.FirstName = tbFirstName.Value;
            getApplicant.LastName = tbLastName.Value;
            getApplicant.Phone = tbPhoneNumber.Value;
            getApplicant.AlternativePhoneNumber = tbAlternativePhoneNumber.Value;
            getApplicant.Email = tbEmailAddress.Value;
            getApplicant.BirthPlace = tbPlaceOfBirth.Value;
            getApplicant.Nationality = Utils.GetSelectedValue<string>(ddNationality);
            getApplicant.NumberOfChildren = Utils.GetSelectedValue<string>(ddNumberOfChildren);
            getApplicant.MaritalStatusSpecification = Utils.GetSelectedValue<string>(ddMaritalStatus);
            getApplicant.Religion = Utils.GetSelectedValue<string>(ddReligion);
            getApplicant.GenderSpecification = Utils.GetSelectedValue<string>(ddGender);
            getApplicant.Religion = Utils.GetSelectedValue<string>(ddReligion);
            getApplicant.IDCardNumber = tbIDCardNumber.Value;
            if (!string.IsNullOrEmpty(tbBirthDate.Value))
                getApplicant.Birthday = Utils.ConvertFormatDateNumericToDateTime(tbBirthDate.Value);
            ApplicantManager.UpdateApplicant(getApplicant);
        }

        #endregion

        #region button
        protected void nextbutton_ServerClick(object sender, EventArgs e)
        {
            bool validIDCard = true;
            if (validIDCard)
            {
                Applicants checkIDCardNumber = ApplicantManager.GetApplicantByIDCardNumber(tbIDCardNumber.Value);
                if (checkIDCardNumber != null && checkIDCardNumber.Code != Utils.GetQueryString<string>("code"))
                    JQueryHelper.InvokeJavascript("alert('ID Card already registered');", Page);
                else
                {
                    Update(Utils.GetQueryString<string>("code"));
                    Response.Redirect("contactInfoProfile.aspx?code=" + Utils.GetQueryString<string>("code"));
                }
            }
            else
            {
                JQueryHelper.InvokeJavascript("alert('" + _msg + "')", Page);
                return;
            }
        }
        #endregion

        #region ods
        protected void odsProfile_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Applicants applicant = e.ReturnValue as Applicants;

            }
        }
        #endregion
    }
}
