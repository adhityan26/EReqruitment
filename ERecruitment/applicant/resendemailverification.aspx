﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="resendemailverification.aspx.cs" Inherits="ERecruitment.resendemailverification" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
    
     <form defaultbutton="btnResend">
<div style="float:left;margin:5px; margin-top:14px; text-align:center; padding:15px; width:100%; height:100%;">
    <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px;">
        Enter your email address and we'll resend email verification
    </h1>
    <div class="form-group col-md-6 col-md-offset-3">
        <input class="form-control" type="text" name="email" placeholder="Email Address (format: mail@example.com)"
             pattern="[A-Za-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" 
            id="tbEmail" required="required"
            runat="server"  />
    </div>
    <div class="col-md-6 col-md-offset-3">
        <button class="btn btn-primary col-xs-12" id="btnResend" type="submit"
            runat="server" onserverclick="btnResend_ServerClick">
            Send Link
        </button>
    </div>
</div>

</asp:Content>
