﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/applicant-profile.Master" AutoEventWireup="true" CodeBehind="update-profile.aspx.cs" Inherits="ERecruitment.update_profile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

<div>
       <div class="row">
      
        <div class="row">
        <div class="col-sm-3" >
            <label runat="server" id="lbUpdateProfile_FirstName">First Name</label><span class="requiredmark">*</span>
        </div>
        <div class="col-xs-12 col-sm-3 space10">
            <input disallowed-chars="[^a-zA-Zs ]+" id="tbFirstName"
                    name="name" ng-model="registerFormData.name" form-field="registerForm"
                    ng-minlength="3" min-length="3" required="" runat="server"
                    class="form-control" type="text"
                    value='<%# Bind("FirstName") %>'
                    />
                                        
        </div>
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_LastName">Last Name</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-3 space10">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbLastName"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required=""
                        class="form-control" type="text"/>
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label style="text-align:left;" runat="server" id="lbUpdateProfile_Email">Email Address</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-3 space10">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbEmailAddress"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required=""
                        class="form-control" type="text" />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_PhoneNumber">Phone Number</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-3 space10">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbPhoneNumber"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        value='<%# Bind("Phone") %>'
                        class="form-control" type="text">
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_AlternativePhone">Alternative Phone Number</label>
            </div>
            <div class="col-xs-12 col-sm-3 space10">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbAlternativePhoneNumber"
                        name="name" ng-model="registerFormData.name"
                        ng-minlength="3" min-length="3" runat="server"
                        value='<%# Bind("AlternativePhoneNumber") %>'
                        class="form-control" type="text">
                                        
            </div>  
        </div>
        <div class="row">
           <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_DOB">Date of Birth</label>
            </div>
            <div class="col-sm-3">
                <div class="controls">
				<div class="input-group date col-sm-9">
					<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
					<input type="text" data-date-format="mm/dd/yyyy" required="" runat="server"
                           id="tbBirthDate" 
                         class="form-control date-picker" />
				</div>
                </div>              
            </div>
            
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_POB">Place of Birth</label>
            </div>
            <div class="col-xs-12 col-sm-3 space10">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbPlaceOfBirth"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3"  runat="server"
                        value='<%# Bind("BirthPlace") %>'
                        class="form-control" type="text">
                                        
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_Gender">Gender</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-2 space10">
                <asp:DropDownList id="ddGender" runat="server" required="required" class="form-control">
                    <asp:ListItem Text="Select an Item" Value="" />					
                    <asp:ListItem>Male</asp:ListItem>
					<asp:ListItem>Female</asp:ListItem>
				</asp:DropDownList>         
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_Religion">Religion</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-2 space10">
                <asp:DropDownList id="ddReligion" runat="server" class="form-control">
                    <asp:ListItem Text="Select an Item" Value="" />
                    <asp:ListItem>Islam</asp:ListItem>
					<asp:ListItem>Christian</asp:ListItem>
					<asp:ListItem>Catholic</asp:ListItem>
					<asp:ListItem>Hindhu</asp:ListItem>
					<asp:ListItem>Buddha</asp:ListItem>
				</asp:DropDownList>         
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_Nationality">Nationality</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-2 space10">
               <asp:DropDownList ID="ddNationality" OnDataBound="ddDatabound" runat="server" DataValueField="Code"
                                 CssClass="form-control" DataTextField="Nationality"
                                 DataSourceID="odsNationality" />                
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_IDNumber">ID Card Number</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-3 space10">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbIDCardNumber"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                       
                        class="form-control" type="text">
                                        
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_MaritalStatus">Marital Status</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-2 space10">
                <asp:DropDownList id="ddMaritalStatus" runat="server" class="form-control">
                    <asp:ListItem Text="Select an Item" Value="" />
					<asp:ListItem>Single</asp:ListItem>
					<asp:ListItem>Married</asp:ListItem>
                    <asp:ListItem>Divorce</asp:ListItem>
				</asp:DropDownList>         
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-3" >
                <label runat="server" id="lbUpdateProfile_NumberOfChildren">Number of Children</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-2 space10">
                <asp:DropDownList id="ddNumberOfChildren" runat="server" class="form-control">
					<asp:ListItem>0</asp:ListItem>
					<asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>3</asp:ListItem>
                    <asp:ListItem>4</asp:ListItem>
                    <asp:ListItem>More than 4</asp:ListItem>

				</asp:DropDownList>         
            </div>   
        </div>
        <div class="row">
            <div class="col-sm-10">
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary" runat="server" onserverclick="nextbutton_ServerClick" id="nextbutton">
                    Next &nbsp;
                    <i class="fa fa-arrow-right"></i>
                </button>         
            </div>   
        </div>
        
    </div>
</div>
   
    <asp:ObjectDataSource ID="odsMonth" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Month"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetMonthList"
    />
   
    <asp:ObjectDataSource ID="odsYear" runat="server"
                           DataObjectTypeName="ERecruitment.Domain.Years"
                           TypeName="ERecruitment.Domain.ERecruitmentManager"
                           SelectMethod="GetYearList"
        />
  <asp:ObjectDataSource ID="odsCountry" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Country"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCountryList"
    />
    <asp:ObjectDataSource ID="odsNationality" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Country"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetNationalityList"
    />
    
  
      
<asp:ObjectDataSource ID="odsProfile"
                      runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Applicants"
                      TypeName="ERecruitment.Domain.AuthenticationManager"
                      SelectMethod="GetApplicant"
                      OnUpdated="odsProfile_Updated"
                      UpdateMethod="UpdateApplicant"
                >
      <SelectParameters>
          <asp:QueryStringParameter Name="code" QueryStringField="code" />
      </SelectParameters>
</asp:ObjectDataSource>
    
</asp:Content>
