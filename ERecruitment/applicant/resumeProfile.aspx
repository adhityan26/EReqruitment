﻿<%@ Page Title="Upload Files" Language="C#" MasterPageFile="~/masters/applicant-profile.Master" AutoEventWireup="true" CodeBehind="resumeProfile.aspx.cs" Inherits="ERecruitment.resumeProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">
   
    
    function CheckAttachment() {
        var isProcess = true;

        var invalidPhoto = CheckPhoto();
        var invalidCV = CheckCV();
        var invalidOther = CheckOther();
 
        if (invalidPhoto == false || invalidCV == false || invalidOther==false) {
            <%= hidAttachment.ClientID %>.value = "invalid";
            return false;
        }
        if (invalidPhoto == "empty" && invalidCV =="empty" && invalidOther == "empty")
        {
            
            return false;
        }
        else {
            <%= hidAttachment.ClientID %>.value = "process";
            __doPostBack('nextbutton','nextbutton');   
        }
      
    }

    function CheckPhoto() {

        var photo = document.getElementById('<%= fuPhoto.ClientID %>');

        if (photo.value != "") {

            var photoFileName = photo.value;
            var photoFileSize = photo.files[0].size;
            var ext = photoFileName.substr(photoFileName.lastIndexOf('.') + 1).toLowerCase();

            if (!(ext == "jpeg" || ext == "jpg" || ext == "png")) {
                showNotification("Invalid photo file, must select a *.jpeg, *.jpg, or *.png file.");
                return false;
            }
            else if (photoFileSize > 2097512) {
                showNotification("photo is too large, must select file under 2  Mb");
                return false;
            }
            else
                return true;
        }
        else
            return "empty";
    }

    function CheckCV() {

        var cv = document.getElementById('<%= fuCV.ClientID %>');
      
        if (cv.value != "") {

            var cvFileName = cv.value;
            var cvFileSize = cv.files[0].size;
            var ext = cvFileName.substr(cvFileName.lastIndexOf('.') + 1).toLowerCase();

            if (!(ext == "doc" || ext == "docx" || ext == "pdf")) {
                showNotification("Invalid cv file, must select a *.doc, *.docx, or *.pdf file.");
                return false;
            }

            else if (cvFileSize > 2097512) {
                showNotification("cv is too large, must select file under 2  Mb");
                return false;
            }
            else
                return true;
        }
        else
            return "empty";
    }

    function CheckOther()
    {
        var other = document.getElementById('<%= fuOther.ClientID %>');
      
        if (other.value != "") {

            var otherFileName = other.value;
            var otherFileSize = other.files[0].size;
            var ext = otherFileName.substr(otherFileName.lastIndexOf('.') + 1).toLowerCase();

            if (otherFileSize > 2097512) {
                showNotification("other is too large, must select file under 2  Mb");
                return false;
            }
            else
                return true;
        }
        else
            return "empty";
    
    }

</script>

<script type="text/javascript">

    $(document).ready(function () {

        loadData();
        loadOtherAttachment()
    });


    $('body').on('click', '.attachment_delete', function () {

        var keys = $(this).attr('name').split("_");

        var id = keys[0];
        var param = "&id=" + id;
        $.ajax({
            url: "../handlers/HandlerApplicants.ashx?commandName=RemoveOtherAttachment" + param,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {
                // refresh
                var tableData = $('#tableOtherAttachment').dataTable();
                tableData.fnDraw();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                showNotification(xhr.responseText);
                // refresh
                var tableData = $('#tableOtherAttachment').dataTable();
                tableData.fnDraw();
            }
        });
    });


    $('body').on('click', '.delete', function () {

        var applicantCode = GetQueryStringParams("code");
        var param = "&applicantCode=" + applicantCode;
        $.ajax({
            url: "../handlers/HandlerApplicants.ashx?commandName=RemoveCV" + param,
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {
                // refresh
                var tableData = $('#tableData').dataTable();
                tableData.fnDraw();
            },
            error: function (xhr, ajaxOptions, thrownError) {
                showNotification(xhr.responseText);
                // refresh
                var tableData = $('#tableData').dataTable();
                tableData.fnDraw();
            }
        });
    });

    function loadOtherAttachment() {
        var ex = document.getElementById('tableOtherAttachment');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableOtherAttachment").dataTable().fnDestroy();
        }

        var applicantCode = GetQueryStringParams("code");
        var param = "&applicantCode=" + applicantCode;
        var OTableData = $('#tableOtherAttachment').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 2] },
                     { "sClass": "controlIcon", "aTargets": [0] },

            ],
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetOtherAttachments" + param

        });
    }

    function loadData() {
        var ex = document.getElementById('tableData');
        if ($.fn.DataTable.fnIsDataTable(ex)) {
            // data table, then destroy first
            $("#tableData").dataTable().fnDestroy();
        }

        var applicantCode = GetQueryStringParams("code");
        var param = "&applicantCode=" + applicantCode;
        var OTableData = $('#tableData').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "iDisplayLength": 10,
            "bJQueryUI": true,
            "bAutoWidth": false,
            "sDom": "ftipr",
            "bDeferRender": true,
            "aoColumnDefs": [
                   { "bSortable": false, "aTargets": [0, 2] },
                     { "sClass": "controlIcon", "aTargets": [0] },

            ],
            "sAjaxSource": "../datatable/HandlerDataTableApplicant.ashx?commandName=GetCurriculumVitaes" + param

        });
    }

</script>

    <asp:HiddenField ID="hidAttachment" runat="server" />
<div>
    <div class="row">
        <div class="col-sm-4">
            <img id="imgfoto" width="150" height="150" runat="server" src="" alt="Photo"  />
            </div>
        </div>
    <div class="row">
        <div class="col-sm-4">
            <label runat="server" id="lbResumeProfile_UploadPhoto">Upload Photo ( jpg, jpeg or png )</label>
        </div>
        <div class="col-sm-4">
            <asp:FileUpload ID="fuPhoto" runat="server" />   
        </div> 
         
    </div>
    <div class="row">
        <div class="col-sm-4">
            <label runat="server" id="lbResumeProfile_UploadCV">Upload CV (docx, doc or pdf)</label>
        </div>
        <div class="col-sm-4">
          <asp:FileUpload ID="fuCV" runat="server" />
        </div>
       
    </div>
   
    <div class="row">
        <div class="col-sm-4">
            <label runat="server" id="lbResumeProfile_UploadOthers">Upload Others</label>
        </div>
        <div class="col-sm-4">
          <asp:FileUpload ID="fuOther" runat="server" />
        </div>
       
      </div>
    
    
    <div class="row">
        <div class="col-sm-4">
            <label runat="server" id="lbResumeProfile_Legend">Each uploaded document must be less than 2 MB</label>
        </div>
    </div>
         
    <div class="row">
        <h1>CV</h1>
    <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
        <thead>
        <tr>
            <th></th>
            <th>File Name</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        </tbody>
    </table>
   </div> 
     
    <div class="row">
        <h1>Other Attachment</h1>
     <table class="table table-striped table-bordered dt-responsive nowrap" id="tableOtherAttachment">
        <thead>
        <tr>
            <th></th>
            <th>File Name</th>
            <th>Action</th>
        </tr>
        </thead>

        <tbody>
        </tbody>
    </table>
    </div>
    
    <div class="row">
        <div class="col-sm-10">
        </div>
        <div class="col-sm-2">
            <button class="btn btn-primary" runat="server" onclick="CheckAttachment();return false;" id="nextbutton">
                Save &nbsp;
                    
            </button>         
        </div>   
    </div>

</div>

</asp:Content>
