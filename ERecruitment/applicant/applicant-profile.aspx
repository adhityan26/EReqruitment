﻿<%@ Page Title="My Profile" Language="C#" MasterPageFile="~/masters/applicant.Master" AutoEventWireup="true" CodeBehind="applicant-profile.aspx.cs" Inherits="ERecruitment.applicant_profile1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">


    <script type="text/javascript">
        var EducationLevels;

        function getParameterByName(name, url) {
            if (!url) url = window.location.href;
            name = name.replace(/[\[\]]/g, "\\$&");
            var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, " "));
        }

        window.onload = function () {
            //var isRegis = $("#spIsRegis").text();
            var isRegis = getParameterByName('isRegis'); // "lorem"
            if (isRegis == "true") {
                $('#updatePasswordFrameModel').modal({ backdrop: 'static', keyboard: false })
                $('#updatePasswordFrameModel').modal('show');
            }
        }

        $(document).ready(function () {
            $(".glyphicon-exclamation-sign").tooltip();
            $("#rowmajor").show();
            loadDataInstitution(0);
            loadPersonalInformation();
            loadEducationInfoList();
            loadExperienceInfoList();
            loadSkillInfoList();
            loadHobbyInterestInfoList();
            loadAwardInfoList();
            loadTrainingInfoList();
            loadAttachmentList();

            $('#personalInfoFrameModel').on('show', function () {
                $.fn.modal.Constructor.prototype.enforceFocus = function () { };
            });
            //$('#ddMajor').html('<option selected="selected" value="0">Select an option</option>');

            $('.month-picker').datepicker({
                format: "mm/yyyy",
                startView: "months",
                minViewMode: "months",
                autocose: true
            });
        });

        // handler untuk edit, menggunakan selector clas vw yang di define saat datatable initiation
        $('body').on('click', '.choose', function () {

            var institution = $(this).attr('name');
            var inst = institution.split("~");
            $("#tbInstitutionCode").val(inst[0]);
            $("#tbInstitutionName").val(inst[1]);
            $("#InstitutionDataFrameModel").modal('hide');


        });

        function toggleEducationCity() {
            var selectedCountry = $("#<%= ddEducationCountry.ClientID %> option:selected").text();
            if (selectedCountry == "Indonesia") {
                $("#<%= ddEducationCity.ClientID %>_chosen").show();
                $("#tbEducationCity").hide();
            }
            else {
                $("#<%= ddEducationCity.ClientID %>_chosen").hide();
                $("#tbEducationCity").show();
                $("#tbEducationCity").val("");
            }

        }

        function updateTextBoxCity() {
            $("#tbEducationCity").val($("#<%= ddEducationCity.ClientID %> option:selected").text());
        }

        function toggleInstitution() {
            var selectedLevel = $("#<%= ddEducationLevel.ClientID %> option:selected").text();
            var kategori;
            if (selectedLevel == "SMA" || selectedLevel == "SMK") {
                if (selectedLevel == "SMA")
                    kategori = 2;
                else if (selectedLevel == "SMK")
                    kategori = 3

                rebindingFieldOfStudy(kategori, "x");
                loadDataInstitution(kategori);
              
                $("#rowmajor").hide();
            }
            else {
                rebindingFieldOfStudy(1, "x");
                loadDataInstitution(1);
                $("#rowmajor").show();
            }
            $("#tbInstitutionCode").val("");
            $("#tbInstitutionName").val("");
        }

        function loadDataInstitution(kategori) {
            var ex = document.getElementById('tableData');
            if ($.fn.DataTable.fnIsDataTable(ex)) {
                // data table, then destroy first
                $("#tableData").dataTable().fnDestroy();
            }

            var OTableData = $('#tableData').dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "iDisplayLength": 10,
                "bJQueryUI": true,
                "bAutoWidth": false,
                "sDom": "ftipr",
                "bDeferRender": true,
                "aoColumnDefs": [
                       { "bSortable": false, "aTargets": [0, 1] },
                       { "sClass": "controlIcon", "aTargets": [0, 1] },
                       { "bVisible": false, "aTargets": [1] }

                ],
                "oLanguage":
                                   { "sSearch": "Search By Name" },

                "sAjaxSource": "../datatable/HandlerDataTableSettings.ashx?commandName=GetUniversitiesByKategori&Kategori=" + kategori

            });
        }

        function rebindingFieldOfStudy(issma, data)
        {
            var ddTarget = "<%= ddFieldOfStudy.ClientID %>";
            var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetFieldOfStudyByFilter&id=" + issma;
            $.ajax({
                url: handlerUrl,
                async: true,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    var dataModel = $.parseJSON(queryResult);

                    $("#" + ddTarget).children().remove();

                    if (dataModel.length > 0) {
                        $("#" + ddTarget).append('<option value="">Select an Option</option>');
                        for (var i = 0; i < dataModel.length; i++) {
                            if (data == dataModel[i].Code)
                                $("#" + ddTarget).append('<option selected value="' + dataModel[i].Code + '">' + dataModel[i].Name + '</option>');
                            else
                                $("#" + ddTarget).append('<option value="' + dataModel[i].Code + '">' + dataModel[i].Name + '</option>');
                        }
                    }
                    $("#" + ddTarget).trigger("chosen:updated");
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });
        }

        function rebindingInstitutions(issma, data)
        {
          <%--  var ddTarget = "<%= ddUniversity.ClientID %>";--%>
            var handlerUrl = "../handlers/HandlerGlobalSettings.ashx?commandName=GetUniversityByFilter&id=" + issma;
            $.ajax({
                url: handlerUrl,
                async: true,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    var dataModel = $.parseJSON(queryResult);

                    $("#" + ddTarget).children().remove();

                    if (dataModel.length > 0) {
                        $("#" + ddTarget).append('<option value="">Select an Option</option>');
                        for (var i = 0; i < dataModel.length; i++) {
                            if (data == dataModel[i].Code)
                                $("#" + ddTarget).append('<option selected value="' + dataModel[i].Code + '">' + dataModel[i].Name + '</option>');
                            else
                                $("#" + ddTarget).append('<option value="' + dataModel[i].Code + '">' + dataModel[i].Name + '</option>');

                        }
                    }
                    $("#" + ddTarget).trigger("chosen:updated");
                },
                error: function (xhr, ajaxOptions, thrownError) {

                    console.log("Error");
                }
            });
        }

   <%--     function toggleMajor() {
            var selectedInstitution = $("#<%= ddUniversity.ClientID %>").val();
            var selectedEducationLevel = $("#ddEducationLevel").val();
            var selectedFieldOfStudy = $("#ddFieldOfStudy").val();
            if (selectedFieldOfStudy == "" || selectedFieldOfStudy == 0) {
                $('#ddMajor').html('<option selected="selected" value=0>Select an option</option>');
            }
            else {
                loadMajorList(selectedInstitution, selectedEducationLevel, selectedFieldOfStudy, "new");
            }
        }--%>

      <%--  function toggleEducationLevel() {
            var selectedInstitution = $("#<%= ddUniversity.ClientID %>").val();
        if (selectedInstitution == "" || selectedInstitution == 0) {
            $('#ddEducationLevel').html('<option selected="selected" value=0>Select an option</option>');
        }
        else {
            loadEducationLevelList(selectedInstitution, "new");
            //toggleFieldOfStudy();

        }
    }--%>

   <%-- function toggleFieldOfStudy() {

        var selectedInstitution = $("#<%= ddUniversity.ClientID %>").val();
        var selectedEducationLevel = $("#ddEducationLevel").val();
        if (selectedEducationLevel == "" || selectedEducationLevel == 0) {
            $('#ddFieldOfStudy').html('<option selected="selected" value=0>Select an option</option>');
        }
        else {
            loadEducationFieldOfStudyList(selectedInstitution, selectedEducationLevel, "new");
            //toggleMajor();
        }
    }--%>

    function loadMajorList(universities, educationLevel, fieldOfStudy, selectedValue) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationMajor&UniversitiesCode=" + universities + "&EducationLevel=" + educationLevel + "&FieldOfStudyCode=" + fieldOfStudy;
        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {
            },
            success: function (queryResult) {
                var dataModel = $.parseJSON(queryResult);
                //EducationLevels = $.parseJSON(queryResult);
                var options = "";
                for (var i = 0; i < dataModel.length; i++) {
                    if (i == 0) {
                        options += '<option value="">Select an option</option>';
                    }
                    if (selectedValue == dataModel[i].Code) {
                        options += '<option selected value="' + dataModel[i].Code + '">' + dataModel[i].Major + '</option>';
                    }
                    else {
                        options += '<option value="' + dataModel[i].Code + '">' + dataModel[i].Major + '</option>';
                    }
                }
                $('#ddMajor').html(options);
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadEducationLevelList(dataId, selectedValue) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationLevel&UniversitiesCode=" + dataId;
        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {
            },
            success: function (queryResult) {
                var dataModel = $.parseJSON(queryResult);
                //EducationLevels = $.parseJSON(queryResult);
                var options = "";
                for (var i = 0; i < dataModel.length; i++) {
                    if (i == 0) {
                        options += '<option value="">Select an option</option>';
                    }
                    if (selectedValue == dataModel[i].EducationLevel) {
                        options += '<option selected value="' + dataModel[i].EducationLevel + '">' + dataModel[i].Name + '</option>';
                    }
                    else {
                        options += '<option value="' + dataModel[i].EducationLevel + '">' + dataModel[i].Name + '</option>';
                    }

                }
                $('#ddEducationLevel').html(options);

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadEducationFieldOfStudyList(universities, educationLevel, selectedValue) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationFieldOfStudy&UniversitiesCode=" + universities + "&EducationLevel=" + educationLevel;
        $.ajax({
            url: handlerUrl,
            async: true,
            beforeSend: function () {
            },
            success: function (queryResult) {
                var dataModel = $.parseJSON(queryResult);
                //EducationLevels = $.parseJSON(queryResult);
                var options = "";
                for (var i = 0; i < dataModel.length; i++) {
                    if (i == 0) {
                        options += '<option value="">Select an option</option>';
                    }
                    if (selectedValue == dataModel[i].Code) {
                        options += '<option selected value="' + dataModel[i].Code + '">' + dataModel[i].Name + '</option>';
                    }
                    else {
                        options += '<option value="' + dataModel[i].Code + '">' + dataModel[i].Name + '</option>';
                    }
                }
                $('#ddFieldOfStudy').html(options);
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function toggleExperienceEndYear() {
        var untillNow = $("#cbExperienceUntillNow").prop("checked");
        if (untillNow) {
            // hide year end
            $("#tbExperienceYearEnd").hide();
            $("#tbExperienceYearEnd").removeAttr("required");
            $("#separatorExperienceYear").hide();
        }
        else {
            // show year end
            $("#tbExperienceYearEnd").show();
            $("#tbExperienceYearEnd").attr("required", "required");
            $("#separatorExperienceYear").show();
        }
    }

    function loadPersonalInformation() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantInfo";

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var applicantModel = $.parseJSON(queryResult);
                if (applicantModel["Photo"] == null) {
                    if (applicantModel["SocMedName"] == "facebook")
                        $("#imgAvatar").attr("src", "http://graph.facebook.com/" + applicantModel["SocMedID"] + "/picture?type=large");
                    else if (applicantModel["SocMedName"] == "linkedin")
                        $("#imgAvatar").attr("src", applicantModel["LinkedInImageUrl"]);
                    else
                        $("#imgAvatar").attr("src", "../assets/images/avatar.png");

                }
                else
                    $("#imgAvatar").attr("src", "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + applicantModel["Code"]);

                if (applicantModel["Name"] != null)
                    $("#spApplicantName").text(applicantModel["Name"]);
                else
                    $("#spApplicantName").text("Unknown");

                $("#spApplicantEmail").text(applicantModel["Email"]);
                $("#spApplicantPhone").text(applicantModel["Phone"]);
                $("#spGenderSpecification").text(applicantModel["GenderSpecification"]);
                $("#spMaritalSpecification").text(applicantModel["MaritalStatusSpecification"]);

                if (applicantModel["CurrentCardAddress"] != null)
                    $("#spApplicantAddress").text(applicantModel["CurrentCardAddress"]);

                if (applicantModel["CurrentCardCityName"] != null)
                    $("#spApplicantCity").text(applicantModel["CurrentCardCityName"]);

                if (applicantModel["CurrentCardCountry"] != null)
                    $("#spApplicantCountry").text(applicantModel["CurrentCardCountry"]);

                $("#spApplicantSalary").text(formatMoney(applicantModel["ExpectedSalary"]));

                $("#spApplicantPriorNotification").text(applicantModel["NoticePeriod"]);
                $("#spApplicantAssignedAnywhere").text(applicantModel["EligibleForAnyCities"]);
                // form
                loadDataIntoForm("personalInfoFrameModel", applicantModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

        function showUploadPhotoForm() {
            $('#photoFrameModel').modal('show');
        }

        function showPersonalInfoForm() {

            $('#personalInfoFrameModel').modal('show');
            //  $('#personalInfoFrameModel').modal({ backdrop: 'static', keyboard: false })
        }

        function uploadPhoto() {
            var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UploadApplicantPhoto";
            var fileUpload = $("#photoUpload").get(0);
            var files = fileUpload.files;
            if (files != null && files.length > 0) {

                if (files[0].size > 500000) {
                    showNotification("File image too large, Maximal file size is 500KB");
                    return;
                }

                if (!isImage(files[0].name)) {
                    showNotification("You have uploaded an invalid image file type, allowed file type : jpg, jpeg, png, gif and bmp");
                    return;
                }
            }
            var test = new FormData();
            for (var i = 0; i < files.length; i++) {
                test.append(files[i].name, files[i]);
            }
            $.ajax({
                url: actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            // dataType: "json",
            success: function (result) {
                window.location.href = window.location.href;
            },
            error: function (err) {
                showNotification(err.statusText);
            }
        });
    }

    function getExtension(filename) {
        var parts = filename.split('.');
        return parts[parts.length - 1];
    }

    function isImage(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'gif':
            case 'bmp':
            case 'png':
            case 'jpeg':
                //etc
                return true;
        }
        return false;
    }

    function updatePassword() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdatePassword";

        var param = "&updatePassword=" + $("#tbUpdatePassword").val();

        if ($("#tbUpdatePassword").val() == "" || $("#tbUpdatePasswordConfirm").val() == "") {

            if ($("#tbUpdatePassword").val() == "")
                showNotification("Password is required!");

            if ($("#tbUpdatePasswordConfirm").val() == "")
                showNotification("Confirm Password is required!");

        } else {

            if ($("#tbUpdatePassword").val() != $("#tbUpdatePasswordConfirm").val()) {
                showNotification("Password does not match the confirm password!");
            } else {
                $.ajax({
                    url: handlerUrl + param,
                    async: true,
                    beforeSend: function () {
                    },
                    success: function (queryResult) {

                        $("#tbUpdatePassword").val("");
                        $("#tbUpdatePasswordConfirm").val("");
                        $('#updatePasswordFrameModel').modal('hide');
                        showNotification("Password successfully updated!");
                    },
                    error: function (xhr, ajaxOptions, thrownError) {

                        showNotification(xhr.responseText);
                    }
                });
            }
        }
    }

    function updatePersonalInfoForm() {
        var form = "personalInfoFrameModel";
        var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateApplicantInfo";
        var formData = {};

        if (!getNodeData(form, formData)) return false;

        $.post(actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(), formData).done(function (data) {

            loadPersonalInformation();
            $('#personalInfoFrameModel').modal('hide');

        });
    }

    function showEducationInfoForm(dataId) {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationInfo&id=" + dataId;

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var kategori;
                var dataModel = $.parseJSON(queryResult);

                loadDataIntoForm("educationFrameModel", dataModel);
                if (dataModel["LevelName"] == "SMA" || dataModel["LevelName"] == "SMK") {
                    if (dataModel["LevelName"] == "SMA")
                        kategori = 2;
                    else
                        kategori = 3;
                    rebindingFieldOfStudy(kategori, dataModel["FieldOfStudyCode"]);
                    loadDataInstitution(kategori);
                    //rebindingInstitutions(kategori, dataModel["InstitutionCode"]);
                    $("#rowmajor").hide();
                }
                else {
                    rebindingFieldOfStudy(1, dataModel["FieldOfStudyCode"]);
                    loadDataInstitution(1);
                    //rebindingInstitutions(1, dataModel["InstitutionCode"]);
                    $("#rowmajor").show();
                }
               
                if (dataModel["EducationCountryCode"] == "Indonesia") {
                    $("#<%= ddEducationCity.ClientID %>_chosen").show();
                    $("#tbEducationCity").hide();
                }
                else {
                    $("#<%= ddEducationCity.ClientID %>_chosen").hide();
                    $("#tbEducationCity").show();
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#educationFrameModel').modal('show');
    }

    function loadEducationInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationInfoList";
        var listContainer = $("#containerEducationList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".educationData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["LevelName"] + " at " + item["InstitutionName"];
                    //loadMajorList(item["LevelName"]);
                    if (item["FieldOfStudyName"] != null)
                        dataInfoText += ", Field of Study: " + item["FieldOfStudyName"];
                    if (item["MajorName"] != null)
                        dataInfoText += ", Majoring in: " + item["MajorName"];


                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("data-toggle", "confirmation").attr("data-on-confirm", "deleteEducationInfoForm").attr("data-key", item["Id"]);
                    var dataInfoWrapper = $(document.createElement('span')).attr("class", "link").attr("onclick", "showEducationInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block educationData").append(commandIcon).append(dataInfoWrapper);

                    listContainer.append(dataNode);


                });


                initiateConfirmation();
            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

        function chooseInstitution()
        {
            $("#InstitutionDataFrameModel").modal('show');
        }

        function updateEducationInfoForm() {
            var minYear = $("#educationStartYear").val();
            var maxYear = $("#educationEndYear").val();
            var gpa = parseFloat($("#educationGPA").val());
            var gpaTotal = parseFloat($("#educationGPATotal").val());

            var validationMessage = "";
            var formValid = true;

            if (minYear > maxYear) {
                formValid = false;
                validationMessage = "End year must be later than or equal start year!";
            }

            if (gpa < 0) {
                formValid = false;
                validationMessage = "GPA must not be minus value!";
            }

            if (gpa > gpaTotal) {
                formValid = false;
                validationMessage = "GPA Total must be greater than your GPA!";
            }

            if (!formValid) {
                alert(validationMessage);
            }
            else {
                var form = "educationFrameModel";
                var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateApplicantEducationInfo";
                var formData = {};
                var issma = false;
                var isvalid = true;

                if ($("#<%= ddEducationLevel.ClientID %> option:selected").val() == "")
                {
                    validationMessage = "Please fill required(*) field!";
                    isvalid = false;
                    alert(validationMessage);
                } else if ($("#<%= ddEducationLevel.ClientID %> option:selected").text() == "SMA" ||  ($("#<%= ddEducationLevel.ClientID %> option:selected").text() == "SMK"))
                {
                    issma = true;
                    if ( $("#<%= ddEducationCountry.ClientID %> option:selected").val() == "" || $("#tbEducationCity").val() == "" ||
                    $("#tbInstitutionName").val() == "" || $("#<%= ddEducationLevel.ClientID %> option:selected").val() == "" ||
                    $("#<%= ddFieldOfStudy.ClientID %> option:selected").val() == "" ) {
                        validationMessage = "Please fill required(*) field!";
                        isvalid = false;
                        alert(validationMessage);
                    } 
                } else {
                    issma = false;
                    if ($("#tbInstitutionName").val() == "" ||  $("#<%= ddEducationCountry.ClientID %> option:selected").val() == "" ||
                  $("#tbEducationCity").val() == "" || $("#<%= ddEducationLevel.ClientID %> option:selected").val() == "" ||
                  $("#<%= ddFieldOfStudy.ClientID %> option:selected").val() == "" ||  $("#<%= ddMajor.ClientID %> option:selected").val() == "") {
                        validationMessage = "Please fill required(*) field!";
                        isvalid = false;
                        alert(validationMessage);
                    } 
                }
            }

            //if (!getNodeData(form, formData)) return false;

            if (isvalid) {
                formData["Id"] = $("#Id").val(); 
                formData["LevelCode"] = $("#<%= ddEducationLevel.ClientID %>").val();
                formData["StartYear"] = $("#educationStartYear").val();
                formData["EndYear"] = $("#educationEndYear").val();
                formData["FieldOfStudyCode"] = $("#<%= ddFieldOfStudy.ClientID %>").val();
                if ($("#<%= ddEducationLevel.ClientID %> option:selected").text() == "SMA" || ($("#<%= ddEducationLevel.ClientID %> option:selected").text() == "SMK"))
                {
                    formData["MajorCode"] = null;
                } else {
                    formData["MajorCode"] = $("#<%= ddMajor.ClientID %>").val();
                }
                
                formData["Score"] = $("#educationGPA").val();
                formData["ScoreTotal"] = $("#educationGPATotal").val();

                formData["EducationCountryCode"] = $("#<%= ddEducationCountry.ClientID %>").val();
                formData["InstitutionCode"] =  $("#tbInstitutionCode").val();
                formData["InstitutionName"] =  $("#tbInstitutionName").val();
                var selectedCountry = $("#<%= ddEducationCountry.ClientID %>").val();
                 
                if (selectedCountry == "Indonesia") {
                    formData["EducationCityName"] = $("#tbEducationCity").val();
                    formData["EducationCityCode"] = $("#<%= ddEducationCity.ClientID %>").val();
                }
                else {
                    formData["EducationCityCode"] = null;
                    formData["EducationCityName"] = $("#tbEducationCity").val();
                }
                $.post(actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(), formData).done(function (data) {

                    loadEducationInfoList();
                    $('#educationFrameModel').modal('hide');

                }).fail(function (xhr, status, error) {

                    showNotification(xhr.responseText);

                });
            }
        }


    

    function showExperienceInfoForm(dataId) {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantExperienceInfo&id=" + dataId;

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("experienceFrameModel", dataModel);
                toggleExperienceEndYear();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#experienceFrameModel').modal('show');
    }

    function loadExperienceInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantExperienceInfoList";
        var listContainer = $("#containerExperienceList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".experienceData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                    var startmonth = monthNames[item["StartMonth"] - 1];
                    var endmonth = monthNames[item["EndMonth"] - 1];

                    var dataInfoText = startmonth + " " + item["StartYear"] + " - " + endmonth + " " + item["EndYear"] + ", " + item["PositionName"] + " at " + item["OrganizationName"];
                    if (item["StillUntillNow"] == true)
                        dataInfoText = startmonth + " " + item["StartYear"] + " - Now, " + item["PositionName"] + " at " + item["OrganizationName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("data-toggle", "confirmation").attr("data-on-confirm", "deleteExperienceInfoForm").attr("data-key", item["Id"]);
                    var dataInfoWrapper = $(document.createElement('span')).attr("class", "link").attr("onclick", "showExperienceInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block experienceData").append(commandIcon).append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

                initiateConfirmation();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function updateExperienceInfoForm() {

        var min = $("#tbExperienceYearStart").val();
        var max = $("#tbExperienceYearEnd").val();
        var untillNow = $('#cbExperienceUntillNow').is(':checked');

        var validationMessage = "";
        var formValid = true;

        if (!untillNow) {
            if (min > max) {
                formValid = false;
                validationMessage = "End month year must be later than or equal start month year";
            }

        }

        if (!formValid) {
            alert(validationMessage);
        }
        else {

            var form = "experienceFrameModel";
            var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateApplicantExperienceInfo";
            var formData = {};

            if (!getNodeData(form, formData)) return false;

            $.post(actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(), formData).done(function (data) {

                loadExperienceInfoList();
                $('#experienceFrameModel').modal('hide');

            });
        }
    }

    function showSkillInfoForm(dataId) {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantSkillInfo&id=" + dataId;

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("skillFrameModel", dataModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#skillFrameModel').modal('show');
    }

    function loadSkillInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantSkillInfoList";
        var listContainer = $("#containerSkillList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".skillData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["SkillName"] + " with Proficiency: " + item["Proficiency"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("data-toggle", "confirmation").attr("data-on-confirm", "deleteSkillInfoForm").attr("data-key", item["Id"]);
                    var dataInfoWrapper = $(document.createElement('span')).attr("class", "link").attr("onclick", "showSkillInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block skillData").append(commandIcon).append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

                initiateConfirmation();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function updateSkillInfoForm() {

        var form = "skillFrameModel";
        var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateApplicantSkillInfo";
        var formData = {};

        if (!getNodeData(form, formData)) return false;

        $.post(actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(), formData).done(function (data) {

            loadSkillInfoList();
            $('#skillFrameModel').modal('hide');

        });
    }

    function showAttachmentForm(dataId) {

        $('#attachmentFrameModel').modal('show');
    }

    function loadAttachmentList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAttachmentInfoList";
        var listContainer = $("#containerAttachmentList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".attachmentData").remove();
            },
            success: function (queryResult) {


                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["FileName"] + " - " + item["Notes"];


                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("data-toggle", "confirmation").attr("data-on-confirm", "deleteAttachmentInfoForm").attr("data-key", item["Id"]);
                    var dataInfoWrapper = $(document.createElement('a')).attr("href", "../handlers/HandlerApplicants.ashx?commandName=DownloadAttachment&id=" + item["Id"]).attr("target", "_blank").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block attachmentData").append(commandIcon).append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

                initiateConfirmation();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function isAttachment(filename) {
        var ext = getExtension(filename);
        switch (ext.toLowerCase()) {
            case 'jpg':
            case 'gif':
            case 'bmp':
            case 'png':
            case 'doc':
            case 'ppt':
            case 'pdf':
            case 'jpeg':
                //etc
                return true;
        }
        return false;
    }

    function uploadAttachment() {
        var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UploadApplicantAttachment";
        var notes = $("#tbAttachmentNotes").val();
        var fileUpload = $("#attachmentUpload").get(0);
        var files = fileUpload.files;
        if (!isAttachment(files[0].name)) {
            showNotification("You have uploaded an invalid image file type, allowed file type : jpg, jpeg, png, gif, bmp, doc, ppt and pdf");
            return;
        }

        if (files != null && files.length > 0) {
            if (files[0].size > 5000000) {
                showNotification("File image too large, Maximal file size is 5MB");
                return;
            }
        }
        var test = new FormData();
        for (var i = 0; i < files.length; i++) {
            test.append(files[i].name, files[i]);
        }
        $.ajax({
            url: actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val() + "&notes=" + notes,
            type: "POST",
            contentType: false,
            processData: false,
            data: test,
            // dataType: "json",
            success: function (result) {
                loadAttachmentList();
                $('#attachmentFrameModel').modal('hide');
            },
            error: function (err) {
                showNotification(err.statusText);
            }
        });
    }

    function deleteEducationInfoForm() {

        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantEducationInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadEducationInfoList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function deleteExperienceInfoForm() {

        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantExperienceInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadExperienceInfoList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function deleteSkillInfoForm() {

        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantSkillInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadSkillInfoList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function deleteAttachmentInfoForm() {

        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantAttachmentInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadAttachmentList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function deleteSkillInfoForm() {


        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantSkillInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadSkillInfoList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function deleteAwardInfoForm() {

        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantAwardInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadAwardInfoList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function deleteHobbyInterestInfoForm() {

        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantHobbyInterestInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadHobbyInterestInfoList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function deleteTrainingInfoForm() {

        var id = $(this).attr("data-key");
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=RemoveApplicantTrainingInfo&id=" + id;
        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                loadTrainingInfoList();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }


    function showHobbyInterestInfoForm(dataId) {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantHobbyInterestInfo&id=" + dataId;

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("hobbyInterestFrameModel", dataModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#hobbyInterestFrameModel').modal('show');
    }

    function loadHobbyInterestInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantHobbyInterestInfoList";
        var listContainer = $("#containerHobbyInterestList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".hobbyInterestData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Description"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("data-toggle", "confirmation").attr("data-on-confirm", "deleteHobbyInterestInfoForm").attr("data-key", item["Id"]);
                    var dataInfoWrapper = $(document.createElement('span')).attr("class", "link").attr("onclick", "showHobbyInterestInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block hobbyInterestData").append(commandIcon).append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

                initiateConfirmation();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function updateHobbyInterestInfoForm() {

        var form = "hobbyInterestFrameModel";
        var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateApplicantHobbyInterestInfo";
        var formData = {};

        if (!getNodeData(form, formData)) return false;

        $.post(actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(), formData).done(function (data) {

            loadHobbyInterestInfoList();
            $('#hobbyInterestFrameModel').modal('hide');

        });
    }


    function showAwardInfoForm(dataId) {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAwardInfo&id=" + dataId;

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("awardFrameModel", dataModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#awardFrameModel').modal('show');
    }

    function loadAwardInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAwardInfoList";
        var listContainer = $("#containerAwardList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".awardData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {


                    var dataInfoText = item["Year"] + ", " + item["AwardName"];
                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("data-toggle", "confirmation").attr("data-on-confirm", "deleteAwardInfoForm").attr("data-key", item["Id"]);
                    var dataInfoWrapper = $(document.createElement('span')).attr("class", "link").attr("onclick", "showAwardInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block awardData").append(commandIcon).append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

                initiateConfirmation();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function updateAwardInfoForm() {
        var form = "awardFrameModel";
        var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateApplicantAwardInfo";
        var year = parseInt($("#AwardYear").val());
        var yearNow = new Date().getFullYear();

        var validationMessage = "";
        var formValid = true;

        if (year > yearNow) {
            formValid = false;
            validationMessage = "Award's year must be later than or equal this year";
        }


        if (!formValid) {
            alert(validationMessage);
        }
        else {
            var formData = {};

            if (!getNodeData(form, formData)) return false;
            $.post(actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(), formData).done(function (data) {

                loadAwardInfoList();
                $('#awardFrameModel').modal('hide');

            });
        }
    }




    function showTrainingInfoForm(dataId) {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantTrainingInfo&id=" + dataId;

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var dataModel = $.parseJSON(queryResult);
                // form
                loadDataIntoForm("trainingFrameModel", dataModel);


            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });

        $('#trainingFrameModel').modal('show');
    }

    function loadTrainingInfoList() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantTrainingInfoList";
        var listContainer = $("#containerTrainingList");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".trainingData").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {
                    var pattern = /Date\(([^)]+)\)/;
                    var resultsStart = pattern.exec(item["StartDate"]);
                    var resultsEnd = pattern.exec(item["EndDate"]);
                    var date = new Date(parseFloat(resultsStart[1]));
                    var date2 = new Date(parseFloat(resultsEnd[1]));
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var month = date.getMonth();
                    var month2 = date2.getMonth();
                    var properdateStart = date.getDate() + " " + monthNames[month] + " " + date.getFullYear();
                    var properdateEnd = date2.getDate() + " " + monthNames[month2] + " " + date2.getFullYear();

                    var dataInfoText = properdateStart + " - " + properdateEnd + ", " + item["TrainingName"] + " by " + item["ProviderName"];
                    if (item["NewCertified"] == true)
                        dataInfoText += "; Certified";
                    else
                        dataInfoText += "; Non Certified";

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("data-toggle", "confirmation").attr("data-on-confirm", "deleteTrainingInfoForm").attr("data-key", item["Id"]);
                    var dataInfoWrapper = $(document.createElement('span')).attr("class", "link").attr("onclick", "showTrainingInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block trainingData").append(commandIcon).append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

                initiateConfirmation();

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function updateTrainingInfoForm() {

        var fromDate = $("#trainingStartDate").datepicker('getDate');
        var toDate = $("#trainingEndDate").datepicker('getDate');

        var validationMessage = "";
        var formValid = true;

        if (toDate < fromDate) {
            formValid = false;
            validationMessage = "End Date must be later than or equal Start Date!";
        }

        if (!formValid) {
            alert(validationMessage);
        }
        else {

            var form = "trainingFrameModel";
            var actionUrl = "../handlers/HandlerApplicants.ashx?commandName=UpdateApplicantTrainingInfo";
            var formData = {};

            if (!getNodeData(form, formData)) return false;

            $.post(actionUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(), formData).done(function (data) {

                loadTrainingInfoList();
                $('#trainingFrameModel').modal('hide');

            });
        }
    }

    function showChangePasswordForm() {
        $("#tbOldPassword").val("");
        $("#tbNewPassword").val("");
        $('#changePasswordFrameModel').modal('show');

    }

    function commitChangePassword() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=ChangePassword";

        var param = "&oldPassword=" + $("#tbOldPassword").val();
        param += "&newPassword=" + $("#tbNewPassword").val();
        param += "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val();

        $.ajax({
            url: handlerUrl + param,
            async: true,
            beforeSend: function () {
            },
            success: function (queryResult) {

                $("#tbOldPassword").val("");
                $("#tbNewPassword").val("");
                $('#changePasswordFrameModel').modal('hide');
                showNotification("Password successfully updated");
            },
            error: function (xhr, ajaxOptions, thrownError) {

                showNotification(xhr.responseText);
            }
        });
    }

    function showApplicantProfile(applicantCode) {
        loadPersonalInformation2();
        loadEducationInfoList2();
        loadExperienceInfoList2();
        loadSkillInfoList2();
        loadHobbyInterestInfoList2();
        loadAwardInfoList2();
        loadTrainingInfoList2();
        loadAttachmentList2();
        $('#applicantProfileForm').modal('show');
    }


    function loadPersonalInformation2() {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantInfo";

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {

            },
            success: function (queryResult) {

                var applicantModel = $.parseJSON(queryResult);

                if (applicantModel["Photo"] == null)
                    $("#imgAvatar2").attr("src", "../assets/images/avatar.png");
                else
                    $("#imgAvatar2").attr("src", "../handlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + applicantModel["Code"]);

                if (applicantModel["Name"] != null)
                    $("#spApplicantName2").text(applicantModel["Name"]);
                else
                    $("#spApplicantName2").text("Unknown");

                $("#spApplicantEmail2").text(applicantModel["Email"]);
                $("#spApplicantPhone2").text(applicantModel["Phone"]);
                $("#spGenderSpecification2").text(applicantModel["GenderSpecification"]);
                $("#spMaritalSpecification2").text(applicantModel["MaritalStatusSpecification"]);


                if (applicantModel["CurrentCardAddress"] != null)
                    $("#spApplicantAddress2").text(applicantModel["CurrentCardAddress"]);

                if (applicantModel["CurrentCardCityName"] != null)
                    $("#spApplicantCity2").text(applicantModel["CurrentCardCityName"]);

                if (applicantModel["CurrentCardCountry"] != null)
                    $("#spApplicantCountry2").text(applicantModel["CurrentCardCountry"]);

                $("#spApplicantSalary2").text(formatMoney(applicantModel["ExpectedSalary"]));

                $("#spApplicantPriorNotification2").text(applicantModel["NoticePeriod"]);

                if (applicantModel["CurrentCardCountry"] != "true")
                    $("#spApplicantAssignedAnywhere2").text("Yes");
                else
                    $("#spApplicantAssignedAnywhere2").text("No");

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadEducationInfoList2() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantEducationInfoList";
        var listContainer = $("#containerEducationList2");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".educationData2").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["StartYear"] + " - " + item["EndYear"] + ", " + item["LevelName"] + " at " + item["InstitutionName"];
                    if (item["FieldOfStudyName"] != null)
                        dataInfoText += ", Field of Study: " + item["FieldOfStudyName"];
                    if (item["MajorName"] != null)
                        dataInfoText += ", Majoring in: " + item["MajorName"];


                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteEducationInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showEducationInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block educationData2").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadExperienceInfoList2() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantExperienceInfoList";
        var listContainer = $("#containerExperienceList2");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".experienceData2").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

                    var startmonth = monthNames[item["StartMonth"]];
                    var endmonth = monthNames[item["EndMonth"]];

                    var dataInfoText = startmonth + " " + item["StartYear"] + " - " + endmonth + " " + item["EndYear"] + ", " + item["PositionName"] + " at " + item["OrganizationName"];
                    if (item["StillUntillNow"] == true)
                        dataInfoText = startmonth + " " + item["StartYear"] + " - Now, " + item["PositionName"] + " at " + item["OrganizationName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteExperienceInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showExperienceInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block experienceData2").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadSkillInfoList2() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantSkillInfoList";
        var listContainer = $("#containerSkillList2");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".skillData2").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["SkillName"] + " with Proficiency: " + item["Proficiency"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteSkillInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showSkillInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block skillData2").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAttachmentList2() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAttachmentInfoList";
        var listContainer = $("#containerAttachmentList2");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".attachmentData2").remove();
            },
            success: function (queryResult) {


                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["FileName"] + " - " + item["Notes"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteAttachmentInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('a')).attr("href", "../handlers/HandlerApplicants.ashx?commandName=DownloadAttachment&id=" + item["Id"]).attr("target", "_blank").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block attachmentData2").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadHobbyInterestInfoList2() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantHobbyInterestInfoList";
        var listContainer = $("#containerHobbyInterestList2");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".hobbyInterestData2").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Description"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteHobbyInterestInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showHobbyInterestInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block hobbyInterestData2").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadAwardInfoList2() {
        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantAwardInfoList";
        var listContainer = $("#containerAwardList2");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".awardData2").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {

                    var dataInfoText = item["Year"] + " with " + item["AwardName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteAwardInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showAwardInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block awardData2").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    function loadTrainingInfoList2() {

        var handlerUrl = "../handlers/HandlerApplicants.ashx?commandName=GetApplicantTrainingInfoList";
        var listContainer = $("#containerTrainingList2");

        $.ajax({
            url: handlerUrl + "&applicantCode=" + $("#<%= hidCurrentApplicantCode.ClientID %>").val(),
            async: true,
            beforeSend: function () {
                $(".trainingData2").remove();
            },
            success: function (queryResult) {

                var modelDataList = $.parseJSON(queryResult);
                $.each(modelDataList, function (i, item) {
                    var pattern = /Date\(([^)]+)\)/;
                    var resultsStart = pattern.exec(item["StartDate"]);
                    var resultsEnd = pattern.exec(item["EndDate"]);
                    var date = new Date(parseFloat(resultsStart[1]));
                    var date2 = new Date(parseFloat(resultsEnd[1]));
                    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
                    "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                    var month = date.getMonth();
                    var month2 = date2.getMonth();
                    var properdateStart = date.getDate() + " " + monthNames[month] + " " + date.getFullYear();
                    var properdateEnd = date2.getDate() + " " + monthNames[month2] + " " + date2.getFullYear();

                    var dataInfoText = properdateStart + " - " + properdateEnd + ", " + item["TrainingName"] + " by " + item["ProviderName"];

                    var commandIcon = $(document.createElement('i')).attr("class", "fa fa-trash").attr("onclick", "deleteTrainingInfoForm(" + item["Id"] + ")");
                    var dataInfoWrapper = $(document.createElement('span')).attr("onclick", "showTrainingInfoForm(" + item["Id"] + ")").text(dataInfoText);
                    var dataNode = $(document.createElement('span')).attr("class", "block trainingData2").append(dataInfoWrapper);

                    listContainer.append(dataNode);

                });

            },
            error: function (xhr, ajaxOptions, thrownError) {

                console.log("Error");
            }
        });
    }

    </script>


    <asp:HiddenField ID="hidCurrentApplicantCode" runat="server" />
    <div id="profile" class="row">
        <div class="col-md-12 col-sm-12">
            <div class="row jobSection" style="border-top: none;">
                <div class="col-md-2 col-sm-2">
                    <img class="img-thumbnail" onerror='imgError(this);' id="imgAvatar" width="304" height="236" />
                </div>
                <div class="col-md-10 col-sm-10">
                    <div class="block capitalLetter">
                        <h3><span id="spApplicantName"></span></h3>
                    </div>
                    <div class="block"><span id="spApplicantEmail"></span></div>
                    <div class="block"><span id="spApplicantPhone"></span></div>
                    <div class="block"><span id="spGenderSpecification"></span>, <span id="spMaritalSpecification"></span></div>
                    <div class="block"><span id="spIsRegis"></span></div>
                    <%-- <div class="block link">
                        <span data-lang="EditProfileButton" onclick="showPersonalInfoForm(); return false;"><i class="fa fa-pencil"></i>Edit Profile</span>
                        &nbsp;&nbsp;<span data-lang="EditPhotoButton" onclick="showUploadPhotoForm(); return false;"><i class="fa fa-camera"></i>Edit Photo</span>
                        &nbsp;&nbsp;<span data-lang="ChangePasswordButton" onclick="showChangePasswordForm();"><i class="fa fa-lock"></i>Change Password</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-laptop"></i><span class="link" data-lang="ReviewProfileButton" onclick="showApplicantProfile(); return false;">Review Profile</span>
                    </div>--%>
                    <div class="block">
                        <i class="fa fa-pencil"></i><span class="link" data-lang="EditProfileButton" onclick="showPersonalInfoForm(); return false;">Edit Profile</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-camera"></i><span class="link" data-lang="EditPhotoButton" onclick="showUploadPhotoForm(); return false;">Edit Photo</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-lock"></i><span class="link" data-lang="ChangePasswordButton" onclick="showChangePasswordForm();">Change Password</span>
                        &nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-laptop"></i><span class="link" data-lang="ReviewProfileButton" onclick="showApplicantProfile();">Review Profile</span>
                    </div>

                </div>
            </div>
        </div>
        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <i class="fa fa-map-marker"></i><strong><span data-lang="CategoryAddressLabel">Address Information</span></strong>
            </div>
            <div class="col-md-10 col-sm-10">
                <div class="block"><span id="spApplicantAddress"></span></div>
                <div class="block"><span id="spApplicantCity"></span></div>
                <div class="block"><span id="spApplicantCountry"></span></div>
            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <strong><i class="fa fa-briefcase"></i><span data-lang="CategoryPlacementLabel">Career & Placements</span></strong>
            </div>
            <div class="col-md-10 col-sm-10">
                <div data-lang="CategoryExpectedSalaryLabel" class="block">Expected Salary: <span id="spApplicantSalary"></span></div>
                <div data-lang="CategoryPriorNotificationLabel" class="block">Prior Notification Period: <span id="spApplicantPriorNotification"></span><span data-lang="MonthFrontEndLabel">month(s)</span></div>
                <div data-lang="CategoryWillingAssignedLabel" class="block">Willing to be assigned in difference cities in Indonesia? <span id="spApplicantAssignedAnywhere"></span></div>
            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <i class="fa fa-book"></i><strong><span data-lang="CategoryEducationLabel">Education</span></strong>
            </div>
            <div class="col-md-10 col-sm-10" id="containerEducationList">
                <div class="block"><i class="fa fa-plus"></i><a  data-lang="ButtonEducationLabel" href="#" onclick="showEducationInfoForm(); return false;">Add Education</a></div>

            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <strong><i class="fa fa-suitcase"></i><span data-lang="CategoryExperienceLabel">Experiences</span></strong>
            </div>
            <div class="col-md-10 col-sm-10" id="containerExperienceList">
                <div class="block"><i class="fa fa-plus"></i><a data-lang="ButtonExperienceLabel" onclick="showExperienceInfoForm(); return false;">Add Experience</a></div>
            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <strong><i class="fa fa-wrench"></i><span data-lang="CategorySkillLabel">Skills</span></strong>
            </div>
            <div class="col-md-10 col-sm-10" id="containerSkillList">
                <div class="block"><i class="fa fa-plus"></i><a data-lang="ButtonSkillLabel" onclick="showSkillInfoForm(); return false;">Add Skill</a></div>
            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <i class="fa fa-smile-o"></i><strong><span data-lang="CategoryHobbyAndInterestLabel">Hobbies & Interest</span></strong>
            </div>
            <div class="col-md-10 col-sm-10" id="containerHobbyInterestList">
                <div class="block"><i class="fa fa-plus"></i><a data-lang="ButtonHobbyAndInterestLabel" onclick="showHobbyInterestInfoForm(); return false;">Add Hobby & Interest</a></div>
            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <strong><i class="fa fa-trophy"></i><span data-lang="CategoryAwardLabel">Awards</span></strong>
            </div>
            <div class="col-md-10 col-sm-10" id="containerAwardList">
                <div class="block"><i class="fa fa-plus"></i><a data-lang="ButtonAwardLabel" onclick="showAwardInfoForm(); return false;">Add Awards</a></div>
            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <strong><i class="fa fa-gears"></i><span data-lang="CategoryTrainingLabel">Trainings</span></strong>
            </div>
            <div class="col-md-10 col-sm-10" id="containerTrainingList">
                <div class="block"><i class="fa fa-plus"></i><a data-lang="ButtonTrainingLabel" onclick="showTrainingInfoForm(); return false;">Add Training Records</a></div>
            </div>
        </div>

        <div class="row jobSection">
            <div class="col-md-2 col-sm-2">
                <strong><i class="fa fa-file"></i><span data-lang="CategoryAttachmentLabel">Attachments</span></strong>
            </div>
            <div class="col-md-10 col-sm-10" id="containerAttachmentList">
                <div class="block"><i class="fa fa-plus"></i><a data-lang="ButtonAttachmentLabel" onclick="showAttachmentForm(); return false;">Add Attachment</a></div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="applicantProfileForm">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="row jobSection" style="border-top: none;">
                                <div class="col-md-2 col-sm-2">
                                    <img class="img-circle" onerror='imgError(this);' id="imgAvatar2" width="304" height="236" />
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="block capitalLetter">
                                        <h3><span class="applicantname" id="spApplicantName2"></span></h3>
                                    </div>
                                    <div class="block"><span id="spApplicantEmail2"></span></div>
                                    <div class="block"><span id="spApplicantPhone2"></span></div>
                                    <div class="block"><span id="spGenderSpecification2"></span>, <span id="spMaritalSpecification2"></span></div>
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryAddressLabel">Address Information</h4>
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="block"><span id="spApplicantAddress2"></span></div>
                                    <div class="block"><span id="spApplicantCity2"></span></div>
                                    <div class="block"><span id="spApplicantCountry2"></span></div>
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryPlacementLabel">Career & Placements</h4>
                                </div>
                                <div class="col-md-10 col-sm-10">
                                    <div class="block"><span data-lang="CategoryExpectedSalaryLabel">Expected Salary: </span><span id="spApplicantSalary2"></span></div>
                                    <div class="block"><span data-lang="CategoryPriorNotificationLabel">Prior Notification Period:</span> <span id="spApplicantPriorNotification2"></span><span data-lang="MonthFrontEndLabel">month(s)</span></div>
                                    <div class="block"><span data-lang="CategoryWillingAssignedLabel">Willing to be assigned in difference cities in Indonesia? </span><span id="spApplicantAssignedAnywhere2"></span></div>
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryEducationLabel">Education</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerEducationList2">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryExperienceLabel">Experiences</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerExperienceList2">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategorySkillLabel">
                                    Skills</h>
                               
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerSkillList2">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryHobbyAndInterestLabel">Hobbies & Interest</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerHobbyInterestList2">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryAwardLabel">Awards</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerAwardList2">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryTrainingLabel">Trainings</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerTrainingList2">
                                </div>
                            </div>

                            <div class="row jobSection">
                                <div class="col-md-2 col-sm-2">
                                    <h4 data-lang="CategoryAttachmentLabel">Attachments</h4>
                                </div>
                                <div class="col-md-10 col-sm-10" id="containerAttachmentList2">
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="photoFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="photoFrameModel_Title" class="modal-title center">Upload Photo</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="photoFrameModel_UploadPhoto" class="col-md-3 col-sm-3">
                            Upload Photo
                       
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="file" id="photoUpload" data-attribute="Photo" />
                            <p style="font-size: 10px">*Max file size is 500KB </br> type : .jpeg .jpg, .gif, .bmp, .png</p>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-lang="ButtonSave" type="button" class="btn btn-primary" onclick="uploadPhoto(); return false;">Save</button>
                    <button data-lang="ButtonCancel" type="button" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="personalInfoFrameModel" data-keyboard="false" data-backdrop="static">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="personalInfoFrameModel_Title" class="modal-title center">Update Personal Information</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_FirstNameLabel" class="col-md-3 col-sm-3">
                            First Name<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control capitalLetter" type="text" required="" data-attribute="FirstName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_LastNameLabel" class="col-md-3 col-sm-3">
                            Last Name
                       
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input class="form-control capitalLetter" type="text" required="" data-attribute="LastName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_GenderLabel" class="col-md-3 col-sm-3">
                            Gender<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="GenderSpecification" class="form-control">
                                <option></option>
                                <option>Male</option>
                                <option>Female</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_MaritalStatusLabel" class="col-md-3 col-sm-3">
                            Marital Status<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="MaritalStatusSpecification" class="form-control">
                                <option></option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Divorce</option>
                            </select>

                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_BirthDateLabel" class="col-md-3 col-sm-3">
                            Birth Date<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="controls">
                                <div class="input-group date col-sm-9">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" data-date-format="dd/mm/yyyy" required=""
                                        data-attribute="Birthday"
                                        class="form-control datepicker" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_BirthPlaceLabel" class="col-md-3 col-sm-3">
                            Birth Place<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control" type="text" required="" data-attribute="BirthPlace" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang=" " class="col-md-3 col-sm-3">
                            Email<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <input disallowed-chars="[^a-zA-Zs ]+" data-attribute="Email"
                                required=""
                                class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_PhoneLabel" class="col-md-3 col-sm-3">
                            Phone<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input disallowed-chars="[^a-zA-Zs ]+" data-attribute="Phone"
                                required=""
                                class="form-control plainnumber" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_ReligionLabel" class="col-md-3 col-sm-3">
                            Religion<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="Religion" class="form-control">
                                <option></option>
                                <option>Moslem</option>
                                <option>Christian</option>
                                <option>Catholic</option>
                                <option>Hindhu</option>
                                <option>Buddha</option>
                                <option>Kong Hu cu</option>
                                <option>Kepercayaan</option>
                            </select>

                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_NationalityLabel" class="col-md-3 col-sm-3">
                            Nationality<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="Nationality" id="ddNationality" datavaluefield="Code" datatextfield="Name" runat="server" class="form-control">
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_IDNumberLabel" class="col-md-3 col-sm-3">
                            ID Number<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input disallowed-chars="[^a-zA-Zs ]+" data-attribute="IDCardNumber"
                                required=""
                                class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_AddressLabel" class="col-md-3 col-sm-3">
                            Address<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <textarea class="form-control" required="" data-attribute="CurrentCardAddress" id="tbAddress"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_CityLabel" class="col-md-3 col-sm-3">
                            City<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select id="ddCity" runat="server" datavaluefield="CityCode" data-attribute="CurrentCardCity" datatextfield="CityName" class="form-control"></select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_DesiredSalaryLabel" class="col-md-3 col-sm-3">
                            Desired Salary
                       
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input data-attribute="ExpectedSalary" required="" class="form-control numeric" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_PlacedInDifferentCitiesLabel" class="col-md-4 col-sm-4">
                            Placed in Different Cities?
                       
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="EligibleForAnyCities" class="form-control">
                                <option></option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_PriorNotificationLabel" class="col-md-5 col-sm-5">
                            Prior Notification Period (Months)
                       
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control numeric" type="text" required="" data-attribute="NoticePeriod" />
                        </div>
                        <div class="col-sm-2">
                            <i class="glyphicon glyphicon-exclamation-sign text-info" title="2 months, means, your first day will be 2 months after signing the offering letter."></i>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_FacebookLabel" class="col-md-3 col-sm-3">
                            Facebook
                       
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input data-attribute="FacebookUrl" class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_TwitterLabel" class="col-md-3 col-sm-3">
                            Twitter
                       
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input data-attribute="TwitterUrl" class="form-control" type="text" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="personalInfoFrameModel_LinkedInLabel" class="col-md-3 col-sm-3">
                            Linked IN
                       
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input data-attribute="LinkedInUrl" class="form-control" type="text" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updatePersonalInfoForm(); return false;">Save</button>
                    <button type="button" data-lang="ButtonCancel" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="educationFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="educationFrameModel_Title" class="modal-title center">Education Information</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="educationFrameModel_YearLabel" class="col-md-3 col-sm-3">
                            Year<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control plainnumber" id="educationStartYear" maxlength="4" name="startyear" type="text" required="" data-attribute="StartYear" />
                        </div>
                        <div class="col-md-1 col-sm-1 center">
                            -
                       
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control plainnumber" id="educationEndYear" maxlength="4" name="endyear" type="text" required="" data-attribute="EndYear" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="educationFrameModel_CountryLabel" class="col-md-3 col-sm-3">
                            Country<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="form-control" id="ddEducationCountry" runat="server" onchange="toggleEducationCity();" datavaluefield="Code" datatextfield="Name" required="" data-attribute="EducationCountryCode" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="educationFrameModel_LocationLabel" class="col-md-3 col-sm-3">
                            Location<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <select class="form-control" id="ddEducationCity" runat="server" datavaluefield="CityCode" datatextfield="CityName" required="" data-attribute="EducationCityCode" onchange="updateTextBoxCity();" />
                            <input type="text" id="tbEducationCity" class="form-control" data-attribute="EducationCityName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="educationFrameModel_EducationLevelLabel" class="col-md-3 col-sm-3">
                            Education Level<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <%-- <select id="ddEducationLevel" onchange="toggleFieldOfStudy();" data-attribute="LevelCode">
                                </select>--%>
                            <select class="form-control" id="ddEducationLevel" runat="server" datavaluefield="EducationLevel" datatextfield="Name" required="" data-attribute="LevelCode" onchange="toggleInstitution();" />
                            <input type="hidden" data-attribute="Id" id="Id" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="educationFrameModel_InstitutionLabel" class="col-md-3 col-sm-3">
                            Institution Name<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-7 col-sm-7">
                          <input class="form-control" type="text" required="" data-attribute="InstitutionName" id="tbInstitutionName" disabled/>
                            <input type="hidden" data-attribute="InstitutionCode" id="tbInstitutionCode" />
                            <%--<select class="form-control" id="ddUniversity" runat="server" datavaluefield="Code" datatextfield="Name" required="" data-attribute="InstitutionCode" onchange="toggleEducationLevel();" />--%>
                            <%--<select class="form-control" id="ddUniversity" runat="server" datavaluefield="Code" datatextfield="Name" required="" data-attribute="InstitutionCode" />--%>

                        </div>
                        <div class="col-md-2col-sm-2">
                            <button type="button" data-lang="ButtonChooseUniv" class="btn btn-default" onclick="chooseInstitution(); return false;">Select</button>

                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="educationFrameModel_FieldOfStudyLabel" class="col-md-3 col-sm-3">
                            Field Of Study<span class="requiredmark">*</span>

                        </div>
                        <div class="col-md-9 col-sm-9">
                            <select class="form-control" id="ddFieldOfStudy" runat="server" datavaluefield="Code" datatextfield="Name" required="" data-attribute="FieldOfStudyCode" />
                            <%-- <select id="ddFieldOfStudy" data-attribute="FieldOfStudyCode" onchange="toggleMajor();"></select>--%>
                        </div>
                    </div>
                    <div class="row" id="rowmajor">
                        <div data-lang="educationFrameModel_MajorLabel" class="col-md-3 col-sm-3">
                            Major<span class="requiredmark">*</span>

                        </div>
                        <div class="col-md-9 col-sm-9">
                            <%--<select id="ddMajor" data-attribute="MajorCode"></select>--%>
                            <select class="form-control" id="ddMajor" runat="server" datavaluefield="Code" datatextfield="Major" data-attribute="MajorCode" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="educationFrameModel_GPALabel" class="col-md-3 col-sm-3">
                            GPA
                       
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control plainnumber" id="educationGPA" maxlength="4" type="text" data-attribute="Score" />
                        </div>
                        <div data-lang="educationFrameModel_GPAOutOfLabel" class="col-md-2 col-sm-2">
                            Out Of
                       
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control plainnumber" id="educationGPATotal" maxlength="4" type="text" data-attribute="ScoreTotal" />
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updateEducationInfoForm(); return false;">Save</button>
                    <button type="button" data-lang="ButtonSave" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="experienceFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="experienceFrameModel_Title" class="modal-title center">Experience Information</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="experienceFrameModel_YearLabel" class="col-md-4 col-sm-4">
                            Year<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-2 col-sm-2" style="padding-right: 0px">
                            <input type="text"
                                id="tbExperienceYearStart" required="" placeholder="Start Month"
                                class="form-control month-picker" data-attribute="StartMonthYear" />
                            <input type="hidden" data-attribute="Id" />
                        </div>
                        <div class="col-md-1 col-sm-1 center" style="padding: 0px" id="separatorExperienceYear">
                            -
                       
                        </div>
                        <div class="col-md-2 col-sm-2" style="padding-left: 0px">
                            <input type="text"
                                id="tbExperienceYearEnd" required="" placeholder="End Month"
                                class="form-control month-picker" data-attribute="EndMonthYear" />
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input type="checkbox" data-attribute="StillUntillNow" id="cbExperienceUntillNow" onchange="toggleExperienceEndYear();" />
                            <span data-lang="UntilNowLabel">Until Now</span>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="experienceFrameModel_OrganizationNameLabel" class="col-md-4 col-sm-4">
                            Organization Name<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" required="" data-attribute="OrganizationName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="experienceFrameModel_PositionLabel" class="col-md-4 col-sm-4">
                            Position<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" required="" data-attribute="PositionName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="experienceFrameModel_JobDescriptionLabel" class="col-md-4 col-sm-4">
                            Job Description<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <textarea class="form-control" required="" data-attribute="JobDescription"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="experienceFrameModel_SalaryLabel" class="col-md-4 col-sm-4">
                            Salary
                       
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <input class="form-control numeric" type="text" data-attribute="SalaryAmount" />
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updateExperienceInfoForm(); return false;">Save</button>
                    <button type="button" data-lang="ButtonCancel" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="skillFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="skillFrameModel_Title" class="modal-title center">Skill Information</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="skillFrameModel_SkillNameLabel" class="col-md-3 col-sm-3">
                            Skill Name<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" type="text" required="" data-attribute="SkillName" />
                            <input type="hidden" data-attribute="Id" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="skillFrameModel_ProficiencyLabel" class="col-md-3 col-sm-3">
                            Proficiency
                       
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <select data-attribute="Proficiency" class="form-control">
                                <option value="Beginner">Beginner</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="Advance">Advance</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updateSkillInfoForm(); return false;">Save</button>
                    <button type="button" data-lang="ButtonCancel" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="hobbyInterestFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="hobbyInterestFrameModel_Title" class="modal-title center">Hobby & Interests</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="hobbyInterestFrameModel_DescriptionLabel" class="col-md-3 col-sm-3">
                            Description<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" type="text" required="" data-attribute="Description" />
                            <input type="hidden" data-attribute="Id" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updateHobbyInterestInfoForm(); return false;">Save</button>
                    <button type="button" data-lang="ButtonCancel" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="awardFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="awardFrameModel_Title" class="modal-title center">Award</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="awardFrameModel_YearLabel" class="col-md-3 col-sm-3">
                            Year<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control plainnumber" required="" data-attribute="Year" id="AwardYear" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="awardFrameModel_TitleLabel" class="col-md-3 col-sm-3">
                            Title<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" type="text" required="" data-attribute="AwardName" />
                            <input type="hidden" data-attribute="Id" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="awardFrameModel_DescriptionLabel" class="col-md-3 col-sm-3">
                            Description<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <textarea class="form-control" required="" data-attribute="Description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updateAwardInfoForm(); return false;">Save</button>
                    <button type="button" data-lang="ButtonCancel" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="trainingFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="trainingFrameModel_Title" class="modal-title center">Training</h1>
                </div>
                <div class="modal-body">
                    <div class="row" style="display: none;">
                        <div data-lang="trainingFrameModel_YearLabel" class="col-md-4 col-sm-4">
                            Year<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <input class="form-control plainnumber" type="text" required="" data-attribute="Year" />
                            <input type="hidden" data-attribute="Id" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="trainingFrameModel_TitleLabel" class="col-md-4 col-sm-4">
                            Title<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" required="" data-attribute="TrainingName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="trainingFrameModel_TrainingProviderLabel" class="col-md-4 col-sm-4">
                            Training Provider<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <input class="form-control" type="text" required="" data-attribute="ProviderName" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="trainingFrameModel_TrainingStartDateLabel" class="col-md-4 col-sm-4">
                            Start Date
                       
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="controls">
                                <div class="input-group date col-sm-8">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" data-date-format="mm/dd/yyyy" required=""
                                        data-attribute="StartDate" id="trainingStartDate" name="trainingStartDate"
                                        class="form-control datepicker" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="trainingFrameModel_TrainingEndDateLabel" class="col-md-4 col-sm-4">
                            End Date
                       
                        </div>
                        <div class="col-md-5 col-sm-5">
                            <div class="controls">
                                <div class="input-group date col-sm-8">
                                    <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                    <input type="text" data-date-format="mm/dd/yyyy" required=""
                                        data-attribute="EndDate" id="trainingEndDate" name="trainingEndDate"
                                        class="form-control datepicker" />
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="trainingFrameModel_ProfessionalCertifiedLabel" class="col-md-4 col-sm-4">
                            Professionaly Certified
                       
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <input type="checkbox" data-attribute="NewCertified" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="trainingFrameModel_DescriptionLabel" class="col-md-4 col-sm-4">
                            Description<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-8 col-sm-8">
                            <textarea class="form-control" required="" data-attribute="Description"></textarea>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updateTrainingInfoForm(); return false;">Save</button>
                    <button type="button" data-lang="ButtonCancel" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="attachmentFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h1 data-lang="attachmentFrameModel_Title" class="modal-title center">Upload Attachment</h1>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="attachmentFrameModel_AttachmentLabel" class="col-md-3 col-sm-3">
                            Attachment
                       
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <input type="file" id="attachmentUpload" data-attribute="Attachment" />
                            <input type="hidden" data-attribute="Id" />
                            <p style="font-size: 10px">*Max file size is 5MB </br> type : .doc, .pdf, .ppt, .jpeg, .jpg, .gif, .bmp, .png</p>
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="attachmentFrameModel_NotesLabel" class="col-md-3 col-sm-3">
                            Notes<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" type="text" required="" id="tbAttachmentNotes" data-attribute="Notes" />
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="uploadAttachment(); return false;">Save</button>
                    <button type="button" data-lang="ButtonCancel" class="btn btn-default" data-dismiss="modal" onclick="hidePopup(); return false;">Cancel</button>
                </div>
            </div>
        </div>
    </div>

    <%--    New Modal Form for Update Password After Registrasion Sosmed--%>
    <div class="modal fade" id="updatePasswordFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 data-lang="updatePasswordFrameModel_Title" class="modal-title center">Update Password</h1>
                    <br />
                    <br />
                    <h5 data-lang="updatePasswordFrameModel_SubTitle" class="modal-title center">Your current password is "<mark>12345</mark>"</h5>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div data-lang="updatePasswordFrameModel_PasswordLabel" class="col-md-3 col-sm-3">
                            Password<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" id="tbUpdatePassword" type="password" />
                        </div>
                    </div>
                    <div class="row">
                        <div data-lang="updatePasswordFrameModel_PasswordConfirmLabel" class="col-md-3 col-sm-3">
                            Confirm Password<span class="requiredmark">*</span>
                        </div>
                        <div class="col-md-9 col-sm-9">
                            <input class="form-control" id="tbUpdatePasswordConfirm" type="password" />
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" data-lang="ButtonSave" class="btn btn-primary" onclick="updatePassword(); return false;">Save</button>
                </div>
            </div>
        </div>
    </div>

     <div class="modal fade" id="InstitutionDataFrameModel">
        <div class="modal-dialog">
		    <div class="modal-content">
            <div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			    <h1 class="modal-title center" data-lang="UnivCaptionTitle">Choose Institution</h1>
		    </div>
            <div class="modal-body">
                <table class="table table-striped table-bordered dt-responsive nowrap" id="tableData">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th><span data-lang="UnivNameTitle">Name</span></th>
                       
                        </tr>
                    </thead>

                    <tbody>
                    </tbody>
                </table>

            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>  

    <asp:ObjectDataSource ID="odsJobList"
        runat="server"
        TypeName="ERecruitment.Domain.ApplicantManager"
        DataObjectTypeName="ERecruitment.Domain.Vacancies"
        SelectMethod="GetApplicantActiveVacancies">
        <SelectParameters>
            <asp:Parameter Name="applicantCode" />
        </SelectParameters>
    </asp:ObjectDataSource>
</asp:Content>
