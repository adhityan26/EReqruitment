﻿<%@ Page Title="Contact Information" Language="C#" MasterPageFile="~/masters/applicant-profile.Master" AutoEventWireup="true" CodeBehind="contactInfoProfile.aspx.cs" Inherits="ERecruitment.contactInfoProfile" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript">


    $(document).ready(function () {

        toggleIDCardProvinceAndCity();
        toggleCurrentProvinceAndCity();
       
    });

    function toggleIDCardProvinceAndCity()
    {
        var country = $("#<%= ddCountry.ClientID %>").val();
      
        if(country == "Indonesia")
        {
            $("#<%= ddProvince.ClientID %>").show();
            $("#<%= ddCity.ClientID %>").show();

            $("#<%= tbProvince.ClientID %>").val("");
            $("#<%= tbCity.ClientID %>").val("");

            $("#<%= tbProvince.ClientID %>").hide();
            $("#<%= tbCity.ClientID %>").hide();
        }
        else {

            $("#<%= ddProvince.ClientID %>").hide();
            $("#<%= ddCity.ClientID %>").hide();

            $("#<%= tbProvince.ClientID %>").show();
            $("#<%= tbCity.ClientID %>").show();
        }
       
    }

    
    function toggleCurrentProvinceAndCity()
    {
        var country = $("#<%= ddCurrenctCountry.ClientID %>").val();
        if(country == "Indonesia")
        {
            $("#<%= ddCurrentProvince.ClientID %>").show();
            $("#<%= ddCurrentCity.ClientID %>").show();

            $("#<%= tbCurrentProvince.ClientID %>").val("");
            $("#<%= tbCurrentCity.ClientID %>").val("");

            $("#<%= tbCurrentProvince.ClientID %>").hide();
            $("#<%= tbCurrentCity.ClientID %>").hide();
        }
        else {

            $("#<%= ddCurrentProvince.ClientID %>").hide();
            $("#<%= ddCurrentCity.ClientID %>").hide();

            $("#<%= tbCurrentProvince.ClientID %>").show();
            $("#<%= tbCurrentCity.ClientID %>").show();
        }
    }

    function toggleIDCardAddressIdentic()
    {
        if($("#<%= cbSameAsPermanentAddress.ClientID %>").prop("checked"))
        {
            
            $("#<%= tbCurrentAddress.ClientID %>").val($("#<%= tbAddress.ClientID %>").val()); 
            $("#<%= ddCurrenctCountry.ClientID %>").val($("#<%= ddCountry.ClientID %>").val());
            

            toggleCurrentProvinceAndCity();

            
            $("#<%= ddCurrentProvince.ClientID %>").val($("#<%= ddProvince.ClientID %>").val()); 
            $("#<%= ddCurrentCity.ClientID %>").val($("#<%= ddCity.ClientID %>").val()); 

            
            $("#<%= tbCurrentProvince.ClientID %>").val($("#<%= tbProvince.ClientID %>").val()); 
            $("#<%= tbCurrentCity.ClientID %>").val($("#<%= tbCity.ClientID %>").val());
            
            $("#<%= tbCurrentPostalCode.ClientID %>").val($("#<%= tbPostalCode.ClientID %>").val());
        }
        else {

            $("#<%= tbCurrentAddress.ClientID %>").val(""); 
            $("#<%= ddCurrenctCountry.ClientID %>").val("Indonesia");

            toggleCurrentProvinceAndCity();

            $("#<%= ddCurrentProvince.ClientID %>").val(""); 
            $("#<%= ddCurrentCity.ClientID %>").val(""); 

            
            $("#<%= tbCurrentProvince.ClientID %>").val(""); 
            $("#<%= tbCurrentCity.ClientID %>").val("");
            
            $("#<%= tbCurrentPostalCode.ClientID %>").val("");

        }
    }

function loadCity()
{
    var provinceCode = $("#<%=ddProvince.ClientID %>").val();
    var ddTarget = "<%=ddCity.ClientID %>";
    
    $.ajax({    
        url: "../handlers/HandlerApplicants.ashx?commandName=GetCity&provinceCode=" + provinceCode,
        async: true,
        beforeSend : function(){
        $("#"+ddTarget).after('<span class="loadingIndicator">&nbsp;</span>');
        },
        success: function(queryResult) {
            
            var dataList = queryResult.toString().split("|");
            $("#"+ddTarget).children().remove();
          
            if(dataList.length > 0)
            {
                for (var i = 0; i < dataList.length; i++) {
                    var dataPair = dataList[i].split(",");
                    if (typeof dataPair[1] === "undefined")  continue;
                    
                    if(dataPair.length > 0)
                        $("#"+ddTarget).append('<option value="' + dataPair[0] + '">' + dataPair[1] + '</option>');
                }
            }
           
            $(".loadingIndicator").remove();
        }
    }            
    );
}

</script>   
 <div>
    <div class="row">
     <div class="row">
         <div class="col-xs-12 col-sm-12 space10" style="text-align:left;">
             <h4><b>Permanent Address (based on ID)</b></h4>
         </div>
     </div>        
      <div class="row">
            <div class="col-sm-2">
                <label runat="server"  id="lbContactProfile_Address">Address</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-8 space10">
                  <textarea cols="40" rows="4" disallowed-chars="[^a-zA-Zs ]+" id="tbAddress" 
                                name="name" ng-model="registerFormData.name" form-field="registerForm"
                                ng-minlength="3" min-length="3" required="" runat="server"
                                class="form-control"   />
                                        
            </div>
       </div>
       <div class="row">
            <div class="col-xs-12 col-sm-2 space10" style="text-align:left">
                <label runat="server" id="lbContactProfile_Country">Country</label><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-4">
                <select id="ddCountry" datasourceid="odsCountry" datavaluefield="Code" 
                    onchange="toggleIDCardProvinceAndCity();"
                        datatextfield="Name" runat="server" class="form-control" />   
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_Province">Province</label>
            </div>
            <div class="col-sm-4">
                    <select id="ddProvince" datasourceid="odsProvince" datavaluefield="ProvinceCode" 
                            datatextfield="ProvinceName" onchange="loadCity();"
                            runat="server" class="form-control" />
                    <input disallowed-chars="[^a-zA-Zs ]+" id="tbProvince"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" runat="server"
                        class="form-control" type="text" />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_City">City</label>
            </div>
            <div class="col-sm-4">
                <select id="ddCity" datasourceid="odsCity" datavaluefield="CityCode" 
                    datatextfield="CityName"    
                    runat="server" class="form-control" />   
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCity"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" runat="server"
                        class="form-control" type="text" />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_PostalCode">Postal Code</label><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-4">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbPostalCode"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                                        
            </div>  
        </div>

         <div class="row">
             <div class="col-xs-12 col-sm-12 space10" style="text-align:left;">
                 <h4><b>Current Address</b></h4>
             </div>
         </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_Same">Same with ID Card</label>
            </div>
            <div class="col-xs-12 col-sm-1 space10" style="text-align:left">
                <asp:CheckBox ID="cbSameAsPermanentAddress" onchange="toggleIDCardAddressIdentic();" runat="server" />
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_CurrentAddress">Address</label><span class="requiredmark">*</span>
            </div>
            <div class="col-xs-12 col-sm-8 space10">
                <textarea cols="40" rows="4" disallowed-chars="[^a-zA-Zs ]+" id="tbCurrentAddress" 
                    name="name" ng-model="registerFormData.name" form-field="registerForm"
                    ng-minlength="3" min-length="3" required="" runat="server"
                    class="form-control"   />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_CurrentCountry">Country</label><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-4">
                    <select id="ddCurrenctCountry" datasourceid="odsCountry" 
                        datavaluefield="Code" onchange="toggleCurrentProvinceAndCity();" 
                        datatextfield="Name" runat="server" class="form-control" />   
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_CurrenctProvince">Province</label>
            </div>
            <div class="col-sm-4">
                        <select id="ddCurrentProvince" datasourceid="odsProvince"  onchange="loadCity();return false;"
                            datavaluefield="ProvinceCode" 
                         datatextfield="ProvinceName"    
                         runat="server" class="form-control" />
                        
                    <input disallowed-chars="[^a-zA-Zs ]+" id="tbCurrentProvince"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" runat="server"
                        class="form-control" type="text" />
                                        
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_CurrentCity">City</label>
            </div>
            <div class="col-sm-4">
                <select id="ddCurrentCity" datasourceid="odsCity" datavaluefield="CityCode" 
                    datatextfield="CityName"    
                    runat="server" class="form-control" />   
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCurrentCity"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" runat="server"
                        class="form-control" type="text" />
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-2">
                <label runat="server" id="lbContactProfile_CurrentPostalCode">Postal Code</label><span class="requiredmark">*</span>
            </div>
            <div class="col-sm-4">
                <input disallowed-chars="[^a-zA-Zs ]+" id="tbCurrentPostalCode"
                        name="name" ng-model="registerFormData.name" form-field="registerForm"
                        ng-minlength="3" min-length="3" required="" runat="server"
                        class="form-control" type="text" />
                                        
            </div>  
        </div>
        <div class="row">
            <div class="col-sm-10">
            </div>
            <div class="col-sm-2">
                <button class="btn btn-primary" runat="server" onserverclick="nextbutton_ServerClick" id="nextbutton">
                    Next &nbsp;
                    <i class="fa fa-arrow-right"></i>
                </button>         
            </div>   
        </div>

    </div>
</div>
            



<asp:ObjectDataSource ID="odsProfile"
                      runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Applicants"
                      TypeName="ERecruitment.Domain.AuthenticationManager"
                      SelectMethod="GetApplicant"
                      OnUpdated="odsProfile_Updated"
                      UpdateMethod="UpdateApplicant"
                >
      <SelectParameters>
          <asp:QueryStringParameter Name="code" QueryStringField="code" />
      </SelectParameters>
</asp:ObjectDataSource>


    <asp:ObjectDataSource ID="odsCity" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.City"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCityList"
    />

<asp:ObjectDataSource ID="odsProvince" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Province"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetProvinceList"
    />
    <asp:ObjectDataSource ID="odsCountry" runat="server"
                      DataObjectTypeName="ERecruitment.Domain.Country"
                      TypeName="ERecruitment.Domain.ERecruitmentManager"
                      SelectMethod="GetCountryList"
    />
     
     

</asp:Content>
