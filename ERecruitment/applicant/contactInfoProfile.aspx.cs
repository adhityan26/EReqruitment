﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using System.Web.UI.HtmlControls;
using SS.Web.UI;


namespace ERecruitment
{
    public partial class contactInfoProfile: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
                initiateForm();
        }

        private void initiateForm()
        {
            Applicants getApplicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("code"));

            tbAddress.Value = getApplicant.IDCardAddress;
            ddCountry.DataBind();
            ddCountry.Value = getApplicant.IDCardCountry;

            if (getApplicant.IDCardCountry == "Indonesia" || getApplicant.IDCardCountry == "")
            {
                ddProvince.DataBind();
                ddProvince.Value = getApplicant.IDCardProvince;
                ddCity.DataBind();
                ddCity.Value = getApplicant.IDCardCity;
            }
            else
            {
                tbProvince.Value = getApplicant.IDCardProvince;
                tbCity.Value = getApplicant.IDCardCity;
             
            }
            ddCurrenctCountry.DataBind();
            ddCurrenctCountry.Value = getApplicant.CurrentCardCountry;

            if (getApplicant.CurrentCardCountry == "Indonesia" || getApplicant.CurrentCardCountry == "")
            {
                ddCurrentProvince.DataBind();
                ddCurrentProvince.Value = getApplicant.CurrentCardProvince;
                ddCurrentCity.DataBind();
                ddCurrentCity.Value = getApplicant.CurrentCardCity;
            }
            else
            {
                tbCurrentProvince.Value = getApplicant.CurrentCardProvince;
                tbCurrentCity.Value = getApplicant.CurrentCardCity;
            }

            tbCurrentAddress.Value = getApplicant.CurrentCardAddress;

            tbPostalCode.Value = getApplicant.IDCardPostalCode;
            tbCurrentPostalCode.Value = getApplicant.CurrentCardPostalCode;
        }

        private Applicants saveForm()
        {
            Applicants rv = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("code"));
            rv.IDCardAddress = tbAddress.Value;
            rv.IDCardCountry = ddCountry.Value;
            if (ddCountry.Value == "Indonesia")
            {
                rv.IDCardProvince = ddProvince.Value;
                rv.IDCardCity = ddCity.Value;
            }
            else
            {
                rv.IDCardProvince = tbProvince.Value;
                rv.IDCardCity = tbCity.Value;
            }

            rv.CurrentCardCountry = ddCurrenctCountry.Value;
            if (ddCurrenctCountry.Value == "Indonesia")
            {
                rv.CurrentCardProvince = ddCurrentProvince.Value;
                rv.CurrentCardCity = ddCurrentCity.Value;
            }
            else
            {
                rv.CurrentCardProvince = tbCurrentProvince.Value;
                rv.CurrentCardCity = tbCurrentCity.Value;
            }

            rv.CurrentCardAddress = tbCurrentAddress.Value;
            rv.IDCardPostalCode = tbPostalCode.Value;
            rv.CurrentCardPostalCode = tbCurrentPostalCode.Value;

            ApplicantManager.UpdateApplicant(rv);

            return rv;
        }

        protected void nextbutton_ServerClick(object sender, EventArgs e)
        {
            try
            {
                Applicants applicant = saveForm();

                Response.Redirect ("educationInfoList.aspx?code=" + applicant.Code);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void odsProfile_Updated(object sender, ObjectDataSourceStatusEventArgs e)
        {
            if (e.Exception == null)
            {
                Applicants applicant = e.ReturnValue as Applicants;
                Response.Redirect (MainCompany.ATSBaseUrl + "applicant/educationInfoList.aspx?code=" + applicant.Code);
            }
        }
    }
}