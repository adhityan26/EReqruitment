﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/applicant.Master" AutoEventWireup="true" CodeBehind="changepassword.aspx.cs" Inherits="ERecruitment.changepassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<script type="text/javascript"> 
</script> 

<div style="float:left;margin:5px; margin-top:14px; text-align:center; padding:15px; width:100%; height:100%;">
    <div style="float:left;text-align:center;width:100%; height:100%;">
        <h5 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px; text-align:center">
            Your default password is "<mark>12345</mark>"
        </h5>
    </div>
    <div style="float:left;text-align:center;width:100%; height:100%;">
        <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px; text-align:center">
            Password:
        </h1>
        <div class="form-group col-md-6 col-md-offset-3">
            <input class="form-control" type="password" required="required" name="password" id="tbNewPassword" runat="server" placeholder="New Password" />
        </div>
    </div>
    <div style="float:left;text-align:center; width:100%; height:100%;">
            <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px; text-align:center">
                Confirm your password:
            </h1>
        <div class="form-group col-md-6 col-md-offset-3">
            <input class="form-control" type="password" required="required" name="password" id="tbConfirmPassword" runat="server"
                 placeholder="Confirm Password" />
        </div>
    </div>
    <div class="col-md-6 col-md-offset-3">
        <button class="btn btn-primary col-xs-12" id="btnResend" runat="server" onserverclick="btnResend_ServerClick">
            Change Password
        </button>
    </div>
</div>
</asp:Content>
