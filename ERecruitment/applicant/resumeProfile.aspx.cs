﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SS.Web.UI;
using ERecruitment.Domain;
namespace ERecruitment
{
    public partial class resumeProfile: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Applicants checkApplicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("code"));
                if (checkApplicant.Photo != null)
                    imgfoto.Src = "~/system/httphandlers/HandlerApplicants.ashx?commandName=GetApplicantPhoto&code=" + Utils.GetQueryString<string>("code");
                else
                    imgfoto.Src = "~/system/img/photodefault.gif";
            }
            if (Request.Form["__EVENTARGUMENT"] == "nextbutton")
            {
                // Fire event
                nextbutton_ServerClick(this, new EventArgs());
            }
        }

        protected void nextbutton_ServerClick(object sender, EventArgs e)
        {
            string process = hidAttachment.Value;
            if (!string.IsNullOrEmpty(Utils.GetQueryString<string>("code")))
            {
                Applicants getApplicant = ApplicantManager.GetApplicant(Utils.GetQueryString<string>("code"));

                if (getApplicant != null)
                {

                    if (fuPhoto.HasFile)
                    {
                        getApplicant.FileType = fuPhoto.PostedFile.ContentType;
                        getApplicant.FileName = fuPhoto.PostedFile.FileName;
                        getApplicant.Photo = Utils.GetFile(fuPhoto);

                        ApplicantManager.UpdateApplicant(getApplicant);

                    }
                    if (fuCV.HasFile)
                    {

                        getApplicant.CVFileType = fuCV.PostedFile.ContentType;
                        getApplicant.CVFileName = fuCV.PostedFile.FileName;
                        getApplicant.CV = Utils.GetFile(fuCV);
                        ApplicantManager.UpdateApplicant(getApplicant);

                    }

                    if (fuOther.HasFile)
                    {

                        ApplicantAttachments otherAttachment = new ApplicantAttachments();
                        otherAttachment.FileType = fuOther.PostedFile.ContentType;
                        otherAttachment.FileName = fuOther.PostedFile.FileName;
                        otherAttachment.Attachment = Utils.GetFile(fuOther);
                        otherAttachment.ApplicantCode = getApplicant.Code;
                        otherAttachment.InsertStamp = DateTime.Now;
                        ERecruitmentManager.SaveApplicantAttachment(otherAttachment);


                    }
                    string url ="resumeProfile.aspx?code=" + getApplicant.Code;
                    JQueryHelper.InvokeJavascript("showNotification('Data saved');window.close();window.location.href=' " + url + "';", Page);

                }
            }
        }
    }
}