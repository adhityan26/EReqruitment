﻿<%@ Page Title="My Applications" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="myapplicationform.aspx.cs" Inherits="ERecruitment.myapplicationform" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <script type="text/javascript">


    $(document).ready(function () {

        initiateList();

    });

    function initiateList() {

        $('#listContainer').easyPaginate({
            paginateElement: "div.jobSection",
            elementsPerPage: 5,
            effect: 'climb',
            firstButton: false,
            lastButton: false,
            prevButtonText: "Previous",
            nextButtonText: "Next"
        });
    }

    </script>
    
    <div id="listContainer">
    <asp:Repeater ID="repJobList"
                    runat="server"
                    DataSourceID="odsJobList"
                    OnItemDataBound="repJobList_ItemDataBound"
                    >
        <ItemTemplate>
            <div class="row jobSection">                    
                <div class="col-md-4 col-sm-4">
                    <img id="imgLogo" runat="server" alt=""/>
                </div>                    
                <div class="col-md-8 col-sm-8">
                    <div class="block"><strong><%# Eval("VacancyPositionName") %></strong></div>
                    <div class="block"><i class="fa fa-building"></i><%# Eval("VacancyCompanyName") %></div>
                    <div class="block"><i class="fa fa-map-marker"></i><%# Eval("VacancyCityName") %></div>
                    <div class="block"><i class="fa fa-clock-o"></i>Applied On: <%# Eval("AppliedDate","{0:dd, MMM yyyy}") %></div>
                    <div class="block"><i class="fa fa-file"></i>Current Status: <%# Eval("CandidateStatusLabel") %></div>
                    <div class="block"><i class="fa fa-clock-o"></i>Last Update: <%# Eval("UpdateStamp","{0:dd, MMM yyyy}") %></div>
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    </div>
    
<asp:ObjectDataSource ID="odsJobList"
                      runat="server"
                      TypeName="ERecruitment.Domain.ApplicantManager"
                      DataObjectTypeName="ERecruitment.Domain.Vacancies"
                      SelectMethod="GetApplicantActiveVacancies"
                      >
    <SelectParameters>
        <asp:Parameter Name="applicantCode" />
    </SelectParameters>
</asp:ObjectDataSource>

</asp:Content>
