﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class changepassword: AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnResend_ServerClick(object sender, EventArgs e)
        {
            UserAccounts userAccount = Session["currentuser"] as UserAccounts;
            Applicants getApplicant = ApplicantManager.GetApplicant(userAccount.Code);
            //if (tbOldPassword .Value != userAccount.Password)
            //{
            //    JQueryHelper.InvokeJavascript("showNotification('Invalid password');", Page);
            //    return;
            //}
            if (tbNewPassword.Value != tbConfirmPassword.Value)
            {
                JQueryHelper.InvokeJavascript("showNotification('New password and confirm password do not match');", Page);
                return;
            }
            userAccount.Password = tbNewPassword.Value;
            AuthenticationManager.UpdateUserAccount(userAccount);
            if (userAccount.IsApplicant)
                JQueryHelper.InvokeJavascript("showNotification('Password Updated'); window.location.href='" + MainCompany.ATSBaseUrl + "public/joblist.aspx';", Page);
            else if (userAccount.IsAdmin)
                JQueryHelper.InvokeJavascript("showNotification('Password Updated'); window.location.href='" + MainCompany.ATSBaseUrl + "recruiter/dashboard.aspx';", Page);

        }
    }
}