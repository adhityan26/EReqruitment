﻿<%@ Page Title="" Language="C#" MasterPageFile="~/masters/portal.Master" AutoEventWireup="true" CodeBehind="emailverificationcompleted.aspx.cs" Inherits="ERecruitment.emailverificationcompleted" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    
    <script type="text/javascript">

        var appMode;
        function getAppMode()
        {
            var handlerUrl = "../handlers/HandlerVacancies.ashx?commandName=GetAppMode";
            $.ajax({
                url: handlerUrl,
                async: false,
                beforeSend: function () {
                },
                success: function (queryResult) {
                    appMode = queryResult;
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification(xhr.responseText);

                }
            });
        }


        function showNotif()
        {
            getAppMode();
            if (appMode == "Single")
            {
                $('#appModeSingleFrameModel').modal('show');
                    
            } else if (appMode == "Multi") {
                $('#appModeMultiFrameModel').modal('show');
            }
        }

    </script>
<div style="float:left;margin:5px; margin-top:14px; text-align:center; padding:15px; width:100%; height:100%;">
    <h1 style="font-family:'Verdana',Arial,Arial, Helvetica, sans-serif;font-size:18px;">
        Verification completed, Click <span>Here</span> to continue.</h1>
</div>

             <div class="modal fade" id="appModeSingleFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title center">Information</h1>
                </div>
                <div class="modal-body" id="informationContainer1" style="height:400px ; overflow-y:auto;">
                    <asp:Literal ID="litInformationSingle" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onserverclick="btnSubmit_ServerClick" runat="server" >Ok</button>
                </div>
            </div>
        </div>
    </div>

         <div class="modal fade" id="appModeMultiFrameModel">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1 class="modal-title center">Information</h1>
                </div>
                <div class="modal-body" id="informationContainer2" style="height:400px ; overflow-y:auto;">
                    <asp:Literal ID="litInformationMulti" runat="server" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" onserverclick="btnSubmit_ServerClick" runat="server">Ok</button>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
