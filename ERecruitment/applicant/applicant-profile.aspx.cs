﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    public partial class applicant_profile1 : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                odsJobList.SelectParameters["applicantCode"].DefaultValue = CurrentUser.ApplicantCode;

                hidCurrentApplicantCode.Value = CurrentUser.ApplicantCode;

                ddNationality.DataSource = ERecruitmentManager.GetNationalityList();
                ddNationality.DataBind();
                ddNationality.Items.Insert(0, new ListItem("Select an option", ""));

                ddCity.DataSource = ERecruitmentManager.GetCityList();
                ddCity.DataBind();
                ddCity.Items.Insert(0, new ListItem("Select an option", ""));

                ddEducationCountry.DataSource = ERecruitmentManager.GetCountryList();
                ddEducationCountry.DataBind();
                ddEducationCountry.Items.Insert(0, new ListItem("Select an option", ""));

                ddEducationCity.DataSource = ERecruitmentManager.GetCityList();
                ddEducationCity.DataBind();
                ddEducationCity.Items.Insert(0, new ListItem("Select an option", ""));

                ddEducationLevel.DataSource = ERecruitmentManager.GetEducationLevels();
                ddEducationLevel.DataBind();
                ddEducationLevel.Items.Insert(0, new ListItem("Select an option", ""));

                ddFieldOfStudy.DataSource = ERecruitmentManager.GetFieldOfStudyList();
                ddFieldOfStudy.DataBind();
                ddFieldOfStudy.Items.Insert(0, new ListItem("Select an option", ""));

                //ddUniversity.DataSource = ERecruitmentManager.GetUniversityList();
                //ddUniversity.DataBind();
                //ddUniversity.Items.Insert(0, new ListItem("Select an option", ""));

                ddMajor.DataSource = ERecruitmentManager.GetEducationMajorList();
                ddMajor.DataBind();
                ddMajor.Items.Insert(0, new ListItem("Select an option", ""));
            }
        }
        
        protected void ddDatabound(object sender, EventArgs e)
        {
            DropDownList dd = sender as DropDownList;
            if (dd != null)
            {
                dd.Items.Insert(0, new ListItem("All", string.Empty));
            }
        }


        protected void ddEducationLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddFieldOfStudy.DataSource = ERecruitmentManager.GetEducationLevels();
            ddFieldOfStudy.DataBind();
            ddFieldOfStudy.Items.Insert(0, new ListItem("Select an option", ""));

        }
        //protected void ddEducationLevel_sele


    }
}