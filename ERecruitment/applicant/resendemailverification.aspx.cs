﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Text;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class resendemailverification: BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            { }
        }

        protected void btnResend_ServerClick(object sender, EventArgs e)
        {

            //UserAccounts userAccount = Session["unverifieduser"] as UserAccounts;
            Applicants applicant = ERecruitment.Domain.ApplicantManager.GetApplicantByEmail(tbEmail.Value);
            string loginUrl =  "../public/login.aspx";
            if (applicant != null)
            {
                if (applicant.Verified)
                {
                    JQueryHelper.InvokeJavascript("showNotification('Your email is already registered');window.close();window.location.href='" + loginUrl + "';", Page);
                    return;
                }
                if (Utils.ConvertString<bool>(ConfigurationManager.AppSettings["EnableEmail"]))
                {
                    Companies emailSetting = ERecruitmentManager.GetCompany(MainCompany.Code);
                    SmtpClient client = new SmtpClient();
                    client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                    client.Host = emailSetting.EmailSmtpHostAddress;
                    if (emailSetting.EmailSmtpUseSSL)
                        client.EnableSsl = true;
                    client.Timeout = 20000;
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                    string address = emailSetting.EmailSmtpEmailAddress;
                    string displayName = emailSetting.MailSender;

                    if (!address.Contains("@"))
                    {
                        address = displayName;
                    }
            
                    MailMessage mm = new MailMessage(new MailAddress(address,
                            displayName),
                                                                     new MailAddress(applicant.Email, ""));
                    mm.Subject = "Email verification from " + emailSetting.ApplicationTitle;
                    mm.Body = "Click this <a href='[link]'>link</a> to verify account.";
                    string link = MainCompany.ATSBaseUrl  +"/applicant/emailverificationcompleted.aspx?code=" + applicant.Code;
                    mm.Body = mm.Body.Replace("[link]", link);
                    mm.BodyEncoding = UTF8Encoding.UTF8;
                    mm.IsBodyHtml = true;
                    mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                    client.Send(mm);
                    

                    JQueryHelper.InvokeJavascript("showNotification('Email sent, please check your email');window.close();window.location.href='" + loginUrl + "';", Page);
                }
            }
            else
            {
                string registeredPage = "../public/registration-form.aspx";
                JQueryHelper.InvokeJavascript("showNotification('Email is not registered');window.close();window.location.href = '" + registeredPage + "';", Page);
            }
        }
    }
}