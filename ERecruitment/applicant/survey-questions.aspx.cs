﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ERecruitment.Domain;
using SS.Web.UI;

namespace ERecruitment
{
    public partial class survey_questions : AuthorizedPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidSurveyIds.Value = MainCompany.SurveyQuestionCodes;
                hidCurrentApplicantCode.Value = CurrentUser.ApplicantCode;
                hidCompanyCode.Value = MainCompany.Code;
                hidVacancyCode.Value = Utils.GetQueryString<string>("vacancyCode");
            }
        }

    }
}