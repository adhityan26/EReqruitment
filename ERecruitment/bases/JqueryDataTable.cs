using System;
using System.Data;
using System.Configuration;
using System.Collections.Generic;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace ERecruitment
{
    
    public class JqueryTagEditItemContainer
    {
        public JqueryTagEditItemContainer(string value, string display, string caption)
        {
            Value = value;
            Label = display;
            Id = caption;
            this.display = display;
        }

        public string Id;
        public string Label;
        public string Value;
        public string display;
    }

    public class JqueryDataTable
    {
        private int _echo;
        private int _totalrecords;
        private List<string> _data = new List<string>();

        public static string iDisplayStart = "iDisplayStart";
        public static string iDisplayLength = "iDisplayLength";
        public static string iSortingCols = "iSortCol_0";
        public static string sSearch = "sSearch";
        public static string sEcho = "sEcho";
        public static string sSortDir = "sSortDir_0";


        public JqueryDataTable(List<string> dataList, int sEcho, int totalRecords)
        {
            _data = dataList;
            _echo = sEcho;
            _totalrecords = totalRecords;
        }

        public string ToJson()
        {
            string json = @"
                            {
                              'sEcho': [echo],
                              'iTotalRecords': '[totalrecords]',
                              'iTotalDisplayRecords': '[totaldisplayrecords]',
                              'aaData': [[data]]
                            }
            
                            ";
            json = json.Replace("[echo]", _echo.ToString())
                       .Replace("[totalrecords]", _totalrecords.ToString())
                       .Replace("[totaldisplayrecords]", _totalrecords.ToString())
                       .Replace("[data]", string.Join(",", _data.ToArray()));

            return json.Replace(Environment.NewLine, "").Replace("'", "\"");
        }

        public static string StripData(string data)
        {
            if (!string.IsNullOrEmpty(data))
                return string.Concat("\"", data.Replace(Environment.NewLine, " ").Replace("\\", "\\\\").Replace("\n", " ").Replace("\"", "\\\"").Replace("'", ""), "\"");
            else
                return string.Concat("\"", "-", "\"");
        }
    }
}
