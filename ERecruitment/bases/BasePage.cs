﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Configuration;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.WebControls;


namespace ERecruitment
{
    public class BasePage : System.Web.UI.Page
    {
        public UserAccounts CurrentUser
        {
            get
            {
                return Session["currentuser"] as UserAccounts;
            }
        }

        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (Session["maincompany"] != null)
                    mainCompany = Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    Uri myUri = new Uri(Request.Url.ToString());
                    string host = myUri.Host;
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(host);
                    if (mainCompany == null)
                        Response.Redirect("~/underconstruction.html");
                    Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public Roles MainRole
        {
            get
            {
                if (Session["mainrole"] != null)
                    return Session["mainrole"] as Roles;
                else
                    return new Roles();
            }
        }

    }
}