﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Configuration;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.WebControls;


namespace ERecruitment
{
    public class BaseMasterPage : System.Web.UI.MasterPage
    {
        public UserAccounts CurrentUser
        {
            get
            {
                return Session["currentuser"] as UserAccounts;
            }
        }

        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (Session["maincompany"] != null)
                    mainCompany = Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    Uri myUri = new Uri(Request.Url.ToString());
                    string host = myUri.Host;
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(host);
                    if (mainCompany == null)
                        Response.Redirect("~/home.html");
                    Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }

        public Roles MainRole
        {
            get
            {
                if (Session["mainrole"] != null)
                    return Session["mainrole"] as Roles;
                else
                    return new Roles();
            }
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            if (!IsPostBack)
            {
                Page.Title = Page.Title + " - " + MainCompany.ApplicationTitle;

                Literal litUserName = this.FindControl("litUserName") as Literal;
                if (litUserName != null)
                {
                    if (CurrentUser != null)
                        litUserName.Text = CurrentUser.Name;
                }

                #region themes config

                Literal litCssPlaceHolder = this.FindControl("litCssPlaceHolder") as Literal;
                if (litCssPlaceHolder != null)
                {
                    litCssPlaceHolder.Text = "<style type=\"text/css\">" + MainCompany.CssContent + "</style>";
                }

                Literal litCopyrightText = this.FindControl("litCopyrightText") as Literal;
                if (litCopyrightText != null)
                {
                    litCopyrightText.Text = DateTime.Now.Year.ToString() + " " + MainCompany.CopyrightText;
                }

                #endregion

                #region social config

                // linked in
                Literal litLinkedAPIPlaceHolder = this.FindControl("litLinkedAPIPlaceHolder") as Literal;
                if (litLinkedAPIPlaceHolder != null)
                {
                    if (!string.IsNullOrEmpty(MainCompany.LinkedINAPIKey) && MainCompany.LinkedINAPIKey != "-")
                        litLinkedAPIPlaceHolder.Text = @"<script type='text/javascript' src='//platform.linkedin.com/in.js'>api_key: " + MainCompany.LinkedINAPIKey + @"</script>";
                }

                // facebook
                Literal litFacebookAPIPlaceHolder = this.FindControl("litFacebookAPIPlaceHolder") as Literal;
                if (litFacebookAPIPlaceHolder != null)
                {
                    if (!string.IsNullOrEmpty(MainCompany.FacebookAPIKey) && MainCompany.FacebookAPIKey != "-")
                    {
                        litFacebookAPIPlaceHolder.Text = @"<div id='fb-root'></div>
                                                          <script>
                                                            window.fbAsyncInit = function() {
                                                                            FB.init({
                                                                                appId: '[facebookkey]',
                                                                                xfbml: true,
                                                                                version: 'v2.10'
                                                                            });
                                                                        };

                                                                        (function(d, s, id){
                                                                            var js, fjs = d.getElementsByTagName(s)[0];
                                                                            if (d.getElementById(id)) { return; }
                                                                            js = d.createElement(s); js.id = id;
                                                                            js.src = '//connect.facebook.net/en_US/sdk.js';
                                                                            fjs.parentNode.insertBefore(js, fjs);
                                                                        }
                                                                        (document, 'script', 'facebook-jssdk'));
                                                        </script> ";
                        litFacebookAPIPlaceHolder.Text = litFacebookAPIPlaceHolder.Text.Replace("[facebookkey]", MainCompany.FacebookAPIKey);
                    }
                }


                #endregion

                executeRoleAccess();
                highlightMenu();
            }
        }

        private void executeRoleAccess()
        {
            string masterPageFile = System.IO.Path.GetFileName(Page.MasterPageFile);
            string pageFile = System.IO.Path.GetFileName(HttpContext.Current.Request.FilePath);
            Repeater repPages = this.FindControl("repPages") as Repeater;
            // applying main role access
            if (pageFile == "company-profile.aspx")
            {
                if (!MainRole.AccessCSSTheme)
                {
                    HtmlGenericControl liCssTheme = this.FindControl("ContentPlaceHolder1").FindControl("liCssTheme") as HtmlGenericControl;
                    if (liCssTheme != null)
                        liCssTheme.Visible = false;
                }
            }

            if (!MainRole.AccessSetup)
            {
                #region find setup menu, then hide them

                foreach (RepeaterItem menuItem in repPages.Items)
                {
                    HtmlAnchor menuLink = menuItem.FindControl("menuLink") as HtmlAnchor;
                    if (menuLink != null)
                    {
                        if (menuLink.Attributes["data-context"] == "Setup")
                            menuItem.Visible = false;
                    }

                    Repeater repSubPages = menuItem.FindControl("repSubPages") as Repeater;
                    if (repSubPages != null)
                    {
                        foreach (RepeaterItem pageItem in repSubPages.Items)
                        {
                            HtmlAnchor pageLink = pageItem.FindControl("menuLink") as HtmlAnchor;
                            if (pageLink != null)
                            {

                                // check if current page is nested under setup menu, if yes, then throw to un authorized page
                                if (pageLink.Attributes["data-context"] == "Setup")
                                {
                                    if (pageLink.HRef == HttpContext.Current.Request.Url.ToString())
                                        Response.Redirect("~/default.aspx");
                                }

                            }
                        }
                    }
                }

                #endregion
            }

            if (!MainRole.AccessSetting)
            {
                #region find setting menu, then hide them

                foreach (RepeaterItem menuItem in repPages.Items)
                {
                    HtmlAnchor menuLink = menuItem.FindControl("menuLink") as HtmlAnchor;
                    if (menuLink != null)
                    {
                        if (menuLink.Attributes["data-context"] == "Settings")
                            menuItem.Visible = false;
                    }

                    Repeater repSubPages = menuItem.FindControl("repSubPages") as Repeater;
                    if (repSubPages != null)
                    {
                        foreach (RepeaterItem pageItem in repSubPages.Items)
                        {
                            HtmlAnchor pageLink = pageItem.FindControl("menuLink") as HtmlAnchor;
                            if (pageLink != null)
                            {

                                // check if current page is nested under setup menu, if yes, then throw to un authorized page
                                if (pageLink.Attributes["data-context"] == "Settings")
                                {
                                    if (pageLink.HRef == HttpContext.Current.Request.Url.ToString())
                                        Response.Redirect("~/default.aspx");
                                }

                            }
                        }
                    }
                }

                #endregion
            }

            if (!MainRole.AccessReport)
            {
                #region find report menu, then hide them

                foreach (RepeaterItem menuItem in repPages.Items)
                {
                    HtmlAnchor menuLink = menuItem.FindControl("menuLink") as HtmlAnchor;
                    if (menuLink != null)
                    {
                        if (menuLink.Attributes["data-context"] == "Reports")
                            menuItem.Visible = false;
                    }

                    Repeater repSubPages = menuItem.FindControl("repSubPages") as Repeater;
                    if (repSubPages != null)
                    {
                        foreach (RepeaterItem pageItem in repSubPages.Items)
                        {
                            HtmlAnchor pageLink = pageItem.FindControl("menuLink") as HtmlAnchor;
                            if (pageLink != null)
                            {

                                // check if current page is nested under setup menu, if yes, then throw to un authorized page
                                if (pageLink.Attributes["data-context"] == "Reports")
                                {
                                    if (pageLink.HRef == HttpContext.Current.Request.Url.ToString())
                                        Response.Redirect("~/default.aspx");
                                }

                            }
                        }
                    }
                }

                #endregion
            }

            if (!MainRole.AccessApplicant)
            {
                #region find report menu, then hide them

                foreach (RepeaterItem menuItem in repPages.Items)
                {
                    HtmlAnchor menuLink = menuItem.FindControl("menuLink") as HtmlAnchor;
                    if (menuLink != null)
                    {
                        if (menuLink.Attributes["data-context"] == "Applicants")
                        {
                            menuItem.Visible = false;
                            if (menuLink.HRef == HttpContext.Current.Request.Url.ToString())
                                Response.Redirect("~/default.aspx");
                        }
                    }

                    Repeater repSubPages = menuItem.FindControl("repSubPages") as Repeater;
                    if (repSubPages != null)
                    {
                        foreach (RepeaterItem pageItem in repSubPages.Items)
                        {
                            HtmlAnchor pageLink = pageItem.FindControl("menuLink") as HtmlAnchor;
                            if (pageLink != null)
                            {

                                // check if current page is nested under setup menu, if yes, then throw to un authorized page
                                if (pageLink.Attributes["data-context"] == "Applicants")
                                {
                                    if (pageLink.HRef == HttpContext.Current.Request.Url.ToString())
                                        Response.Redirect("~/default.aspx");
                                }

                            }
                        }
                    }
                }

                #endregion
            }

            if (pageFile == "job-requisition-list.aspx")
            {
                #region turn on/off

                if (!MainRole.CreateJob)
                {
                    HtmlButton btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnCreateJob") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;
                }

                if (!MainRole.PostUnPostJob)
                {
                    HtmlButton btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnPostJob") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;

                    btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnUnPostJob") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;
                }

                if (!MainRole.ArchiveJob)
                {
                    HtmlButton btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnArchiveJob") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;
                }

                if (!MainRole.DeleteJob)
                {
                    HtmlButton btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnDeleteJob") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;
                }

                #endregion
            }

            if (pageFile == "job-requisition-form.aspx")
            {
                if (!MainRole.CreateJob)
                {
                    Response.Redirect("~/default.aspx");
                }
            }

            if (pageFile == "detail-application.aspx")
            {
                Repeater repApplicantList = this.FindControl("ContentPlaceHolder1").FindControl("repApplicantList") as Repeater;
                foreach (RepeaterItem repeaterItem in repApplicantList.Items)
                {
                    HtmlGenericControl commandTestOffering = repeaterItem.FindControl("commandTestOffering") as HtmlGenericControl;
                    HtmlGenericControl commandSocial = repeaterItem.FindControl("commandSocial") as HtmlGenericControl;
                    HtmlGenericControl commandAction = repeaterItem.FindControl("commandAction") as HtmlGenericControl;
                    HtmlGenericControl flagPanel = repeaterItem.FindControl("flagPanel") as HtmlGenericControl;

                    if (!MainRole.OfferingFunctionality)
                    {
                        JQueryHelper.InvokeJavascript("hideElement('.btnIssueOffering')", Page);
                    }

                    if (!MainRole.SchedulingFunctionality)
                    {
                        JQueryHelper.InvokeJavascript("hideElement('.btnIssueInvitation')", Page);
                    }

                    if (!MainRole.ReferFunctionality)
                    {
                        JQueryHelper.InvokeJavascript("hideElement('.btnReferApplicant')", Page);
                    }

                    if (!MainRole.FlagFunctionality)
                    {
                        flagPanel.Visible = false;
                    }

                    if (!MainRole.DisqualifyFunctionality)
                    {
                        JQueryHelper.InvokeJavascript("hideElement('.btnDisqualify')", Page);
                    }

                    if (!MainRole.ChangeApplicantStatus)
                    {
                        JQueryHelper.InvokeJavascript("hideElement('.btnIssueComments')", Page);
                        JQueryHelper.InvokeJavascript("hideElement('.btnTestResult')", Page);
                    }
                }

                if (!MainRole.ChangeApplicantStatus)
                {
                    HtmlButton btnProceeedStep = this.FindControl("ContentPlaceHolder1").FindControl("btnProceeedStep") as HtmlButton;
                    btnProceeedStep.Visible = false;
                }
            }

            if (pageFile == "dashboard.aspx")
            {
                if (!MainRole.DownloadDashboardData)
                {
                    HtmlButton btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnDownloadPieData") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;

                    btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnDownloadBarData") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;

                    btnAction = this.FindControl("ContentPlaceHolder1").FindControl("btnDownloadSurveyData") as HtmlButton;
                    if (btnAction != null)
                        btnAction.Visible = false;
                }
            }

            if (pageFile == "batchprocess.aspx")
            {
                if (!MainRole.ChangeApplicantStatus)
                {
                    Response.Redirect("~/default.aspx");
                }
            }

            if (pageFile == "vacancy-rank-qualifications.aspx")
            {
                if (!MainRole.RankFunctionality)
                {
                    Response.Redirect("~/default.aspx");
                }
            }
        }

        private void highlightMenu()
        {
            HtmlAnchor staticLink = this.FindControl("staticLink") as HtmlAnchor;
            if (staticLink != null)
            {
                if (staticLink.HRef == HttpContext.Current.Request.Url.ToString())
                    JQueryHelper.InvokeJavascript("highlightNavigationContainer(\"" + staticLink.ClientID + "\");", Page);
            }

            Repeater repPages = this.FindControl("repPages") as Repeater;
            foreach (RepeaterItem menuItem in repPages.Items)
            {
                HtmlAnchor menuLink = menuItem.FindControl("menuLink") as HtmlAnchor;
                Repeater repSubPages = menuItem.FindControl("repSubPages") as Repeater;
                if (repSubPages != null)
                {
                    foreach (RepeaterItem pageItem in repSubPages.Items)
                    {
                        HtmlAnchor pageLink = pageItem.FindControl("menuLink") as HtmlAnchor;
                        if (pageLink != null)
                        {
                            if (pageLink.HRef == HttpContext.Current.Request.Url.ToString())
                            {
                                // highlight page link and menu
                                JQueryHelper.InvokeJavascript("highlightNavigationContainer(\"" + menuLink.ClientID + "\");", Page);
                                JQueryHelper.InvokeJavascript("highlightNavigationContainer(\"" + pageLink.ClientID + "\");", Page);
                            }
                        }
                    }
                }
                if (menuLink != null)
                {
                    if (menuLink.HRef == HttpContext.Current.Request.Url.ToString())
                        JQueryHelper.InvokeJavascript("highlightNavigationContainer(\"" + menuLink.ClientID + "\");", Page);
                }
            }
        }

    }
}