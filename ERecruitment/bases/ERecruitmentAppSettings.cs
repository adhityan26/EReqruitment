﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Globalization;
using ERecruitment.Domain;
using System.Net;
using System.Net.Mail;
using System.Runtime.Remoting.Messaging;
using SS.Web.UI;
using System.Text;
using System.Text.RegularExpressions;
using ERecruitment.Domain.Helper;


namespace ERecruitment
{
    public static class ERecruitmentAppSettings
    {
        public static string BaseUrl = ConfigurationManager.AppSettings["BaseURL"];
        public static string applicationMode = ConfigurationManager.AppSettings["ApplicationMode"];

        #region email
        public static void SendEmail(Companies emailSetting, string emailDestination, string subject, string body)
        {
            SendEmail(emailSetting, emailDestination, subject, body, false);
        }
        
        public static void SendEmail(Companies emailSetting, string emailDestination, string subject, string body, Boolean htmlFormat)
        {
            Mail.SendEmail(emailSetting, emailDestination, subject, body, htmlFormat);
        }

        public static void SendApplicantUpdateStatus(ApplicantVacancies application)
        {
            Companies company = ERecruitmentManager.GetCompany(application.CompanyCode);
            
            Vacancies vacancy = ERecruitmentManager.GetVacancy(application.VacancyCode);
            Pipelines pipeline = HiringManager.GetPipeline(vacancy.PipelineCode);

            List<PipelineSteps> pipelineStepList = HiringManager.GetPipelineStepList(vacancy.PipelineCode);
            pipelineStepList.Add(new PipelineSteps(pipeline.AutoDisqualifiedRecruitmentStatusCode,
                pipeline.AutoDisqualifiedRecruitmentStatusEmailTemplateId));
            pipelineStepList.Add(new PipelineSteps(pipeline.ReferedDisqualifiedRecruitmentStatusCode,
                pipeline.ReferedDisqualifiedRecruitmentStatusEmailTemplateId));
            pipelineStepList.Add(new PipelineSteps(pipeline.HiredRecruitmentStatusCode,
                pipeline.HiredRecruitmentStatusEmailTemplateId));
            pipelineStepList.Add(new PipelineSteps(pipeline.DisqualifiedRecruitmentStatusCode,
                pipeline.DisqualifiedRecruitmentStatusEmailTemplateId));

            PipelineSteps recruitmentPipeline = pipelineStepList.Find(delegate(PipelineSteps temp)
            {
                return temp.StatusCode == application.Status;
            });

            if (recruitmentPipeline == null)
            {
                throw new ApplicationException("Missing pipeline step");   
            } 
                
            Emails emailTemplate = ERecruitmentManager.GetEmail(recruitmentPipeline.ApplicantEmailTemplateId);
            
            if (emailTemplate == null)
                throw new ApplicationException("Missing email template");
            
            // compose message body
            VacancyStatus recruitmentProcess = ERecruitmentManager.GetVacancyStatus(application.Status);
            Dictionary<String, String> data = new Dictionary<string, string>
            {
                {"[Name]", application.ApplicantName},
                {"[Site]", company.ATSSite + "/public/joblist.aspx"},
                {"[Status]", recruitmentProcess.CandidateLabel},
                {"[Title]", application.VacancyPositionName},
                {"[Company]", company.Name}
            };
            string body = Mail.EmailParser(emailTemplate.Body, data);

            string subject = emailTemplate
                .Subject
                .Replace("[Title]", application.VacancyPositionName)
                .Replace("[Company]", company.Name);
            // send email

            SendEmail(company, application.ApplicantEmail, subject, body, true);
        }

        public static void SendApplicantOfferingEmail(ApplicantVacancyOffering applicantOffering, int emailTemplateId)
        {
            ApplicantVacancies application = VacancyManager.GetApplicantVacancy(applicantOffering.VacancyCode, applicantOffering.ApplicantCode);
            
            Companies company = ERecruitmentManager.GetCompany(application.CompanyCode);
            // get email template
            Emails emailTemplate = ERecruitmentManager.GetEmail(emailTemplateId);
            if (emailTemplate == null)
                return;
            // compose message body
//            VacancyStatus recruitmentProcess = ERecruitmentManager.GetVacancyStatus(application.Status);

            Dictionary<String, String> data = new Dictionary<string, string>
            {
                {"[Name]", application.ApplicantName},
                {"[MonthlySalary]", Utils.DisplayMoneyAmount(applicantOffering.MonthlySalary)},
                {"[THR]", applicantOffering.EligibleForTHR.ToString(CultureInfo.CurrentCulture)},
                {"[Incentive]", applicantOffering.EligibleForIncentive.ToString(CultureInfo.CurrentCulture)},
                {"[Bonus]", applicantOffering.EligibleForBonus.ToString(CultureInfo.CurrentCulture)},
                {"[Company]", company.Name},
                {"[Title]", application.VacancyPositionName},
                {"[Notes]", applicantOffering.Notes}
            };

            String emailBody = Mail.EmailParser(emailTemplate.Body, data);

            string subject = emailTemplate
                .Subject
                .Replace("[Title]", application.VacancyPositionName)
                .Replace("[Company]", company.Name);
            
            // send email

            SendEmail(company, application.ApplicantEmail, subject, emailBody, true);
        }

        public static void SendApplicantReferer(ApplicantVacancies application, string[] listCompany, int emailTemplateId)
        {
            List<string> listCompanyName = new List<string>(); 
            // get email template
            Emails emailTemplate = ERecruitmentManager.GetEmail(emailTemplateId);
            if (emailTemplate == null)
            {
                return;   
            }
            Companies company = ERecruitmentManager.GetCompany(application.CompanyCode);

            foreach (string companyCode in listCompany)
            {
                Companies companyRefer = ERecruitmentManager.GetCompany(companyCode);
                if (companyRefer != null)
                {
                    listCompanyName.Add(companyRefer.Name);   
                }
            }
            // compose message body
            
            Dictionary<String, String> data = new Dictionary<string, string>
            {
                {"[Name]", application.ApplicantName},
                {"[Site]", company.ATSSite + "/public/joblist.aspx"},
                {"[RefererCompany]", String.Join(", ", listCompanyName.ToArray())},
                {"[Title]", application.VacancyPositionName},
                {"[Company]", company.Name}
            };
            string body = Mail.EmailParser(emailTemplate.Body, data);

            string subject = emailTemplate
                .Subject
                .Replace("[Title]", application.VacancyPositionName)
                .Replace("[Company]", company.Name);
            // send email

            SendEmail(company, application.ApplicantEmail, subject, body, true);
        }

        public static void SendApplicantInvitationEmail(ApplicantVacancySchedules applicantInvitation, int emailTemplateId)
        {
            ApplicantVacancies application = VacancyManager.GetApplicantVacancy(applicantInvitation.VacancyCode, applicantInvitation.ApplicantCode);

            Companies company = ERecruitmentManager.GetCompany(application.CompanyCode);
            // get email template
            Emails emailTemplate = ERecruitmentManager.GetEmail(emailTemplateId);
            if (emailTemplate == null)
                return;
            // compose message body

            string approveParam = "id=" + applicantInvitation.Id.ToString() + "&response=approve";
            string reviseParam = "id=" + applicantInvitation.Id.ToString() + "&response=revise";
            string rejectParam = "id=" + applicantInvitation.Id.ToString() + "&response=reject";
            
            string approveLink = HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + "/applicant/confirmation.aspx?_x=" + Encriptor.Encrypt(approveParam) + "&type=schedulling";
            string reviseLink = HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + "/applicant/confirmation.aspx?_x=" + Encriptor.Encrypt(reviseParam) + "&type=schedulling";
            string rejectLink = HttpContext.Current.Request.Url.Scheme + Uri.SchemeDelimiter + HttpContext.Current.Request.Url.Host + "/applicant/confirmation.aspx?_x=" + Encriptor.Encrypt(rejectParam) + "&type=schedulling";
            string approve = @"<a href='" + approveLink + "'>Approve</a>";
            string revise = @"<a href='" + reviseLink + "'>Reschedule</a>";
            string reject = @"<a href='" + rejectLink + "'>Reject</a>";
//            VacancyStatus recruitmentProcess = ERecruitmentManager.GetVacancyStatus(application.Status);

            Dictionary<String, String> data = new Dictionary<string, string>
            {
                {"[Name]", application.ApplicantName},
                {"[Place]", applicantInvitation.Place},
                {"[Company]", company.Name},
                {"[Time Start]", applicantInvitation.TimeStart},
                {"[Time End]", applicantInvitation.TimeEnd},
                {"[PIC]", applicantInvitation.Pic},
                {"[Notes]", applicantInvitation.Notes},
                {"[Title]", application.VacancyPositionName},
                {"[Date]", Utils.DisplayDateTime(applicantInvitation.Date)},
                {"[Approve]", approve},
                {"[Revise]", revise},
                {"[Reject]", reject}
            };

            string body = Mail.EmailParser(emailTemplate.Body, data);
            // send email

            string subject = emailTemplate
                .Subject
                .Replace("[Title]", application.VacancyPositionName)
                .Replace("[Company]", company.Name);

            SendEmail(company, application.ApplicantEmail, subject, body, true);
        }

        public static void SendInvitationFeedbackEmail(ApplicantVacancySchedules applicantInvitation)
        {
            ApplicantVacancies application = VacancyManager.GetApplicantVacancy(applicantInvitation.VacancyCode, applicantInvitation.ApplicantCode);

            Companies company = ERecruitmentManager.GetCompany(application.CompanyCode);
            
            // get email template
            Emails emailTemplate = ERecruitmentManager.GetEmail(applicantInvitation.FeedbackInvitationEmail);
            if (emailTemplate == null)
            {
                return;   
            }

            Dictionary<String, String> data = new Dictionary<string, string>
            {
                {"[Name]", application.ApplicantName},
                {"[Company]", company.Name}
            };

            // compose message body
            string body = Mail.EmailParser(emailTemplate.Body, data);

            string subject = emailTemplate
                .Subject
                .Replace("[Title]", application.VacancyPositionName)
                .Replace("[Company]", company.Name);
            
            // send email
            SendEmail(company, application.ApplicantEmail, subject, body, true);
        }

        #endregion
    }
}