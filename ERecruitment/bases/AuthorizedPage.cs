﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Configuration;
using ERecruitment.Domain;
using SS.Web.UI;
using System.Web.UI.WebControls;


namespace ERecruitment
{
    public class AuthorizedPage : System.Web.UI.Page
    {
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if(!IsPostBack)
            {
                if (CurrentUser == null)
                {
                    String param = "";
                    if (!String.IsNullOrEmpty(Utils.GetQueryString<string>("openLogin")))
                    {
                        param = "?openLogin=true";
                        String nextPage = "";

                        foreach (string queryKey in Request.QueryString.AllKeys)
                        {
                            if (queryKey != "openLogin")
                            {
                                nextPage += (nextPage.Length == 0 ? "?" : "&") + queryKey + "=" + Request.QueryString[queryKey];   
                            }
                        }

                        String url = Request.Url.AbsolutePath.ToString();
                        nextPage = url.Substring(1) + nextPage;
                        
                        HttpContext.Current.Session["nextPage"] = nextPage;
                    }
                    Response.Redirect("../default.aspx" + param);   
                }
            }
        }

        public UserAccounts CurrentUser
        {
            get
            {
                return Session["currentuser"] as UserAccounts;
            }
        }

        public Companies MainCompany
        {
            get
            {
                Companies mainCompany = null;
                if (Session["maincompany"] != null)
                    mainCompany = Session["maincompany"] as Companies;

                if (mainCompany == null)
                {
                    Uri myUri = new Uri(Request.Url.ToString());
                    string host = myUri.Host;
                    mainCompany = ERecruitment.Domain.ERecruitmentManager.GetMainCompany(host);
                    if (mainCompany == null)
                        Response.Redirect("~/home.html");
                    Session["maincompany"] = mainCompany;
                }

                return mainCompany;
            }
        }


        public Roles MainRole
        {
            get
            {
                if (Session["mainrole"] != null)
                    return Session["mainrole"] as Roles;
                else
                    return new Roles();
            }
        }
    }
}