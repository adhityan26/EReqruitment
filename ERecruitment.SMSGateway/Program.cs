﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using GsmComm.PduConverter;
using GsmComm.GsmCommunication;
using GsmComm.Interfaces;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using ERecruitment.Domain;

namespace ERecruitment.SMSGateway
{
    class Program
    {
        static void Main(string[] args)
        {
            GsmCommMain comm = new GsmCommMain("COM3", 115200, 300);

            #region opening port

            try
            {
                comm.Open();
                Console.WriteLine("Port is opened.");
            }
            catch (CommException ex)
            {
                if (ex.InnerException != null)
                {
                    string iex = ex.InnerException.GetBaseException().ToString();
                    Console.WriteLine("Problem opening port! -- " + ex.Message + " | " + iex);
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Problem opening port! -- " + ex.Message);
                    Console.ReadLine();
                }
            }

            #endregion

            Console.WriteLine("Starting..");
            while (true)
            {

                int day = (int)DateTime.Now.DayOfWeek;
                int hour = DateTime.Now.Hour;

                byte dcs = (byte)DataCodingScheme.GeneralCoding.Alpha7BitDefault;
                DecodedShortMessage[] messages = null;
                
                #region get un sent message from database

                List<SMSMessages> unverifiedOutgoingMessageList = MessageManager.GetUnVerifiedOutgoingMessageList();
                foreach (SMSMessages item in unverifiedOutgoingMessageList)
                {
                    SmsSubmitPdu pdu = new SmsSubmitPdu(item.Message, item.RecipientNumber, dcs);
                    comm.SendMessage(pdu, true);
                    item.Verified = true;
                    MessageManager.Update(item);
                }

                #endregion

                // pause for 3 seconds
                Thread.Sleep(3 * 1000); //Only do this every 10 seconds
            }
        }
    }
}
