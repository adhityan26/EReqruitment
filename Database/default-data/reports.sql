INSERT [dbo].[Reports] ([Code], [Name], [ReportFile], [Mode]) VALUES (N'01', N'Job Requisitions', N'jobrequisitionlist.frx', NULL)
GO
INSERT [dbo].[Reports] ([Code], [Name], [ReportFile], [Mode]) VALUES (N'02', N'Candidate List', N'candidatelist.frx', NULL)
GO
INSERT [dbo].[Reports] ([Code], [Name], [ReportFile], [Mode]) VALUES (N'03', N'Number of Candidate Per Jobs', N'applicantjobsummary.frx', NULL)
GO
INSERT [dbo].[Reports] ([Code], [Name], [ReportFile], [Mode]) VALUES (N'04', N'Candidates Per Job (Details)', N'applicantjobdetails.frx', NULL)
GO
INSERT [dbo].[Reports] ([Code], [Name], [ReportFile], [Mode]) VALUES (N'05', N'Lead Time Report', N'applicantpipelineleadtimes.frx', NULL)
GO
INSERT [dbo].[Reports] ([Code], [Name], [ReportFile], [Mode]) VALUES (N'06', N'Applicant Logs', N'applicantloginlog.frx', NULL)
GO
INSERT [dbo].[Reports] ([Code], [Name], [ReportFile], [Mode]) VALUES (N'07', N'Job Req. Logs', N'jobrequisitionlog.frx', NULL)
GO
