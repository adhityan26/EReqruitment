delete Emails

/****** Object:  Table [dbo].[Emails]    Script Date: 05/07/2017 20:27:21 ******/
SET IDENTITY_INSERT [dbo].[Emails] ON
INSERT [dbo].[Emails] ([Id], [TemplateName], [Category], [Subject], [Body], [Active], [LogError]) VALUES (1, N'Update Status', N'ApplicantNotification', N'Application Status', N'Dear [Name],

Congratulations, you’re qualify to be at [Status] stage. We’ll inform you about the schedule soon.
Thank you.

Regards,
Recruiter', 1, N'Failure sending mail.')
INSERT [dbo].[Emails] ([Id], [TemplateName], [Category], [Subject], [Body], [Active], [LogError]) VALUES (2, N'Psychotest Invitation Applicant', N'ApplicantNotification', N'Email Invitation for Applicant', N'hi [Name],

thanks for your application to [Company]. we were impressed by your background and would like to invite you to interview to tell you a little more about the position and get to know you better. 
please let me know which of the following times work for you, and i can send over a confirmation and details:
- [Date] , [Time Start] - [Time End] at [Place]. with [PIC]
click [approve], if you accept the appointment and [revise] if you want to reschedule
note :
[Notes]

looking forward to meeting you. ', 1, NULL)
INSERT [dbo].[Emails] ([Id], [TemplateName], [Category], [Subject], [Body], [Active], [LogError]) VALUES (5, N'Interview Invitation for Recruiter', N'RecruiterNotification', N'Email Invitation for Recruiter', N'dear [PIC], an appointment has been set for you at  [Date], from [Time Start]  -  [Time End] at [Place]', 1, NULL)
INSERT [dbo].[Emails] ([Id], [TemplateName], [Category], [Subject], [Body], [Active], [LogError]) VALUES (6, N'Offering', N'ApplicantNotification', N'Offering', N'hi [Name],we have all really enjoyed speaking with you and getting to know you over the course of the last few weeks. the team and i have been impressed with your background and approach and would love to formally offer you a position as a [jobtitle] at [Company].we can offer you a [$x] annual base salary [bonus and equity information, if equitable]. we offer [benefits details] and [number of days] of vacation per year. we can discuss start dates based on what is possible on your end, but we’d be excited to have you start [as soon as possible / on xyz date].please let me know if you have any questions or would like to discuss the offer in more detail. we would be thrilled to welcome you to the team! click [approve] to accept the offering, or [revise]


[Name]
[annualsalary]
[monthlysalary]
[thr]
[leave]
[transportation]
[telecommunication]
[bonus]
[incentive]
[otherallowance1]
[otherallowance2]
[otherallowance3]
[Notes]
[Company]', 1, NULL)
INSERT [dbo].[Emails] ([Id], [TemplateName], [Category], [Subject], [Body], [Active], [LogError]) VALUES (7, N'MedCheck', N'ApplicantNotification', N'Invitation for Medical Checkup', N'Hi [Name],

You are invited to have Medical Checkup at : 
Day, Date', 1, NULL)
INSERT [dbo].[Emails] ([Id], [TemplateName], [Category], [Subject], [Body], [Active], [LogError]) VALUES (8, N'Regret Letter', N'ApplicantNotification', N'Lamaran atas posisi [title] di [Company]', N'Dengan hormat,
Sehubungan dengan lamaran saudara atas posisi [title] diperusahaan kami, maka dengan ini kami informasikan bahwa untuk saat ini bapak/ibu [Name] belum dapat kami terima. ', 1, NULL)
INSERT [dbo].[Emails] ([Id], [TemplateName], [Category], [Subject], [Body], [Active], [LogError]) VALUES (9, N'UserInterview', N'ApplicantNotification', N'You are invited to user intervew', N'Dear [Name],

Anda telah berhasil melalui tahapan test sebelumnya dan sekarang diundang untuk melakukan interview dengan User. Mohon dapat hadir pada : 
Hari : [Date]
Waktu : 08.00 - 09.00
Tempat : [Company]

Terima kasih', 0, NULL)
SET IDENTITY_INSERT [dbo].[Emails] OFF
