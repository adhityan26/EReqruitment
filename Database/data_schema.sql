/****** Object:  Table [dbo].[Years]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Years](
	[Year] [nvarchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VacancySteps]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VacancySteps](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[Description] [nvarchar](500) NULL,
 CONSTRAINT [PK_VacancySteps] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VacancyStatus]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VacancyStatus](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[RecruiterLabel] [nvarchar](50) NULL,
	[CandidateLabel] [nvarchar](50) NULL,
	[Description] [nvarchar](200) NULL,
	[Removable] [bit] NULL,
	[IsArchive] [bit] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_VacancyStatus] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VacancyQualifications]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VacancyQualifications](
	[VacancyCode] [nvarchar](50) NOT NULL,
	[ComponentCode] [nvarchar](50) NOT NULL,
	[ValueRangeStart] [nvarchar](50) NULL,
	[ValueRangeEnd] [nvarchar](50) NULL,
	[Weight] [float] NULL,
 CONSTRAINT [PK_VacancyQualifications] PRIMARY KEY CLUSTERED 
(
	[VacancyCode] ASC,
	[ComponentCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VacancyMilestoneLogs]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VacancyMilestoneLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VacancyCode] [nvarchar](50) NULL,
	[VacancyStatus] [nvarchar](50) NULL,
	[AdvertisementStatus] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[ExecutorCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_VacancyMilestoneLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VacancyDisqualifierQuestions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VacancyDisqualifierQuestions](
	[VacancyCode] [nvarchar](50) NOT NULL,
	[QuestionCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_VacancyDisqualifierQuestions] PRIMARY KEY CLUSTERED 
(
	[VacancyCode] ASC,
	[QuestionCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VacancyCategories]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VacancyCategories](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [nvarchar](1000) NULL,
	[IconCode] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_VacancyCategories] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Vacancies]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Vacancies](
	[Code] [nvarchar](50) NOT NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[CategoryCode] [nvarchar](50) NULL,
	[Subject] [nvarchar](500) NULL,
	[PositionCode] [nvarchar](50) NULL,
	[PositionName] [nvarchar](500) NULL,
	[EffectiveDate] [datetime] NULL,
	[ExpirationDate] [datetime] NULL,
	[SalaryCurrencyCode] [nvarchar](50) NULL,
	[SalaryRangeBottom] [money] NULL,
	[SalaryRangeTop] [money] NULL,
	[GenderSpecification] [nvarchar](10) NULL,
	[MaritalStatusSpecification] [nvarchar](10) NULL,
	[AgeRangeBottom] [int] NULL,
	[AgeRangeTop] [int] NULL,
	[YearsOfExperience] [int] NULL,
	[Status] [nvarchar](50) NULL,
	[Description] [nvarchar](500) NULL,
	[Location] [nvarchar](50) NULL,
	[TypeOfEmployment] [nvarchar](50) NULL,
	[HiringManager] [nvarchar](50) NULL,
	[Recruiter] [nvarchar](50) NULL,
	[BusinessJustification] [ntext] NULL,
	[JobRole] [nvarchar](50) NULL,
	[JobStage] [nvarchar](50) NULL,
	[RequiredEducationLevel] [nvarchar](50) NULL,
	[Department] [nvarchar](50) NULL,
	[ReportingTo] [nvarchar](50) NULL,
	[TeamName] [nvarchar](50) NULL,
	[Country] [nvarchar](50) NULL,
	[OrganizationUnit] [nvarchar](50) NULL,
	[Province] [nvarchar](50) NULL,
	[City] [nvarchar](50) NULL,
	[JobCode] [nvarchar](50) NULL,
	[NumberOfOpening] [int] NULL,
	[ApprovedDate] [datetime] NULL,
	[JobRequirement] [ntext] NULL,
	[JobDescription] [ntext] NULL,
	[Competencies] [nvarchar](500) NULL,
	[DisqualifierQuestions] [nvarchar](1000) NULL,
	[Applied] [int] NULL,
	[PostDate] [datetime] NULL,
	[ExpiredPostDate] [datetime] NULL,
	[PreferredStartDate] [datetime] NULL,
	[RecruitmentTargetGroup] [nvarchar](50) NULL,
	[JobPostingOption] [nvarchar](50) NULL,
	[NewPosition] [nvarchar](50) NULL,
	[IsShowCompany] [bit] NULL,
	[IsShowSalary] [bit] NULL,
	[Division] [nvarchar](50) NULL,
	[CostCentre] [money] NULL,
	[AdvertisementStatus] [nvarchar](50) NULL,
	[PipelineCode] [nvarchar](50) NULL,
	[DisqualifierQuestion] [nvarchar](50) NULL,
	[RankQualificationExperienceMinimumYear] [int] NULL,
	[RankQualificationMinimumEducationLevelCode] [nvarchar](50) NULL,
	[RankQualificationEducationFieldCode] [nvarchar](50) NULL,
	[RankQualificationEducationGPARangeBottom] [float] NULL,
	[RankQualificationEducationGPARangeTop] [float] NULL,
	[RankQualificationEducationLocationCode] [nvarchar](50) NULL,
	[RankQualificationResidenceLocationCode] [nvarchar](50) NULL,
	[RankQualificationSalaryRangeBottom] [money] NULL,
	[RankQualificationSalaryRangeTop] [money] NULL,
	[RankQualificationAgeRangeBottom] [int] NULL,
	[RankQualificationAgeRangeTop] [int] NULL,
	[RankQualificationExperienceMinimumYearWeight] [int] NULL,
	[RankQualificationMinimumEducationLevelCodeWeight] [int] NULL,
	[RankQualificationEducationFieldCodeWeight] [int] NULL,
	[RankQualificationEducationGPARangeWeight] [int] NULL,
	[RankQualificationEducationLocationCodeWeight] [int] NULL,
	[RankQualificationResidenceLocationCodeWeight] [int] NULL,
	[RankQualificationSalaryRangeWeight] [int] NULL,
	[RankQualificationAgeRangeWeight] [int] NULL,
	[SAPJobPositionID] [ntext] NULL,
	[SAPVacantJobPositionID] [ntext] NULL,
	[InsertStamp] [datetime] NOT NULL,
	[UpdateStamp] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NOT NULL,
	[InsertedBy] [nvarchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Vacancies] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserRoles]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserRoles](
	[UserCode] [nvarchar](50) NOT NULL,
	[RoleCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserRoles] PRIMARY KEY CLUSTERED 
(
	[UserCode] ASC,
	[RoleCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserExternalAccounts]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserExternalAccounts](
	[ApplicationName] [nvarchar](50) NOT NULL,
	[ExternalID] [nvarchar](50) NOT NULL,
	[InternalID] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserExternalAccounts] PRIMARY KEY CLUSTERED 
(
	[ApplicationName] ASC,
	[ExternalID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserCompanies]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserCompanies](
	[UserCode] [nvarchar](50) NOT NULL,
	[CompanyCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UserCompanies] PRIMARY KEY CLUSTERED 
(
	[UserCode] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[UserAccounts]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserAccounts](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[UserName] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[IsAdmin] [bit] NULL,
	[IsApplicant] [bit] NULL,
	[IsCompany] [bit] NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Active] [bit] NOT NULL,
	[IsRecruiter] [bit] NULL,
	[LastLogin] [datetime] NULL,
	[Email] [nvarchar](50) NULL,
	[FileName] [nvarchar](50) NULL,
	[FileType] [nvarchar](50) NULL,
	[Photo] [varbinary](max) NULL,
	[RoleCode] [nvarchar](1000) NULL,
	[Position] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserAccounts] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UniversityMajors]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UniversityMajors](
	[UniversityCode] [nvarchar](50) NOT NULL,
	[MajorCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_UniversityMajors] PRIMARY KEY CLUSTERED 
(
	[UniversityCode] ASC,
	[MajorCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Universities]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Universities](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[CountryCode] [nvarchar](50) NULL,
	[IsAffiliated] [bit] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Universities] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tests]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tests](
	[TestCode] [nvarchar](50) NOT NULL,
	[TestName] [nvarchar](50) NULL,
	[DurationInSeconds] [int] NULL,
	[Description] [nvarchar](1000) NULL,
	[PageSize] [int] NULL,
	[ShowTimer] [bit] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Tests] PRIMARY KEY CLUSTERED 
(
	[TestCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestQuestions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestQuestions](
	[TestCode] [nvarchar](50) NOT NULL,
	[QuestionCode] [nvarchar](50) NOT NULL,
	[DurationInSeconds] [int] NULL,
	[Point] [int] NULL,
	[CanSkip] [bit] NULL,
	[ShowTimer] [bit] NULL,
	[OrderId] [int] NULL,
 CONSTRAINT [PK_TestQuestions] PRIMARY KEY CLUSTERED 
(
	[TestCode] ASC,
	[QuestionCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestAttachments]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestAttachments](
	[AttachmentCode] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_TestAttachments] PRIMARY KEY CLUSTERED 
(
	[AttachmentCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableSynchronizationSessions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableSynchronizationSessions](
	[Code] [nvarchar](50) NOT NULL,
	[Date] [datetime] NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[ProcessedByCode] [nvarchar](50) NULL,
	[ConnectionString] [nvarchar](500) NULL,
	[MapCodes] [nvarchar](500) NULL,
	[Status] [nvarchar](50) NULL,
	[SynchronizationLogs] [nvarchar](max) NULL,
	[SynchronizationFlow] [nvarchar](50) NULL,
 CONSTRAINT [PK_TableSynchronizationSessions] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableMaps]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableMaps](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[InternalTableName] [nvarchar](100) NULL,
	[ExternalTableName] [nvarchar](100) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_TableMaps] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableMapProperties]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableMapProperties](
	[MapCode] [nvarchar](50) NOT NULL,
	[PropertyName] [nvarchar](50) NOT NULL,
	[ExternalPropertyName] [nvarchar](50) NULL,
	[DataType] [nvarchar](500) NULL,
	[IsKey] [bit] NULL,
	[AllowNull] [bit] NULL,
	[DefaultValue] [nvarchar](500) NULL,
 CONSTRAINT [PK_TableMapProperties] PRIMARY KEY CLUSTERED 
(
	[MapCode] ASC,
	[PropertyName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SurveyQuestions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyQuestions](
	[Code] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Question] [nvarchar](1000) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_SurveyQuestions] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SurveyQuestionAnswerOptions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SurveyQuestionAnswerOptions](
	[QuestionCode] [nvarchar](50) NOT NULL,
	[Number] [int] NOT NULL,
	[Label] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_SurveyQuestionAnswerOptions] PRIMARY KEY CLUSTERED 
(
	[QuestionCode] ASC,
	[Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SMSTemplates]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSTemplates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[Subject] [nvarchar](50) NULL,
	[Category] [nvarchar](50) NULL,
	[Body] [ntext] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_SMSTemplates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SMSMessages]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SMSMessages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Flow] [nvarchar](50) NULL,
	[SenderNumber] [nvarchar](50) NULL,
	[RecipientNumber] [nvarchar](50) NULL,
	[MessageType] [nvarchar](50) NULL,
	[Message] [nvarchar](1000) NULL,
	[LogTime] [datetime] NULL,
	[Verified] [bit] NULL,
 CONSTRAINT [PK_SMSMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[RoleCode] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[CompanyCodes] [nvarchar](1000) NULL,
	[AccessSetting] [bit] NULL,
	[AccessSetup] [bit] NULL,
	[AccessReport] [bit] NULL,
	[AccessApplicant] [bit] NULL,
	[ImportBulkData] [bit] NULL,
	[CreateJob] [bit] NULL,
	[PostUnPostJob] [bit] NULL,
	[DeleteJob] [bit] NULL,
	[ArchiveJob] [bit] NULL,
	[SchedulingFunctionality] [bit] NULL,
	[OfferingFunctionality] [bit] NULL,
	[RankFunctionality] [bit] NULL,
	[ReferFunctionality] [bit] NULL,
	[FlagFunctionality] [bit] NULL,
	[DisqualifyFunctionality] [bit] NULL,
	[DownloadDashboardData] [bit] NULL,
	[AccessCompensationData] [bit] NULL,
	[ChangeApplicantStatus] [bit] NULL,
	[MassScheduling] [bit] NULL,
	[UpdateApplicantApplicationData] [bit] NULL,
	[SearchApplicant] [bit] NULL,
	[ExportBulkData] [bit] NULL,
	[Approval] [bit] NULL,
	[AccessCSSTheme] [bit] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED 
(
	[RoleCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RoleCompanyAccessList]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RoleCompanyAccessList](
	[RoleCode] [nvarchar](50) NOT NULL,
	[CompanyCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_RoleCompanyAccessList] PRIMARY KEY CLUSTERED 
(
	[RoleCode] ASC,
	[CompanyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReportSubscribers]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReportSubscribers](
	[ReportCode] [nvarchar](50) NOT NULL,
	[UserCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ReportSubscribers] PRIMARY KEY CLUSTERED 
(
	[ReportCode] ASC,
	[UserCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Reports]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Reports](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[ReportFile] [nvarchar](100) NULL,
	[Mode] [nvarchar](50) NULL,
	[Schedule] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_Reports] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ReasonForLeaving]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReasonForLeaving](
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ReasonForLeaving] PRIMARY KEY CLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[RankQualificationComponents]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RankQualificationComponents](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](100) NULL,
	[EvaluatorMethodName] [nvarchar](100) NULL,
	[EvaluatorAssemblyName] [nvarchar](500) NULL,
 CONSTRAINT [PK_RankQualificationComponents] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Questions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Questions](
	[Code] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [nvarchar](1000) NULL,
	[Image] [varbinary](max) NULL,
	[Question] [nvarchar](1000) NULL,
	[AnswerFormat] [nvarchar](50) NULL,
	[CorrectAnswerOperator] [nvarchar](50) NULL,
	[CorrectAnswerLabel] [nvarchar](50) NULL,
	[OrderId] [int] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Questions] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QuestionAnswers]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuestionAnswers](
	[QuestionCode] [nvarchar](50) NOT NULL,
	[Number] [int] NOT NULL,
	[Label] [nvarchar](50) NOT NULL,
	[IsCorrectAnswer] [bit] NULL,
 CONSTRAINT [PK_TestQuestionAnswers] PRIMARY KEY CLUSTERED 
(
	[QuestionCode] ASC,
	[Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Province]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Province](
	[CountryCode] [nvarchar](50) NOT NULL,
	[ProvinceCode] [nvarchar](50) NOT NULL,
	[ProvinceName] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Province] PRIMARY KEY CLUSTERED 
(
	[CountryCode] ASC,
	[ProvinceCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Positions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Positions](
	[PositionCode] [nvarchar](50) NOT NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[Name] [nvarchar](200) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED 
(
	[PositionCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PipelineTests]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PipelineTests](
	[PipelineCode] [nvarchar](50) NOT NULL,
	[TestCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PipelineTests] PRIMARY KEY CLUSTERED 
(
	[PipelineCode] ASC,
	[TestCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PipelineSteps]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PipelineSteps](
	[PipelineCode] [nvarchar](50) NOT NULL,
	[StatusCode] [nvarchar](50) NOT NULL,
	[Required] [bit] NULL,
	[Compensation] [bit] NULL,
	[Scheduling] [bit] NULL,
	[Comment] [bit] NULL,
	[RecruiterEmailTemplateId] [int] NULL,
	[ApplicantEmailTemplateId] [int] NULL,
	[SortOrder] [int] NULL,
	[UpdateStatusTemplateId] [int] NULL,
	[SchedullingRecruiterTemplateId] [int] NULL,
	[SchedullingApplicantTemplateId] [int] NULL,
	[OfferingApplicantTemplateId] [int] NULL,
	[IsHidden] [bit] NULL,
	[UpdateStatusSMSTemplateId] [int] NULL,
	[SchedullingSMSTemplateId] [int] NULL,
	[TestCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_PipelineSteps] PRIMARY KEY CLUSTERED 
(
	[PipelineCode] ASC,
	[StatusCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pipelines]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pipelines](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[TestCodes] [nvarchar](1000) NULL,
	[AttachmentCodes] [nvarchar](1000) NULL,
	[AutoDisqualifiedRecruitmentStatusCode] [nvarchar](50) NULL,
	[ReferedDisqualifiedRecruitmentStatusCode] [nvarchar](50) NULL,
	[HiredRecruitmentStatusCode] [nvarchar](50) NULL,
	[DisqualifiedRecruitmentStatusCode] [nvarchar](50) NULL,
	[AutoDisqualifiedRecruitmentStatusEmailTemplateId] [int] NULL,
	[ReferedDisqualifiedRecruitmentStatusEmailTemplateId] [int] NULL,
	[HiredRecruitmentStatusEmailTemplateId] [int] NULL,
	[DisqualifiedRecruitmentStatusEmailTemplateId] [int] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Pipelines] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PipelineAttachments]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PipelineAttachments](
	[PipelineCode] [nvarchar](50) NOT NULL,
	[AttachmentCode] [nvarchar](50) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PageTranslatableLabels]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageTranslatableLabels](
	[PageCode] [nvarchar](50) NOT NULL,
	[Label] [nvarchar](200) NOT NULL,
	[Caption] [nvarchar](200) NULL,
	[OrderID] [int] NULL,
 CONSTRAINT [PK_PageTranslatableLabels] PRIMARY KEY CLUSTERED 
(
	[PageCode] ASC,
	[Label] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PageTranslatableLabelContents]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageTranslatableLabelContents](
	[CompanyCode] [nvarchar](50) NOT NULL,
	[PageCode] [nvarchar](50) NOT NULL,
	[Label] [nvarchar](200) NOT NULL,
	[IDText] [ntext] NULL,
	[ENText] [ntext] NULL,
	[OrderID] [int] NULL,
 CONSTRAINT [PK_PageTranslatableLabelContents] PRIMARY KEY CLUSTERED 
(
	[CompanyCode] ASC,
	[PageCode] ASC,
	[Label] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pages]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pages](
	[ID] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[ParentID] [nvarchar](50) NULL,
	[BaseURL] [nvarchar](200) NULL,
	[SubURL] [nvarchar](200) NULL,
	[PageName] [nvarchar](200) NULL,
	[IsPublicPage] [bit] NULL,
	[IsApplicantPage] [bit] NULL,
	[IsRecruiterPage] [bit] NULL,
	[IsAdminPage] [bit] NULL,
	[IsTranslatable] [bit] NULL,
	[OrderID] [int] NULL,
	[DataLangAttribute] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Pages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PageHighlightContexts]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PageHighlightContexts](
	[PageFileName] [nvarchar](50) NOT NULL,
	[ContextFileName] [nvarchar](100) NULL,
 CONSTRAINT [PK_PageHighlightContexts] PRIMARY KEY CLUSTERED 
(
	[PageFileName] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrganizationType]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganizationType](
	[Code] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](500) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_OrganizationTypes] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrganizationPosition]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrganizationPosition](
	[Code] [nvarchar](50) NOT NULL,
	[Position] [nvarchar](500) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_OrganizationPositions] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Month]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Month](
	[Id] [int] NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_Month] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Locations]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Locations](
	[Location] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Locations] PRIMARY KEY CLUSTERED 
(
	[Location] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Languages]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Code] [nvarchar](50) NOT NULL,
	[Language] [nvarchar](100) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Jobs]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Jobs](
	[Code] [nvarchar](50) NOT NULL,
	[JobCode] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[Requirements] [nvarchar](800) NULL,
	[JobRole] [nvarchar](50) NULL,
	[JobStage] [nvarchar](50) NULL,
	[Active] [bit] NULL,
	[RequiredEducationLevel] [nvarchar](50) NULL,
	[JobDescription] [nvarchar](800) NULL,
 CONSTRAINT [PK_Jobs] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Industries]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Industries](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Description] [nvarchar](1000) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Industries] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmploymentStatus]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmploymentStatus](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_EmploymentStatus] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EmailTokens]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EmailTokens](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TokenCode] [nvarchar](50) NULL,
	[SwitcherEngine] [nvarchar](100) NULL,
	[SwitcherMethodName] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_EmailTokens] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Emails]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Emails](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[Category] [nvarchar](50) NULL,
	[Subject] [nvarchar](50) NULL,
	[Body] [ntext] NULL,
	[Active] [bit] NULL,
	[LogError] [ntext] NULL,
 CONSTRAINT [PK_Emails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EducationMajor]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationMajor](
	[Code] [nvarchar](50) NOT NULL,
	[Major] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_EducationMajor] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EducationLevels]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationLevels](
	[EducationLevel] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[MajorExist] [bit] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Educations] PRIMARY KEY CLUSTERED 
(
	[EducationLevel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EducationFieldOfStudy]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EducationFieldOfStudy](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_EducationFieldOfStudy] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Divisions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Divisions](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Divisions] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DisqualifierQuestions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DisqualifierQuestions](
	[Code] [nvarchar](50) NOT NULL,
	[Type] [nvarchar](50) NULL,
	[Name] [nvarchar](50) NULL,
	[ParentCode] [nvarchar](50) NULL,
	[Question] [nvarchar](1000) NULL,
	[CategoryCode] [nvarchar](50) NULL,
	[AnswerFormat] [nvarchar](50) NULL,
	[Active] [bit] NULL,
	[OrderId] [int] NULL,
	[CorrectAnswerOperator] [nvarchar](50) NULL,
	[CorrectAnswerLabel] [nvarchar](50) NULL,
	[QuestionType] [nvarchar](50) NULL,
 CONSTRAINT [PK_DisqualifierQuestions] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DisqualifierQuestionAnswerOptions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DisqualifierQuestionAnswerOptions](
	[QuestionCode] [nvarchar](50) NOT NULL,
	[Number] [int] NOT NULL,
	[Label] [nvarchar](50) NOT NULL,
	[IsCorrectAnswer] [bit] NULL,
 CONSTRAINT [PK_DisqualifierQuestionAnswerOptions] PRIMARY KEY CLUSTERED 
(
	[QuestionCode] ASC,
	[Number] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Departments]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Departments](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Departments] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CurriculumVitaes]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CurriculumVitaes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[Attachment] [varbinary](max) NULL,
	[FileName] [nvarchar](200) NULL,
	[FileType] [nvarchar](200) NULL,
	[Notes] [nvarchar](500) NULL,
	[InsertedBy] [nvarchar](50) NOT NULL,
	[InsertStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_CurriculumVitaes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Country]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Country](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Nationality] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Country] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CompanyIndustries]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CompanyIndustries](
	[CompanyCode] [nvarchar](50) NOT NULL,
	[IndustryCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_CompanyIndustries] PRIMARY KEY CLUSTERED 
(
	[CompanyCode] ASC,
	[IndustryCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Companies]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Companies](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[AddressLine1] [nvarchar](200) NULL,
	[AddressLine2] [nvarchar](200) NULL,
	[AddressLine3] [nvarchar](200) NULL,
	[Status] [nvarchar](50) NULL,
	[Logo] [varbinary](max) NULL,
	[CoverImage] [varbinary](max) NULL,
	[CoverVideoUrl] [nvarchar](200) NULL,
	[AboutUsImage] [varbinary](max) NULL,
	[FaviconLogo] [varbinary](max) NULL,
	[CssTheme] [nvarchar](50) NULL,
	[EnableMedsosAuthentication] [bit] NULL,
	[ApplicationTitle] [nvarchar](1000) NULL,
	[Industry] [nvarchar](50) NULL,
	[ContactPerson] [nvarchar](50) NULL,
	[ContactNumber] [nvarchar](50) NULL,
	[ContactExtensionNumber] [nvarchar](50) NULL,
	[ContactMobileNumber] [nvarchar](50) NULL,
	[ContactMobilePerson] [nvarchar](50) NULL,
	[Website] [nvarchar](50) NULL,
	[ATSSite] [nvarchar](100) NULL,
	[ATSBaseUrl] [nvarchar](100) NULL,
	[MailSender] [nvarchar](50) NULL,
	[FileName] [nvarchar](50) NULL,
	[FileType] [nvarchar](50) NULL,
	[LandingOpeningTitle] [nvarchar](50) NULL,
	[LandingOpeningText] [nvarchar](50) NULL,
	[LandingAboutUsTitle] [nvarchar](50) NULL,
	[LandingAboutUsSubTitle] [nvarchar](100) NULL,
	[LandingAboutUsText] [nvarchar](1000) NULL,
	[FacebookAPIKey] [nvarchar](200) NULL,
	[LinkedINAPIKey] [nvarchar](200) NULL,
	[CopyrightText] [nvarchar](200) NULL,
	[FacebookUrl] [nvarchar](200) NULL,
	[LinkedINUrl] [nvarchar](200) NULL,
	[SurveyQuestionCodes] [nvarchar](1000) NULL,
	[SurveyDeliveryTerms] [nvarchar](50) NULL,
	[SurveyEmailTemplateId] [int] NULL,
	[EmailSmtpHostAddress] [nvarchar](200) NULL,
	[EmailSmtpPortNumber] [nvarchar](200) NULL,
	[EmailSmtpEmailAddress] [nvarchar](200) NULL,
	[EmailSmtpEmailPassword] [nvarchar](200) NULL,
	[EmailSmtpUseSSL] [bit] NULL,
	[CoverImage1] [varbinary](max) NULL,
	[CoverImage2] [varbinary](max) NULL,
	[CoverImage3] [varbinary](max) NULL,
	[CoverImage4] [varbinary](max) NULL,
	[CoverVideoUrl1] [nvarchar](200) NULL,
	[CoverVideoUrl2] [nvarchar](200) NULL,
	[CoverVideoUrl3] [nvarchar](200) NULL,
	[CoverVideoUrl4] [nvarchar](200) NULL,
	[CssContent] [nvarchar](max) NULL,
	[CompanyCodes] [nvarchar](1000) NULL,
	[InsertStamp] [datetime] NOT NULL,
	[UpdateStamp] [datetime] NOT NULL,
	[UpdatedBy] [nvarchar](50) NOT NULL,
	[InsertedBy] [nvarchar](50) NOT NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Companies] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[City]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City](
	[CityCode] [nvarchar](50) NOT NULL,
	[CountryCode] [nvarchar](50) NOT NULL,
	[ProvinceCode] [nvarchar](50) NOT NULL,
	[CityName] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_City_1] PRIMARY KEY CLUSTERED 
(
	[CityCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BatchProcessSchemas]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BatchProcessSchemas](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[AssemblyName] [nvarchar](500) NULL,
	[ProcesserAssemblyName] [nvarchar](500) NULL,
	[ProcessMethodName] [nvarchar](100) NULL,
	[SchemaGeneratorMethodName] [nvarchar](100) NULL,
 CONSTRAINT [PK_BatchProcessSchemas] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[BatchProcesses]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[BatchProcesses](
	[Code] [nvarchar](50) NOT NULL,
	[SchemaCode] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[ProcessedBy] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Attachment] [varbinary](max) NULL,
	[FileName] [nvarchar](100) NULL,
	[FileType] [nvarchar](100) NULL,
	[VacancyCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_BatchProcesses] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Awards]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Awards](
	[Code] [nvarchar](50) NOT NULL,
	[Award] [nvarchar](100) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_Awards] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssestmentSessions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssestmentSessions](
	[Code] [nvarchar](50) NOT NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[TestCode] [nvarchar](50) NULL,
	[StartDate] [datetime] NULL,
	[EndDate] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
	[ActualStartDate] [datetime] NULL,
	[ActualEndDate] [datetime] NULL,
	[AssedBy] [nvarchar](50) NULL,
	[InsertStamp] [datetime] NULL,
	[InsertedBy] [nvarchar](50) NULL,
	[UpdateStamp] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_AssestmentSessions] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AssestmentDetails]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AssestmentDetails](
	[AssestmentCode] [nvarchar](50) NOT NULL,
	[QuestionCode] [nvarchar](50) NOT NULL,
	[Point] [int] NULL,
 CONSTRAINT [PK_AssestmentDetails] PRIMARY KEY CLUSTERED 
(
	[AssestmentCode] ASC,
	[QuestionCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationThemes]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationThemes](
	[Code] [nvarchar](50) NOT NULL,
	[FileName] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicationThemes] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationSettings]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantPortal] [bit] NULL,
	[ApplicantTrackingSystem] [bit] NULL,
	[ApprovalFunction] [bit] NULL,
	[JobAgeCalculationMethod] [nvarchar](50) NULL,
	[VacancyApplicationMode] [nvarchar](50) NULL,
	[EnableSurveyQuestion] [bit] NULL,
	[AutoDisqualifiedRecruitmentStatusCode] [nvarchar](50) NULL,
	[ReferedDisqualifiedRecruitmentStatusCode] [nvarchar](50) NULL,
	[HiredRecruitmentStatusCode] [nvarchar](50) NULL,
	[DisqualifiedRecruitmentStatusCode] [nvarchar](50) NULL,
	[PeerConnectionString] [nvarchar](500) NULL,
 CONSTRAINT [PK_ApplicationSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationPageControlTexts]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationPageControlTexts](
	[PageName] [nvarchar](50) NOT NULL,
	[LanguageCode] [nvarchar](50) NOT NULL,
	[ControlID] [nvarchar](100) NOT NULL,
	[Text] [nvarchar](1000) NULL,
 CONSTRAINT [PK_ApplicationPageControlTexts] PRIMARY KEY CLUSTERED 
(
	[PageName] ASC,
	[LanguageCode] ASC,
	[ControlID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationPageControls]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationPageControls](
	[PageName] [nvarchar](50) NOT NULL,
	[ControlID] [nvarchar](100) NOT NULL,
	[ControlType] [nvarchar](500) NULL,
	[OrderID] [int] NULL,
 CONSTRAINT [PK_ApplicationPageControls] PRIMARY KEY CLUSTERED 
(
	[PageName] ASC,
	[ControlID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicationLanguages]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicationLanguages](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicationLanguages] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancyTestResults]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancyTestResults](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[VacancyCode] [nvarchar](50) NOT NULL,
	[TestCode] [nvarchar](50) NOT NULL,
	[Result] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicantVacancyTestResults] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[VacancyCode] ASC,
	[TestCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancySchedules]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancySchedules](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[VacancyCode] [nvarchar](50) NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[InviteTo] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
	[TimeStart] [nvarchar](50) NULL,
	[TimeEnd] [nvarchar](50) NULL,
	[StartHour] [nvarchar](50) NULL,
	[StartMinute] [nvarchar](50) NULL,
	[EndHour] [nvarchar](50) NULL,
	[EndMinute] [nvarchar](50) NULL,
	[Place] [nvarchar](50) NULL,
	[PIC] [nvarchar](50) NULL,
	[Notes] [ntext] NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[ApplicantStatusNotification] [nvarchar](50) NULL,
	[RecruiterStatusNotification] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicantVacancySchedules] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancyReferences]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancyReferences](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[VacancyCode] [nvarchar](50) NOT NULL,
	[CompanyReferenceCode] [nvarchar](50) NOT NULL,
	[IssuedByCode] [nvarchar](50) NULL,
	[IssuedDate] [datetime] NULL,
	[Notes] [nvarchar](200) NULL,
 CONSTRAINT [PK_ApplicantVacancyReferences] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[VacancyCode] ASC,
	[CompanyReferenceCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancyOffering]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancyOffering](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[VacancyCode] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[CreatedDate] [datetime] NULL,
	[CreatedBy] [nvarchar](50) NULL,
	[AnnualSalary] [money] NULL,
	[MonthlySalary] [money] NULL,
	[THR] [money] NULL,
	[AnnualLeave] [int] NULL,
	[Transportation] [money] NULL,
	[Telecommunication] [money] NULL,
	[Bonus] [money] NULL,
	[Incentive] [money] NULL,
	[OtherAllowance1] [money] NULL,
	[OtherAllowance2] [money] NULL,
	[OtherAllowance3] [money] NULL,
	[EffectiveDate] [datetime] NULL,
	[Notes] [ntext] NULL,
	[ApplicantStatusNotification] [nvarchar](50) NULL,
	[EligibleForTHR] [bit] NULL,
	[EligibleForIncentive] [bit] NULL,
	[EligibleForBonus] [bit] NULL,
 CONSTRAINT [PK_ApplicantVacancyOffering] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancyDisqualificationAnswers]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancyDisqualificationAnswers](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[VacancyCode] [nvarchar](50) NOT NULL,
	[QuestionCode] [nvarchar](50) NOT NULL,
	[Answer] [nvarchar](500) NULL,
 CONSTRAINT [PK_ApplicantVacancyDisqualificationAnswers] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[VacancyCode] ASC,
	[QuestionCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancyComments]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancyComments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[VacancyCode] [nvarchar](50) NULL,
	[Comment] [ntext] NULL,
	[Status] [nvarchar](50) NULL,
	[CommentBy] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_ApplicantVacancyComments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancyCategories]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancyCategories](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[VacancyCategoryCode] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ApplicantVacancyCategories] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[VacancyCategoryCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancyAttachmentFiles]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicantVacancyAttachmentFiles](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[VacancyCode] [nvarchar](50) NOT NULL,
	[AttachmentCode] [nvarchar](50) NOT NULL,
	[Attachment] [varbinary](max) NULL,
	[FileName] [nvarchar](200) NULL,
	[FileType] [nvarchar](200) NULL,
 CONSTRAINT [PK_ApplicantVacancyAttachmentFiles] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[VacancyCode] ASC,
	[AttachmentCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ApplicantVacancies]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancies](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[VacancyCode] [nvarchar](50) NOT NULL,
	[AppliedDate] [datetime] NULL,
	[Status] [nvarchar](50) NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[OfferingEffectiveDate] [datetime] NULL,
	[OfferingPosition] [nvarchar](500) NULL,
	[OfferingAddress] [nvarchar](500) NULL,
	[OfferingAnnualSalary] [money] NULL,
	[OfferingMonthlySalary] [money] NULL,
	[OfferingTHR] [money] NULL,
	[OfferingLeaveDays] [int] NULL,
	[OfferingTransportation] [nvarchar](500) NULL,
	[OfferingTelecommunication] [nvarchar](500) NULL,
	[OfferingBonus] [money] NULL,
	[OfferingIncentive] [money] NULL,
	[InformationSource] [nvarchar](50) NULL,
	[InformationSourceNotes] [nvarchar](500) NULL,
	[Ranked] [bit] NULL,
	[RankWeight] [float] NULL,
	[RankNumber] [int] NULL,
	[RankQualificationExperienceMinimumYear] [int] NULL,
	[RankQualificationMinimumEducationLevelCode] [nvarchar](50) NULL,
	[RankQualificationEducationFieldCode] [nvarchar](50) NULL,
	[RankQualificationEducationGPARangeBottom] [float] NULL,
	[RankQualificationEducationGPARangeTop] [float] NULL,
	[RankQualificationEducationLocationCode] [nvarchar](50) NULL,
	[RankQualificationResidenceLocationCode] [nvarchar](50) NULL,
	[RankQualificationSalaryRangeBottom] [money] NULL,
	[RankQualificationSalaryRangeTop] [money] NULL,
	[RankQualificationAgeRangeBottom] [int] NULL,
	[RankQualificationAgeRangeTop] [int] NULL,
	[RankQualificationExperienceMinimumYearWeight] [int] NULL,
	[RankQualificationMinimumEducationLevelCodeWeight] [int] NULL,
	[RankQualificationEducationFieldCodeWeight] [int] NULL,
	[RankQualificationEducationGPARangeWeight] [int] NULL,
	[RankQualificationEducationLocationCodeWeight] [int] NULL,
	[RankQualificationResidenceLocationCodeWeight] [int] NULL,
	[RankQualificationSalaryRangeWeight] [int] NULL,
	[RankQualificationAgeRangeWeight] [int] NULL,
	[UpdateStamp] [datetime] NULL,
	[UpdatedBy] [nvarchar](50) NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_ApplicantVacancies] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[VacancyCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantVacancieInformationSources]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantVacancieInformationSources](
	[Code] [nvarchar](50) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[AdditionalInfoRequired] [bit] NULL,
	[OrderID] [int] NULL,
	[Active] [bit] NULL,
 CONSTRAINT [PK_ApplicantVacancieInformationSources] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantTrainings]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantTrainings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[TrainingName] [nvarchar](200) NULL,
	[Country] [nvarchar](100) NULL,
	[StateRegion] [nvarchar](100) NULL,
	[ProviderName] [nvarchar](1000) NULL,
	[Year] [int] NULL,
	[Certified] [nvarchar](50) NULL,
	[Description] [nvarchar](1000) NULL,
 CONSTRAINT [PK_ApplicantTrainings_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantsVacancyMilestoneLogs]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantsVacancyMilestoneLogs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[VacancyCode] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
	[Date] [datetime] NULL,
 CONSTRAINT [PK_ApplicantsVacancyMilestoneLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantSurveySessions]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantSurveySessions](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[IssuedDate] [datetime] NULL,
	[CompletedDate] [datetime] NULL,
 CONSTRAINT [PK_ApplicantSurveySessions] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantSurveys]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantSurveys](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[QuestionCode] [nvarchar](50) NOT NULL,
	[Answer] [nvarchar](500) NULL,
 CONSTRAINT [PK_ApplicantSurveys] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[QuestionCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantSurveyAnswers]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantSurveyAnswers](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[DisqualifierCode] [nvarchar](50) NOT NULL,
	[Answer] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicantSurveyAnswers] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC,
	[DisqualifierCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantSkills]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantSkills](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[SkillType] [nvarchar](50) NULL,
	[SkillName] [nvarchar](200) NULL,
	[ProficiencySpeaking] [nvarchar](50) NULL,
	[ProficiencyWriting] [nvarchar](50) NULL,
	[ProficiencyListening] [nvarchar](50) NULL,
	[Proficiency] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicantSkills_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Applicants]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Applicants](
	[Code] [nvarchar](50) NOT NULL,
	[CompanyCode] [nvarchar](50) NULL,
	[Photo] [varbinary](max) NULL,
	[Name] [nvarchar](500) NULL,
	[FirstName] [nvarchar](500) NULL,
	[LastName] [nvarchar](500) NULL,
	[GenderSpecification] [nvarchar](50) NULL,
	[MaritalStatusSpecification] [nvarchar](50) NULL,
	[YearsOfExperience] [int] NULL,
	[Age] [int] NULL,
	[Birthday] [datetime] NULL,
	[BirthPlace] [nvarchar](100) NULL,
	[Address] [ntext] NULL,
	[Email] [nvarchar](100) NULL,
	[Phone] [nvarchar](50) NULL,
	[AlternativePhoneNumber] [nvarchar](50) NULL,
	[Mobile] [nvarchar](50) NULL,
	[Religion] [nvarchar](50) NULL,
	[Nationality] [nvarchar](50) NULL,
	[MaritalStatus] [nvarchar](50) NULL,
	[NumberOfChildren] [nvarchar](100) NULL,
	[RegisteredDate] [datetime] NULL,
	[PreviouslyEmployed] [bit] NULL,
	[Attachment] [varbinary](max) NULL,
	[FileName] [nvarchar](100) NULL,
	[FileType] [nvarchar](100) NULL,
	[IDCardNumber] [nvarchar](100) NULL,
	[SocMedName] [nvarchar](100) NULL,
	[SocMedID] [nvarchar](100) NULL,
	[IDCardAddress] [nvarchar](100) NULL,
	[IDCardCountry] [nvarchar](100) NULL,
	[IDCardProvince] [nvarchar](100) NULL,
	[IDCardCity] [nvarchar](100) NULL,
	[IDCardPostalCode] [nvarchar](100) NULL,
	[CurrentCardAddress] [nvarchar](100) NULL,
	[CurrentCardCountry] [nvarchar](100) NULL,
	[CurrentCardProvince] [nvarchar](100) NULL,
	[CurrentCardCity] [nvarchar](100) NULL,
	[CurrentCardPostalCode] [nvarchar](100) NULL,
	[ExpectedSalary] [money] NULL,
	[ExpectedPositionOrFunction] [nvarchar](50) NULL,
	[NoticePeriod] [int] NULL,
	[EligibleForAnyCities] [nvarchar](50) NULL,
	[ExpectedSalaryCurrency] [nvarchar](50) NULL,
	[ExpectedSalaryMethod] [nvarchar](50) NULL,
	[OtherAttachment] [varbinary](max) NULL,
	[OtherFileName] [nvarchar](100) NULL,
	[OtherFileType] [nvarchar](100) NULL,
	[CV] [varbinary](max) NULL,
	[CVFileName] [nvarchar](100) NULL,
	[CVFileType] [nvarchar](100) NULL,
	[FacebookUrl] [nvarchar](500) NULL,
	[TwitterUrl] [nvarchar](500) NULL,
	[LinkedInUrl] [nvarchar](500) NULL,
	[Verified] [bit] NULL,
	[ExpiredDate] [datetime] NULL,
	[SurveyInvitationSent] [bit] NULL,
	[SurveyTaken] [bit] NULL,
	[Active] [bit] NOT NULL,
 CONSTRAINT [PK_Applicants] PRIMARY KEY CLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ApplicantHobbyInterests]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantHobbyInterests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[Description] [nvarchar](200) NULL,
 CONSTRAINT [PK_ApplicantHobbyInterests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantFlagStatuses]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantFlagStatuses](
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[Notes] [nvarchar](200) NULL,
	[FlagStatus] [nvarchar](50) NULL,
	[IssuedByCode] [nvarchar](50) NULL,
	[IssuedDate] [datetime] NULL,
	[VacancyCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicantFlagStatuses] PRIMARY KEY CLUSTERED 
(
	[ApplicantCode] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantExperiences]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantExperiences](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[ExperienceType] [nvarchar](50) NULL,
	[OrganizationName] [nvarchar](200) NULL,
	[PositionName] [nvarchar](200) NULL,
	[StartYear] [int] NULL,
	[StartMonth] [int] NULL,
	[EndYear] [int] NULL,
	[EndMonth] [int] NULL,
	[StillUntillNow] [bit] NULL,
	[PositionLevel] [nvarchar](200) NULL,
	[EmploymentStatus] [nvarchar](200) NULL,
	[SalaryCurrency] [nvarchar](50) NULL,
	[SalaryAmount] [money] NULL,
	[SalaryMethod] [nvarchar](50) NULL,
	[ReasonForLeaving] [nvarchar](50) NULL,
	[JobDescription] [nvarchar](500) NULL,
 CONSTRAINT [PK_ApplicantExperiences_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantEducations]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantEducations](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[LevelCode] [nvarchar](50) NULL,
	[StartYear] [int] NULL,
	[EndYear] [int] NULL,
	[InstitutionName] [nvarchar](200) NULL,
	[FieldOfStudyCode] [nvarchar](200) NULL,
	[MajorCode] [nvarchar](50) NULL,
	[Score] [float] NULL,
	[EducationCityCode] [nvarchar](50) NULL,
	[EducationCountryCode] [nvarchar](50) NULL,
	[EducationCityName] [nvarchar](500) NULL,
	[ScoreTotal] [float] NULL,
 CONSTRAINT [PK_ApplicantEducations_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantDisqualifierAnswers]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantDisqualifierAnswers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[VacancyCode] [nvarchar](50) NULL,
	[DisqualifierCode] [nvarchar](50) NULL,
	[Answer] [nvarchar](50) NULL,
 CONSTRAINT [PK_ApplicantDisqualifierAnswers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantAwards]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ApplicantAwards](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ApplicantCode] [nvarchar](50) NOT NULL,
	[AwardType] [nvarchar](50) NULL,
	[AwardName] [nvarchar](200) NULL,
	[Description] [nvarchar](1000) NULL,
	[Year] [int] NULL,
 CONSTRAINT [PK_ApplicantAwards_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ApplicantAttachments]    Script Date: 06/11/2017 18:09:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplicantAttachments](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[AttachmentType] [nvarchar](50) NULL,
	[ApplicantCode] [nvarchar](50) NULL,
	[Attachment] [varbinary](max) NULL,
	[FileName] [nvarchar](200) NULL,
	[FileType] [nvarchar](200) NULL,
	[Notes] [nvarchar](50) NULL,
	[InsertedBy] [nvarchar](50) NULL,
	[InsertStamp] [datetime] NULL,
 CONSTRAINT [PK_ApplicantAttachments] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
