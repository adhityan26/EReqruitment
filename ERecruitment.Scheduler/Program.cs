﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERecruitment.Domain;
using System.Web;
using System.Configuration;
using System.Net;
using System.Net.Mail;
using SS.Web.UI;
using FastReport;
using FastReport.Export;
using FastReport.Utils;
using System.Configuration;
using System.IO;

namespace ERecruitment.Scheduler
{
    class Program
    {
        public static void SendEmail(Companies emailSetting, string emailDestination, string subject, string body)
        {
            body = body.Replace(Environment.NewLine, "<br/>");
            SmtpClient client = new SmtpClient();
            client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
            client.Host = emailSetting.EmailSmtpHostAddress;
            if (emailSetting.EmailSmtpUseSSL)
                client.EnableSsl = true;
            client.Timeout = 20000;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

            string address = emailSetting.EmailSmtpEmailAddress;
            string displayName = emailSetting.MailSender;

            if (!address.Contains("@"))
            {
                address = displayName;
            }
            
            MailMessage mm = new MailMessage(new MailAddress(address,
                    displayName),
                                                             new MailAddress(emailDestination, ""));
            mm.Subject = subject;
            mm.Body = body;
            mm.BodyEncoding = UTF8Encoding.UTF8;
            mm.IsBodyHtml = true;
            mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

            client.Send(mm);
        }

        public static void SendReport(Companies emailSetting, string recipient, string reportFile, string startDate, string endDate, string companyCode)
        {
            Config.ReportSettings.ShowProgress = false; //Disable progress window
            Report report = new Report(); //Create new report object
            string[] dateComponent1 = startDate.Split('/');
            string[] dateComponent2 = endDate.Split('/');
            DateTime from = new DateTime(Utils.ConvertString<int>(dateComponent1[2]), Utils.ConvertString<int>(dateComponent1[1]), Utils.ConvertString<int>(dateComponent1[0]));
            DateTime to = new DateTime(Utils.ConvertString<int>(dateComponent2[2]), Utils.ConvertString<int>(dateComponent2[1]), Utils.ConvertString<int>(dateComponent2[0]));

            to = to.AddHours(23);
            to = to.AddMinutes(59);
            to = to.AddSeconds(59);
            report.Load(ConfigurationManager.AppSettings["ReportPath"] + reportFile); //Load report
            report.SetParameterValue("StartDate", from);
            report.SetParameterValue("EndDate", to);
            report.SetParameterValue("CompanyCode", companyCode);
            report.Dictionary.Connections[0].ConnectionString = ConfigurationManager.ConnectionStrings["ERecruitmentConnectionString"].ConnectionString;
            report.Dictionary.Connections[0].CommandTimeout = 3600;
            report.Prepare(); //Prepare report
            FastReport.Export.Pdf.PDFExport pdf = new FastReport.Export.Pdf.PDFExport(); //Cteate PDF export
//            FastReport.Export.Email.EmailExport email = new FastReport.Export.Email.EmailExport(); //Create Email export

//            SmtpClient client = new SmtpClient();
//            client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
//            client.Host = emailSetting.EmailSmtpHostAddress;
//            if (emailSetting.EmailSmtpUseSSL)
//                client.EnableSsl = true;
//            client.Timeout = 20000;
//            client.DeliveryMethod = SmtpDeliveryMethod.Network;
//            client.UseDefaultCredentials = false;
//            client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

            //email mailer settings
//            email.Account.Address = emailSetting.MailSender;
//            email.Account.Name = emailSetting.MailSender;
//            email.Account.Host = emailSetting.EmailSmtpHostAddress;
//            email.Account.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
//            email.Account.UserName = emailSetting.EmailSmtpEmailAddress;
//            email.Account.Password = emailSetting.EmailSmtpEmailPassword;
//            email.Account.MessageTemplate = "Test";
//            email.Account.EnableSSL = emailSetting.EmailSmtpUseSSL;

            //email addressee settings
//            email.Address = recipient;
//            email.Subject = "Report from " + emailSetting.Name;
//            email.MessageBody = "This is an automated email";
//
//            email.Export = pdf; //Set export type
//            email.SendEmail(report1); //Send email
            
            using (MemoryStream memoryStream = new MemoryStream())
            {
                
                pdf.Export(report, (Stream) memoryStream);
                
                string fileName = report.FileName;
                string str1 = !string.IsNullOrEmpty(fileName) ? Path.GetFileNameWithoutExtension(fileName) : "Report";
                string str2 = ".fpx";
                
                string fileFilter = pdf.FileFilter;
                str2 = fileFilter.Substring(fileFilter.LastIndexOf('.'));
                if (pdf.HasMultipleFiles)
                    str2 = ".zip";
                
                string name = str1 + str2;
                memoryStream.Position = 0L;
                
                SmtpClient client = new SmtpClient();
                client.Port = Utils.ConvertString<int>(emailSetting.EmailSmtpPortNumber);
                client.Host = emailSetting.EmailSmtpHostAddress;
                if (emailSetting.EmailSmtpUseSSL)
                    client.EnableSsl = true;
                client.Timeout = 20000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential(emailSetting.EmailSmtpEmailAddress, emailSetting.EmailSmtpEmailPassword);

                string address = emailSetting.EmailSmtpEmailAddress;
                string displayName = emailSetting.MailSender;

                if (!address.Contains("@"))
                {
                    address = displayName;
                }
            
                MailMessage mm = new MailMessage(new MailAddress(address,
                        displayName),
                    new MailAddress(recipient));
                mm.Subject = "Report from " + emailSetting.Name;
                mm.Body = "This is an automated email";
                mm.BodyEncoding = UTF8Encoding.UTF8;
                mm.IsBodyHtml = true;
                mm.Attachments.Add(new Attachment((Stream) memoryStream, name));
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;

                client.Send(mm);
                
//                SmtpClient smtpClient = new SmtpClient();
//                using (MailMessage message = new MailMessage(new MailAddress(email.Account.Address, email.Account.Name),
//                    new MailAddress(email.Address)))
//                {
//                    if (email.CC != null)
//                    {
//                        foreach (string address in email.CC)
//                            message.CC.Add(new MailAddress(address));
//                    }
//                    message.Subject = email.Subject;
//                    message.SubjectEncoding = Encoding.UTF8;
//                    message.Body = email.MessageBody;
//                    message.BodyEncoding = Encoding.UTF8;
//                    message.Attachments.Add(new Attachment((Stream) memoryStream, name));
//                    smtpClient.Host = email.Account.Host;
//                    smtpClient.Port = email.Account.Port;
//                    if (!string.IsNullOrEmpty(email.Account.UserName) && !string.IsNullOrEmpty(email.Account.Password))
//                        smtpClient.Credentials =
//                            (ICredentialsByHost) new NetworkCredential(email.Account.UserName, email.Account.Password);
//                    if (email.Account.EnableSSL)
//                        smtpClient.EnableSsl = true;
////                    Config.ReportSettings.OnStartProgress(report);
////                    int num = (int) memoryStream.Length / 1024;
////                    if (num < 1)
////                        num = 1;
////                    Config.ReportSettings.OnProgress(report,
////                        string.Format(Res.Get("Export,Email,SendingEmail"), (object) num));
////                    try
////                    {
//                        smtpClient.Send(message);
////                    }
////                    finally
////                    {
////                        Config.ReportSettings.OnFinishProgress(report);
////                    }
//                }
            }
        }

        static void Main(string[] args)
        {
            try
            {
                #region survey link

                string now = "";

                if (args.Length > 0)
                {
                    now = args[0];
                }
                else
                {
                    now = DateTime.Now.ToString("HH:mm");
                }

                if ("09:00".Equals(now))
                {
                    List<Applicants> applicantList = ApplicantManager.GetApplicantWithNoSurveyInvitationList();
                    foreach (Applicants applicant in applicantList)
                    {
                        Companies company = ERecruitmentManager.GetCompany(applicant.CompanyCode);
                        int applicantAge = ((TimeSpan) (DateTime.Now - applicant.RegisteredDate)).Days;
                        if (!string.IsNullOrEmpty(company.SurveyQuestionCodes))
                        {
                            if (company.SurveyEmailTemplateId > 0)
                            {
                                if (applicantAge >= 30)
                                {
                                    Emails emailTemplate = ERecruitmentManager.GetEmail(company.SurveyEmailTemplateId);
                                    if (emailTemplate != null)
                                    {
                                        // compose message body
                                        string body = emailTemplate.Body.Replace("[Name]", applicant.Name)
                                            .Replace("[Company]", company.Name);
                                        // send email

                                        Domain.Helper.Mail.SendEmail(company, applicant.Email, emailTemplate.Subject,
                                            body, true);
                                        //                                    SendEmail(company, applicant.Email, emailTemplate.Subject, body);
                                    }
                                }
                            }
                        }
                    }
                }

                #endregion

                #region report subscriber

                List<ReportAutomatic> reportAutoList = ApplicationManager.GetReportAutoList(now);
                if (reportAutoList != null && reportAutoList.Count > 0)
                {
                    foreach (ReportAutomatic report in reportAutoList)
                    {
                        //List<ReportSubscribers> subscriberList = ApplicationManager.GetReportSubscriberList(report.Code);
                        DateTime sdt = Convert.ToDateTime(report.StartDate);
                        DateTime edt = Convert.ToDateTime(report.EndDate);

                        String from = sdt.ToString("dd/MM/yyyy");
                        String to = edt.ToString("dd/MM/yyyy");

                        string schedule = report.Schedule;
                        string[] email = report.Email.Split(new char[] {','}, StringSplitOptions.RemoveEmptyEntries);

                        switch (schedule)
                        {
                            case "Daily":

                                if (email != null && email.Length > 0)
                                {
                                    foreach (String code in email)
                                    {
                                        UserAccounts userAccount =
                                            ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                        string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                            ? userAccount.UserName
                                            : userAccount.Email;
                                        Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                        Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                        SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                            userAccount.CompanyCode);
                                    }
                                }
                                break;
                            case "Monday":
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "Tuesday":
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Tuesday)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "Wednesday":
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "Thursday":
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "Friday":
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "Saturday":
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "Sunday":
                                if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "FirstDayMonthly":
                                if (DateTime.Now.Day == 1)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                            case "LastDayMonthly":
                                if (DateTime.Now.Day == (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1))
                                    .AddMonths(1).AddDays(-1).Day)
                                {
                                    if (email != null && email.Length > 0)
                                    {
                                        foreach (String code in email)
                                        {
                                            UserAccounts userAccount =
                                                ERecruitment.Domain.AuthenticationManager.GetUserAccount(code);
                                            string toEmail = String.IsNullOrEmpty(userAccount.Email)
                                                ? userAccount.UserName
                                                : userAccount.Email;
                                            Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                            Reports fileReport = ERecruitmentManager.GetReportByCode(report.Report);
                                            SendReport(company, toEmail, fileReport.ReportFile, from, to,
                                                userAccount.CompanyCode);
                                        }
                                    }
                                }
                                break;
                        }
                    }
                }


                /*List<Reports> reportList = ApplicationManager.GetReportList();
                foreach (Reports report in reportList)
                {
                    List<ReportSubscribers> subscriberList = ApplicationManager.GetReportSubscriberList(report.Code);
                    string schedule = report.Schedule;
                    switch (schedule)
                    {
                        case "Daily":
                            foreach (ReportSubscribers subscriber in subscriberList)
                            {
                                UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                SendReport(company, userAccount.Email, report.ReportFile);
                            }
                            break;
                        case "Monday":
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Monday)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "Tuesday":
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Tuesday)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "Wednesday":
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Wednesday)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "Thursday":
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Thursday)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "Friday":
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Friday)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "Saturday":
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Saturday)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "Sunday":
                            if (DateTime.Now.DayOfWeek == DayOfWeek.Sunday)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "FirstDayMonthly":
                            if (DateTime.Now.Day == 1)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                        case "LastDayMonthly":
                            if (DateTime.Now.Day == (new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1)).AddMonths(1).AddDays(-1).Day)
                            {
                                foreach (ReportSubscribers subscriber in subscriberList)
                                {
                                    UserAccounts userAccount = ERecruitment.Domain.AuthenticationManager.GetUserAccount(subscriber.UserCode);
                                    Companies company = ERecruitmentManager.GetCompany(userAccount.CompanyCode);
                                    SendReport(company, userAccount.Email, report.ReportFile);
                                }
                            }
                            break;
                    }
                }*/

                #endregion
            }
            catch (Exception e)
            {
                throw e;
            }

        }
    }
}
